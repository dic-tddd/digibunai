Contributions to **DigiBunai™** project can be made in following ways:<br>

# Development

Contributions can be made in terms of fixing open [issues](https://gitlab.com/dic-tddd/digibunai/-/issues), optimizing functions, new features development. You may first need to get the [latest source code](https://gitlab.com/dic-tddd/digibunai#getting-source-code) and [build the project](https://gitlab.com/dic-tddd/digibunai#building).

# Code Formatting & Cleanup

Contributions may also be made by formatting source code files as per [Coding Standard](https://digibunai.dic.gov.in/index.php/developers-corner#coding-standard).

# Documentation

Contributions may also be made in terms of improving [Project Wiki](https://gitlab.com/dic-tddd/digibunai/-/wikis/home), adding/updating Javadoc, writing featured articles on DigiBunai™ features/capabilities etc.

# Translation

## Translation of application UI

Application uses key-value properties files for supporting multiple languages. To translate application UI in a new language, you need to translate all the values (words/sentences after =) in the [given properties file](https://gitlab.com/dic-tddd/digibunai/-/blob/master/mla/language/ENGLISH.properties) into that new language.<br><br>

Example: In properties file consider following line:
> WINDOWABOUTUS=About Software

Here value is “About Software”. This expression needs to be translated into the new language (E.g. Hindi Translation: सॉफ्टवेयर के बारे में). On updating this, corresponding entry in properties file will become:
> WINDOWABOUTUS=सॉफ्टवेयर के बारे में

Similarly, all other value entries needs to be translated into the new language. And updated properties file may be shared with the DigiBunai™ team for review.

## Translation of user manual, handouts, training video scripts

For contributing in translation of User Manual, Handouts or Training Video Scripts into a new language, please contact DigiBunai™ team for getting access to these files.
