/*
 * Copyright (C) Digital India Corporation (Media Lab Asia)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mla.fabric;

import com.mla.artwork.Artwork;
import com.mla.artwork.ArtworkAction;
import com.mla.artwork.ArtworkView;
import com.mla.cloth.ClothView;
import com.mla.dictionary.DictionaryAction;
import com.mla.main.Configuration;
import com.mla.main.DigiBunaiCommons;
import com.mla.main.EncryptZip;
import com.mla.main.IDGenerator;
import com.mla.main.Logging;
import com.mla.main.MessageView;
import com.mla.main.RadioOptionsView;
import com.mla.main.UndoRedo;
import com.mla.main.WindowView;
import com.mla.pattern.Pattern;
import com.mla.pattern.PatternAction;
import com.mla.pattern.PatternView;
import com.mla.print.PrintView;
import com.mla.secure.Security;
import com.mla.simulator.MappingEditView;
import com.mla.simulator.SimulatorEditView;
import com.mla.user.UserAction;
import com.mla.user.UserLoginView;
import com.mla.utility.AboutView;
import com.mla.utility.CardView;
import com.mla.utility.ContactView;
import com.mla.utility.Device;
import com.mla.utility.HelpView;
import com.mla.utility.TechnicalView;
import com.mla.utility.UtilityAction;
import com.mla.weave.Weave;
import com.mla.weave.WeaveAction;
import com.mla.weave.WeaveImportView;
import com.mla.weave.WeaveView;
import com.mla.yarn.Yarn;
import com.mla.yarn.YarnAction;
import com.mla.yarn.YarnEditView;
import com.sun.media.jai.codec.ByteArraySeekableStream;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.SeekableStream;
import java.awt.BasicStroke;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.awt.print.PrinterJob;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import javax.imageio.ImageIO;
import javax.media.jai.PlanarImage;
/**
 *
 * @Designing GUI window for fabric editor
 * @author Amit Kumar Singh
 * 
 */
public class FabricView extends Application {
  
    PrinterJob objPrinterJob = PrinterJob.getPrinterJob();
    javax.print.attribute.HashPrintRequestAttributeSet objPrinterAttribute = new javax.print.attribute.HashPrintRequestAttributeSet();
    Process objProcess_ProcessBuilder;
                        
    Configuration objConfiguration = null;
    DictionaryAction objDictionaryAction = null;
    Fabric objFabric;
    Weave objWeave;
    Weave objWeaveR;
    UndoRedo objUR;
    
    FabricAction objFabricAction;
    ArtworkAction objArtworkAction;
    WeaveAction objWeaveAction;
    public static Stage fabricStage;
    private Stage fabricChildStage;

    private String selectedMenu = "FILE";
    
    private Yarn[] warpYarn;
    private Yarn[] weftYarn;
    private Yarn[] warpExtraYarn;
    private Yarn[] weftExtraYarn;
    private ArrayList<Yarn> lstYarn = null;
    private Map<String, String> warpColorPalete = new HashMap<>();
    private Map<String, String> weftColorPalete = new HashMap<>();    
    private String[] threadPaletes;
    private float zoomfactor = 1;
    private byte graphFactor = 2;
    private byte plotViewActionMode = 1; //1= composite, 2 = grid, 3 = graph, 4 = graph m/c, 5 = visulization, 6 = flip, 7 = switch, 8 = simulation 2D, 9 = simulation 3D
    private byte plotEditActionMode = 1; //1= others, 3 = graph edit, 4 = weave edit, 
    private String editRepeatMode = "Rectangular (Default)";
    private int editVerticalRepeatValue=1; //base weft
    private int editHorizontalRepeatValue=1; //base warp
    private int editThreadPrimaryValue=0; //base weft
    private int editThreadSecondaryValue=1; //base warp
    private boolean isNew = true;
    private boolean isWorkingMode = false;
    private boolean isEditingMode = false;
    private boolean yarnSimulationMode = false;
    private int[] yarnGridSize = {1,2,3,4,5,6,7,8,9,10,11,12,15,20,25,30,40,50,100};
    private int yarnGridSizeIndex = 9; // points to element 10
    private int yarnSpacing = 1;
    private java.awt.Color emptySpaceColor = java.awt.Color.BLACK;
    
    private MenuButton menuSelected;
    private ToolBar toolBar;
    private MenuBar menuBar;    
    private BorderPane root;
    private Scene scene;
    private ScrollPane container;
    private GridPane bodyContainer;
    private Label lblStatus;
    private ProgressBar progressB;
    private ProgressIndicator progressI;
    private ContextMenu contextMenu;
    
    private ImageView fabric;
    private Label hRuler;
    private Label vRuler;
    private Menu homeMenu;
    private Menu fileMenu;
    private Menu editMenu;
    private Menu viewMenu;
    private Menu utilityMenu;
    private Menu supportMenu;
    private Menu runMenu;
    private Menu transformMenu;
    private Label fileMenuLabel;
    private Label editMenuLabel;
    private Label viewMenuLabel;
    private Label utilityMenuLabel;
    private Label runMenuLabel;
    private Label transformMenuLabel;
    
 /**
 * FabricView(Stage)
 * <p>
 * This constructor is used for individual call of class. 
 * 
 * @param       Stage primaryStage
 * @author      Amit Kumar Singh
 * @version     %I%, %G%
 * @since       1.0
 * @see         javafx.stage.*;
 * @link        WindowView
 */
    public FabricView(final Stage primaryStage) {}
 /**
 * FabricView(Configuration)
 * <p>
 * This class is used for prompting fabric editor. 
 * 
 * @param       Configuration objConfigurationCall
 * @author      Amit Kumar Singh
 * @version     %I%, %G%
 * @since       1.0
 * @see         javafx.event.*;
 * @link        WindowView
 */
    public FabricView(Configuration objConfigurationCall) {
        objConfiguration = objConfigurationCall;
        objDictionaryAction = new DictionaryAction(objConfiguration);
        objUR = new UndoRedo();
        //System.err.println("I am in Fabric : "+objConfiguration.getStrClothType()+"="+objConfiguration.getStrRecentFabric());
                
        objFabric= new Fabric();
        objFabric.setObjConfiguration(objConfiguration);
        
        fabricStage = new Stage();
        root = new BorderPane();
        scene = new Scene(root, objConfiguration.WIDTH, objConfiguration.HEIGHT, Color.WHITE);
        scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        resolutionControl();
        
        HBox footContainer = new HBox();
        progressB = new ProgressBar(0);
        progressB.setVisible(false);
        progressI = new ProgressIndicator(0);
        progressI.setVisible(false);
        lblStatus = new Label(objDictionaryAction.getWord("WELCOMETOCADTOOL"));
        lblStatus.setId("message");
        footContainer.getChildren().addAll(lblStatus,progressB,progressI);
        footContainer.setId("footContainer");
        root.setBottom(footContainer);        
        /*
        VBox topContainer = new VBox();
        toolBar = new ToolBar();
        toolBar.setMinHeight(95);
        menuBar = new MenuBar();
        menuBar.prefWidthProperty().bind(fabricStage.widthProperty());
        topContainer.getChildren().add(menuBar);
        topContainer.getChildren().add(toolBar); 
        topContainer.setId("topContainer");
        root.setTop(topContainer);
        */
        VBox topContainer = new VBox(); 
        menuBar = new MenuBar();
        menuBar.prefWidthProperty().bind(fabricStage.widthProperty());
        
        toolBar = new ToolBar();
        //toolBar.setMinHeight(45);
        menuSelected = new MenuButton();
        menuSelected.setGraphic(new ImageView("/media/text_menu.png"));
        menuSelected.setAlignment(Pos.CENTER_RIGHT);
        menuSelected.setPickOnBounds(false);
        menuSelected.setStyle("-fx-background-color: rgba(0,0,0,0);"); 
        
        AnchorPane toolAnchor = new AnchorPane();
        toolAnchor.getChildren().addAll(menuSelected);
        AnchorPane.setRightAnchor(menuSelected, 0.0);
        toolAnchor.setPickOnBounds(false);
        
        StackPane toolPane=new StackPane();
        toolPane.getChildren().addAll(toolBar,toolAnchor);
        
        /*
        final Button showHideToolBar = new Button();
        showHideToolBar.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/a_show.png"));
        showHideToolBar.setId("showing");
        showHideToolBar.setPrefSize(15, 15);
        showHideToolBar.setAlignment(Pos.TOP_RIGHT);
        showHideToolBar.setPickOnBounds(false);
        showHideToolBar.setStyle("-fx-background-color: rgba(255,255,255,0.5);");         
        AnchorPane menuAnchor = new AnchorPane();
        menuAnchor.getChildren().addAll(showHideToolBar);
        AnchorPane.setRightAnchor(showHideToolBar, 1.0);
        menuAnchor.setPickOnBounds(false);        
        showHideToolBar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                if(showHideToolBar.getId().equalsIgnoreCase("showing")){
                    toolBar.setVisible(false);
                    //toolBar.setPrefSize(0, 0);
                    showHideToolBar.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/a_hide.png"));
                    showHideToolBar.setId("hiding");
                }else{
                    toolBar.setVisible(true);
                    showHideToolBar.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/a_show.png"));
                    showHideToolBar.setId("showing");
                }
            }
        });
        StackPane menuPane=new StackPane();
        menuPane.getChildren().addAll(menuBar,menuAnchor);
        */
        
        topContainer.getChildren().add(menuBar);
        topContainer.getChildren().add(toolPane); 
        topContainer.setId("topContainer");
        root.setTop(topContainer);
        
        //menu bar and events
        homeMenu  = new Menu();
        HBox homeMenuHB = new HBox();
        homeMenuHB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/home.png"),new Label(objDictionaryAction.getWord("HOME")));
        homeMenu.setGraphic(homeMenuHB);
        homeMenu.setAccelerator(new KeyCodeCombination(KeyCode.M, KeyCombination.SHIFT_DOWN));
        homeMenuHB.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                homeMenuAction();  
                me.consume();
            }
        });       
        // File menu - new, save, save as, open, load recent.
        fileMenuLabel = new Label(objDictionaryAction.getWord("FILE"));
        fileMenu = new Menu();
        fileMenu.setGraphic(fileMenuLabel);
        fileMenu.setAccelerator(new KeyCodeCombination(KeyCode.F, KeyCombination.SHIFT_DOWN));
        fileMenuLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                fileMenuAction();            
            }
        });
        // Edit menu - Toolbar, Color, Composite View, Support Lines, Ruler, Zoom-in, zoom-out
        editMenuLabel = new Label(objDictionaryAction.getWord("EDIT"));
        editMenu = new Menu();
        editMenu.setGraphic(editMenuLabel);
        editMenu.setAccelerator(new KeyCodeCombination(KeyCode.E, KeyCombination.SHIFT_DOWN));
        editMenuLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                editMenuAction();
            }
        });  
        // View menu - Toolbar, Color, Composite View, Support Lines, Ruler, Zoom-in, zoom-out
        viewMenuLabel = new Label(objDictionaryAction.getWord("VIEW"));
        viewMenu = new Menu();
        viewMenu.setGraphic(viewMenuLabel);
        viewMenu.setAccelerator(new KeyCodeCombination(KeyCode.V, KeyCombination.SHIFT_DOWN));
        viewMenuLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                viewMenuAction();
            }
        });  
        // Utility menu - Weave, Calculation, Language
        utilityMenuLabel = new Label(objDictionaryAction.getWord("UTILITY"));
        utilityMenu = new Menu();
        utilityMenu.setGraphic(utilityMenuLabel);
        utilityMenu.setAccelerator(new KeyCodeCombination(KeyCode.U, KeyCombination.SHIFT_DOWN));
        utilityMenuLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                utilityMenuAction();
            }
        });
        //Help Menu items
        Label supportMenuLabel = new Label(objDictionaryAction.getWord("SUPPORT"));
        supportMenuLabel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUPPORT")));
        supportMenu = new Menu();
        supportMenu.setGraphic(supportMenuLabel);
        
        MenuItem helpMenuItem = new MenuItem(objDictionaryAction.getWord("HELP"));
        helpMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        helpMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.H, KeyCombination.SHIFT_DOWN));
        helpMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {       
                helpMenuAction();
            }
        });        
        MenuItem technicalMenuItem = new MenuItem(objDictionaryAction.getWord("TECHNICAL"));
        technicalMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/technical_info.png"));
        technicalMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.T, KeyCombination.SHIFT_DOWN));
        technicalMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {       
                technicalMenuAction();
            }
        });
        MenuItem aboutMenuItem = new MenuItem(objDictionaryAction.getWord("ABOUTUS"));
        aboutMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/about_software.png"));
        aboutMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.A, KeyCombination.SHIFT_DOWN));
        aboutMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                aboutMenuAction();
            }
        });
        MenuItem contactMenuItem = new MenuItem(objDictionaryAction.getWord("CONTACTUS"));
        contactMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/contact_us.png"));
        contactMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.SHIFT_DOWN));
        contactMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                contactMenuAction();
            }
        });
        MenuItem logoutMenuItem = new MenuItem(objDictionaryAction.getWord("LOGOUT"));
        logoutMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/logout.png"));
        logoutMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.L, KeyCombination.SHIFT_DOWN));
        logoutMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                logoutMenuAction();
            }
        });
        MenuItem exitMenuItem = new MenuItem(objDictionaryAction.getWord("EXIT"));
        exitMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/quit.png"));
        exitMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.ESCAPE));
        exitMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                exitMenuAction();
            }
        });     
        //run menu: view all in one go
        runMenuLabel = new Label();
        runMenuLabel.setGraphic(new ImageView("/media/run.png"));
        runMenuLabel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPREALTIMEVIEW")));
        //runMenuLabel.setStyle("-fx-background-image:url('/media/run.png');-fx-background-repeat: no-repeat;-fx-background-size: 10, 10;-fx-pref-height:10;");
        runMenu  = new Menu();
        runMenu.setGraphic(runMenuLabel);
        runMenu.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.SHIFT_DOWN));
        runMenuLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                runMenuAction();
            }
        });
        // Transform Operation menu - Weave, Calculation, Language
        transformMenuLabel = new Label(objDictionaryAction.getWord("TRANSFORMOPERATIONEDIT"));
        transformMenu = new Menu();
        transformMenu.setGraphic(transformMenuLabel);
        transformMenu.hide();
        transformMenu.setVisible(false);
        transformMenu.setDisable(true);
        transformMenu.setAccelerator(new KeyCodeCombination(KeyCode.U, KeyCombination.SHIFT_DOWN));
        transformMenuLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                transformMenuAction();
            }
        });
        supportMenu.getItems().addAll(helpMenuItem, technicalMenuItem, aboutMenuItem, contactMenuItem, new SeparatorMenuItem(), logoutMenuItem);//exitMenuItem);
        menuBar.getMenus().addAll(homeMenu,fileMenu, editMenu, viewMenu, utilityMenu, supportMenu, runMenu, transformMenu);
        
        container = new ScrollPane();
        container.setPrefSize(objConfiguration.WIDTH, objConfiguration.HEIGHT);
        
        bodyContainer = new GridPane();
        bodyContainer.setMinSize(objConfiguration.WIDTH, objConfiguration.HEIGHT);
        bodyContainer.setVgap(0);
        bodyContainer.setHgap(0);
                       
        fabric = new ImageView(); 
        root.setCenter(container);
        
        //setting Context menu and events
        addContextMenu();        
        // Code Added for ShortCuts
        addAccelratorKey();
        //Manage Events
        addFabricEventHandler();
        
        container.setContent(fabric);
        selectedMenu = "FILE";
        menuHighlight();
        
        fabricStage.getIcons().add(new Image("/media/icon.png"));
        fabricStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWFABRICEDITOR")+"( "+objConfiguration.getStrClothType()+" ) \u00A9 "+objDictionaryAction.getWord("TITLE"));
        fabricStage.setX(0);
        fabricStage.setY(0);
        //fabricStage.setFocused(true);
        //fabricStage.initModality(Modality.WINDOW_MODAL);
        //fabricStage.initOwner(fabricStage);
        //fabricStage.setIconified(true);
        //fabricStage.setFullScreen(true);
        fabricStage.setResizable(false);
        fabricStage.setScene(scene);
        fabricStage.show();
        fabricStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                homeMenuAction(); 
                we.consume();
            }
        });
        //manage context switching
        if(objConfiguration.getStrRecentArtwork()!=null && !objConfiguration.getStrRecentArtwork().trim().equalsIgnoreCase("") && objConfiguration.strWindowFlowContext.equalsIgnoreCase("ArtworkEditor")){
            //System.err.println("point 1"+objConfiguration.getStrClothType());
            objFabric.setStrBaseWeaveID(objConfiguration.getStrRecentWeave());
            newFabric();
        } else if(objConfiguration.getStrRecentWeave()!=null && !objConfiguration.getStrRecentWeave().trim().equalsIgnoreCase("") && objConfiguration.strWindowFlowContext.equalsIgnoreCase("WeaveEditor")){
            //System.err.println("point 2"+objConfiguration.getStrClothType());
            objFabric.setStrBaseWeaveID(objConfiguration.getStrRecentWeave());
            newFabric();
        } else if(objConfiguration.getStrRecentFabric()!=null && !objConfiguration.getStrRecentFabric().trim().equalsIgnoreCase("") && (objConfiguration.strWindowFlowContext.equalsIgnoreCase("Dashboard") || objConfiguration.strWindowFlowContext.equalsIgnoreCase("ClothEditor"))){
            //System.err.println("point 3"+objConfiguration.getStrClothType());
            objFabric.setStrFabricID(objConfiguration.getStrRecentFabric());
            try {
                FabricAction objFabricAction = new FabricAction();        
                objFabricAction.getFabric(objFabric, "Main");
                //objConfiguration.setStrClothType(objFabric.getStrClothType());
                //UserAction objUserAction = new UserAction();
                //objUserAction.getConfiguration(objConfiguration);
                //objConfiguration.clothRepeat();
                objFabric.setObjConfiguration(objConfiguration);
                loadFabric();
            } catch (SQLException ex) {
                //Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            } catch (Exception ex) {
                //Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        } else if(objConfiguration.getStrRecentArtwork()!=null && objConfiguration.getStrRecentFabric()!=null){
            //System.err.println("point 4"+objConfiguration.getStrClothType());
            final Stage dialogStage = new Stage();
            dialogStage.initStyle(StageStyle.UTILITY);
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.setResizable(false);
            dialogStage.setIconified(false);
            dialogStage.setFullScreen(false);
            dialogStage.setTitle(objDictionaryAction.getWord("ALERT"));
            BorderPane root = new BorderPane();
            Scene scene = new Scene(root, 300, 100, Color.WHITE);
            GridPane popup=new GridPane();
            popup.setHgap(5);
            popup.setVgap(5);
            popup.setPadding(new Insets(25, 25, 25, 25));
            popup.add(new ImageView("/media/alert_warning.png"), 0, 0); 
            Label lblAlert = new Label(objDictionaryAction.getWord("ALERTWHAT"));
            lblAlert.setStyle("-fx-wrap-text:true;");
            lblAlert.setPrefWidth(250);
            popup.add(lblAlert, 1, 0);
            Button btnYes = new Button(objDictionaryAction.getWord("RECENT")+" "+objDictionaryAction.getWord("ARTWORK"));
            btnYes.setPrefWidth(150);
            btnYes.setStyle("-fx-text-fill: #ffffff; -fx-padding: 1; -fx-font-size: 13px; -fx-font-weight: bold; -fx-background-color: linear-gradient(#00ff00, #009900); -fx-effect: dropshadow( three-pass-box , rgba(0,1,1,0.6) , 5, 0.0 , 0 , 1 );");
            btnYes.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    dialogStage.close();
                    System.gc();
                    objFabric.setStrBaseWeaveID(objConfiguration.getStrRecentWeave());
                    newFabric();
                }
            });
            Button btnNo = new Button(objDictionaryAction.getWord("RECENT")+" "+objDictionaryAction.getWord("FABRIC"));
            btnNo.setPrefWidth(150);
            btnNo.setStyle("-fx-text-fill: #ffffff; -fx-padding: 1; -fx-font-size: 13px; -fx-font-weight: bold; -fx-background-color: linear-gradient(#0000ff, #0000ff); -fx-effect: dropshadow( three-pass-box , rgba(0,1,1,0.6) , 5, 0.0 , 0 , 1 );");
            btnNo.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    objFabric.setStrFabricID(objConfiguration.getStrRecentFabric());
                    try {
                        FabricAction objFabricAction = new FabricAction();        
                        objFabricAction.getFabric(objFabric, "Main");
                        objConfiguration.setStrClothType(objFabric.getStrClothType());
                        UserAction objUserAction = new UserAction();
                        objUserAction.getConfiguration(objConfiguration);
                        //objConfiguration.clothRepeat();
                        objFabric.setObjConfiguration(objConfiguration);
                        loadFabric();
                    } catch (SQLException ex) {
                        new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                        lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                    } catch (Exception ex) {
                        new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                        lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                    }
                    dialogStage.close();
                    System.gc();
                }
            });                
            HBox hbbtn =new HBox();
            hbbtn.getChildren().addAll(btnYes,btnNo);
            popup.add(hbbtn, 0, 1, 2, 1);
            root.setCenter(popup);
            dialogStage.setScene(scene);
            dialogStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                public void handle(WindowEvent we) {
                    objFabric.setStrFabricID(objConfiguration.getStrRecentFabric());
                    try {
                        FabricAction objFabricAction = new FabricAction();        
                        objFabricAction.getFabric(objFabric, "Main");
                        objConfiguration.setStrClothType(objFabric.getStrClothType());
                        UserAction objUserAction = new UserAction();
                        objUserAction.getConfiguration(objConfiguration);
                        //objConfiguration.clothRepeat();
                        objFabric.setObjConfiguration(objConfiguration);
                        loadFabric();
                    } catch (SQLException ex) {
                        new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                        lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                    } catch (Exception ex) {
                        new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                        lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                    }
                    dialogStage.close();
                    System.gc();
                }
            });
            dialogStage.showAndWait();
        } else{       
            //System.err.println("point 5"+objConfiguration.getStrClothType());
            try {
                UserAction objUserAction = new UserAction();
                objUserAction.getConfiguration(objConfiguration);
                //objConfiguration.clothRepeat();
            } catch (SQLException ex) {
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            } catch (Exception ex) {
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        }
        //objConfiguration.strWindowFlowContext = "ClothEditor";
    }
/**
* resolutionControl(Stage)
* <p>
* This function is used for reassign width and height. 
*  
* @param       Stage primaryStage
* @author      Amit Kumar Singh
* @version     %I%, %G%
* @since       1.0
* @see         javafx.stage.*;
* @link        WindowView
*/
    private void resolutionControl(){
        //windowStage.resizableProperty().addListener(listener);
        scene.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                setGlobalWidthHeight();
                fabricStage.setHeight(objConfiguration.HEIGHT);
                fabricStage.setWidth(objConfiguration.WIDTH);
                //selectedMenu = "FILE";
                menuHighlight();
            }
        });
        scene.heightProperty().addListener(new ChangeListener<Number>() {
            @Override 
            public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneHeight, Number newSceneHeight) {
                setGlobalWidthHeight();
                fabricStage.setHeight(objConfiguration.HEIGHT);
                fabricStage.setWidth(objConfiguration.WIDTH);
                //selectedMenu = "FILE";
                menuHighlight();
            }
        });
    }
    private void setGlobalWidthHeight(){
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        objConfiguration.WIDTH = screenSize.getWidth();
        objConfiguration.HEIGHT = screenSize.getHeight();
        objConfiguration.strIconResolution = (objConfiguration.WIDTH<1280)?"hd_none":(objConfiguration.WIDTH<1920)?"hd":(objConfiguration.WIDTH<2560)?"hd_full":"hd_quad";
    }
       
    /**
     * addContextMenu
     * <p>
     * Function use for drawing menu bar for menu item,
     * and binding events for each menus with style. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void addContextMenu(){
        contextMenu = new ContextMenu();
        MenuItem jaquardConverstion = new MenuItem(objDictionaryAction.getWord("JACQUARDCONVERSIONEDIT"));
        jaquardConverstion.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/assign_artwork_weave.png"));
        MenuItem graphCorrection = new MenuItem(objDictionaryAction.getWord("GRAPHCORRECTIONEDIT"));
        graphCorrection.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/graph_editor.png"));
        MenuItem density = new MenuItem(objDictionaryAction.getWord("DENSITYEDIT"));
        density.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/density.png"));
        MenuItem yarnProperties = new MenuItem(objDictionaryAction.getWord("YARNEDIT"));
        yarnProperties.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/yarn.png"));
        MenuItem switchColor = new MenuItem(objDictionaryAction.getWord("SWITCHCOLOREDIT"));
        switchColor.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/switch_color.png"));
        MenuItem threadSequence = new MenuItem(objDictionaryAction.getWord("THREADPATTERNEDIT"));
        threadSequence.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/thread_pattern.png"));        
        MenuItem transformOperation = new MenuItem(objDictionaryAction.getWord("TRANSFORMOPERATIONEDIT"));
        transformOperation.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/edit.png"));
        contextMenu.getItems().addAll(jaquardConverstion, graphCorrection, density, yarnProperties, switchColor, threadSequence, transformOperation);
        jaquardConverstion.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                event.consume();
                jacquardConverstionAction();
            }
        });
        graphCorrection.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                event.consume();
                graphCorrectionAction();
            }
        });
        density.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                event.consume();
                densityAction();
            }
        });
        yarnProperties.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                event.consume();
                yarnPropertiesAction();
            }
        });
        switchColor.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                event.consume();
                switchColorAction();
            }
        });
        threadSequence.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                event.consume();
                threadSequenceAction();
            }
        });
        transformOperation.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                event.consume();
                transformOperationAction();
            }
        });
        /*contextMenu.setOnShowing(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent e) {
                System.out.println("showing");
            }
        });
        contextMenu.setOnShown(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent e) {
                System.out.println("shown");
            }
        });
        contextMenu.setOnHiding(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent e) {
                System.out.println("hiding");
            }
        });
        contextMenu.setOnHidden(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent e) {
                System.out.println("hidden");
            }
        });*/
        container.setContextMenu(contextMenu);
        //contextMenu.hide();
        container.setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {
            @Override
            public void handle(ContextMenuEvent event) {
                if(objFabric==null || plotEditActionMode == 4 || plotEditActionMode == 3){
                    contextMenu.hide();
                    event.consume();
                    //System.err.println(editThreadPrimaryValue+"=isPrimaryButtonDown"+editThreadSecondaryValue);
                }
            }
        });
    }
    /**
     * addAccelratorKey
     * <p>
     * Function use for adding shortcut key combinations. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void addAccelratorKey(){
        // shortcuts variable names: kc + Alphabet + ALT|CONTROL|SHIFT // ACS alphabetical
        final KeyCodeCombination homeMenu = new KeyCodeCombination(KeyCode.M, KeyCombination.SHIFT_DOWN); // Home Menu
        final KeyCodeCombination fileMenu = new KeyCodeCombination(KeyCode.F, KeyCombination.SHIFT_DOWN); // File Menu
        final KeyCodeCombination editMenu = new KeyCodeCombination(KeyCode.E, KeyCombination.SHIFT_DOWN); // Edit Menu
        final KeyCodeCombination viewMenu = new KeyCodeCombination(KeyCode.V, KeyCombination.SHIFT_DOWN); // View Menu
        final KeyCodeCombination utilityMenu = new KeyCodeCombination(KeyCode.U, KeyCombination.SHIFT_DOWN); // Utility Menu
        //utility menu items
        final KeyCodeCombination infoUtility = new KeyCodeCombination(KeyCode.I, KeyCombination.SHIFT_DOWN); // info Menu
        final KeyCodeCombination consumptionUtility = new KeyCodeCombination(KeyCode.Q, KeyCombination.SHIFT_DOWN); // consumption
        final KeyCodeCombination priceUtility = new KeyCodeCombination(KeyCode.R, KeyCombination.SHIFT_DOWN); // price 
        final KeyCodeCombination weaveUtility = new KeyCodeCombination(KeyCode.W, KeyCombination.SHIFT_DOWN); // weave
        final KeyCodeCombination artworkUtility = new KeyCodeCombination(KeyCode.D, KeyCombination.SHIFT_DOWN); // artwork
        final KeyCodeCombination clothUtility = new KeyCodeCombination(KeyCode.G, KeyCombination.SHIFT_DOWN); // cloth
        final KeyCodeCombination deviceUtility = new KeyCodeCombination(KeyCode.N, KeyCombination.SHIFT_DOWN); // punch application
        final KeyCodeCombination cardViewUtility = new KeyCodeCombination(KeyCode.V, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // card view
        //view menu items
        final KeyCodeCombination frontSideView = new KeyCodeCombination(KeyCode.F1, KeyCombination.CONTROL_DOWN); // front side
        final KeyCodeCombination rearSideView = new KeyCodeCombination(KeyCode.F2, KeyCombination.CONTROL_DOWN); // rear side
        final KeyCodeCombination frontVisualizationView = new KeyCodeCombination(KeyCode.F4, KeyCombination.CONTROL_DOWN); // front visualization
        final KeyCodeCombination rearVisualizationView = new KeyCodeCombination(KeyCode.F5, KeyCombination.CONTROL_DOWN); // rear visualization
        final KeyCodeCombination flipVisualizationView = new KeyCodeCombination(KeyCode.F6, KeyCombination.CONTROL_DOWN); // flip visualization
        final KeyCodeCombination frontCrossSectionView = new KeyCodeCombination(KeyCode.F7, KeyCombination.CONTROL_DOWN); // front cut
        final KeyCodeCombination rearCrossSectionView = new KeyCodeCombination(KeyCode.F8, KeyCombination.CONTROL_DOWN); // rear cut
        final KeyCodeCombination gridView = new KeyCodeCombination(KeyCode.F10, KeyCombination.CONTROL_DOWN); // grid
        final KeyCodeCombination graphView = new KeyCodeCombination(KeyCode.F11, KeyCombination.CONTROL_DOWN); // graph 
        final KeyCodeCombination tilledView = new KeyCodeCombination(KeyCode.F12, KeyCombination.CONTROL_DOWN); // tilled view
        final KeyCodeCombination yarnSimulationView = new KeyCodeCombination(KeyCode.F4, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // yarn simulation
        //final KeyCodeCombination simulationView = new KeyCodeCombination(KeyCode.F4, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // simulation
        final KeyCodeCombination mappingView = new KeyCodeCombination(KeyCode.F2, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // mapping
        final KeyCodeCombination zoomInView = new KeyCodeCombination(KeyCode.EQUALS, KeyCombination.CONTROL_DOWN); // zoom in
        final KeyCodeCombination normalView = new KeyCodeCombination(KeyCode.ENTER, KeyCombination.CONTROL_DOWN); // zoom normal
        final KeyCodeCombination zoomOutView = new KeyCodeCombination(KeyCode.MINUS, KeyCombination.CONTROL_DOWN); // zoom out
        final KeyCodeCombination zoomInAlternateView = new KeyCodeCombination(KeyCode.ADD, KeyCombination.CONTROL_DOWN); // zoom in
        final KeyCodeCombination zoomOutAlternateView = new KeyCodeCombination(KeyCode.SUBTRACT, KeyCombination.CONTROL_DOWN); // zoom out
        //Edit menu items
        final KeyCodeCombination undoEdit = new KeyCodeCombination(KeyCode.Z, KeyCombination.CONTROL_DOWN); // undo
        final KeyCodeCombination redoEdit = new KeyCodeCombination(KeyCode.Y, KeyCombination.CONTROL_DOWN); // redo
        final KeyCodeCombination jacquardConversionEdit = new KeyCodeCombination(KeyCode.J, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // artwork assingment
        final KeyCodeCombination graphCorrectionEdit = new KeyCodeCombination(KeyCode.G, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN); // edit graph
        final KeyCodeCombination densityEdit = new KeyCodeCombination(KeyCode.D, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // edit density
        final KeyCodeCombination yarnEdit = new KeyCodeCombination(KeyCode.Y, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // edit yarn
        final KeyCodeCombination threadPatternEdit = new KeyCodeCombination(KeyCode.T, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // thread pattern
        final KeyCodeCombination switchColorEdit = new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // switch color
        final KeyCodeCombination repeatEdit = new KeyCodeCombination(KeyCode.R, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN); // repeats
        final KeyCodeCombination transformOperationEdit = new KeyCodeCombination(KeyCode.W, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // weaving pattern
        // Transform Menu Items
        final KeyCodeCombination vMirrorTransform = new KeyCodeCombination(KeyCode.V, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN); // vertical mirror
        final KeyCodeCombination hMirrorTransform = new KeyCodeCombination(KeyCode.H, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN); // horizontal mirror
        final KeyCodeCombination clockwiseRotateTransform = new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN); // clockwise rotation
        final KeyCodeCombination antiClockwiseRotateTransform = new KeyCodeCombination(KeyCode.A, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN); // anti-clockwise rotation
        final KeyCodeCombination moveRightTransform = new KeyCodeCombination(KeyCode.RIGHT, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // move right
        final KeyCodeCombination moveLeftTransform = new KeyCodeCombination(KeyCode.LEFT, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // move left
        final KeyCodeCombination moveUpTransform = new KeyCodeCombination(KeyCode.UP, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // move up
        final KeyCodeCombination moveDownTransform = new KeyCodeCombination(KeyCode.DOWN, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // move down
        final KeyCodeCombination move8RightTransform = new KeyCodeCombination(KeyCode.RIGHT, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN); // move 8 right
        final KeyCodeCombination move8LeftTransform = new KeyCodeCombination(KeyCode.LEFT, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN); // move 8 left
        final KeyCodeCombination move8UpTransform = new KeyCodeCombination(KeyCode.UP, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN); // move 8 up
        final KeyCodeCombination move8DownTransform = new KeyCodeCombination(KeyCode.DOWN, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN); // move 8 down
        final KeyCodeCombination tiltRightTransform = new KeyCodeCombination(KeyCode.RIGHT, KeyCombination.ALT_DOWN, KeyCombination.CONTROL_DOWN); // tilt right
        final KeyCodeCombination tiltLeftTransform = new KeyCodeCombination(KeyCode.LEFT, KeyCombination.ALT_DOWN, KeyCombination.CONTROL_DOWN); // tilt left
        final KeyCodeCombination tiltUpTransform = new KeyCodeCombination(KeyCode.UP, KeyCombination.ALT_DOWN, KeyCombination.CONTROL_DOWN); // tilt up
        final KeyCodeCombination tiltDownTransform = new KeyCodeCombination(KeyCode.DOWN, KeyCombination.ALT_DOWN, KeyCombination.CONTROL_DOWN); // tilt down
        final KeyCodeCombination invertTransform = new KeyCodeCombination(KeyCode.I, KeyCombination.ALT_DOWN, KeyCombination.CONTROL_DOWN); // inversion
        final KeyCodeCombination closeTransform = new KeyCodeCombination(KeyCode.ESCAPE); // close
        //File menu items
        final KeyCodeCombination newFile = new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN); // new
        final KeyCodeCombination openFile = new KeyCodeCombination(KeyCode.O, KeyCombination.CONTROL_DOWN); // open
        final KeyCodeCombination loadFile = new KeyCodeCombination(KeyCode.L, KeyCombination.CONTROL_DOWN); // load recent
        final KeyCodeCombination saveFile = new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN); // save
        final KeyCodeCombination saveAsFile = new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // save as
        final KeyCodeCombination saveTextureFile = new KeyCodeCombination(KeyCode.E, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // save as texture
        final KeyCodeCombination saveXMLFile = new KeyCodeCombination(KeyCode.X, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // save xml
        final KeyCodeCombination saveTxtFile = new KeyCodeCombination(KeyCode.F, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // save txt file
        final KeyCodeCombination saveGridFile = new KeyCodeCombination(KeyCode.H, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // save as grid
        final KeyCodeCombination saveGraphFile = new KeyCodeCombination(KeyCode.G, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // save as graph
        final KeyCodeCombination printFile = new KeyCodeCombination(KeyCode.P, KeyCombination.CONTROL_DOWN); // print
        
        scene.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                if(homeMenu.match(t))
                    homeMenuAction();
                else if(fileMenu.match(t) && plotEditActionMode != 4)
                    fileMenuAction();
                else if(editMenu.match(t) && plotEditActionMode != 4)
                    editMenuAction();
                else if(viewMenu.match(t) && plotEditActionMode != 4)
                    viewMenuAction();
                else if(utilityMenu.match(t) && plotEditActionMode != 4)
                    utilityMenuAction();
                else if(infoUtility.match(t) && isWorkingMode && plotEditActionMode != 4)
                    fabricSettingAction();
                else if(consumptionUtility.match(t) && isWorkingMode && plotEditActionMode != 4)
                    consumptionCalculatorAction();
                else if(priceUtility.match(t) && isWorkingMode && plotEditActionMode != 4)
                    priceCalculatorAction();
                else if(weaveUtility.match(t) && isWorkingMode && plotEditActionMode != 4)
                    weaveUtilityAction();
                else if(artworkUtility.match(t) && isWorkingMode && plotEditActionMode != 4)
                    artworkUtilityAction();
                else if(clothUtility.match(t) && isWorkingMode && plotEditActionMode != 4)
                    clothUtilityAction();
                else if(deviceUtility.match(t) && isWorkingMode && plotEditActionMode != 4)
                    punchApplicationAction();
                else if(cardViewUtility.match(t) && isWorkingMode && plotEditActionMode != 4)
                    cardViewAction();
                else if(frontSideView.match(t) && isWorkingMode && plotEditActionMode != 4)
                    frontSideViewAction();
                else if(rearSideView.match(t) && isWorkingMode && plotEditActionMode != 4)
                    rearSideViewAction();
                else if(frontVisualizationView.match(t) && isWorkingMode && plotEditActionMode != 4)
                    frontVisualizationViewAction();
                else if(rearVisualizationView.match(t) && isWorkingMode && plotEditActionMode != 4)
                    rearVisualizationViewAction();
                else if(flipVisualizationView.match(t) && isWorkingMode && plotEditActionMode != 4)
                    flipVisualizationViewAction();
                else if(frontCrossSectionView.match(t) && isWorkingMode && plotEditActionMode != 4)
                    frontCutViewAction();
                else if(rearCrossSectionView.match(t) && isWorkingMode && plotEditActionMode != 4)
                    rearCutViewAction();
                else if(gridView.match(t) && isWorkingMode && plotEditActionMode != 4)
                    gridViewAction();
                else if(graphView.match(t) && isWorkingMode && plotEditActionMode != 4)
                    graphViewAction();
                else if(tilledView.match(t) && isWorkingMode && plotEditActionMode != 4)
                    tilledViewAction();
                else if(yarnSimulationView.match(t) && isWorkingMode && plotEditActionMode != 4)
                    plotYarnSimulationView();
                //else if(simulationView.match(t) && isWorkingMode && plotEditActionMode != 4)
                //    simulationViewAction();
                else if(mappingView.match(t) && isWorkingMode && plotEditActionMode != 4)
                    mappingViewAction();
                else if(tilledView.match(t) || zoomInAlternateView.match(t))
                    zoomInAction();
                else if(zoomInView.match(t))
                    zoomInAction();
                else if(normalView.match(t))
                    normalAction();
                else if(zoomOutView.match(t) || zoomOutAlternateView.match(t))
                    zoomOutAction();
                else if(undoEdit.match(t) && isWorkingMode && plotEditActionMode != 4)
                    undoAction();
                else if(redoEdit.match(t) && isWorkingMode && plotEditActionMode != 4)
                    redoAction();
                else if(jacquardConversionEdit.match(t) && isWorkingMode && plotEditActionMode != 4)
                    jacquardConverstionAction();
                else if(graphCorrectionEdit.match(t) && isWorkingMode && plotEditActionMode != 4)
                    graphCorrectionAction();
                else if(densityEdit.match(t) && isWorkingMode && plotEditActionMode != 4)
                    densityAction();
                else if(yarnEdit.match(t) && isWorkingMode && plotEditActionMode != 4)
                    yarnPropertiesAction();
                else if(threadPatternEdit.match(t) && isWorkingMode && plotEditActionMode != 4)
                    threadSequenceAction();
                else if(switchColorEdit.match(t) && isWorkingMode && plotEditActionMode != 4)
                    switchColorAction();
                else if(repeatEdit.match(t) && isWorkingMode && plotEditActionMode != 4)
                    repeatOrientationAction();
                else if(transformOperationEdit.match(t) && isWorkingMode && plotEditActionMode != 4)
                    transformOperationAction();
                else if(vMirrorTransform.match(t) && isWorkingMode)
                    mirrorVerticalAction();
                else if(hMirrorTransform.match(t) && isWorkingMode)
                    mirrorHorizontalAction();
                else if(clockwiseRotateTransform.match(t) && isWorkingMode)
                    rotateClockwiseAction();
                else if(antiClockwiseRotateTransform.match(t) && isWorkingMode)
                    rotateAntiClockwiseAction();
                else if(moveRightTransform.match(t) && isWorkingMode)
                    moveRightAction();
                else if(moveLeftTransform.match(t) && isWorkingMode)
                    moveLeftAction();
                else if(moveUpTransform.match(t) && isWorkingMode)
                    moveUpAction();
                else if(moveDownTransform.match(t) && isWorkingMode)
                    moveDownAction();
                else if(move8RightTransform.match(t) && isWorkingMode)
                    moveRight8Action();
                else if(move8LeftTransform.match(t) && isWorkingMode)
                    moveLeft8Action();
                else if(move8UpTransform.match(t) && isWorkingMode)
                    moveUp8Action();
                else if(move8DownTransform.match(t) && isWorkingMode)
                    moveDown8Action();
                else if(tiltRightTransform.match(t) && isWorkingMode)
                    tiltRightAction();
                else if(tiltLeftTransform.match(t) && isWorkingMode)
                    tiltLeftAction();
                else if(tiltUpTransform.match(t) && isWorkingMode)
                    tiltUpAction();
                else if(tiltDownTransform.match(t) && isWorkingMode)
                    tiltDownAction();
                else if(invertTransform.match(t) && isWorkingMode)
                    inversionAction();
                else if(closeTransform.match(t) && isWorkingMode)
                    closeAction();
                else if(newFile.match(t) && plotEditActionMode != 4)
                    createMenuAction();
                else if(openFile.match(t) && plotEditActionMode != 4)
                    openMenuAction();
                else if(loadFile.match(t) && plotEditActionMode != 4)
                    loadRecentMenuAction();
                else if(saveFile.match(t) && isWorkingMode && !isNew && plotEditActionMode != 4)
                    saveMenuAction();
                else if(saveAsFile.match(t) && isWorkingMode && plotEditActionMode != 4)
                    saveAsMenuAction();
                else if(saveTextureFile.match(t) && isWorkingMode && plotEditActionMode != 4)
                    exportTextureAction();
                else if(saveXMLFile.match(t) && isWorkingMode && plotEditActionMode != 4)
                    exportHtmlAction();
                else if(saveTxtFile.match(t) && isWorkingMode && plotEditActionMode != 4)
                    exportTxtAction();
                else if(saveGridFile.match(t) && isWorkingMode && plotEditActionMode != 4)
                    exportGridAction();
                else if(saveGraphFile.match(t) && isWorkingMode && plotEditActionMode != 4)
                    exportGraphAction();
                else if(printFile.match(t) && isWorkingMode && plotEditActionMode != 4)
                    printMenuAction();
            }
        });
        // disabled as Up, Down Keys conflicting with Transform Operations
        /*scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                KeyCode key = t.getCode();
                if (key == KeyCode.DOWN){
                     zoomOutAction();
                } else if (key == KeyCode.UP){
                     zoomInAction();
                } else if (key == KeyCode.ENTER){
                     normalAction();
                }
            }
        });*/
    }
    /**
     * addFabricEventHandler
     * <p>
     * Function use for drawing menu bar for menu item,
     * and binding events for each menus with style. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void addFabricEventHandler(){
        fabric.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                addFabricMouseEvent(event);
                /*
                System.out.println("In clicked event"+ "," + objConfiguration.getIntBoxSize() + "," +zoomfactor);
                System.out.println(event.getX() + "," + event.getY());
                System.out.println(event.getSceneX() + "," + event.getSceneY());
                System.out.println(event.getScreenX() + "," + event.getScreenY());
                System.out.println(container.getBoundsInLocal());
                System.out.println(fabric.getBoundsInParent());
                System.out.println(fabric.getBoundsInLocal());
                */
            }
        });
        // drag code
        fabric.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                addFabricMouseEvent(event);
            }
        });
        fabric.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                addFabricMouseEvent(event);
            }
        });
        fabric.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                addFabricMouseEvent(event);
            }
        });
    }
    private void addFabricMouseEvent(MouseEvent event){
        if(plotEditActionMode==3){ //isEditGraph ==3
            fabricGraphEditEvent(event);
        }
    }
    /**
     * homeMenuAction
     * <p>
     * Function use for exit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void homeMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHOME"));
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.setIconified(false);
        dialogStage.setFullScreen(false);
        dialogStage.setTitle(objDictionaryAction.getWord("ALERT"));
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 300, 100, Color.WHITE);
        scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        
        popup.add(new ImageView("/media/alert_warning.png"), 0, 0);   
        Label lblAlert = new Label(objDictionaryAction.getWord("ALERTCLOSE"));
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(250);
        popup.add(lblAlert, 1, 0);
        Button btnYes = new Button(objDictionaryAction.getWord("YES"));
        btnYes.setPrefWidth(50);
        btnYes.setId("btnYes");
        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if(objFabric.getStrFabricID()!=null && !isNew)
                    objConfiguration.setStrRecentFabric(objFabric.getStrFabricID());
                else{
                    //objConfiguration.setoStrRecentFabric(objFabric.getStrFabricID());
                }
                dialogStage.close();
                fabricStage.close();
                System.gc();
                WindowView objWindowView = new WindowView(objConfiguration);
            }
        });
        popup.add(btnYes, 0, 1);
        Button btnNo = new Button(objDictionaryAction.getWord("NO"));
        btnNo.setPrefWidth(50);
        btnNo.setId("btnNo");
        btnNo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
            }
        });
        popup.add(btnNo, 1, 1);
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.showAndWait();
    }
    /**
     * fileMenuAction
     * <p>
     * Function use for file menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void fileMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONFILE"));
        selectedMenu = "FILE";
        menuHighlight();
    }
    /**
     * editMenuAction
     * <p>
     * Function use for edit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void editMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONEDIT"));
        selectedMenu = "EDIT";
        menuHighlight();
    }
    /**
     * viewMenuAction
     * <p>
     * Function use for view menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void viewMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONVIEW"));
        selectedMenu = "VIEW";
        menuHighlight();
    }
    /**
     * utilityMenuAction
     * <p>
     * Function use for utility menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void utilityMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("TOOLTIPUTILITY"));
        selectedMenu = "UTILITY";
        menuHighlight();
    }
    /**
     * helpMenuAction
     * <p>
     * Function use for help menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void helpMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHELP"));
        HelpView objHelpView = new HelpView(objConfiguration);
    }
    /**
     * technicalMenuAction
     * <p>
     * Function use for technical menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void technicalMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONTECHNICAL"));
        TechnicalView objTechnicalView = new TechnicalView(objConfiguration);
    }
    /**
     * aboutMenuAction
     * <p>
     * Function use for about menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void aboutMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONABOUTUS"));
        AboutView objAboutView = new AboutView(objConfiguration);
    }
    /**
     * contactMenuAction
     * <p>
     * Function use for contact menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void contactMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCONTACTUS"));
        ContactView objContactView = new ContactView(objConfiguration);
    }
    /**
     * logoutMenuAction
     * <p>
     * Function use for exit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void logoutMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHOME"));
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.setIconified(false);
        dialogStage.setFullScreen(false);
        dialogStage.setTitle(objDictionaryAction.getWord("ALERT"));
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 300, 100, Color.WHITE);
        scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        
        popup.add(new ImageView("/media/alert_warning.png"), 0, 0);   
        Label lblAlert = new Label(objDictionaryAction.getWord("ALERTCLOSE"));
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(250);
        popup.add(lblAlert, 1, 0);
        Button btnYes = new Button(objDictionaryAction.getWord("YES"));
        btnYes.setPrefWidth(50);
        btnYes.setId("btnYes");
        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                objConfiguration.setStrRecentFabric(null);
                dialogStage.close();
                fabricStage.close();
                System.gc();
                UserLoginView objUserLoginView = new UserLoginView(objConfiguration);
            }
        });
        popup.add(btnYes, 0, 1);
        Button btnNo = new Button(objDictionaryAction.getWord("NO"));
        btnNo.setPrefWidth(50);
        btnNo.setId("btnNo");
        btnNo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
            }
        });
        popup.add(btnNo, 1, 1);
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.showAndWait();
    }
    /**
     * exitMenuAction
     * <p>
     * Function use for exit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void exitMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONEXIT"));
        System.gc();
        fabricStage.close();
    }
    private void runMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONREALTIMEVIEW"));
        if(isWorkingMode){
            try {
                final Stage dialogStage = new Stage();
                dialogStage.initOwner(fabricStage);
                dialogStage.initStyle(StageStyle.UTILITY);
                //dialogStage.initModality(Modality.WINDOW_MODAL);
                
                GridPane popup=new GridPane();
                popup.setId("popup");
                popup.setAlignment(Pos.CENTER);
                popup.setHgap(5);
                popup.setVgap(5);
                popup.setPadding(new Insets(25, 25, 25, 25));

                int intHeight = objFabric.getIntWeft();
                int intLength = objFabric.getIntWarp();                
                BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
                BufferedImage bufferedImageResize = new BufferedImage(111, 111, BufferedImage.TYPE_INT_RGB);
                Graphics2D g = bufferedImageResize.createGraphics();
                objFabricAction = new FabricAction();
                
                // Front Texture View item;
                bufferedImage = objFabricAction.plotFrontSideView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), intLength, intHeight);
                g.drawImage(bufferedImage, 0, 0, 111, 111, null);
                g.dispose();
                //ImageIO.write(bufferedImageesize, "png", new File(System.getProperty("user.dir")+"/mla/tempmenu_composite.png"));
                VBox frontSideViewVB = new VBox(); 
                frontSideViewVB.getChildren().add(new ImageView(SwingFXUtils.toFXImage(bufferedImageResize, null)));
                frontSideViewVB.getChildren().add(new Label("Ctrl+F1"));
                frontSideViewVB.setPrefWidth(111);
                frontSideViewVB.getStyleClass().addAll("myBox");
                Tooltip frontSideViewTT = new Tooltip(objDictionaryAction.getWord("FRONTSIDEVIEW")+" (Ctrl+F1)\n"+objDictionaryAction.getWord("TOOLTIPFRONTSIDEVIEW"));
                frontSideViewTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_view.png"));
                Tooltip.install(frontSideViewVB, frontSideViewTT);
                frontSideViewVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        frontSideViewAction();
                    }
                });
                popup.add(frontSideViewVB, 0, 0);
                
                // Rear Texture View item
                bufferedImage = objFabricAction.plotRearSideView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), intLength, intHeight);
                g = bufferedImageResize.createGraphics();
                g.drawImage(bufferedImage, 0, 0, 111, 111, null);
                g.dispose();
                //ImageIO.write(bufferedImageesize, "png", new File(System.getProperty("user.dir")+"/mla/tempmenu_composite.png"));
                VBox rearSideViewVB = new VBox(); 
                rearSideViewVB.getChildren().add(new ImageView(SwingFXUtils.toFXImage(bufferedImageResize, null)));
                rearSideViewVB.getChildren().add(new Label("Ctrl+F2"));
                rearSideViewVB.setPrefWidth(111);
                rearSideViewVB.getStyleClass().addAll("myBox");        
                Tooltip rearSideViewTT = new Tooltip(objDictionaryAction.getWord("REARSIDEVIEW")+" (Ctrl+F2)\n"+objDictionaryAction.getWord("TOOLTIPREARSIDEVIEW"));
                rearSideViewTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_view.png"));
                Tooltip.install(rearSideViewVB, rearSideViewTT);
                rearSideViewVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        rearSideViewAction();
                    }
                });
                popup.add(rearSideViewVB, 1, 0);
        
                // Front Visulization View item;
                bufferedImage = objFabricAction.plotVisualizationFrontView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), intLength, intHeight);
                g = bufferedImageResize.createGraphics();
                g.drawImage(bufferedImage, 0, 0, 111, 111, null);
                g.dispose();
                //ImageIO.write(bufferedImageesize, "png", new File(System.getProperty("user.dir")+"/mla/tempmenu_composite.png"));
                VBox frontVisualizationViewVB = new VBox(); 
                frontVisualizationViewVB.getChildren().add(new ImageView(SwingFXUtils.toFXImage(bufferedImageResize, null)));
                frontVisualizationViewVB.getChildren().add(new Label("Ctrl+F4"));
                frontVisualizationViewVB.setPrefWidth(111);
                frontVisualizationViewVB.getStyleClass().addAll("myBox");        
                Tooltip frontVisualizationViewTT = new Tooltip(objDictionaryAction.getWord("VISUALIZATIONFRONTVIEW")+" (Ctrl+F4)\n"+objDictionaryAction.getWord("TOOLTIPVISUALIZATIONFRONTVIEW"));
                frontVisualizationViewTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_visualization.png"));
                Tooltip.install(frontVisualizationViewVB, frontVisualizationViewTT);
                frontVisualizationViewVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        frontVisualizationViewAction();
                    }
                });
                popup.add(frontVisualizationViewVB, 2, 0);
                
                // Rear Visaulization View item;
                bufferedImage = objFabricAction.plotVisualizationRearView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), intLength, intHeight);
                g = bufferedImageResize.createGraphics();
                g.drawImage(bufferedImage, 0, 0, 111, 111, null);
                g.dispose();
                //ImageIO.write(bufferedImageesize, "png", new File(System.getProperty("user.dir")+"/mla/tempmenu_composite.png"));
                VBox rearVisualizationViewVB = new VBox(); 
                rearVisualizationViewVB.getChildren().add(new ImageView(SwingFXUtils.toFXImage(bufferedImageResize, null)));
                rearVisualizationViewVB.getChildren().add(new Label("Ctrl+F5"));
                rearVisualizationViewVB.setPrefWidth(111);
                rearVisualizationViewVB.getStyleClass().addAll("myBox");        
                Tooltip rearVisualizationViewTT = new Tooltip(objDictionaryAction.getWord("VISUALIZATIONREARVIEW")+" (Ctrl+F5)\n"+objDictionaryAction.getWord("TOOLTIPVISUALIZATIONREARVIEW"));
                rearVisualizationViewTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_visualization.png"));
                Tooltip.install(rearVisualizationViewVB, rearVisualizationViewTT);
                rearVisualizationViewVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        rearVisualizationViewAction();
                    }
                });
                popup.add(rearVisualizationViewVB, 3, 0);
                
                // Switch Side View item;
                bufferedImage = objFabricAction.plotVisualizationFlipView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), intLength, intHeight);
                g = bufferedImageResize.createGraphics();
                g.drawImage(bufferedImage, 0, 0, 111, 111, null);
                g.dispose();
                //ImageIO.write(bufferedImageesize, "png", new File(System.getProperty("user.dir")+"/mla/tempmenu_composite.png"));
                VBox flipVisualizationViewVB = new VBox(); 
                flipVisualizationViewVB.getChildren().add(new ImageView(SwingFXUtils.toFXImage(bufferedImageResize, null)));
                flipVisualizationViewVB.getChildren().add(new Label("Ctrl+F6"));
                flipVisualizationViewVB.setPrefWidth(111);
                flipVisualizationViewVB.getStyleClass().addAll("myBox");        
                Tooltip flipVisualizationViewTT = new Tooltip(objDictionaryAction.getWord("VISUALIZATIONFLIPVIEW")+" (Ctrl+F6)\n"+objDictionaryAction.getWord("TOOLTIPVISUALIZATIONFLIPVIEW"));
                flipVisualizationViewTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/flip_visualization.png"));
                Tooltip.install(flipVisualizationViewVB, flipVisualizationViewTT);
                flipVisualizationViewVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        flipVisualizationViewAction();
                    }
                });
                popup.add(flipVisualizationViewVB, 4, 0);
        
                // cross Section View item;
                intLength = objFabric.getIntWarp();
                intHeight = objFabric.getIntWeft();
                byte[][] newMatrix = objFabric.getFabricMatrix();
                List lstLines = new ArrayList();
                ArrayList<Byte> lstEntry = null;
                int lineCount = 0;
                for (int i = 0; i < intHeight; i++){
                    lstEntry = new ArrayList();
                    for (int j = 0; j < intLength; j++){
                        //add the first color on array
                        if(lstEntry.size()==0 && newMatrix[i][j]!=1)
                            lstEntry.add(newMatrix[i][j]);
                        //check for redudancy
                        else {                
                            if(!lstEntry.contains(newMatrix[i][j]) && newMatrix[i][j]!=1)
                                lstEntry.add(newMatrix[i][j]);
                        }
                    }
                    lstLines.add(lstEntry);
                    lineCount+=lstEntry.size();
                }
                byte[][] baseMatrix = objArtworkAction.repeatMatrix(objFabric.getBaseWeaveMatrix(),lineCount,intLength);
                byte[][] fontMatrix = new byte[lineCount][intLength];
                lineCount=0;
                byte init = 0;
                for (int i = 0 ; i < intHeight; i++){
                    lstEntry = (ArrayList)lstLines.get(i);
                    for(int k=0; k<lstEntry.size(); k++, lineCount++){
                        init = (byte)lstEntry.get(k);
                        for (int j = 0; j < intLength; j++){
                            if(init==0){
                                fontMatrix[lineCount][j] = baseMatrix[i][j];
                            }else{
                                if(newMatrix[i][j]==init){
                                    fontMatrix[lineCount][j] = init;
                                }else{
                                    fontMatrix[lineCount][j] = (byte)1;
                                }
                            }   
                        }
                    }
                }

                newMatrix = null;
                baseMatrix = null;
                intHeight = lineCount;
                bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
                bufferedImage = objFabricAction.plotCrossSectionView(objFabric, fontMatrix, intLength, intHeight);
                intHeight = 111; //(int)(intHeight*zoomfactor*3);
                intLength = 111; //(int)(intLength*zoomfactor*3);
                g = bufferedImageResize.createGraphics();
                g.drawImage(bufferedImage, 0, 0, intLength, intHeight, null);
                g.dispose();
                //ImageIO.write(bufferedImageesize, "png", new File(System.getProperty("user.dir")+"/mla/tempmenu_composite.png"));
                VBox frontCrossSectionViewVB = new VBox(); 
                frontCrossSectionViewVB.getChildren().add(new ImageView(SwingFXUtils.toFXImage(bufferedImageResize, null)));
                frontCrossSectionViewVB.getChildren().add(new Label("Ctrl+F7"));
                frontCrossSectionViewVB.setPrefWidth(111);
                frontCrossSectionViewVB.getStyleClass().addAll("myBox");        
                Tooltip frontCrossSectionViewTT = new Tooltip(objDictionaryAction.getWord("CROSSSECTIONFRONTVIEW")+" (Ctrl+F7)\n"+objDictionaryAction.getWord("TOOLTIPCROSSSECTIONFRONTVIEW"));
                frontCrossSectionViewTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_cut.png"));
                Tooltip.install(frontCrossSectionViewVB, frontCrossSectionViewTT);
                frontCrossSectionViewVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        frontCutViewAction();
                    }
                });
                popup.add(frontCrossSectionViewVB, 0, 1);
           
                // cross Section View item;
                intLength = objFabric.getIntWarp();
                intHeight = objFabric.getIntWeft();
                newMatrix = objFabric.getFabricMatrix();
                lstLines = new ArrayList();
                lstEntry = null;
                lineCount = 0;
                for (int i = 0; i < intHeight; i++){
                    lstEntry = new ArrayList();
                    for (int j = 0; j < intLength; j++){
                        //add the first color on array
                        if(lstEntry.size()==0 && newMatrix[i][j]!=1)
                            lstEntry.add(newMatrix[i][j]);
                        //check for redudancy
                        else {                
                            if(!(lstEntry.contains(newMatrix[i][j]))  && newMatrix[i][j]!=1)
                                lstEntry.add(newMatrix[i][j]);
                        }
                    }
                    lstLines.add(lstEntry);
                    lineCount+=lstEntry.size();
                }
                baseMatrix = objArtworkAction.repeatMatrix(objFabric.getBaseWeaveMatrix(),lineCount,intLength);
                baseMatrix=objArtworkAction.invertMatrix(baseMatrix);
                fontMatrix = new byte[lineCount][intLength];
                lineCount=0;
                init = 0;
                for (int i = 0 ; i < intHeight; i++){
                    lstEntry = (ArrayList)lstLines.get(i);
                    for(int k=0; k<lstEntry.size(); k++, lineCount++){
                        init = (byte)lstEntry.get(k);
                        for (int j = 0; j < intLength; j++){
                            if(init==0)
                                fontMatrix[lineCount][j] = baseMatrix[i][j];
                            else{
                                if(newMatrix[i][j]==init){
                                    fontMatrix[lineCount][j] = (byte)1;
                                }else{
                                    fontMatrix[lineCount][j] = init;
                                }
                            }   
                        }
                    }
                }
                intHeight = lineCount;
                bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
                objFabricAction = new FabricAction();
                bufferedImage = objFabricAction.plotCrossSectionView(objFabric, fontMatrix, intLength, intHeight);
                intHeight = 111; //(int)(intHeight*zoomfactor*3);
                intLength = 111; //(int)(intLength*zoomfactor*3);
                bufferedImageResize = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
                g = bufferedImageResize.createGraphics();
                g.drawImage(bufferedImage, 0, 0, intLength, intHeight, null);
                g.dispose();
                //ImageIO.write(bufferedImageesize, "png", new File(System.getProperty("user.dir")+"/mla/tempmenu_composite.png"));
                VBox rearCrossSectionViewVB = new VBox(); 
                rearCrossSectionViewVB.getChildren().add(new ImageView(SwingFXUtils.toFXImage(bufferedImageResize, null)));
                rearCrossSectionViewVB.getChildren().add(new Label("Ctrl+F8"));
                rearCrossSectionViewVB.setPrefWidth(111);
                rearCrossSectionViewVB.getStyleClass().addAll("myBox");        
                Tooltip rearCrossSectionViewTT = new Tooltip(objDictionaryAction.getWord("CROSSSECTIONREARVIEW")+" (Ctrl+F8)\n"+objDictionaryAction.getWord("TOOLTIPCROSSSECTIONREARVIEW"));
                rearCrossSectionViewTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_cut.png"));
                Tooltip.install(rearCrossSectionViewVB, rearCrossSectionViewTT);
                rearCrossSectionViewVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        rearCutViewAction();
                    }
                });
                popup.add(rearCrossSectionViewVB, 1, 1);
         
                // Grid View item
                intHeight=objFabric.getIntWeft();
                intLength=objFabric.getIntWarp();
                bufferedImage = new BufferedImage(objFabric.getIntWarp(), objFabric.getIntWeft(),BufferedImage.TYPE_INT_RGB);
                objFabricAction = new FabricAction();
                if(objFabric.getIntExtraWarp()>0 || objFabric.getIntExtraWeft()>0){
                    if(objConfiguration.getBlnPunchCard()){
                        bufferedImage = objFabricAction.plotGraphJaquardMachineView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                        intLength=objFabric.getIntWarp()*(objFabric.getIntExtraWarp()+objFabric.getIntExtraWeft()+2);
                    } else{
                        bufferedImage = objFabricAction.plotGraphJaquardView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                    }
                }else{
                    bufferedImage = objFabricAction.plotGraphDobbyView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                }
                //================================ For Outer to be square ===============================
                String[] data = {"10","10"};
                bufferedImageResize = new BufferedImage((int)(intLength*zoomfactor*Integer.parseInt(data[1])), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])),BufferedImage.TYPE_INT_RGB);
                g = bufferedImageResize.createGraphics();
                g.drawImage(bufferedImage, 0, 0, (int)(intLength*zoomfactor*Integer.parseInt(data[1])), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])), null);
                g.setColor(java.awt.Color.BLACK);
                BasicStroke bs = new BasicStroke(1);
                g.setStroke(bs);
                for(int i = 0; i < objFabric.getIntWeft(); i++) {
                    for(int j = 0; j < intLength; j++) {                        
                        g.drawLine((int)(j*zoomfactor*Integer.parseInt(data[1])), 0,  (int)(j*zoomfactor*Integer.parseInt(data[1])), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])));
                    }
                    g.drawLine(0, (int)(i*zoomfactor*Integer.parseInt(data[0])), (int)(intLength*zoomfactor)*Integer.parseInt(data[1]), (int)(i*zoomfactor*Integer.parseInt(data[0])));
                }
                    
                g.dispose();
                bufferedImage = bufferedImageResize;
                intLength = 111;//(int)(objFabric.getIntWarp()*zoomfactor*3);
                intHeight = 111;//(int)(objFabric.getIntWeft()*zoomfactor*3);
                bufferedImageResize = new BufferedImage(intLength, intHeight,BufferedImage.TYPE_INT_RGB);
                g = bufferedImageResize.createGraphics();
                g.drawImage(bufferedImage, 0, 0, 111, 111, null);
                g.dispose();
                //ImageIO.write(bufferedImageesize, "png", new File(System.getProperty("user.dir")+"/mla/tempmenu_composite.png"));
                VBox gridViewVB = new VBox(); 
                gridViewVB.getChildren().add(new ImageView(SwingFXUtils.toFXImage(bufferedImageResize, null)));
                gridViewVB.getChildren().add(new Label("Ctrl+F10"));
                gridViewVB.setPrefWidth(111);
                gridViewVB.getStyleClass().addAll("myBox");        
                Tooltip gridViewTT = new Tooltip(objDictionaryAction.getWord("GRIDVIEW")+" (Ctrl+F10)\n"+objDictionaryAction.getWord("TOOLTIPGRIDVIEW"));
                gridViewTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/grid_view.png"));
                Tooltip.install(gridViewVB, gridViewTT);
                gridViewVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        gridViewAction();
                    }
                });
                popup.add(gridViewVB, 2, 1);
        
                // Graph View item
                intHeight=objFabric.getIntWeft();
                intLength=objFabric.getIntWarp();
                bufferedImage = new BufferedImage(objFabric.getIntWarp(), objFabric.getIntWeft(),BufferedImage.TYPE_INT_RGB);
                objFabricAction = new FabricAction();
                if(objFabric.getIntExtraWarp()>0 || objFabric.getIntExtraWeft()>0){
                    if(objConfiguration.getBlnPunchCard()){
                        bufferedImage = objFabricAction.plotGraphJaquardMachineView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                        intLength=objFabric.getIntWarp()*(objFabric.getIntExtraWarp()+objFabric.getIntExtraWeft()+2);
                    } else{
                        bufferedImage = objFabricAction.plotGraphJaquardView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                    }
                }else{
                    bufferedImage = objFabricAction.plotGraphDobbyView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                }
                //================================ For Outer to be square ===============================
                data = new String[]{"12","16"};
                bufferedImageResize = new BufferedImage((int)(intLength*zoomfactor*Integer.parseInt(data[1])), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])),BufferedImage.TYPE_INT_RGB);
                g = bufferedImageResize.createGraphics();
                g.drawImage(bufferedImage, 0, 0, (int)(intLength*zoomfactor*Integer.parseInt(data[1])), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])), null);
                g.setColor(java.awt.Color.BLACK);
                bs = new BasicStroke(2);
                g.setStroke(bs);
                for(int i = 0; i < objFabric.getIntWeft(); i++) {
                    for(int j = 0; j < intLength; j++) {
                        if((j%Integer.parseInt(data[0]))==0){
                            bs = new BasicStroke(2);
                            g.setStroke(bs);
                        }else{
                            bs = new BasicStroke(1);
                            g.setStroke(bs);
                        }
                        g.drawLine((int)(j*zoomfactor*Integer.parseInt(data[1])), 0,  (int)(j*zoomfactor*Integer.parseInt(data[1])), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])));
                    }
                    if((i%Integer.parseInt(data[1]))==0){
                        bs = new BasicStroke(2);
                        g.setStroke(bs);
                    }else{
                        bs = new BasicStroke(1);
                        g.setStroke(bs);
                    }
                    g.drawLine(0, (int)(i*zoomfactor*Integer.parseInt(data[0])), (int)(intLength*zoomfactor)*Integer.parseInt(data[1]), (int)(i*zoomfactor*Integer.parseInt(data[0])));
                }
                g.dispose();
                bufferedImage = bufferedImageResize;
                intLength = 111;//(int)(objFabric.getIntWarp()*zoomfactor*3);
                intHeight = 111;//(int)(objFabric.getIntWeft()*zoomfactor*3);
                bufferedImageResize = new BufferedImage(intLength, intHeight,BufferedImage.TYPE_INT_RGB);
                g = bufferedImageResize.createGraphics();
                g.drawImage(bufferedImage, 0, 0, 111, 111, null);
                g.dispose();
                //ImageIO.write(bufferedImageesize, "png", new File(System.getProperty("user.dir")+"/mla/tempmenu_composite.png"));
                VBox graphViewVB = new VBox(); 
                graphViewVB.getChildren().add(new ImageView(SwingFXUtils.toFXImage(bufferedImageResize, null)));
                graphViewVB.getChildren().add(new Label("Ctrl+F11"));
                graphViewVB.setPrefWidth(111);
                graphViewVB.getStyleClass().addAll("myBox");        
                Tooltip graphViewTT = new Tooltip(objDictionaryAction.getWord("GRAPHVIEW")+" (Ctrl+F11)\n"+objDictionaryAction.getWord("TOOLTIPGRAPHVIEW"));
                graphViewTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/graph_view.png"));
                Tooltip.install(graphViewVB, graphViewTT);
                graphViewVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        graphViewAction();
                    }
                });
                popup.add(graphViewVB, 3, 1);
        
                // Tiled View
                bufferedImage = objFabricAction.plotTilledView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                g = bufferedImageResize.createGraphics();
                g.drawImage(bufferedImage, 0, 0, 111, 111, null);
                g.dispose();
                //ImageIO.write(bufferedImageesize, "png", new File(System.getProperty("user.dir")+"/mla/tempmenu_composite.png"));
                VBox tilledViewVB = new VBox(); 
                tilledViewVB.getChildren().add(new ImageView(SwingFXUtils.toFXImage(bufferedImageResize, null)));
                tilledViewVB.getChildren().add(new Label("Ctrl+F12"));
                tilledViewVB.setPrefWidth(111);
                tilledViewVB.getStyleClass().addAll("myBox"); 
                Tooltip tilledViewTT = new Tooltip(objDictionaryAction.getWord("TILLEDVIEW")+" (Ctrl+F12)\n"+objDictionaryAction.getWord("TOOLTIPTILLEDVIEW"));
                tilledViewTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tiled_view.png"));
                Tooltip.install(tilledViewVB, tilledViewTT);                
                tilledViewVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        tilledViewAction();
                    }
                });
                popup.add(tilledViewVB, 4, 1);
                
                bufferedImageResize = null;
                System.gc();

                Button btnCancel = new Button(objDictionaryAction.getWord("CLOSE"));
                btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
                btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCLOSE")));
                btnCancel.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent e) {  
                        lblStatus.setText(objDictionaryAction.getWord("ACTIONCLOSE"));
                        dialogStage.close();
                    }
                });
                popup.add(btnCancel, 4, 2);

                Scene scene = new Scene(popup);
                scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
                dialogStage.setScene(scene);
                dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("VIEW"));
                dialogStage.showAndWait();
            } catch (SQLException ex) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            //} catch (IOException ex) {
              //  Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            }
        } else{
            lblStatus.setText(objDictionaryAction.getWord("NOFABRIC"));
        }
    }
    
    /**
     * transformMenuAction
     * <p>
     * Function use for weave menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void transformMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONTRANSFORMOPERATIONEDIT"));
        selectedMenu = "TRANSFORMOPERATION";
        transformMenu.show();
        transformMenu.setVisible(true);
        fileMenu.setDisable(true);
        editMenu.setDisable(true);
        viewMenu.setDisable(true);
        utilityMenu.setDisable(true);
        runMenu.setDisable(true);
        transformMenu.setDisable(false);
        menuHighlight(); 
    } 
/**
 * menuHighlight
 * <p>
 * Function use for drawing menu bar for menu item,
 * and binding events for each menus with style. 
 * 
 * @param       String strMenu
 * @author      Amit Kumar Singh
 * @version     %I%, %G%
 * @since       1.0
 * @see         javafx.event.*;
 * @link        FabricView
 */
    public void menuHighlight(){
        fileMenu.setStyle("-fx-background-color: linear-gradient(#000000,#5C594A);");
        editMenu.setStyle("-fx-background-color: linear-gradient(#000000,#5C594A);");
        viewMenu.setStyle("-fx-background-color: linear-gradient(#000000,#5C594A);");
        utilityMenu.setStyle("-fx-background-color: linear-gradient(#000000,#5C594A);");
        transformMenu.setStyle("-fx-background-color: linear-gradient(#000000,#5C594A);");
        fileMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-font-family: \"Segoe UI Semibold\"; -fx-text-fill: #FFFFFF;");
        editMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-font-family: \"Segoe UI Semibold\"; -fx-text-fill: #FFFFFF;");
        viewMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-font-family: \"Segoe UI Semibold\"; -fx-text-fill: #FFFFFF;");
        utilityMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-font-family: \"Segoe UI Semibold\"; -fx-text-fill: #FFFFFF;");
        transformMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-font-family: \"Segoe UI Semibold\"; -fx-text-fill: #FFFFFF;");
        toolBar.getItems().clear();
        menuSelected.getItems().clear();
        
        System.gc();
        if(selectedMenu == "TRANSFORMOPERATION"){
            transformMenu.setStyle("-fx-background-color: linear-gradient(#FFFFFF,#FFFEEE);");
            transformMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-text-fill: #000000;");
            populateTransformToolbar();
            populateTransformToolbarMenu();
        } else if(selectedMenu == "UTILITY"){
            utilityMenu.setStyle("-fx-background-color: linear-gradient(#FFFFFF,#FFFEEE);");
            utilityMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-text-fill: #000000;");
            populateUtilityToolbar();
            populateUtilityToolbarMenu();
        } else if(selectedMenu == "VIEW"){
            viewMenu.setStyle("-fx-background-color: linear-gradient(#FFFFFF,#FFFEEE);");
            viewMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-text-fill: #000000;");
            populateViewToolbar();
            populateViewToolbarMenu();
        } else if(selectedMenu == "EDIT"){
            editMenu.setStyle("-fx-background-color: linear-gradient(#FFFFFF,#FFFEEE);");
            editMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-text-fill: #000000;");
            populateEditToolbar();
            populateEditToolbarMenu();
        } else{
            fileMenu.setStyle("-fx-background-color: linear-gradient(#FFFFFF,#FFFEEE);");
            fileMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-text-fill: #000000;");
            populateFileToolbar();
            populateFileToolbarMenu();
        }
        System.gc();
    }      
 /**
 * populateFileToolbar
 * <p>
 * Function use for drawing tool bar for menu item File,
 * and binding events for each tools. 
 * 
 * @exception   (@throws SQLException)
 * @author      Amit Kumar Singh
 * @version     %I%, %G%
 * @since       1.0
 * @see         javafx.event.*;
 * @link        FabricView
 */
    private void populateFileToolbarMenu(){
        //For drop down menus
        menuSelected.setTooltip(new Tooltip(objDictionaryAction.getWord("FILE")));
        
        MenuItem newFileMI = new MenuItem(objDictionaryAction.getWord("NEWFILE"));
        MenuItem openFileMI = new MenuItem(objDictionaryAction.getWord("OPENFILE"));
        MenuItem loadFileMI = new MenuItem(objDictionaryAction.getWord("LOADFILE"));
        MenuItem saveFileMI = new MenuItem(objDictionaryAction.getWord("SAVEFILE"));
        MenuItem saveAsFileMI = new MenuItem(objDictionaryAction.getWord("SAVEASFILE"));
        MenuItem saveTextureFileMI = new MenuItem(objDictionaryAction.getWord("SAVETEXTUREFILE"));
        MenuItem saveXMLFileMI = new MenuItem(objDictionaryAction.getWord("SAVEXMLFILE"));
        MenuItem saveTxtFileMI = new MenuItem(objDictionaryAction.getWord("SAVETXTFILE"));
        MenuItem saveGridFileMI = new MenuItem(objDictionaryAction.getWord("SAVEGRIDFILE"));
        MenuItem saveGraphFileMI = new MenuItem(objDictionaryAction.getWord("SAVEGRAPHFILE"));
        MenuItem printFileMI = new MenuItem(objDictionaryAction.getWord("PRINTFILE"));
        
        newFileMI.setAccelerator(new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN));
        openFileMI.setAccelerator(new KeyCodeCombination(KeyCode.O, KeyCombination.CONTROL_DOWN));
        loadFileMI.setAccelerator(new KeyCodeCombination(KeyCode.L, KeyCombination.CONTROL_DOWN));
        saveFileMI.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN));
        saveAsFileMI.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        saveTextureFileMI.setAccelerator(new KeyCodeCombination(KeyCode.E, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        saveXMLFileMI.setAccelerator(new KeyCodeCombination(KeyCode.X, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        saveTxtFileMI.setAccelerator(new KeyCodeCombination(KeyCode.F, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        saveGridFileMI.setAccelerator(new KeyCodeCombination(KeyCode.H, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        saveGraphFileMI.setAccelerator(new KeyCodeCombination(KeyCode.G, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        printFileMI.setAccelerator(new KeyCodeCombination(KeyCode.P, KeyCombination.CONTROL_DOWN));
        
        newFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/new.png"));
        openFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/open.png"));
        loadFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/open_recent.png"));
        saveFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
        saveAsFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save_as.png"));
        saveTextureFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_texture_as.png"));
        saveXMLFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_html_as.png"));
        saveTxtFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_txt_as.png"));
        saveGridFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_grid_as.png"));
        saveGraphFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_graph_as.png"));
        printFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/print.png"));
             
        Menu exportMenu = new Menu(objDictionaryAction.getWord("EXPORT"));
        exportMenu.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export.png"));
        exportMenu.getItems().addAll(saveTextureFileMI, saveXMLFileMI, saveTxtFileMI, saveGridFileMI, saveGraphFileMI);
        
        //add enable disable condition
        if(!isWorkingMode){
            saveFileMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/save.png"));
            saveFileMI.setDisable(true);
            saveAsFileMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/save_as.png"));
            saveAsFileMI.setDisable(true);
            saveTextureFileMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/export_texture_as.png"));
            saveTextureFileMI.setDisable(true);
            saveXMLFileMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/export_html_as.png"));
            saveXMLFileMI.setDisable(true);
            saveTxtFileMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/export_txt_as.png"));
            saveTxtFileMI.setDisable(true);
            saveGridFileMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/export_grid_as.png"));
            saveGridFileMI.setDisable(true);
            saveGraphFileMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/export_graph_as.png"));
            saveGraphFileMI.setDisable(true);
            printFileMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/print.png"));
            printFileMI.setDisable(true);
        }else{
            saveFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
            saveFileMI.setDisable(false);
            saveAsFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save_as.png"));
            saveAsFileMI.setDisable(false);
            saveTextureFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_texture_as.png"));
            saveTextureFileMI.setDisable(false);
            saveXMLFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_html_as.png"));
            saveXMLFileMI.setDisable(false);
            saveTxtFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_txt_as.png"));
            saveTxtFileMI.setDisable(false);
            saveGridFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_grid_as.png"));
            saveGridFileMI.setDisable(false);
            saveGraphFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_graph_as.png"));
            saveGraphFileMI.setDisable(false);
            printFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/print.png"));
            printFileMI.setDisable(false);
        } 
        //Add the action to Buttons.
        newFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                createMenuAction();
            }
        });        
        openFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                openMenuAction();
            }
        });
        loadFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                loadRecentMenuAction();
            }
        });
        saveFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                saveMenuAction();
            }
        });
        saveAsFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                saveAsMenuAction();
            }
        });
        saveTextureFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                exportTextureAction();
            }
        });
        saveXMLFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                exportHtmlAction();
            }
        }); 
        saveTxtFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                exportTxtAction();
            }
        }); 
        saveGridFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                exportGridAction();
            }
        });
        saveGraphFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                exportGraphAction();
            }
        });
        printFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
               printMenuAction();
            }
        });
        
        menuSelected.getItems().addAll(newFileMI, openFileMI, loadFileMI, new SeparatorMenuItem(), saveFileMI, saveAsFileMI, new SeparatorMenuItem(), exportMenu, printFileMI);
    }
    private void populateFileToolbar(){
        // New file item
        Button newFileBtn = new Button();
        newFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/new.png"));
        newFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("NEWFILE")+" (Ctrl+N)\n"+objDictionaryAction.getWord("TOOLTIPNEWFILE")));
        newFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        newFileBtn.getStyleClass().add("toolbar-button");    
        // Open file item
        Button openFileBtn = new Button();
        openFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/open.png"));
        openFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("OPENFILE")+" (Ctrl+O)\n"+objDictionaryAction.getWord("TOOLTIPOPENFILE")));
        openFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        openFileBtn.getStyleClass().add("toolbar-button");
        // load recent file item
        Button loadFileBtn = new Button();        
        loadFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/open_recent.png"));
        loadFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("LOADFILE")+" (Ctrl+L)\n"+objDictionaryAction.getWord("TOOLTIPLOADFILE")));
        loadFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        loadFileBtn.getStyleClass().addAll("toolbar-button");
        // Save file item
        Button saveFileBtn = new Button();        
        saveFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
        saveFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SAVEFILE")+" (Ctrl+S)\n"+objDictionaryAction.getWord("TOOLTIPSAVEFILE")));
        saveFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        saveFileBtn.getStyleClass().addAll("toolbar-button");
        // Save As file item
        Button saveAsFileBtn = new Button();        
        saveAsFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save_as.png"));
        saveAsFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SAVEASFILE")+" (Ctrl+Shift+S)\n"+objDictionaryAction.getWord("TOOLTIPSAVEASFILE")));
        saveAsFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        saveAsFileBtn.getStyleClass().addAll("toolbar-button");
        // Save Texture file item
        Button saveTextureFileBtn = new Button();        
        saveTextureFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_texture_as.png"));
        saveTextureFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SAVETEXTUREFILE")+" (Ctrl+Shift+E)\n"+objDictionaryAction.getWord("TOOLTIPSAVETEXTUREFILE")));
        saveTextureFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        saveTextureFileBtn.getStyleClass().addAll("toolbar-button");
        // Save As file item
        Button saveXMLFileBtn = new Button();        
        saveXMLFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_html_as.png"));
        saveXMLFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SAVEXMLFILE")+" (Ctrl+Shift+X)\n"+objDictionaryAction.getWord("TOOLTIPSAVEXMLFILE")));
        saveXMLFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        saveXMLFileBtn.getStyleClass().addAll("toolbar-button");
        // Save As file item
        Button saveTxtFileBtn = new Button();        
        saveTxtFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_txt_as.png"));
        saveTxtFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SAVETXTFILE")+" (Ctrl+Shift+F)\n"+objDictionaryAction.getWord("TOOLTIPSAVETXTFILE")));
        saveTxtFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        saveTxtFileBtn.getStyleClass().addAll("toolbar-button");
        // Save grid file item
        Button saveGridFileBtn = new Button();        
        saveGridFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_grid_as.png"));
        saveGridFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SAVEGRIDFILE")+" (Ctrl+Shift+H)\n"+objDictionaryAction.getWord("TOOLTIPSAVEGRIDFILE")));
        saveGridFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        saveGridFileBtn.getStyleClass().addAll("toolbar-button");
        // Save graph file item
        Button saveGraphFileBtn = new Button();        
        saveGraphFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_graph_as.png"));
        saveGraphFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SAVEGRAPHFILE")+" (Ctrl+Shift+G)\n"+objDictionaryAction.getWord("TOOLTIPSAVEGRAPHFILE")));
        saveGraphFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        saveGraphFileBtn.getStyleClass().addAll("toolbar-button");
        // print File menu
        Button printFileBtn = new Button();        
        printFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/print.png"));
        printFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("PRINTFILE")+" (Ctrl+P)\n"+objDictionaryAction.getWord("TOOLTIPPRINTFILE")));
        printFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        printFileBtn.getStyleClass().addAll("toolbar-button");
        //add enable disable condition
        if(!isWorkingMode){
            saveFileBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/save.png"));
            saveFileBtn.setDisable(true);
            saveFileBtn.setCursor(Cursor.WAIT);
            saveAsFileBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/save_as.png"));
            saveAsFileBtn.setDisable(true);
            saveAsFileBtn.setCursor(Cursor.WAIT);
            saveTextureFileBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/export_texture_as.png"));
            saveTextureFileBtn.setDisable(true);
            saveTextureFileBtn.setCursor(Cursor.WAIT);            
            saveXMLFileBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/export_html_as.png"));
            saveXMLFileBtn.setDisable(true);
            saveXMLFileBtn.setCursor(Cursor.WAIT);
            saveTxtFileBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/export_txt_as.png"));
            saveTxtFileBtn.setDisable(true);
            saveTxtFileBtn.setCursor(Cursor.WAIT);
            saveGridFileBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/export_grid_as.png"));
            saveGridFileBtn.setDisable(true);
            saveGridFileBtn.setCursor(Cursor.WAIT);
            saveGraphFileBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/export_graph_as.png"));
            saveGraphFileBtn.setDisable(true);
            saveGraphFileBtn.setCursor(Cursor.WAIT);
            printFileBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/print.png"));
            printFileBtn.setDisable(true);
            printFileBtn.setCursor(Cursor.WAIT);
        }else{
            saveFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
            saveFileBtn.setDisable(false);
            saveFileBtn.setCursor(Cursor.HAND); 
            saveAsFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save_as.png"));
            saveAsFileBtn.setDisable(false);
            saveAsFileBtn.setCursor(Cursor.HAND); 
            saveTextureFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_texture_as.png"));
            saveTextureFileBtn.setDisable(false);
            saveTextureFileBtn.setCursor(Cursor.HAND); 
            saveXMLFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_html_as.png"));
            saveXMLFileBtn.setDisable(false);
            saveXMLFileBtn.setCursor(Cursor.HAND);             
            saveTxtFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_txt_as.png"));
            saveTxtFileBtn.setDisable(false);
            saveTxtFileBtn.setCursor(Cursor.HAND); 
            saveGridFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_grid_as.png"));
            saveGridFileBtn.setDisable(false);
            saveGridFileBtn.setCursor(Cursor.HAND);
            saveGraphFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_graph_as.png"));
            saveGraphFileBtn.setDisable(false);
            saveGraphFileBtn.setCursor(Cursor.HAND);
            printFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/print.png"));
            printFileBtn.setDisable(false);
            printFileBtn.setCursor(Cursor.HAND);
        } 
        //Add the action to Buttons.
        newFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                createMenuAction();
            }
        });        
        openFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                openMenuAction();
            }
        });
        loadFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadRecentMenuAction();
            }
        });
        saveFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                saveMenuAction();
            }
        });
        saveAsFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                saveAsMenuAction();
            }
        });
        saveTextureFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                exportTextureAction();
            }
        });
        saveXMLFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                exportHtmlAction();
            }
        }); 
        saveTxtFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                exportTxtAction();
            }
        });
        saveGridFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                exportGridAction();
            }
        });
        saveGraphFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                exportGraphAction();
            }
        });
        printFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
               printMenuAction();
            }
        });
        //Create some Buttons.        
        FlowPane flow = new FlowPane();  
        flow.setPrefWrapLength(objConfiguration.WIDTH*.95);
        flow.getStyleClass().addAll("flow");
        //Add the Buttons to the ToolBar.
        flow.getChildren().addAll(newFileBtn,openFileBtn,loadFileBtn,saveFileBtn,saveAsFileBtn,saveTextureFileBtn,saveXMLFileBtn,saveTxtFileBtn,saveGridFileBtn,saveGraphFileBtn,printFileBtn);
        flow.setAlignment(Pos.TOP_LEFT);
        toolBar.getItems().addAll(flow);
    }
    private void createMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONNEWFILE"));
        objWeave = new Weave();
        objWeave.setObjConfiguration(objConfiguration);
        WeaveImportView objWeaveImportView= new WeaveImportView(objWeave);
        if(objWeave.getStrWeaveID()!=null){
            objFabric.setStrBaseWeaveID(objWeave.getStrWeaveID());
            objWeave = null;
            newFabric();
        }else{
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            plotViewAction();
        }
    }
    private void openMenuAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONOPENFILE"));
            Fabric objFabricNew = new Fabric();
            objFabricNew.setObjConfiguration(objConfiguration);
            objFabricNew.setStrClothType(objConfiguration.getStrClothType());
            FabricImportView objFabricImportView= new FabricImportView(objFabricNew,"FabricEditor");
            if(objFabricNew.getStrFabricID()!=null){
                objFabric = objFabricNew;
                FabricAction objFabricAction = new FabricAction();
                objFabricAction.getFabric(objFabric, "Main");
                loadFabric();
            } else{
                lblStatus.setText(objDictionaryAction.getWord("NOITEM"));
                //plotViewAction();
            }
        } catch (SQLException ex) {
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
        } catch (Exception ex) {
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
        }
        System.gc();
    }
    private void loadRecentMenuAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONLOADFILE"));
            Fabric objFabricNew = new Fabric();
            objFabricNew.setObjConfiguration(objConfiguration);
            if(objConfiguration.getStrRecentFabric()!=null){
                objFabricNew.setStrFabricID(objConfiguration.getStrRecentFabric());
            }else{
                objFabric.setStrCondition("");
                objFabricNew.setStrSearchBy(objConfiguration.getStrClothType());  
                objFabric.setStrSearchAccess("All User Data");
                objFabric.setStrOrderBy("Date");
                objFabric.setStrDirection("Ascending");
                
                FabricAction objFabricAction = new FabricAction();
                List lstFabric = (ArrayList)objFabricAction.lstImportFabric(objFabricNew).get(0);
                if(lstFabric!=null && lstFabric.size()>0)
                    objFabricNew.setStrFabricID(lstFabric.get(0).toString());
            }
            if(objFabricNew.getStrFabricID()!=null && !isNew){
                FabricAction objFabricAction = new FabricAction();
                objFabricAction.getFabric(objFabricNew, "Main");
                if(objFabricNew.getStrFabricAccess().equalsIgnoreCase("Public")){
                    objConfiguration.setServicePasswordValid(true);
                }
                new MessageView(objConfiguration);
                if(objConfiguration.getServicePasswordValid()){
                    objFabric = objFabricNew;
                    objConfiguration.setServicePasswordValid(false);
                    loadFabric();
                }
                objFabricNew = null;
                System.gc();
            } else{
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                plotViewAction();
            }
        } catch (SQLException ex) {
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
        } catch (Exception ex) {
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
        }
        System.gc();
    }
    private void saveMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONSAVEFILE"));
        int fabricCount = 0;
        if(objFabric.getStrFabricID()!=null){
            try {
                objFabricAction = new FabricAction();
                fabricCount = objFabricAction.countFabricUsage(objFabric.getStrFabricID());
            } catch (SQLException ex) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }
        if(isNew || fabricCount > 0 || objFabric.getStrFabricAccess().equalsIgnoreCase("Public")){
            isNew = true;
        }
        saveUpdateFabric();
        System.gc();
    }
    private void saveAsMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONSAVEASFILE"));
        objFabric.setStrFabricAccess(objConfiguration.getObjUser().getUserAccess("FABRIC_LIBRARY"));
        isNew = true;
        saveUpdateFabric();
    }
    private void exportTextureAction(){
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
            lblStatus.setText(objDictionaryAction.getWord("ACTIONSAVETEXTUREFILE"));
            saveAsImage();
            lblStatus.setText(objDictionaryAction.getWord("DATASAVED"));
        }
    }
    private void exportHtmlAction(){
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
            lblStatus.setText(objDictionaryAction.getWord("ACTIONSAVEXMLFILE"));
            saveAsHtml();
            lblStatus.setText(objDictionaryAction.getWord("DATASAVED"));
        }
    }
    
    /**
     * Provision for selecting from two modes of Numeric Graph
     * 1. Numeric Graph Color
     * 2. Numeric Graph Background
     * @see FabricView.saveAsTxtGraph()
     * @since v0.9.6
     * @author Aatif Ahmad Khan
     */
    private void exportTxtAction(){
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
            lblStatus.setText(objDictionaryAction.getWord("ACTIONSAVETXTFILE"));
            String[] shuffleModeCaptions = {objDictionaryAction.getWord("NUMGRAPHCOLOR"), objDictionaryAction.getWord("NUMGRAPHBGR"), objDictionaryAction.getWord("INVERTDESIGN")+" "+objDictionaryAction.getWord("NUMGRAPHCOLOR"), objDictionaryAction.getWord("INVERTDESIGN")+" "+objDictionaryAction.getWord("NUMGRAPHBGR")};
            RadioOptionsView objRadioOptionsView = new RadioOptionsView(shuffleModeCaptions, shuffleModeCaptions);
            if(objRadioOptionsView.optionRBValue == 0) // numeric graph color
                saveAsTxtGraph(true, false);
            else if(objRadioOptionsView.optionRBValue == 1) // numeric graph background
                saveAsTxtGraph(false, false);
            else if(objRadioOptionsView.optionRBValue == 2) // invert design numeric graph color
                saveAsTxtGraph(true, true);
            else if(objRadioOptionsView.optionRBValue == 3) // invert design numeric graph background
                saveAsTxtGraph(false, true);
            lblStatus.setText(objDictionaryAction.getWord("DATASAVED"));
        }
    }
    private void exportGridAction(){
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
            lblStatus.setText(objDictionaryAction.getWord("ACTIONSAVEGRIDFILE"));
            saveAsGrid();
            lblStatus.setText(objDictionaryAction.getWord("DATASAVED"));
        }
    }   
    private void exportGraphAction(){
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
            lblStatus.setText(objDictionaryAction.getWord("ACTIONSAVEGRAPHFILE"));
            saveAsGraph();
            lblStatus.setText(objDictionaryAction.getWord("DATASAVED"));
        }
    }    
    private void printMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONPRINTFILE"));
        int intLength = objFabric.getIntWarp();
        int intHeight = objFabric.getIntWeft(); 
        BufferedImage printImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
        try {
            if(yarnSimulationMode){ // print simulated fabric as it appears
                printImage = SwingFXUtils.fromFXImage(fabric.snapshot(null, null), null);
            }else if(plotViewActionMode==1){
                BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
                objFabricAction = new FabricAction();
                bufferedImage = objFabricAction.plotFrontSideView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), objFabric.getIntWarp(), objFabric.getIntWeft());

                intLength = (int)(((72*objFabric.getIntWarp())/objFabric.getIntEPI())*zoomfactor);
                intHeight = (int)(((72*objFabric.getIntWeft())/objFabric.getIntPPI())*zoomfactor);
                printImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
                Graphics2D g = printImage.createGraphics();
                g.drawImage(bufferedImage, 0, 0, intLength, intHeight, null);
                g.dispose();
            }else if(plotViewActionMode==10){
                printImage = new BufferedImage(objFabric.getIntWarp(), objFabric.getIntWeft(),BufferedImage.TYPE_INT_RGB);
                objFabricAction = new FabricAction();
                if(objFabric.getIntExtraWarp()>0 || objFabric.getIntExtraWeft()>0){
                    if(objConfiguration.getBlnPunchCard()){
                        printImage = objFabricAction.plotGraphJaquardMachineView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                        intLength=objFabric.getIntWarp()*(objFabric.getIntExtraWarp()+objFabric.getIntExtraWeft()+2);
                    } else{
                        printImage = objFabricAction.plotGraphJaquardView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                    }
                } else{
                    printImage = objFabricAction.plotGraphDobbyView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                }
            }else if(plotViewActionMode==11){
                printImage = new BufferedImage(objFabric.getIntWarp(), objFabric.getIntWeft(),BufferedImage.TYPE_INT_RGB);
                objFabricAction = new FabricAction();
                if(objFabric.getIntExtraWarp()>0 || objFabric.getIntExtraWeft()>0){
                    if(objConfiguration.getBlnPunchCard()){
                        printImage = objFabricAction.plotGraphJaquardMachineView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                        intLength=objFabric.getIntWarp()*(objFabric.getIntExtraWarp()+objFabric.getIntExtraWeft()+2);
                    } else{
                        printImage = objFabricAction.plotGraphJaquardView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                    }
                } else{
                    printImage = objFabricAction.plotGraphDobbyView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                }
            }else{
                printImage = SwingFXUtils.fromFXImage(fabric.getImage(), null);
            }
            new PrintView(printImage, plotViewActionMode, objFabric.getStrFabricName(), objConfiguration.getObjUser().getStrName(), objConfiguration.getStrGraphSize());
            lblStatus.setText("");
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);  
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (OutOfMemoryError err){
            lblStatus.setText("Error while printing. Image to be printed is very large.");
        }
    }
/**
 * populateEditToolbar
 * <p>
 * Function use for drawing tool bar for menu item Edit,
 * and binding events for each tools. 
 * 
 * @exception   (@throws SQLException)
 * @author      Amit Kumar Singh
 * @version     %I%, %G%
 * @since       1.0
 * @see         javafx.event.*;
 * @link        FabricView
 */
    private void populateEditToolbarMenu(){
        //For drop down menus
        menuSelected.setTooltip(new Tooltip(objDictionaryAction.getWord("EDIT")));
        
        MenuItem undoEditMI = new MenuItem(objDictionaryAction.getWord("UNDO"));
        MenuItem redoEditMI = new MenuItem(objDictionaryAction.getWord("REDO"));
        MenuItem jacquardConversionEditMI = new MenuItem(objDictionaryAction.getWord("JACQUARDCONVERSIONEDIT"));
        MenuItem graphCorrectionEditMI = new MenuItem(objDictionaryAction.getWord("GRAPHCORRECTIONEDIT"));
        MenuItem densityEditMI = new MenuItem(objDictionaryAction.getWord("DENSITYEDIT"));
        MenuItem yarnEditMI = new MenuItem(objDictionaryAction.getWord("YARNEDIT"));
        MenuItem threadPatternEditMI = new MenuItem(objDictionaryAction.getWord("THREADPATTERNEDIT"));
        MenuItem switchColorEditMI = new MenuItem(objDictionaryAction.getWord("SWITCHCOLOREDIT"));
        MenuItem repeatEditMI = new MenuItem(objDictionaryAction.getWord("REPEATORIENTATION"));
        MenuItem transformOperationEditMI = new MenuItem(objDictionaryAction.getWord("TRANSFORMOPERATIONEDIT"));
        
        undoEditMI.setAccelerator(new KeyCodeCombination(KeyCode.Z, KeyCombination.CONTROL_DOWN));
        redoEditMI.setAccelerator(new KeyCodeCombination(KeyCode.Y, KeyCombination.CONTROL_DOWN));
        jacquardConversionEditMI.setAccelerator(new KeyCodeCombination(KeyCode.J, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        graphCorrectionEditMI.setAccelerator(new KeyCodeCombination(KeyCode.G, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        densityEditMI.setAccelerator(new KeyCodeCombination(KeyCode.D, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        yarnEditMI.setAccelerator(new KeyCodeCombination(KeyCode.Y, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        threadPatternEditMI.setAccelerator(new KeyCodeCombination(KeyCode.T, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        switchColorEditMI.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        repeatEditMI.setAccelerator(new KeyCodeCombination(KeyCode.R, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        transformOperationEditMI.setAccelerator(new KeyCodeCombination(KeyCode.W, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        
        undoEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/undo.png"));
        redoEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/redo.png"));
        jacquardConversionEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/assign_artwork_weave.png"));
        graphCorrectionEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/graph_editor.png"));
        densityEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/density.png"));
        yarnEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/yarn.png"));
        threadPatternEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/thread_pattern.png"));
        switchColorEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/switch_color.png"));
        repeatEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/repeat.png"));
        transformOperationEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/edit.png"));
        
        //add enable disbale condition
        if(!isWorkingMode){
            undoEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/undo.png"));
            undoEditMI.setDisable(true);
            redoEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/redo.png"));
            redoEditMI.setDisable(true);
            jacquardConversionEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/assign_artwork_weave.png"));
            jacquardConversionEditMI.setDisable(true);
            graphCorrectionEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/graph_editor.png"));
            graphCorrectionEditMI.setDisable(true);
            densityEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/density.png"));
            densityEditMI.setDisable(true);
            yarnEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/yarn.png"));
            yarnEditMI.setDisable(true);
            threadPatternEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/thread_pattern.png"));
            threadPatternEditMI.setDisable(true);
            switchColorEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/switch_color.png"));
            switchColorEditMI.setDisable(true);
            repeatEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/repeat.png"));
            repeatEditMI.setDisable(true);
            transformOperationEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/edit.png"));
            transformOperationEditMI.setDisable(true);            
        }else{
            undoEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/undo.png"));
            undoEditMI.setDisable(false);
            redoEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/redo.png"));
            redoEditMI.setDisable(false);
            jacquardConversionEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/assign_artwork_weave.png"));
            jacquardConversionEditMI.setDisable(false);
            graphCorrectionEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/graph_editor.png"));
            graphCorrectionEditMI.setDisable(false);
            densityEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/density.png"));
            densityEditMI.setDisable(false);
            yarnEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/yarn.png"));
            yarnEditMI.setDisable(false);
            threadPatternEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/thread_pattern.png"));
            threadPatternEditMI.setDisable(false);
            switchColorEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/switch_color.png"));
            switchColorEditMI.setDisable(false);
            repeatEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/repeat.png"));
            repeatEditMI.setDisable(false);
            transformOperationEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/edit.png"));
            transformOperationEditMI.setDisable(false);
        }
        //Add the action to Buttons.
        undoEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                undoAction();
            }
        });
        redoEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                redoAction();
            }
        });
        jacquardConversionEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                jacquardConverstionAction();
            }
        });
        graphCorrectionEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                graphCorrectionAction();
            }
        });
        densityEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {               
                densityAction();
            }
        });  
        yarnEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                yarnPropertiesAction();
            }
        }); 
        threadPatternEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {              
                threadSequenceAction();
            }
        });   
        switchColorEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {              
                switchColorAction();
            }
        });
        repeatEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                repeatOrientationAction();
            }
        });
        transformOperationEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                transformOperationAction();
            }
        });     
        menuSelected.getItems().addAll(undoEditMI, redoEditMI, new SeparatorMenuItem(), jacquardConversionEditMI, graphCorrectionEditMI, densityEditMI, yarnEditMI, threadPatternEditMI, switchColorEditMI, repeatEditMI, transformOperationEditMI);
    }
    private void populateEditToolbar(){    
        // Undo edit item
        Button undoEditBtn = new Button();
        undoEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/undo.png"));
        undoEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("UNDO")+" (Ctrl+Z)\n"+objDictionaryAction.getWord("TOOLTIPUNDO")));
        undoEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        undoEditBtn.getStyleClass().add("toolbar-button");    
        // Redo edit item 
        Button redoEditBtn = new Button();
        redoEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/redo.png"));
        redoEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("REDO")+" (Ctrl+Y)\n"+objDictionaryAction.getWord("TOOLTIPREDO")));
        redoEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        redoEditBtn.getStyleClass().add("toolbar-button");    
        // artwork edit item
        Button jacquardConversionEditBtn = new Button();
        jacquardConversionEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/assign_artwork_weave.png"));
        jacquardConversionEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("JACQUARDCONVERSIONEDIT")+" (Ctrl+Shift+J)\n"+objDictionaryAction.getWord("TOOLTIPJACQUARDCONVERSIONEDIT")));
        jacquardConversionEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        jacquardConversionEditBtn.getStyleClass().add("toolbar-button");    
        // graph edit item
        Button graphCorrectionEditBtn = new Button();
        graphCorrectionEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/graph_editor.png"));
        graphCorrectionEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("GRAPHCORRECTIONEDIT")+" (Alt+Shift+G)\n"+objDictionaryAction.getWord("TOOLTIPGRAPHCORRECTIONEDIT")));
        graphCorrectionEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        graphCorrectionEditBtn.getStyleClass().add("toolbar-button");    
        // density edit item
        Button densityEditBtn = new Button();
        densityEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/density.png"));
        densityEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("DENSITYEDIT")+" (Ctrl+Shift+D)\n"+objDictionaryAction.getWord("TOOLTIPDENSITYEDIT")));
        densityEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        densityEditBtn.getStyleClass().add("toolbar-button");    
        // yarn Calculation item
        Button yarnEditBtn = new Button();
        yarnEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/yarn.png"));
        yarnEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("YARNEDIT")+" (Ctrl+Shift+Y)\n"+objDictionaryAction.getWord("TOOLTIPYARNEDIT")));
        yarnEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        yarnEditBtn.getStyleClass().add("toolbar-button");    
        // Pattern edit item
        Button threadPatternEditBtn = new Button();
        threadPatternEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/thread_pattern.png"));
        threadPatternEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("THREADPATTERNEDIT")+" (Ctrl+Shift+T)\n"+objDictionaryAction.getWord("TOOLTIPTHREADPATTERNEDIT")));
        threadPatternEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        threadPatternEditBtn.getStyleClass().add("toolbar-button");    
        // switch color edit item
        Button switchColorEditBtn = new Button();
        switchColorEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/switch_color.png"));
        switchColorEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SWITCHCOLOREDIT")+" (Ctrl+Shift+C)\n"+objDictionaryAction.getWord("TOOLTIPSWITCHCOLOREDIT")));
        switchColorEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        switchColorEditBtn.getStyleClass().add("toolbar-button");    
        // repeat
        Button repeatEditBtn = new Button(); 
        repeatEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/repeat.png"));
        repeatEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("REPEATORIENTATION")+" (Alt+Shift+R)\n"+objDictionaryAction.getWord("TOOLTIPREPEATORIENTATION")));
        repeatEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        repeatEditBtn.getStyleClass().addAll("toolbar-button"); 
        // transform Operation edit item
        Button transformOperationEditBtn = new Button();
        transformOperationEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/edit.png"));
        transformOperationEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("TRANSFORMOPERATIONEDIT")+" (Ctrl+Shift+W)\n"+objDictionaryAction.getWord("TOOLTIPTRANSFORMOPERATIONEDIT")));
        transformOperationEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        transformOperationEditBtn.getStyleClass().add("toolbar-button");    
        //add enable disbale condition
        if(!isWorkingMode){
            undoEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/undo.png"));
            undoEditBtn.setDisable(true);
            undoEditBtn.setCursor(Cursor.WAIT);
            redoEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/redo.png"));
            redoEditBtn.setDisable(true);
            redoEditBtn.setCursor(Cursor.WAIT);
            jacquardConversionEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/assign_artwork_weave.png"));
            jacquardConversionEditBtn.setDisable(true);
            jacquardConversionEditBtn.setCursor(Cursor.WAIT);
            graphCorrectionEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/graph_editor.png"));
            graphCorrectionEditBtn.setDisable(true);
            graphCorrectionEditBtn.setCursor(Cursor.WAIT);
            densityEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/density.png"));
            densityEditBtn.setDisable(true);
            densityEditBtn.setCursor(Cursor.WAIT);            
            yarnEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/yarn.png"));
            yarnEditBtn.setDisable(true);
            yarnEditBtn.setCursor(Cursor.WAIT);
            threadPatternEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/thread_pattern.png"));
            threadPatternEditBtn.setDisable(true);
            threadPatternEditBtn.setCursor(Cursor.WAIT);
            switchColorEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/switch_color.png"));
            switchColorEditBtn.setDisable(true);
            switchColorEditBtn.setCursor(Cursor.WAIT);
            repeatEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/repeat.png"));
            repeatEditBtn.setDisable(true);
            repeatEditBtn.setCursor(Cursor.WAIT);
            transformOperationEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/edit.png"));
            transformOperationEditBtn.setDisable(true);
            transformOperationEditBtn.setCursor(Cursor.WAIT);            
        }else{
            undoEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/undo.png"));
            undoEditBtn.setDisable(false);
            undoEditBtn.setCursor(Cursor.HAND); 
            redoEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/redo.png"));
            redoEditBtn.setDisable(false);
            redoEditBtn.setCursor(Cursor.HAND);
            jacquardConversionEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/assign_artwork_weave.png"));
            jacquardConversionEditBtn.setDisable(false);
            jacquardConversionEditBtn.setCursor(Cursor.HAND);
            graphCorrectionEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/graph_editor.png"));
            graphCorrectionEditBtn.setDisable(false);
            graphCorrectionEditBtn.setCursor(Cursor.HAND);
            densityEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/density.png"));
            densityEditBtn.setDisable(false);
            densityEditBtn.setCursor(Cursor.HAND);            
            yarnEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/yarn.png"));
            yarnEditBtn.setDisable(false);
            yarnEditBtn.setCursor(Cursor.HAND);
            threadPatternEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/thread_pattern.png"));
            threadPatternEditBtn.setDisable(false);
            threadPatternEditBtn.setCursor(Cursor.HAND);
            switchColorEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/switch_color.png"));
            switchColorEditBtn.setDisable(false);
            switchColorEditBtn.setCursor(Cursor.HAND);
            repeatEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/repeat.png"));
            repeatEditBtn.setDisable(false);
            repeatEditBtn.setCursor(Cursor.HAND); 
            transformOperationEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/edit.png"));
            transformOperationEditBtn.setDisable(false);
            transformOperationEditBtn.setCursor(Cursor.HAND);
        }
        //Add the action to Buttons.
        undoEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                undoAction();
            }
        });
        redoEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                redoAction();
            }
        });
        jacquardConversionEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                jacquardConverstionAction();
            }
        });
        graphCorrectionEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                graphCorrectionAction();
            }
        });
        densityEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {                
                densityAction();
            }
        });  
        yarnEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                yarnPropertiesAction();
            }
        }); 
        threadPatternEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {                
                threadSequenceAction();
            }
        });   
        switchColorEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {                
                switchColorAction();
            }
        });
        repeatEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                repeatOrientationAction();
            }
        });
        transformOperationEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                transformOperationAction();
            }
        });
        //Create some Buttons.        
        FlowPane flow = new FlowPane();        
        flow.setPrefWrapLength(objConfiguration.WIDTH*.95);
        flow.getStyleClass().addAll("flow");
        //Add the Buttons to the ToolBar.
        flow.getChildren().addAll(undoEditBtn,redoEditBtn,jacquardConversionEditBtn,graphCorrectionEditBtn,densityEditBtn,yarnEditBtn,threadPatternEditBtn,switchColorEditBtn,repeatEditBtn,transformOperationEditBtn);
        flow.setAlignment(Pos.TOP_LEFT);
        toolBar.getItems().addAll(flow);
    }
//////////////////////////// Edit Menu Action ///////////////////////////
    /**
     * undoAction
     * <p>
     * This method is used for undo.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for creating GUI of undo.
     * @see         javafx.stage.*;
     * @link        com.mla.fabric.Fabric
     */
    private void undoAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONUNDO"));
        try {
            if(objUR.canUndo()){
                objUR.undo();
                objFabric = (Fabric)objUR.getObjData();
                Fabric clonedFabric = cloneFabric();
                objFabric = clonedFabric;
                objFabric.setObjConfiguration(objConfiguration);
                initFabricValue();
                editMenuAction(); // retain selection on edit menu
                //System.out.println("Undo Fabric Name: "+objConfiguration.WIDTH);
                lblStatus.setText(objUR.getStrTag()+" "+objDictionaryAction.getWord("UNDO")+":"+objDictionaryAction.getWord("SUCCESS"));
            }else{
                lblStatus.setText(objDictionaryAction.getWord("MAXUNDO"));
            }
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Operation Undo",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    /**
     * redoAction
     * <p>
     * This method is used for redo.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @see         javafx.stage.*;
     * @link        com.mla.fabric.Fabric
     */
    private void redoAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONREDO"));
        try {
            if(objUR.canRedo()){
                objUR.redo();
                objFabric = (Fabric)objUR.getObjData();
                Fabric clonedFabric = cloneFabric();
                objFabric = clonedFabric;
                objFabric.setObjConfiguration(objConfiguration);
                initFabricValue();
                editMenuAction(); // retain selection on edit menu
                //System.out.println("Redo Fabric Name: "+objConfiguration.WIDTH);
                lblStatus.setText(objUR.getStrTag()+" "+objDictionaryAction.getWord("REDO")+":"+objDictionaryAction.getWord("SUCCESS"));
            }else{
                lblStatus.setText(objDictionaryAction.getWord("MAXREDO"));
            }
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Operation Redo",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void jacquardConverstionAction(){
        plotViewActionMode = 1;
        plotViewAction();
        lblStatus.setText(objDictionaryAction.getWord("ACTIONJACQUARDCONVERSIONEDIT"));
        
        FabricEditView objFabricEditView= new FabricEditView(objFabric);
        if(objFabric.getStrArtworkID()==null||objFabric.getStrArtworkID().equalsIgnoreCase("null")){
            byte[][] baseWeaveMatrix = objFabric.getBaseWeaveMatrix();
            try {
                objArtworkAction= new ArtworkAction();
                objFabric.setFabricMatrix(objArtworkAction.repeatMatrix(baseWeaveMatrix,objFabric.getIntWeft(),objFabric.getIntWarp()));
                objArtworkAction= new ArtworkAction();
                objFabric.setReverseMatrix(objArtworkAction.invertMatrixNonObject(objFabric.getFabricMatrix()));
            } catch (SQLException ex) {
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
            warpExtraYarn = null;
            objFabric.setWarpExtraYarn(warpExtraYarn);
            objFabric.setIntExtraWarp(objConfiguration.getIntExtraWarp());
            weftExtraYarn = null;
            objFabric.setWeftExtraYarn(weftExtraYarn);
            objFabric.setIntExtraWeft(objConfiguration.getIntExtraWeft());
        }else{
            objFabric.setIntWeft(objFabric.getArtworkMatrix().length);        
            objFabric.setIntWarp(objFabric.getArtworkMatrix()[0].length);
            objFabric.setIntHooks((int) Math.ceil(objFabric.getIntWarp()/(double)objFabric.getIntTPD()));
            objFabric.setFabricMatrix(objFabric.getArtworkMatrix());
            objConfiguration.setStrRecentArtwork(objFabric.getObjConfiguration().getStrRecentArtwork());
        }
        
        Fabric objFabricCopy = cloneFabric();
        objFabricCopy.setStrFabricName("EditArtwork");
        objUR.doCommand("Edit Artwork", objFabricCopy);
        
        plotViewActionMode = 1;
        plotViewAction();
    }
    /**
     * graphCorrectionAction
     * <p>
     * This method is used for creating GUI of fabric reduce color pane.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @see         javafx.stage.*;
     * @link        com.mla.fabric.Fabric
     */
    public void graphCorrectionAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONGRAPHCORRECTIONEDIT"));
        try {
            lblStatus.setText(objDictionaryAction.getWord("GETGRAPHVIEW"));
            objFabricAction = new FabricAction();
            isEditingMode = true;
            plotEditActionMode = 3; //editgraph = 3
            plotEditGraph();
            populateEditGraphPane();
            lblStatus.setText(objDictionaryAction.getWord("GOTGRAPHVIEW"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"graphCorrectionAction(): Error while editing garph."+ex.getMessage(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
        Fabric objFabricCopy = cloneFabric();
        objFabricCopy.setStrFabricName("EditGraph");
        objUR.doCommand("Edit Graph", objFabricCopy);
    }
    /**
     * plotEditGraph
     * <p>
     * This method is used for creating GUI of graph edit in fabric
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @see         javafx.stage.*;
     * @link        com.mla.fabric.Fabric
     */
    private void plotEditGraph(){
        try {
            /////////////// for error correction  //////////////////
            if(objConfiguration.getBlnCorrectGraph()){
                objFabricAction.plotGraphErrorCorrection(objFabric,objConfiguration.getIntFloatBind());
                //objConfiguration.setBlnCorrectGraph(false);
            } 
            int intHeight=objFabric.getIntWeft();
            int intLength=objFabric.getIntWarp();
            BufferedImage bufferedImage = new BufferedImage(objFabric.getIntWarp(), objFabric.getIntWeft(),BufferedImage.TYPE_INT_RGB);
            if(objFabric.getIntExtraWarp()>0 || objFabric.getIntExtraWeft()>0){
                bufferedImage = objFabricAction.plotGraphJaquardView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
            }else{
                bufferedImage = objFabricAction.plotGraphDobbyView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
            }
   
            //================================ For Outer to be square ===============================
            String[] data = objFabric.getObjConfiguration().getStrGraphSize().split("x");
            BufferedImage bufferedImageesize = new BufferedImage((int)(intLength*zoomfactor*Integer.parseInt(data[1])/graphFactor), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])/graphFactor),BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bufferedImageesize.createGraphics();
            g.drawImage(bufferedImage, 0, 0, (int)(intLength*zoomfactor*Integer.parseInt(data[1])/graphFactor), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])/graphFactor), null);
            g.setColor(java.awt.Color.BLACK);
            BasicStroke bs = new BasicStroke(2);
            g.setStroke(bs);

            for(int i = 0; i < objFabric.getIntWeft(); i++) {
                for(int j = 0; j < intLength; j++) {
                    if((j%Integer.parseInt(data[0]))==0){
                        bs = new BasicStroke(2);
                        g.setStroke(bs);
                    }else{
                        bs = new BasicStroke(1);
                        g.setStroke(bs);
                    }
                    g.drawLine((int)(j*zoomfactor*Integer.parseInt(data[1])/graphFactor), 0,  (int)(j*zoomfactor*Integer.parseInt(data[1])/graphFactor), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])/graphFactor));
                }
                if((i%Integer.parseInt(data[1]))==0){
                    bs = new BasicStroke(2);
                    g.setStroke(bs);
                }else{
                    bs = new BasicStroke(1);
                    g.setStroke(bs);
                }
                g.drawLine(0, (int)(i*zoomfactor*Integer.parseInt(data[0])/graphFactor), (int)(intLength*zoomfactor)*Integer.parseInt(data[1])/graphFactor, (int)(i*zoomfactor*Integer.parseInt(data[0])/graphFactor));
            }
            g.drawLine(0, (int)((objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])/graphFactor)-1), (int)(intLength*zoomfactor)*Integer.parseInt(data[1])/graphFactor, (int)((objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])/graphFactor)-1));
            g.drawLine((int)((intLength*zoomfactor*Integer.parseInt(data[1])/graphFactor)-1), 0,  (int)((intLength*zoomfactor*Integer.parseInt(data[1])/graphFactor)-1), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])/graphFactor));
            
            /////////////// for error box  //////////////////
            if(objConfiguration.getBlnErrorGraph()){
                BufferedImage errorImage = ImageIO.read(getClass().getResource("/media/error.png"));
                byte[][] errorMatrix;
                errorMatrix = objFabricAction.plotGraphErrorMatrix(objFabric,objConfiguration.getIntFloatBind());
                for(int x = 0; x < objFabric.getIntWeft(); x++) {
                    for(int y = 0; y < objFabric.getIntWarp(); y++) {
                        if(errorMatrix[x][y]==-1)
                            g.drawImage(errorImage, (int)(y*Integer.parseInt(data[1])*zoomfactor/graphFactor), (int)(x*Integer.parseInt(data[0])*zoomfactor/graphFactor), (int)(Integer.parseInt(data[1])*zoomfactor/graphFactor), (int)(Integer.parseInt(data[0])*zoomfactor/graphFactor), null);  
                    }
                }
                errorMatrix = null;                
            }
    
            g.dispose();
            bufferedImage = null;
            
            fabric.setImage(SwingFXUtils.toFXImage(bufferedImageesize, null));
            container.setContent(fabric);
            bufferedImageesize = null;
            System.gc();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"plotEditGraph "+ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void fabricGraphEditEvent(MouseEvent event){
        String[] data = objFabric.getObjConfiguration().getStrGraphSize().split("x");
        if(event.isSecondaryButtonDown() || (event.getButton() == MouseButton.SECONDARY)){
            objFabric.getFabricMatrix()[(int)(event.getY()/(Integer.parseInt(data[0])*zoomfactor/graphFactor))][(int)(event.getX()/(Integer.parseInt(data[1])*zoomfactor/graphFactor))] = (byte)editThreadSecondaryValue;
            objFabric.getReverseMatrix()[(int)(event.getY()/(Integer.parseInt(data[0])*zoomfactor/graphFactor))][(int)(event.getX()/(Integer.parseInt(data[1])*zoomfactor/graphFactor))] = (editThreadSecondaryValue==1)?(byte)0:(byte)1;
        } else{
            objFabric.getFabricMatrix()[(int)(event.getY()/(Integer.parseInt(data[0])*zoomfactor/graphFactor))][(int)(event.getX()/(Integer.parseInt(data[1])*zoomfactor/graphFactor))] = (byte)editThreadPrimaryValue;
            objFabric.getReverseMatrix()[(int)(event.getY()/(Integer.parseInt(data[0])*zoomfactor/graphFactor))][(int)(event.getX()/(Integer.parseInt(data[1])*zoomfactor/graphFactor))] = (editThreadPrimaryValue==1)?(byte)0:(byte)1;
        }
        plotEditGraph();
    }
    private void populateEditGraphPane(){
        if(fabricChildStage!=null){
            fabricChildStage.close();
            fabricChildStage = null;
            System.gc();
        }
        fabricChildStage = new Stage();
        fabricChildStage.initOwner(fabricStage);
        fabricChildStage.initStyle(StageStyle.UTILITY);
        //dialogStage.initModality(Modality.WINDOW_MODAL);
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setAlignment(Pos.CENTER);
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        
        Label lblNote = new Label(objDictionaryAction.getWord("HELP"));
        lblNote.setWrapText(true);
        lblNote.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        lblNote.setTooltip(new Tooltip(objDictionaryAction.getWord("HELPGRAPHCORRECTION")));
        //lblNote.setPrefWidth(objConfiguration.WIDTH*0.15);
        lblNote.setAlignment(Pos.CENTER);
        lblNote.setStyle("-fx-font: bold italic 11pt Arial; -fx-text-fill: #FF0000;");
        popup.add(lblNote, 0, 0, 1, 1);
        
        final Label lblLeft = new Label();
        lblLeft.setGraphic(new ImageView("/media/mouse_left.png"));
        lblLeft.setTooltip(new Tooltip(objDictionaryAction.getWord("LEFT")));
        //lblLeft.setPrefWidth(objConfiguration.WIDTH*0.15);
        lblLeft.setAlignment(Pos.CENTER);
        lblLeft.setStyle("-fx-font: bold italic 11pt Arial; -fx-text-fill: #FF0000; -fx-background-color: #FFFFFF;");
        popup.add(lblLeft, 1, 0, 1, 1);
        final Label lblRight = new Label();
        lblRight.setGraphic(new ImageView("/media/mouse_right.png"));
        lblRight.setTooltip(new Tooltip(objDictionaryAction.getWord("RIGHT")));
        //lblRight.setPrefWidth(objConfiguration.WIDTH*0.15);
        lblRight.setAlignment(Pos.CENTER);

        if(objFabric.getIntExtraWarp()>0 || objFabric.getIntExtraWeft()>0)
            lblRight.setStyle("-fx-font: bold italic 11pt Arial; -fx-text-fill: #FF0000; -fx-background-color: #FFFFFF;");
        else
            lblRight.setStyle("-fx-font: bold italic 11pt Arial; -fx-text-fill: #FF0000; -fx-background-color: #000000;");
        popup.add(lblRight, 2, 0, 1, 1);
        
        Separator sepHor1 = new Separator();
        sepHor1.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHor1, 0, 1);
        GridPane.setColumnSpan(sepHor1, 3);
        popup.getChildren().add(sepHor1);
        
        popup.add(new Label(objDictionaryAction.getWord("EDITTHREADTYPE")+" : "), 0, 2, 3, 1);
        GridPane editThreadCB = new GridPane();
        editThreadCB.setAlignment(Pos.TOP_LEFT);
        editThreadCB.setHgap(5);
        editThreadCB.setVgap(5);
        editThreadCB.setCursor(Cursor.HAND);
        //editThreadCB.getChildren().clear();
        //System.err.println("Extra weaft"+objFabric.getIntExtraWeft());
        for(int i=-(objFabric.getIntExtraWarp()+1), j=0;i<=objFabric.getIntExtraWeft()+1;i++,j++){
            if(i==-1){
                //do nothing
                j--;
            } else {
                final Label lblColor  = new Label ();
                lblColor.setPrefSize(33, 33);
                String webColor = "#FFFFFF";
                int id = i;
                if(i==0){
                    if(objFabric.getIntExtraWarp()>0 || objFabric.getIntExtraWeft()>0)
                        webColor = "#FFFFFF";
                    lblColor.setTooltip(new Tooltip("Weft \n Color:"+webColor));
                } else if(i==1){
                    if(objFabric.getIntExtraWarp()>0 || objFabric.getIntExtraWeft()>0)
                        webColor = "#000000";
                    lblColor.setTooltip(new Tooltip("Warp \n Color:"+webColor));
                } else if(i==-1){
                    //do nothing
                } else if(i>1){
                    webColor = objFabric.getWeftExtraYarn()[i-2].getStrYarnColor();
                    lblColor.setTooltip(new Tooltip("Extra Weft #"+(i-1)+"\n Color:"+webColor));
                } else {
                    id = -(j+2);
                    webColor = objFabric.getWarpExtraYarn()[i+objFabric.getIntExtraWarp()+1].getStrYarnColor();
                    lblColor.setTooltip(new Tooltip("Extra Warp #"+(objFabric.getIntExtraWarp()+i+2)+"\n Color:"+webColor));
                }
                lblColor.setStyle("-fx-background-color:"+webColor+"; -fx-border-color: #FFFFFF;");
                //System.err.println("i="+i);
                lblColor.setUserData(id);
                lblColor.setId(webColor);
                lblColor.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        if(event.isSecondaryButtonDown() || (event.getButton() == MouseButton.SECONDARY)){
                            editThreadSecondaryValue=Integer.parseInt(lblColor.getUserData().toString());
                            lblRight.setStyle("-fx-font: bold italic 11pt Arial; -fx-text-fill: #FF0000; -fx-background-color:"+lblColor.getId()+";");
                        }else{
                            editThreadPrimaryValue=Integer.parseInt(lblColor.getUserData().toString());
                            lblLeft.setStyle("-fx-font: bold italic 11pt Arial; -fx-text-fill: #FF0000; -fx-background-color:"+lblColor.getId()+";");
                        }
                    }
                });
                editThreadCB.add(lblColor, j%6, j/6);
            }
        }
        editThreadPrimaryValue=0; //base weft
        editThreadSecondaryValue=1; //base warp
        popup.add(editThreadCB, 0, 3, 3, 1);
        
        final CheckBox errorGraphCB = new CheckBox(objDictionaryAction.getWord("SHOWERRORGRAPH"));
        errorGraphCB.setSelected(objConfiguration.getBlnErrorGraph());
        errorGraphCB.selectedProperty().addListener(new ChangeListener<Boolean>() {
           public void changed(ObservableValue<? extends Boolean> ov,
             Boolean old_val, Boolean new_val) {
                objConfiguration.setBlnErrorGraph(errorGraphCB.isSelected());
                plotEditGraph();                
            }
        });
        popup.add(errorGraphCB, 0, 4, 3, 1);
        
        final CheckBox correctGraphCB = new CheckBox(objDictionaryAction.getWord("SHOWCORRECTGRAPH"));
        correctGraphCB.setWrapText(true);
        correctGraphCB.setSelected(objConfiguration.getBlnCorrectGraph());
        correctGraphCB.selectedProperty().addListener(new ChangeListener<Boolean>() {
           public void changed(ObservableValue<? extends Boolean> ov,
             Boolean old_val, Boolean new_val) {
                objConfiguration.setBlnCorrectGraph(correctGraphCB.isSelected());
                plotEditGraph();
                //objConfiguration.setBlnCorrectGraph(false);                
                //correctGraphCB.setSelected(false);
            }
        });
        popup.add(correctGraphCB, 0, 5, 3, 1);
                
        popup.add(new Label(objDictionaryAction.getWord("FLOATBINDSIZE")+" : "), 0, 6, 1, 1);
        final TextField doubleBindTF = new TextField(Integer.toString(objConfiguration.getIntFloatBind())){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        doubleBindTF.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(doubleBindTF.getText()!=null && doubleBindTF.getText()!="" && Integer.parseInt(doubleBindTF.getText())>0){
                    objConfiguration.setIntFloatBind(Integer.parseInt(doubleBindTF.getText()));
                    plotEditGraph();
                }
            }
        });
        doubleBindTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPFLOATBINDSIZE")));
        popup.add(doubleBindTF, 1, 6, 2, 1);
        
        fabricChildStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                fabricChildStage.close();
                plotViewActionMode = 11;
                plotViewAction();
                objConfiguration.setBlnCorrectGraph(false);
            }
        });
        Scene popupScene = new Scene(popup);       
        popupScene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        fabricChildStage.setScene(popupScene);
        fabricChildStage.setResizable(false);
        fabricChildStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("WINDOWEDITGRAPH"));
        fabricChildStage.showAndWait();        
    }
    private void densityAction(){
        plotViewActionMode = 1;
        plotViewAction();
        lblStatus.setText(objDictionaryAction.getWord("ACTIONDENSITYEDIT"));
        
        FabricDensityView objFabricDensityView = new FabricDensityView(objFabric);
        objFabric.setIntHPI((int) Math.ceil(objFabric.getIntEPI()/(double)objFabric.getIntTPD()));
        objFabric.getObjConfiguration().setStrGraphSize(12+"x"+(int)((12*objFabric.getIntPPI())/objFabric.getIntEPI()));
        Fabric objFabricCopy = cloneFabric();
        objFabricCopy.setStrFabricName("EditDensity");
        objUR.doCommand("Edit Density", objFabricCopy);
        plotViewAction();
    }
    private void yarnPropertiesAction(){
        plotViewActionMode = 1;
        plotViewAction();
        lblStatus.setText(objDictionaryAction.getWord("ACTIONYARNEDIT"));
            
        objFabric.getObjConfiguration().setWarpYarn(objFabric.getWarpYarn());
        objFabric.getObjConfiguration().setWeftYarn(objFabric.getWeftYarn());
        objFabric.getObjConfiguration().setWarpExtraYarn(objFabric.getWarpExtraYarn());
        objFabric.getObjConfiguration().setWeftExtraYarn(objFabric.getWeftExtraYarn());
        objFabric.getObjConfiguration().setIntExtraWeft(objFabric.getIntExtraWeft());
        objFabric.getObjConfiguration().setIntExtraWarp(objFabric.getIntExtraWarp());
        objFabric.getObjConfiguration().setColourPalette(objFabric.getColourPalette());
        try{
            YarnEditView objYarnEditView = new YarnEditView(objFabric.getObjConfiguration());
        }catch(Exception ex){
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        objFabric.setWarpYarn(objFabric.getObjConfiguration().getWarpYarn());
        objFabric.setWeftYarn(objFabric.getObjConfiguration().getWeftYarn());
        objFabric.setWarpExtraYarn(objFabric.getObjConfiguration().getWarpExtraYarn());
        objFabric.setWeftExtraYarn(objFabric.getObjConfiguration().getWeftExtraYarn());
        objFabric.setIntExtraWeft(objFabric.getObjConfiguration().getIntExtraWeft());
        objFabric.setIntExtraWarp(objFabric.getObjConfiguration().getIntExtraWarp());
        objFabric.setColourPalette(objFabric.getObjConfiguration().getColourPalette());
        Fabric objFabricCopy = cloneFabric();
        objFabricCopy.setStrFabricName("EditYarn");
        objUR.doCommand("Edit Yarn", objFabricCopy);
        plotViewAction();
    }
    private void threadSequenceAction(){
        plotViewActionMode = 1;
        plotViewAction();
        lblStatus.setText(objDictionaryAction.getWord("ACTIONTHREADPATTERNEDIT"));
            
        try {
            objFabric.getObjConfiguration().setColourPalette(objFabric.getColourPalette());
            objFabric.getObjConfiguration().setStrWarpPatternID(objFabric.getStrWarpPatternID());
            objFabric.getObjConfiguration().setStrWeftPatternID(objFabric.getStrWeftPatternID());
            objFabric.getObjConfiguration().setWarpYarn(objFabric.getWarpYarn());
            objFabric.getObjConfiguration().setWeftYarn(objFabric.getWeftYarn());
            PatternView objPatternView = new PatternView(objFabric.getObjConfiguration());            
            objFabric.setColourPalette(objFabric.getObjConfiguration().getColourPalette());
            objFabric.setStrWarpPatternID(objFabric.getObjConfiguration().getStrWarpPatternID());
            objFabric.setStrWeftPatternID(objFabric.getObjConfiguration().getStrWeftPatternID());
            objFabric.setWarpYarn(objFabric.getObjConfiguration().getWarpYarn());
            objFabric.setWeftYarn(objFabric.getObjConfiguration().getWeftYarn());
            
            warpYarn = objFabric.getWarpYarn();
            weftYarn = objFabric.getWeftYarn();
            threadPaletes = objFabric.getColourPalette();
            Fabric objFabricCopy = cloneFabric();
            objUR.doCommand("Edit Thread Sequence", objFabricCopy);
            plotViewAction();
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);            
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void switchColorAction(){
        plotViewActionMode = 1;
        plotViewAction();
        lblStatus.setText(objDictionaryAction.getWord("ACTIONSWITCHCOLOREDIT"));
        
        threadPaletes = objFabric.getColourPalette(); //new String[]{"161616", "B40431", "B4045F", "B40486", "B404AE", "8904B1", "5F04B4", "3104B4", "0404B4", "0431B4", "045FB4", "0489B1", "04B4AE", "04B486", "04B45F", "04B431", "04B404", "31B404", "5FB404", "86B404", "AEB404", "B18904", "B45F04", "B43104", "B40404", "FFFFFF", "BBA677", "B40431", "B4045F", "B40486", "B404AE", "8904B1", "5F04B4", "3104B4", "0404B4", "0431B4", "045FB4", "0489B1", "04B4AE", "04B486", "04B45F", "04B431", "04B404", "31B404", "5FB404", "86B404", "AEB404", "B18904", "B45F04", "B43104", "B40404", "FFFFFF"}; 
        objFabric.getColourPalette();
        String temp;
        for(int i=0; i<threadPaletes.length/2;i++){
            temp = threadPaletes[i];
            threadPaletes[i] = threadPaletes[i+26];
            threadPaletes[i+26] = temp;
        }
        weftYarn = objFabric.getWeftYarn();
        warpYarn = objFabric.getWarpYarn();
        char character;
        for(int i=0; i<warpYarn.length;i++){
            //65 - 96
            character = warpYarn[i].getStrYarnSymbol().trim().toUpperCase().charAt(0); // This gives the character 'a'
            int ascii = (int) character; 
            warpYarn[i].setStrYarnColor("#"+threadPaletes[ascii-65]);        
        }
        for(int i=0; i<weftYarn.length;i++){
            //97 - 113
            character = weftYarn[i].getStrYarnSymbol().trim().toLowerCase().charAt(0); // This gives the character 'a'
            int ascii = (int) character; 
            weftYarn[i].setStrYarnColor("#"+threadPaletes[ascii-71]);
        }
        objFabric.setWarpYarn(warpYarn);
        objFabric.setWeftYarn(weftYarn);
        objFabric.setColourPalette(threadPaletes);
        Fabric objFabricCopy = cloneFabric();
        objFabricCopy.setStrFabricName("EditSwitchColor");
        objUR.doCommand("Edit Switch Color", objFabricCopy);
        plotViewAction();
    }
    private void switchThreadSequence(){
        warpYarn = objFabric.getWeftYarn();
        weftYarn = objFabric.getWarpYarn();
        for(int i=0; i<warpYarn.length;i++){
            warpYarn[i].setStrYarnType("Warp");
            warpYarn[i].setStrYarnName("Warp"+i);
            warpYarn[i].setStrYarnSymbol(warpYarn[i].getStrYarnSymbol().toUpperCase());
        }
        for(int i=0; i<weftYarn.length;i++){
            weftYarn[i].setStrYarnType("Weft");
            weftYarn[i].setStrYarnName("Weft"+i);
            weftYarn[i].setStrYarnSymbol(weftYarn[i].getStrYarnSymbol().toLowerCase());
        }
        objFabric.setWarpYarn(warpYarn);
        objFabric.setWeftYarn(weftYarn);
        String pid = objFabric.getStrWarpPatternID();
        objFabric.setStrWarpPatternID(objFabric.getStrWeftPatternID());
        objFabric.setStrWeftPatternID(pid);
        threadPaletes = objFabric.getColourPalette();
        Fabric objFabricCopy = cloneFabric();
        objFabricCopy.setStrFabricName("EditThreadSequence");
        objUR.doCommand("Edit Thread Sequence", objFabricCopy);
        plotViewAction();
    }    
    private void transformOperationAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONTRANSFORMOPERATIONEDIT"));
        plotViewActionMode = 1;
        plotViewAction();
        transformMenuAction();
            
        try {
            objWeave = new Weave();
            objWeave.setObjConfiguration(objFabric.getObjConfiguration());
            objWeave.setDesignMatrix(objFabric.getFabricMatrix());
            objWeave.setIntWeft(objFabric.getIntWeft());
            objWeave.setIntWarp(objFabric.getIntWarp());

            objWeaveR = new Weave();
            objWeave.setObjConfiguration(objFabric.getObjConfiguration());
            objWeaveR.setDesignMatrix(objFabric.getReverseMatrix());
            objWeaveR.setIntWeft(objFabric.getIntWeft());
            objWeaveR.setIntWarp(objFabric.getIntWarp());

            objWeaveAction = new WeaveAction();
            isEditingMode = true;
            plotEditActionMode = 4; //editweave = 4
            plotEditWeave();
            Fabric objFabricCopy = cloneFabric();
            objFabricCopy.setStrFabricName("EditWeavePattern");
            objUR.doCommand("Edit Weave Pattern", objFabricCopy);
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"editWeave "+ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"editWeave "+ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    /**
     * correct weave
     * <p>
     * This method is used for creating GUI of graph edit in fabric
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @see         javafx.stage.*;
     * @link        com.mla.fabric.Fabric
     */
    private void plotEditWeave(){
        try {
            objFabric.setFabricMatrix(objWeave.getDesignMatrix());
            objFabric.setReverseMatrix(objWeaveR.getDesignMatrix());
            objFabric.setIntWeft(objWeave.getIntWeft());
            objFabric.setIntWarp(objWeave.getIntWarp());
            BufferedImage bufferedImage = new BufferedImage(objWeave.getIntWeft(), objWeave.getIntWarp(),BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            //bufferedImage = objFabricAction.plotFabricEditOLD(objWeave.getDesignMatrix(),objConfiguration.getIntBoxSize(),objWeave.getIntWarp(),objWeave.getIntWeft()); 
            bufferedImage = objFabricAction.plotFabricEdit(objFabric, objWeave.getDesignMatrix(),objFabric.getObjConfiguration().getIntBoxSize(),objWeave.getIntWarp(),objWeave.getIntWeft()); 
            int intLength = (int)(objFabric.getIntWarp()*zoomfactor);
            int intHeight = (int)(objFabric.getIntWeft()*zoomfactor);
            
            BufferedImage bufferedImageesize = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bufferedImageesize.createGraphics();
            g.drawImage(bufferedImage, 0, 0, intLength, intHeight, null);
            g.dispose();
            bufferedImage = bufferedImageesize;
            bufferedImageesize = null;
            fabric.setImage(SwingFXUtils.toFXImage(bufferedImage, null));
            container.setContent(fabric);
            bufferedImage = null;
            System.gc();
        } catch (SQLException ex) {
            //Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            new Logging("SEVERE",getClass().getName(),"plotEditWeave "+ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
 /**
 * populateTransformToolbar
 * <p>
 * Function use for editing tool bar for menu item Edit,
 * and binding events for each tools. 
 * 
 * @exception   (@throws SQLException)
 * @author      Amit Kumar Singh
 * @version     %I%, %G%
 * @since       1.0
 * @see         javafx.event.*;
 * @link        WeaveView
 */
    private void populateTransformToolbarMenu(){
        //For drop down menus
        menuSelected.setTooltip(new Tooltip(objDictionaryAction.getWord("EDIT")));
       
        MenuItem mirrorVerticalEditMI = new MenuItem(objDictionaryAction.getWord("VERTICALMIRROR"));
        MenuItem mirrorHorizontalEditMI = new MenuItem(objDictionaryAction.getWord("HORIZENTALMIRROR"));
        MenuItem rotateClockwiseEditMI = new MenuItem(objDictionaryAction.getWord("CLOCKROTATION"));
        MenuItem rotateAnticlockwiseEditMI = new MenuItem(objDictionaryAction.getWord("ANTICLOCKROTATION"));
        MenuItem moveRightEditMI = new MenuItem(objDictionaryAction.getWord("MOVERIGHT"));
        MenuItem moveLeftEditMI = new MenuItem(objDictionaryAction.getWord("MOVELEFT"));
        MenuItem moveUpEditMI = new MenuItem(objDictionaryAction.getWord("MOVEUP"));
        MenuItem moveDownEditMI = new MenuItem(objDictionaryAction.getWord("MOVEDOWN"));        
        MenuItem moveRight8EditMI = new MenuItem(objDictionaryAction.getWord("MOVERIGHT8"));
        MenuItem moveLeft8EditMI = new MenuItem(objDictionaryAction.getWord("MOVELEFT8"));
        MenuItem moveUp8EditMI = new MenuItem(objDictionaryAction.getWord("MOVEUP8"));
        MenuItem moveDown8EditMI = new MenuItem(objDictionaryAction.getWord("MOVEDOWN8"));        
        MenuItem tiltRightEditMI = new MenuItem(objDictionaryAction.getWord("TILTRIGHT"));
        MenuItem tiltLeftEditMI = new MenuItem(objDictionaryAction.getWord("TILTLEFT"));
        MenuItem tiltUpEditMI = new MenuItem(objDictionaryAction.getWord("TILTUP"));
        MenuItem tiltDownEditMI = new MenuItem(objDictionaryAction.getWord("TILTDOWN")); 
        MenuItem inversionEditMI = new MenuItem(objDictionaryAction.getWord("INVERSION"));
        MenuItem closeEditMI = new MenuItem(objDictionaryAction.getWord("CLOSE"));
        
        mirrorVerticalEditMI.setAccelerator(new KeyCodeCombination(KeyCode.V, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        mirrorHorizontalEditMI.setAccelerator(new KeyCodeCombination(KeyCode.H, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        rotateClockwiseEditMI.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        rotateAnticlockwiseEditMI.setAccelerator(new KeyCodeCombination(KeyCode.A, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        moveUpEditMI.setAccelerator(new KeyCodeCombination(KeyCode.UP, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        moveDownEditMI.setAccelerator(new KeyCodeCombination(KeyCode.DOWN, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        moveLeftEditMI.setAccelerator(new KeyCodeCombination(KeyCode.LEFT, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        moveRightEditMI.setAccelerator(new KeyCodeCombination(KeyCode.RIGHT, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        moveUp8EditMI.setAccelerator(new KeyCodeCombination(KeyCode.UP, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        moveDown8EditMI.setAccelerator(new KeyCodeCombination(KeyCode.DOWN, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        moveLeft8EditMI.setAccelerator(new KeyCodeCombination(KeyCode.LEFT, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        moveRight8EditMI.setAccelerator(new KeyCodeCombination(KeyCode.RIGHT, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        tiltUpEditMI.setAccelerator(new KeyCodeCombination(KeyCode.UP, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        tiltDownEditMI.setAccelerator(new KeyCodeCombination(KeyCode.DOWN, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        tiltLeftEditMI.setAccelerator(new KeyCodeCombination(KeyCode.LEFT, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        tiltRightEditMI.setAccelerator(new KeyCodeCombination(KeyCode.RIGHT, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        inversionEditMI.setAccelerator(new KeyCodeCombination(KeyCode.I, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        closeEditMI.setAccelerator(new KeyCodeCombination(KeyCode.ESCAPE));
                
        mirrorVerticalEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
        mirrorHorizontalEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
        rotateClockwiseEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_90.png"));
        rotateAnticlockwiseEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_anti_90.png"));
        moveRightEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right.png"));
        moveLeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left.png"));
        moveUpEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up.png"));
        moveDownEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down.png"));
        moveRight8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right_by_8.png"));
        moveLeft8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left_by_8.png"));
        moveUp8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up_by_8.png"));
        moveDown8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down_by_8.png"));        
        tiltRightEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_right.png"));
        tiltLeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_left.png"));
        tiltUpEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_up.png"));
        tiltDownEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_down.png"));
        inversionEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/inversion.png"));
        closeEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
             
        Menu mirrorMenu = new Menu(objDictionaryAction.getWord("MIRROR"));
        Menu rotateMenu = new Menu(objDictionaryAction.getWord("ROTATE"));
        Menu moveMenu = new Menu(objDictionaryAction.getWord("MOVE"));
        Menu tiltMenu = new Menu(objDictionaryAction.getWord("TILT"));        
        mirrorMenu.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/symmetry.png"));
        rotateMenu.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/update.png"));
        moveMenu.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/skip.png"));
        tiltMenu.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/diamond.png"));
        mirrorMenu.getItems().addAll(mirrorVerticalEditMI,mirrorHorizontalEditMI);
        rotateMenu.getItems().addAll(rotateClockwiseEditMI,rotateAnticlockwiseEditMI);
        moveMenu.getItems().addAll(moveRightEditMI, moveLeftEditMI, moveUpEditMI, moveDownEditMI, moveRight8EditMI, moveLeft8EditMI, moveUp8EditMI, moveDown8EditMI);
        tiltMenu.getItems().addAll(tiltRightEditMI, tiltLeftEditMI, tiltUpEditMI, tiltDownEditMI);
    
        //Add menu enable disable condition
        if(!isWorkingMode){
            mirrorVerticalEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
            mirrorVerticalEditMI.setDisable(true);
            mirrorHorizontalEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
            mirrorHorizontalEditMI.setDisable(true);
            rotateClockwiseEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_90.png"));
            rotateClockwiseEditMI.setDisable(true);
            rotateAnticlockwiseEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_anti_90.png"));
            rotateAnticlockwiseEditMI.setDisable(true);
            moveRightEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right.png"));
            moveRightEditMI.setDisable(true);
            moveLeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left.png"));
            moveLeftEditMI.setDisable(true);
            moveUpEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up.png"));
            moveUpEditMI.setDisable(true);
            moveDownEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down.png"));
            moveDownEditMI.setDisable(true);
            moveRight8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right_by_8.png"));
            moveRight8EditMI.setDisable(true);
            moveLeft8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left_by_8.png"));
            moveLeft8EditMI.setDisable(true);
            moveUp8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up_by_8.png"));
            moveUp8EditMI.setDisable(true);
            moveDown8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down_by_8.png"));
            moveDown8EditMI.setDisable(true);
            tiltRightEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_right.png"));
            tiltRightEditMI.setDisable(true);
            tiltLeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_left.png"));
            tiltLeftEditMI.setDisable(true);
            tiltUpEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_up.png"));
            tiltUpEditMI.setDisable(true);
            tiltDownEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_down.png"));
            tiltDownEditMI.setDisable(true);
            inversionEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/inversion.png"));
            inversionEditMI.setDisable(true);
            closeEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            closeEditMI.setDisable(true);
        }else{
            mirrorVerticalEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
            mirrorVerticalEditMI.setDisable(false);
            mirrorHorizontalEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
            mirrorHorizontalEditMI.setDisable(false);
            rotateClockwiseEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_90.png"));
            rotateClockwiseEditMI.setDisable(false);
            rotateAnticlockwiseEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_anti_90.png"));
            rotateAnticlockwiseEditMI.setDisable(false);
            moveRightEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right.png"));
            moveRightEditMI.setDisable(false);
            moveLeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left.png"));
            moveLeftEditMI.setDisable(false);
            moveUpEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up.png"));
            moveUpEditMI.setDisable(false);
            moveDownEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down.png"));
            moveDownEditMI.setDisable(false);
            moveRight8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right_by_8.png"));
            moveRight8EditMI.setDisable(false);
            moveLeft8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left_by_8.png"));
            moveLeft8EditMI.setDisable(false);
            moveUp8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up_by_8.png"));
            moveUp8EditMI.setDisable(false);
            moveDown8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down_by_8.png"));
            moveDown8EditMI.setDisable(false);
            tiltRightEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_right.png"));
            tiltRightEditMI.setDisable(false);
            tiltLeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_left.png"));
            tiltLeftEditMI.setDisable(false);
            tiltUpEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_up.png"));
            tiltUpEditMI.setDisable(false);
            tiltDownEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_down.png"));
            tiltDownEditMI.setDisable(false);
            inversionEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/inversion.png"));
            inversionEditMI.setDisable(false);
            closeEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            closeEditMI.setDisable(false);
        }        
        //Add the action to Buttons.        
        mirrorVerticalEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                mirrorVerticalAction();
            }
        });  
        mirrorHorizontalEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                mirrorHorizontalAction();
            }
        }); 
        rotateClockwiseEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                rotateClockwiseAction();
            }
        }); 
        rotateAnticlockwiseEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                rotateAntiClockwiseAction();
            }
        }); 
        moveRightEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveRightAction();
            }
        });
        moveLeftEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveLeftAction();
            }
        });
        moveUpEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveUpAction();
            }
        });
        moveDownEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveDownAction();
            }
        });
        moveRight8EditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveRight8Action();
            }
        });
        moveLeft8EditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveLeft8Action();
            }
        });
        moveUp8EditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveUp8Action();
            }
        });
        moveDown8EditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveDown8Action();
            }
        });
        tiltRightEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                tiltRightAction();
            }
        });
        tiltLeftEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                tiltLeftAction();
            }
        });
        tiltUpEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                tiltUpAction();
            }
        });
        tiltDownEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                tiltDownAction();
            }
        });        
        inversionEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                inversionAction();
            }
        });
        closeEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                closeAction();
            }
        });
        //undoEditMI, redoEditMI, new SeparatorMenuItem(), 
        menuSelected.getItems().addAll(mirrorMenu,rotateMenu, moveMenu, tiltMenu, inversionEditMI, new SeparatorMenuItem(), closeEditMI);
    }
    private void populateTransformToolbar(){
        // mirror edit item
        Button mirrorVerticalEditBtn = new Button(); 
        mirrorVerticalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
        mirrorVerticalEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("VERTICALMIRROR")+" (Ctrl+Alt+V)\n"+objDictionaryAction.getWord("TOOLTIPVERTICALMIRROR")));
        mirrorVerticalEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        mirrorVerticalEditBtn.getStyleClass().addAll("toolbar-button");   
        // mirror edit item
        Button mirrorHorizontalEditBtn = new Button(); 
        mirrorHorizontalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
        mirrorHorizontalEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("HORIZENTALMIRROR")+" (Ctrl+Alt+H)\n"+objDictionaryAction.getWord("TOOLTIPHORIZENTALMIRROR")));
        mirrorHorizontalEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        mirrorHorizontalEditBtn.getStyleClass().addAll("toolbar-button");   
        // clock Wise edit item
        Button rotateClockwiseEditBtn = new Button(); 
        rotateClockwiseEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_90.png"));
        rotateClockwiseEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CLOCKROTATION")+" (Ctrl+Alt+C)\n"+objDictionaryAction.getWord("TOOLTIPCLOCKROTATION")));
        rotateClockwiseEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rotateClockwiseEditBtn.getStyleClass().addAll("toolbar-button");   
        // Anti clock wise edit item
        Button rotateAnticlockwiseEditBtn = new Button(); 
        rotateAnticlockwiseEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_anti_90.png"));
        rotateAnticlockwiseEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ANTICLOCKROTATION")+" (Ctrl+Alt+A)\n"+objDictionaryAction.getWord("TOOLTIPANTICLOCKROTATION")));
        rotateAnticlockwiseEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rotateAnticlockwiseEditBtn.getStyleClass().addAll("toolbar-button");
        // move Right
        Button moveRightEditBtn = new Button(); 
        moveRightEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right.png"));
        moveRightEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MOVERIGHT")+" (Ctrl+Shift+Right)\n"+objDictionaryAction.getWord("TOOLTIPMOVERIGHT")));
        moveRightEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveRightEditBtn.getStyleClass().addAll("toolbar-button");    
        // move Left
        Button moveLeftEditBtn = new Button(); 
        moveLeftEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left.png"));
        moveLeftEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MOVELEFT")+" (Ctrl+Shift+Left)\n"+objDictionaryAction.getWord("TOOLTIPMOVELEFT")));
        moveLeftEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveLeftEditBtn.getStyleClass().addAll("toolbar-button");    
        // move Up
        Button moveUpEditBtn = new Button(); 
        moveUpEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up.png"));
        moveUpEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MOVEUP")+" (Ctrl+Shift+Up)\n"+objDictionaryAction.getWord("TOOLTIPMOVEUP")));
        moveUpEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveUpEditBtn.getStyleClass().addAll("toolbar-button");    
        // move Down
        Button moveDownEditBtn = new Button(); 
        moveDownEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down.png"));
        moveDownEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MOVEDOWN")+" (Ctrl+Shift+Down)\n"+objDictionaryAction.getWord("TOOLTIPMOVEDOWN")));
        moveDownEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveDownEditBtn.getStyleClass().addAll("toolbar-button");
        // move Right 8
        Button moveRight8EditBtn = new Button(); 
        moveRight8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right_by_8.png"));
        moveRight8EditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MOVERIGHT8")+" (Alt+Shift+Right)\n"+objDictionaryAction.getWord("TOOLTIPMOVERIGHT8")));
        moveRight8EditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveRight8EditBtn.getStyleClass().addAll("toolbar-button");    
        // move Left 8
        Button moveLeft8EditBtn = new Button(); 
        moveLeft8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left_by_8.png"));
        moveLeft8EditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MOVELEFT8")+" (Alt+Shift+Left)\n"+objDictionaryAction.getWord("TOOLTIPMOVELEFT8")));
        moveLeft8EditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveLeft8EditBtn.getStyleClass().addAll("toolbar-button");    
        // move Up 8
        Button moveUp8EditBtn = new Button(); 
        moveUp8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up_by_8.png"));
        moveUp8EditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MOVEUP8")+" (Alt+Shift+Up)\n"+objDictionaryAction.getWord("TOOLTIPMOVEUP8")));
        moveUp8EditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveUp8EditBtn.getStyleClass().addAll("toolbar-button");    
        // move Down 8
        Button moveDown8EditBtn = new Button(); 
        moveDown8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down_by_8.png"));
        moveDown8EditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MOVEDOWN8")+" (Alt+Shift+Down)\n"+objDictionaryAction.getWord("TOOLTIPMOVEDOWN8")));
        moveDown8EditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveDown8EditBtn.getStyleClass().addAll("toolbar-button");
        // Tilt Right
        Button tiltRightEditBtn = new Button(); 
        tiltRightEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_right.png"));
        tiltRightEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("TILTRIGHT")+" (Ctrl+Alt+Right)\n"+objDictionaryAction.getWord("TOOLTIPTILTRIGHT")));
        tiltRightEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tiltRightEditBtn.getStyleClass().addAll("toolbar-button");    
        // Tilt Left
        Button tiltLeftEditBtn = new Button(); 
        tiltLeftEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_left.png"));
        tiltLeftEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("TILTLEFT")+" (Ctrl+Alt+Left)\n"+objDictionaryAction.getWord("TOOLTIPTILTLEFT")));
        tiltLeftEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tiltLeftEditBtn.getStyleClass().addAll("toolbar-button");    
        // Tilt Up
        Button tiltUpEditBtn = new Button(); 
        tiltUpEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_up.png"));
        tiltUpEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("TILTUP")+" (Ctrl+Alt+Up)\n"+objDictionaryAction.getWord("TOOLTIPTILTUP")));
        tiltUpEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tiltUpEditBtn.getStyleClass().addAll("toolbar-button");    
        // Tilt Down
        Button tiltDownEditBtn = new Button(); 
        tiltDownEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_down.png"));
        tiltDownEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("TILTDOWN")+" (Ctrl+Alt+Down)\n"+objDictionaryAction.getWord("TOOLTIPTILTDOWN")));
        tiltDownEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tiltDownEditBtn.getStyleClass().addAll("toolbar-button");
        // Inversion
        Button inversionEditBtn = new Button(); 
        inversionEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/inversion.png"));
        inversionEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("INVERSION")+" (Ctrl+Alt+I)\n"+objDictionaryAction.getWord("TOOLTIPINVERSION")));
        inversionEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        inversionEditBtn.getStyleClass().addAll("toolbar-button"); 
        // Clear
        Button closeEditBtn = new Button(); 
        closeEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        closeEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CLOSE")+" (Esc)\n"+objDictionaryAction.getWord("TOOLTIPCLOSE")));
        closeEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        closeEditBtn.getStyleClass().addAll("toolbar-button"); 
        //Add menu enable disable condition
        if(!isWorkingMode){
            mirrorVerticalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
            mirrorVerticalEditBtn.setDisable(true);
            mirrorVerticalEditBtn.setCursor(Cursor.WAIT);
            mirrorHorizontalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
            mirrorHorizontalEditBtn.setDisable(true);
            mirrorHorizontalEditBtn.setCursor(Cursor.WAIT);            
            rotateClockwiseEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_90.png"));
            rotateClockwiseEditBtn.setDisable(true);
            rotateClockwiseEditBtn.setCursor(Cursor.WAIT);
            rotateAnticlockwiseEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_anti_90.png"));
            rotateAnticlockwiseEditBtn.setDisable(true);
            rotateAnticlockwiseEditBtn.setCursor(Cursor.WAIT);
            moveRightEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right.png"));
            moveRightEditBtn.setDisable(true);
            moveRightEditBtn.setCursor(Cursor.WAIT);
            moveLeftEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left.png"));
            moveLeftEditBtn.setDisable(true);
            moveLeftEditBtn.setCursor(Cursor.WAIT);
            moveUpEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up.png"));
            moveUpEditBtn.setDisable(true);
            moveUpEditBtn.setCursor(Cursor.WAIT);
            moveDownEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down.png"));
            moveDownEditBtn.setDisable(true);
            moveDownEditBtn.setCursor(Cursor.WAIT);
            moveRight8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right_by_8.png"));
            moveRight8EditBtn.setDisable(true);
            moveRight8EditBtn.setCursor(Cursor.WAIT);
            moveLeft8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left_by_8.png"));
            moveLeft8EditBtn.setDisable(true);
            moveLeft8EditBtn.setCursor(Cursor.WAIT);
            moveUp8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up_by_8.png"));
            moveUp8EditBtn.setDisable(true);
            moveUp8EditBtn.setCursor(Cursor.WAIT);
            moveDown8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down_by_8.png"));
            moveDown8EditBtn.setDisable(true);
            moveDown8EditBtn.setCursor(Cursor.WAIT);
            tiltRightEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_right.png"));
            tiltRightEditBtn.setDisable(true);
            tiltRightEditBtn.setCursor(Cursor.WAIT);
            tiltLeftEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_left.png"));
            tiltLeftEditBtn.setDisable(true);
            tiltLeftEditBtn.setCursor(Cursor.WAIT);
            tiltUpEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_up.png"));
            tiltUpEditBtn.setDisable(true);
            tiltUpEditBtn.setCursor(Cursor.WAIT);
            tiltDownEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_down.png"));
            tiltDownEditBtn.setDisable(true);
            tiltDownEditBtn.setCursor(Cursor.WAIT);
            inversionEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/inversion.png"));
            inversionEditBtn.setDisable(true);
            inversionEditBtn.setCursor(Cursor.WAIT);
            closeEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            closeEditBtn.setDisable(true);
            closeEditBtn.setCursor(Cursor.WAIT);
        }else{
            mirrorVerticalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
            mirrorVerticalEditBtn.setDisable(false);
            mirrorVerticalEditBtn.setCursor(Cursor.HAND);
            mirrorHorizontalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
            mirrorHorizontalEditBtn.setDisable(false);
            mirrorHorizontalEditBtn.setCursor(Cursor.HAND);
            rotateClockwiseEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_90.png"));
            rotateClockwiseEditBtn.setDisable(false);
            rotateClockwiseEditBtn.setCursor(Cursor.HAND);
            rotateAnticlockwiseEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_anti_90.png"));
            rotateAnticlockwiseEditBtn.setDisable(false);
            rotateAnticlockwiseEditBtn.setCursor(Cursor.HAND);
            moveRightEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right.png"));
            moveRightEditBtn.setDisable(false);
            moveRightEditBtn.setCursor(Cursor.HAND);
            moveLeftEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left.png"));
            moveLeftEditBtn.setDisable(false);
            moveLeftEditBtn.setCursor(Cursor.HAND);
            moveUpEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up.png"));
            moveUpEditBtn.setDisable(false);
            moveUpEditBtn.setCursor(Cursor.HAND);
            moveDownEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down.png"));
            moveDownEditBtn.setDisable(false);
            moveDownEditBtn.setCursor(Cursor.HAND);
            moveRight8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right_by_8.png"));
            moveRight8EditBtn.setDisable(false);
            moveRight8EditBtn.setCursor(Cursor.HAND);
            moveLeft8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left_by_8.png"));
            moveLeft8EditBtn.setDisable(false);
            moveLeft8EditBtn.setCursor(Cursor.HAND);
            moveUp8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up_by_8.png"));
            moveUp8EditBtn.setDisable(false);
            moveUp8EditBtn.setCursor(Cursor.HAND);
            moveDown8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down_by_8.png"));
            moveDown8EditBtn.setDisable(false);
            moveDown8EditBtn.setCursor(Cursor.HAND);
            tiltRightEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_right.png"));
            tiltRightEditBtn.setDisable(false);
            tiltRightEditBtn.setCursor(Cursor.HAND);
            tiltLeftEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_left.png"));
            tiltLeftEditBtn.setDisable(false);
            tiltLeftEditBtn.setCursor(Cursor.HAND);
            tiltUpEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_up.png"));
            tiltUpEditBtn.setDisable(false);
            tiltUpEditBtn.setCursor(Cursor.HAND);
            tiltDownEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_down.png"));
            tiltDownEditBtn.setDisable(false);
            tiltDownEditBtn.setCursor(Cursor.HAND);
            inversionEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/inversion.png"));
            inversionEditBtn.setDisable(false);
            inversionEditBtn.setCursor(Cursor.HAND);
            closeEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            closeEditBtn.setDisable(false);
            closeEditBtn.setCursor(Cursor.HAND);         
        }        
        //Add the action to Buttons.
        mirrorVerticalEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mirrorVerticalAction();
            }
        });  
        mirrorHorizontalEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mirrorHorizontalAction();
            }
        }); 
        rotateClockwiseEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rotateClockwiseAction();
            }
        }); 
        rotateAnticlockwiseEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rotateAntiClockwiseAction();
            }
        }); 
        moveRightEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                moveRightAction();
            }
        });
        moveLeftEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                moveLeftAction();
            }
        });
        moveUpEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                moveUpAction();
            }
        });
        moveDownEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                moveDownAction();
            }
        });
        moveRight8EditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                moveRight8Action();
            }
        });
        moveLeft8EditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                moveLeft8Action();
            }
        });
        moveUp8EditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                moveUp8Action();
            }
        });
        moveDown8EditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                moveDown8Action();
            }
        });
        tiltRightEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                tiltRightAction();
            }
        });
        tiltLeftEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                tiltLeftAction();
            }
        });
        tiltUpEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                tiltUpAction();
            }
        });
        tiltDownEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                tiltDownAction();
            }
        });        
        inversionEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                inversionAction();
            }
        });
        closeEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                closeAction();
            }
        });
        //Create some Buttons.        
        FlowPane flow = new FlowPane();        
        flow.setPrefWrapLength(objConfiguration.WIDTH*.95);
        flow.getStyleClass().addAll("flow");
        //Add the Buttons to the ToolBar.
        //undoEditBtn,redoEditBtn,
        flow.getChildren().addAll(mirrorVerticalEditBtn,mirrorHorizontalEditBtn,rotateClockwiseEditBtn,rotateAnticlockwiseEditBtn,moveRightEditBtn,moveLeftEditBtn,moveUpEditBtn,moveDownEditBtn,moveRight8EditBtn,moveLeft8EditBtn,moveUp8EditBtn,moveDown8EditBtn,tiltRightEditBtn,tiltLeftEditBtn,tiltUpEditBtn,tiltDownEditBtn,inversionEditBtn,closeEditBtn);
        flow.setAlignment(Pos.TOP_LEFT);
        toolBar.getItems().addAll(flow);
    }
    private void mirrorVerticalAction(){
        try {                       
            lblStatus.setText(objDictionaryAction.getWord("ACTIONVERTICALMIRROR"));
            objWeaveAction.mirrorVertical(objWeave);
            objWeaveAction.mirrorVertical(objWeaveR);
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Mirror Vertical",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }  
    }
    private void mirrorHorizontalAction(){        
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONHORIZENTALMIRROR"));
            objWeaveAction.mirrorHorizontal(objWeave);
            objWeaveAction.mirrorHorizontal(objWeaveR);
            plotEditWeave();
        } catch (Exception ex) {
          new Logging("SEVERE",getClass().getName(),"Mirror Vertical",ex);
          lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void rotateClockwiseAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONCLOCKROTATION"));
            objWeaveAction.rotation(objWeave);
            objWeaveAction.rotation(objWeaveR);
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Rotate Clock Wise ",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void rotateAntiClockwiseAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONANTICLOCKROTATION"));
            objWeaveAction.rotationAnti(objWeave);
            objWeaveAction.rotationAnti(objWeaveR);
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Anti Rotate Clock Wise ",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void moveRightAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVERIGHT"));
            objWeaveAction.moveRight(objWeave);
            objWeaveAction.moveRight(objWeaveR);
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Right",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void moveLeftAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVELEFT"));
            objWeaveAction.moveLeft(objWeave);
            objWeaveAction.moveLeft(objWeaveR);
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Left",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void moveUpAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVEUP"));
            objWeaveAction.moveUp(objWeave);
            objWeaveAction.moveUp(objWeaveR);
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Up",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void moveDownAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVEDOWN"));
            objWeaveAction.moveDown(objWeave);
            objWeaveAction.moveDown(objWeaveR);
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Down",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void moveRight8Action(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVERIGHT8"));
            objWeaveAction.moveRight8(objWeave);
            objWeaveAction.moveRight8(objWeaveR);
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Right",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void moveLeft8Action(){        
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVELEFT8"));
            objWeaveAction.moveLeft8(objWeave);
            objWeaveAction.moveLeft8(objWeaveR);
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Left",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void moveUp8Action(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVEUP8"));
            objWeaveAction.moveUp8(objWeave);
            objWeaveAction.moveUp8(objWeaveR);
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Up",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void moveDown8Action(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVEDOWN8"));
            objWeaveAction.moveDown8(objWeave);
            objWeaveAction.moveDown8(objWeaveR);
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Down",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void tiltRightAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONTILTRIGHT"));
            objWeaveAction.tiltRight(objWeave);
            objWeaveAction.tiltRight(objWeaveR);
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Tilt Right ",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void tiltLeftAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONTILTLEFT"));
            objWeaveAction.tiltLeft(objWeave);
            objWeaveAction.tiltLeft(objWeaveR);
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Tilt Left ",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void tiltUpAction(){
        try {
             lblStatus.setText(objDictionaryAction.getWord("ACTIONTILTUP"));
             objWeaveAction.tiltUp(objWeave);
             objWeaveAction.tiltUp(objWeaveR);
             plotEditWeave();
         } catch (Exception ex) {
             new Logging("SEVERE",getClass().getName(),"Tilt Up ",ex);
             lblStatus.setText(objDictionaryAction.getWord("ERROR"));
         } 
    }
    private void tiltDownAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONTILTDOWN"));
            objWeaveAction.tiltDown(objWeave);
            objWeaveAction.tiltDown(objWeaveR);
            plotEditWeave();
        } catch (Exception ex) {
           new Logging("SEVERE",getClass().getName(),"Tilt Down ",ex);
           lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void inversionAction(){        
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONINVERSION"));
            objWeaveAction.inversion(objWeave);
            objWeaveAction.inversion(objWeaveR);
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Inversion ",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void closeAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
        selectedMenu = "EDIT";
        transformMenu.show();
        transformMenu.setVisible(false);
        fileMenu.setDisable(false);
        editMenu.setDisable(false);
        viewMenu.setDisable(false);
        utilityMenu.setDisable(false);
        runMenu.setDisable(false);
        transformMenu.setDisable(true);
        menuHighlight();       
        
        plotViewActionMode = 1;
        plotViewAction();
    }
/**
 * populateViewToolbar
 * <p>
 * Function use for drawing tool bar for menu item View,
 * and binding events for each tools. 
 * 
 * @exception   (@throws SQLException)
 * @author      Amit Kumar Singh
 * @version     %I%, %G%
 * @since       1.0
 * @see         javafx.event.*;
 * @link        FabricView
 */    
    private void populateViewToolbarMenu(){
        //For drop down menus
        menuSelected.setTooltip(new Tooltip(objDictionaryAction.getWord("VIEW")));
        
        MenuItem frontSideViewMI = new MenuItem(objDictionaryAction.getWord("FRONTSIDEVIEW"));
        MenuItem rearSideViewMI = new MenuItem(objDictionaryAction.getWord("REARSIDEVIEW"));
        MenuItem frontVisualizationViewMI = new MenuItem(objDictionaryAction.getWord("VISUALIZATIONFRONTVIEW"));
        MenuItem rearVisualizationViewMI = new MenuItem(objDictionaryAction.getWord("VISUALIZATIONREARVIEW"));
        MenuItem flipVisualizationViewMI = new MenuItem(objDictionaryAction.getWord("VISUALIZATIONFLIPVIEW"));
        MenuItem frontCrossSectionViewMI = new MenuItem(objDictionaryAction.getWord("CROSSSECTIONFRONTVIEW"));
        MenuItem rearCrossSectionViewMI = new MenuItem(objDictionaryAction.getWord("CROSSSECTIONREARVIEW"));
        MenuItem gridViewMI = new MenuItem(objDictionaryAction.getWord("GRIDVIEW"));
        MenuItem graphViewMI = new MenuItem(objDictionaryAction.getWord("GRAPHVIEW"));
        MenuItem tilledViewMI = new MenuItem(objDictionaryAction.getWord("TILLEDVIEW"));
        MenuItem yarnSimulationViewMI = new MenuItem(objDictionaryAction.getWord("FABRIC")+" "+objDictionaryAction.getWord("SIMULATION"));
        //MenuItem simulationViewMI = new MenuItem(objDictionaryAction.getWord("SIMULATION"));
        MenuItem mappingViewMI = new MenuItem(objDictionaryAction.getWord("MAPPING"));
        MenuItem zoomInViewMI = new MenuItem(objDictionaryAction.getWord("ZOOMINVIEW"));
        MenuItem normalViewMI = new MenuItem(objDictionaryAction.getWord("ZOOMNORMALVIEW"));
        MenuItem zoomOutViewMI = new MenuItem(objDictionaryAction.getWord("ZOOMOUTVIEW"));
        
        frontSideViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F1, KeyCombination.CONTROL_DOWN));
        rearSideViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F2, KeyCombination.CONTROL_DOWN));
        frontVisualizationViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F4, KeyCombination.CONTROL_DOWN));
        rearVisualizationViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F5, KeyCombination.CONTROL_DOWN));
        flipVisualizationViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F6, KeyCombination.CONTROL_DOWN));
        frontCrossSectionViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F7, KeyCombination.CONTROL_DOWN));
        rearCrossSectionViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F8, KeyCombination.CONTROL_DOWN));
        gridViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F10, KeyCombination.CONTROL_DOWN));
        graphViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F11, KeyCombination.CONTROL_DOWN)); 
        tilledViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F12, KeyCombination.CONTROL_DOWN));
        yarnSimulationViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F4, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        //simulationViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F4, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        mappingViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F2, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        zoomInViewMI.setAccelerator(new KeyCodeCombination(KeyCode.PLUS, KeyCombination.CONTROL_DOWN));
        normalViewMI.setAccelerator(new KeyCodeCombination(KeyCode.ENTER, KeyCombination.CONTROL_DOWN));
        zoomOutViewMI.setAccelerator(new KeyCodeCombination(KeyCode.MINUS, KeyCombination.CONTROL_DOWN));
        
        frontSideViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_view.png"));
        rearSideViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_view.png"));
        frontVisualizationViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_visualization.png"));
        rearVisualizationViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_visualization.png"));
        flipVisualizationViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/flip_visualization.png"));
        frontCrossSectionViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_cut.png"));
        rearCrossSectionViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_cut.png"));
        gridViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/grid_view.png"));
        graphViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/graph_view.png"));
        tilledViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tiled_view.png"));
        yarnSimulationViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fabric_simulation.png"));
        //simulationViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/simulation.png"));
        mappingViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/mapping_view.png"));
        zoomInViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
        normalViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
        zoomOutViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
     
        //enable disable conditions
        if(!isWorkingMode){
            frontSideViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/front_view.png"));
            frontSideViewMI.setDisable(true);
            rearSideViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/rear_view.png"));
            rearSideViewMI.setDisable(true);
            frontVisualizationViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/front_visualization.png"));
            frontVisualizationViewMI.setDisable(true);
            rearVisualizationViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/rear_visualization.png"));
            rearVisualizationViewMI.setDisable(true);
            flipVisualizationViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/flip_visualization.png"));
            flipVisualizationViewMI.setDisable(true);
            frontCrossSectionViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/front_cut.png"));
            frontCrossSectionViewMI.setDisable(true);
            rearCrossSectionViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/rear_cut.png"));
            rearCrossSectionViewMI.setDisable(true);
            gridViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/grid_view.png"));
            gridViewMI.setDisable(true);
            graphViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/graph_view.png"));
            graphViewMI.setDisable(true);
            tilledViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/tiled_view.png"));
            tilledViewMI.setDisable(true);
            yarnSimulationViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/fabric_simulation.png"));
            yarnSimulationViewMI.setDisable(true);
            //simulationViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/simulation.png"));
            //simulationViewMI.setDisable(true);
            mappingViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/mapping_view.png"));
            mappingViewMI.setDisable(true);
            zoomInViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
            zoomInViewMI.setDisable(true);
            normalViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
            normalViewMI.setDisable(true);
            zoomOutViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
            zoomOutViewMI.setDisable(true);              
        }else{
            frontSideViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_view.png"));
            frontSideViewMI.setDisable(false);
            rearSideViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_view.png"));
            rearSideViewMI.setDisable(false);
            frontVisualizationViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_visualization.png"));
            frontVisualizationViewMI.setDisable(false);
            rearVisualizationViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_visualization.png"));
            rearVisualizationViewMI.setDisable(false);
            flipVisualizationViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/flip_visualization.png"));
            flipVisualizationViewMI.setDisable(false);          
            frontCrossSectionViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_cut.png"));
            frontCrossSectionViewMI.setDisable(false);
            rearCrossSectionViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_cut.png"));
            rearCrossSectionViewMI.setDisable(false);
            gridViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/grid_view.png"));
            gridViewMI.setDisable(false);
            graphViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/graph_view.png"));
            graphViewMI.setDisable(false);
            tilledViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tiled_view.png"));
            tilledViewMI.setDisable(false);
            yarnSimulationViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fabric_simulation.png"));
            yarnSimulationViewMI.setDisable(false);
            //simulationViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/simulation.png"));
            //simulationViewMI.setDisable(false);
            mappingViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/mapping_view.png"));
            mappingViewMI.setDisable(false);
            zoomInViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
            zoomInViewMI.setDisable(false);
            normalViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
            normalViewMI.setDisable(false);
            zoomOutViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
            zoomOutViewMI.setDisable(false);
        }
        //Add the action to Buttons.
        frontSideViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                frontSideViewAction();
            }
        });
        rearSideViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                rearSideViewAction();
            }
        });
        frontVisualizationViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                frontVisualizationViewAction();
            }
        });
        rearVisualizationViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                rearVisualizationViewAction();
            }
        });
        flipVisualizationViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                flipVisualizationViewAction();
            }
        });
        frontCrossSectionViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                frontCutViewAction();
            }
        });
        rearCrossSectionViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                rearCutViewAction();
            }
        });
        gridViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                gridViewAction();
            }
        });
        graphViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                graphViewAction();
            }
        });
        tilledViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                tilledViewAction();
            }
        });
        yarnSimulationViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                setMappingPixelView();
                plotYarnSimulationView();
            }
        });
        //simulationViewMI.setOnAction(new EventHandler<ActionEvent>() {
        //    @Override
        //    public void handle(ActionEvent e) {
        //        simulationViewAction();
        //    }
        //}); 
        mappingViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                mappingViewAction();
            }
        });
        zoomInViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                zoomInAction();
            }
        });
        normalViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                normalAction();
            }
        });
        zoomOutViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                zoomOutAction();
            }
        });
        //,simulationViewMI
        menuSelected.getItems().addAll(frontSideViewMI,rearSideViewMI,frontVisualizationViewMI,rearVisualizationViewMI,flipVisualizationViewMI,frontCrossSectionViewMI,rearCrossSectionViewMI,gridViewMI,graphViewMI,tilledViewMI,mappingViewMI,yarnSimulationViewMI,new SeparatorMenuItem(), zoomInViewMI,normalViewMI,zoomOutViewMI);
    }    
    private void populateViewToolbar(){
        // Front Texture View item;
        Button frontSideViewBtn = new Button();
        frontSideViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_view.png"));
        frontSideViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("FRONTSIDEVIEW")+" (Ctrl+F1)\n"+objDictionaryAction.getWord("TOOLTIPFRONTSIDEVIEW")));
        frontSideViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        frontSideViewBtn.getStyleClass().add("toolbar-button");    
        // Rear Texture View item
        Button rearSideViewBtn = new Button();
        rearSideViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_view.png"));
        rearSideViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("REARSIDEVIEW")+" (Ctrl+F2)\n"+objDictionaryAction.getWord("TOOLTIPREARSIDEVIEW")));
        rearSideViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rearSideViewBtn.getStyleClass().add("toolbar-button");    
        // Front Visulization View item;
        Button frontVisualizationViewBtn = new Button();
        frontVisualizationViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_visualization.png"));
        frontVisualizationViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("VISUALIZATIONFRONTVIEW")+" (Ctrl+F4)\n"+objDictionaryAction.getWord("TOOLTIPVISUALIZATIONFRONTVIEW")));
        frontVisualizationViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        frontVisualizationViewBtn.getStyleClass().add("toolbar-button");    
        // Rear Visaulization View item;
        Button rearVisualizationViewBtn = new Button();
        rearVisualizationViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_visualization.png"));
        rearVisualizationViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("VISUALIZATIONREARVIEW")+" (Ctrl+F5)\n"+objDictionaryAction.getWord("TOOLTIPVISUALIZATIONREARVIEW")));
        rearVisualizationViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rearVisualizationViewBtn.getStyleClass().add("toolbar-button");    
        // Switch Side View item;
        Button flipVisualizationViewBtn = new Button();
        flipVisualizationViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/flip_visualization.png"));
        flipVisualizationViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("VISUALIZATIONFLIPVIEW")+" (Ctrl+F6)\n"+objDictionaryAction.getWord("TOOLTIPVISUALIZATIONFLIPVIEW")));
        flipVisualizationViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        flipVisualizationViewBtn.getStyleClass().add("toolbar-button");    
        // cross Section View item;
        Button frontCrossSectionViewBtn = new Button();
        frontCrossSectionViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_cut.png"));
        frontCrossSectionViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CROSSSECTIONFRONTVIEW")+" (Ctrl+F7)\n"+objDictionaryAction.getWord("TOOLTIPCROSSSECTIONFRONTVIEW")));
        frontCrossSectionViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        frontCrossSectionViewBtn.getStyleClass().add("toolbar-button");    
        // cross Section View item;
        Button rearCrossSectionViewBtn = new Button();
        rearCrossSectionViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_cut.png"));
        rearCrossSectionViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CROSSSECTIONREARVIEW")+" (Ctrl+F8)\n"+objDictionaryAction.getWord("TOOLTIPCROSSSECTIONREARVIEW")));
        rearCrossSectionViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rearCrossSectionViewBtn.getStyleClass().add("toolbar-button");    
        // Grid View item
        Button gridViewBtn = new Button();
        gridViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/grid_view.png"));
        gridViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("GRIDVIEW")+" (Ctrl+F10)\n"+objDictionaryAction.getWord("TOOLTIPGRIDVIEW")));
        gridViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        gridViewBtn.getStyleClass().add("toolbar-button");    
        // Graph View item
        Button graphViewBtn = new Button();
        graphViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/graph_view.png"));
        graphViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("GRAPHVIEW")+" (Ctrl+F11)\n"+objDictionaryAction.getWord("TOOLTIPGRAPHVIEW")));
        graphViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        graphViewBtn.getStyleClass().add("toolbar-button");    
        // Tiled View
        Button tilledViewBtn = new Button();
        tilledViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tiled_view.png"));
        tilledViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("TILLEDVIEW")+" (Ctrl+F12)\n"+objDictionaryAction.getWord("TOOLTIPTILLEDVIEW")));
        tilledViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tilledViewBtn.getStyleClass().add("toolbar-button");
        // Yarn Simulation View menu
        Button yarnSimulationViewBtn = new Button();
        yarnSimulationViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fabric_simulation.png"));
        yarnSimulationViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("FABRIC")+" "+objDictionaryAction.getWord("SIMULATION")+" (Ctrl+Shift+F4)\n"+objDictionaryAction.getWord("TOOLTIPSIMULATION")));
        yarnSimulationViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        yarnSimulationViewBtn.getStyleClass().add("toolbar-button");
        // Simulation View menu
        //Button simulationViewBtn = new Button();
        //simulationViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/simulation.png"));
        //simulationViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SIMULATION")+" (Ctrl+Shift+F4)\n"+objDictionaryAction.getWord("TOOLTIPSIMULATION")));
        //simulationViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        //simulationViewBtn.getStyleClass().add("toolbar-button");    
        // mapping View menu
        Button mappingViewBtn = new Button();
        mappingViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/mapping_view.png"));
        mappingViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MAPPING")+" (Ctrl+Shift+F2)\n"+objDictionaryAction.getWord("TOOLTIPMAPPING")));
        mappingViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        mappingViewBtn.getStyleClass().add("toolbar-button");    
        // Zoom-In item
        Button zoomInViewBtn = new Button();
        zoomInViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
        zoomInViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ZOOMINVIEW")+" (Ctrl+Plus)\n"+objDictionaryAction.getWord("TOOLTIPZOOMINVIEW")));
        zoomInViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        zoomInViewBtn.getStyleClass().add("toolbar-button");    
        // Normal item
        Button normalViewBtn = new Button();
        normalViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
        normalViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ZOOMNORMALVIEW")+" (Ctrl+Enter)\n"+objDictionaryAction.getWord("TOOLTIPZOOMNORMALVIEW")));
        normalViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        normalViewBtn.getStyleClass().add("toolbar-button");    
        // Zoom-Out item
        Button zoomOutViewBtn = new Button();
        zoomOutViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
        zoomOutViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ZOOMOUTVIEW")+" (Ctrl+Minus)\n"+objDictionaryAction.getWord("TOOLTIPZOOMOUTVIEW")));
        zoomOutViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        zoomOutViewBtn.getStyleClass().add("toolbar-button");    
        //enable disable conditions
        if(!isWorkingMode){
            frontSideViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/front_view.png"));
            frontSideViewBtn.setDisable(true);
            frontSideViewBtn.setCursor(Cursor.WAIT);
            rearSideViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/rear_view.png"));
            rearSideViewBtn.setDisable(true);
            rearSideViewBtn.setCursor(Cursor.WAIT);
            frontVisualizationViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/front_visualization.png"));
            frontVisualizationViewBtn.setDisable(true);
            frontVisualizationViewBtn.setCursor(Cursor.WAIT);
            rearVisualizationViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/rear_visualization.png"));
            rearVisualizationViewBtn.setDisable(true);
            rearVisualizationViewBtn.setCursor(Cursor.WAIT);
            flipVisualizationViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/flip_visualization.png"));
            flipVisualizationViewBtn.setDisable(true);
            flipVisualizationViewBtn.setCursor(Cursor.WAIT);
            frontCrossSectionViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/front_cut.png"));
            frontCrossSectionViewBtn.setDisable(true);
            frontCrossSectionViewBtn.setCursor(Cursor.WAIT);
            rearCrossSectionViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/rear_cut.png"));
            rearCrossSectionViewBtn.setDisable(true);
            rearCrossSectionViewBtn.setCursor(Cursor.WAIT);
            gridViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/grid_view.png"));
            gridViewBtn.setDisable(true);
            gridViewBtn.setCursor(Cursor.WAIT);
            graphViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/graph_view.png"));
            graphViewBtn.setDisable(true);
            graphViewBtn.setCursor(Cursor.WAIT);
            tilledViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/tiled_view.png"));
            tilledViewBtn.setDisable(true);
            tilledViewBtn.setCursor(Cursor.WAIT);
            yarnSimulationViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/fabric_simulation.png"));
            yarnSimulationViewBtn.setDisable(true);
            yarnSimulationViewBtn.setCursor(Cursor.WAIT);
            //simulationViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/simulation.png"));
            //simulationViewBtn.setDisable(true);
            //simulationViewBtn.setCursor(Cursor.WAIT);
            mappingViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/mapping_view.png"));
            mappingViewBtn.setDisable(true);
            mappingViewBtn.setCursor(Cursor.WAIT);
            zoomInViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
            zoomInViewBtn.setDisable(true);
            zoomInViewBtn.setCursor(Cursor.WAIT);
            normalViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
            normalViewBtn.setDisable(true);
            normalViewBtn.setCursor(Cursor.WAIT);
            zoomOutViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
            zoomOutViewBtn.setDisable(true);
            zoomOutViewBtn.setCursor(Cursor.WAIT);                
        }else{
            frontSideViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_view.png"));
            frontSideViewBtn.setDisable(false);
            frontSideViewBtn.setCursor(Cursor.HAND);
            rearSideViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_view.png"));
            rearSideViewBtn.setDisable(false);
            rearSideViewBtn.setCursor(Cursor.HAND);
            frontVisualizationViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_visualization.png"));
            frontVisualizationViewBtn.setDisable(false);
            frontVisualizationViewBtn.setCursor(Cursor.HAND);
            rearVisualizationViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_visualization.png"));
            rearVisualizationViewBtn.setDisable(false);
            rearVisualizationViewBtn.setCursor(Cursor.HAND);
            flipVisualizationViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/flip_visualization.png"));
            flipVisualizationViewBtn.setDisable(false);
            flipVisualizationViewBtn.setCursor(Cursor.HAND);            
            frontCrossSectionViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_cut.png"));
            frontCrossSectionViewBtn.setDisable(false);
            frontCrossSectionViewBtn.setCursor(Cursor.HAND);
            rearCrossSectionViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_cut.png"));
            rearCrossSectionViewBtn.setDisable(false);
            rearCrossSectionViewBtn.setCursor(Cursor.HAND);
            gridViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/grid_view.png"));
            gridViewBtn.setDisable(false);
            gridViewBtn.setCursor(Cursor.HAND); 
            graphViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/graph_view.png"));
            graphViewBtn.setDisable(false);
            graphViewBtn.setCursor(Cursor.HAND);
            tilledViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tiled_view.png"));
            tilledViewBtn.setDisable(false);
            tilledViewBtn.setCursor(Cursor.HAND); 
            yarnSimulationViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fabric_simulation.png"));
            yarnSimulationViewBtn.setDisable(false);
            yarnSimulationViewBtn.setCursor(Cursor.HAND);
            //simulationViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/simulation.png"));
            //simulationViewBtn.setDisable(false);
            //simulationViewBtn.setCursor(Cursor.HAND);
            mappingViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/mapping_view.png"));
            mappingViewBtn.setDisable(false);
            mappingViewBtn.setCursor(Cursor.HAND);
            zoomInViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
            zoomInViewBtn.setDisable(false);
            zoomInViewBtn.setCursor(Cursor.HAND); 
            normalViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
            normalViewBtn.setDisable(false);
            normalViewBtn.setCursor(Cursor.HAND); 
            zoomOutViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
            zoomOutViewBtn.setDisable(false);
            zoomOutViewBtn.setCursor(Cursor.HAND); 
        }
        //Add the action to Buttons.
        frontSideViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                frontSideViewAction();
            }
        });
        rearSideViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rearSideViewAction();
            }
        });
        frontVisualizationViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                frontVisualizationViewAction();
            }
        });
        rearVisualizationViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rearVisualizationViewAction();
            }
        });
        flipVisualizationViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                flipVisualizationViewAction();
            }
        });
        frontCrossSectionViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                frontCutViewAction();
            }
        });
        rearCrossSectionViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rearCutViewAction();
            }
        });
        gridViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                gridViewAction();
            }
        });
        graphViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                graphViewAction();
            }
        });
        tilledViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                tilledViewAction();
            }
        });
        yarnSimulationViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                setMappingPixelView();
                plotYarnSimulationView();
            }
        });
        //simulationViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
        //    @Override
        //    public void handle(MouseEvent event) {
        //        simulationViewAction();
        //    }
        //}); 
        mappingViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mappingViewAction();
            }
        });
        zoomInViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                zoomInAction();
            }
        });
        normalViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                normalAction();
            }
        });
        zoomOutViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                zoomOutAction();
            }
        });
        //Create some Buttons.        
        FlowPane flow = new FlowPane();        
        flow.setPrefWrapLength(objConfiguration.WIDTH*.95);
        flow.getStyleClass().addAll("flow"); 
        //Add the Buttons to the ToolBar. ,simulationViewBtn
        flow.getChildren().addAll(frontSideViewBtn,rearSideViewBtn,frontVisualizationViewBtn,rearVisualizationViewBtn,flipVisualizationViewBtn,frontCrossSectionViewBtn,rearCrossSectionViewBtn,gridViewBtn,graphViewBtn,tilledViewBtn,mappingViewBtn,yarnSimulationViewBtn,zoomInViewBtn,normalViewBtn,zoomOutViewBtn);
        flow.setAlignment(Pos.TOP_LEFT);
        toolBar.getItems().addAll(flow);        
    }
  
    /**
     * frontSideViewAction
     * <p>
     * Function use for plotting front View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */    
    private void frontSideViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONFRONTSIDEVIEW"));
        plotViewActionMode = 1;
        plotViewAction();
    }
    /**
     * rearSideViewAction
     * <p>
     * Function use for plotting rear View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */    
    private void rearSideViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONREARSIDEVIEW"));
        plotViewActionMode = 2;
        plotViewAction();
    }
    /**
     * frontVisualizationViewAction
     * <p>
     * Function use for plotting front visualization View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void frontVisualizationViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONVISUALIZATIONFRONTVIEW"));
        plotViewActionMode = 4;
        plotViewAction();
    }
    /**
     * rearVisualizationViewAction
     * <p>
     * Function use for plotting rear visualization View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void rearVisualizationViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONVISUALIZATIONREARVIEW"));
        plotViewActionMode = 5;
        plotViewAction();
    }
    /**
     * flipVisualizationViewAction
     * <p>
     * Function use for plotting flip visualization View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void flipVisualizationViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONVISUALIZATIONFLIPVIEW"));
        plotViewActionMode = 6;
        plotViewAction();
    }
    /**
     * frontCutViewAction
     * <p>
     * Function use for plotting front cut View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void frontCutViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCROSSSECTIONFRONTVIEW"));
        plotViewActionMode = 7;
        plotViewAction();
    }
    /**
     * rearCutViewAction
     * <p>
     * Function use for plotting rear cut View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void rearCutViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCROSSSECTIONREARVIEW"));
        plotViewActionMode = 8;
        plotViewAction();
    }
    /**
     * gridViewAction
     * <p>
     * Function use for plotting grid View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void gridViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONGRIDVIEW"));
        plotViewActionMode = 10;
        plotViewAction();
    }
    /**
     * graphViewAction
     * <p>
     * Function use for plotting graph View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void graphViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONGRAPHVIEW"));
        plotViewActionMode = 11;
        plotViewAction();
    }
    /**
     * tilledViewAction
     * <p>
     * Function use for plotting tilled View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void tilledViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONTILLEDVIEW"));
        plotViewActionMode = 12;
        plotViewAction();
    }
    /**
     * plotViewAction
     * <p>
     * Function use for controlling different plots for menu item View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void plotViewAction(){
        //menuHighlight("VIEW");
        yarnSimulationMode = false;
        fabric.setEffect(null);
        isEditingMode = false;
        isWorkingMode = true;
        plotEditActionMode = 0;
        
        if(fabricChildStage!=null){
            fabricChildStage.close();            
            System.gc();
        }
        fabricChildStage = null;        
        
        if(plotViewActionMode==1) //front side
            plotFrontSideView();
        else if(plotViewActionMode==2) //rear side
            plotRearSideView();
        /*else if(plotViewActionMode==3)//flip side */ 
        else if(plotViewActionMode==4) // front visualization
            plotVisualizationFrontView();
        else if(plotViewActionMode==5) // rear visualization
            plotVisualizationRearView();
        else if(plotViewActionMode==6) // flip visualization
            plotVisualizationFlipView();
        else if(plotViewActionMode==7) // front cross scetion
            plotCrossSectionFrontView();
        else if(plotViewActionMode==8) // rear cross scetion
            plotCrossSectionRearView();
        /*else if(plotViewActionMode==9) // flip cross scetion*/
        else if(plotViewActionMode==10) //grid
            plotGridView();        
        else if(plotViewActionMode==11) //graph
            plotGraphView();
        else if(plotViewActionMode==12) //tilled
            plotTilledView();           
        else
            plotFrontSideView();
        //System.err.println("zoomfactor:"+zoomfactor+"boxsize:"+objConfiguration.getIntBoxSize()+"intWeft:"+objFabric.getIntWeft()+" intWarp:"+objFabric.getIntWarp()+" intVRepeat:"+editVerticalRepeatValue+" intHRepeat:"+editHorizontalRepeatValue+" weft diameter:"+objFabric.getIntEPI()+" Warp diameter:"+objFabric.getIntPPI());
        System.gc();
    }
    private void plotFrontSideView(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("GETFRONTSIDEVIEW"));
            int intHeight = objFabric.getIntWeft();
            int intLength = objFabric.getIntWarp();
            BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            bufferedImage = objFabricAction.plotFrontSideView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), intLength, intHeight);
            /*if((objFabric.getIntEPI()/objFabric.getIntPPI())<=0){
                intHeight = (int)(intLength*(objFabric.getIntPPI()/objFabric.getIntEPI())*zoomfactor);
                intLength = (int)(intLength*zoomfactor);
            }else{
                intLength = (int)(intHeight*(objFabric.getIntEPI()/objFabric.getIntPPI())*zoomfactor);
                intHeight = (int)(intHeight*zoomfactor);
            }*/
            intLength = (int)(((objConfiguration.getIntDPI()*objFabric.getIntWarp())/objFabric.getIntEPI())*zoomfactor);
            intHeight = (int)(((objConfiguration.getIntDPI()*objFabric.getIntWeft())/objFabric.getIntPPI())*zoomfactor);
            plotOrientation(bufferedImage, intLength, intHeight);
            bufferedImage = null;
            lblStatus.setText(objDictionaryAction.getWord("GOTFRONTSIDEVIEW"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"plotFrontSideView() : Error while viewing composte view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void plotRearSideView(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("GETREARSIDEVIEW"));
            int intHeight = objFabric.getIntWeft();
            int intLength = objFabric.getIntWarp();
            BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            bufferedImage = objFabricAction.plotRearSideView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), intLength, intHeight);
            /*if((objFabric.getIntEPI()/objFabric.getIntPPI())<=0){
                intHeight = (int)(intLength*(objFabric.getIntPPI()/objFabric.getIntEPI())*zoomfactor);
                intLength = (int)(intLength*zoomfactor);
            }else{
                intLength = (int)(intHeight*(objFabric.getIntEPI()/objFabric.getIntPPI())*zoomfactor);
                intHeight = (int)(intHeight*zoomfactor);
            }*/
            intLength = (int)(((objConfiguration.getIntDPI()*objFabric.getIntWarp())/objFabric.getIntEPI())*zoomfactor);
            intHeight = (int)(((objConfiguration.getIntDPI()*objFabric.getIntWeft())/objFabric.getIntPPI())*zoomfactor);
            plotOrientation(bufferedImage, intLength, intHeight);
            bufferedImage = null;
            lblStatus.setText(objDictionaryAction.getWord("GOTREARSIDEVIEW"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"plotRearSideView() : Error while viewing composte view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    
    private double getYarnDiameterInMm(Yarn objYarn){
        double dblYarnDiameterInMm = 0;
        try {
            FabricAction objFabricAction = new FabricAction(false);
            double dblYarnCountNeC = objFabricAction.convertUnit(objYarn.getStrYarnCountUnit(), "English Cotton (NeC)", objYarn.getIntYarnCount());
            //System.out.println("Count NeC: "+dblYarnCountNeC);
            if(dblYarnCountNeC>=1.0&&dblYarnCountNeC<=80.0){
                if(objYarn.getIntYarnPly()>=1&&objYarn.getIntYarnPly()<=6){
                    dblYarnDiameterInMm = 25.4/(28*Math.sqrt(dblYarnCountNeC/objYarn.getIntYarnPly()));
                }
                else
                    lblStatus.setText("Yarn Ply should be between 1 to 6.");
            }
            else
                lblStatus.setText("Yarn Count (NeC) should be between 1 to 80.");
        } catch (SQLException ex) {
            Logger.getLogger(FabricView.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dblYarnDiameterInMm;
    }
    
    private double getSpacePerWarpYarnInMm(boolean blnUseLoomParameters){
        if(blnUseLoomParameters)
            return (50.8/(double)(objFabric.getIntReedCount()*objFabric.getIntTPD()));
        else
            return (25.4/(double)objFabric.getIntEPI());
    }
    
    private double getSpacePerWeftYarnInMm(){
        return (25.4/(double)objFabric.getIntPPI());
    }
    
    private void setMappingPixelView(){
        Stage childStage = new Stage();
        childStage.initOwner(fabricStage);
        childStage.initStyle(StageStyle.UTILITY);
        final Label lblMapCaption = new Label("1 mm := selected Mapping pixels");
        final ComboBox mappingPixelValueCB = new ComboBox();
        for(int i=0; i<yarnGridSize.length; i++)
            mappingPixelValueCB.getItems().add(yarnGridSize[i]);
        mappingPixelValueCB.setValue(yarnGridSize[yarnGridSizeIndex]);
        mappingPixelValueCB.valueProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                if(t1!=null){
                    for(int i=0; i<yarnGridSize.length; i++)
                        if(yarnGridSize[i]==(int)t1)
                            yarnGridSizeIndex = i;
                    plotYarnSimulationView();
                }
            }
        });
        
        // adding light, color effects
        final ColorAdjust colorAdjust = new ColorAdjust();
        
        Label lblColorBrightness= new Label(objDictionaryAction.getWord("BRIGHTNESS")+" :");
        lblColorBrightness.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPBRIGHTNESS")));
        final Slider colorBrightnessSlider = new Slider(-1.0, 1.0, 0.0);    
        colorBrightnessSlider.setShowTickLabels(true);
        colorBrightnessSlider.setShowTickMarks(true);
        colorBrightnessSlider.setBlockIncrement(0.1);
        colorBrightnessSlider.setMajorTickUnit(0.5);
        colorBrightnessSlider.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov,
                Number old_val, Number new_val) {
                    colorAdjust.setBrightness(new_val.doubleValue());
            }
        });
        
        Label lblColorContrast= new Label(objDictionaryAction.getWord("CONTRAST")+" :");
        lblColorContrast.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCONTRAST")));
        final Slider colorContrastSlider = new Slider(-1.0, 1.0, 0.0);    
        colorContrastSlider.setShowTickLabels(true);
        colorContrastSlider.setShowTickMarks(true);
        colorContrastSlider.setBlockIncrement(0.1);
        colorContrastSlider.setMajorTickUnit(0.5);
        colorContrastSlider.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov,
                Number old_val, Number new_val) {
                    colorAdjust.setContrast(new_val.doubleValue());
            }
        });
        
        Label lblColorHue= new Label(objDictionaryAction.getWord("HUE")+" :");
        lblColorHue.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPHUE")));
        final Slider colorHueSlider = new Slider(-1.0, 1.0, 0.0);    
        colorHueSlider.setShowTickLabels(true);
        colorHueSlider.setShowTickMarks(true);
        colorHueSlider.setBlockIncrement(0.1);
        colorHueSlider.setMajorTickUnit(0.5);
        colorHueSlider.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov,
                Number old_val, Number new_val) {
                    colorAdjust.setHue(new_val.doubleValue());
            }
        });
        
        Label lblColorSaturation= new Label(objDictionaryAction.getWord("SATURATION")+" :");
        lblColorSaturation.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSATURATION")));
        final Slider colorSaturationSlider = new Slider(-1.0, 1.0, 0.0);    
        colorSaturationSlider.setShowTickLabels(true);
        colorSaturationSlider.setShowTickMarks(true);
        colorSaturationSlider.setBlockIncrement(0.1);
        colorSaturationSlider.setMajorTickUnit(0.5);
        colorSaturationSlider.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov,
                Number old_val, Number new_val) {
                    colorAdjust.setSaturation(new_val.doubleValue());
            }
        });
        // setting default Brightness, Contrast, Saturation
        colorBrightnessSlider.setValue(0.315);
        colorContrastSlider.setValue(0.315);
        colorSaturationSlider.setValue(0.315);
        fabric.setEffect(colorAdjust);
        
        Label lblSpaceColor= new Label("Empty Space Color :");
        // empty spacing color
        final ComboBox cmbSpaceColor = new ComboBox();
        cmbSpaceColor.getItems().addAll("#"+Integer.toHexString(java.awt.Color.BLACK.getRGB()).substring(2)
                , "#"+Integer.toHexString(java.awt.Color.DARK_GRAY.getRGB()).substring(2)
                , "#"+Integer.toHexString(java.awt.Color.LIGHT_GRAY.getRGB()).substring(2)
                , "#"+Integer.toHexString(java.awt.Color.WHITE.getRGB()).substring(2));
        cmbSpaceColor.setValue(cmbSpaceColor.getItems().get(0));
        cmbSpaceColor.setStyle("-fx-background-color: "+cmbSpaceColor.getValue().toString()+";");
        cmbSpaceColor.setCellFactory(new Callback<ListView<String>, ListCell<String>>(){
            @Override
            public ListCell<String> call(ListView<String> p) {
                return new ListCell<String>(){
                    @Override
                    protected void updateItem(String t, boolean bln) {
                        super.updateItem(t, bln);
                        setStyle("-fx-background-color: "+t+";");
                    }
                };
            }
        });
        cmbSpaceColor.valueProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                if(t1!=null){
                    emptySpaceColor = java.awt.Color.decode(t1.toString());
                    cmbSpaceColor.setStyle("-fx-background-color: "+cmbSpaceColor.getValue().toString()+";");
                    plotYarnSimulationView();
                }
            }
        });
        
        VBox vbox = new VBox(5);
        vbox.setAlignment(Pos.CENTER);
        vbox.setPadding(new Insets(25, 25, 25, 25));
        vbox.getChildren().addAll(lblMapCaption, mappingPixelValueCB, lblColorBrightness, colorBrightnessSlider, lblColorContrast, colorContrastSlider, lblColorHue, colorHueSlider, lblColorSaturation, colorSaturationSlider, lblSpaceColor, cmbSpaceColor);
        Scene popupScene = new Scene(vbox, 250, 400);
        popupScene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        childStage.setScene(popupScene);
        childStage.setTitle(objDictionaryAction.getWord("PROJECT"));
        childStage.show();
    }
    
    private void plotYarnSimulationView(){
        // print loom parameters values
        //System.out.println("Reed Count: "+objFabric.getIntReedCount());
        //System.out.println("TPD: "+objFabric.getIntTPD());
        //System.out.println("EPI: "+objFabric.getIntEPI());
        //System.out.println("PPI: "+objFabric.getIntPPI());
        //System.out.println("Warp: "+objFabric.getIntWarp()); // total number of warp threads
        //System.out.println("Weft: "+objFabric.getIntWeft()); // total number of weft threads
        //System.out.println("Extra Warp: "+objFabric.getIntExtraWarp()); // number of extra warp yarns used e.g. 0, 1, 2 etc.
        //System.out.println("Extra Weft: "+objFabric.getIntExtraWeft()); // number of extra weft yarns used e.g. 0, 1, 2 etc.
        
        // mapping 1 mm = p pixels
        int MAPPING_PIXEL = yarnGridSize[yarnGridSizeIndex];
        int emptySpace[] = new int[objFabric.getIntExtraWarp()+objFabric.getWarpYarn().length+objFabric.getIntExtraWeft()+objFabric.getWeftYarn().length];
        
        // space available per warp yarn
        //System.out.println("Space per Warp Yarn using Loom Parameters (in mm): "+getSpacePerWarpYarnInMm(true));
        //System.out.println("Space per Warp Yarn (in mm): "+getSpacePerWarpYarnInMm(false));
        int spaceAllocatedPerWarpYarn = (int)Math.round(getSpacePerWarpYarnInMm(false)*MAPPING_PIXEL);
        //System.out.println("Space per Warp Yarn (in pixels): "+spaceAllocatedPerWarpYarn);
        //System.out.println("Image Width: "+(spaceAllocatedPerWarpYarn*objFabric.getIntWarp()));
        
        // space available per weft yarn
        //System.out.println("Space per Weft Yarn (in mm): "+getSpacePerWeftYarnInMm());
        int spaceAllocatedPerWeftYarn = (int)Math.round(getSpacePerWeftYarnInMm()*MAPPING_PIXEL);
        //System.out.println("Space per Weft Yarn (in pixels): "+spaceAllocatedPerWeftYarn);
        //System.out.println("Image Heignt: "+(spaceAllocatedPerWeftYarn*objFabric.getIntWeft()));
        
        // check for lower bounds i.e. less than 1 pixel assigned to yarn
        if(spaceAllocatedPerWarpYarn<1 || spaceAllocatedPerWeftYarn<1){
            yarnGridSizeIndex++;
            new MessageView("error", "Increase Mapping Pixels", "Increase Mapping Pixels, space allocated per warp or weft yarn is 0.");
            lblStatus.setText("Increase Mapping Pixels, space allocated per warp or weft yarn is 0.");
            return;
        }
        
        // check for image size(h or w > 10000 pixels)
        if((spaceAllocatedPerWarpYarn*objFabric.getIntWarp())>10000 || (spaceAllocatedPerWeftYarn*objFabric.getIntWeft())>10000){
            yarnGridSizeIndex--;
            new MessageView("error", "Decrease Mapping Pixels", "Decrease Mapping Pixels, either Image Width or Height is too large.");
            //lblStatus.setText("Decrease Mapping Pixels, either Image Width or Height is too large.");
            return;
        }
        
        //System.out.println("\nWarp Yarns Information:");
        //System.out.println("Warp Yarn Length: "+objFabric.getWarpYarn().length);
        for(int wp=0; wp<objFabric.getWarpYarn().length; wp++){
            //System.out.println("Count: "+objFabric.getWarpYarn()[wp].getIntYarnCount());
            //System.out.println("Count Unit: "+objFabric.getWarpYarn()[wp].getStrYarnCountUnit());
            //System.out.println("Ply: "+objFabric.getWarpYarn()[wp].getIntYarnPly());
            //System.out.println("Diameter (New in mm): "+getYarnDiameterInMm(objFabric.getWarpYarn()[wp]));
            int diameter = (int)Math.round(getYarnDiameterInMm(objFabric.getWarpYarn()[wp])*MAPPING_PIXEL);
            if(diameter == 0){
                yarnGridSizeIndex++;
                new MessageView("error", "Warp Yarn Diameter is 0", "Diameter of Warp Yarn is 0 pixels, Update Yarn Count or Increase Mapping Pixels");
                return;
            }
            //System.out.println("Diameter (pixels): "+diameter);
            //System.out.println("Color: "+objFabric.getWarpYarn()[wp].getStrYarnColor());
            emptySpace[objFabric.getIntExtraWarp()+wp] = spaceAllocatedPerWarpYarn - diameter;
        }
        //System.out.println("\nWeft Yarns Information:");
        //System.out.println("Weft Yarn Length: "+objFabric.getWeftYarn().length);
        for(int wf=0; wf<objFabric.getWeftYarn().length; wf++){
            //System.out.println("Count: "+objFabric.getWeftYarn()[wf].getIntYarnCount());
            //System.out.println("Count Unit: "+objFabric.getWeftYarn()[wf].getStrYarnCountUnit());
            //System.out.println("Ply: "+objFabric.getWeftYarn()[wf].getIntYarnPly());
            //System.out.println("Diameter (New in mm): "+getYarnDiameterInMm(objFabric.getWeftYarn()[wf]));
            int diameter = (int)Math.round(getYarnDiameterInMm(objFabric.getWeftYarn()[wf])*MAPPING_PIXEL);
            if(diameter == 0){
                yarnGridSizeIndex++;
                new MessageView("error", "Weft Yarn Diameter is 0", "Diameter of Weft Yarn is 0 pixels, Update Yarn Count or Increase Mapping Pixels");
                return;
            }
            //System.out.println("Diameter (pixels): "+diameter);
            //System.out.println("Color: "+objFabric.getWeftYarn()[wf].getStrYarnColor());
            emptySpace[objFabric.getIntExtraWarp()+objFabric.getWarpYarn().length+objFabric.getIntExtraWeft()+wf] = spaceAllocatedPerWeftYarn - diameter;
        }
        //System.out.println("\nExtra Warp Yarns Information:");
        //System.out.println("Extra Warp Yarn Length: "+objFabric.getWarpExtraYarn().length);
        for(int ewp=0; ewp<objFabric.getIntExtraWarp(); ewp++){
            //System.out.println("Count: "+objFabric.getWarpExtraYarn()[ewp].getIntYarnCount());
            //System.out.println("Count Unit: "+objFabric.getWarpExtraYarn()[ewp].getStrYarnCountUnit());
            //System.out.println("Ply: "+objFabric.getWarpExtraYarn()[ewp].getIntYarnPly());
            //System.out.println("Diameter (New in mm): "+getYarnDiameterInMm(objFabric.getWarpExtraYarn()[ewp]));
            int diameter = (int)Math.round(getYarnDiameterInMm(objFabric.getWarpExtraYarn()[ewp])*MAPPING_PIXEL);
            if(diameter == 0){
                yarnGridSizeIndex++;
                new MessageView("error", "Extra Warp Yarn Diameter is 0", "Diameter of Extra Warp Yarn is 0 pixels, Update Yarn Count or Increase Mapping Pixels");
                return;
            }
            //System.out.println("Diameter (pixels): "+diameter);
            //System.out.println("Color: "+objFabric.getWarpExtraYarn()[ewp].getStrYarnColor());
            emptySpace[ewp] = spaceAllocatedPerWarpYarn - diameter;
        }
        //System.out.println("\nExtra Weft Yarns Information:");
        //System.out.println("Extra Weft Yarn Length: "+objFabric.getWeftExtraYarn().length);
        for(int ewf=0; ewf<objFabric.getIntExtraWeft(); ewf++){
            //System.out.println("Count: "+objFabric.getWeftExtraYarn()[ewf].getIntYarnCount());
            //System.out.println("Count Unit: "+objFabric.getWeftExtraYarn()[ewf].getStrYarnCountUnit());
            //System.out.println("Ply: "+objFabric.getWeftExtraYarn()[ewf].getIntYarnPly());
            //System.out.println("Diameter (New in mm): "+getYarnDiameterInMm(objFabric.getWeftExtraYarn()[ewf]));
            int diameter = (int)Math.round(getYarnDiameterInMm(objFabric.getWeftExtraYarn()[ewf])*MAPPING_PIXEL);
            if(diameter == 0){
                yarnGridSizeIndex++;
                new MessageView("error", "Extra Weft Yarn Diameter is 0", "Diameter of Extra Weft Yarn is 0 pixels, Update Yarn Count or Increase Mapping Pixels");
                return;
            }
            //System.out.println("Diameter (pixels): "+diameter);
            //System.out.println("Color: "+objFabric.getWeftExtraYarn()[ewf].getStrYarnColor());
            emptySpace[objFabric.getIntExtraWarp()+objFabric.getWarpYarn().length+ewf] = spaceAllocatedPerWeftYarn - diameter;
        }
        
        try{
            yarnSimulationMode = true;
            lblStatus.setText(objDictionaryAction.getWord("ACTIONSIMULATION"));
            int intHeight = objFabric.getIntWeft();
            int intLength = objFabric.getIntWarp();
            BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            //bufferedImage = objFabricAction.plotYarnSimulationView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), intLength, intHeight, yarnGridSize[yarnGridSizeIndex], yarnGridSize[yarnGridSizeIndex], yarnSpacing, "5_v_100.png", "5_h_100.png");
            boolean isRearView = (plotViewActionMode == 2) || (plotViewActionMode ==5); // if rear view/visualization was active, then show simulation for fabric rear
            bufferedImage = objFabricAction.plotYarnSimulationView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), intLength, intHeight, spaceAllocatedPerWarpYarn, spaceAllocatedPerWeftYarn, emptySpace, emptySpaceColor, isRearView);
            /*if((objFabric.getIntEPI()/objFabric.getIntPPI())<=0){
                intHeight = (int)(intLength*(objFabric.getIntPPI()/objFabric.getIntEPI())*zoomfactor*3);
                intLength = (int)(intLength*zoomfactor*3);
            }else{
                intLength = (int)(intHeight*(objFabric.getIntEPI()/objFabric.getIntPPI())*zoomfactor*3);
                intHeight = (int)(intHeight*zoomfactor*3);
            }*/
            intLength = bufferedImage.getWidth();//(int)(((objConfiguration.getIntDPI()*bufferedImage.getWidth())/objFabric.getIntEPI())*zoomfactor);
            intHeight = bufferedImage.getHeight();//(int)(((objConfiguration.getIntDPI()*bufferedImage.getHeight())/objFabric.getIntPPI())*zoomfactor);
            plotOrientation(bufferedImage, intLength, intHeight);
            bufferedImage = null;
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"plotYarnSimulationView() : Error while viewing visulization view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } 
    }
    private void plotVisualizationFrontView(){
        try{
            lblStatus.setText(objDictionaryAction.getWord("GETVISUALIZATIONFRONTVIEW"));
            int intHeight = objFabric.getIntWeft();
            int intLength = objFabric.getIntWarp();
            BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            bufferedImage = objFabricAction.plotVisualizationFrontView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), intLength, intHeight);
            /*if((objFabric.getIntEPI()/objFabric.getIntPPI())<=0){
                intHeight = (int)(intLength*(objFabric.getIntPPI()/objFabric.getIntEPI())*zoomfactor*3);
                intLength = (int)(intLength*zoomfactor*3);
            }else{
                intLength = (int)(intHeight*(objFabric.getIntEPI()/objFabric.getIntPPI())*zoomfactor*3);
                intHeight = (int)(intHeight*zoomfactor*3);
            }*/
            intLength = (int)(((objConfiguration.getIntDPI()*objFabric.getIntWarp())/objFabric.getIntEPI())*zoomfactor*3);
            intHeight = (int)(((objConfiguration.getIntDPI()*objFabric.getIntWeft())/objFabric.getIntPPI())*zoomfactor*3);
            plotOrientation(bufferedImage, intLength, intHeight);
            bufferedImage = null;
            lblStatus.setText(objDictionaryAction.getWord("GOTVISUALIZATIONFRONTVIEW"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"plotVisualizationFrontView() : Error while viewing visulization view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } 
    }
    private void plotVisualizationRearView(){
        try{
            lblStatus.setText(objDictionaryAction.getWord("GETVISUALIZATIONREARVIEW"));
            int intHeight = objFabric.getIntWeft();
            int intLength = objFabric.getIntWarp();
            BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            bufferedImage = objFabricAction.plotVisualizationRearView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), intLength, intHeight);
            /*if((objFabric.getIntEPI()/objFabric.getIntPPI())<=0){
                intHeight = (int)(intLength*(objFabric.getIntPPI()/objFabric.getIntEPI())*zoomfactor*3);
                intLength = (int)(intLength*zoomfactor*3);
            }else{
                intLength = (int)(intHeight*(objFabric.getIntEPI()/objFabric.getIntPPI())*zoomfactor*3);
                intHeight = (int)(intHeight*zoomfactor*3);
            }*/
            intLength = (int)(((objConfiguration.getIntDPI()*objFabric.getIntWarp())/objFabric.getIntEPI())*zoomfactor*3);
            intHeight = (int)(((objConfiguration.getIntDPI()*objFabric.getIntWeft())/objFabric.getIntPPI())*zoomfactor*3);
            plotOrientation(bufferedImage, intLength, intHeight);
            bufferedImage = null; 
            lblStatus.setText(objDictionaryAction.getWord("GOTVISUALIZATIONREARVIEW"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"plotVisualizationRearView() : Error while viewing flip view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }  
    }
    private void plotVisualizationFlipView(){
        try{
            lblStatus.setText(objDictionaryAction.getWord("GETVISUALIZATIONFLIPVIEW"));
            int intHeight = objFabric.getIntWeft();
            int intLength = objFabric.getIntWarp();
            BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            bufferedImage = objFabricAction.plotVisualizationFlipView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), intLength, intHeight);
            /*if((objFabric.getIntEPI()/objFabric.getIntPPI())<=0){
                intHeight = (int)(intLength*(objFabric.getIntPPI()/objFabric.getIntEPI())*zoomfactor*3);
                intLength = (int)(intLength*zoomfactor*3);
            }else{
                intLength = (int)(intHeight*(objFabric.getIntEPI()/objFabric.getIntPPI())*zoomfactor*3);
                intHeight = (int)(intHeight*zoomfactor*3);
            }*/
            intLength = (int)(((objConfiguration.getIntDPI()*objFabric.getIntWarp())/objFabric.getIntEPI())*zoomfactor*3);
            intHeight = (int)(((objConfiguration.getIntDPI()*objFabric.getIntWeft())/objFabric.getIntPPI())*zoomfactor*3);
            plotOrientation(bufferedImage, intLength, intHeight);
            bufferedImage = null;
            lblStatus.setText(objDictionaryAction.getWord("GOTVISUALIZATIONFLIPVIEW"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"plotVisualizationFlipView() : Error while viewing reverse view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } 
    }    
    private void plotCrossSectionFrontView(){
        try{
            lblStatus.setText(objDictionaryAction.getWord("GETCROSSSECTIONFRONTVIEW"));
            int intLength = objFabric.getIntWarp();
            int intHeight = objFabric.getIntWeft();            
            BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            byte[][] fabricMatrix = objFabricAction.getCrossSectionGraph(objFabric,false);
            intHeight = fabricMatrix.length;
            intLength = fabricMatrix[0].length;
            //replace -1 for extra weft by respective warp
            for(int i=0; i<intHeight; i++){
                boolean isUpdate = false;
                for(int j=0; j<intLength; j++){
                    if(fabricMatrix[i][j]>1) isUpdate=true;
                }
                if(isUpdate)
                    for(int j=0; j<intLength; j++)
                        if(fabricMatrix[i][j]<=1)
                           fabricMatrix[i][j]=1;                     
            }
            //replace -1 for extra warp by respective weft
            for(int j=0; j<intLength; j++){
                boolean isUpdate = false;
                for(int i=0; i<intHeight; i++){
                    if(fabricMatrix[i][j]<-1) isUpdate=true;
                }
                if(isUpdate)
                    for(int i=0; i<intHeight; i++)
                        if(fabricMatrix[i][j]>=-1)
                           fabricMatrix[i][j]=0;                     
            }
            /*//print the matrix
            System.out.println("Front Cross Section Fabric Matrix");
            for(int i=0; i<intHeight; i++){
                for(int j=0; j<intLength; j++){
                    System.err.print(" "+fabricMatrix[i][j]);
                }
                System.err.println();
            }*/
            
            bufferedImage = objFabricAction.plotCrossSectionView(objFabric, fabricMatrix, intLength, intHeight);
            intHeight = (int)(intHeight*zoomfactor*3);
            intLength = (int)(intLength*zoomfactor*3);
            plotOrientation(bufferedImage, intLength, intHeight);
            bufferedImage = null;
            fabricMatrix = null;
            System.gc();
            lblStatus.setText(objDictionaryAction.getWord("GETCROSSSECTIONFRONTVIEW"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"plotCrossSectionFrontView() : Error while viewing  cross section view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"plotCrossSectionFrontView() : Error while viewing  cross section view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void plotCrossSectionRearView(){
        try{
            lblStatus.setText(objDictionaryAction.getWord("GETCROSSSECTIONREARVIEW"));
            int intLength = objFabric.getIntWarp();
            int intHeight = objFabric.getIntWeft();            
            BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            byte[][] fabricMatrix = objFabricAction.getCrossSectionGraph(objFabric,false);
            intHeight = fabricMatrix.length;
            intLength = fabricMatrix[0].length;
            //invert base matrix
            byte[][] baseMatrix = objArtworkAction.repeatMatrix(objFabric.getBaseWeaveMatrix(),intHeight,intLength);
            baseMatrix=objArtworkAction.invertMatrix(baseMatrix);
            for(int i=0; i<intHeight; i++){
                for(int j=0; j<intLength; j++){
                    if(fabricMatrix[i][j]>1 || fabricMatrix[i][j]<0);
                    else
                        fabricMatrix[i][j] = baseMatrix[i][j];
                }
            }            
            //replace -1 for extra weft by respective warp
            for(int i=0; i<intHeight; i++){
                byte intRow = -1;
                for(int j=0; j<intLength; j++){
                    if(fabricMatrix[i][j]>1) intRow=fabricMatrix[i][j];
                }
                if(intRow>1){
                    for(int j=0; j<intLength; j++){
                        if(fabricMatrix[i][j]==intRow)
                           fabricMatrix[i][j]=1;
                        else
                            fabricMatrix[i][j]=intRow;
                    }
                }
            }
            //replace -1 for extra warp by respective weft
            for(int j=0; j<intLength; j++){
                byte intRow = -1;
                for(int i=0; i<intHeight; i++){
                    if(fabricMatrix[i][j]<-1) intRow=fabricMatrix[i][j];
                }
                if(intRow<-1){
                    for(int i=0; i<intHeight; i++){
                        if(fabricMatrix[i][j]==intRow)
                           fabricMatrix[i][j]=0;
                        else
                            fabricMatrix[i][j]=intRow;
                    }
                }
            }
            /*//print the matrix
            System.out.println("Rear Cross Section Fabric Matrix");
            for(int i=0; i<intHeight; i++){
                for(int j=0; j<intLength; j++){
                    System.err.print(" "+fabricMatrix[i][j]);
                }
                System.err.println();
            }*/
            
            bufferedImage = objFabricAction.plotCrossSectionView(objFabric, fabricMatrix, intLength, intHeight);
            intHeight = (int)(intHeight*zoomfactor*3);
            intLength = (int)(intLength*zoomfactor*3);
            plotOrientation(bufferedImage, intLength, intHeight);
            bufferedImage = null;
            fabricMatrix = null;
            System.gc();
            lblStatus.setText(objDictionaryAction.getWord("GETCROSSSECTIONREARVIEW"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"plotCrossSectionRearView() : Error while viewing  cross section view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"plotCrossSectionRearView() : Error while viewing cross section view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void plotGridView(){
        try {
            int intHeight=objFabric.getIntWeft();
            int intLength=objFabric.getIntWarp();
            lblStatus.setText(objDictionaryAction.getWord("GETGRIDVIEW"));
            BufferedImage bufferedImage = new BufferedImage(objFabric.getIntWarp(), objFabric.getIntWeft(),BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            
            if(objFabric.getIntExtraWarp()>0 || objFabric.getIntExtraWeft()>0){
                if(objConfiguration.getBlnPunchCard()){
                    bufferedImage = objFabricAction.plotGraphJaquardMachineView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                    intLength=objFabric.getIntWarp()*(objFabric.getIntExtraWarp()+objFabric.getIntExtraWeft()+2);
                } else{
                    bufferedImage = objFabricAction.plotGraphJaquardView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                }
            }else{
                bufferedImage = objFabricAction.plotGraphDobbyView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
            }
            
            //================================ For Outer to be square ===============================
            
            String[] data = {"10","10"};
            BufferedImage bufferedImageesize = new BufferedImage((int)(intLength*zoomfactor*Integer.parseInt(data[1])/graphFactor), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])/graphFactor),BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bufferedImageesize.createGraphics();
            g.drawImage(bufferedImage, 0, 0, (int)(intLength*zoomfactor*Integer.parseInt(data[1])/graphFactor), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])/graphFactor), null);
            g.setColor(java.awt.Color.BLACK);
            BasicStroke bs = new BasicStroke(1);
            g.setStroke(bs);
            
            for(int i = 0; i < objFabric.getIntWeft(); i++) {
                for(int j = 0; j < intLength; j++) {
                    g.drawLine((int)(j*zoomfactor*Integer.parseInt(data[1])/graphFactor), 0,  (int)(j*zoomfactor*Integer.parseInt(data[1])/graphFactor), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])/graphFactor));
                }
                g.drawLine(0, (int)(i*zoomfactor*Integer.parseInt(data[0])/graphFactor), (int)(intLength*zoomfactor)*Integer.parseInt(data[1])/graphFactor, (int)(i*zoomfactor*Integer.parseInt(data[0])/graphFactor));
            }
            g.drawLine(0, (int)((objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])/graphFactor)-1), (int)(intLength*zoomfactor)*Integer.parseInt(data[1])/graphFactor, (int)((objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])/graphFactor)-1));
            g.drawLine((int)((intLength*zoomfactor*Integer.parseInt(data[1])/graphFactor)-1), 0,  (int)((intLength*zoomfactor*Integer.parseInt(data[1])/graphFactor)-1), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])/graphFactor));
                
            g.dispose();
            bufferedImage = null;
            fabric.setImage(SwingFXUtils.toFXImage(bufferedImageesize, null));
            container.setContent(fabric);
            bufferedImageesize = null;
            lblStatus.setText(objDictionaryAction.getWord("GOTGRIDVIEW"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"plotGridView() : Error while viewing dobby garph view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void plotGraphView(){
        try {
            int intHeight=objFabric.getIntWeft();
            int intLength=objFabric.getIntWarp();
            lblStatus.setText(objDictionaryAction.getWord("GETGRAPHVIEW"));
            BufferedImage bufferedImage = new BufferedImage(objFabric.getIntWarp(), objFabric.getIntWeft(),BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            
            if(objFabric.getIntExtraWarp()>0 || objFabric.getIntExtraWeft()>0){
                if(objConfiguration.getBlnPunchCard()){
                    bufferedImage = objFabricAction.plotGraphJaquardMachineView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                    intLength=objFabric.getIntWarp()*(objFabric.getIntExtraWarp()+objFabric.getIntExtraWeft()+2);
                } else{
                    bufferedImage = objFabricAction.plotGraphJaquardView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                }
            }else{
                bufferedImage = objFabricAction.plotGraphDobbyView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
            }
            
            //================================ For Outer to be square ===============================
            
            String[] data = objFabric.getObjConfiguration().getStrGraphSize().split("x");
            BufferedImage bufferedImageesize = new BufferedImage((int)(intLength*zoomfactor*Integer.parseInt(data[1])/graphFactor), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])/graphFactor),BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bufferedImageesize.createGraphics();
            g.drawImage(bufferedImage, 0, 0, (int)(intLength*zoomfactor*Integer.parseInt(data[1])/graphFactor), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])/graphFactor), null);
            g.setColor(java.awt.Color.BLACK);
            BasicStroke bs = new BasicStroke(2);
            g.setStroke(bs);
            
            for(int i = 0; i < objFabric.getIntWeft(); i++) {
                for(int j = 0; j < intLength; j++) {
                    if((j%Integer.parseInt(data[0]))==0){
                        bs = new BasicStroke(2);
                        g.setStroke(bs);
                    }else{
                        bs = new BasicStroke(1);
                        g.setStroke(bs);
                    }
                    g.drawLine((int)(j*zoomfactor*Integer.parseInt(data[1])/graphFactor), 0,  (int)(j*zoomfactor*Integer.parseInt(data[1])/graphFactor), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])/graphFactor));
                }
                if((i%Integer.parseInt(data[1]))==0){
                    bs = new BasicStroke(2);
                    g.setStroke(bs);
                }else{
                    bs = new BasicStroke(1);
                    g.setStroke(bs);
                }
                g.drawLine(0, (int)(i*zoomfactor*Integer.parseInt(data[0])/graphFactor), (int)(intLength*zoomfactor)*Integer.parseInt(data[1])/graphFactor, (int)(i*zoomfactor*Integer.parseInt(data[0])/graphFactor));
            }
            bs = new BasicStroke(2);
            g.setStroke(bs);
            g.drawLine(0, (int)((objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])/graphFactor)-1), (int)(intLength*zoomfactor)*Integer.parseInt(data[1])/graphFactor, (int)((objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])/graphFactor)-1));
            g.drawLine((int)((intLength*zoomfactor*Integer.parseInt(data[1])/graphFactor)-1), 0,  (int)((intLength*zoomfactor*Integer.parseInt(data[1])/graphFactor)-1), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])/graphFactor));
                  
            g.dispose();
            bufferedImage = null;
            fabric.setImage(SwingFXUtils.toFXImage(bufferedImageesize, null));
            container.setContent(fabric);
            bufferedImageesize = null;
            lblStatus.setText(objDictionaryAction.getWord("GOTGRAPHVIEW"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"plotGraphDobbyView() : Error while viewing dobby garph view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void plotTilledView(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("GETTILLEDVIEW"));
            bodyContainer.getChildren().clear();
            BufferedImage bufferedImage = new BufferedImage(objFabric.getIntWarp()*3, objFabric.getIntWeft()*3,BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            bufferedImage = objFabricAction.plotTilledView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
            BufferedImage bufferedImageesize = new BufferedImage((int)(objFabric.getIntWarp()*zoomfactor*3), (int)(objFabric.getIntWeft()*zoomfactor*3),BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bufferedImageesize.createGraphics();
            g.drawImage(bufferedImage, 0, 0, (int)(objFabric.getIntWarp()*zoomfactor*3), (int)(objFabric.getIntWeft()*zoomfactor*3), null);
            g.dispose();
            bufferedImage = null;
            fabric.setImage(SwingFXUtils.toFXImage(bufferedImageesize, null));
            container.setContent(fabric);
            bufferedImageesize = null;
            lblStatus.setText(objDictionaryAction.getWord("GOTTILLEDVIEW"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"plotTilledView() : Error while viewing tilled view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    } 
    private void simulationViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONSIMULATION"));
        //plotViewActionMode = 13;
        //plotViewAction();
        try {
            int intHeight = objFabric.getIntWeft();
            int intLength = objFabric.getIntWarp();
            BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            bufferedImage = objFabricAction.plotYarnSimulationView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), intLength, intHeight, 3, 3, new int[objFabric.getIntExtraWarp()+objFabric.getWarpYarn().length+objFabric.getIntExtraWeft()+objFabric.getWeftYarn().length], emptySpaceColor, false);
            /*if((objFabric.getIntEPI()/objFabric.getIntPPI())<=0){
                intHeight = (int)(intLength*(objFabric.getIntPPI()/objFabric.getIntEPI())*zoomfactor);
                intLength = (int)(intLength*zoomfactor);
            }else{
                intLength = (int)(intHeight*(objFabric.getIntEPI()/objFabric.getIntPPI())*zoomfactor);
                intHeight = (int)(intHeight*zoomfactor);
            }*/
            intLength = (int)(((objConfiguration.getIntDPI()*bufferedImage.getWidth())/objFabric.getIntEPI())*zoomfactor);
            intHeight = (int)(((objConfiguration.getIntDPI()*bufferedImage.getHeight())/objFabric.getIntPPI())*zoomfactor);
            BufferedImage bufferedImageesize = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bufferedImageesize.createGraphics();
            g.drawImage(bufferedImage, 0, 0, intLength, intHeight, null);
            g.dispose();
            SimulatorEditView objBaseSimulationView = new SimulatorEditView(objConfiguration, bufferedImage); // bufferedImageeSize
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void mappingViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONMAPPING"));
        try {
            int intHeight = objFabric.getIntWeft();//(int)objFabric.getObjConfiguration().HEIGHT;
            int intLength = objFabric.getIntWarp();//(int)objFabric.getObjConfiguration().WIDTH;
            BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            bufferedImage = objFabricAction.plotFrontSideView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), intLength, intHeight);
            MappingEditView objMappingEditView = new MappingEditView(objConfiguration,bufferedImage);
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }    
    private void zoomInAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONZOOMINVIEW"));
        if(yarnSimulationMode){
            if(yarnGridSizeIndex==(yarnGridSize.length-1)){
                lblStatus.setText(objDictionaryAction.getWord("MAXZOOMIN"));
                return;
            }
            yarnGridSizeIndex = yarnGridSizeIndex==(yarnGridSize.length-1)?(yarnGridSize.length-1):++yarnGridSizeIndex;
            plotYarnSimulationView();
            return;
        }
        //fabric.setScaleX(zoomfactor*=2);
        //fabric.setScaleY(zoomfactor);
        //container.setContent(fabric);
        double nonGraph=zoomfactor*2*objFabric.getIntWarp()*objFabric.getIntWeft()*editVerticalRepeatValue*editHorizontalRepeatValue;
        double yesGraph=zoomfactor*2*objFabric.getIntWarp()*objFabric.getIntWeft();
        if(nonGraph<3333333 && yesGraph<3333333)//3840000.0
            //zoomfactor*=2;
            zoomfactor+=.5; //.1
        else
            lblStatus.setText(objDictionaryAction.getWord("MAXZOOMIN"));
        if(isEditingMode){
            if(plotEditActionMode==3)
                plotEditGraph();
            else if(plotEditActionMode==4)
                plotEditWeave();
            else
                plotViewAction();
        }else{
            plotViewAction();
        }
    }
    private void normalAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONZOOMNORMALVIEW"));
        if(yarnSimulationMode){
            yarnGridSizeIndex = 9; // points to element 10
            plotYarnSimulationView();
            return;
        }
        zoomfactor=1;
        if(isEditingMode){
            if(plotEditActionMode==3)
                plotEditGraph();
            else if(plotEditActionMode==4)
                plotEditWeave();
            else
                plotViewAction();
        }else{
            plotViewAction();
        }
    }
    private void zoomOutAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONZOOMOUTVIEW"));
        if(yarnSimulationMode){
            if(yarnGridSizeIndex==0){
                lblStatus.setText(objDictionaryAction.getWord("MAXZOOMOUT"));
                return;
            }
            yarnGridSizeIndex = yarnGridSizeIndex==0?0:--yarnGridSizeIndex;
            plotYarnSimulationView();
            return;
        }
        //fabric.setScaleX(zoomfactor/=2);
        //fabric.setScaleY(zoomfactor);
        double nonGraphWarp=zoomfactor*objFabric.getIntWarp()/2;
        double nonGraphWeft=zoomfactor*objFabric.getIntWeft()/2;
        double yesGraphWarp=zoomfactor*objFabric.getIntWarp()/2;
        double yesGraphWeft=zoomfactor*objFabric.getIntWeft()/2;
        if(nonGraphWarp>0 && nonGraphWeft>0 && yesGraphWarp>0 && yesGraphWeft>0 && zoomfactor>.5)
            //zoomfactor/=2;
            zoomfactor-=.5; //.1
        else
            lblStatus.setText(objDictionaryAction.getWord("MAXZOOMOUT"));
        if(isEditingMode){
            if(plotEditActionMode==3)
                plotEditGraph();
            else if(plotEditActionMode==4)
                plotEditWeave();
            else
                plotViewAction();
        }else{
            plotViewAction();
        }
    }
/**
 * populateUtilityToolbar
 * <p>
 * Function use for drawing tool bar for menu item Utility,
 * and binding events for each tools. 
 * 
 * @exception   (@throws SQLException)
 * @author      Amit Kumar Singh
 * @version     %I%, %G%
 * @since       1.0
 * @see         javafx.event.*;
 * @link        FabricView
 */  
    private void populateUtilityToolbarMenu(){ 
        //For drop down menus
        menuSelected.setTooltip(new Tooltip(objDictionaryAction.getWord("UTILITY")));
        
        MenuItem infoUtilityMI = new MenuItem(objDictionaryAction.getWord("INFOSETTINGSUTILITY"));
        MenuItem consumptionUtilityMI = new MenuItem(objDictionaryAction.getWord("CONSUMPTIONCALCULATIONUTILITY"));
        MenuItem priceUtilityMI = new MenuItem(objDictionaryAction.getWord("PRICECALCULATORUTILITY"));
        MenuItem weaveUtilityMI = new MenuItem(objDictionaryAction.getWord("WEAVEEDITOR"));
        MenuItem artworkUtilityMI = new MenuItem(objDictionaryAction.getWord("ARTWORKEDITOR"));
        MenuItem clothUtilityMI = new MenuItem(objDictionaryAction.getWord("CLOTHEDITOR"));
        MenuItem deviceUtilityMI = new MenuItem(objDictionaryAction.getWord("PUNCHAPPLICATION"));
        MenuItem cardViewMI = new MenuItem(objDictionaryAction.getWord("CARDVIEW"));
        
        infoUtilityMI.setAccelerator(new KeyCodeCombination(KeyCode.I, KeyCombination.SHIFT_DOWN));
        consumptionUtilityMI.setAccelerator(new KeyCodeCombination(KeyCode.Q, KeyCombination.SHIFT_DOWN));
        priceUtilityMI.setAccelerator(new KeyCodeCombination(KeyCode.R, KeyCombination.SHIFT_DOWN)); 
        weaveUtilityMI.setAccelerator(new KeyCodeCombination(KeyCode.W, KeyCombination.SHIFT_DOWN));
        artworkUtilityMI.setAccelerator(new KeyCodeCombination(KeyCode.D, KeyCombination.SHIFT_DOWN));
        clothUtilityMI.setAccelerator(new KeyCodeCombination(KeyCode.G, KeyCombination.SHIFT_DOWN));
        deviceUtilityMI.setAccelerator(new KeyCodeCombination(KeyCode.N, KeyCombination.SHIFT_DOWN));
        cardViewMI.setAccelerator(new KeyCodeCombination(KeyCode.V, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        
        infoUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/settings.png"));
        consumptionUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/consumption.png"));
        priceUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/price.png"));
        weaveUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/weave_editor.png"));
        artworkUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/artwork_editor.png"));
        clothUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_editor.png"));
        deviceUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/punch_card.png"));
        cardViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/card_view.png"));
     
        //add enable disable condition
        if(!isWorkingMode){
            infoUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/settings.png"));
            infoUtilityMI.setDisable(true);
            consumptionUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/consumption.png"));
            consumptionUtilityMI.setDisable(true);
            priceUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/price.png"));
            priceUtilityMI.setDisable(true);
            weaveUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/weave_editor.png"));
            weaveUtilityMI.setDisable(true);
            artworkUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/artwork_editor.png"));
            artworkUtilityMI.setDisable(true);
            clothUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/cloth_editor.png"));
            clothUtilityMI.setDisable(true);
            deviceUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/punch_card.png"));
            deviceUtilityMI.setDisable(true);  
            cardViewMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/card_view.png"));
            cardViewMI.setDisable(true);
        }else{
            infoUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/settings.png"));
            infoUtilityMI.setDisable(false);
            consumptionUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/consumption.png"));
            consumptionUtilityMI.setDisable(false);
            priceUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/price.png"));
            priceUtilityMI.setDisable(false);
            weaveUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/weave_editor.png"));
            weaveUtilityMI.setDisable(false);
            artworkUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/artwork_editor.png"));
            artworkUtilityMI.setDisable(false);
            clothUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_editor.png"));
            clothUtilityMI.setDisable(false);  
            deviceUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/punch_card.png"));
            deviceUtilityMI.setDisable(false);
            cardViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/card_view.png"));
            cardViewMI.setDisable(false);
        }
        //Add the action to Buttons.
        infoUtilityMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                fabricSettingAction();
            }
        });
        consumptionUtilityMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                consumptionCalculatorAction();
            }
        });
        priceUtilityMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                priceCalculatorAction();
            }
        });
        weaveUtilityMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                weaveUtilityAction();
            }
        });
        artworkUtilityMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                artworkUtilityAction();
            }
        });
        clothUtilityMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                clothUtilityAction();
            }
        });
        deviceUtilityMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                punchApplicationAction();
            }
        });
        cardViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                cardViewAction();
            }
        });
     
        menuSelected.getItems().addAll(infoUtilityMI, consumptionUtilityMI, priceUtilityMI, weaveUtilityMI, artworkUtilityMI, clothUtilityMI, deviceUtilityMI, cardViewMI);
    }            
    private void populateUtilityToolbar(){ 
        // fabric settings item
        Button infoUtilityBtn = new Button();
        infoUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/settings.png"));
        infoUtilityBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("INFOSETTINGSUTILITY")+" (Shift+I)\n"+objDictionaryAction.getWord("TOOLTIPINFOSETTINGSUTILITY")));
        infoUtilityBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        infoUtilityBtn.getStyleClass().add("toolbar-button");    
        // consumption Calculation item
        Button consumptionUtilityBtn = new Button();
        consumptionUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/consumption.png"));
        consumptionUtilityBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CONSUMPTIONCALCULATIONUTILITY")+" (Shift+Q)\n"+objDictionaryAction.getWord("TOOLTIPCONSUMPTIONCALCULATIONUTILITY")));
        consumptionUtilityBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        consumptionUtilityBtn.getStyleClass().add("toolbar-button");    
        // price Calculation item
        Button priceUtilityBtn = new Button();
        priceUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/price.png"));
        priceUtilityBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("PRICECALCULATORUTILITY")+" (Shift+R)\n"+objDictionaryAction.getWord("TOOLTIPPRICECALCULATORUTILITY")));
        priceUtilityBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        priceUtilityBtn.getStyleClass().add("toolbar-button");    
        // weave editor item
        Button weaveUtilityBtn = new Button();
        weaveUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/weave_editor.png"));
        weaveUtilityBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("WEAVEEDITOR")+" (Shift+W)\n"+objDictionaryAction.getWord("TOOLTIPWEAVEEDITOR")));
        weaveUtilityBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        weaveUtilityBtn.getStyleClass().add("toolbar-button");    
        // artwork editor item
        Button artworkUtilityBtn = new Button();
        artworkUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/artwork_editor.png"));
        artworkUtilityBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ARTWORKEDITOR")+" (Shift+D)\n"+objDictionaryAction.getWord("TOOLTIPARTWORKEDITOR")));
        artworkUtilityBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        artworkUtilityBtn.getStyleClass().add("toolbar-button");    
        // garment viewer File menu
        Button clothUtilityBtn = new Button();
        clothUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_editor.png"));
        clothUtilityBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CLOTHEDITOR")+" (Shift+G)\n"+objDictionaryAction.getWord("TOOLTIPCLOTHEDITOR")));
        clothUtilityBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        clothUtilityBtn.getStyleClass().add("toolbar-button");    
        // punch card File menu
        Button deviceUtilityBtn = new Button();
        deviceUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/punch_card.png"));
        deviceUtilityBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("PUNCHAPPLICATION")+" (Shift+N)\n"+objDictionaryAction.getWord("TOOLTIPPUNCHAPPLICATION")));
        deviceUtilityBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        deviceUtilityBtn.getStyleClass().add("toolbar-button");    
        // card view menu
        Button cardViewBtn = new Button();
        cardViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/card_view.png"));
        cardViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CARDVIEW")+" (Ctrl+Shift+V)\n"+objDictionaryAction.getWord("TOOLTIPCARDVIEW")));
        cardViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        cardViewBtn.getStyleClass().add("toolbar-button");
        //add enable disable condition
        if(!isWorkingMode){
            infoUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/settings.png"));
            infoUtilityBtn.setDisable(true);
            infoUtilityBtn.setCursor(Cursor.WAIT);
            consumptionUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/consumption.png"));
            consumptionUtilityBtn.setDisable(true);
            consumptionUtilityBtn.setCursor(Cursor.WAIT);
            priceUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/price.png"));
            priceUtilityBtn.setDisable(true);
            priceUtilityBtn.setCursor(Cursor.WAIT);
            weaveUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/weave_editor.png"));
            weaveUtilityBtn.setDisable(true);
            weaveUtilityBtn.setCursor(Cursor.WAIT);
            artworkUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/artwork_editor.png"));
            artworkUtilityBtn.setDisable(true);
            artworkUtilityBtn.setCursor(Cursor.WAIT);
            clothUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/cloth_editor.png"));
            clothUtilityBtn.setDisable(true);
            clothUtilityBtn.setCursor(Cursor.WAIT);
            deviceUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/punch_card.png"));
            deviceUtilityBtn.setDisable(true);  
            deviceUtilityBtn.setCursor(Cursor.WAIT);
            cardViewBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/card_view.png"));
            cardViewBtn.setDisable(true);  
            cardViewBtn.setCursor(Cursor.WAIT);
        }else{
            infoUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/settings.png"));
            infoUtilityBtn.setDisable(false);
            infoUtilityBtn.setCursor(Cursor.HAND);
            consumptionUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/consumption.png"));
            consumptionUtilityBtn.setDisable(false);
            consumptionUtilityBtn.setCursor(Cursor.HAND);
            priceUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/price.png"));
            priceUtilityBtn.setDisable(false);
            priceUtilityBtn.setCursor(Cursor.HAND);
            weaveUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/weave_editor.png"));
            weaveUtilityBtn.setDisable(false);
            weaveUtilityBtn.setCursor(Cursor.HAND);
            artworkUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/artwork_editor.png"));
            artworkUtilityBtn.setDisable(false);
            artworkUtilityBtn.setCursor(Cursor.HAND);
            clothUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_editor.png"));
            clothUtilityBtn.setDisable(false);  
            clothUtilityBtn.setCursor(Cursor.HAND);
            deviceUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/punch_card.png"));
            deviceUtilityBtn.setDisable(false);  
            deviceUtilityBtn.setCursor(Cursor.HAND);
            cardViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/card_view.png"));
            cardViewBtn.setDisable(false);  
            cardViewBtn.setCursor(Cursor.HAND);
         }
        //Add the action to Buttons.
        infoUtilityBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                fabricSettingAction();
            }
        });
        consumptionUtilityBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                consumptionCalculatorAction();
            }
        });
        priceUtilityBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                priceCalculatorAction();
            }
        });
        weaveUtilityBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                weaveUtilityAction();
            }
        });
        artworkUtilityBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                artworkUtilityAction();
            }
        });
        clothUtilityBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                clothUtilityAction();
            }
        });
        deviceUtilityBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                punchApplicationAction();
            }
        });
        cardViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                cardViewAction();
            }
        });
        //Create some Buttons. 
        FlowPane flow = new FlowPane();        
        flow.setPrefWrapLength(objConfiguration.WIDTH*.95); // preferred width allows for two columns
        flow.getStyleClass().addAll("flow");                
        //Add the Buttons to the ToolBar.
        flow.getChildren().addAll(infoUtilityBtn,consumptionUtilityBtn,priceUtilityBtn,weaveUtilityBtn,artworkUtilityBtn,clothUtilityBtn,deviceUtilityBtn,cardViewBtn);
        flow.setAlignment(Pos.TOP_LEFT);
        toolBar.getItems().addAll(flow);
    }
    private void fabricSettingAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONINFOSETTINGSUTILITY"));
        if(fabricChildStage!=null){
            fabricChildStage.close();
            fabricChildStage = null;
            System.gc();
        }        
        fabricChildStage = new Stage();
        fabricChildStage.initOwner(fabricStage);
        fabricChildStage.initStyle(StageStyle.UTILITY);
        fabricChildStage.initModality(Modality.APPLICATION_MODAL);
        //dialogStage.initModality(Modality.WINDOW_MODAL);
        
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setAlignment(Pos.CENTER);
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        
        objFabric.getObjConfiguration().setStrGraphSize(12+"x"+(int)((12*objFabric.getIntPPI())/objFabric.getIntEPI()));
        
        Label fabricDetails = new Label();
        fabricDetails.setText(objDictionaryAction.getWord("FABRICLENGTH")+"(inch):"+objFabric.getDblFabricLength()
                        +"\n"+objDictionaryAction.getWord("FABRICWIDTH")+"(inch):"+objFabric.getDblFabricWidth()
                        +"\n"+objDictionaryAction.getWord("REEDCOUNT")+":"+objFabric.getIntReedCount()
                        +"\n"+objDictionaryAction.getWord("DENTS")+":"+objFabric.getIntDents()
                        +"\n"+objDictionaryAction.getWord("TPD")+":"+objFabric.getIntTPD()
                        +"\n"+objDictionaryAction.getWord("HOOKS")+":"+objFabric.getIntHooks()
                        +"\n"+objDictionaryAction.getWord("ENDS")+":"+objFabric.getIntWarp()
                        +"\n"+objDictionaryAction.getWord("PICKS")+":"+objFabric.getIntWeft()
                        +"\n"+objDictionaryAction.getWord("SHAFT")+":"+objFabric.getIntShaft()
                        +"\n"+objDictionaryAction.getWord("HPI")+":"+objFabric.getIntHPI()
                        +"\n"+objDictionaryAction.getWord("EPI")+":"+objFabric.getIntEPI()
                        +"\n"+objDictionaryAction.getWord("PPI")+":"+objFabric.getIntPPI()
                        +"\n"+objDictionaryAction.getWord("GRAPHSIZE")+":"+objFabric.getObjConfiguration().getStrGraphSize());
        fabricDetails.setAlignment(Pos.TOP_LEFT);
        popup.add(fabricDetails, 0, 0);
       
        Scene scene = new Scene(popup, 300, 300, Color.WHITE);
        scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        fabricChildStage.setScene(scene);
        fabricChildStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("FABRICSETTINGS"));
        fabricChildStage.showAndWait();
        System.gc();
    }
    private void repeatOrientationAction(){
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setAlignment(Pos.CENTER);
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));

        final CheckBox rulerCB = new CheckBox(objDictionaryAction.getWord("RULERVIEW"));
        rulerCB.setSelected(objConfiguration.getBlnPunchCard());
        rulerCB.selectedProperty().addListener(new ChangeListener<Boolean>() {
           public void changed(ObservableValue<? extends Boolean> ov,
             Boolean old_val, Boolean new_val) {
             //objConfiguration.setBlnRuler(rulerCB.isSelected());
            lblStatus.setText(objDictionaryAction.getWord("ACTIONRULERVIEW"));
            hRuler = new Label();
            vRuler = new Label();

            bodyContainer.add(hRuler, 1, 0);
            bodyContainer.add(vRuler, 0, 1);
            //plotRulerLine();
             plotViewAction();
          }
        });
        popup.add(rulerCB, 0, 0, 2, 1);
        rulerCB.setDisable(true);
        
        final CheckBox punchCardCB = new CheckBox(objDictionaryAction.getWord("PUNCHCARDCB"));
        punchCardCB.setSelected(objConfiguration.getBlnPunchCard());
        punchCardCB.selectedProperty().addListener(new ChangeListener<Boolean>() {
           public void changed(ObservableValue<? extends Boolean> ov,
             Boolean old_val, Boolean new_val) {
             objConfiguration.setBlnPunchCard(punchCardCB.isSelected());
             plotViewAction();
          }
        });
        popup.add(punchCardCB, 0, 1, 2, 1);
        
        final CheckBox multipleRepeatCB = new CheckBox(objDictionaryAction.getWord("MREPEAT"));
        multipleRepeatCB.setSelected(objConfiguration.getBlnMRepeat());        
        multipleRepeatCB.setDisable(true);
        final GridPane repateGP = new GridPane();
        repateGP.setVisible(objConfiguration.getBlnMRepeat());
        multipleRepeatCB.selectedProperty().addListener(new ChangeListener<Boolean>() {
           public void changed(ObservableValue<? extends Boolean> ov,
             Boolean old_val, Boolean new_val) {
             objConfiguration.setBlnMRepeat(multipleRepeatCB.isSelected());
             repateGP.setVisible(objConfiguration.getBlnMRepeat());
             plotViewAction();
          }
        });
        popup.add(multipleRepeatCB, 0, 2, 2, 1);
       
        if(plotViewActionMode==10 || plotViewActionMode==11){
            punchCardCB.setDisable(false);
        }else{
            punchCardCB.setDisable(true);
        }
       
        
        
        if(plotViewActionMode==1 || plotViewActionMode==2  || plotViewActionMode==3
        || plotViewActionMode==4  || plotViewActionMode==5 || plotViewActionMode==6
        || plotViewActionMode==7  || plotViewActionMode==8 || plotViewActionMode==9){
            
            multipleRepeatCB.setDisable(false);
            repateGP.setVisible(objConfiguration.getBlnMRepeat());
            
            Separator sepHor1 = new Separator();
            sepHor1.setValignment(VPos.CENTER);
            GridPane.setConstraints(sepHor1, 0, 3);
            GridPane.setColumnSpan(sepHor1, 2);
            popup.getChildren().add(sepHor1);

            popup.add(repateGP, 0, 4, 2, 1);
            
            Label vertical= new Label(objDictionaryAction.getWord("VREPEAT")+" :");
            vertical.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPVREPEAT")));
            repateGP.add(vertical, 0, 4);
            final TextField verticalTF = new TextField(Integer.toString(editVerticalRepeatValue)){
                @Override public void replaceText(int start, int end, String text) {
                if (text.matches("[0-9]*")) {
                        super.replaceText(start, end, text);
                    }
                }
                @Override public void replaceSelection(String text) {
                if (text.matches("[0-9]*")) {
                        super.replaceSelection(text);
                    }
                }
            };
            verticalTF.setPromptText(objDictionaryAction.getWord("TOOLTIPVREPEAT"));
            verticalTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPVREPEAT")));
            repateGP.add(verticalTF, 1, 4);

            Label horizental= new Label(objDictionaryAction.getWord("HREPEAT")+" :");
            horizental.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPHREPEAT")));
            repateGP.add(horizental, 0, 5);
            final TextField horizentalTF = new TextField(Integer.toString(editHorizontalRepeatValue)){
                @Override public void replaceText(int start, int end, String text) {
                if (text.matches("[0-9]*")) {
                        super.replaceText(start, end, text);
                    }
                }
                @Override public void replaceSelection(String text) {
                if (text.matches("[0-9]*")) {
                        super.replaceSelection(text);
                    }
                }
            };
            horizentalTF.setPromptText(objDictionaryAction.getWord("TOOLTIPHREPEAT"));
            horizentalTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPHREPEAT")));
            repateGP.add(horizentalTF, 1, 5); 
/*
            Label lblRotationNew = new Label(objDictionaryAction.getWord("ROTATION"));
            lblRotationNew.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPROTATION")));
            popup.add(lblRotationNew, 0, 6);
            final ChoiceBox rotationCB=new ChoiceBox();
            rotationCB.getItems().add(0, "0");
            rotationCB.getItems().add(1, "90");
            rotationCB.getItems().add(2, "180");
            rotationCB.getItems().add(3, "270");
            rotationCB.setValue("0");
            popup.add(rotationCB, 1, 6);
*/
            Label lblRepeatMode=new Label(objDictionaryAction.getWord("REPEATMODE"));
            lblRepeatMode.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPREPEATMODE")));
            repateGP.add(lblRepeatMode, 0, 7);
            final ChoiceBox repeatModeCB=new ChoiceBox();
            repeatModeCB.getItems().add(0, "Rectangular (Default)");
            repeatModeCB.getItems().add(1, "1/2 Horizontal");
            repeatModeCB.getItems().add(2, "1/2 Vertical");
            repeatModeCB.getItems().add(3, "1/3 Horizontal");
            repeatModeCB.getItems().add(4, "1/3 Vertical");
            repeatModeCB.getItems().add(5, "1/4 Horizontal");
            repeatModeCB.getItems().add(6, "1/4 Vertical");
            repeatModeCB.getItems().add(7, "1/5 Horizontal");
            repeatModeCB.getItems().add(8, "1/5 Vertical");
            repeatModeCB.getItems().add(9, "1/6 Horizontal");
            repeatModeCB.getItems().add(10, "1/6 Vertical");
            repeatModeCB.setValue(editRepeatMode);
            repateGP.add(repeatModeCB, 1, 7);

            Button btnApply = new Button(objDictionaryAction.getWord("APPLY"));
            btnApply.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPAPPLY")));
            btnApply.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
            btnApply.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
            btnApply.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {  
                    lblStatus.setText(objDictionaryAction.getWord("Process")+" "+objDictionaryAction.getWord("Completed."));

                    editVerticalRepeatValue = Integer.parseInt(verticalTF.getText());
                    editHorizontalRepeatValue = Integer.parseInt(horizentalTF.getText());
                    editRepeatMode = repeatModeCB.getValue().toString();
                    //System.err.println(editVerticalRepeatValue+":"+editHorizontalRepeatValue+"="+editRepeatMode);
                    dialogStage.close();
                    System.gc();
                    plotViewAction();
                }
            });
            repateGP.add(btnApply, 0, 8);

            Button btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));
            btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
            btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            btnCancel.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
            btnCancel.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {  
                    lblStatus.setText(objDictionaryAction.getWord("TOOLTIPCANCEL"));
                    dialogStage.close();
                }
            });
            repateGP.add(btnCancel, 1, 8);
        }
        
        Scene scene = new Scene(popup);
        scene.getStylesheets().add(FabricView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        dialogStage.setScene(scene);
        dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("REPEATORIENTATION"));
        dialogStage.showAndWait();        
    }
    private void plotOrientation(BufferedImage bufferedImage, int intLength, int intHeight){
        if(intLength<=0||intHeight<=0)
            return;
        try {            
            BufferedImage bufferedImageesize = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bufferedImageesize.createGraphics();
            g.drawImage(bufferedImage, 0, 0, intLength, intHeight, null);
            g.dispose();
            bufferedImage = bufferedImageesize;
            if(objConfiguration.getBlnMRepeat()){
                if(editVerticalRepeatValue>0 && editHorizontalRepeatValue>0){
                //objArtworkAction = new ArtworkAction();
                //bufferedImageesize = objArtworkAction.getImageRepeat(bufferedImage, editVerticalRepeatValue, editHorizontalRepeatValue);
                } else {
                    editVerticalRepeatValue=1;
                    editHorizontalRepeatValue=1;
                }
                int vAling = 1;
                int hAling = 1;
                if(editRepeatMode.contains("Vertical")){
                    vAling = Integer.parseInt(editRepeatMode.substring(0, editRepeatMode.indexOf("/")).trim());
                    hAling = Integer.parseInt(editRepeatMode.substring(2, editRepeatMode.indexOf("Vertical")).trim());
                }else if(editRepeatMode.contains("Horizontal")){
                    hAling = Integer.parseInt(editRepeatMode.substring(0, editRepeatMode.indexOf("/")).trim());
                    vAling = Integer.parseInt(editRepeatMode.substring(2, editRepeatMode.indexOf("Horizontal")).trim());
                }else{
                    vAling = 1;
                    hAling = 1;
                }
                //System.err.println(editVerticalRepeatValue+":"+editHorizontalRepeatValue+"="+vAling+":"+hAling);
                    
                objArtworkAction = new ArtworkAction(false);
                bufferedImageesize = objArtworkAction.designRepeat(bufferedImageesize, vAling, hAling, editVerticalRepeatValue, editHorizontalRepeatValue);
            }
            bufferedImage = null;            
            fabric.setImage(SwingFXUtils.toFXImage(bufferedImageesize, null));
            container.setContent(fabric);
            bufferedImageesize = null;
        } catch (SQLException ex) {               
            new Logging("SEVERE",getClass().getName(),"Operation apply",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch(OutOfMemoryError ex){
            //undoAction();
            lblStatus.setText(objDictionaryAction.getWord("MAXVALUE"));
        }
        System.gc();
    }
    private void plotRulerLine(){ 
        NumberAxis axisX = new NumberAxis(0, 400, 25);
        /*
        NumberAxis axisY = new NumberAxis(0, 333, 25);
        axisY.setRotate(90);
        axisY.setScaleY(-1);
        container.add(axisY, 0, 7);
        */
        int intLength = objFabric.getIntWarp();
        int intHeight = objFabric.getIntWeft(); 
        int dpi = objConfiguration.getIntDPI();
        int warpFactor = dpi/objFabric.getIntEPI();
        int weftFactor = dpi/objFabric.getIntPPI();
        byte[][] baseWeaveMatrix = objFabric.getBaseWeaveMatrix();
        int warpCount = warpYarn.length;
        int weftCount = weftYarn.length;
        int j = baseWeaveMatrix[0].length;
        int i = baseWeaveMatrix.length;
        BufferedImage newImage = new BufferedImage(intLength, intHeight,BufferedImage.TYPE_INT_RGB);   
        for(int x = 0; x<intHeight; x++){
            int weft = x/weftFactor;
            if(x%weftFactor==0){
                for(int y = 0; y<intLength; y++){
                    int rgb = 0;
                    int warp = y/warpFactor;
                    if(y%warpFactor==0){
                        if(baseWeaveMatrix[weft%i][warp%j]==1)
                            rgb = new java.awt.Color((float)Color.web(warpYarn[warp%warpCount].getStrYarnColor()).getRed(),(float)Color.web(warpYarn[warp%warpCount].getStrYarnColor()).getGreen(),(float)Color.web(warpYarn[warp%warpCount].getStrYarnColor()).getBlue()).getRGB(); 
                        else
                            rgb = new java.awt.Color((float)Color.web(weftYarn[weft%weftCount].getStrYarnColor()).getRed(),(float)Color.web(weftYarn[weft%weftCount].getStrYarnColor()).getGreen(),(float)Color.web(weftYarn[weft%weftCount].getStrYarnColor()).getBlue()).getRGB(); 
                    }else{
                        rgb = new java.awt.Color((float)Color.LIGHTGRAY.getRed(),(float)Color.LIGHTGRAY.getGreen(),(float)Color.LIGHTGRAY.getBlue()).getRGB();
                    }
                    newImage.setRGB(y, x, rgb);
              }
            }else{
                for(int y = 0; y<intLength; y++){
                    int rgb = new java.awt.Color((float)Color.LIGHTGRAY.getRed(),(float)Color.LIGHTGRAY.getGreen(),(float)Color.LIGHTGRAY.getBlue()).getRGB();
                    newImage.setRGB(y, x, rgb);
              }
            }
        } 
        BufferedImage bufferedImageesize = new BufferedImage(intLength*(int)zoomfactor, intHeight*(int)zoomfactor,BufferedImage.TYPE_INT_RGB);        
        Graphics2D g = bufferedImageesize.createGraphics();
        g.drawImage(newImage, 0, 0, intLength*(int)zoomfactor, intHeight*(int)zoomfactor, null);
        g.dispose();
        
        fabric.setImage(SwingFXUtils.toFXImage(bufferedImageesize, null));
        container.setContent(fabric);        
    }
    private void consumptionCalculatorAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCONSUMPTIONCALCULATIONUTILITY"));
        ConsumptionView objConsumptionView = new ConsumptionView(objFabric); 
    }
    private void priceCalculatorAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONPRICECALCULATORUTILITY"));
        PriceView objPriceView = new PriceView(objFabric);
    }
    private void weaveUtilityAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONWEAVEEDITOR"));
        if(objFabric.getStrFabricID()!=null && objFabric.getStrClothType()!=null){
            objConfiguration.setStrRecentFabric(objFabric.getStrFabricID());
            objConfiguration.strWindowFlowContext="FabricEditor";
            fabricStage.close();
            if(objFabric.getStrBaseWeaveID()!=null)
                objConfiguration.setStrRecentWeave(objFabric.getStrBaseWeaveID());
            WeaveView objWeaveView = new WeaveView(objConfiguration);
        } else{
            new MessageView("error", objDictionaryAction.getWord("NOVALUE"), objDictionaryAction.getWord("UNSAVEDDATA"));
            lblStatus.setText(objDictionaryAction.getWord("UNSAVEDDATA"));
        }
    }
    private void artworkUtilityAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONARTWORKEDITOR"));
        if(objFabric.getStrFabricID()!=null && objFabric.getStrClothType()!=null){
            objConfiguration.setStrRecentFabric(objFabric.getStrFabricID());
            objConfiguration.strWindowFlowContext="FabricEditor";
            fabricStage.close();
            if(objFabric.getStrArtworkID()!=null)
                objConfiguration.setStrRecentArtwork(objFabric.getStrArtworkID());
            ArtworkView objArtworkView = new ArtworkView(objConfiguration);
        } else{
            new MessageView("error", objDictionaryAction.getWord("NOVALUE"), objDictionaryAction.getWord("UNSAVEDDATA"));
            lblStatus.setText(objDictionaryAction.getWord("UNSAVEDDATA"));
        }
    }
    private void clothUtilityAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCLOTHEDITOR"));
        if(objFabric.getStrFabricID()!=null && objFabric.getStrClothType()!=null){
            //fabricStage.setOpacity(.5);
            //fabricStage.setIconified(true);
            objConfiguration.setStrRecentFabric(objFabric.getStrFabricID());
            if(objFabric.getStrClothType().equalsIgnoreCase("Body")){
                if(Integer.parseInt(objConfiguration.getObjRecentCloth().mapClothFabricDetails.get("Body")[3])>0)
                    objConfiguration.getObjRecentCloth().mapClothFabricDetails.put("Body", new String[]{objFabric.getStrFabricID(),"90","Rectangular (Default)","1",Integer.toString(objFabric.getIntWarp()),Integer.toString(objFabric.getIntWeft())});
                objConfiguration.getObjRecentCloth().mapClothFabric.put("Body", objFabric);
            }
            else if(objFabric.getStrClothType().equalsIgnoreCase("Blouse")){
                if(Integer.parseInt(objConfiguration.getObjRecentCloth().mapClothFabricDetails.get("Blouse")[3])>0)
                    objConfiguration.getObjRecentCloth().mapClothFabricDetails.put("Blouse", new String[]{objFabric.getStrFabricID(),"90","Rectangular (Default)","1",Integer.toString(objFabric.getIntWarp()),Integer.toString(objFabric.getIntWeft())});
                objConfiguration.getObjRecentCloth().mapClothFabric.put("Blouse", objFabric);
            }
            else if(objFabric.getStrClothType().equalsIgnoreCase("Skirt")){
                if(Integer.parseInt(objConfiguration.getObjRecentCloth().mapClothFabricDetails.get("Skirt")[3])>0)
                    objConfiguration.getObjRecentCloth().mapClothFabricDetails.put("Skirt", new String[]{objFabric.getStrFabricID(),"90","Rectangular (Default)","1",Integer.toString(objFabric.getIntWarp()),Integer.toString(objFabric.getIntWeft())});
                objConfiguration.getObjRecentCloth().mapClothFabric.put("Skirt", objFabric);
            }
            else if(objFabric.getStrClothType().equalsIgnoreCase("Border")){
                boolean hasPart = false;
                if(Integer.parseInt(objConfiguration.getObjRecentCloth().mapClothFabricDetails.get("Left Border")[3])>0){
                    objConfiguration.getObjRecentCloth().mapClothFabric.put("Left Border", objFabric);
                    hasPart = true;
                }
                if(Integer.parseInt(objConfiguration.getObjRecentCloth().mapClothFabricDetails.get("Right Border")[3])>0){
                    objConfiguration.getObjRecentCloth().mapClothFabric.put("Right Border", objFabric);
                    hasPart = true;
                }
                if(!hasPart){
                    objConfiguration.getObjRecentCloth().mapClothFabricDetails.put("Left Border", new String[]{objFabric.getStrFabricID(),"90","Rectangular (Default)","1",Integer.toString(objFabric.getIntWarp()),Integer.toString(objFabric.getIntWeft())});
                    objConfiguration.getObjRecentCloth().mapClothFabric.put("Left Border", objFabric);
                    objConfiguration.getObjRecentCloth().mapClothFabricDetails.put("Right Border", new String[]{objFabric.getStrFabricID(),"90","Rectangular (Default)","1",Integer.toString(objFabric.getIntWarp()),Integer.toString(objFabric.getIntWeft())});
                    objConfiguration.getObjRecentCloth().mapClothFabric.put("Right Border", objFabric);
                }
            }
            else if(objFabric.getStrClothType().equalsIgnoreCase("Cross Border")){
                boolean hasPart = false;
                if(Integer.parseInt(objConfiguration.getObjRecentCloth().mapClothFabricDetails.get("Leftside Left Cross Border")[3])>0){
                    objConfiguration.getObjRecentCloth().mapClothFabric.put("Leftside Left Cross Border", objFabric);
                    hasPart = true;
                }
                if(Integer.parseInt(objConfiguration.getObjRecentCloth().mapClothFabricDetails.get("Leftside Right Cross Border")[3])>0){
                    objConfiguration.getObjRecentCloth().mapClothFabric.put("Leftside Right Cross Border", objFabric);
                    hasPart = true;
                }
                if(Integer.parseInt(objConfiguration.getObjRecentCloth().mapClothFabricDetails.get("Rightside Left Cross Border")[3])>0){
                    objConfiguration.getObjRecentCloth().mapClothFabric.put("Rightside Left Cross Border", objFabric);
                    hasPart = true;
                }
                if(Integer.parseInt(objConfiguration.getObjRecentCloth().mapClothFabricDetails.get("Rightside Right Cross Border")[3])>0){
                    objConfiguration.getObjRecentCloth().mapClothFabric.put("Rightside Right Cross Border", objFabric);
                    hasPart = true;
                }
                if(!hasPart){
                    objConfiguration.getObjRecentCloth().mapClothFabricDetails.put("Leftside Left Cross Border", new String[]{objFabric.getStrFabricID(),"90","Rectangular (Default)","1",Integer.toString(objFabric.getIntWarp()),Integer.toString(objFabric.getIntWeft())});
                    objConfiguration.getObjRecentCloth().mapClothFabric.put("Leftside Left Cross Border", objFabric);
                    objConfiguration.getObjRecentCloth().mapClothFabricDetails.put("Leftside Right Cross Border", new String[]{objFabric.getStrFabricID(),"90","Rectangular (Default)","1",Integer.toString(objFabric.getIntWarp()),Integer.toString(objFabric.getIntWeft())});
                    objConfiguration.getObjRecentCloth().mapClothFabric.put("Leftside Right Cross Border", objFabric);
                    //objConfiguration.getObjRecentCloth().mapClothFabricDetails.put("Rightside Left Cross Border", new String[]{objFabric.getStrFabricID(),"90","Rectangular (Default)","0",Integer.toString(objFabric.getIntWarp()),Integer.toString(objFabric.getIntWeft())});
                    //objConfiguration.getObjRecentCloth().mapClothFabric.put("Rightside Left Cross Border", objFabric);
                    //objConfiguration.getObjRecentCloth().mapClothFabricDetails.put("Rightside Right Cross Border", new String[]{objFabric.getStrFabricID(),"90","Rectangular (Default)","0",Integer.toString(objFabric.getIntWarp()),Integer.toString(objFabric.getIntWeft())});
                    //objConfiguration.getObjRecentCloth().mapClothFabric.put("Rightside Right Cross Border", objFabric);
                }
            }
            else if(objFabric.getStrClothType().equalsIgnoreCase("Pallu")){
                boolean hasPart = false;
                if(Integer.parseInt(objConfiguration.getObjRecentCloth().mapClothFabricDetails.get("Left Pallu")[3])>0){
                    objConfiguration.getObjRecentCloth().mapClothFabric.put("Left Pallu", objFabric);
                    hasPart = true;
                }
                if(Integer.parseInt(objConfiguration.getObjRecentCloth().mapClothFabricDetails.get("Right Pallu")[3])>0){
                    objConfiguration.getObjRecentCloth().mapClothFabric.put("Right Pallu", objFabric);
                    hasPart = true;
                }
                if(!hasPart){
                    //MAINFABRICID, ROTATIONANGLE, REPEATMODE, FABRICREPEAT, WIDTH, HEIGHT
                    objConfiguration.getObjRecentCloth().mapClothFabricDetails.put("Left Pallu", new String[]{objFabric.getStrFabricID(),"90","Rectangular (Default)","1",Integer.toString(objFabric.getIntWarp()),Integer.toString(objFabric.getIntWeft())});
                    objConfiguration.getObjRecentCloth().mapClothFabric.put("Left Pallu", objFabric);
                    //objConfiguration.getObjRecentCloth().mapClothFabricDetails.put("Right Pallu", new String[]{objFabric.getStrFabricID(),"90","Rectangular (Default)","0",Integer.toString(objFabric.getIntWarp()),Integer.toString(objFabric.getIntWeft())});
                    //objConfiguration.getObjRecentCloth().mapClothFabric.put("Right Pallu", objFabric);
                }
            }
            fabricStage.close();
            objConfiguration.strWindowFlowContext="FabricEditor";
            ClothView objClothView=new ClothView(objConfiguration);
        } else{
            new MessageView("error", objDictionaryAction.getWord("NOVALUE"), objDictionaryAction.getWord("UNSAVEDDATA"));
            lblStatus.setText(objDictionaryAction.getWord("UNSAVEDDATA"));
        }
    }
    private void punchApplicationAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONPUNCHAPPLICATION"));
        try {
            getDataMatrix();
            final Stage dialogStage = new Stage();
            dialogStage.initStyle(StageStyle.UTILITY);
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.setResizable(false);
            dialogStage.setIconified(false);
            dialogStage.setFullScreen(false);
            dialogStage.setTitle(objDictionaryAction.getWord("ALERT"));
            BorderPane root = new BorderPane();
            Scene scene = new Scene(root, 360, 180, Color.WHITE);
            scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
            GridPane popup=new GridPane();
            popup.setId("popup");
            popup.setHgap(5);
            popup.setVgap(10);
            popup.setPadding(new Insets(25, 25, 25, 25));
            
            popup.add(new Label(objDictionaryAction.getWord("SELECT")+" "+objDictionaryAction.getWord("PUNCHTYPE")), 0, 0);
            final ComboBox graphCB = new ComboBox();
            graphCB.getItems().add("Single Page-Single Graph");
            graphCB.setValue("Single Page-Single Graph");
            if(objFabric.getIntExtraWarp()>0 || objFabric.getIntExtraWeft()>0)
                graphCB.getItems().addAll("Multi Page-Single Graph","Multi Page-Multi Graph","Multi Page-Multi Graph (base)","Multi Page-Multi Graph (color base)","Superimposed Single Graph","Superimposed Single Graph (base)","Superimposed Single Graph (skip card)");
            popup.add(graphCB, 1, 0);
            
            popup.add(new Label(objDictionaryAction.getWord("SELECT")+" "+objDictionaryAction.getWord("PUNCHAPPLICATION")), 0, 1);
            final ComboBox deviceCB = new ComboBox();
            deviceCB.setValue("Select");
            
            UtilityAction objUtilityAction = new UtilityAction();
            Device objDevice = new Device(null, "Punching M/C", null, null);
            objDevice.setObjConfiguration(objConfiguration);
            Device[] devices = objUtilityAction.getDevices(objDevice);
            deviceCB.getItems().add("Select");
            for(int i=0; i<devices.length; i++)
                deviceCB.getItems().add(devices[i].getStrDeviceName());
            deviceCB.setValue("Select");
            popup.add(deviceCB, 1, 1);
            
            Label lblPassword=new Label(objDictionaryAction.getWord("SERVICEPASSWORD"));
            lblPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
            popup.add(lblPassword, 0, 2);
            final PasswordField passPF =new PasswordField();
            passPF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
            passPF.setPromptText(objDictionaryAction.getWord("PROMPTSERVICEPASSWORD"));
            popup.add(passPF, 1, 2);
            final Label lblAlert=new Label("");
            lblAlert.setStyle("-fx-wrap-text:true;");
            lblAlert.setPrefWidth(250);
            popup.add(lblAlert, 0, 4, 2, 1);
            
            Button btnYes = new Button(objDictionaryAction.getWord("RUN"));
            btnYes.setPrefWidth(50);
            btnYes.setId("btnYes");
            btnYes.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    if(Security.SecurePassword(passPF.getText(), objConfiguration.getObjUser().getStrUsername()).equals(objConfiguration.getObjUser().getStrAppPassword())){
                        try {
                            if(graphCB.getValue().toString().equalsIgnoreCase("Multi Page-Single Graph")){
                                getMultiPageSingleGraphMC();
                            } else if(graphCB.getValue().toString().equalsIgnoreCase("Multi Page-Multi Graph")){
                                getMultiPageMultiGraphMC();
                            } else if(graphCB.getValue().toString().equalsIgnoreCase("Multi Page-Multi Graph (base)")){
                                getMultiPageMultiGraphBaseMC();
                            } else if(graphCB.getValue().toString().equalsIgnoreCase("Multi Page-Multi Graph (color base)")){
                                getMultiPageMultiGraphColorBaseMC();
                            } else if(graphCB.getValue().toString().equalsIgnoreCase("Superimposed Single Graph")){
                                getSuperimposedGraphMC(false, false);
                            } else if(graphCB.getValue().toString().equalsIgnoreCase("Superimposed Single Graph (base)")){
                                getSuperimposedGraphMC(true, false);
                            } else if(graphCB.getValue().toString().equalsIgnoreCase("Superimposed Single Graph (skip card)")){
                                getSuperimposedGraphMC(false, true);
                            } else { //if(graphCB.getValue().toString().equalsIgnoreCase("Single Page-Single Graph"))
                                getSinglePageSingleGraphMC();
                            }
                            UtilityAction objUtilityAction = new UtilityAction();
                            Device objDevice = new Device(null, null, deviceCB.getValue().toString(), null);
                            objDevice.setObjConfiguration(objConfiguration);
                            objUtilityAction.getDevice(objDevice);
                            Runtime rt = Runtime.getRuntime();
                            if(objProcess_ProcessBuilder!=null)
                                objProcess_ProcessBuilder.destroy();
                            //objProcess_ProcessBuilder =new ProcessBuilder(objDevice.getDevicePath()).start();
                            //objProcess_ProcessBuilder=new ProcessBuilder(objDevice.getDevicePath(),filePath).start();
                            String exeDir=objDevice.getStrDevicePath().substring(0, objDevice.getStrDevicePath().lastIndexOf("\\")+1);
                            String exeName=objDevice.getStrDevicePath().substring(objDevice.getStrDevicePath().lastIndexOf("\\")+1,objDevice.getStrDevicePath().length());
                            File projDir = new File(exeDir);
                            Runtime.getRuntime().exec(new String[]{"cmd","/c",exeName}, null, projDir);
                        } catch (SQLException ex) {
                            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                        } catch (Exception ex) {
                            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                        }
                        dialogStage.close();
                        System.gc();
                    }
                    else if(passPF.getText().length()==0){
                        lblAlert.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                    }
                    else{
                        lblAlert.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                    }
                }
            });
            popup.add(btnYes, 0, 3);
            
            Button btnNo = new Button(objDictionaryAction.getWord("CANCEL"));
            btnNo.setPrefWidth(50);
            btnNo.setId("btnNo");
            btnNo.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    dialogStage.close();
                    System.gc();
                }
            });
            popup.add(btnNo, 1, 3);
            
            root.setCenter(popup);
            dialogStage.setScene(scene);
            dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("SELECTTO")+" "+objDictionaryAction.getWord("RUN")+" "+objDictionaryAction.getWord("PUNCHAPPLICATION"));
            dialogStage.showAndWait();
            
            lblStatus.setText(objDictionaryAction.getWord("ACTIONPUNCHAPPLICATION"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    
    /**
     * Card View for transferring BMP output compatible with electronic jacquard
     * Input required is Split Graph
     * @since 0.9.7
     */
    private void cardViewAction(){
        try {
            BufferedImage graphImage;
            objFabricAction = new FabricAction();
            byte[][] fabricMatrix = objFabricAction.getCrossSectionGraph(objFabric, false); // isSkipBase = false
            graphImage = objFabricAction.plotCrossSectionGraphView(objFabric, fabricMatrix, true); // showBase = true
            System.gc();
            CardView objCardView = new CardView(graphImage, objConfiguration);
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    
    private void getDataMatrix(){
        try {
            Yarn[] weftYarn = objFabric.getWeftYarn();
            Yarn[] warpYarn = objFabric.getWarpYarn();
            Yarn[] weftExtraYarn = objFabric.getWeftExtraYarn();
            Yarn[] warpExtraYarn = objFabric.getWarpExtraYarn();
            
            int warpCount = warpYarn.length;
            int weftCount = weftYarn.length;
            int weftExtraCount = objFabric.getIntExtraWeft();
            int warpExtraCount = objFabric.getIntExtraWarp();
        
            /*//print weft yarn sequence
            System.out.println("=====>print weft yarn sequence<=====");
            for(int x = 0; x<weftCount; x++){
                System.out.println("Code 0 ="+weftYarn[x].getStrYarnColor()+"\t rgb("+(float)javafx.scene.paint.Color.web(weftYarn[x].getStrYarnColor()).getRed()+","+(float)javafx.scene.paint.Color.web(weftYarn[x].getStrYarnColor()).getGreen()+","+(float)javafx.scene.paint.Color.web(weftYarn[x].getStrYarnColor()).getBlue()+")");
            }    
            //print warp yarn sequence
            System.out.println("=====>print warp yarn sequence<=====");
            for(int x = 0; x<warpCount; x++){
                System.out.println("Code 1 ="+warpYarn[x].getStrYarnColor()+"\t rgb("+(float)javafx.scene.paint.Color.web(warpYarn[x].getStrYarnColor()).getRed()+","+(float)javafx.scene.paint.Color.web(warpYarn[x].getStrYarnColor()).getGreen()+","+(float)javafx.scene.paint.Color.web(warpYarn[x].getStrYarnColor()).getBlue()+")");
            }
            //print extra weft yarn sequence
            System.out.println("=====>print extra weft yarn sequence<=====");
            for(int x = 0; x<weftExtraCount; x++){
                System.out.println("Code "+(x+2)+" ="+weftExtraYarn[x].getStrYarnColor()+"\t rgb("+(float)javafx.scene.paint.Color.web(weftExtraYarn[x].getStrYarnColor()).getRed()+","+(float)javafx.scene.paint.Color.web(weftExtraYarn[x].getStrYarnColor()).getGreen()+","+(float)javafx.scene.paint.Color.web(weftExtraYarn[x].getStrYarnColor()).getBlue()+")");
            }
            //print extra warp yarn sequence
            System.out.println("=====>print extra warp yarn sequence<=====");
            for(int x = 0; x<warpExtraCount; x++){
                System.out.println("Code - ="+warpExtraYarn[x].getStrYarnColor()+"\t rgb("+(float)javafx.scene.paint.Color.web(warpExtraYarn[x].getStrYarnColor()).getRed()+","+(float)javafx.scene.paint.Color.web(warpExtraYarn[x].getStrYarnColor()).getGreen()+","+(float)javafx.scene.paint.Color.web(warpExtraYarn[x].getStrYarnColor()).getBlue()+")");
            }
            */
            
            //print front side matrix
            //System.out.println("=====>print front side matrix<=====");
            int intLength = objFabric.getIntWarp();
            int intHeight = objFabric.getIntWeft();
            byte[][] repeatMatrix = objFabric.getFabricMatrix();
            /*for(int x = 0; x < intHeight; x++) {
                for(int y = 0; y < intLength; y++) {
                    System.out.print("\t"+repeatMatrix[x][y]);
                }
                System.out.println("");
            }*/
            //print back side matrix
            //System.out.println("=====>print back side matrix<=====");
            intLength = objFabric.getIntWarp();
            intHeight = objFabric.getIntWeft();
            repeatMatrix = objFabric.getReverseMatrix();
            /*for(int x = 0; x < intHeight; x++) {
                for(int y = 0; y < intLength; y++) {
                    System.out.print("\t"+repeatMatrix[x][y]);
                }
                System.out.println("");
            }*/
            //print left side matrix <- 
            //System.out.println("=====>print left side matrix<=====");
            intLength = 2;
            intHeight = objFabric.getIntWeft();
            repeatMatrix = new byte[intHeight][intLength];
            for(int x = 0, z = 0; x < intHeight; x++) {
                repeatMatrix[x][0]=objFabric.getFabricMatrix()[x][z];
                repeatMatrix[x][1]=objFabric.getReverseMatrix()[x][z];
                /*for(int y = 0; y < intLength; y++) {
                    System.out.print("\t"+repeatMatrix[x][y]);
                }
                System.out.println("");*/
            }
            
            //print right side matrix ->
            //System.out.println("=====>print right side matrix<=====");
            intLength = 2;
            intHeight = objFabric.getIntWeft();
            repeatMatrix = new byte[intHeight][intLength];
            for(int x = 0, z = objFabric.getIntWarp()-1; x < intHeight; x++) {
                repeatMatrix[x][0]=objFabric.getFabricMatrix()[x][z];
                repeatMatrix[x][1]=objFabric.getReverseMatrix()[x][z];
                /*for(int y = 0; y < intLength; y++) {
                    System.out.print("\t"+repeatMatrix[x][y]);
                }
                System.out.println("");*/
            }
            //print top side matrix
            //System.out.println("=====>print top side matrix<=====");
            intLength = objFabric.getIntWarp();
            intHeight = 2;
            repeatMatrix = new byte[intHeight][intLength];
            for(int y = 0, z = 0; y < intLength; y++) {
                repeatMatrix[0][y]=objFabric.getFabricMatrix()[z][y];
                repeatMatrix[1][y]=objFabric.getReverseMatrix()[z][y];
            }
            /*for(int x = 0; x < intHeight; x++) {
                for(int y = 0; y < intLength; y++) {
                    System.out.print("\t"+repeatMatrix[x][y]);
                }
                System.out.println("");
            }*/
            //print bottom side matrix
            //System.out.println("=====>print bottom side matrix<=====");
            intLength = objFabric.getIntWarp();
            intHeight = 2;
            repeatMatrix = new byte[intHeight][intLength];
            for(int y = 0, z = objFabric.getIntWeft()-1; y < intLength; y++) {
                repeatMatrix[0][y]=objFabric.getFabricMatrix()[z][y];
                repeatMatrix[1][y]=objFabric.getReverseMatrix()[z][y];
            }
            /*for(int x = 0; x < intHeight; x++) {
                for(int y = 0; y < intLength; y++) {
                    System.out.print("\t"+repeatMatrix[x][y]);
                }
                System.out.println("");
            }*/
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private static void recursiveDelete(File file) {
        //to end the recursive loop
        if (!file.exists())
            return;         
        //if directory, go inside and call recursively
        if (file.isDirectory()) {
            for (File f : file.listFiles()) {
                //call recursively
                recursiveDelete(f);
            }
        }
        //call delete to delete files and empty directory
        file.delete();
        //System.out.println("Deleted file/folder: "+file.getAbsolutePath());
    }
    private void getSinglePageSingleGraphMC(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("GETGRAPHVIEW"));
            BufferedImage bufferedImage = new BufferedImage(objFabric.getIntWeft(), objFabric.getIntWarp(),BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            
            if(objFabric.getIntExtraWarp()>0 || objFabric.getIntExtraWeft()>0){
                if(objConfiguration.getBlnPunchCard()){
                    bufferedImage = objFabricAction.plotGraphJaquardMachineView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                } else{
                    bufferedImage = objFabricAction.plotGraphJaquardView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                }
            }else{
                bufferedImage = objFabricAction.plotGraphDobbyView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
            }
            FileChooser fileChoser=new FileChooser();
            fileChoser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTBMP")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
            FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("BMP (.bmp)", "*.bmp");
            fileChoser.getExtensionFilters().add(extFilterBMP);
            File file=fileChoser.showSaveDialog(fabricStage);
            if(file==null)
                return;
            else
                fabricStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+file.getAbsoluteFile().getName()+"]");
            if (!file.getName().endsWith("bmp"))
                file = new File(file.getPath() +".bmp");
            ImageIO.write(bufferedImage, "BMP", file);
            bufferedImage = null;
            
            if(objConfiguration.getBlnAuthenticateService()){
                ArrayList<File> filesToZip=new ArrayList<>();
                filesToZip.add(file);
                String zipFilePath=file.getAbsolutePath()+".zip";
                String passwordToZip = file.getName();
                new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                file.delete();
            }
                
            lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+file.getCanonicalPath());
        } catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void getMultiPageSingleGraphMC(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("GETGRAPHVIEW"));
            BufferedImage bufferedImage = new BufferedImage(objFabric.getIntWeft(), objFabric.getIntWarp(),BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            bufferedImage = objFabricAction.plotGraphJaquardMachineView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
            
            FileChooser fileChoser=new FileChooser();
            fileChoser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTBMP")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
            FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("BMP (.bmp)", "*.bmp");
            fileChoser.getExtensionFilters().add(extFilterBMP);
            File file=fileChoser.showSaveDialog(fabricStage);
            if(file==null)
                return;
            else
                fabricStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+file.getAbsoluteFile().getName()+"]");
            if (!file.getName().endsWith("bmp"))
                file = new File(file.getPath() +".bmp");
            ImageIO.write(bufferedImage, "BMP", file);
            bufferedImage = null;
            
            if(objConfiguration.getBlnAuthenticateService()){
                ArrayList<File> filesToZip=new ArrayList<>();
                filesToZip.add(file);
                String zipFilePath=file.getAbsolutePath()+".zip";
                String passwordToZip = file.getName();
                new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                file.delete();
            }

            lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+file.getCanonicalPath());
        } catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }    
    private void getMultiPageMultiGraphMC(){
        try {
            DirectoryChooser directoryChooser=new DirectoryChooser();
            fabricStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTBMP")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
            File selectedDirectory = directoryChooser.showDialog(fabricStage);
            if(selectedDirectory == null)
                return;
            else
                fabricStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+selectedDirectory.getAbsolutePath()+"]");
            
            lblStatus.setText(objDictionaryAction.getWord("GETGRAPHVIEW"));
            BufferedImage bufferedImage = new BufferedImage(objFabric.getIntWarp(), objFabric.getIntWeft(),BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
            Date date = new Date();
            String currentDate = dateFormat.format(date);
            String path = selectedDirectory.getPath()+"\\Fabric_"+currentDate+"\\";
            File file = new File(path);
            
            ArrayList<File> filesToZip=new ArrayList<>();
            
            if (!file.exists()) {
                if (!file.mkdir())
                    path = selectedDirectory.getPath();
            }
            for(int i=0; i<objFabric.getIntExtraWeft(); i++){
                bufferedImage = objFabricAction.plotJaquardGraphView(objFabric, i);
                file = new File(path + i +".bmp");
                ImageIO.write(bufferedImage, "BMP", file);
                filesToZip.add(file);
            }
            for(int i=0; i<objFabric.getIntExtraWarp(); i++){
                bufferedImage = objFabricAction.plotJaquardGraphView(objFabric, i);
                file = new File(path + i +".bmp");
                ImageIO.write(bufferedImage, "BMP", file);
                filesToZip.add(file);
            }
            if(objConfiguration.getBlnAuthenticateService()){
                String zipFilePath=selectedDirectory.getPath()+"\\Fabric_"+currentDate+".zip";
                String passwordToZip = "Fabric_"+currentDate;
                new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                //delete folder recursively
                recursiveDelete(new File(path));
            }
            bufferedImage = null;
            lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+path);
        } catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }    
    private void getMultiPageMultiGraphBaseMC(){
        try {
            DirectoryChooser directoryChooser=new DirectoryChooser();
            fabricStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTBMP")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
            File selectedDirectory = directoryChooser.showDialog(fabricStage);
            if(selectedDirectory == null)
                return;
            else
                fabricStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+selectedDirectory.getAbsolutePath()+"]");
            
            lblStatus.setText(objDictionaryAction.getWord("GETGRAPHVIEW"));
            BufferedImage bufferedImage = new BufferedImage(objFabric.getIntWarp(), objFabric.getIntWeft(),BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
            Date date = new Date();
            String currentDate = dateFormat.format(date);
            String path = selectedDirectory.getPath()+"\\Fabric_"+currentDate+"\\";
            File file = new File(path);
            
            ArrayList<File> filesToZip=new ArrayList<>();
            
            if (!file.exists()) {
                if (!file.mkdir())
                    path = selectedDirectory.getPath();
            }
            
            bufferedImage = objFabricAction.plotGraphJaquardBaseView(objFabric, false);
            file = new File(path +"base.bmp");
            ImageIO.write(bufferedImage, "BMP", file);
            filesToZip.add(file);
            
            for(int i=0; i<objFabric.getIntExtraWeft(); i++){
                bufferedImage = objFabricAction.plotJaquardGraphView(objFabric, i);
                file = new File(path + i +".bmp");
                ImageIO.write(bufferedImage, "BMP", file);
                filesToZip.add(file);
            }
            for(int i=0; i<objFabric.getIntExtraWarp(); i++){
                bufferedImage = objFabricAction.plotJaquardGraphView(objFabric, i);
                file = new File(path + i +".bmp");
                ImageIO.write(bufferedImage, "BMP", file);
                filesToZip.add(file);
            }
            if(objConfiguration.getBlnAuthenticateService()){
                String zipFilePath=selectedDirectory.getPath()+"\\Fabric_"+currentDate+".zip";
                String passwordToZip = "Fabric_"+currentDate;
                new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                //delete folder recursively
                recursiveDelete(new File(path));
            }
            bufferedImage = null;
            lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+path);
        } catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }    
    private void getMultiPageMultiGraphColorBaseMC(){
        try {
            DirectoryChooser directoryChooser=new DirectoryChooser();
            fabricStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTBMP")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
            File selectedDirectory = directoryChooser.showDialog(fabricStage);
            if(selectedDirectory == null)
                return;
            else
                fabricStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+selectedDirectory.getAbsolutePath()+"]");
            
            lblStatus.setText(objDictionaryAction.getWord("GETGRAPHVIEW"));
            BufferedImage bufferedImage = new BufferedImage(objFabric.getIntWarp(), objFabric.getIntWeft(),BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
            Date date = new Date();
            String currentDate = dateFormat.format(date);
            String path = selectedDirectory.getPath()+"\\Fabric_"+currentDate+"\\";
            File file = new File(path);
            
            ArrayList<File> filesToZip=new ArrayList<>();
            
            if (!file.exists()) {
                if (!file.mkdir())
                    path = selectedDirectory.getPath();
            }
            
            bufferedImage = objFabricAction.plotGraphJaquardBaseView(objFabric, true);
            file = new File(path +"colorbase.bmp");
            ImageIO.write(bufferedImage, "BMP", file);
            filesToZip.add(file);
            
            for(int i=0; i<objFabric.getIntExtraWeft(); i++){
                bufferedImage = objFabricAction.plotJaquardGraphView(objFabric, i);
                file = new File(path + i +".bmp");
                ImageIO.write(bufferedImage, "BMP", file);
                filesToZip.add(file);
            }
            for(int i=0; i<objFabric.getIntExtraWarp(); i++){
                bufferedImage = objFabricAction.plotJaquardGraphView(objFabric, i);
                file = new File(path + i +".bmp");
                ImageIO.write(bufferedImage, "BMP", file);
                filesToZip.add(file);
            }
            if(objConfiguration.getBlnAuthenticateService()){
                String zipFilePath=selectedDirectory.getPath()+"\\Fabric_"+currentDate+".zip";
                String passwordToZip = "Fabric_"+currentDate;
                new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                //delete folder recursively
                recursiveDelete(new File(path));
            }
            bufferedImage = null;
            lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+path);
        } catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void getSuperimposedGraphMC(boolean showBase, boolean isSkipBase){
        try{
            BufferedImage bufferedImage = new BufferedImage(objFabric.getIntWeft(), objFabric.getIntWarp(),BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            byte[][] fabricMatrix = objFabricAction.getCrossSectionGraph(objFabric, isSkipBase);
            bufferedImage = objFabricAction.plotCrossSectionGraphView(objFabric, fabricMatrix, showBase);
            System.gc();
                        
            FileChooser fileChoser=new FileChooser();
            fileChoser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTBMP")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
            FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("BMP (.bmp)", "*.bmp");
            fileChoser.getExtensionFilters().add(extFilterBMP);
            File file=fileChoser.showSaveDialog(fabricStage);
            if(file==null)
                return;
            else
                fabricStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+file.getAbsoluteFile().getName()+"]");
            if (!file.getName().endsWith("bmp"))
                file = new File(file.getPath() +".bmp");
            ImageIO.write(bufferedImage, "BMP", file);
            bufferedImage = null;
            
            if(objConfiguration.getBlnAuthenticateService()){
                ArrayList<File> filesToZip=new ArrayList<>();
                filesToZip.add(file);
                String zipFilePath=file.getAbsolutePath()+".zip";
                String passwordToZip = file.getName();
                new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                file.delete();
            }
                
            lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+file.getCanonicalPath());
        } catch (IOException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex); 
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex); 
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex); 
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
//////////////// File Menu Action execution //////////////////    
    
/**
 * initFabricValue
 * <p>
 * Function use for saving fabric.
 * 
 * @exception (@throws )
 * @value
 * @author Amit Kumar Singh
 * @version     %I%, %G%
 * @since 1.0
 */    
    private void initFabricValue(){
         try {
            isWorkingMode = true;
            isEditingMode = false;
            selectedMenu = "FILE";
            menuHighlight();
            //objConfiguration.setStrRecentFabric(objFabric.getStrFabricID());
            //intVRepeat = (int)(objConfiguration.getDblFabricLength()/objConfiguration.getDblArtworkLength());
            //intHRepeat = (int)(objConfiguration.getDblFabricWidth()/objConfiguration.getDblArtworkWidth());
            warpExtraYarn = objFabric.getWarpExtraYarn();
            weftExtraYarn = objFabric.getWeftExtraYarn();
            threadPaletes = objFabric.getColourPalette();
            warpYarn = objFabric.getWarpYarn();
            weftYarn = objFabric.getWeftYarn();
            //objConfiguration = objFabric.getObjConfiguration();
            objFabric.getObjConfiguration().setStrGraphSize(12+"x"+(int)((12*objFabric.getIntPPI())/objFabric.getIntEPI()));
            plotViewActionMode = 1;
            plotViewAction();            
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }         
    }
/**
 * cloneFabric
 * <p>
 * Function use for saving fabric as clone.
 * 
 * @exception (@throws )
 * @value
 * @author Amit Kumar Singh
 * @version     %I%, %G%
 * @since 1.0
 */    
    public Fabric cloneFabric(){
        long cloneTime = 0L;
        long start = System.currentTimeMillis();
        Fabric objFabricCopy = new Fabric();
        try {
            objFabricCopy.setStrFabricID(objFabric.getStrFabricID());
            objFabricCopy.setStrFabricName(objFabric.getStrFabricName());
            objFabricCopy.setStrFabricType(objFabric.getStrFabricType());
            objFabricCopy.setStrClothType(objFabric.getStrClothType());
            objFabricCopy.setStrColourType(objFabric.getStrColourType());
            objFabricCopy.setDblFabricLength(objFabric.getDblFabricLength());
            objFabricCopy.setDblFabricWidth(objFabric.getDblFabricWidth());
            objFabricCopy.setDblArtworkLength(Double.parseDouble(String.format("%.3f",objFabric.getDblArtworkLength())));
            objFabricCopy.setDblArtworkWidth(Double.parseDouble(String.format("%.3f",objFabric.getDblArtworkWidth())));
            objFabricCopy.setStrArtworkID(objFabric.getStrArtworkID());
            objFabricCopy.setStrBaseWeaveID(objFabric.getStrBaseWeaveID());
            objFabricCopy.setIntWeft(objFabric.getIntWeft());
            objFabricCopy.setIntWarp(objFabric.getIntWarp());
            objFabricCopy.setIntExtraWeft(objFabric.getIntExtraWeft());
            objFabricCopy.setIntExtraWarp(objFabric.getIntExtraWarp());
            objFabricCopy.setIntPPI(objFabric.getIntPPI());
            objFabricCopy.setIntEPI(objFabric.getIntEPI());
            objFabricCopy.setIntHooks(objFabric.getIntHooks());
            objFabricCopy.setIntHPI(objFabric.getIntHPI());
            objFabricCopy.setIntReedCount(objFabric.getIntReedCount());
            objFabricCopy.setIntDents(objFabric.getIntDents());
            objFabricCopy.setIntTPD(objFabric.getIntTPD());
            objFabricCopy.setIntShaft(objFabric.getIntShaft());
            objFabricCopy.setIntProtection(objFabric.getIntProtection());
            objFabricCopy.setIntBinding(objFabric.getIntBinding());
            objFabricCopy.setBlnArtworkAssingmentSize(objFabric.getBlnArtworkAssingmentSize());
            objFabricCopy.setBlnArtworkOutline(objFabric.getBlnArtworkOutline());
            objFabricCopy.setBlnArtworkAspectRatio(objFabric.getBlnArtworkAspectRatio());
            objFabricCopy.setStrWarpPatternID(objFabric.getStrWarpPatternID());
            objFabricCopy.setStrWeftPatternID(objFabric.getStrWeftPatternID());
            objFabricCopy.setStrFabricFile(objFabric.getStrFabricFile());
            objFabricCopy.setStrFabricRData(objFabric.getStrFabricRData());
            objFabricCopy.setStrFabricDate(objFabric.getStrFabricDate());
            objFabricCopy.setColorArtwork(objFabric.getColorArtwork());
            objFabricCopy.setColorCountArtwork(objFabric.getColorCountArtwork());
            objFabricCopy.setStrFabricAccess(objFabric.getStrFabricAccess());
            
            byte[] bytFabricIcon = new byte[objFabric.getBytFabricIcon().length];
            for(int i=0; i<objFabric.getBytFabricIcon().length; i++)
                bytFabricIcon[i] = objFabric.getBytFabricIcon()[i];
            objFabricCopy.setBytFabricIcon(bytFabricIcon);
            
            byte[][] baseWeaveMatrix = new byte[objFabric.getBaseWeaveMatrix().length][objFabric.getBaseWeaveMatrix()[0].length];
            for(int i=0; i<objFabric.getBaseWeaveMatrix().length; i++)
                for(int j=0; j<objFabric.getBaseWeaveMatrix()[0].length; j++)
                    baseWeaveMatrix[i][j] = objFabric.getBaseWeaveMatrix()[i][j];
            objFabricCopy.setBaseWeaveMatrix(baseWeaveMatrix);
            
            if(objFabric.getArtworkMatrix()!=null){
                byte[][] artworkMatrix = new byte[objFabric.getArtworkMatrix().length][objFabric.getArtworkMatrix()[0].length];
                for(int i=0; i<objFabric.getArtworkMatrix().length; i++)
                    for(int j=0; j<objFabric.getArtworkMatrix()[0].length; j++)
                        artworkMatrix[i][j] = objFabric.getArtworkMatrix()[i][j];
                objFabricCopy.setArtworkMatrix(artworkMatrix);
            }
            byte[][] reverseMatrix = new byte[objFabric.getReverseMatrix().length][objFabric.getReverseMatrix()[0].length];
            for(int i=0; i<objFabric.getReverseMatrix().length; i++)
                for(int j=0; j<objFabric.getReverseMatrix()[0].length; j++)
                    reverseMatrix[i][j] = objFabric.getReverseMatrix()[i][j];
            objFabricCopy.setReverseMatrix(reverseMatrix);
            
            byte[][] fabricMatrix = new byte[objFabric.getFabricMatrix().length][objFabric.getFabricMatrix()[0].length];
            for(int i=0; i<objFabric.getFabricMatrix().length; i++)
                for(int j=0; j<objFabric.getFabricMatrix()[0].length; j++)
                    fabricMatrix[i][j] = objFabric.getFabricMatrix()[i][j];
            objFabricCopy.setFabricMatrix(fabricMatrix);

            Yarn[] warpYarn = new Yarn[objFabric.getWarpYarn().length];
            for(int i=0; i<objFabric.getWarpYarn().length; i++)
                warpYarn[i] = new Yarn(objFabric.getWarpYarn()[i].getStrYarnId(), objFabric.getWarpYarn()[i].getStrYarnType(), objFabric.getWarpYarn()[i].getStrYarnName(), objFabric.getWarpYarn()[i].getStrYarnColor(), objFabric.getWarpYarn()[i].getIntYarnRepeat(), objFabric.getWarpYarn()[i].getStrYarnSymbol(), objFabric.getWarpYarn()[i].getIntYarnCount(), objFabric.getWarpYarn()[i].getStrYarnCountUnit(), objFabric.getWarpYarn()[i].getIntYarnPly(), objFabric.getWarpYarn()[i].getIntYarnDFactor(), objFabric.getWarpYarn()[i].getDblYarnDiameter(), objFabric.getWarpYarn()[i].getIntYarnTwist(), objFabric.getWarpYarn()[i].getStrYarnTModel(), objFabric.getWarpYarn()[i].getIntYarnHairness(), objFabric.getWarpYarn()[i].getIntYarnHProbability(), objFabric.getWarpYarn()[i].getDblYarnPrice(), objFabric.getWarpYarn()[i].getStrYarnAccess(), objFabric.getWarpYarn()[i].getStrYarnUser(), objFabric.getWarpYarn()[i].getStrYarnDate());
            objFabricCopy.setWarpYarn(warpYarn);
            
            Yarn[] weftYarn = new Yarn[objFabric.getWeftYarn().length];
            for(int i=0; i<objFabric.getWeftYarn().length; i++)
                weftYarn[i] = new Yarn(objFabric.getWeftYarn()[i].getStrYarnId(), objFabric.getWeftYarn()[i].getStrYarnType(), objFabric.getWeftYarn()[i].getStrYarnName(), objFabric.getWeftYarn()[i].getStrYarnColor(), objFabric.getWeftYarn()[i].getIntYarnRepeat(), objFabric.getWeftYarn()[i].getStrYarnSymbol(), objFabric.getWeftYarn()[i].getIntYarnCount(), objFabric.getWeftYarn()[i].getStrYarnCountUnit(), objFabric.getWeftYarn()[i].getIntYarnPly(), objFabric.getWeftYarn()[i].getIntYarnDFactor(), objFabric.getWeftYarn()[i].getDblYarnDiameter(), objFabric.getWeftYarn()[i].getIntYarnTwist(), objFabric.getWeftYarn()[i].getStrYarnTModel(), objFabric.getWeftYarn()[i].getIntYarnHairness(), objFabric.getWeftYarn()[i].getIntYarnHProbability(), objFabric.getWeftYarn()[i].getDblYarnPrice(), objFabric.getWeftYarn()[i].getStrYarnAccess(), objFabric.getWeftYarn()[i].getStrYarnUser(), objFabric.getWeftYarn()[i].getStrYarnDate());
            objFabricCopy.setWeftYarn(weftYarn);
            
            if(objFabric.getWarpExtraYarn()!=null){
                Yarn[] warpExtraYarn = new Yarn[objFabric.getWarpExtraYarn().length];
                for(int i=0; i<objFabric.getWarpExtraYarn().length; i++)
                    warpExtraYarn[i] = new Yarn(objFabric.getWarpExtraYarn()[i].getStrYarnId(), objFabric.getWarpExtraYarn()[i].getStrYarnType(), objFabric.getWarpExtraYarn()[i].getStrYarnName(), objFabric.getWarpExtraYarn()[i].getStrYarnColor(), objFabric.getWarpExtraYarn()[i].getIntYarnRepeat(), objFabric.getWarpExtraYarn()[i].getStrYarnSymbol(), objFabric.getWarpExtraYarn()[i].getIntYarnCount(), objFabric.getWarpExtraYarn()[i].getStrYarnCountUnit(), objFabric.getWarpExtraYarn()[i].getIntYarnPly(), objFabric.getWarpExtraYarn()[i].getIntYarnDFactor(), objFabric.getWarpExtraYarn()[i].getDblYarnDiameter(), objFabric.getWarpExtraYarn()[i].getIntYarnTwist(), objFabric.getWarpExtraYarn()[i].getStrYarnTModel(), objFabric.getWarpExtraYarn()[i].getIntYarnHairness(), objFabric.getWarpExtraYarn()[i].getIntYarnHProbability(), objFabric.getWarpExtraYarn()[i].getDblYarnPrice(), objFabric.getWarpExtraYarn()[i].getStrYarnAccess(), objFabric.getWarpExtraYarn()[i].getStrYarnUser(), objFabric.getWarpExtraYarn()[i].getStrYarnDate());
                objFabricCopy.setWarpExtraYarn(warpExtraYarn);
            }
            
            if(objFabric.getWeftExtraYarn()!=null){            
                Yarn[] weftExtraYarn = new Yarn[objFabric.getWeftExtraYarn().length];
                for(int i=0; i<objFabric.getWeftExtraYarn().length; i++)
                    weftExtraYarn[i] = new Yarn(objFabric.getWeftExtraYarn()[i].getStrYarnId(), objFabric.getWeftExtraYarn()[i].getStrYarnType(), objFabric.getWeftExtraYarn()[i].getStrYarnName(), objFabric.getWeftExtraYarn()[i].getStrYarnColor(), objFabric.getWeftExtraYarn()[i].getIntYarnRepeat(), objFabric.getWeftExtraYarn()[i].getStrYarnSymbol(), objFabric.getWeftExtraYarn()[i].getIntYarnCount(), objFabric.getWeftExtraYarn()[i].getStrYarnCountUnit(), objFabric.getWeftExtraYarn()[i].getIntYarnPly(), objFabric.getWeftExtraYarn()[i].getIntYarnDFactor(), objFabric.getWeftExtraYarn()[i].getDblYarnDiameter(), objFabric.getWeftExtraYarn()[i].getIntYarnTwist(), objFabric.getWeftExtraYarn()[i].getStrYarnTModel(), objFabric.getWeftExtraYarn()[i].getIntYarnHairness(), objFabric.getWeftExtraYarn()[i].getIntYarnHProbability(), objFabric.getWeftExtraYarn()[i].getDblYarnPrice(), objFabric.getWeftExtraYarn()[i].getStrYarnAccess(), objFabric.getWeftExtraYarn()[i].getStrYarnUser(), objFabric.getWeftExtraYarn()[i].getStrYarnDate());
                objFabricCopy.setWeftExtraYarn(weftExtraYarn);
            }
            
            if(objFabric.getColorWeave()!=null){
                String[][] colorWeave = new String[objFabric.getColorWeave().length][objFabric.getColorWeave()[0].length];
                for(int i=0; i<objFabric.getColorWeave().length; i++)
                    for(int j=0; j<objFabric.getColorWeave()[0].length; j++)
                        colorWeave[i][j] = objFabric.getColorWeave()[i][j];
                objFabricCopy.setColorWeave(colorWeave);
            }
            String[] threadPaletes = new String[objFabric.getColourPalette().length];
            for(int i=0; i<objFabric.getColourPalette().length; i++)
                threadPaletes[i] = objFabric.getColourPalette()[i];
            objFabricCopy.setColourPalette(threadPaletes);
            
            if(objFabric.getExtraColourPalette()!=null){
                String[] threadExtraPaletes = new String[objFabric.getExtraColourPalette().length];
                for(int i=0; i<objFabric.getExtraColourPalette().length; i++)
                    threadExtraPaletes[i] = objFabric.getExtraColourPalette()[i];
                objFabricCopy.setExtraColourPalette(threadExtraPaletes);
            }
            if(objFabric.getLstYarn()!=null){
                ArrayList<Yarn> lstYarn = new ArrayList<Yarn>();
                for(int i=0; i<objFabric.getLstYarn().size(); i++)
                    lstYarn.set(i, new Yarn(objFabric.getLstYarn().get(i).getStrYarnId(), objFabric.getLstYarn().get(i).getStrYarnType(), objFabric.getLstYarn().get(i).getStrYarnName(), objFabric.getLstYarn().get(i).getStrYarnColor(), objFabric.getLstYarn().get(i).getIntYarnRepeat(), objFabric.getLstYarn().get(i).getStrYarnSymbol(), objFabric.getLstYarn().get(i).getIntYarnCount(), objFabric.getLstYarn().get(i).getStrYarnCountUnit(), objFabric.getLstYarn().get(i).getIntYarnPly(), objFabric.getLstYarn().get(i).getIntYarnDFactor(), objFabric.getLstYarn().get(i).getDblYarnDiameter(), objFabric.getLstYarn().get(i).getIntYarnTwist(), objFabric.getLstYarn().get(i).getStrYarnTModel(), objFabric.getLstYarn().get(i).getIntYarnHairness(), objFabric.getLstYarn().get(i).getIntYarnHProbability(), objFabric.getLstYarn().get(i).getDblYarnPrice(), objFabric.getLstYarn().get(i).getStrYarnAccess(), objFabric.getLstYarn().get(i).getStrYarnUser(), objFabric.getLstYarn().get(i).getStrYarnDate()));
                objFabricCopy.setLstYarn(lstYarn);
            }
            if(objFabric.getLstPattern()!=null){
                ArrayList<Pattern> lstPattern = new ArrayList<Pattern>();
                for(int i=0; i<objFabric.getLstPattern().size(); i++)
                    lstPattern.set(i, new Pattern(objFabric.getLstPattern().get(i).getStrPatternID(), objFabric.getLstPattern().get(i).getStrPattern(), objFabric.getLstPattern().get(i).getIntPatternType(), objFabric.getLstPattern().get(i).getIntPatternRepeat(), objFabric.getLstPattern().get(i).getIntPatternUsed(), objFabric.getLstPattern().get(i).getStrPatternAccess(), objFabric.getLstPattern().get(i).getStrPatternUser(), objFabric.getLstPattern().get(i).getStrPatternDate()));
                objFabricCopy.setLstPattern(lstPattern);
            }
            if(objFabric.getYarnBI()!=null){
                BufferedImage yarnBI = new BufferedImage(objFabric.getYarnBI().getWidth(), objFabric.getYarnBI().getHeight(), objFabric.getYarnBI().getType());
                Graphics2D g = yarnBI.createGraphics();
                g.drawImage(objFabric.getYarnBI(), 0, 0, objFabric.getYarnBI().getWidth(), objFabric.getYarnBI().getHeight(), null);
                g.dispose();
                objFabricCopy.setYarnBI(yarnBI);
            }
            objFabricCopy.setObjConfiguration(objConfiguration);
            
        } catch (Exception ex) {
            //Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);         
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
        cloneTime += (System.currentTimeMillis() - start);
        //System.out.println("Cloning Time"+cloneTime);
        return objFabricCopy;
    }
/**
 * newFabric
 * <p>
 * Function use for drawing tool bar for menu item File,
 * and binding events for each tools.
 * 
 * @param  
 * @return
 * @exception (@throws )
 * @value
 * @author Amit Kumar Singh
 * @version     %I%, %G%
 * @since 1.0
 * @see java.awt
 * @link
 * @serial (or @serialField or @serialData)
 * @serialField
 * @serialData  
 * //@deprecated (see How and When To Deprecate APIs)
 */    
    private void newFabric(){
        try {
            isNew = true;            
            
            UserAction objUserAction = new UserAction();
            objUserAction.getConfiguration(objConfiguration);
            //objConfiguration.clothRepeat();
            objConfiguration.getColourPalette()[0]=objConfiguration.getStrWeftColor();
            objConfiguration.getColourPalette()[26]=objConfiguration.getStrWarpColor();
            
            objFabricAction = new FabricAction();
            objFabric.setObjConfiguration(objConfiguration);
            objFabric.setStrFabricName("My Fabric");
            //objFabric.setStrFabricID(new IDGenerator().getIDGenerator("FABRIC_LIBRARY", objConfiguration.getObjUser().getStrUserID()));
            objFabric.setStrFabricType(objConfiguration.getStrFabricType());
            objFabric.setStrColourType(objConfiguration.getStrColourType());
            objFabric.setStrClothType(objConfiguration.getStrClothType());
            objFabric.setDblFabricLength(objConfiguration.getDblFabricLength());
            objFabric.setDblFabricWidth(objConfiguration.getDblFabricWidth());
            objFabric.setDblArtworkLength(Double.parseDouble(String.format("%.3f",((double) objConfiguration.getIntPixs())/objConfiguration.getIntPPI())));//objConfiguration.getDblArtworkLength());
            objFabric.setDblArtworkWidth(Double.parseDouble(String.format("%.3f",((double) objConfiguration.getIntEnds())/objConfiguration.getIntEPI())));//objConfiguration.getDblArtworkWidth());
            objFabric.setIntWarp(objConfiguration.getIntEnds());
            objFabric.setIntWeft(objConfiguration.getIntPixs());
            //System.out.println(objConfiguration.getIntEnds()+":"+objConfiguration.getIntPixs()+"::"+objFabric.getIntWarp()+":"+objFabric.getIntWeft());
            objFabric.setIntHooks(objConfiguration.getIntHooks());
            objFabric.setIntShaft(0);
            objFabric.setIntExtraWarp(objConfiguration.getIntExtraWarp());
            objFabric.setIntExtraWeft(objConfiguration.getIntExtraWeft());
            objFabric.setIntReedCount(objConfiguration.getIntReedCount());
            objFabric.setIntDents(objConfiguration.getIntDents());
            objFabric.setIntTPD(objConfiguration.getIntTPD());
            objFabric.setIntEPI(objConfiguration.getIntEPI());
            objFabric.setIntHPI(objConfiguration.getIntHPI());
            objFabric.setIntPPI(objConfiguration.getIntPPI());
            objFabric.setIntProtection(objConfiguration.getIntProtection());
            objFabric.setIntBinding(objConfiguration.getIntBinding());
            objFabric.setBlnArtworkAssingmentSize(objConfiguration.getBlnArtworkAssingmentSize());
            objFabric.setBlnArtworkOutline(objConfiguration.getBlnArtworkOutline());
            objFabric.setBlnArtworkAspectRatio(objConfiguration.getBlnArtworkAspectRatio());
            //intVRepeat = (int)(objConfiguration.getDblFabricLength()/objConfiguration.getDblArtworkLength());
            //intHRepeat = (int)(objConfiguration.getDblFabricWidth()/objConfiguration.getDblArtworkWidth());
            objFabric.setStrArtworkID(null);
            Weave objWeave = new Weave();
            objWeave.setObjConfiguration(objFabric.getObjConfiguration());
            objWeave.setStrWeaveID(objFabric.getStrBaseWeaveID());
            objWeaveAction = new WeaveAction(); 
            objWeaveAction.getWeave(objWeave);
            objWeaveAction = new WeaveAction(objWeave,true); 
            objWeaveAction.extractWeaveContent(objWeave);
            objFabric.setBaseWeaveMatrix(objWeave.getDesignMatrix());
            objArtworkAction= new ArtworkAction();            
            byte[][] fabricMatrix = new byte[objFabric.getIntWeft()][objFabric.getIntWarp()];
            fabricMatrix = objArtworkAction.repeatMatrix(objWeave.getDesignMatrix(),objFabric.getIntWeft(),objFabric.getIntWarp());
            byte[][] reverseMatrix = new byte[objFabric.getIntWeft()][objFabric.getIntWarp()];
            for(int i = 0; i < objFabric.getIntWeft(); i++) {
                for(int j = 0; j < objFabric.getIntWarp(); j++) {
                    if(fabricMatrix[i][j]==0){
                         reverseMatrix[i][j] = 1;  
                    }else{
                         reverseMatrix[i][j] = 0;
                    }                
                }
            } 
            objFabric.setFabricMatrix(fabricMatrix);
            objFabric.setReverseMatrix(reverseMatrix);
            reverseMatrix=null;
            fabricMatrix = null;
            objFabric.setColourPalette(objConfiguration.getColourPalette());
            objConfiguration.setStrWarpColor(objConfiguration.getColourPalette()[0]);
            objConfiguration.setStrWeftColor(objConfiguration.getColourPalette()[26]);
            Yarn objYarn = null;
            objYarn = new Yarn(null, "Warp", objConfiguration.getStrWarpName(), "#"+objConfiguration.getStrWarpColor(), objConfiguration.getIntWarpRepeat(), "A", 10, "English Cotton (NeC)", objConfiguration.getIntWarpPly(), objConfiguration.getIntWarpFactor(), 0.287, objConfiguration.getIntWarpTwist(), objConfiguration.getStrWarpSence(), objConfiguration.getIntWarpHairness(), objConfiguration.getIntWarpDistribution(), objFabric.getObjConfiguration().getDblWarpPrice(), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);
            objYarn.setObjConfiguration(objConfiguration);
            warpYarn = new Yarn[]{objYarn};
            objYarn = new Yarn(null, "Weft", objConfiguration.getStrWeftName(), "#"+objConfiguration.getStrWeftColor(), objConfiguration.getIntWeftRepeat(), "a", 10, "English Cotton (NeC)", objConfiguration.getIntWeftPly(), objConfiguration.getIntWeftFactor(), 0.287, objConfiguration.getIntWeftTwist(), objConfiguration.getStrWeftSence(), objConfiguration.getIntWeftHairness(), objConfiguration.getIntWeftDistribution(), objFabric.getObjConfiguration().getDblWeftPrice(), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);
            objYarn.setObjConfiguration(objConfiguration);
            weftYarn = new Yarn[]{objYarn};
            weftExtraYarn = null;
            warpExtraYarn = null;
            objFabric.setWarpYarn(warpYarn);
            objFabric.setWeftYarn(weftYarn);
            objFabric.setWarpExtraYarn(warpExtraYarn);
            objFabric.setWeftExtraYarn(weftExtraYarn);
            PatternAction objPatternAction = new PatternAction();
            objFabric.setStrWarpPatternID(objPatternAction.getPatternAvibilityID(objConfiguration.getStrWarpPattern(),(byte)1));
            objPatternAction = new PatternAction();
            objFabric.setStrWeftPatternID(objPatternAction.getPatternAvibilityID(objConfiguration.getStrWeftPattern(),(byte)2));
            objFabricAction = new FabricAction();
            objFabricAction.fabricImage(objFabric, objFabric.getIntWeft(), objFabric.getIntWarp());
            objFabric.setObjConfiguration(objConfiguration);
            objFabric.getObjConfiguration().setStrGraphSize(12+"x"+(int)((12*objFabric.getIntPPI())/objFabric.getIntEPI()));
            
            initFabricValue();
            if(objConfiguration.getStrRecentArtwork()!=null && objConfiguration.getStrRecentArtwork()!="" && objConfiguration.strWindowFlowContext.equalsIgnoreCase("ArtworkEditor")){
                jacquardConverstionAction();
            }
            yarnGridSizeIndex = 9; // Simulation Mapping pixels reset to 10
            objUR.clear();
            Fabric objFabricCopy = cloneFabric();
            objUR.doCommand("Initial Create Fabric", objFabricCopy);
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.getMessage().toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
/**
 * loadFabric
 * <p>
 * Function use for saving fabric.
 * 
 * @exception (@throws )
 * @value
 * @author Amit Kumar Singh
 * @version     %I%, %G%
 * @since 1.0
 */    
    private void loadFabric(){
        try {
            isNew = false;
            
            objConfiguration.setStrClothType(objFabric.getStrClothType());
            UserAction objUserAction = new UserAction();
            objUserAction.getConfiguration(objConfiguration);
            //objConfiguration.clothRepeat();
                                    
            objConfiguration.setStrRecentFabric(objFabric.getStrFabricID());
            
            //objConfiguration.mapRecentFabric.put(objConfiguration.getStrClothType(),objFabric.getStrFabricID());
            objFabricAction = new FabricAction();
            //intVRepeat = (int)(objConfiguration.getDblFabricLength()/objConfiguration.getDblArtworkLength());
            //intHRepeat = (int)(objConfiguration.getDblFabricWidth()/objConfiguration.getDblArtworkWidth());
            Weave objWeave = new Weave();
            objWeave.setObjConfiguration(objFabric.getObjConfiguration());
            objWeave.setStrWeaveID(objFabric.getStrBaseWeaveID());
            objWeaveAction = new WeaveAction(); 
            objWeaveAction.getWeave(objWeave);
            objWeaveAction = new WeaveAction(); 
            objWeaveAction.extractWeaveContent(objWeave);
            objFabric.setBaseWeaveMatrix(objWeave.getDesignMatrix());
            
            if(objFabric.getStrArtworkID()!=null&&!objFabric.getStrArtworkID().equalsIgnoreCase("null")){                
                Artwork objArtwork = new Artwork();
                objArtwork.setObjConfiguration(objFabric.getObjConfiguration());
                objArtwork.setStrArtworkId(objFabric.getStrArtworkID());
                objFabric.setArtworkMatrix(objFabric.getFabricMatrix());
                objFabricAction = new FabricAction();
                objFabricAction.getFabricArtwork(objFabric, "Main");
                YarnAction objYarnAction = new YarnAction();                
                warpExtraYarn = objYarnAction.getFabricYarn(objFabric.getStrFabricID(), "Extra Warp", "Main");
                objFabric.setWarpExtraYarn(warpExtraYarn);
                objYarnAction = new YarnAction();
                weftExtraYarn = objYarnAction.getFabricYarn(objFabric.getStrFabricID(), "Extra Weft", "Main");
                objFabric.setWeftExtraYarn(weftExtraYarn);
            }else{
                warpExtraYarn = null;
                objFabric.setWarpExtraYarn(warpExtraYarn);
                objFabric.setIntExtraWarp(objConfiguration.getIntExtraWarp());
                weftExtraYarn = null;
                objFabric.setWeftExtraYarn(weftExtraYarn);
                objFabric.setIntExtraWeft(objConfiguration.getIntExtraWeft());
            }
            objFabricAction = new FabricAction();
            threadPaletes = objFabricAction.getFabricPallets(objFabric, "Main");
            YarnAction objYarnAction = new YarnAction();                
            warpYarn = objYarnAction.getFabricYarn(objFabric.getStrFabricID(), "Warp", "Main");
            objYarnAction = new YarnAction();
            weftYarn = objYarnAction.getFabricYarn(objFabric.getStrFabricID(), "Weft", "Main");
            
            char character;
            for(int i=0; i<warpYarn.length;i++){
                //65 - 96
                character = warpYarn[i].getStrYarnSymbol().trim().toUpperCase().charAt(0); // This gives the character 'a'
                int ascii = (int) character; 
                warpYarn[i].setStrYarnColor((warpYarn[i].getStrYarnColor().startsWith("#"))?warpYarn[i].getStrYarnColor():"#"+warpYarn[i].getStrYarnColor());
                threadPaletes[ascii-65] = (warpYarn[i].getStrYarnColor().length()==6)?warpYarn[i].getStrYarnColor():warpYarn[i].getStrYarnColor().substring(1);
            }
            
            for(int i=0; i<weftYarn.length;i++){
                //97 - 113
                character = weftYarn[i].getStrYarnSymbol().trim().toLowerCase().charAt(0); // This gives the character 'a'
                int ascii = (int) character; 
                weftYarn[i].setStrYarnColor((weftYarn[i].getStrYarnColor().startsWith("#"))?weftYarn[i].getStrYarnColor():"#"+weftYarn[i].getStrYarnColor());
                threadPaletes[ascii-71] = (weftYarn[i].getStrYarnColor().length()==6)?weftYarn[i].getStrYarnColor():weftYarn[i].getStrYarnColor().substring(1);
            }
                   
            objFabric.setColourPalette(threadPaletes);
            //objConfiguration.setColourPalette(threadPaletes);
            objFabric.setWarpYarn(warpYarn);
            objFabric.setWeftYarn(weftYarn);
              
            if(objFabric.getStrArtworkID()!=null&&!objFabric.getStrArtworkID().equalsIgnoreCase("null")){
                Artwork objArtwork=new Artwork();
                objArtwork.setObjConfiguration(objFabric.getObjConfiguration());
                objArtwork.setStrArtworkId(objFabric.getStrArtworkID());
                objArtworkAction = new ArtworkAction();
                objArtworkAction.getArtwork(objArtwork);
                byte[] bytArtworkThumbnil=objArtwork.getBytArtworkThumbnil();
                SeekableStream stream = new ByteArraySeekableStream(bytArtworkThumbnil);
                String[] names = ImageCodec.getDecoderNames(stream);
                ImageDecoder dec = ImageCodec.createImageDecoder(names[0], stream, null);
                RenderedImage im = dec.decodeAsRenderedImage();
                BufferedImage bufferedImage = PlanarImage.wrapRenderedImage(im).getAsBufferedImage();
                bytArtworkThumbnil=null;
                System.gc();                        
                ArrayList<java.awt.Color> colors = objArtworkAction.getImageColor(bufferedImage);
                objFabric.setColorCountArtwork(colors.size());
                String[][] colorWeave = new String[colors.size()*colors.size()][4];
                for(int i=0, j=(objFabric.getColorWeave().length<colorWeave.length?objFabric.getColorWeave().length:colorWeave.length);i<j;i++){
                    colorWeave[i][0] = objFabric.getColorWeave()[i][0];
                    colorWeave[i][1] = objFabric.getColorWeave()[i][1];
                    colorWeave[i][2] = objFabric.getColorWeave()[i][2];
                    if(objFabric.getColorWeave()[i][1]!=null)
                        colorWeave[i][3] = Double.toString(objArtworkAction.getImageColorPercentage(bufferedImage,objFabric.getColorWeave()[i][0]));
                    else
                        colorWeave[i][3] = null;
                }
                objFabric.setColorWeave(colorWeave);
                colorWeave = null;
            }
            objFabric.setObjConfiguration(objConfiguration);
            objFabric.getObjConfiguration().setStrGraphSize(12+"x"+(int)((12*objFabric.getIntPPI())/objFabric.getIntEPI()));
            initFabricValue();
            yarnGridSizeIndex = 9; // Simulation Mapping pixels reset to 10
            objUR.clear();
            Fabric objFabricCopy = cloneFabric();
            objUR.doCommand("Initial Load Fabric", objFabricCopy);
        } catch (SQLException ex) {
            //Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            //Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
 /**
 * saveFabric
 * <p>
 * Function use for saving fabric.
 * 
 * @exception (@throws )
 * @value
 * @author Amit Kumar Singh
 * @version     %I%, %G%
 * @since 1.0
 */    
    private void saveFabric(){
        try{
            objFabric.setStrFabricID(new IDGenerator().getIDGenerator("FABRIC_LIBRARY", objConfiguration.getObjUser().getStrUserID()));
            objFabricAction = new FabricAction();
            objFabricAction.fabricImage(objFabric,objFabric.getIntWeft(),objFabric.getIntWarp());
            objFabricAction = new FabricAction();
            if(objFabricAction.setFabric(objFabric, "Main")>0){
                objFabricAction = new FabricAction();
                objFabricAction.setFabricPallets(objFabric, "Main");
                //objFabricAction = new FabricAction();
                //objFabricAction.setFabricYarn(objFabric);
                //objFabricAction.clearFabricYarn(objFabric.getStrFabricID());
                String yarnId = null;
                YarnAction objYarnAction;
                for(int i = 0; i<objFabric.getWarpYarn().length; i++){
                    yarnId = new IDGenerator().getIDGenerator("YARN_LIBRARY", objConfiguration.getObjUser().getStrUserID());
                    objFabric.getWarpYarn()[i].setStrYarnId(yarnId);
                    objFabric.getWarpYarn()[i].setObjConfiguration(objConfiguration);
                    objYarnAction = new YarnAction();
                    objYarnAction.setYarn(objFabric.getWarpYarn()[i]);
                    /*objFabricAction = new FabricAction();
                    yarnId = objFabricAction.getYarnID(objFabric.getWarpYarn()[i]);
                    objFabric.getWarpYarn()[i].setThreadId(yarnId);
                    */objYarnAction = new YarnAction();
                    objYarnAction.setFabricYarn(objFabric.getStrFabricID(), objFabric.getWarpYarn()[i],i, "Main");
                }
                for(int i = 0; i<objFabric.getWeftYarn().length; i++){
                    yarnId = new IDGenerator().getIDGenerator("YARN_LIBRARY", objConfiguration.getObjUser().getStrUserID());
                    objFabric.getWeftYarn()[i].setStrYarnId(yarnId);
                    objFabric.getWeftYarn()[i].setObjConfiguration(objConfiguration);
                    objYarnAction = new YarnAction();
                    objYarnAction.setYarn(objFabric.getWeftYarn()[i]);
                    /*objYarnAction = new YarnAction();
                    yarnId = objYarnAction.getYarnID(objFabric.getWarpYarn()[i]);
                    objFabric.getWarpYarn()[i].setThreadId(yarnId);
                    */objYarnAction = new YarnAction();
                    objYarnAction.setFabricYarn(objFabric.getStrFabricID(), objFabric.getWeftYarn()[i],i, "Main");
                }
                if(objFabric.getWarpExtraYarn()!=null){
                    for(int i = 0; i<objFabric.getWarpExtraYarn().length; i++){
                        yarnId = new IDGenerator().getIDGenerator("YARN_LIBRARY", objConfiguration.getObjUser().getStrUserID());
                        objFabric.getWarpExtraYarn()[i].setStrYarnId(yarnId);
                        objFabric.getWarpExtraYarn()[i].setObjConfiguration(objConfiguration);
                        objYarnAction = new YarnAction();
                        objYarnAction.setYarn(objFabric.getWarpExtraYarn()[i]);
                        /*objFabricAction = new FabricAction();
                        yarnId = objFabricAction.getYarnID(objFabric.getWarpYarn()[i]);
                        objFabric.getWarpYarn()[i].setThreadId(yarnId);
                        */objYarnAction = new YarnAction();
                        objYarnAction.setFabricYarn(objFabric.getStrFabricID(), objFabric.getWarpExtraYarn()[i],i, "Main");
                    }
                }
                if(objFabric.getWeftExtraYarn()!=null){
                    for(int i = 0; i<objFabric.getWeftExtraYarn().length; i++){
                        yarnId = new IDGenerator().getIDGenerator("YARN_LIBRARY", objConfiguration.getObjUser().getStrUserID());
                        objFabric.getWeftExtraYarn()[i].setStrYarnId(yarnId);
                        objFabric.getWeftExtraYarn()[i].setObjConfiguration(objConfiguration);
                        objYarnAction = new YarnAction();
                        objYarnAction.setYarn(objFabric.getWeftExtraYarn()[i]);
                        /*objFabricAction = new FabricAction();
                        yarnId = objFabricAction.getYarnID(objFabric.getWarpYarn()[i]);
                        objFabric.getWarpYarn()[i].setThreadId(yarnId);
                        */objYarnAction = new YarnAction();
                        objYarnAction.setFabricYarn(objFabric.getStrFabricID(), objFabric.getWeftExtraYarn()[i],i, "Main");
                    }
                }                        
                if(objFabric.getStrArtworkID()!=null){
                    objFabricAction = new FabricAction();
                    objFabricAction.setFabricArtwork(objFabric, "Main");
                }
            }
            isNew = false;
            lblStatus.setText(objFabric.getStrFabricName()+" : "+objDictionaryAction.getWord("DATASAVED"));
        } catch (SQLException ex) {
            //Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            //Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }

 /**
 * updateFabric
 * <p>
 * Function use for saving fabric.
 * 
 * @exception (@throws )
 * @value
 * @author Amit Kumar Singh
 * @version     %I%, %G%
 * @since 1.0
 */    
    private void updateFabric(){
        try{
            objFabricAction = new FabricAction();
            objFabricAction.fabricImage(objFabric,objFabric.getIntWeft(),objFabric.getIntWarp());
            objFabricAction = new FabricAction();
            objFabricAction.resetFabric(objFabric, "Main");
            objFabricAction = new FabricAction();
            objFabricAction.resetFabricPallets(objFabric, "Main");
            
            YarnAction objYarnAction = new YarnAction();
            //objFabricAction.setFabricYarn(objFabric);
            objYarnAction.clearFabricYarn(objFabric.getStrFabricID(), "Main");
            String yarnId = null;
            for(int i = 0; i<objFabric.getWarpYarn().length; i++){
                yarnId = new IDGenerator().getIDGenerator("YARN_LIBRARY", objConfiguration.getObjUser().getStrUserID());
                objFabric.getWarpYarn()[i].setStrYarnId(yarnId);
                objFabric.getWarpYarn()[i].setObjConfiguration(objConfiguration);
                objYarnAction = new YarnAction();
                objYarnAction.setYarn(objFabric.getWarpYarn()[i]);
                objYarnAction = new YarnAction();
                objYarnAction.setFabricYarn(objFabric.getStrFabricID(), objFabric.getWarpYarn()[i],i, "Main");
            }
            for(int i = 0; i<objFabric.getWeftYarn().length; i++){
                yarnId = new IDGenerator().getIDGenerator("YARN_LIBRARY", objConfiguration.getObjUser().getStrUserID());
                objFabric.getWeftYarn()[i].setStrYarnId(yarnId);
                objFabric.getWeftYarn()[i].setObjConfiguration(objConfiguration);
                objYarnAction = new YarnAction();
                objYarnAction.setYarn(objFabric.getWeftYarn()[i]);
                objYarnAction = new YarnAction();
                objYarnAction.setFabricYarn(objFabric.getStrFabricID(), objFabric.getWeftYarn()[i],i, "Main");
            }
            if(objFabric.getWarpExtraYarn()!=null){
                for(int i = 0; i<objFabric.getWarpExtraYarn().length; i++){
                    yarnId = new IDGenerator().getIDGenerator("YARN_LIBRARY", objConfiguration.getObjUser().getStrUserID());
                    objFabric.getWarpExtraYarn()[i].setStrYarnId(yarnId);
                    objFabric.getWarpExtraYarn()[i].setObjConfiguration(objConfiguration);
                    objYarnAction = new YarnAction();
                    objYarnAction.setYarn(objFabric.getWarpExtraYarn()[i]);
                    objYarnAction = new YarnAction();
                    objYarnAction.setFabricYarn(objFabric.getStrFabricID(), objFabric.getWarpExtraYarn()[i],i, "Main");
                }
            }
            if(objFabric.getWeftExtraYarn()!=null){
                for(int i = 0; i<objFabric.getWeftExtraYarn().length; i++){
                    yarnId = new IDGenerator().getIDGenerator("YARN_LIBRARY", objConfiguration.getObjUser().getStrUserID());
                    objFabric.getWeftExtraYarn()[i].setStrYarnId(yarnId);
                    objFabric.getWeftExtraYarn()[i].setObjConfiguration(objConfiguration);
                    objYarnAction = new YarnAction();
                    objYarnAction.setYarn(objFabric.getWeftExtraYarn()[i]);
                    objYarnAction = new YarnAction();
                    objYarnAction.setFabricYarn(objFabric.getStrFabricID(), objFabric.getWeftExtraYarn()[i],i, "Main");
                }
            }
            if(objFabric.getStrArtworkID()!=null){
                objFabricAction = new FabricAction();
                objFabricAction.clearFabricArtwork(objFabric.getStrFabricID(), "Main");
                objFabricAction = new FabricAction();
                objFabricAction.setFabricArtwork(objFabric, "Main");
            }
            lblStatus.setText(objFabric.getStrFabricName()+" : "+objDictionaryAction.getWord("DATASAVED"));
        } catch (SQLException ex) {
            //Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            //Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }    
    }
    private void saveUpdateFabric(){
        objFabric.setDblArtworkLength(Double.parseDouble(String.format("%.3f",objFabric.getDblArtworkLength())));
        objFabric.setDblArtworkWidth(Double.parseDouble(String.format("%.3f",objFabric.getDblArtworkWidth())));
        
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.WINDOW_MODAL);
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setAlignment(Pos.CENTER);
        popup.setHgap(10);
        popup.setVgap(10);
        popup.setPadding(new Insets(10, 10, 10, 10));
            
        Label lblName = new Label(objDictionaryAction.getWord("NAME"));
        lblName.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPNAME")));
        popup.add(lblName, 0, 0);
        final TextField txtName = new TextField();            
        txtName.setPromptText(objDictionaryAction.getWord("TOOLTIPNAME"));
        txtName.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPNAME")));
        txtName.setDisable(true);
        if(objFabric.getStrFabricName()!=null)
            txtName.setText(objFabric.getStrFabricName());
        if(isNew)
            txtName.setDisable(false);
        popup.add(txtName, 1, 0, 2, 1);
            
        final ToggleGroup fabricTG = new ToggleGroup();
        RadioButton fabricPublicRB = new RadioButton(objDictionaryAction.getWord("PUBLIC"));
        fabricPublicRB.setToggleGroup(fabricTG);
        fabricPublicRB.setUserData("Public");
        popup.add(fabricPublicRB, 0, 1);
        RadioButton fabricProtectedRB = new RadioButton(objDictionaryAction.getWord("PROTECTED"));
        fabricProtectedRB.setToggleGroup(fabricTG);
        fabricProtectedRB.setUserData("Protected");
        popup.add(fabricProtectedRB, 1, 1);
        RadioButton fabricPrivateRB = new RadioButton(objDictionaryAction.getWord("PRIVATE"));
        fabricPrivateRB.setToggleGroup(fabricTG);
        fabricPrivateRB.setUserData("Private");
        popup.add(fabricPrivateRB, 2, 1);
        if(isNew){
            objFabric.setStrFabricAccess(objConfiguration.getObjUser().getUserAccess("FABRIC_LIBRARY"));
        }            
        if(objFabric.getStrFabricAccess().equalsIgnoreCase("Public")){
            fabricTG.selectToggle(fabricPublicRB);
            fabricPublicRB.setDisable(true);
            fabricProtectedRB.setDisable(true);
            fabricPrivateRB.setDisable(true);
        } else if(objFabric.getStrFabricAccess().equalsIgnoreCase("Protected")){
            fabricTG.selectToggle(fabricProtectedRB);
        } else{
            fabricTG.selectToggle(fabricPrivateRB);
        }
            
        final PasswordField passPF= new PasswordField();
        final Label lblAlert = new Label();
        if(objConfiguration.getBlnAuthenticateService()){
            Label lblPassword=new Label(objDictionaryAction.getWord("SERVICEPASSWORD"));
            lblPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
            popup.add(lblPassword, 0, 2);
            passPF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
            passPF.setPromptText(objDictionaryAction.getWord("PROMPTSERVICEPASSWORD"));
            popup.add(passPF, 1, 2, 2, 1);                                
        }
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(250);
        popup.add(lblAlert, 0, 4, 3, 1);

        Button btnSave = new Button(objDictionaryAction.getWord("SAVE"));
        btnSave.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSAVE")));
        btnSave.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
        btnSave.setDefaultButton(true);
        btnSave.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                objFabric.setStrFabricAccess(fabricTG.getSelectedToggle().getUserData().toString());
                if(txtName.getText().trim().length()==0){
                    lblAlert.setText(objDictionaryAction.getWord("WRONGINPUT"));
                    lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
                } else{
                    if(objConfiguration.getBlnAuthenticateService()){
                        if(passPF.getText()!=null && passPF.getText()!="" && passPF.getText().trim().length()!=0){
                            if(Security.SecurePassword(passPF.getText(), objConfiguration.getObjUser().getStrUsername()).equals(objConfiguration.getObjUser().getStrAppPassword())){
                                if(isNew){
                                    if(txtName.getText()!=null && txtName.getText()!="" && txtName.getText().trim().length()!=0)
                                        objFabric.setStrFabricName(txtName.getText().toString());
                                    else
                                        objFabric.setStrFabricName("Unknown Fabric");
                                    saveFabric();
                                } else{
                                    updateFabric();
                                }
                                dialogStage.close();
                            } else{
                                lblAlert.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                                lblStatus.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                            }
                        } else {                            
                            lblAlert.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                            lblStatus.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                        }
                    } else{   // service password is disabled
                        if(isNew){
                            if(txtName.getText()!=null && txtName.getText()!="" && txtName.getText().trim().length()!=0)
                                objFabric.setStrFabricName(txtName.getText().toString());
                            else
                                objFabric.setStrFabricName("Unknown fabric");                        
                            saveFabric();
                        } else {
                            updateFabric();
                        }
                        dialogStage.close();
                    }
                }
            }
        });
        popup.add(btnSave, 1, 3);
        Button btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));
        btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
        btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        btnCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                dialogStage.close();
                lblStatus.setText(objDictionaryAction.getWord("TOOLTIPCANCEL"));
           }
        });
        popup.add(btnCancel, 0, 3);

        Scene scene = new Scene(popup, 300, 200);
        scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        dialogStage.setScene(scene);
        dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("SAVE"));
        dialogStage.showAndWait();        
    }
    private void saveAsHtml() {   
        try {
            FileChooser fileChoser=new FileChooser();
            fileChoser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTHTML")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
            FileChooser.ExtensionFilter extFilterHTML = new FileChooser.ExtensionFilter("HTML (*.html)", "*.html");
            fileChoser.getExtensionFilters().add(extFilterHTML);
            File ifile=fileChoser.showSaveDialog(fabricStage);
            if(ifile==null)
                return;
            else
                fabricStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+ifile.getAbsoluteFile().getName()+"]");
            
            BufferedImage texture = new BufferedImage(objFabric.getIntWarp(), objFabric.getIntWeft(),BufferedImage.TYPE_INT_RGB);        
            for(int x = 0; x < objFabric.getIntWeft(); x++) {
                for(int y = 0; y < objFabric.getIntWarp(); y++) {
                    if(objFabric.getFabricMatrix()[x][y]==0)
                        texture.setRGB(y, x, java.awt.Color.WHITE.getRGB());
                    else
                        texture.setRGB(y, x, java.awt.Color.BLACK.getRGB());
                }
            }
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
            Date date = new Date();
            String currentDate = dateFormat.format(date);
            String path = ifile.getPath()+"Fabric_"+objFabric.getStrFabricID()+"_"+currentDate;
            File pngFile = new File(path+".png");
            ImageIO.write(texture, "png", pngFile);
            File htmlFile = new File(path+".html");
            
            String INITIAL_TEXT = " This is coumputer generated, So no signature required";
            String IMAGE_TEXT = "<img src=\"file:\\\\" + 
                path+".png" + 
             "\" width=\""+objFabric.getIntWeft()+"\" height=\""+objFabric.getIntWarp()+"\" >";
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(htmlFile, true))) {
                bw.write("<table border=0 width=\"100%\">");
                    bw.newLine();
                    bw.write("<tr>");
                        bw.newLine();
                        bw.write("<td><h1>Digital India Corporation (Media Lab Asia)</h1></td>");
                        bw.newLine();
                        bw.write("<td><h6 align=right>");
                            bw.newLine();
                            bw.write(objDictionaryAction.getWord("PROJECT")+": Fabric_"+objFabric.getStrFabricID()+"_"+currentDate+"<br>");
                            bw.newLine();
                            bw.write("<a href=\"http://www.medialabasia.in\">http://www.medialabasia.in</a><br>");
                            bw.newLine();
                            bw.write("&copy; Digital India Corporation (Media Lab Asia); New Delhi, India<br><br>"+date);
                            bw.newLine();
                        bw.write("</h6></td>");
                    bw.newLine();
                    bw.write("</tr>");
                bw.newLine();
                bw.write("</table>");
                bw.newLine();
                bw.write("<table border='1' cellspacing='0' cellpadding='5'>");
                bw.newLine();
                bw.write("<tr align=right><th>Fabric Category</th><td>"+objFabric.getStrClothType()+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("FABRICTYPE")+"</th><td>"+objFabric.getStrFabricType()+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("NAME")+"</th><td>"+objFabric.getStrFabricName()+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("FABRICLENGTH")+"</th><td>"+objFabric.getDblFabricLength()+" inch</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("FABRICWIDTH")+"</th><td>"+objFabric.getDblFabricWidth()+" inch</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("REEDCOUNT")+"</th><td>"+objFabric.getIntReedCount()+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("DENTS")+"</th><td>"+objFabric.getIntDents()+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("TPD")+"</th><td>"+objFabric.getIntTPD()+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("PICKS")+"</th><td>"+objFabric.getIntWeft()+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("HOOKS")+"</th><td>"+objFabric.getIntHooks()+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("ENDS")+"</th><td>"+objFabric.getIntWarp()+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("PPI")+"</th><td>"+objFabric.getIntPPI()+" / inch</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("HPI")+"</th><td>"+objFabric.getIntHPI()+" / inch</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("EPI")+"</th><td>"+objFabric.getIntEPI()+" / inch</td></tr>");
                bw.newLine();
                if(objFabric.getStrArtworkID()!=null){
                    bw.newLine();
                    objFabric.setDblArtworkLength(Double.parseDouble(String.format("%.3f",objFabric.getDblArtworkLength())));
                    bw.write("<tr align=right><th>"+objDictionaryAction.getWord("ARTWORKLENGTH")+"</th><td>"+objFabric.getDblArtworkLength()+" inch</td></tr>");
                    bw.newLine();
                    objFabric.setDblArtworkWidth(Double.parseDouble(String.format("%.3f",objFabric.getDblArtworkWidth())));
                    bw.write("<tr align=right><th>"+objDictionaryAction.getWord("ARTWORKWIDTH")+"</th><td>"+objFabric.getDblArtworkWidth()+" inch</td></tr>");
                }   
                bw.newLine();
                objFabricAction = new FabricAction(false);
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("WARPYARNCONSUMPTION")+"</th><td>"+objFabricAction.getWarpNumber(objFabric)+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("WARPYARNWEIGHT")+"</th><td>"+Double.parseDouble(String.format("%.3f",objFabricAction.getWarpWeight(objFabric)))+" gram</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("WARPYARNLONG")+"</th><td>"+Double.parseDouble(String.format("%.3f",objFabricAction.getWarpLong(objFabric)))+" meter</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("WEFTYARNCONSUMPTION")+"</th><td>"+objFabricAction.getWeftNumber(objFabric)+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("WEFTYARNWEIGHT")+"</th><td>"+Double.parseDouble(String.format("%.3f",objFabricAction.getWeftWeight(objFabric)))+" gram</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("WEFTYARNLONG")+"</th><td>"+Double.parseDouble(String.format("%.3f",objFabricAction.getWeftLong(objFabric)))+" meter</td></tr>");
                bw.newLine();
                
                // Below code is redundant from ConsumptionView.calculateExtraWeftConsumption()
                double extraWeft = 0;
                int lineCount = 0;
                ArrayList<Byte> lstEntry = null;
                for (int i = 0; i < objFabric.getIntWeft(); i++){
                    lstEntry = new ArrayList();
                    for (int j = 0; j < objFabric.getIntWarp(); j++){
                        //add the first color on array
                        if(lstEntry.size()==0 && objFabric.getFabricMatrix()[i][j]!=1)
                            lstEntry.add(objFabric.getFabricMatrix()[i][j]);
                        //check for redudancy
                        else {                
                            if(!lstEntry.contains(objFabric.getFabricMatrix()[i][j]) && objFabric.getFabricMatrix()[i][j]!=1)
                                lstEntry.add(objFabric.getFabricMatrix()[i][j]);
                        }
                    }
                    lineCount+=lstEntry.size();
                }
                lineCount -=objFabric.getIntWeft();
                for(int i=0;i<objFabric.getColorWeave().length;i++){
                    if(objFabric.getColorWeave()[i][0]!=null && 
                        objFabric.getColorWeave()[i][1]!=null &&
                        objFabric.getColorWeave()[i][2]!=null &&
                        objFabric.getColorWeave()[i][3]!=null){
                        if(objFabric.getColorCountArtwork()<=Integer.parseInt(objFabric.getColorWeave()[i][2]))
                            extraWeft += Double.parseDouble(objFabric.getColorWeave()[i][3]);
                    }
                }
                objFabric.getObjConfiguration().setDblWeftLong(objFabricAction.getWeftLong(objFabric));
                objFabric.getObjConfiguration().setDblWeftWeight(objFabricAction.getWeftWeight(objFabric));
                objFabric.getObjConfiguration().setDblWeftNumber(objFabricAction.getWeftNumber(objFabric));
                objFabric.getObjConfiguration().setDblExtraWeftNumber(lineCount*objFabric.getObjConfiguration().getDblWeftNumber()/objFabric.getIntWeft());
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("EXTRAWEFTYARNCONSUMPTION")+"</th><td>"+objFabric.getObjConfiguration().getDblExtraWeftNumber()+"</td></tr>");
                bw.newLine();
                objFabric.getObjConfiguration().setDblExtraWeftLong(objFabricAction.getExtraWeftLong(objFabric));
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("EXTRAWEFTYARNLONG")+"</th><td>"+Double.parseDouble(String.format("%.3f",objFabric.getObjConfiguration().getDblExtraWeftLong()))+" meter</td></tr>");
                bw.newLine();
                objFabric.getObjConfiguration().setDblExtraWeftWeight(objFabricAction.getExtraWeftWeight(objFabric));
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("EXTRAWEFTYARNWEIGHT")+"</th><td>"+Double.parseDouble(String.format("%.3f",objFabric.getObjConfiguration().getDblExtraWeftWeight()))+" gram</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("EXTRAWEFTYARNUSEDLONG")+"</th><td>"+Double.parseDouble(String.format("%.3f",objFabric.getObjConfiguration().getDblWeftLong()*extraWeft/100))+" meter</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("EXTRAWEFTYARNUSEDWEIGHT")+"</th><td>"+Double.parseDouble(String.format("%.3f",objFabric.getObjConfiguration().getDblWeftWeight()*extraWeft/100))+" gram</td></tr>");
                bw.newLine();
                
                // adding GSM and Fabric Cover
                double warpCountNeC = objFabricAction.convertUnit(objFabric.getWarpYarn()[0].getStrYarnCountUnit(), "English Cotton (NeC)", objFabric.getWarpYarn()[0].getIntYarnCount());
                double weftCountNeC = objFabricAction.convertUnit(objFabric.getWeftYarn()[0].getStrYarnCountUnit(), "English Cotton (NeC)", objFabric.getWeftYarn()[0].getIntYarnCount());
                // calculate warp and weft weights and GSM
                double wpWt = (objFabric.getIntEPI()*39.37*0.59*(100+objFabric.getObjConfiguration().getIntWarpCrimp()))/(100*warpCountNeC);
                double wfWt = (objFabric.getIntPPI()*39.37*0.59*(100+objFabric.getObjConfiguration().getIntWeftCrimp()))/(100*weftCountNeC);
                double gsm = wpWt + wfWt;
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("GSM")+"</th><td>"+Double.parseDouble(String.format("%.3f", gsm))+" gram per sq. meter</td></tr>");
                bw.newLine();
                // calculate Fabric Cover
                double wpCover = objFabric.getIntEPI()/(Math.sqrt(warpCountNeC));
                double wfCover = objFabric.getIntPPI()/(Math.sqrt(weftCountNeC));
                double fabCover = wpCover + wfCover - ((wpCover*wfCover)/28);
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("WARP")+" "+objDictionaryAction.getWord("COVER")+"</th><td>"+Double.parseDouble(String.format("%.3f", wpCover))+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("WEFT")+" "+objDictionaryAction.getWord("COVER")+"</th><td>"+Double.parseDouble(String.format("%.3f", wfCover))+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("FABRIC")+" "+objDictionaryAction.getWord("COVER")+"</th><td>"+Double.parseDouble(String.format("%.3f", fabCover))+"</td></tr>");
                bw.newLine();
                bw.write("</table>");                
            bw.newLine();
            bw.write("");
            if(objFabric.getWarpYarn().length>0){
                bw.newLine();
                bw.write("<table border=1 cellspacing=0>");
                    bw.newLine();
                    PatternAction objPatternAction = new PatternAction();
                    bw.write("<caption>"+objDictionaryAction.getWord("WARP")+" "+objDictionaryAction.getWord("YARNPATTERN")+" \t"+objPatternAction.getPattern(objFabric.getStrWarpPatternID()).getStrPattern()+"</caption>");
                    bw.newLine();
                    bw.write("<tr align=right><th>"+objDictionaryAction.getWord("WARP")+"</th><th>"+objDictionaryAction.getWord("NAME")+"</th><th>"+objDictionaryAction.getWord("YARNTYPE")+"</th><th>"+objDictionaryAction.getWord("YARNREPEAT")+"</th><th>"+objDictionaryAction.getWord("YARNCOUNT")+"</th><th>"+objDictionaryAction.getWord("YARNUNIT")+"</th><th>"+objDictionaryAction.getWord("YARNPLY")+"</th><th>"+objDictionaryAction.getWord("YARNFACTOR")+"</th><th>"+objDictionaryAction.getWord("YARNDIAMETER")+"(mm)</th><th>"+objDictionaryAction.getWord("YARNWEIGHT")+"(kg)</th><th>"+objDictionaryAction.getWord("YARNPRICE")+"(kg)</th><th>"+objDictionaryAction.getWord("YARNTWIST")+"/ inch</th><th>"+objDictionaryAction.getWord("YARNSENCE")+"</th><th>"+objDictionaryAction.getWord("YARNHAIRNESS")+"</th><th>"+objDictionaryAction.getWord("YARNDISTRIBUTION")+"</th><th>"+objDictionaryAction.getWord("YARNCOLOR")+"</th></tr>");
                    for(int i = 0; i<objFabric.getWarpYarn().length;i++){
                        bw.newLine();
                        bw.write("<tr align=right><td>"+objFabric.getWarpYarn()[i].getStrYarnSymbol()+"</td><td>"+objFabric.getWarpYarn()[i].getStrYarnName()+"</td><td>"+objFabric.getWarpYarn()[i].getStrYarnType()+"</td><td>"+objFabric.getWarpYarn()[i].getIntYarnRepeat()+"</td><td>"+objFabric.getWarpYarn()[i].getIntYarnCount()+"</td><td>"+objFabric.getWarpYarn()[i].getStrYarnCountUnit()+"</td><td>"+objFabric.getWarpYarn()[i].getIntYarnPly()+"</td><td>"+objFabric.getWarpYarn()[i].getIntYarnDFactor()+"</td><td>"+objFabric.getWarpYarn()[i].getDblYarnDiameter()+"</td><td>-</td><td>"+objFabric.getWarpYarn()[i].getDblYarnPrice()+"</td><td>"+objFabric.getWarpYarn()[i].getIntYarnTwist()+"</td><td>"+objFabric.getWarpYarn()[i].getStrYarnTModel()+"</td><td>"+objFabric.getWarpYarn()[i].getIntYarnHairness()+"</td><td>"+objFabric.getWarpYarn()[i].getIntYarnHProbability()+"</td><th bgcolor=\""+objFabric.getWarpYarn()[i].getStrYarnColor()+"\">"+objFabric.getWarpYarn()[i].getStrYarnColor()+"</th></tr>");
                    }
                bw.newLine();
                bw.write("</table>");
            }
            if(objFabric.getWeftYarn().length>0){
                bw.newLine();
                bw.write("<table border=1 cellspacing=0>");
                    bw.newLine();
                    PatternAction objPatternAction = new PatternAction();
                    bw.write("<caption>"+objDictionaryAction.getWord("WEFT")+" "+objDictionaryAction.getWord("YARNPATTERN")+" \t "+objPatternAction.getPattern(objFabric.getStrWeftPatternID()).getStrPattern()+"</caption>");
                    bw.newLine();
                    bw.write("<tr align=right><th>"+objDictionaryAction.getWord("WEFT")+"</th><th>"+objDictionaryAction.getWord("NAME")+"</th><th>"+objDictionaryAction.getWord("YARNTYPE")+"</th><th>"+objDictionaryAction.getWord("YARNREPEAT")+"</th><th>"+objDictionaryAction.getWord("YARNCOUNT")+"</th><th>"+objDictionaryAction.getWord("YARNUNIT")+"</th><th>"+objDictionaryAction.getWord("YARNPLY")+"</th><th>"+objDictionaryAction.getWord("YARNFACTOR")+"</th><th>"+objDictionaryAction.getWord("YARNDIAMETER")+"(mm)</th><th>"+objDictionaryAction.getWord("YARNWEIGHT")+"(kg)</th><th>"+objDictionaryAction.getWord("YARNPRICE")+"(kg)</th><th>"+objDictionaryAction.getWord("YARNTWIST")+"/ inch</th><th>"+objDictionaryAction.getWord("YARNSENCE")+"</th><th>"+objDictionaryAction.getWord("YARNHAIRNESS")+"</th><th>"+objDictionaryAction.getWord("YARNDISTRIBUTION")+"</th><th>"+objDictionaryAction.getWord("YARNCOLOR")+"</th></tr>");
                    for(int i = 0; i<objFabric.getWeftYarn().length;i++){
                        bw.newLine();
                        bw.write("<tr align=right><td>"+objFabric.getWeftYarn()[i].getStrYarnSymbol()+"</td><td>"+objFabric.getWeftYarn()[i].getStrYarnName()+"</td><td>"+objFabric.getWeftYarn()[i].getStrYarnType()+"</td><td>"+objFabric.getWeftYarn()[i].getIntYarnRepeat()+"</td><td>"+objFabric.getWeftYarn()[i].getIntYarnCount()+"</td><td>"+objFabric.getWeftYarn()[i].getStrYarnCountUnit()+"</td><td>"+objFabric.getWeftYarn()[i].getIntYarnPly()+"</td><td>"+objFabric.getWeftYarn()[i].getIntYarnDFactor()+"</td><td>"+objFabric.getWeftYarn()[i].getDblYarnDiameter()+"</td><td>-</td><td>"+objFabric.getWeftYarn()[i].getDblYarnPrice()+"</td><td>"+objFabric.getWeftYarn()[i].getIntYarnTwist()+"</td><td>"+objFabric.getWeftYarn()[i].getStrYarnTModel()+"</td><td>"+objFabric.getWeftYarn()[i].getIntYarnHairness()+"</td><td>"+objFabric.getWeftYarn()[i].getIntYarnHProbability()+"</td><th bgcolor=\""+objFabric.getWeftYarn()[i].getStrYarnColor()+"\">"+objFabric.getWeftYarn()[i].getStrYarnColor()+"</th></tr>");
                    }
                bw.newLine();
                bw.write("</table>");
            }
            if(objFabric.getWarpExtraYarn().length>0){
                bw.newLine();
                bw.write("<table border=1 cellspacing=0>");
                    bw.newLine();
                    bw.write("<caption>"+objDictionaryAction.getWord("EXTRA")+" "+objDictionaryAction.getWord("WARP")+"</caption>");
                    bw.newLine();
                    bw.write("<tr align=right><th>"+objDictionaryAction.getWord("EXTRA")+" "+objDictionaryAction.getWord("WARP")+"</th><th>"+objDictionaryAction.getWord("NAME")+"</th><th>"+objDictionaryAction.getWord("YARNTYPE")+"</th><th>"+objDictionaryAction.getWord("YARNREPEAT")+"</th><th>"+objDictionaryAction.getWord("YARNCOUNT")+"</th><th>"+objDictionaryAction.getWord("YARNUNIT")+"</th><th>"+objDictionaryAction.getWord("YARNPLY")+"</th><th>"+objDictionaryAction.getWord("YARNFACTOR")+"</th><th>"+objDictionaryAction.getWord("YARNDIAMETER")+"(mm)</th><th>"+objDictionaryAction.getWord("YARNWEIGHT")+"(kg)</th><th>"+objDictionaryAction.getWord("YARNPRICE")+"(kg)</th><th>"+objDictionaryAction.getWord("YARNTWIST")+"/ inch</th><th>"+objDictionaryAction.getWord("YARNSENCE")+"</th><th>"+objDictionaryAction.getWord("YARNHAIRNESS")+"</th><th>"+objDictionaryAction.getWord("YARNDISTRIBUTION")+"</th><th>"+objDictionaryAction.getWord("YARNCOLOR")+"</th></tr>");
                    for(int i=0; i<objFabric.getIntExtraWarp(); i++){
                        bw.newLine();
                        bw.write("<tr align=right><td>"+objFabric.getWarpExtraYarn()[i].getStrYarnSymbol()+"</td><td>"+objFabric.getWarpExtraYarn()[i].getStrYarnName()+"</td><td></td><td>"+objFabric.getWarpExtraYarn()[i].getStrYarnType()+"</td><td>"+objFabric.getWarpExtraYarn()[i].getIntYarnRepeat()+"</td><td>"+objFabric.getWarpExtraYarn()[i].getIntYarnCount()+"</td><td>"+objFabric.getWarpExtraYarn()[i].getStrYarnCountUnit()+"</td><td>"+objFabric.getWarpExtraYarn()[i].getIntYarnPly()+"</td><td>"+objFabric.getWarpExtraYarn()[i].getIntYarnDFactor()+"</td><td>"+objFabric.getWarpExtraYarn()[i].getDblYarnDiameter()+"</td><td>-</td><td>"+objFabric.getWarpExtraYarn()[i].getDblYarnPrice()+"</td><td>"+objFabric.getWarpExtraYarn()[i].getIntYarnTwist()+"</td><td>"+objFabric.getWarpExtraYarn()[i].getStrYarnTModel()+"</td><td>"+objFabric.getWarpExtraYarn()[i].getIntYarnHairness()+"</td><td>"+objFabric.getWarpExtraYarn()[i].getIntYarnHProbability()+"</td><th bgcolor=\""+objFabric.getWarpExtraYarn()[i].getStrYarnColor()+"\">"+objFabric.getWarpExtraYarn()[i].getStrYarnColor()+"</th></tr>");
                    }
                bw.newLine();
                bw.write("</table>");
            }
            if(objFabric.getWeftExtraYarn().length>0){
                bw.newLine();
                bw.write("<table border=1 cellspacing=0>");
                    bw.newLine();
                    bw.write("<caption>"+objDictionaryAction.getWord("EXTRA")+" "+objDictionaryAction.getWord("WEFT")+"</caption>");
                    bw.newLine();
                    bw.write("<tr align=right><th>"+objDictionaryAction.getWord("EXTRA")+" "+objDictionaryAction.getWord("WEFT")+"</th><th>"+objDictionaryAction.getWord("NAME")+"</th><th>"+objDictionaryAction.getWord("YARNTYPE")+"</th><th>"+objDictionaryAction.getWord("YARNREPEAT")+"</th><th>"+objDictionaryAction.getWord("YARNCOUNT")+"</th><th>"+objDictionaryAction.getWord("YARNUNIT")+"</th><th>"+objDictionaryAction.getWord("YARNPLY")+"</th><th>"+objDictionaryAction.getWord("YARNFACTOR")+"</th><th>"+objDictionaryAction.getWord("YARNDIAMETER")+"(mm)</th><th>"+objDictionaryAction.getWord("YARNWEIGHT")+"(kg)</th><th>"+objDictionaryAction.getWord("YARNPRICE")+"(kg)</th><th>"+objDictionaryAction.getWord("YARNTWIST")+"/ inch</th><th>"+objDictionaryAction.getWord("YARNSENCE")+"</th><th>"+objDictionaryAction.getWord("YARNHAIRNESS")+"</th><th>"+objDictionaryAction.getWord("YARNDISTRIBUTION")+"</th><th>"+objDictionaryAction.getWord("YARNCOLOR")+"</th></tr>");
                    for(int i=0; i<objFabric.getIntExtraWeft(); i++){
                        bw.newLine();
                        bw.write("<tr align=right><td>"+objFabric.getWeftExtraYarn()[i].getStrYarnSymbol()+"</td><td>"+objFabric.getWeftExtraYarn()[i].getStrYarnName()+"</td><td>"+objFabric.getWeftExtraYarn()[i].getStrYarnType()+"</td><td>"+objFabric.getWeftExtraYarn()[i].getIntYarnRepeat()+"</td><td>"+objFabric.getWeftExtraYarn()[i].getIntYarnCount()+"</td><td>"+objFabric.getWeftExtraYarn()[i].getStrYarnCountUnit()+"</td><td>"+objFabric.getWeftExtraYarn()[i].getIntYarnPly()+"</td><td>"+objFabric.getWeftExtraYarn()[i].getIntYarnDFactor()+"</td><td>"+objFabric.getWeftExtraYarn()[i].getDblYarnDiameter()+"</td><td>-</td><td>"+objFabric.getWeftExtraYarn()[i].getDblYarnPrice()+"</td><td>"+objFabric.getWeftExtraYarn()[i].getIntYarnTwist()+"</td><td>"+objFabric.getWeftExtraYarn()[i].getStrYarnTModel()+"</td><td>"+objFabric.getWeftExtraYarn()[i].getIntYarnHairness()+"</td><td>"+objFabric.getWeftExtraYarn()[i].getIntYarnHProbability()+"</td><th bgcolor=\""+objFabric.getWeftExtraYarn()[i].getStrYarnColor()+"\">"+objFabric.getWeftExtraYarn()[i].getStrYarnColor()+"</th></tr>");
                    }
                bw.newLine();
                bw.write("</table>");
            }
            bw.newLine();
            bw.write("<br><b>"+objDictionaryAction.getWord("GRAPHVIEW")+"<b><br>");
            bw.newLine();
            bw.write(IMAGE_TEXT);
            bw.newLine();
            bw.write("<br>"+INITIAL_TEXT+"<br>");
            bw.newLine();
            } catch (IOException ex) {
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            } catch (SQLException ex) {
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            } catch (Exception ex) {
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
            
            if(objConfiguration.getBlnAuthenticateService()){
                ArrayList<File> filesToZip=new ArrayList<>();
                filesToZip.add(pngFile);
                filesToZip.add(htmlFile);
                String zipFilePath=path+".zip";
                String passwordToZip = ifile.getName()+"Fabric_"+objFabric.getStrFabricID()+"_"+currentDate;
                new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                pngFile.delete();
                htmlFile.delete();
            }
            
            lblStatus.setText("HTML "+objDictionaryAction.getWord("EXPORTEDTO")+" "+path+".html");
        } catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    
    private void saveAsTxtGraph(boolean numericColorMode, boolean invertDesign){
        try {
            String strGraphData="";
            int intLength = objFabric.getIntWarp();
            int intHeight = objFabric.getIntWeft(); 
            int rgb = 0;
            int intBoards=0;
            BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            for(int i=(invertDesign?intHeight-1:0); (invertDesign?i>=0:i<intHeight); i=(invertDesign?i-1:i+1)){
                String strLineData = "";
                for(int j=0; j<intLength; j++){
                    if(objFabric.getIntExtraWarp()>0 || objFabric.getIntExtraWeft()>0){
                        if(objFabric.getFabricMatrix()[i][j]>1 || objFabric.getFabricMatrix()[i][j]<-1){
                            if(numericColorMode)
                                strLineData +=(j+1)+",";
                            rgb = new java.awt.Color((float)javafx.scene.paint.Color.web("#000000").getRed(),(float)javafx.scene.paint.Color.web("#000000").getGreen(),(float)javafx.scene.paint.Color.web("#000000").getBlue()).getRGB();
                        } else {
                            if(!numericColorMode)
                                strLineData +=(j+1)+",";
                            rgb = new java.awt.Color((float)javafx.scene.paint.Color.web("#FFFFFF").getRed(),(float)javafx.scene.paint.Color.web("#FFFFFF").getGreen(),(float)javafx.scene.paint.Color.web("#FFFFFF").getBlue()).getRGB();
                        }
                    }else{
                        if(objFabric.getFabricMatrix()[i][j]==1){
                            if(numericColorMode)
                                strLineData +=(j+1)+",";
                            rgb = new java.awt.Color((float)javafx.scene.paint.Color.web("#000000").getRed(),(float)javafx.scene.paint.Color.web("#000000").getGreen(),(float)javafx.scene.paint.Color.web("#000000").getBlue()).getRGB();
                        } else {
                            if(!numericColorMode)
                                strLineData +=(j+1)+",";
                            rgb = new java.awt.Color((float)javafx.scene.paint.Color.web("#FFFFFF").getRed(),(float)javafx.scene.paint.Color.web("#FFFFFF").getGreen(),(float)javafx.scene.paint.Color.web("#FFFFFF").getBlue()).getRGB();
                        } 
                    }
                    bufferedImage.setRGB(j, i, rgb);
                }
                if(strLineData.trim().length()>0){
                    intBoards++;
                    strGraphData += "\n"+ (invertDesign?intHeight-i:i+1)+":>"+DigiBunaiCommons.replaceStrWithHyphen(strLineData.substring(0, strLineData.length()-1))+"||";
                }
                strLineData = null;
            }
            strGraphData = "Thread : "+intLength+" Board : "+intBoards+"\nDescription\n\n"+strGraphData;
            
            DirectoryChooser directoryChooser=new DirectoryChooser();
            fabricStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTBMP")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
            File selectedDirectory = directoryChooser.showDialog(fabricStage);
            if(selectedDirectory == null)
                return;
            else
                fabricStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+selectedDirectory.getAbsolutePath()+"]");
            
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
            Date date = new Date();
            String currentDate = dateFormat.format(date);
            String path = selectedDirectory.getPath()+"\\Fabric_"+currentDate+"\\";
            File file = new File(path);            
            if (!file.exists()) {
                if (!file.mkdir())
                    path = selectedDirectory.getPath();
            }
            ArrayList<File> filesToZip=new ArrayList<>();            
            //adding txt graph file
            file = new File(path +"data.txt");
            //System.err.println(objWeave.getStrWeaveFile());
            FileWriter writer = new FileWriter(file);
            writer.write(strGraphData);
            writer.close();
            filesToZip.add(file);
            //adding image file
            file = new File(path + "graph.bmp");
            ImageIO.write(bufferedImage, "BMP", file);
            filesToZip.add(file);
            if(objConfiguration.getBlnAuthenticateService()){
                //prepare archive folder
                String zipFilePath=selectedDirectory.getPath()+"\\Fabric_"+currentDate+".zip";
                String passwordToZip = "Fabric_"+currentDate;
                new EncryptZip(zipFilePath, filesToZip, passwordToZip);            
                //delete folder recursively
                recursiveDelete(new File(path));
            }
            bufferedImage = null;
            lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+path);
        } catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }   
    }
    private void saveAsImage() {   
        try {
            int intLength = objFabric.getIntWarp();
            int intHeight = objFabric.getIntWeft();
            BufferedImage texture = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            if(yarnSimulationMode){ // export simulated fabric as it appears
                texture = SwingFXUtils.fromFXImage(fabric.snapshot(null, null), null);
            } else {
                BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
                objFabricAction = new FabricAction();
                bufferedImage = objFabricAction.plotFrontSideView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), objFabric.getIntWarp(), objFabric.getIntWeft());
                //intLength = (int)(((objConfiguration.getIntDPI()*objFabric.getIntWarp())/objFabric.getIntEPI())*zoomfactor);
                //intHeight = (int)(((objConfiguration.getIntDPI()*objFabric.getIntWeft())/objFabric.getIntPPI())*zoomfactor);

                Graphics2D g = texture.createGraphics();
                g.drawImage(bufferedImage, 0, 0, intLength, intHeight, null);
                g.dispose();
            }
            
            FileChooser fileChoser=new FileChooser();
            fileChoser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTPNG")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
            FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG (*.png)", "*.png");
            fileChoser.getExtensionFilters().add(extFilterPNG);
            File file=fileChoser.showSaveDialog(fabricStage);
            if(file==null)
                return;
            else
                fabricStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+file.getAbsoluteFile().getName()+"]");
            if (!file.getName().endsWith("png")) 
                file = new File(file.getPath() +".png");
            ImageIO.write(texture, "png", file);
            
            if(objConfiguration.getBlnAuthenticateService()){
                ArrayList<File> filesToZip=new ArrayList<>();
                filesToZip.add(file);
                String zipFilePath=file.getAbsolutePath()+".zip";
                String passwordToZip = file.getName();
                new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                file.delete();
            }

            lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+file.getCanonicalPath());
            /*
            BufferedImage bufferedImage = new BufferedImage(objFabric.getIntWarp(), objFabric.getIntWeft(),BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            bufferedImage = objFabricAction.plotFrontSideView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), objFabric.getIntWarp(), objFabric.getIntWeft());
            FileChooser fileChoser=new FileChooser();
            fileChoser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTPNG")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
            FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG (*.png)", "*.png");
            fileChoser.getExtensionFilters().add(extFilterPNG);
            File file=fileChoser.showSaveDialog(fabricStage);
            if(file==null)
                return;
            else
                fabricStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+file.getAbsoluteFile().getName()+"]");
            if (!file.getName().endsWith("png")) 
                file = new File(file.getPath() +".png");
            ImageIO.write(bufferedImage, "png", file);
            lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+file.getCanonicalPath());
            */
        } catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch(OutOfMemoryError ex){
            new Logging("SEVERE",getClass().getName(),ex.toString(),null);
            lblStatus.setText(objDictionaryAction.getWord("MAXVALUE"));
        }
    }
    private void saveAsGrid() {   
        try {            
            int intLength = objFabric.getIntWarp();
            int intHeight = objFabric.getIntWeft();
            lblStatus.setText(objDictionaryAction.getWord("GETGRIDVIEW"));
            BufferedImage bufferedImage = new BufferedImage(intHeight, intLength, BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            
            BufferedImage texture = new BufferedImage(objFabric.getIntWarp(), objFabric.getIntWeft(),BufferedImage.TYPE_INT_RGB);
            for(int x = 0; x < objFabric.getIntWeft(); x++) {
                for(int y = 0; y < objFabric.getIntWarp(); y++) {
                    if(objFabric.getFabricMatrix()[x][y]==0)
                        texture.setRGB(y, x, java.awt.Color.WHITE.getRGB());
                    else
                        texture.setRGB(y, x, java.awt.Color.BLACK.getRGB());
                }
            }
            FileChooser fileChoser=new FileChooser();
            fileChoser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTBMP")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
            FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("BMP (.bmp)", "*.bmp");
            fileChoser.getExtensionFilters().add(extFilterBMP);
            File file=fileChoser.showSaveDialog(fabricStage);
            if(file==null)
                return;
            else
                fabricStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+file.getAbsoluteFile().getName()+"]");
            if (!file.getName().endsWith("bmp")) 
                file = new File(file.getPath() +".bmp");
            ImageIO.write(texture, "BMP", file);
            
            if(objConfiguration.getBlnAuthenticateService()){
                ArrayList<File> filesToZip=new ArrayList<>();
                filesToZip.add(file);
                String zipFilePath=file.getAbsolutePath()+".zip";
                String passwordToZip = file.getName();
                new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                file.delete();
            }

            lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+file.getCanonicalPath());
        } catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch(OutOfMemoryError ex){
            new Logging("SEVERE",getClass().getName(),ex.toString(),null);
            lblStatus.setText(objDictionaryAction.getWord("MAXVALUE"));
        }
    }
    private void saveAsGraph(){
        try {
            int intLength = objFabric.getIntWarp();
            int intHeight = objFabric.getIntWeft();
            lblStatus.setText(objDictionaryAction.getWord("GETGRAPHVIEW"));
            BufferedImage bufferedImage = new BufferedImage(intHeight, intLength, BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            
            //================================ For Outer to be square ===============================
            if(objFabric.getIntExtraWarp()>0 || objFabric.getIntExtraWeft()>0){
                if(objConfiguration.getBlnPunchCard()){
                    bufferedImage = objFabricAction.plotGraphJaquardMachineView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                    intLength=objFabric.getIntWarp()*(objFabric.getIntExtraWarp()+objFabric.getIntExtraWeft()+2);
                } else{
                    bufferedImage = objFabricAction.plotGraphJaquardView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                }
            }else{
                bufferedImage = objFabricAction.plotGraphDobbyView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
            }
            
            String[] data = objFabric.getObjConfiguration().getStrGraphSize().split("x");
            BufferedImage texture = new BufferedImage((int)(intLength*zoomfactor*Integer.parseInt(data[1])), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])),BufferedImage.TYPE_INT_RGB);
            Graphics2D g = texture.createGraphics();
            g.drawImage(bufferedImage, 0, 0, (int)(intLength*zoomfactor*Integer.parseInt(data[1])), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])), null);
            g.setColor(java.awt.Color.BLACK);
            BasicStroke bs = new BasicStroke(2);
            g.setStroke(bs);
            
            for(int i = 0; i < objFabric.getIntWeft(); i++) {
                for(int j = 0; j < intLength; j++) {
                    if((j%Integer.parseInt(data[0]))==0){
                        bs = new BasicStroke(2);
                        g.setStroke(bs);
                    }else{
                        bs = new BasicStroke(1);
                        g.setStroke(bs);
                    }
                    g.drawLine((int)(j*zoomfactor*Integer.parseInt(data[1])), 0,  (int)(j*zoomfactor*Integer.parseInt(data[1])), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])));
                }
                if((i%Integer.parseInt(data[1]))==0){
                    bs = new BasicStroke(2);
                    g.setStroke(bs);
                }else{
                    bs = new BasicStroke(1);
                    g.setStroke(bs);
                }
                g.drawLine(0, (int)(i*zoomfactor*Integer.parseInt(data[0])), (int)(intLength*zoomfactor)*Integer.parseInt(data[1]), (int)(i*zoomfactor*Integer.parseInt(data[0])));
            }
            // cover right and bottom boundary lines
            g.drawLine((int)(intLength*zoomfactor*Integer.parseInt(data[1]))-1, 0,  (int)(intLength*zoomfactor*Integer.parseInt(data[1]))-1, (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])));
            g.drawLine(0, (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0]))-1, (int)(intLength*zoomfactor)*Integer.parseInt(data[1]), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0]))-1);
            /*
            ============================= For Inner to be square ===============================
            if(objFabric.getIntExtraWarp()>0 || objFabric.getIntExtraWeft()>0){
                bufferedImage = objFabricAction.plotGraphJaquardView(objFabric, intWarp, intWeft);
            }else{
                bufferedImage = objFabricAction.plotGraphDobbyView(objFabric, intWarp, intWeft);
            }
            BufferedImage texture = new BufferedImage((int)(intWeft), (int)(intWarp)+100,BufferedImage.TYPE_INT_RGB);
            Graphics2D g = texture.createGraphics();
            g.drawImage(bufferedImage, 0, 0, (int)(intWeft), (int)(intWarp), null);
            g.setColor(java.awt.Color.BLACK);
            BasicStroke bs = new BasicStroke(2);
            g.setStroke(bs);
            String[] data = objConfiguration.getStrGraphSize().split("x");
            for(int x = 0; x < (int)(intWarp); x+=(int)(objConfiguration.getIntBoxSize())) {
                for(int y = 0; y < (int)(intWeft); y+=(int)(objConfiguration.getIntBoxSize())) {
                    if(y%(int)(Integer.parseInt(data[0]))==0){
                        bs = new BasicStroke(2);
                        g.setStroke(bs);
                    }else{
                        bs = new BasicStroke(1);
                        g.setStroke(bs);
                    }
                    g.drawLine(y, 0,  y, (int)(intWarp));
                }
                if(x%(int)(Integer.parseInt(data[1]))==0){
                    bs = new BasicStroke(2);
                    g.setStroke(bs);
                }else{
                    bs = new BasicStroke(1);
                    g.setStroke(bs);
                }
                g.drawLine(0, x, (int)(intWeft), x);
            }
            */
			//adding data on the garphs
            /*
            g.setColor(java.awt.Color.WHITE);
            bs = new BasicStroke(3);
            g.setStroke(bs);
            g.drawString(objDictionaryAction.getWord("LOOM")+" "+objDictionaryAction.getWord("SPECIFICATIONS")+" : ", 10, (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(1*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("REEDCOUNT")+" : "+objFabric.getIntReedCount(), (int)(intLength*Integer.parseInt(data[0])/2), (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(1*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("DENTS")+" : "+objFabric.getIntDents(), 10, (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(2*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("PPI")+" : "+objFabric.getIntPPI(), (int)(intLength*Integer.parseInt(data[0])/2), (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(2*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("PICKS")+" : "+objFabric.getIntWeft(), 10, (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(3*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("HPI")+" : "+objFabric.getIntHPI(), (int)(intLength*Integer.parseInt(data[0])/2), (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(3*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("HOOKS")+" : "+objFabric.getIntHooks(), 10, (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(4*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("EPI")+" : "+objFabric.getIntEPI(), (int)(intLength*Integer.parseInt(data[0])/2), (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(4*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("ENDS")+" : "+objFabric.getIntWarp(), 10, (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(5*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("TPD")+" : "+objFabric.getIntTPD(), (int)(intLength*Integer.parseInt(data[0])/2), (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(5*g.getFontMetrics().getHeight()));
			*/
            g.dispose();
            bufferedImage = null;
            FileChooser fileChoser=new FileChooser();
            fileChoser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTBMP")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
            FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("BMP (.bmp)", "*.bmp");
            fileChoser.getExtensionFilters().add(extFilterBMP);
            File file=fileChoser.showSaveDialog(fabricStage);
            if(file==null)
                return;
            else
                fabricStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+file.getAbsoluteFile().getName()+"]");
            if (!file.getName().endsWith("bmp")) 
                file = new File(file.getPath() +".bmp");
            ImageIO.write(texture, "BMP", file);
            
            if(objConfiguration.getBlnAuthenticateService()){
                ArrayList<File> filesToZip=new ArrayList<>();
                filesToZip.add(file);
                String zipFilePath=file.getAbsolutePath()+".zip";
                String passwordToZip = file.getName();
                new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                file.delete();
            }

            lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+file.getCanonicalPath());
        } catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch(OutOfMemoryError ex){
            new Logging("SEVERE",getClass().getName(),ex.toString(),null);
            lblStatus.setText(objDictionaryAction.getWord("MAXVALUE"));
        }
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        new FabricView(stage);
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
/////////////////////////////// U N U S E D    C O D E ////////////////////////    
    /*
    private void printSetupAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONPRINTSETUPFILE"));
        objPrinterJob.pageDialog(objPrinterAttribute);
    }
    private void printScreenAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONPRINTSCREENFILE"));
        printScreen();
    }
    private void printGridAction(){
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
            lblStatus.setText(objDictionaryAction.getWord("ACTIONPRINTGRIDFILE"));
            printGrid();
        }
    }
    private void printGraphAction(){
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
            lblStatus.setText(objDictionaryAction.getWord("ACTIONPRINTGRAPHFILE"));
            printGraph();
        }
    }
    private void printTextureAction(){
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
            lblStatus.setText(objDictionaryAction.getWord("ACTIONPRINTTEXTUREFILE"));
            printTexture();
        }
    }
    private void printScreen() {   
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        //dialogStage.initModality(Modality.WINDOW_MODAL);
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setMinSize(objConfiguration.WIDTH/2, objConfiguration.HEIGHT/2);

        Button btnPrint = new Button(objDictionaryAction.getWord("PRINT"));
        btnPrint.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPPRINT")));
        btnPrint.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/print.png"));
        btnPrint.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                new MessageView(objConfiguration);
                if(objConfiguration.getServicePasswordValid()){
                    objConfiguration.setServicePasswordValid(false);
                    try {
                        if(objPrinterJob.printDialog()){
                            objPrinterJob.setPrintable(new Printable() {
                                @Override
                                public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
                                    if (pageIndex != 0) {
                                        return NO_SUCH_PAGE;
                                    }
                                    
                                    //objPrinterJob.getJobSettings().setPageRanges(new PageRange(1, numPages));
                                    //JobSettings js = job.getJobSettings();
                                    //    for (PageRange pr : js.getPageRanges()) {
                                    //        for (int p = pr.getStartPage(); p <= pr.getEndPage(); p++) {
                                    //            boolean ok = job.printPage(...code to get your node for the page...);
                                    //            ...take action on success/failure etc.
                                    //        }
                                    //    }
                                    
                                    BufferedImage texture = SwingFXUtils.fromFXImage(fabric.getImage(), null);
                                    Graphics2D g2d=(Graphics2D)graphics;
                                    g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
                                    
                                    //graphics.drawImage(texture, 0, 0, texture.getWidth(), texture.getHeight(), null);
                                    graphics.drawImage(texture, 0, 0, (int)pageFormat.getImageableWidth(), (int)pageFormat.getImageableWidth(), null);
                                    return PAGE_EXISTS;                    
                                }
                            });    
                            objPrinterJob.print();
                        }
                        dialogStage.close();
                    } catch (PrinterException ex) {
                        lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                        new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                    } catch (Exception ex) {
                        new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                        lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                    }
                }
            }
        });
        popup.add(btnPrint, 0, 0);
        Button btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));
        btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
        btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        btnCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                dialogStage.close();
           }
        });
        popup.add(btnCancel, 1, 0);
        
        BufferedImage texture = SwingFXUtils.fromFXImage(fabric.getImage(), null);
        ImageView printImage = new ImageView();
        printImage.setImage(SwingFXUtils.toFXImage(texture, null));        
        popup.add(printImage, 0, 1, 2, 1);

        Scene scene=new Scene(popup);
        scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        dialogStage.setScene(scene);
        dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+":"+objDictionaryAction.getWord("PRINT")+" "+objDictionaryAction.getWord("PREVIEW"));
        dialogStage.showAndWait();   
    }
    private void printGrid() {
        BufferedImage texture = new BufferedImage(objFabric.getIntWarp(), objFabric.getIntWeft(),BufferedImage.TYPE_INT_RGB);
        for(int x = 0; x < objFabric.getIntWeft(); x++) {
            for(int y = 0; y < objFabric.getIntWarp(); y++) {
                if(objFabric.getFabricMatrix()[x][y]==0)
                    texture.setRGB(y, x, java.awt.Color.WHITE.getRGB());
                else
                    texture.setRGB(y, x, java.awt.Color.BLACK.getRGB());
            }
        }
       try {            
            if(objPrinterJob.printDialog()){
                objPrinterJob.setPrintable(new Printable() {
                    @Override
                    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
                        if (pageIndex != 0) {
                            return NO_SUCH_PAGE;
                        }
                        BufferedImage texture = new BufferedImage(objFabric.getIntWarp(), objFabric.getIntWeft(),BufferedImage.TYPE_INT_RGB);
                        for(int x = 0; x < objFabric.getIntWeft(); x++) {
                            for(int y = 0; y < objFabric.getIntWarp(); y++) {
                                if(objFabric.getFabricMatrix()[x][y]==0)
                                    texture.setRGB(y, x, java.awt.Color.WHITE.getRGB());
                                else
                                    texture.setRGB(y, x, java.awt.Color.BLACK.getRGB());
                            }
                        }
                        Graphics2D g2d=(Graphics2D)graphics;
                        g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
                    
                        graphics.drawImage(texture, 0, 0, texture.getWidth(), texture.getHeight(), null);
                        return PAGE_EXISTS;                    
                    }
                });     
                objPrinterJob.print();
            }
        } catch (PrinterException ex) {             
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);  
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void printGraphOld(){
        try {
            int intLength = objFabric.getIntWarp();
            int intHeight = objFabric.getIntWeft(); 
            lblStatus.setText(objDictionaryAction.getWord("GETGRAPHVIEW"));
            BufferedImage bufferedImage = new BufferedImage(objFabric.getIntWarp(), objFabric.getIntWeft(),BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            
            //================================ For Outer to be square ===============================
            if(objFabric.getIntExtraWarp()>0 || objFabric.getIntExtraWeft()>0){
                if(objConfiguration.getBlnPunchCard()){
                    bufferedImage = objFabricAction.plotGraphJaquardMachineView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                    intLength=objFabric.getIntWarp()*(objFabric.getIntExtraWeft()+2);
                } else{
                    bufferedImage = objFabricAction.plotGraphJaquardView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                }
            }else{
                bufferedImage = objFabricAction.plotGraphDobbyView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
            }
            
            String[] data = objFabric.getObjConfiguration().getStrGraphSize().split("x");
            final BufferedImage texture = new BufferedImage((int)(intLength*zoomfactor*Integer.parseInt(data[1])), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0]))+100,BufferedImage.TYPE_INT_RGB);
            Graphics2D g = texture.createGraphics();
            g.drawImage(bufferedImage, 0, 0, (int)(intLength*zoomfactor*Integer.parseInt(data[1])), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])), null);
            g.setColor(java.awt.Color.BLACK);
            BasicStroke bs = new BasicStroke(2);
            g.setStroke(bs);
            
            for(int i = 0; i < objFabric.getIntWeft(); i++) {
                for(int j = 0; j < intLength; j++) {
                    if((j%Integer.parseInt(data[0]))==0){
                        bs = new BasicStroke(2);
                        g.setStroke(bs);
                    }else{
                        bs = new BasicStroke(1);
                        g.setStroke(bs);
                    }
                    g.drawLine((int)(j*zoomfactor*Integer.parseInt(data[1])), 0,  (int)(j*zoomfactor*Integer.parseInt(data[1])), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])));
                }
                if((i%Integer.parseInt(data[1]))==0){
                    bs = new BasicStroke(2);
                    g.setStroke(bs);
                }else{
                    bs = new BasicStroke(1);
                    g.setStroke(bs);
                }
                g.drawLine(0, (int)(i*zoomfactor*Integer.parseInt(data[0])), (int)(intLength*zoomfactor)*Integer.parseInt(data[1]), (int)(i*zoomfactor*Integer.parseInt(data[0])));
            }
           
            g.setColor(java.awt.Color.WHITE);
            bs = new BasicStroke(3);
            g.setStroke(bs);
            g.drawString(objDictionaryAction.getWord("LOOM")+" "+objDictionaryAction.getWord("SPECIFICATIONS")+" : ", 10, (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(1*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("REEDCOUNT")+" : "+objFabric.getIntReedCount(), (int)(intLength*Integer.parseInt(data[0])/2), (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(1*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("DENTS")+" : "+objFabric.getIntDents(), 10, (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(2*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("PPI")+" : "+objFabric.getIntPPI(), (int)(intLength*Integer.parseInt(data[0])/2), (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(2*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("PICKS")+" : "+objFabric.getIntWeft(), 10, (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(3*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("HPI")+" : "+objFabric.getIntHPI(), (int)(intLength*Integer.parseInt(data[0])/2), (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(3*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("HOOKS")+" : "+objFabric.getIntHooks(), 10, (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(4*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("EPI")+" : "+objFabric.getIntEPI(), (int)(intLength*Integer.parseInt(data[0])/2), (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(4*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("ENDS")+" : "+objFabric.getIntWarp(), 10, (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(5*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("TPD")+" : "+objFabric.getIntTPD(), (int)(intLength*Integer.parseInt(data[0])/2), (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(5*g.getFontMetrics().getHeight()));
            g.dispose();
            bufferedImage = null;
            if(objPrinterJob.printDialog()){                
                if(objConfiguration.getBlnMPrint()){
                    //System.err.println(objPrinterJob.getPageFormat(objPrinterAttribute).getImageableX()+""+objPrinterJob.getPageFormat(objPrinterAttribute).getImageableX());
                    double h = objPrinterJob.getPageFormat(objPrinterAttribute).getImageableHeight();
                    double w = objPrinterJob.getPageFormat(objPrinterAttribute).getImageableWidth();
                    BufferedImage texture1 = new BufferedImage((int)(w*Math.ceil(texture.getWidth()/w)), (int)(h*Math.ceil(texture.getHeight()/h)),BufferedImage.TYPE_INT_RGB);
                    Graphics2D g1 = texture1.createGraphics();
                    g1.drawImage(texture, 0, 0, (int)(w*Math.ceil(texture.getWidth()/w)), (int)(h*Math.ceil(texture.getHeight()/h)), null);
                    g1.dispose();
                    
                    final BufferedImage page = new BufferedImage((int)w, (int)h, BufferedImage.TYPE_INT_RGB);
                    for(int i=0; i<Math.ceil(texture.getHeight()/h);i++){
                        for(int j=0; j<Math.ceil(texture.getWidth()/w);j++){
                            page.getGraphics().drawImage(texture1, 0, 0, (int)w, (int)h, (int)(j*w), (int)(i*h), (int)((j+1)*w), (int)((i+1)*h), null);
                            objPrinterJob.setPrintable(new Printable() {
                                @Override
                                public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
                                    if (pageIndex != 0) {
                                        return NO_SUCH_PAGE;
                                    }
                                    //graphics.drawImage(texture, 0, 0, texture.getWidth(), texture.getHeight(), null);
                                    Graphics2D g2d=(Graphics2D)graphics;
                                    g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
                                    graphics.drawImage(page, 0, 0, (int)pageFormat.getImageableWidth(), (int)pageFormat.getImageableHeight(), null);
                                    return PAGE_EXISTS;                    
                                }
                            });     
                            objPrinterJob.print();
                        }
                    }
                }else{
                    objPrinterJob.setPrintable(new Printable() {
                        @Override
                        public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
                            if (pageIndex != 0) {
                                return NO_SUCH_PAGE;
                            }
                            //graphics.drawImage(texture, 0, 0, texture.getWidth(), texture.getHeight(), null);
                            Graphics2D g2d=(Graphics2D)graphics;
                            g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
                            graphics.drawImage(texture, 0, 0, (int)pageFormat.getImageableWidth(), (int)pageFormat.getImageableHeight(), null);
                            return PAGE_EXISTS;                    
                        }
                    });     
                    objPrinterJob.print();
                }
            }
        } catch (PrinterException ex) {             
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);  
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void printTexture() {
        int intLength = objFabric.getIntWarp();
        int intHeight = objFabric.getIntWeft(); 
        BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
        try {
            objFabricAction = new FabricAction();
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);  
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
        bufferedImage = objFabricAction.plotFrontSideView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), objFabric.getIntWarp(), objFabric.getIntWeft());

        intLength = (int)(((objConfiguration.getIntDPI()*objFabric.getIntWarp())/objFabric.getIntEPI())*zoomfactor);
        intHeight = (int)(((objConfiguration.getIntDPI()*objFabric.getIntWeft())/objFabric.getIntPPI())*zoomfactor);
        BufferedImage texture = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);

        Graphics2D g = texture.createGraphics();
        g.drawImage(bufferedImage, 0, 0, intLength, intHeight, null);
        g.dispose();
        try {            
            if(objPrinterJob.printDialog()){
                objPrinterJob.setPrintable(new Printable() {
                    @Override
                    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
                        if (pageIndex != 0) {
                            return NO_SUCH_PAGE;
                        }
                        int intLength = objFabric.getIntWarp();
                        int intHeight = objFabric.getIntWeft(); 
                        BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
                        try {
                            objFabricAction = new FabricAction();
                        } catch (SQLException ex) {
                            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);  
                            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                        }
                        bufferedImage = objFabricAction.plotFrontSideView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), objFabric.getIntWarp(), objFabric.getIntWeft());
                        
                        intLength = (int)(((objConfiguration.getIntDPI()*objFabric.getIntWarp())/objFabric.getIntEPI())*zoomfactor);
                        intHeight = (int)(((objConfiguration.getIntDPI()*objFabric.getIntWeft())/objFabric.getIntPPI())*zoomfactor);
                        BufferedImage texture = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            
                        Graphics2D g = texture.createGraphics();
                        g.drawImage(bufferedImage, 0, 0, intLength, intHeight, null);
                        g.dispose();
                        
                        if((objFabric.getIntEPI()/objFabric.getIntPPI())<=0){
                            intHeight = (int)(intLength*(objFabric.getIntPPI()/objFabric.getIntEPI())*zoomfactor);
                            intLength = (int)(intLength*zoomfactor);
                        }else{
                            intLength = (int)(intHeight*(objFabric.getIntEPI()/objFabric.getIntPPI())*zoomfactor);
                            intHeight = (int)(intHeight*zoomfactor);
                        }                        
                        BufferedImage texture = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
                        Graphics2D g = texture.createGraphics();
                        g.drawImage(bufferedImage, 0, 0, intLength, intHeight, null);
                        g.dispose();
                        
                        Graphics2D g2d=(Graphics2D)graphics;
                        g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
                    
                        graphics.drawImage(texture, 0, 0, intLength, intHeight, null);
                        return PAGE_EXISTS;
                    }
                });     
                objPrinterJob.print();
            }
        } catch (PrinterException ex) {             
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);  
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    
    
    private void captureFrame(String fileName) {
        BufferedImage capture = new BufferedImage((int)fabricStage.getWidth(), (int)fabricStage.getHeight(), BufferedImage.TYPE_INT_ARGB);
        capture.getGraphics();
        try {
            FileChooser fileChoser=new FileChooser();
            fileChoser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTPNG")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
            FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG (*.png)", "*.png");
            fileChoser.getExtensionFilters().add(extFilterPNG);
            File file=fileChoser.showSaveDialog(fabricStage);
            if(file==null)
                return;
            else
                fabricStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : ["+file.getAbsoluteFile().getName()+"]");   
            if (!file.getName().endsWith("png")) 
                file = new File(file.getPath() +".png");
            ImageIO.write(capture, "png", file);
            
            ArrayList<File> filesToZip=new ArrayList<>();
            filesToZip.add(file);
            String zipFilePath=file.getAbsolutePath()+".zip";
            String passwordToZip = file.getName();
            new EncryptZip(zipFilePath, filesToZip, passwordToZip);
            file.delete();
                
            lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+file.getCanonicalPath());
        } catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } 
    }
    public void ScreenShoot() {
        WritableImage image = container.snapshot(new SnapshotParameters(), null);
        
        FileChooser fileChoser=new FileChooser();
        fileChoser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTPNG")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
        FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        fileChoser.getExtensionFilters().add(extFilterPNG);
        File file=fileChoser.showSaveDialog(fabricStage);
        if(file==null)
            return;
        else
            fabricStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+file.getAbsoluteFile().getName()+"]");
        if (!file.getName().endsWith("png")) 
            file = new File(file.getPath() +".png");
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
            
            ArrayList<File> filesToZip=new ArrayList<>();
            filesToZip.add(file);
            String zipFilePath=file.getAbsolutePath()+".zip";
            String passwordToZip = file.getName();
            new EncryptZip(zipFilePath, filesToZip, passwordToZip);
            file.delete();
                
            lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+file.getCanonicalPath());
        } catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    public void plotViewSimulation(){
        try {
            Simulator objSimulator = new Simulator();
            objSimulator.setObjConfiguration(objConfiguration);
            objSimulator.setStrCondition("");
            objSimulator.setStrSearchBy("");
            objSimulator.setStrDirection("Ascending");
            objSimulator.setStrOrderBy("Name");
            
            //fabricTypeCB.getItems().addAll("default","cotton","nylon","silk","woolen");
            SimulatorImportView objSimulatorImportView = new SimulatorImportView(objSimulator);
            
            if(objSimulator.getStrBaseFSID()!=null){
                lblStatus.setText(objDictionaryAction.getWord("GETSIMULATION2DVIEW"));
            
                int intHeight = objFabric.getIntWeft();
                int intLength = objFabric.getIntWarp();
                weftYarn = objFabric.getWeftYarn();
                warpYarn = objFabric.getWarpYarn();
                weftExtraYarn = objFabric.getWeftExtraYarn();
                warpExtraYarn = objFabric.getWarpExtraYarn();
                        
                int warpCount = warpYarn.length;
                int weftCount = weftYarn.length;
                int weftExtraCount = objFabric.getIntExtraWeft();
                int warpExtraCount = objFabric.getIntExtraWarp();

                byte[][] repeatMatrix = new byte[intHeight][intLength];
                int dpi = objConfiguration.getIntDPI();
                int warpFactor = dpi/objFabric.getIntEPI();
                int weftFactor = dpi/objFabric.getIntPPI();
                        
                for(int i = 0; i < intHeight; i++) {
                    for(int j = 0; j < intLength; j++) {
                        if(i>=objFabric.getIntWeft() && j<objFabric.getIntWarp()){
                            repeatMatrix[i][j] = objFabric.getFabricMatrix()[i%objFabric.getIntWeft()][j];
                        }else if(i<objFabric.getIntWeft() && j>=objFabric.getIntWarp()){
                            repeatMatrix[i][j] = objFabric.getFabricMatrix()[i][j%objFabric.getIntWarp()];
                        }else if(i>=objFabric.getIntWeft() && j>=objFabric.getIntWarp()){
                            repeatMatrix[i][j] = objFabric.getFabricMatrix()[i%objFabric.getIntWeft()][j%objFabric.getIntWarp()];
                        }else{                
                            repeatMatrix[i][j] = objFabric.getFabricMatrix()[i][j];
                        }
                    }
                }
            
                SimulatorAction objSimulatorAction = new SimulatorAction();
                objSimulatorAction.getBaseFabricSimultion(objSimulator);
                
                byte[] bytBaseThumbnil=objSimulator.getBytBaseFSIcon();
                SeekableStream stream = new ByteArraySeekableStream(bytBaseThumbnil);
                String[] names = ImageCodec.getDecoderNames(stream);
                ImageDecoder dec = ImageCodec.createImageDecoder(names[0], stream, null);
                RenderedImage im = dec.decodeAsRenderedImage();
                BufferedImage srcImage = PlanarImage.wrapRenderedImage(im).getAsBufferedImage();
                bytBaseThumbnil=null;
            
                String srcImageName=System.getProperty("user.dir")+"/mla/";  //fabric colored red_tshirt white-cloth green-fabric
               
                BufferedImage myImage = new BufferedImage((int)(intLength), (int)(intHeight),BufferedImage.TYPE_INT_RGB);
                        
                for(int i = 0; i < intHeight; i++) {
                    for(int j = 0; j < intLength; j++) {
                        if(i>=srcImage.getHeight() && j<srcImage.getWidth()){
                            myImage.setRGB(j, i, srcImage.getRGB(j, i%srcImage.getHeight()));
                        }else if(i<srcImage.getHeight() && j>=srcImage.getWidth()){
                            myImage.setRGB(j, i, srcImage.getRGB(j%srcImage.getWidth(), i));
                        }else if(i>=srcImage.getHeight() && j>=srcImage.getWidth()){
                            myImage.setRGB(j, i, srcImage.getRGB(j%srcImage.getWidth(), i%srcImage.getHeight()));
                        }else{
                            myImage.setRGB(j, i, srcImage.getRGB(j, i));
                        }
                    }
                }
                        
                
                //Graphics2D g = myImage.createGraphics();
                //g.drawImage(srcImage, 0, 0, (int)(intLength), (int)(intHeight), null);
                //g.dispose();
                //srcImage = null;
                
                BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);

                int rgb = 0;
                for(int x = 0; x < intHeight; x++) {
                    for(int y = 0; y < intLength; y++) {
                        int pixel = myImage.getRGB(y, x);
                        int alpha = (pixel >> 24) & 0xff;
                        int red   = (pixel >>16) & 0xff;
                        int green = (pixel >> 8) & 0xff;
                        int blue  =  pixel & 0xff;
                        //if(red!=255 || green!=255 || blue!=255){
                        // Convert RGB to HSB
                        float[] hsb = java.awt.Color.RGBtoHSB(red, green, blue, null);
                        float hue = hsb[0];
                        float saturation = hsb[1];
                        float brightness = hsb[2];
                        hsb = null;

                        if(repeatMatrix[x][y]==1){
                            int nred = new java.awt.Color((float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getRed(),(float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getGreen(),(float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getBlue()).getRed();
                            int ngreen = new java.awt.Color((float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getRed(),(float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getGreen(),(float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getBlue()).getGreen();
                            int nblue = new java.awt.Color((float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getRed(),(float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getGreen(),(float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getBlue()).getBlue();
                            hsb = java.awt.Color.RGBtoHSB(nred, ngreen, nblue, null);
                        }else if(repeatMatrix[x][y]==0){
                            int nred = new java.awt.Color((float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getRed(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getGreen(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getBlue()).getRed();
                            int ngreen = new java.awt.Color((float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getRed(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getGreen(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getBlue()).getGreen();
                            int nblue = new java.awt.Color((float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getRed(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getGreen(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getBlue()).getBlue();
                            hsb = java.awt.Color.RGBtoHSB(nred, ngreen, nblue, null);
                        }else if(weftExtraCount>0){
                            for(int a=0; a<weftExtraCount; a++){
                                if(repeatMatrix[x][y]==(a+2)){
                                    int nred = new java.awt.Color((float)Color.web(weftExtraYarn[a].getStrYarnColor()).getRed(),(float)Color.web(weftExtraYarn[a].getStrYarnColor()).getGreen(),(float)Color.web(weftExtraYarn[a].getStrYarnColor()).getBlue()).getRed();
                                    int ngreen = new java.awt.Color((float)Color.web(weftExtraYarn[a].getStrYarnColor()).getRed(),(float)Color.web(weftExtraYarn[a].getStrYarnColor()).getGreen(),(float)Color.web(weftExtraYarn[a].getStrYarnColor()).getBlue()).getGreen();
                                    int nblue = new java.awt.Color((float)Color.web(weftExtraYarn[a].getStrYarnColor()).getRed(),(float)Color.web(weftExtraYarn[a].getStrYarnColor()).getGreen(),(float)Color.web(weftExtraYarn[a].getStrYarnColor()).getBlue()).getBlue();
                                    hsb = java.awt.Color.RGBtoHSB(nred, ngreen, nblue, null);
                                }
                            }
                        }else{
                            int nred = new java.awt.Color((float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getRed(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getGreen(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getBlue()).getRed();
                            int ngreen = new java.awt.Color((float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getRed(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getGreen(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getBlue()).getGreen();
                            int nblue = new java.awt.Color((float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getRed(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getGreen(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getBlue()).getBlue();
                            hsb = java.awt.Color.RGBtoHSB(nred, ngreen, nblue, null);
                        }
                        hue = hsb[0];
                        saturation = hsb[1];
                        // Convert HSB to RGB value
                        rgb = java.awt.Color.HSBtoRGB(hue, saturation, brightness);
                        //}else{
                        //  rgb = pixel;
                        //}
                        bufferedImage.setRGB(y, x, rgb);
                    }
                }
                //if((objFabric.getIntEPI()/objFabric.getIntPPI())<=0){
                //intHeight = (int)(intLength*(objFabric.getIntPPI()/objFabric.getIntEPI())*zoomfactor);
                //intLength = (int)(intLength*zoomfactor);
                //}else{
                //intLength = (int)(intHeight*(objFabric.getIntEPI()/objFabric.getIntPPI())*zoomfactor);
                //intHeight = (int)(intHeight*zoomfactor);
                //}
                intLength = (int)(((objConfiguration.getIntDPI()*objFabric.getIntWarp())/objFabric.getIntEPI())*zoomfactor);
                intHeight = (int)(((objConfiguration.getIntDPI()*objFabric.getIntWeft())/objFabric.getIntPPI())*zoomfactor);
                BufferedImage bufferedImageesize = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
                Graphics2D g = bufferedImageesize.createGraphics();
                g.drawImage(bufferedImage, 0, 0, intLength, intHeight, null);
                g.dispose();
                bufferedImage = bufferedImageesize;
                if(objConfiguration.getBlnMRepeat()){
                    objArtworkAction = new ArtworkAction();
                    bufferedImageesize = objArtworkAction.getImageRepeat(bufferedImage, editVerticalRepeatValue, editHorizontalRepeatValue);
                }
                bufferedImage = null;
                repeatMatrix = null;

                ImageIO.write(bufferedImageesize, "png", new File(System.getProperty("user.dir")+"/mla/temp/simulation.png"));

                fabric.setImage(SwingFXUtils.toFXImage(bufferedImageesize, null));
                container.setContent(fabric);
                bufferedImageesize=null;
                lblStatus.setText(objDictionaryAction.getWord("GOTSIMULATION2DVIEW"));                
            }else{
                lblStatus.setText(objFabric.getStrFabricName()+" : "+objDictionaryAction.getWord("DATASAVED"));
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }         
    }
    public void plotSimulation2DView(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("GETSIMULATION2DVIEW"));
            int intHeight = (int)objConfiguration.HEIGHT;//intWeft * editVerticalRepeatValue;
            int intLength = (int)objConfiguration.WIDTH;//intWarp * editHorizontalRepeatValue;
            weftYarn = objFabric.getWeftYarn();
            warpYarn = objFabric.getWarpYarn();
            weftExtraYarn = objFabric.getWeftExtraYarn();
            warpExtraYarn = objFabric.getWarpExtraYarn();
            
            int warpCount = warpYarn.length;
            int weftCount = weftYarn.length;
            int weftExtraCount = objFabric.getIntExtraWeft();
            int warpExtraCount = objFabric.getIntExtraWarp();
            
            byte[][] repeatMatrix = new byte[intHeight][intLength];
            int dpi = objConfiguration.getIntDPI();
            int warpFactor = dpi/objFabric.getIntEPI();
            int weftFactor = dpi/objFabric.getIntPPI();
            
            for(int i = 0; i < intHeight; i++) {
                for(int j = 0; j < intLength; j++) {
                    if(i>=objFabric.getIntWeft() && j<objFabric.getIntWarp()){
                        repeatMatrix[i][j] = objFabric.getFabricMatrix()[i%objFabric.getIntWeft()][j];
                    }else if(i<objFabric.getIntWeft() && j>=objFabric.getIntWarp()){
                        repeatMatrix[i][j] = objFabric.getFabricMatrix()[i][j%objFabric.getIntWarp()];
                    }else if(i>=objFabric.getIntWeft() && j>=objFabric.getIntWarp()){
                        repeatMatrix[i][j] = objFabric.getFabricMatrix()[i%objFabric.getIntWeft()][j%objFabric.getIntWarp()];
                    }else{
                        repeatMatrix[i][j] = objFabric.getFabricMatrix()[i][j];                
                    }
                }
            }
            BufferedImage srcImage = ImageIO.read(new File(System.getProperty("user.dir")+"/mla/temp/fabric.jpg")); //colored red_tshirt white-cloth green-fabric
            BufferedImage myImage = new BufferedImage((int)(intLength), (int)(intHeight),BufferedImage.TYPE_INT_RGB);
            Graphics2D g = myImage.createGraphics();
            g.drawImage(srcImage, 0, 0, (int)(intLength), (int)(intHeight), null);
            g.dispose();
            srcImage = null;
            
            BufferedImage bufferedImage = new BufferedImage(intLength, intHeight,BufferedImage.TYPE_INT_RGB);
            int rgb = 0;
            for(int x = 0; x < intHeight; x++) {
                for(int y = 0; y < intLength; y++) {
                    int pixel = myImage.getRGB(y, x);     
                    int alpha = (pixel >> 24) & 0xff;
                    int red   = (pixel >>16) & 0xff;
                    int green = (pixel >> 8) & 0xff;
                    int blue  =  pixel & 0xff;                  
                    //if(red!=255 || green!=255 || blue!=255){
                        // Convert RGB to HSB
                        float[] hsb = java.awt.Color.RGBtoHSB(red, green, blue, null);
                        float hue = hsb[0];
                        float saturation = hsb[1];
                        float brightness = hsb[2];
                        hsb = null;

                        if(repeatMatrix[x][y]==1){
                            int nred = new java.awt.Color((float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getRed(),(float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getGreen(),(float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getBlue()).getRed();
                            int ngreen = new java.awt.Color((float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getRed(),(float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getGreen(),(float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getBlue()).getGreen();
                            int nblue = new java.awt.Color((float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getRed(),(float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getGreen(),(float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getBlue()).getBlue();
                            hsb = java.awt.Color.RGBtoHSB(nred, ngreen, nblue, null);
                        }else if(repeatMatrix[x][y]==0){
                            int nred = new java.awt.Color((float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getRed(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getGreen(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getBlue()).getRed();
                            int ngreen = new java.awt.Color((float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getRed(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getGreen(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getBlue()).getGreen();
                            int nblue = new java.awt.Color((float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getRed(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getGreen(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getBlue()).getBlue();
                            hsb = java.awt.Color.RGBtoHSB(nred, ngreen, nblue, null);
                        }else if(weftExtraCount>0){
                            for(int a=0; a<weftExtraCount; a++){
                                if(repeatMatrix[x][y]==(a+2)){
                                    int nred = new java.awt.Color((float)Color.web(weftExtraYarn[a].getStrYarnColor()).getRed(),(float)Color.web(weftExtraYarn[a].getStrYarnColor()).getGreen(),(float)Color.web(weftExtraYarn[a].getStrYarnColor()).getBlue()).getRed();
                                    int ngreen = new java.awt.Color((float)Color.web(weftExtraYarn[a].getStrYarnColor()).getRed(),(float)Color.web(weftExtraYarn[a].getStrYarnColor()).getGreen(),(float)Color.web(weftExtraYarn[a].getStrYarnColor()).getBlue()).getGreen();
                                    int nblue = new java.awt.Color((float)Color.web(weftExtraYarn[a].getStrYarnColor()).getRed(),(float)Color.web(weftExtraYarn[a].getStrYarnColor()).getGreen(),(float)Color.web(weftExtraYarn[a].getStrYarnColor()).getBlue()).getBlue();
                                    hsb = java.awt.Color.RGBtoHSB(nred, ngreen, nblue, null);
                                }
                            }
                        }else{
                            int nred = new java.awt.Color((float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getRed(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getGreen(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getBlue()).getRed();
                            int ngreen = new java.awt.Color((float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getRed(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getGreen(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getBlue()).getGreen();
                            int nblue = new java.awt.Color((float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getRed(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getGreen(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getBlue()).getBlue();
                            hsb = java.awt.Color.RGBtoHSB(nred, ngreen, nblue, null);
                        }
                        hue = hsb[0];
                        saturation = hsb[1];
                        // Convert HSB to RGB value
                        rgb = java.awt.Color.HSBtoRGB(hue, saturation, brightness);
                    //}else{
                      //  rgb = pixel;
                    //}
                    bufferedImage.setRGB(y, x, rgb);
                }
            }
            repeatMatrix = null;
            
            //BufferedImage bufferedImagesize = new BufferedImage((int)(width*zoomfactor*3), (int)(height*zoomfactor*3),BufferedImage.TYPE_INT_RGB);
            //Graphics2D g = bufferedImagesize.createGraphics();
            //g.drawImage(bufferedImage, 0, 0, (int)(width*zoomfactor*3), (int)(height*zoomfactor*3), null);
            //g.dispose();
            //bufferedImage = null;
            
            fabric.setImage(SwingFXUtils.toFXImage(bufferedImage, null));
            
            //Light.Distant light = new Light.Distant();
            //light.setAzimuth(-135.0f);
            //Lighting l = new Lighting();
            //l.setLight(light);
            //l.setSurfaceScale(5.0f);
            //fabric.setEffect(l);
            
           //fabric.setTranslateX(0);
            //fabric.setTranslateY(0);
            //fabric.setTranslateZ(222);
            
            container.setContent(fabric);
            //bufferedImagesize = null;
            lblStatus.setText(objDictionaryAction.getWord("GOTSIMULATION2DVIEW"));
        } catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    public void plotSimulation3DView1(){
        lblStatus.setText(objDictionaryAction.getWord("GETSIMULATION3DVIEW"));
        int intHeight = objFabric.getIntWeft() * editVerticalRepeatValue;//objConfiguration.HEIGHT
        int intLength = objFabric.getIntWarp() * editHorizontalRepeatValue;//objConfiguration.WIDTH
        weftYarn = objFabric.getWeftYarn();
        warpYarn = objFabric.getWarpYarn();
        weftExtraYarn = objFabric.getWeftExtraYarn();
        warpExtraYarn = objFabric.getWarpExtraYarn();
        
        int warpCount = warpYarn.length;
        int weftCount = weftYarn.length;
        int weftExtraCount = objFabric.getIntExtraWeft();
        int warpExtraCount = objFabric.getIntExtraWarp();
        byte[][] repeatMatrix = new byte[intHeight][intLength];
        int dpi = objConfiguration.getIntDPI();
        int warpFactor = dpi/objFabric.getIntEPI();
        int weftFactor = dpi/objFabric.getIntPPI();
        for(int i = 0; i < intHeight; i++) {
            for(int j = 0; j < intLength; j++) {
                if(i>=objFabric.getIntWeft() && j<objFabric.getIntWarp()){
                    repeatMatrix[i][j] = objFabric.getFabricMatrix()[i%objFabric.getIntWeft()][j];
                }else if(i<objFabric.getIntWeft() && j>=objFabric.getIntWarp()){
                    repeatMatrix[i][j] = objFabric.getFabricMatrix()[i][j%objFabric.getIntWarp()];
                }else if(i>=objFabric.getIntWeft() && j>=objFabric.getIntWarp()){
                    repeatMatrix[i][j] = objFabric.getFabricMatrix()[i%objFabric.getIntWeft()][j%objFabric.getIntWarp()];
                }else{
                    repeatMatrix[i][j] = objFabric.getFabricMatrix()[i][j];                
                }
            }
        }
        BufferedImage bufferedImageesize = new BufferedImage((int)(intLength*zoomfactor*3), (int)(intHeight*zoomfactor*3),BufferedImage.TYPE_INT_RGB);        
        Graphics2D g = bufferedImageesize.createGraphics();
        int rgb = 0;
        java.awt.Color rgbcolor = null;
        for(int x = 0; x < intHeight; x++) {
            for(int y = 0; y < intLength; y++) {
                if(repeatMatrix[x][y]==1){
                    rgbcolor = new java.awt.Color((float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getRed(),(float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getGreen(),(float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getBlue());
                    rgb = rgbcolor.getRGB(); 
                } else if(repeatMatrix[x][y]==0){
                    rgbcolor = new java.awt.Color((float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getRed(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getGreen(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getBlue());
                    rgb = rgbcolor.getRGB(); 
                } else if(weftExtraCount>0){
                    for(int a=0; a<weftExtraCount; a++)
                        if(repeatMatrix[x][y]==(a+2)){
                            rgbcolor = new java.awt.Color((float)Color.web(weftExtraYarn[a].getStrYarnColor()).getRed(),(float)Color.web(weftExtraYarn[a].getStrYarnColor()).getGreen(),(float)Color.web(weftExtraYarn[a].getStrYarnColor()).getBlue());
                            rgb = rgbcolor.getRGB(); 
                            
                        } 
                } else{
                    rgbcolor = new java.awt.Color((float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getRed(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getGreen(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getBlue()); 
                    rgb = rgbcolor.getRGB(); 
                }
                //bufferedImageesize.setRGB(y, x, rgb);
                g.setColor(rgbcolor);
                g.fillOval(x, y, (int)zoomfactor*3, (int)zoomfactor*3);
            }
        }
        g.dispose();
        
        //////// another comemnted code start here   /////////////
        BufferedImage bufferedImage = new BufferedImage(width*3, height*3,BufferedImage.TYPE_INT_RGB);        
        int bands = 3;
        int rgb = 0;
        for(int x = 0, p = 0; x < height; x++) {
            for(int y = 0, q = 0; y < width; y++) {
                for(int i = 0; i < bands; i++) {
                    for(int j = 0; j < bands; j++) {                        
                        if(repeatMatrix[x][y]==1){
                            if(j==0)
                                rgb = new java.awt.Color((float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getRed(),(float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getGreen(),(float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getBlue()).brighter().getRGB();
                            else if(j==2)
                                rgb = new java.awt.Color((float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getRed(),(float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getGreen(),(float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getBlue()).darker().getRGB();
                            else
                                rgb = new java.awt.Color((float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getRed(),(float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getGreen(),(float)Color.web(warpYarn[y%warpCount].getStrYarnColor()).getBlue()).getRGB();
                        } else if(repeatMatrix[x][y]==0){
                            if(i==0)
                                rgb = new java.awt.Color((float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getRed(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getGreen(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getBlue()).brighter().getRGB();
                            else if(i==2)
                                rgb = new java.awt.Color((float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getRed(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getGreen(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getBlue()).darker().getRGB();
                            else
                                rgb = new java.awt.Color((float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getRed(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getGreen(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getBlue()).getRGB();
                        } else if(weftExtraCount>0){
                            for(int a=0; a<weftExtraCount; a++){
                                if(repeatMatrix[x][y]==(a+2)){
                                    if(i==0)
                                        rgb = new java.awt.Color((float)Color.web(weftExtraYarn[a].getStrYarnColor()).getRed(),(float)Color.web(weftExtraYarn[a].getStrYarnColor()).getGreen(),(float)Color.web(weftExtraYarn[a].getStrYarnColor()).getBlue()).brighter().getRGB();
                                    else if(i==2)
                                        rgb = new java.awt.Color((float)Color.web(weftExtraYarn[a].getStrYarnColor()).getRed(),(float)Color.web(weftExtraYarn[a].getStrYarnColor()).getGreen(),(float)Color.web(weftExtraYarn[a].getStrYarnColor()).getBlue()).darker().getRGB();
                                    else
                                        rgb = new java.awt.Color((float)Color.web(weftExtraYarn[a].getStrYarnColor()).getRed(),(float)Color.web(weftExtraYarn[a].getStrYarnColor()).getGreen(),(float)Color.web(weftExtraYarn[a].getStrYarnColor()).getBlue()).getRGB();
                                }
                            }
                        } else{
                            if(i==0)
                                rgb = new java.awt.Color((float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getRed(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getGreen(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getBlue()).brighter().getRGB();
                            else if(i==2)
                                rgb = new java.awt.Color((float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getRed(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getGreen(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getBlue()).darker().getRGB();
                            else
                                rgb = new java.awt.Color((float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getRed(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getGreen(),(float)Color.web(weftYarn[x%weftCount].getStrYarnColor()).getBlue()).getRGB();
                        }
                        bufferedImage.setRGB(q+j, p+i, rgb);
                    }
                }
                q+=bands;
            }
            p+=bands;
        }
        repeatMatrix = null;
        BufferedImage bufferedImageesize = new BufferedImage((int)(width*zoomfactor*3), (int)(height*zoomfactor*3),BufferedImage.TYPE_INT_RGB);        
        Graphics2D g = bufferedImageesize.createGraphics();
        g.drawImage(bufferedImage, 0, 0, (int)(width*zoomfactor*3), (int)(height*zoomfactor*3), null);
        g.dispose();
        bufferedImage = null;
    //////////// other comented code end here //////////////
        
        fabric.setImage(SwingFXUtils.toFXImage(bufferedImageesize, null));
        container.setContent(fabric); 
        bufferedImageesize = null; 
        lblStatus.setText(objDictionaryAction.getWord("GOTSIMULATION3DVIEW"));
        
        
        
                ///////////////// new code start /////////////////////
//clean weave
        int removeRow = 0;
        for(int x=0; x<height; x++){
            byte temp = 0;
            for(int y=0; y<width; y++){
                if(matrix[x][y]!=1){
                    temp = 1;
                }
            }
            if(temp == 0){
                removeRow++;
                for(int z=x; z<height-1; z++){
                    for(int y=0; y<width; y++){
                        matrix[z][y]=matrix[z+1][y];
                    }
                }
            }
        }
        System.err.println("removeRow-"+removeRow);
        if(removeRow>0){
            for(int p = 0; p < height; p++) {
                for(int q = 0; q < width; q++) {
                    if(p>=height-removeRow && q<width){
                        matrix[p][q] = matrix[p%(height-removeRow)][q];  
                    }else if(p<(height-removeRow) && q>=width){
                        matrix[p][q] = matrix[p][q%width];  
                    }else if(p>=(height-removeRow) && q>=width){
                        matrix[p][q] = matrix[p%(height-removeRow)][q%width];  
                    }else{
                        matrix[p][q] = matrix[p][q]; 
                    }
                }
            }
        }          
        ///////////////// new code end /////////////////////
    }
    public void plotSimulation3DView(){
        lblStatus.setText(objDictionaryAction.getWord("GETSIMULATION3DVIEW"));
        int intHeight = objFabric.getIntWeft();// editVerticalRepeatValue;//objConfiguration.HEIGHT
        int intLength = objFabric.getIntWarp();// * editHorizontalRepeatValue;//objConfiguration.WIDTH
        
        weftYarn = objFabric.getWeftYarn();
        warpYarn = objFabric.getWarpYarn();
        weftExtraYarn = objFabric.getWeftExtraYarn();
        warpExtraYarn = objFabric.getWarpExtraYarn();
        
        int warpCount = warpYarn.length;
        int weftCount = weftYarn.length;
        int weftExtraCount = objFabric.getIntExtraWeft();
        int warpExtraCount = objFabric.getIntExtraWarp();
        byte[][] repeatMatrix = new byte[intHeight][intLength];
        int dpi = objConfiguration.getIntDPI();
        int warpFactor = dpi/objFabric.getIntEPI();
        int weftFactor = dpi/objFabric.getIntPPI();
        
        for(int i = 0; i < intHeight; i++) {
            for(int j = 0; j < intLength; j++) {
                if(i>=objFabric.getIntWarp() && j<objFabric.getIntWeft()){
                     repeatMatrix[i][j] = objFabric.getFabricMatrix()[i%objFabric.getIntWarp()][j];  
                }else if(i<objFabric.getIntWarp() && j>=objFabric.getIntWeft()){
                     repeatMatrix[i][j] = objFabric.getFabricMatrix()[i][j%objFabric.getIntWeft()];  
                }else if(i>=objFabric.getIntWarp() && j>=objFabric.getIntWeft()){
                     repeatMatrix[i][j] = objFabric.getFabricMatrix()[i%objFabric.getIntWarp()][j%objFabric.getIntWeft()];  
                }else{
                     repeatMatrix[i][j] = objFabric.getFabricMatrix()[i][j]; 
                }                
            }
        }
        
        bodyContainer.getChildren().clear();
        for(int x = 0; x < intHeight; x++) {
            for(int y = 0; y < intLength; y++) {
                Label lblbox = new Label("");
                lblbox.setFont(new Font("Arial", objConfiguration.intBoxSize));
                lblbox.setPrefSize(objConfiguration.intBoxSize,objConfiguration.intBoxSize);
                //lblbox.setAlignment(Pos.CENTER);
                ImageView objIV ;
                if(repeatMatrix[x][y]==1 ){
                    objIV = new ImageView("/media/yarn/thread1.png");                    
                    //lblbox.setStyle("-fx-background-color: #000000; -fx-border-width: 1;  -fx-border-color: black;");
                }else{
                    objIV = new ImageView("/media/yarn/thread6.png");                   
                   // lblbox.setStyle("-fx-background-color: #ffffff; -fx-border-width: 1;  -fx-border-color: black;");                
                }
                objLbl.setScaleX(zoomfactor);
                objLbl.setScaleY(zoomfactor);
                lblbox.setGraphic(objIV);
                bodyContainer.add(lblbox,y,x);
           }
        }
        container.setContent(bodyContainer);
        lblStatus.setText(objDictionaryAction.getWord("GOTSIMULATION3DVIEW"));
    }
    public void plotMenuIcon(){
        try {
            int intHeight = objFabric.getIntWeft();
            int intLength = objFabric.getIntWarp();
            BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            bufferedImage = objFabricAction.plotFrontSideView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), intLength, intHeight);
            //if((objFabric.getIntEPI()/objFabric.getIntPPI())<=0){
            //intHeight = (int)(intLength*(objFabric.getIntPPI()/objFabric.getIntEPI())*zoomfactor);
            //intLength = (int)(intLength*zoomfactor);
            //}else{
            //intLength = (int)(intHeight*(objFabric.getIntEPI()/objFabric.getIntPPI())*zoomfactor);
            //intHeight = (int)(intHeight*zoomfactor);
            //}
            intLength = 111;//(int)(((objConfiguration.getIntDPI()*objFabric.getIntWarp())/objFabric.getIntEPI())*zoomfactor);
            intHeight = 111;//(int)(((objConfiguration.getIntDPI()*objFabric.getIntWeft())/objFabric.getIntPPI())*zoomfactor);
            BufferedImage bufferedImageesize = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bufferedImageesize.createGraphics();
            g.drawImage(bufferedImage, 0, 0, intLength, intHeight, null);
            g.dispose();
            bufferedImage = null;
            //ImageIO.write(bufferedImageesize, "png", new File(System.getProperty("user.dir")+"/mla/temp/menu_composite.png"));
            //compositeViewVB.getChildren().set(0, new ImageView(SwingFXUtils.toFXImage(bufferedImageesize, null)));

            intHeight = objFabric.getIntWeft()*3;
            intLength = objFabric.getIntWarp()*3;
            bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            bufferedImage = objFabricAction.plotGridView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
            intLength = 111;//(int)(objFabric.getIntWarp()*zoomfactor*3);
            intHeight = 111;//(int)(objFabric.getIntWeft()*zoomfactor*3);
            bufferedImageesize = new BufferedImage(intLength, intHeight,BufferedImage.TYPE_INT_RGB);
            g = bufferedImageesize.createGraphics();
            g.drawImage(bufferedImage, 0, 0, intLength, intHeight, null);
            g.dispose();
            bufferedImage = null;
            //ImageIO.write(bufferedImageesize, "png", new File(Paths.get("src").toAbsolutePath().normalize().toString()+"/media/menu_grid.png"));
            //gridViewVB.getChildren().set(0, new ImageView(SwingFXUtils.toFXImage(bufferedImageesize, null)));
            
            intHeight=objFabric.getIntWeft();
            intLength=objFabric.getIntWarp();
            bufferedImage = new BufferedImage(objFabric.getIntWarp(), objFabric.getIntWeft(),BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            if(objFabric.getIntExtraWarp()>0 || objFabric.getIntExtraWeft()>0){
                if(objConfiguration.getBlnPunchCard()){
                    bufferedImage = objFabricAction.plotGraphJaquardMachineView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                    intLength=objFabric.getIntWarp()*(objFabric.getIntExtraWeft()+2);
                } else{
                    bufferedImage = objFabricAction.plotGraphJaquardView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                }
            }else{
                bufferedImage = objFabricAction.plotGraphDobbyView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
            }
            //================================ For Outer to be square ===============================
            String[] data = objFabric.getObjConfiguration().getStrGraphSize().split("x");
            bufferedImageesize = new BufferedImage((int)(intLength*zoomfactor*Integer.parseInt(data[1])), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])),BufferedImage.TYPE_INT_RGB);
            g = bufferedImageesize.createGraphics();
            g.drawImage(bufferedImage, 0, 0, (int)(intLength*zoomfactor*Integer.parseInt(data[1])), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])), null);
            g.setColor(java.awt.Color.BLACK);
            BasicStroke bs = new BasicStroke(2);
            g.setStroke(bs);
            for(int i = 0; i < objFabric.getIntWeft(); i++) {
                for(int j = 0; j < intLength; j++) {
                    if((j%Integer.parseInt(data[0]))==0){
                        bs = new BasicStroke(2);
                        g.setStroke(bs);
                    }else{
                        bs = new BasicStroke(1);
                        g.setStroke(bs);
                    }
                    g.drawLine((int)(j*zoomfactor*Integer.parseInt(data[1])), 0,  (int)(j*zoomfactor*Integer.parseInt(data[1])), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])));
                }
                if((i%Integer.parseInt(data[1]))==0){
                    bs = new BasicStroke(2);
                    g.setStroke(bs);
                }else{
                    bs = new BasicStroke(1);
                    g.setStroke(bs);
                }
                g.drawLine(0, (int)(i*zoomfactor*Integer.parseInt(data[0])), (int)(intLength*zoomfactor)*Integer.parseInt(data[1]), (int)(i*zoomfactor*Integer.parseInt(data[0])));
            }
            g.dispose();
            bufferedImage = bufferedImageesize;
            intLength = 111;//(int)(objFabric.getIntWarp()*zoomfactor*3);
            intHeight = 111;//(int)(objFabric.getIntWeft()*zoomfactor*3);
            bufferedImageesize = new BufferedImage(intLength, intHeight,BufferedImage.TYPE_INT_RGB);
            g = bufferedImageesize.createGraphics();
            g.drawImage(bufferedImage, 0, 0, intLength, intHeight, null);
            g.dispose();
            bufferedImage = null;
            //ImageIO.write(bufferedImageesize, "png", new File(System.getProperty("user.dir")+"/mla/temp/menu_graph.png"));
            //graphViewVB.getChildren().set(0, new ImageView(SwingFXUtils.toFXImage(bufferedImageesize, null)));
            
            intHeight = objFabric.getIntWeft();
            intLength = objFabric.getIntWarp();
            bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            bufferedImage = objFabricAction.plotFlipSideView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), intLength, intHeight);
            //if((objFabric.getIntEPI()/objFabric.getIntPPI())<=0){
                //intHeight = (int)(intLength*(objFabric.getIntPPI()/objFabric.getIntEPI())*zoomfactor*3);
                //intLength = (int)(intLength*zoomfactor*3);
            //}else{
                //intLength = (int)(intHeight*(objFabric.getIntEPI()/objFabric.getIntPPI())*zoomfactor*3);
                //intHeight = (int)(intHeight*zoomfactor*3);
            //}
            intLength = 111; //(int)(((objConfiguration.getIntDPI()*objFabric.getIntWarp())/objFabric.getIntEPI())*zoomfactor*3);
            intHeight = 111; //(int)(((objConfiguration.getIntDPI()*objFabric.getIntWeft())/objFabric.getIntPPI())*zoomfactor*3);
            bufferedImageesize = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            g = bufferedImageesize.createGraphics();
            g.drawImage(bufferedImage, 0, 0, intLength, intHeight, null);
            g.dispose();
            bufferedImage = null;            
            //ImageIO.write(bufferedImageesize, "png", new File(System.getProperty("user.dir")+"/mla/temp/menu_flipside.png"));
            //flipSideViewVB.getChildren().set(0, new ImageView(SwingFXUtils.toFXImage(bufferedImageesize, null)));
            
            intHeight = objFabric.getIntWeft();
            intLength = objFabric.getIntWarp();
            bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            bufferedImage = objFabricAction.plotSwitchSideView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), intLength, intHeight);
            //if((objFabric.getIntEPI()/objFabric.getIntPPI())<=0){
              //  intHeight = (int)(intLength*(objFabric.getIntPPI()/objFabric.getIntEPI())*zoomfactor*3);
                //intLength = (int)(intLength*zoomfactor*3);
            //}else{
//                intLength = (int)(intHeight*(objFabric.getIntEPI()/objFabric.getIntPPI())*zoomfactor*3);
//                intHeight = (int)(intHeight*zoomfactor*3);
//            }
            intLength = 111; //(int)(((objConfiguration.getIntDPI()*objFabric.getIntWarp())/objFabric.getIntEPI())*zoomfactor*3);
            intHeight = 111; //(int)(((objConfiguration.getIntDPI()*objFabric.getIntWeft())/objFabric.getIntPPI())*zoomfactor*3);
            bufferedImageesize = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            g = bufferedImageesize.createGraphics();
            g.drawImage(bufferedImage, 0, 0, intLength, intHeight, null);
            g.dispose();
            bufferedImage = null;            
            //ImageIO.write(bufferedImageesize, "png", new File(System.getProperty("user.dir")+"/mla/temp/menu_switchside.png"));
            //switchSideViewVB.getChildren().set(0, new ImageView(SwingFXUtils.toFXImage(bufferedImageesize, null)));
            
            intHeight = objFabric.getIntWeft();
            intLength = objFabric.getIntWarp();
            bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            bufferedImage = objFabricAction.plotFrontSideView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), intLength, intHeight);
//            if((objFabric.getIntEPI()/objFabric.getIntPPI())<=0){
//                intHeight = (int)(intLength*(objFabric.getIntPPI()/objFabric.getIntEPI())*zoomfactor*3);
  //              intLength = (int)(intLength*zoomfactor*3);
//            }else{
//                intLength = (int)(intHeight*(objFabric.getIntEPI()/objFabric.getIntPPI())*zoomfactor*3);
              //  intHeight = (int)(intHeight*zoomfactor*3);
            //}
            intLength = 111; //(int)(((objConfiguration.getIntDPI()*objFabric.getIntWarp())/objFabric.getIntEPI())*zoomfactor*3);
            intHeight = 111; //(int)(((objConfiguration.getIntDPI()*objFabric.getIntWeft())/objFabric.getIntPPI())*zoomfactor*3);
            bufferedImageesize = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            g = bufferedImageesize.createGraphics();
            g.drawImage(bufferedImage, 0, 0, intLength, intHeight, null);
            g.dispose();
            bufferedImage = null;            
            //ImageIO.write(bufferedImageesize, "png", new File(System.getProperty("user.dir")+"/mla/temp/menu_visulization.png"));
            //frontSideViewVB.getChildren().set(0, new ImageView(SwingFXUtils.toFXImage(bufferedImageesize, null)));
            
            intLength = objFabric.getIntWarp();
            intHeight = objFabric.getIntWeft();
            byte[][] newMatrix = objFabric.getFabricMatrix();
            List lstLines = new ArrayList();
            ArrayList<Byte> lstEntry = null;
            int lineCount = 0;
            for (int i = 0; i < intHeight; i++){
                lstEntry = new ArrayList();
                for (int j = 0; j < intLength; j++){
                    //add the first color on array
                    if(lstEntry.size()==0 && newMatrix[i][j]!=1)
                        lstEntry.add(newMatrix[i][j]);
                    //check for redudancy
                    else {                
                        if(!lstEntry.contains(newMatrix[i][j]) && newMatrix[i][j]!=1)
                            lstEntry.add(newMatrix[i][j]);
                    }
                }
                lstLines.add(lstEntry);
                lineCount+=lstEntry.size();
            }
            byte[][] baseMatrix = objArtworkAction.repeatMatrix(objFabric.getBaseWeaveMatrix(),lineCount,intLength);
            byte[][] fontMatrix = new byte[lineCount][intLength];
            lineCount=0;
            byte init = 0;
            for (int i = 0 ; i < intHeight; i++){
                lstEntry = (ArrayList)lstLines.get(i);
                for(int k=0; k<lstEntry.size(); k++, lineCount++){
                    init = (byte)lstEntry.get(k);
                    for (int j = 0; j < intLength; j++){
                        if(init==0){
                            fontMatrix[lineCount][j] = baseMatrix[i][j];
                        }else{
                            if(newMatrix[i][j]==init){
                                fontMatrix[lineCount][j] = init;
                            }else{
                                fontMatrix[lineCount][j] = (byte)1;
                            }
                        }   
                    }
                }
            }
//            System.out.println("Row"+lineCount);
//            for (int i = 0 ; i < lineCount; i++){
//                for (int j = 0; j < intLength; j++){
//                    System.err.print(" "+fontMatrix[i][j]);
//                }
//                System.err.println();
//            }
            newMatrix = null;
            baseMatrix = null;
            intHeight = lineCount;
            bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            bufferedImage = objFabricAction.plotCrossSectionView(objFabric, fontMatrix, intLength, intHeight);
            intHeight = 111; //(int)(intHeight*zoomfactor*3);
            intLength = 111; //(int)(intLength*zoomfactor*3);
            bufferedImageesize = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            g = bufferedImageesize.createGraphics();
            g.drawImage(bufferedImage, 0, 0, intLength, intHeight, null);
            g.dispose();
            bufferedImage = null;            
            //ImageIO.write(bufferedImageesize, "png", new File(System.getProperty("user.dir")+"/mla/temp/menu_crosssectionfont.png"));
            //crossSectionFrontViewVB.getChildren().set(0, new ImageView(SwingFXUtils.toFXImage(bufferedImageesize, null)));
            
            intLength = objFabric.getIntWarp();
            intHeight = objFabric.getIntWeft();
            newMatrix = objFabric.getFabricMatrix();
            lstLines = new ArrayList();
            lstEntry = null;
            lineCount = 0;
            for (int i = 0; i < intHeight; i++){
                lstEntry = new ArrayList();
                for (int j = 0; j < intLength; j++){
                    //add the first color on array
                    if(lstEntry.size()==0 && newMatrix[i][j]!=1)
                        lstEntry.add(newMatrix[i][j]);
                    //check for redudancy
                    else {                
                        if(!(lstEntry.contains(newMatrix[i][j]))  && newMatrix[i][j]!=1)
                            lstEntry.add(newMatrix[i][j]);
                    }
                }
                lstLines.add(lstEntry);
                lineCount+=lstEntry.size();
            }
            baseMatrix = objArtworkAction.repeatMatrix(objFabric.getBaseWeaveMatrix(),lineCount,intLength);
            baseMatrix=objArtworkAction.invertMatrix(baseMatrix);
            fontMatrix = new byte[lineCount][intLength];
            lineCount=0;
            init = 0;
            for (int i = 0 ; i < intHeight; i++){
                lstEntry = (ArrayList)lstLines.get(i);
                for(int k=0; k<lstEntry.size(); k++, lineCount++){
                    init = (byte)lstEntry.get(k);
                    for (int j = 0; j < intLength; j++){
                        if(init==0)
                            fontMatrix[lineCount][j] = baseMatrix[i][j];
                        else{
                            if(newMatrix[i][j]==init){
                                fontMatrix[lineCount][j] = (byte)1;
                            }else{
                                fontMatrix[lineCount][j] = init;
                            }
                        }   
                    }
                }
            }
            
            intHeight = lineCount;
            bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            bufferedImage = objFabricAction.plotCrossSectionView(objFabric, fontMatrix, intLength, intHeight);
            intHeight = 111; //(int)(intHeight*zoomfactor*3);
            intLength = 111; //(int)(intLength*zoomfactor*3);
            bufferedImageesize = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            g = bufferedImageesize.createGraphics();
            g.drawImage(bufferedImage, 0, 0, intLength, intHeight, null);
            g.dispose();
            bufferedImage = null;
            //ImageIO.write(bufferedImageesize, "png", new File(System.getProperty("user.dir")+"/mla/temp/menu_crosssectionrear.png"));
            //crossSectionRearViewVB.getChildren().set(0, new ImageView(SwingFXUtils.toFXImage(bufferedImageesize, null)));
            
            //simulationViewVB.getChildren().set(0, new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/grid_view.png"));
            bufferedImageesize = null;
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        //} catch (IOException ex) {
          //  Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void fabricWeaveEditEvent(MouseEvent event){
        if(event.isSecondaryButtonDown() || (event.getButton() == MouseButton.SECONDARY)) {         
            objWeave.getDesignMatrix()[(int)(event.getY()/(int)(objFabric.getObjConfiguration().getIntBoxSize()*zoomfactor))][(int)(event.getX()/(int)(objFabric.getObjConfiguration().getIntBoxSize()*zoomfactor))] = (byte)editThreadSecondaryValue;
            objWeaveR.getDesignMatrix()[(int)(event.getY()/(int)(objFabric.getObjConfiguration().getIntBoxSize()*zoomfactor))][(int)(event.getX()/(int)(objFabric.getObjConfiguration().getIntBoxSize()*zoomfactor))] = (editThreadSecondaryValue==1)?(byte)0:(byte)1;
        }else{
            objWeave.getDesignMatrix()[(int)(event.getY()/(int)(objFabric.getObjConfiguration().getIntBoxSize()*zoomfactor))][(int)(event.getX()/(int)(objFabric.getObjConfiguration().getIntBoxSize()*zoomfactor))] = (byte)editThreadPrimaryValue;
            objWeaveR.getDesignMatrix()[(int)(event.getY()/(int)(objFabric.getObjConfiguration().getIntBoxSize()*zoomfactor))][(int)(event.getX()/(int)(objFabric.getObjConfiguration().getIntBoxSize()*zoomfactor))] = (editThreadPrimaryValue==1)?(byte)0:(byte)1;
        }
        plotEditWeave();                           
    }
    private void populateEditWeavePane(){
        if(fabricChildStage!=null){
            fabricChildStage.close();
            fabricChildStage = null;
            System.gc();
        }
        fabricChildStage = new Stage();
        fabricChildStage.initOwner(fabricStage);
        fabricChildStage.initStyle(StageStyle.UTILITY);
        //dialogStage.initModality(Modality.WINDOW_MODAL);
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setAlignment(Pos.CENTER);
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        
        popup.add(new Label(objDictionaryAction.getWord("EDITTHREADTYPE")+" : "), 0, 0);
        GridPane editThreadCB = new GridPane();
        editThreadCB.setHgap(5);
        editThreadCB.setVgap(5);
        editThreadCB.setCursor(Cursor.HAND);
        //editThreadCB.getChildren().clear();
        for(int i=0;i<=objFabric.getIntExtraWeft()+1;i++){
            final Label lblColor  = new Label ();
            lblColor.setPrefSize(33, 33);
            String webColor = "#FFFFFF";
            if(i==0){
                if(objFabric.getIntExtraWeft()<1){
                    webColor = "#FFFFFF";
                }
                lblColor.setTooltip(new Tooltip("Weft \n Color:"+webColor));
            } else if(i==1){
                if(objFabric.getIntExtraWeft()<1){
                    webColor = "#000000";
                }
                lblColor.setTooltip(new Tooltip("Warp \n Color:"+webColor));
            }else{
                webColor = objFabric.getWeftExtraYarn()[i-2].getStrYarnColor();
                lblColor.setTooltip(new Tooltip("Extra Weft #"+(i-1)+"\n Color:"+webColor));
            }
            lblColor.setStyle("-fx-background-color:"+webColor+"; -fx-border-color: #FFFFFF;");
            lblColor.setUserData(i);
            lblColor.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if(event.isSecondaryButtonDown() || (event.getButton() == MouseButton.SECONDARY))
                        editThreadSecondaryValue=Integer.parseInt(lblColor.getUserData().toString());
                    else
                        editThreadPrimaryValue=Integer.parseInt(lblColor.getUserData().toString());
                }
            });
            editThreadCB.add(lblColor, i%4, i/4);
        }
        editThreadPrimaryValue=0;//base weft
        editThreadSecondaryValue=1;//base warp
        popup.add(editThreadCB, 0, 1);
        
        popup.add(new Label(objDictionaryAction.getWord("EDITTHREADTOOL")+" : "), 0, 2);
        
        GridPane editToolPane = new GridPane();
        editToolPane.setVgap(0);
        editToolPane.setHgap(0);
                
        Button mirrorVerticalTP = new Button();
        Button mirrorHorizontalTP = new Button();
        Button inversionTP = new Button();
        Button rotateTP = new Button();
        Button rotateAntiTP = new Button();
        Button moveRightTP = new Button();
        Button moveLeftTP = new Button();
        Button moveUpTP = new Button();
        Button moveDownTP = new Button();
        Button moveRight8TP = new Button();
        Button moveLeft8TP = new Button();
        Button moveUp8TP = new Button();
        Button moveDown8TP = new Button();
        Button tiltRightTP = new Button();
        Button tiltLeftTP = new Button();
        Button tiltUpTP = new Button();
        Button tiltDownTP = new Button();
                
        mirrorVerticalTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
        mirrorHorizontalTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
        rotateTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_90.png"));
        rotateAntiTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_anti_90.png"));
        moveRightTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right.png"));
        moveLeftTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left.png"));
        moveUpTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up.png"));
        moveDownTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down.png"));
        moveRight8TP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right_by_8.png"));
        moveLeft8TP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left_by_8.png"));
        moveUp8TP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up_by_8.png"));
        moveDown8TP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down_by_8.png"));
        tiltRightTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_right.png"));
        tiltLeftTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_left.png"));
        tiltUpTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_up.png"));
        tiltDownTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_down.png"));
        inversionTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/flip_visualization.png"));

        mirrorVerticalTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPVERTICALMIRROR")));
        mirrorHorizontalTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPHORIZENTALMIRROR")));
        rotateTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCLOCKROTATION")));
        rotateAntiTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPANTICLOCKROTATION")));
        moveRightTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPMOVERIGHT")));
        moveLeftTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPMOVELEFT")));
        moveUpTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPMOVEUP")));
        moveDownTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPMOVEDOWN")));
        moveRight8TP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPMOVERIGHT8")));
        moveLeft8TP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPMOVELEFT8")));
        moveUp8TP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPMOVEUP8")));
        moveDown8TP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPMOVEDOWN8")));
        tiltRightTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPTILTRIGHT")));
        tiltLeftTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPTILTLEFT")));
        tiltUpTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPTILTUP")));
        tiltDownTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPTILTDOWN")));
        inversionTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPINVERSION")));

        mirrorVerticalTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        mirrorHorizontalTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rotateTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rotateAntiTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveRightTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveLeftTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveUpTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveDownTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveRight8TP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveLeft8TP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveUp8TP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveDown8TP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tiltRightTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tiltLeftTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tiltUpTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tiltDownTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);                    
        inversionTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);

        editToolPane.add(mirrorVerticalTP, 0, 0);
        editToolPane.add(mirrorHorizontalTP, 1, 0);
        editToolPane.add(rotateTP, 2, 0);
        editToolPane.add(rotateAntiTP, 3, 0);        
        editToolPane.add(moveRightTP, 0, 1);
        editToolPane.add(moveLeftTP, 1, 1);
        editToolPane.add(moveUpTP, 2, 1);
        editToolPane.add(moveDownTP, 3, 1);        
        editToolPane.add(moveRight8TP, 0, 2);
        editToolPane.add(moveLeft8TP, 1, 2);
        editToolPane.add(moveUp8TP, 2, 2);
        editToolPane.add(moveDown8TP, 3, 2);        
        editToolPane.add(tiltRightTP, 0, 3);
        editToolPane.add(tiltLeftTP, 1, 3);
        editToolPane.add(tiltUpTP, 2, 3);
        editToolPane.add(tiltDownTP, 3, 3);        
        editToolPane.add(inversionTP, 0, 4);

        mirrorVerticalTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {                       
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONVERTICALMIRROR"));
                    objWeaveAction.mirrorVertical(objWeave);
                    objWeaveAction.mirrorVertical(objWeaveR);
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Mirror Vertical",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });  
        mirrorHorizontalTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONHORIZENTALMIRROR"));
                    objWeaveAction.mirrorHorizontal(objWeave);
                    objWeaveAction.mirrorHorizontal(objWeaveR);
                    plotEditWeave();
                } catch (Exception ex) {
                  new Logging("SEVERE",getClass().getName(),"Mirror Vertical",ex);
                  lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        }); 
        rotateTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONCLOCKROTATION"));
                    objWeaveAction.rotation(objWeave);
                    objWeaveAction.rotation(objWeaveR);
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Rotate Clock Wise ",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        }); 
        rotateAntiTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONANTICLOCKROTATION"));
                    objWeaveAction.rotationAnti(objWeave);
                    objWeaveAction.rotationAnti(objWeaveR);
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Anti Rotate Clock Wise ",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        moveRightTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVERIGHT"));
                    objWeaveAction.moveRight(objWeave);
                    objWeaveAction.moveRight(objWeaveR);
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Move Right",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        moveLeftTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVELEFT"));
                    objWeaveAction.moveLeft(objWeave);
                    objWeaveAction.moveLeft(objWeaveR);
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Move Left",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        moveUpTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVEUP"));
                    objWeaveAction.moveUp(objWeave);
                    objWeaveAction.moveUp(objWeaveR);
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Move Up",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        moveDownTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVEDOWN"));
                    objWeaveAction.moveDown(objWeave);
                    objWeaveAction.moveDown(objWeaveR);
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Move Down",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
         moveRight8TP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVERIGHT8"));
                    objWeaveAction.moveRight8(objWeave);
                    objWeaveAction.moveRight8(objWeaveR);
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Move Right",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        moveLeft8TP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVELEFT8"));
                    objWeaveAction.moveLeft8(objWeave);
                    objWeaveAction.moveLeft8(objWeaveR);
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Move Left",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        moveUp8TP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVEUP8"));
                    objWeaveAction.moveUp8(objWeave);
                    objWeaveAction.moveUp8(objWeaveR);
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Move Up",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        moveDown8TP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVEDOWN8"));
                    objWeaveAction.moveDown8(objWeave);
                    objWeaveAction.moveDown8(objWeaveR);
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Move Down",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        tiltRightTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONTILTRIGHT"));
                    objWeaveAction.tiltRight(objWeave);
                    objWeaveAction.tiltRight(objWeaveR);
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Tilt Right ",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        tiltLeftTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONTILTLEFT"));
                    objWeaveAction.tiltLeft(objWeave);
                    objWeaveAction.tiltLeft(objWeaveR);
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Tilt Left ",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        tiltUpTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONTILTUP"));
                    objWeaveAction.tiltUp(objWeave);
                    objWeaveAction.tiltUp(objWeaveR);
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Tilt Up ",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        tiltDownTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONTILTDOWN"));
                    objWeaveAction.tiltDown(objWeave);
                    objWeaveAction.tiltDown(objWeaveR);
                    plotEditWeave();
                } catch (Exception ex) {
                   new Logging("SEVERE",getClass().getName(),"Tilt Down ",ex);
                   lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });         
        inversionTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONINVERSION"));
                    objWeaveAction.inversion(objWeave);
                    objWeaveAction.inversion(objWeaveR);
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Inversion ",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        popup.add(editToolPane, 0, 3);
                
        Button btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));
        btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
        btnCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                fabricChildStage.close();
                plotViewActionMode = 12;
                plotViewAction();
            }
        });
        popup.add(btnCancel, 0, 4);

        fabricChildStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                fabricChildStage.close();
                plotViewActionMode = 12;
                plotViewAction();
            }
        });
        Scene popupScene = new Scene(popup);
        popupScene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        fabricChildStage.setScene(popupScene);
        fabricChildStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("WINDOWEDITWEAVE"));
        fabricChildStage.showAndWait();   
        System.gc();
    }
    private void plotGraphMachineView(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("GETGRAPHVIEW"));
            BufferedImage bufferedImage = new BufferedImage(objFabric.getIntWarp()*(objFabric.getIntExtraWeft()+2), objFabric.getIntWeft(),BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            
            if(objFabric.getIntExtraWarp()>0 || objFabric.getIntExtraWeft()>0){
                if(objConfiguration.getBlnPunchCard()){
                    bufferedImage = objFabricAction.plotGraphJaquardMachineView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                }
            }
            //================================ For Outer to be square ===============================
            String[] data = objFabric.getObjConfiguration().getStrGraphSize().split("x");
            BufferedImage bufferedImageesize = new BufferedImage((int)(objFabric.getIntWarp()*(objFabric.getIntExtraWeft()+2)*zoomfactor*Integer.parseInt(data[1])/graphFactor), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])/graphFactor),BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bufferedImageesize.createGraphics();
            g.drawImage(bufferedImage, 0, 0, (int)(objFabric.getIntWarp()*(objFabric.getIntExtraWeft()+2)*zoomfactor*Integer.parseInt(data[1])/graphFactor), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])/graphFactor), null);
            g.setColor(java.awt.Color.BLACK);
            BasicStroke bs = new BasicStroke(2);
            g.setStroke(bs);
            
            for(int i = 0; i < objFabric.getIntWeft(); i++) {
                for(int j = 0; j < objFabric.getIntWarp()*(objFabric.getIntExtraWeft()+2); j++) {
                    if((j%Integer.parseInt(data[0]))==0){
                        bs = new BasicStroke(2);
                         g.setStroke(bs);
                    }else{
                        bs = new BasicStroke(1);
                        g.setStroke(bs);
                    }
                    g.drawLine((int)(j*zoomfactor*Integer.parseInt(data[1])/graphFactor), 0,  (int)(j*zoomfactor*Integer.parseInt(data[1])/graphFactor), (int)(objFabric.getIntWeft()*zoomfactor*Integer.parseInt(data[0])/graphFactor));
                }
                if((i%Integer.parseInt(data[1]))==0){
                    bs = new BasicStroke(2);
                    g.setStroke(bs);
                }else{
                    bs = new BasicStroke(1);
                    g.setStroke(bs);
                }
                g.drawLine(0, (int)(i*zoomfactor*Integer.parseInt(data[0])/graphFactor), (int)(objFabric.getIntWarp()*(objFabric.getIntExtraWeft()+2)*zoomfactor)*Integer.parseInt(data[1])/graphFactor, (int)(i*zoomfactor*Integer.parseInt(data[0])/graphFactor));
            }
            g.dispose();
            bufferedImage = null;
            fabric.setImage(SwingFXUtils.toFXImage(bufferedImageesize, null));
            container.setContent(fabric);
            bufferedImageesize = null;
            lblStatus.setText(objDictionaryAction.getWord("GOTGRAPHVIEW"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"plotGraphDobbyView() : Error while viewing dobby garph view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void getSuperGraphMC(){
        try {
            byte[][] newMatrix = objFabric.getFabricMatrix();
            List lstLines = new ArrayList();
            ArrayList<Byte> lstEntry = null;
            int lineCount = 0;
            for (int i = 0; i < objFabric.getIntWeft(); i++){
                lstEntry = new ArrayList();
                for (int j = 0; j < objFabric.getIntWarp(); j++){
                    //add the first color on array
                    if(lstEntry.size()==0 && newMatrix[i][j]!=1)
                        lstEntry.add(newMatrix[i][j]);
                    //check for redudancy
                    else {                
                        if(!lstEntry.contains(newMatrix[i][j]) && newMatrix[i][j]!=1)
                            lstEntry.add(newMatrix[i][j]);
                    }
                }
                lstLines.add(lstEntry);
                lineCount+=lstEntry.size();
            }
            byte[][] baseMatrix = objArtworkAction.repeatMatrix(objFabric.getBaseWeaveMatrix(),lineCount,objFabric.getIntWarp());
            byte[][] fontMatrix = new byte[lineCount][objFabric.getIntWarp()];
            lineCount=0;
            byte init = 0;
            for (int i = 0 ; i < objFabric.getIntWeft(); i++){
                lstEntry = (ArrayList)lstLines.get(i);
                for(int k=0; k<lstEntry.size(); k++, lineCount++){
                    init = (byte)lstEntry.get(k);
                    for (int j = 0; j < objFabric.getIntWarp(); j++){
                        if(init==0){
                            fontMatrix[lineCount][j] = baseMatrix[i][j];
                        }else{
                            if(newMatrix[i][j]==init){
                                fontMatrix[lineCount][j] = init;
                            }else{
                                fontMatrix[lineCount][j] = (byte)1;
                            }
                        }   
                    }
                }
            }
            newMatrix = null;
            baseMatrix = null;
            BufferedImage bufferedImage = new BufferedImage(objFabric.getIntWarp(), lineCount, BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            bufferedImage = objFabricAction.plotCrossSectionGraphView(objFabric, fontMatrix, objFabric.getIntWarp(), lineCount);
            System.gc();
            
            FileChooser fileChoser=new FileChooser();
            fileChoser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTBMP")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
            FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("BMP (.bmp)", "*.bmp");
            fileChoser.getExtensionFilters().add(extFilterBMP);
            File file=fileChoser.showSaveDialog(fabricStage);
            if(file==null)
                return;
            else
                fabricStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+file.getAbsoluteFile().getName()+"]");
            if (!file.getName().endsWith("bmp"))
                file = new File(file.getPath() +".bmp");
            ImageIO.write(bufferedImage, "BMP", file);
            bufferedImage = null;
            
            ArrayList<File> filesToZip=new ArrayList<>();
            filesToZip.add(file);
            String zipFilePath=file.getAbsolutePath()+".zip";
            String passwordToZip = file.getName();
            new EncryptZip(zipFilePath, filesToZip, passwordToZip);
            file.delete();

            lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+file.getCanonicalPath());
        } catch (IOException ex) {
            new Logging("SEVERE",FabricView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (SQLException ex) {
            new Logging("SEVERE",FabricView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",FabricView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void getSuperBaseGraphMC(){
        try {
            byte[][] newMatrix = objFabric.getFabricMatrix();
            List lstLines = new ArrayList();
            ArrayList<Byte> lstEntry = null;
            int lineCount = 0;
            byte temp = 1;                                      //added for ashis
            for (int i = 0; i < objFabric.getIntWeft(); i++){
                lstEntry = new ArrayList();
                for (int j = 0; j < objFabric.getIntWarp(); j++){
                    //add the first color on array
                    //if(lstEntry.size()==0 && newMatrix[i][j]!=1){
                    //    if(temp!=1 && temp==newMatrix[i][j]){   //added for ashis
                    //        lstEntry.add((byte)0);              //added for ashis
                    //        System.out.println(temp);           //added for ashis
                    //    }                                       //added for ashis
                   //     lstEntry.add(newMatrix[i][j]);
                    //}
                    //check for redudancy
                   // else {                
                    //    if(!lstEntry.contains(newMatrix[i][j]) && newMatrix[i][j]!=1){
                    //        temp = newMatrix[i][j];             //added for ashis
                    //        lstEntry.add(newMatrix[i][j]);                            
                    //    }
                    //}
                    
                    //-------//added for ashis
                    //add the first color on array
                    if(lstEntry.size()==0){
                        lstEntry.add((byte)0);                        
                    }
                    //check for redudancy
                    else {                
                        if(!lstEntry.contains(newMatrix[i][j]) && newMatrix[i][j]!=1){
                            lstEntry.add(newMatrix[i][j]);                            
                        }
                    }
                    //-------//added for ashis
                }
                Collections.sort(lstEntry);
                lstLines.add(lstEntry);
                lineCount+=lstEntry.size();
            }
            byte[][] baseMatrix = objArtworkAction.repeatMatrix(objFabric.getBaseWeaveMatrix(),lineCount,objFabric.getIntWarp());
            byte[][] fontMatrix = new byte[lineCount][objFabric.getIntWarp()];
            lineCount=0;
            byte init = 0;
            for (int i = 0 ; i < objFabric.getIntWeft(); i++){
                lstEntry = (ArrayList)lstLines.get(i);
                for(int k=0; k<lstEntry.size(); k++, lineCount++){
                    init = (byte)lstEntry.get(k);
                    for (int j = 0; j < objFabric.getIntWarp(); j++){
                        if(init==0){
                            fontMatrix[lineCount][j] = baseMatrix[i][j];
                        }else{
                            if(newMatrix[i][j]==init){
                                fontMatrix[lineCount][j] = init;
                            }else{
                                fontMatrix[lineCount][j] = (byte)1;
                            }
                        }   
                    }
                }
            }
            
            newMatrix = null;
            baseMatrix = null;
            BufferedImage bufferedImage = new BufferedImage(objFabric.getIntWarp(), lineCount, BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            bufferedImage = objFabricAction.plotCrossSectionBaseGraphView(objFabric, fontMatrix, objFabric.getIntWarp(), lineCount);
            System.gc();
            
            FileChooser fileChoser=new FileChooser();
            fileChoser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTBMP")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
            FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("BMP (.bmp)", "*.bmp");
            fileChoser.getExtensionFilters().add(extFilterBMP);
            File file=fileChoser.showSaveDialog(fabricStage);
            if(file==null)
                return;
            else
                fabricStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+file.getAbsoluteFile().getName()+"]");
            if (!file.getName().endsWith("bmp"))
                file = new File(file.getPath() +".bmp");
            ImageIO.write(bufferedImage, "BMP", file);
            bufferedImage = null;
            
            ArrayList<File> filesToZip=new ArrayList<>();
            filesToZip.add(file);
            String zipFilePath=file.getAbsolutePath()+".zip";
            String passwordToZip = file.getName();
            new EncryptZip(zipFilePath, filesToZip, passwordToZip);
            file.delete();

            lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+file.getCanonicalPath());
        } catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void getSuperSkipGraphMC(){
        try {
            byte[][] newMatrix = objFabric.getFabricMatrix();
            List lstLines = new ArrayList();
            ArrayList<Byte> lstEntry = null;
            int lineCount = 0;
            byte temp = 1;                                      //added for ashis
            for (int i = 0; i < objFabric.getIntWeft(); i++){
                lstEntry = new ArrayList();
                for (int j = 0; j < objFabric.getIntWarp(); j++){
                    //-------//added for ashis
                    //add the first color on array
                    if(lstEntry.size()==0 && newMatrix[i][j]>1){
                        lstEntry.add(newMatrix[i][j]);                        
                    }
                    //check for redudancy
                    else {                
                        if(!lstEntry.contains(newMatrix[i][j]) && newMatrix[i][j]>1){
                            lstEntry.add(newMatrix[i][j]);                            
                        }
                    }
                    //-------//added for ashis
                }
                Collections.sort(lstEntry);
                lstLines.add(lstEntry);
                lineCount+=lstEntry.size();
            }
            byte[][] baseMatrix = objArtworkAction.repeatMatrix(objFabric.getBaseWeaveMatrix(),lineCount,objFabric.getIntWarp());
            byte[][] fontMatrix = new byte[lineCount][objFabric.getIntWarp()];
            lineCount=0;
            byte init = 0;
            for (int i = 0 ; i < objFabric.getIntWeft(); i++){
                lstEntry = (ArrayList)lstLines.get(i);
                for(int k=0; k<lstEntry.size(); k++, lineCount++){
                    init = (byte)lstEntry.get(k);
                    for (int j = 0; j < objFabric.getIntWarp(); j++){
                        if(init==0){
                            fontMatrix[lineCount][j] = baseMatrix[i][j];
                        }else{
                            if(newMatrix[i][j]==init){
                                fontMatrix[lineCount][j] = init;
                            }else{
                                fontMatrix[lineCount][j] = (byte)1;
                            }
                        }   
                    }
                }
            }
            
            newMatrix = null;
            baseMatrix = null;
            BufferedImage bufferedImage = new BufferedImage(objFabric.getIntWarp(), lineCount, BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            bufferedImage = objFabricAction.plotCrossSectionBaseGraphView(objFabric, fontMatrix, objFabric.getIntWarp(), lineCount);
            System.gc();
            
            FileChooser fileChoser=new FileChooser();
            fileChoser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTBMP")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
            FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("BMP (.bmp)", "*.bmp");
            fileChoser.getExtensionFilters().add(extFilterBMP);
            File file=fileChoser.showSaveDialog(fabricStage);
            if(file==null)
                return;
            else
                fabricStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+file.getAbsoluteFile().getName()+"]");
            if (!file.getName().endsWith("bmp"))
                file = new File(file.getPath() +".bmp");
            ImageIO.write(bufferedImage, "BMP", file);
            bufferedImage = null;
            
            ArrayList<File> filesToZip=new ArrayList<>();
            filesToZip.add(file);
            String zipFilePath=file.getAbsolutePath()+".zip";
            String passwordToZip = file.getName();
            new EncryptZip(zipFilePath, filesToZip, passwordToZip);
            file.delete();

            lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+file.getCanonicalPath());
        } catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void plotCrossSectionFrontView(){
        try{
            lblStatus.setText(objDictionaryAction.getWord("GETCROSSSECTIONFRONTVIEW"));
            int intLength = objFabric.getIntWarp();
            int intHeight = objFabric.getIntWeft();
            byte[][] newMatrix = objFabric.getFabricMatrix();
            List lstLines = new ArrayList();
            ArrayList<Byte> lstEntry = null;
            int lineCount = 0;
            for (int i = 0; i < intHeight; i++){
                lstEntry = new ArrayList();
                for (int j = 0; j < intLength; j++){
                    //add the first color on array
                    if(lstEntry.size()==0 && newMatrix[i][j]!=1)
                        lstEntry.add(newMatrix[i][j]);
                    //check for redudancy
                    else {                
                        if(!lstEntry.contains(newMatrix[i][j]) && newMatrix[i][j]!=1)
                            lstEntry.add(newMatrix[i][j]);
                    }
                }
                lstLines.add(lstEntry);
                lineCount+=lstEntry.size();
            }
            byte[][] baseMatrix = objArtworkAction.repeatMatrix(objFabric.getBaseWeaveMatrix(),lineCount,intLength);
            byte[][] fontMatrix = new byte[lineCount][intLength];
            lineCount=0;
            byte init = 0;
            for (int i = 0 ; i < intHeight; i++){
                lstEntry = (ArrayList)lstLines.get(i);
                for(int k=0; k<lstEntry.size(); k++, lineCount++){
                    init = (byte)lstEntry.get(k);
                    for (int j = 0; j < intLength; j++){
                        if(init==0){
                            fontMatrix[lineCount][j] = baseMatrix[i][j];
                        }else{
                            if(newMatrix[i][j]==init){
                                fontMatrix[lineCount][j] = init;
                            }else{
                                fontMatrix[lineCount][j] = (byte)1;
                            }
                        }   
                    }
                }
            }
            System.out.println("Row"+lineCount);
            for (int i = 0 ; i < lineCount; i++){
                for (int j = 0; j < intLength; j++){
                    System.err.print(" "+fontMatrix[i][j]);
                }
                System.err.println();
            }
            
            newMatrix = null;
            baseMatrix = null;
            intHeight = lineCount;
            BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            
            
            bufferedImage = objFabricAction.plotCrossSectionView(objFabric, fontMatrix, intLength, intHeight);
            intHeight = (int)(intHeight*zoomfactor*3);
            intLength = (int)(intLength*zoomfactor*3);
            plotOrientation(bufferedImage, intLength, intHeight);
            bufferedImage = null;
            System.gc();
            lblStatus.setText(objDictionaryAction.getWord("GETCROSSSECTIONFRONTVIEW"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"plotCrossSectionFrontView() : Error while viewing  cross section view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"plotCrossSectionFrontView() : Error while viewing  cross section view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void plotCrossSectionRearView(){
        try{
            lblStatus.setText(objDictionaryAction.getWord("GETCROSSSECTIONREARVIEW"));
            int intLength = objFabric.getIntWarp();
            int intHeight = objFabric.getIntWeft();
            byte[][] newMatrix = objFabric.getFabricMatrix();
            List lstLines = new ArrayList();
            ArrayList<Byte> lstEntry = null;
            int lineCount = 0;
            for (int i = 0; i < intHeight; i++){
                lstEntry = new ArrayList();
                for (int j = 0; j < intLength; j++){
                    //add the first color on array
                    if(lstEntry.size()==0 && newMatrix[i][j]!=1)
                        lstEntry.add(newMatrix[i][j]);
                    //check for redudancy
                    else {                
                        if(!(lstEntry.contains(newMatrix[i][j]))  && newMatrix[i][j]!=1)
                            lstEntry.add(newMatrix[i][j]);
                    }
                }
                lstLines.add(lstEntry);
                lineCount+=lstEntry.size();
            }
            byte[][] baseMatrix = objArtworkAction.repeatMatrix(objFabric.getBaseWeaveMatrix(),lineCount,intLength);
            baseMatrix=objArtworkAction.invertMatrix(baseMatrix);
            byte[][] fontMatrix = new byte[lineCount][intLength];
            lineCount=0;
            byte init = 0;
            for (int i = 0 ; i < intHeight; i++){
                lstEntry = (ArrayList)lstLines.get(i);
                for(int k=0; k<lstEntry.size(); k++, lineCount++){
                    init = (byte)lstEntry.get(k);
                    for (int j = 0; j < intLength; j++){
                        if(init==0)
                            fontMatrix[lineCount][j] = baseMatrix[i][j];
                        else{
                            if(newMatrix[i][j]==init){
                                fontMatrix[lineCount][j] = (byte)1;
                            }else{
                                fontMatrix[lineCount][j] = init;
                            }
                        }   
                    }
                }
            }                                             
                     
            intHeight = lineCount;
            BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            bufferedImage = objFabricAction.plotCrossSectionView(objFabric, fontMatrix, intLength, intHeight);
            intHeight = (int)(intHeight*zoomfactor*3);
            intLength = (int)(intLength*zoomfactor*3);
            plotOrientation(bufferedImage, intLength, intHeight);
            bufferedImage = null;
            System.gc();
            lblStatus.setText(objDictionaryAction.getWord("GETCROSSSECTIONREARVIEW"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"plotCrossSectionRearView() : Error while viewing  cross section view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"plotCrossSectionRearView() : Error while viewing cross section view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    */
/////////////////////////////// U N U S E D    C O D E ////////////////////////
 
}