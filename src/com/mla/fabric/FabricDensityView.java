/*
 * Copyright (C) Digital India Corporation (Media Lab Asia)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mla.fabric;

import com.mla.artwork.ArtworkAction;
import com.mla.dictionary.DictionaryAction;
import com.mla.main.Configuration;
import com.mla.main.Logging;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.sql.SQLException;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
/**
 *
 * @Designing GUI window for weave editor
 * @author Amit Kumar Singh
 * 
 */
public class FabricDensityView {
   
    private Fabric objFabric;
    private Configuration objConfiguration;
    DictionaryAction objDictionaryAction;
    
    private Stage densityStage;
    GridPane container; 
    
    private Button btnApply;
    private Button btnCancel;
    
    private TextField epiTF;
    private TextField ppiTF;
    private TextField artworkLengthTF;
    private TextField artworkWidthTF;    
    private TextField fabricLengthTF;
    private TextField fabricWidthTF;
    private ImageView threadIV;
    ImageView warpIV;
    ImageView weftIV;
    
    private Label lblStatus;
    ProgressBar progressB;
    ProgressIndicator progressI;
    
    public FabricDensityView(final Stage primaryStage) {  }

    public FabricDensityView(Fabric objFabricCall) {   
        this.objFabric = objFabricCall;
        objDictionaryAction = new DictionaryAction(objFabric.getObjConfiguration());
        objConfiguration = objFabric.getObjConfiguration();
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 520, 300, Color.WHITE);
        scene.getStylesheets().add(FabricDensityView.class.getResource(objFabric.getObjConfiguration().getStrTemplate()+"/style.css").toExternalForm());
        HBox footContainer = new HBox();
        progressB = new ProgressBar(0);
        progressB.setVisible(false);
        progressI = new ProgressIndicator(0);
        progressI.setVisible(false);
        lblStatus = new Label(objDictionaryAction.getWord("WELCOME"));
        lblStatus.setId("message");
        footContainer.getChildren().addAll(lblStatus);//,progressB,progressI
        
        container = new GridPane();
        container.setId("popup");
        container.setVgap(5);
        container.setHgap(5);
        container.autosize();
        
        Label reedCount= new Label(objDictionaryAction.getWord("REEDCOUNT")+" :");
        reedCount.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPREEDCOUNT")));
        reedCount.setPrefWidth(scene.getWidth()/4);
        container.add(reedCount, 0, 0);
        objConfiguration.setIntReedCount(objFabric.getIntReedCount());
        final TextField reedCountTF = new TextField(Integer.toString(objConfiguration.getIntReedCount())){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };    
        reedCountTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPREEDCOUNT")));
        reedCountTF.setPrefWidth(scene.getWidth()/4);
        container.add(reedCountTF, 1, 0);
        
        Label tpd= new Label(objDictionaryAction.getWord("TPD")+" :");
        tpd.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPTPD")));
        tpd.setPrefWidth(scene.getWidth()/4);
        container.add(tpd, 2, 0);
        objConfiguration.setIntTPD(objFabric.getIntTPD());
        final TextField tpdTF = new TextField(Integer.toString(objConfiguration.getIntTPD())){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };    
        tpdTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPTPD")));
        tpdTF.setPrefWidth(scene.getWidth()/4);
        container.add(tpdTF, 3, 0);
        
        Label hpi= new Label(objDictionaryAction.getWord("HPI")+" :");
        hpi.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPHPI")));
        hpi.setPrefWidth(scene.getWidth()/4);
        container.add(hpi, 0, 1);
        objConfiguration.setIntHPI(objFabric.getIntHPI());
        final TextField hpiTF = new TextField(Integer.toString(objConfiguration.getIntHPI()));
        hpiTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPHPI")));
        hpiTF.setPrefWidth(scene.getWidth()/4);
        hpiTF.setDisable(true);
        container.add(hpiTF, 1, 1);
        
        Label dents= new Label(objDictionaryAction.getWord("DENTS")+" :");
        dents.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPDENTS")));
        dents.setPrefWidth(scene.getWidth()/4);
        container.add(dents, 2, 1);
        objConfiguration.setIntDents(objFabric.getIntDents());
        final TextField dentsTF = new TextField(Integer.toString(objConfiguration.getIntDents()));    
        dentsTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPDENTS")));
        dentsTF.setPrefWidth(scene.getWidth()/4);
        dentsTF.setDisable(true);
        container.add(dentsTF, 3, 1);
        
        Label epi= new Label(objDictionaryAction.getWord("EPI")+" :");
        epi.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPEPI")));
        epi.setPrefWidth(scene.getWidth()/4);
        container.add(epi, 0, 2);
        objConfiguration.setIntEPI(objFabric.getIntEPI());
        epiTF = new TextField(Integer.toString(objConfiguration.getIntEPI())){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };    
        epiTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPEPI")));
        epiTF.setPrefWidth(scene.getWidth()/4);
        epiTF.setEditable(false);
        container.add(epiTF, 1, 2);   
        
        Label ppi= new Label(objDictionaryAction.getWord("PPI")+" :");
        ppi.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPPPI")));
        ppi.setPrefWidth(scene.getWidth()/4);
        container.add(ppi, 2, 2);
        objConfiguration.setIntPPI(objFabric.getIntPPI());
        ppiTF = new TextField(Integer.toString(objConfiguration.getIntPPI())){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };    
        ppiTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPPPI")));
        ppiTF.setPrefWidth(scene.getWidth()/4);
        container.add(ppiTF, 3, 2);
        
        Label artworkWidth= new Label(objDictionaryAction.getWord("ARTWORKWIDTH")+" (inch):");
        artworkWidth.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPARTWORKWIDTH")));
        artworkWidth.setPrefWidth(scene.getWidth()/4);
        container.add(artworkWidth, 0, 3);
        objConfiguration.setIntHooks(objFabric.getIntWarp());
        objConfiguration.setIntEnds(objFabric.getIntWarp());
        objFabric.setDblArtworkWidth(Double.parseDouble(String.format("%.3f",objFabric.getDblArtworkWidth())));
        objConfiguration.setDblArtworkWidth(objFabric.getDblArtworkWidth());
        artworkWidthTF = new TextField(Double.toString(objConfiguration.getDblArtworkWidth()));
        artworkWidthTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPARTWORKWIDTH")));
        artworkWidthTF.setPrefWidth(scene.getWidth()/4);
        artworkWidthTF.setEditable(false);
        container.add(artworkWidthTF, 1, 3);
        
        Label artworkLength= new Label(objDictionaryAction.getWord("ARTWORKLENGTH")+" (inch):");
        artworkLength.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPARTWORKLENGTH")));
        artworkLength.setPrefWidth(scene.getWidth()/4);
        container.add(artworkLength, 2, 3);
        objConfiguration.setIntPixs(objFabric.getIntWeft());
        objFabric.setDblArtworkLength(Double.parseDouble(String.format("%.3f",objFabric.getDblArtworkLength())));
        objConfiguration.setDblArtworkLength(objFabric.getDblArtworkLength());
        artworkLengthTF = new TextField(Double.toString(objConfiguration.getDblArtworkLength()));
        artworkLengthTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPARTWORKLENGTH")));
        artworkLengthTF.setPrefWidth(scene.getWidth()/4);
        artworkLengthTF.setEditable(false);
        container.add(artworkLengthTF, 3, 3);  
        
        Label graphSize= new Label(objDictionaryAction.getWord("GRAPHSIZE")+" :");
        graphSize.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPGRAPHSIZE")));
        graphSize.setPrefWidth(scene.getWidth()/4);
        container.add(graphSize, 0, 4);
        objFabric.getObjConfiguration().setStrGraphSize(12+"x"+(int)((12*objFabric.getIntPPI())/objFabric.getIntHPI()));
        objConfiguration.setStrGraphSize(objFabric.getObjConfiguration().getStrGraphSize());
        final TextField graphSizeTF = new TextField();
        graphSizeTF.setText(objConfiguration.getStrGraphSize());
        graphSizeTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPGRAPHSIZE")));
        graphSizeTF.setPrefWidth(scene.getWidth()/4);
        graphSizeTF.setDisable(true);
        container.add(graphSizeTF, 1, 4);   
        
        CheckBox chkEditEPI = new CheckBox(objDictionaryAction.getWord("EDIT")+" "+objDictionaryAction.getWord("EPI"));
        chkEditEPI.setTooltip(new Tooltip(objDictionaryAction.getWord("EDIT")+" "+objDictionaryAction.getWord("EPI")));
        chkEditEPI.setPrefWidth(scene.getWidth()/4);
        chkEditEPI.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                epiTF.setEditable(t1);
                reedCountTF.setDisable(t1);
                tpdTF.setDisable(t1);
            }
        });
        container.add(chkEditEPI, 2, 4, 2, 1);
        
        Label fabricWidth = new Label(objDictionaryAction.getWord("FABRICWIDTH")+"(inch)");
        fabricWidth.setPrefWidth(scene.getWidth()/4);
        container.add(fabricWidth, 0, 5);
        fabricWidthTF = new TextField(Double.toString(objFabric.getDblFabricWidth()));
        fabricWidthTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPFABRICWIDTH")));
        fabricWidthTF.setPrefWidth(scene.getWidth()/4);
        container.add(fabricWidthTF, 1, 5);
        
        Label fabricLength = new Label(objDictionaryAction.getWord("FABRICLENGTH")+"(inch)");
        fabricLength.setPrefWidth(scene.getWidth()/4);
        container.add(fabricLength, 2, 5);
        fabricLengthTF = new TextField(Double.toString(objFabric.getDblFabricLength()));
        fabricLengthTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPFABRICLENGTH")));
        fabricLengthTF.setPrefWidth(scene.getWidth()/4);
        container.add(fabricLengthTF, 3, 5);
        
        reedCountTF.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                try {
                    if(Integer.parseInt(reedCountTF.getText())>0){
                        objConfiguration.setIntReedCount(Integer.parseInt(t1));
                        dentsTF.setText(Integer.toString(objConfiguration.findIntDents()));
                        hpiTF.setText(Integer.toString(objConfiguration.findIntHPI()));
                        epiTF.setText(Integer.toString(objConfiguration.findIntEPI()));
                    }
                } catch (Exception ex) {
                    new Logging("SEVERE",FabricDensityView.class.getName(),"Reed Count property change",ex);
                }
            }
        });
        tpdTF.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                try {
                    if(Integer.parseInt(dentsTF.getText())>0){
                        objConfiguration.setIntTPD(Integer.parseInt(t1));
                        epiTF.setText(Integer.toString(objConfiguration.findIntEPI()));                        
                    }
                } catch (Exception ex) {
                    new Logging("SEVERE",FabricDensityView.class.getName(),"Threads per dent property change",ex);
                }
            }
        });
        hpiTF.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                try {
                    if(Integer.parseInt(hpiTF.getText())>0 && Integer.parseInt(ppiTF.getText())>0){
                        objConfiguration.setIntHPI(Integer.parseInt(t1));
                        artworkWidthTF.setText(String.format("%.3f",objConfiguration.findDblArtworkWidth()));
                    }
                } catch (Exception ex) {
                    new Logging("SEVERE",FabricDensityView.class.getName(),"Hooks property change",ex);
                }
            }
        });
        epiTF.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                try {
                    if(Integer.parseInt(epiTF.getText())>0){
                        objConfiguration.setIntEPI(Integer.parseInt(t1));
                        graphSizeTF.setText(objConfiguration.findStrGraphSize());
                    }
                } catch (Exception ex) {
                    new Logging("SEVERE",FabricDensityView.class.getName(),"EPI property change",ex);
                }
            }
        });
        ppiTF.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                try {
                    if(Integer.parseInt(ppiTF.getText())>0){
                        objConfiguration.setIntPPI(Integer.parseInt(t1));
                        artworkLengthTF.setText(String.format("%.3f",objConfiguration.findDblArtworkLength()));
                        graphSizeTF.setText(objConfiguration.findStrGraphSize());
                        threadIV.setImage(SwingFXUtils.toFXImage(getMatrix(), null));
                    }
                } catch (Exception ex) {
                    new Logging("SEVERE",FabricDensityView.class.getName(),"PPI property change",ex);
                }
            }
        }); 
        artworkWidthTF.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                try {
                    if(Double.parseDouble(artworkWidthTF.getText())>0){
                        threadIV.setImage(SwingFXUtils.toFXImage(getMatrix(), null));
                    }
                } catch (Exception ex) {
                    new Logging("SEVERE",FabricDensityView.class.getName(),"PPI property change",ex);
                }
            }
        });
        artworkLengthTF.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                try {
                    if(Double.parseDouble(artworkLengthTF.getText())>0){
                        threadIV.setImage(SwingFXUtils.toFXImage(getMatrix(), null));
                    }
                } catch (Exception ex) {
                    new Logging("SEVERE",FabricDensityView.class.getName(),"PPI property change",ex);
                }
            }
        });
        
        final Separator sepHor1 = new Separator();
        sepHor1.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHor1, 0, 6);
        GridPane.setColumnSpan(sepHor1, 4);
        container.getChildren().add(sepHor1);
        
        btnApply = new Button(objDictionaryAction.getWord("APPLY"));
        btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));

        //btnUpdate.setMaxWidth(Double.MAX_VALUE);
        //btnClose.setMaxWidth(Double.MAX_VALUE);        

        btnApply.setGraphic(new ImageView(objFabric.getObjConfiguration().getStrColour()+"/"+objFabric.getObjConfiguration().strIconResolution+"/save.png"));
        btnCancel.setGraphic(new ImageView(objFabric.getObjConfiguration().getStrColour()+"/"+objFabric.getObjConfiguration().strIconResolution+"/close.png"));
        
        btnApply.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPAPPLY")));
        btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
        
        btnApply.setPrefWidth(scene.getWidth()/4);
        btnCancel.setPrefWidth(scene.getWidth()/4);
        
        btnCancel.setDefaultButton(true);
        //btnApply.setDisable(false);
        
        btnApply.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {                                        
               objFabric.setIntReedCount(Integer.parseInt(reedCountTF.getText()));
               objFabric.setIntDents(Integer.parseInt(dentsTF.getText()));
               objFabric.setIntTPD(Integer.parseInt(tpdTF.getText()));
               objFabric.setIntHPI(Integer.parseInt(hpiTF.getText()));
               objFabric.setIntEPI(Integer.parseInt(epiTF.getText()));
               objFabric.setIntPPI(Integer.parseInt(ppiTF.getText()));
               objFabric.setDblArtworkLength(Double.parseDouble(artworkLengthTF.getText()));
               objFabric.setDblArtworkWidth(Double.parseDouble(artworkWidthTF.getText()));
               objFabric.setDblFabricLength(Double.parseDouble(fabricLengthTF.getText()));
               objFabric.setDblFabricWidth(Double.parseDouble(fabricWidthTF.getText()));
               objFabric.getObjConfiguration().setStrGraphSize(graphSizeTF.getText());
               densityStage.close();
            }
        });
        btnCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                densityStage.close();
            }
        });
        
        container.add(btnApply, 1, 7); 
        container.add(btnCancel, 2, 7);
        
        threadIV = new ImageView(objFabric.getObjConfiguration().getStrColour()+"/"+objFabric.getObjConfiguration().strIconResolution+"/help.png");
        threadIV.setFitWidth(450);
        threadIV.setFitHeight(150);
        
        threadIV.setImage(SwingFXUtils.toFXImage(getMatrix(), null));
        //container.add(threadIV, 0, 4, 4, 1);
        lblStatus.setText("Most monitors display images at approximately 75DPI");
        
        root.setTop(container);
        //root.setCenter(threadIV);
        root.setBottom(footContainer);
        densityStage = new Stage(); 
        densityStage.setScene(scene);
        densityStage.initModality(Modality.APPLICATION_MODAL);//WINDOW_MODAL
        densityStage.initStyle(StageStyle.UTILITY); 
        densityStage.getIcons().add(new Image("/media/icon.png"));
        densityStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWDENSITY")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
        densityStage.setIconified(false);
        densityStage.setResizable(false);
        densityStage.showAndWait();
    }
    
    public BufferedImage getMatrix(){
        int intLength = objFabric.getIntWarp();
        int intWidth = objFabric.getIntWeft();
            
        int dpi = objConfiguration.getIntDPI();
        int warpFactor = dpi/Integer.parseInt(epiTF.getText());
        int weftFactor = dpi/Integer.parseInt(ppiTF.getText());
        
        BufferedImage bufferedImage = new BufferedImage(intLength, intWidth,BufferedImage.TYPE_INT_RGB);
        FabricAction objFabricAction = null;
        try {
            objFabricAction = new FabricAction();
        } catch (SQLException ex) {
            new Logging("SEVERE",FabricDensityView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
        bufferedImage = objFabricAction.plotFrontSideView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), intLength, intWidth);
        if((Integer.parseInt(epiTF.getText())/Integer.parseInt(ppiTF.getText()))<=0)
            intWidth = intLength * (Integer.parseInt(ppiTF.getText())/Integer.parseInt(epiTF.getText()));
        else
            intLength = intWidth * (Integer.parseInt(epiTF.getText())/Integer.parseInt(ppiTF.getText()));
        
        BufferedImage newImage = new BufferedImage(intLength, intWidth, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = newImage.createGraphics();
        g.drawImage(bufferedImage, 0, 0, intLength, intWidth, null);
        g.dispose();
        bufferedImage = newImage;
        if(objConfiguration.getBlnMRepeat()){
            try {
                ArtworkAction objArtworkAction = new ArtworkAction();
                newImage = objArtworkAction.getImageRepeat(bufferedImage, 1, ((int)(350/intWidth)>0)?(int)(350/intWidth):1);
            } catch (SQLException ex) {
                new Logging("SEVERE",FabricDensityView.class.getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        }
        bufferedImage = null;
        System.gc();
        return newImage;
     }
    /*
     public BufferedImage getMatrix(){
        int w = 333;
        int h = 111; 
        int dpi = objConfiguration.getIntDPI();
        int warpFactor = dpi/Integer.parseInt(warpDTF.getText());
        int weftFactor = dpi/Integer.parseInt(weftDTF.getText());
        byte[][] threadMatrix = objFabric.getBaseWeaveMatrix();        
        Yarn[] warpYarn = objFabric.getWeftYarn();
        Yarn[] weftYarn = objFabric.getWarpYarn();
        int warpCount = warpYarn.length;
        int weftCount = weftYarn.length;
        int j = threadMatrix[0].length;
        int i = threadMatrix.length;
        BufferedImage newImage = new BufferedImage(w, h,BufferedImage.TYPE_INT_RGB);   
        for(int x = 0; x<h; x++){
            int weft = x/weftFactor;
            if(x%weftFactor==0){
                for(int y = 0; y<w; y++){
                    int rgb = 0;
                    int warp = y/warpFactor;
                    if(y%warpFactor==0){
                        if(threadMatrix[weft%i][warp%j]==0)
                            rgb = new java.awt.Color((float)Color.web(weftYarn[weft%weftCount].getThreadColor()).getRed(),(float)Color.web(weftYarn[weft%weftCount].getThreadColor()).getGreen(),(float)Color.web(weftYarn[weft%weftCount].getThreadColor()).getBlue()).getRGB(); 
                        else
                            rgb = new java.awt.Color((float)Color.web(warpYarn[warp%warpCount].getThreadColor()).getRed(),(float)Color.web(warpYarn[warp%warpCount].getThreadColor()).getGreen(),(float)Color.web(warpYarn[warp%warpCount].getThreadColor()).getBlue()).getRGB();
                    }else{
                        rgb = new java.awt.Color((float)Color.LIGHTGRAY.getRed(),(float)Color.LIGHTGRAY.getGreen(),(float)Color.LIGHTGRAY.getBlue()).getRGB();
                    }
                    newImage.setRGB(y, x, rgb);
              }
            }else{
                for(int y = 0; y<w; y++){
                    int rgb = new java.awt.Color((float)Color.LIGHTGRAY.getRed(),(float)Color.LIGHTGRAY.getGreen(),(float)Color.LIGHTGRAY.getBlue()).getRGB();
                    newImage.setRGB(y, x, rgb);
              }
            }
        }  
        return newImage;
     }
      */  
    public void start(Stage stage) throws Exception {
        stage.initOwner(FabricView.fabricStage);
        new FabricDensityView(stage);        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    } 
}

