/*
 * Copyright (C) Digital India Corporation (formerly  Media Lab Asia)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mla.fabric;

import com.mla.dictionary.DictionaryAction;
import com.mla.main.Logging;
import com.mla.main.MessageView;
import com.mla.user.UserAction;
import com.sun.media.jai.codec.ByteArraySeekableStream;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.SeekableStream;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.media.jai.PlanarImage;
/**
 *
 * @Designing GUI window for fabric import
 * @author Amit Kumar Singh
 * 
 */
public class FabricImportView {
    
   Fabric objFabric=null;
   DictionaryAction objDictionaryAction;
   
   private BorderPane root;
   private Scene scene;
   private Stage fabricStage;
   
   private TextField fileName;
   private ComboBox fileSearch;
   private ComboBox fileAccess;
   private ComboBox fileSort;
   private ComboBox fileDirection;
   
   private GridPane GP_container;
   int currentPage; // index of page loaded from fabric library
   int perPage; // Number of fabric icons per page
	 
   public FabricImportView(final Stage primaryStage) {  }
    /*
    @author Amit Singh
    function for creating weave import window     
    */
   public FabricImportView(Fabric objFabricCall,String strParent) {
        objFabric = objFabricCall;
        objDictionaryAction = new DictionaryAction(objFabric.getObjConfiguration());
        currentPage=0;
        perPage=20;
        
        fabricStage = new Stage();
        fabricStage.initModality(Modality.APPLICATION_MODAL);//WINDOW_MODAL
        fabricStage.initStyle(StageStyle.UTILITY);
        root = new BorderPane();
        scene = new Scene(root, 480, 450, Color.WHITE);
        scene.getStylesheets().add(FabricImportView.class.getResource(objFabric.getObjConfiguration().getStrTemplate()+"/style.css").toExternalForm());
        GridPane topContainer = new GridPane();
        topContainer.setId("subpopup");
        topContainer.setVgap(2);
        topContainer.setHgap(2);
        
        Label lblFileName = new Label(objDictionaryAction.getWord("NAME"));
        lblFileName.setId("filename");
        lblFileName.setPrefWidth(0.8*scene.getWidth()/7);
        topContainer.add(lblFileName,0,0);
        
        fileName = new TextField();
        fileName.setPromptText(objDictionaryAction.getWord("PROMPTNAME"));
        fileName.setPrefWidth(1.4*scene.getWidth()/7);
        topContainer.add(fileName,1,0);
        
        Label lblFileAccess = new Label(objDictionaryAction.getWord("SEARCHACCESS"));
        lblFileAccess.setId("fileAccess");
        lblFileAccess.setPrefWidth(scene.getWidth()/7);
        topContainer.add(lblFileAccess, 2, 0);
   
        fileAccess = new ComboBox();
        fileAccess.getItems().addAll(
            "All User Data",
            "Public",
            "Protected",
            "Private"
        );
        fileAccess.setPromptText(objDictionaryAction.getWord("SEARCHACCESS"));
        fileAccess.setPrefWidth(1.6*scene.getWidth()/7);
        fileAccess.setEditable(false); 
        fileAccess.setValue("All User Data"); 
        topContainer.add(fileAccess, 3, 0); 

        Label lblFileSearch = new Label(objDictionaryAction.getWord("SEARCHBY"));
        lblFileSearch.setId("fileSearch");
        lblFileSearch.setPrefWidth(scene.getWidth()/7);
        topContainer.add(lblFileSearch, 4, 0);
        
        fileSearch = new ComboBox();
        fileSearch.getItems().addAll(
            "All Cloth Type",
            "Body",
            "Pallu",
            "Border",
            "Cross Border",
            "Blouse",
            "Skirt",
            "Konia"
        );
        fileSearch.setPromptText(objDictionaryAction.getWord("SEARCHBY"));
        fileSearch.setPrefWidth(1.2*scene.getWidth()/7);
        fileSearch.setEditable(false); 
        fileSearch.setValue(objFabric.getStrClothType());
        topContainer.add(fileSearch, 5, 0);
        if(strParent.equalsIgnoreCase("ClothEditor"))
            fileSearch.setDisable(true);

        Label lblFileSort = new Label(objDictionaryAction.getWord("SORTBY"));
        lblFileSort.setId("fileSort");
        lblFileSort.setPrefWidth(0.8*scene.getWidth()/7);
        topContainer.add(lblFileSort,0,1);
        
        fileSort = new ComboBox();
        fileSort.getItems().addAll(
            "Name",
            "Date"
        );
        fileSort.setPromptText(objDictionaryAction.getWord("SORTBY"));
        fileSort.setPrefWidth(1.4*scene.getWidth()/7);
        fileSort.setEditable(false); 
        fileSort.setValue("Name"); 
        topContainer.add(fileSort,1,1); 

        Label lblFileDirection = new Label(objDictionaryAction.getWord("SORTDIRCTION"));
        lblFileDirection.setId("fileDirection");
        lblFileDirection.setPrefWidth(scene.getWidth()/7);
        topContainer.add(lblFileDirection, 2, 1);
   
        fileDirection = new ComboBox();
        fileDirection.getItems().addAll(
            "Ascending",
            "Descending"
        );
        fileDirection.setPromptText(objDictionaryAction.getWord("SORTDIRCTION"));
        fileDirection.setPrefWidth(1.6*scene.getWidth()/7);
        fileDirection.setEditable(false); 
        fileDirection.setValue("Ascending"); 
        topContainer.add(fileDirection,3,1); 

        GP_container = new GridPane();         
        GP_container.setAlignment(Pos.CENTER);
        GP_container.setHgap(5);
        GP_container.setVgap(5);
        GP_container.setPadding(new Insets(1, 1, 1, 1));
       
        objFabric.setStrCondition("");
        objFabric.setStrSearchBy(objFabric.getStrClothType());
        objFabric.setStrSearchAccess("All User Data");
        objFabric.setStrOrderBy("Name");
        objFabric.setStrDirection("Ascending");
        objFabric.setStrLimit((currentPage*perPage)+","+perPage);
        populateContainer(); 
        
        final Button btnPrev=new Button("<");
        btnPrev.setDisable(true);
        final Button btnNext=new Button(">");
        btnPrev.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if(currentPage!=0){
                    currentPage--;
                    objFabric.setStrLimit((currentPage*perPage)+","+perPage);
                    populateContainer();
                    if(btnNext.isDisabled())
                        btnNext.setDisable(false);
                }
                else{
                    btnPrev.setDisable(true);
                    btnNext.setDisable(false);
                }
            }
        });
        
        btnNext.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if(!btnNext.isDisabled()){
                    currentPage++;
                    objFabric.setStrLimit((currentPage*perPage)+","+perPage);
                    populateContainer();
                    if(btnPrev.isDisabled())
                        btnPrev.setDisable(false);
                    if(GP_container.getChildren().size()<=1){
                        currentPage--;
                        objFabric.setStrLimit((currentPage*perPage)+","+perPage);
                        populateContainer();
                        btnNext.setDisable(true);
                    }
                }
            }
        });

        fileName.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent ke) {
                objFabric.setStrCondition(fileName.getText());
                populateContainer(); 
            }
        });
        fileSearch.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String oldValue, String newValue) {
                objFabric.setStrSearchBy(newValue);
                populateContainer(); 
            }    
        });   
        fileAccess.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String oldValue, String newValue) {
                objFabric.setStrSearchAccess(newValue);
                populateContainer(); 
            }    
        });   
        fileSort.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String oldValue, String newValue) {
                objFabric.setStrOrderBy(newValue);
                populateContainer(); 
            }    
        });
        fileDirection.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String oldValue, String newValue) {
                objFabric.setStrDirection(newValue);
                populateContainer();
            }    
        });
        
        ScrollPane container = new ScrollPane();
        container.setId("popup");
        container.setContent(GP_container);
        
        HBox bottomContainer = new HBox(10);
        bottomContainer.setAlignment(Pos.CENTER);
        bottomContainer.getChildren().addAll(btnPrev, new Label(objDictionaryAction.getWord("HOVERITEM")), btnNext);
        
        root.setTop(topContainer);  
        root.setCenter(container);
        root.setBottom(bottomContainer);
        fabricStage.setScene(scene);
        fabricStage.getIcons().add(new Image("/media/icon.png"));
        fabricStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWFABRICIMPORT")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
        fabricStage.setIconified(false);
        fabricStage.setResizable(false);
        fabricStage.showAndWait();
    }

    /*
    @author Amit Singh
    Function used to paint grid of iamge with their name    
    */  
    public void populateContainer(){        
        GP_container.getChildren().clear();
        List lstFabricDeatails=null, lstFabric;
        BufferedImage bufferedImage = null;
        try {
            FabricAction objFabricAction = new FabricAction();
            lstFabricDeatails = objFabricAction.lstImportFabric(objFabric);
            if(lstFabricDeatails.size()==0){
                GP_container.add(new Text(objDictionaryAction.getWord("NOVALUE")), 0, 0);
            }else{
                for (int i=0, j = lstFabricDeatails.size(); i<j;i++){
                    lstFabric = (ArrayList)lstFabricDeatails.get(i);
                    SeekableStream stream = new ByteArraySeekableStream((byte[])lstFabric.get(26));
                    String[] names = ImageCodec.getDecoderNames(stream);
                    ImageDecoder dec = ImageCodec.createImageDecoder(names[0], stream, null);
                    RenderedImage im = dec.decodeAsRenderedImage();
                    bufferedImage = PlanarImage.wrapRenderedImage(im).getAsBufferedImage();
                    Image image=SwingFXUtils.toFXImage(bufferedImage, null);
                    
                    final ImageView imageView = new ImageView(image);
                    imageView.setFitHeight(111);
                    imageView.setFitWidth(111);
                    imageView.setId(lstFabric.get(2)+":"+lstFabric.get(29).toString());
                    imageView.setUserData(lstFabric.get(0).toString());
                    String strTooltip =
                            objDictionaryAction.getWord("NAME")+": "+lstFabric.get(1).toString()+"\n"+
                            objDictionaryAction.getWord("CLOTHTYPE")+": "+lstFabric.get(2)+"\n"+
                            objDictionaryAction.getWord("FABRICTYPE")+": "+lstFabric.get(3)+"\n"+
                            objDictionaryAction.getWord("FABRICLENGTH")+": "+lstFabric.get(4)+"\n"+
                            objDictionaryAction.getWord("FABRICWIDTH")+": "+lstFabric.get(5)+"\n"+
                            objDictionaryAction.getWord("ARTWORKLENGTH")+": "+lstFabric.get(6)+"\n"+
                            objDictionaryAction.getWord("ARTWORKWIDTH")+": "+lstFabric.get(7)+"\n"+
                            objDictionaryAction.getWord("WEFT")+": "+lstFabric.get(10)+"\n"+
                            objDictionaryAction.getWord("WARP")+": "+lstFabric.get(11)+"\n"+
                            objDictionaryAction.getWord("SHAFT")+": "+lstFabric.get(14)+"\n"+
                            objDictionaryAction.getWord("HOOKS")+": "+lstFabric.get(15)+"\n"+
                            objDictionaryAction.getWord("HPI")+": "+lstFabric.get(16)+"\n"+
                            objDictionaryAction.getWord("REEDCOUNT")+": "+lstFabric.get(17)+"\n"+
                            objDictionaryAction.getWord("DENTS")+": "+lstFabric.get(18)+"\n"+
                            objDictionaryAction.getWord("TPD")+": "+lstFabric.get(19)+"\n"+
                            objDictionaryAction.getWord("EPI")+": "+lstFabric.get(20)+"\n"+
                            objDictionaryAction.getWord("PPI")+": "+lstFabric.get(21)+"\n"+
                            objDictionaryAction.getWord("PROTECTION")+": "+lstFabric.get(22)+"\n"+
                            objDictionaryAction.getWord("BINDING")+": "+lstFabric.get(23)+"\n"+
                            objDictionaryAction.getWord("PERMISSION")+": "+lstFabric.get(29)+"\n"+
                            objDictionaryAction.getWord("BY")+": "+lstFabric.get(31).toString()+"\n"+
                            objDictionaryAction.getWord("DATE")+": "+lstFabric.get(30).toString();
                    Tooltip toolTip = new Tooltip(strTooltip);
                    Tooltip.install(imageView, toolTip);
                    imageView.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            String strAccess = imageView.getId().substring(imageView.getId().indexOf(':')+1,imageView.getId().length());
                            
                            if(strAccess.equalsIgnoreCase("Public")){
                                objFabric.getObjConfiguration().setServicePasswordValid(true);
                            } else{
                                new MessageView(objFabric.getObjConfiguration());
                            }
                            if(objFabric.getObjConfiguration().getServicePasswordValid()){
                                try {
                                    objFabric.getObjConfiguration().setServicePasswordValid(false);
                                    
                                    objFabric.getObjConfiguration().setStrClothType(imageView.getId().substring(0,imageView.getId().indexOf(':')));
                                    UserAction objUserAction = new UserAction();
                                    objUserAction.getConfiguration(objFabric.getObjConfiguration());
                                    //objFabric.getObjConfiguration().clothRepeat();
                                    objFabric.setStrFabricID(imageView.getUserData().toString());
                                    System.gc();
                                    fabricStage.close();
                                } catch (SQLException ex) {
                                    new Logging("SEVERE",getClass().getName(),"FabricImportView()",ex);
                                }
                            }
                        }
                    });
                    GP_container.add(imageView, i%4, i/4);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            new Logging("SEVERE",getClass().getName(),"FabricImportView()",ex);
        } catch (Exception ex){
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            new Logging("SEVERE",getClass().getName(),"FabricImportView()",ex);
        }
    }
   
    public void start(Stage stage) throws Exception {
        stage.initOwner(FabricView.fabricStage);
        new FabricImportView(stage);        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
  
}
