/*
 * Copyright (C) Digital India Corporation (Media Lab Asia)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.mla.artwork;

import com.mla.dictionary.DictionaryAction;
import com.mla.main.Configuration;
import com.mla.main.Logging;
import com.sun.media.jai.codec.ByteArraySeekableStream;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.SeekableStream;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.imageio.ImageIO;
import javax.media.jai.PlanarImage;
/**
 *
 * @Designing GUI window for weave editor
 * @author Amit Kumar Singh
 * 
 */
public class ArtworkDensityView {
   
    private Artwork objArtwork;
    private Configuration objConfiguration;
    DictionaryAction objDictionaryAction;
    
    private Stage densityStage;
    GridPane container; 
    
    private Button btnApply;
    private Button btnReset;
    private Button btnCancel;
       
    private TextField txtWidthHooks;
    private TextField txtWidth;
    private TextField txtHeight;
    private TextField txtWidthMesaure;
    private TextField txtHeightMesaure;
    private TextField txtWidthDensity;
    private TextField txtWidthDensityHooks;
    private TextField txtHeightDensity;
    private CheckBox aspectRatioCB;
    private Label graphSize;
    private ImageView pixelIV;
    private ImageView linearIV;
    private ImageView densityIV;     
    private ImageView threadIV;
    
    final String regExDouble="[0-9]{1,13}(\\.[0-9]*)?";
    
    byte resizeLockValue = 2; //1=Pixel 2=Inches 3=density
    BufferedImage bufferedImage;
    
    private Label lblStatus;
    ProgressBar progressB;
    ProgressIndicator progressI;
    
    public ArtworkDensityView(final Stage primaryStage) {  }

    public ArtworkDensityView(Artwork objArtworkCall) throws Exception{   
        this.objArtwork = objArtworkCall;
        objDictionaryAction = new DictionaryAction(objArtwork.getObjConfiguration());
        objConfiguration = objArtwork.getObjConfiguration();
        
        byte[] bytArtworkThumbnil=objArtwork.getBytArtworkThumbnil();
        SeekableStream stream = new ByteArraySeekableStream(bytArtworkThumbnil);
        String[] names = ImageCodec.getDecoderNames(stream);
        ImageDecoder dec = ImageCodec.createImageDecoder(names[0], stream, null);
        RenderedImage im = dec.decodeAsRenderedImage();
        bufferedImage = PlanarImage.wrapRenderedImage(im).getAsBufferedImage();
        bytArtworkThumbnil=null;
        
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root);
        scene.getStylesheets().add(ArtworkDensityView.class.getResource(objArtwork.getObjConfiguration().getStrTemplate()+"/style.css").toExternalForm());
        HBox footContainer = new HBox();
        progressB = new ProgressBar(0);
        progressB.setVisible(false);
        progressI = new ProgressIndicator(0);
        progressI.setVisible(false);
        lblStatus = new Label(objDictionaryAction.getWord("WELCOME"));
        lblStatus.setId("message");
        footContainer.getChildren().addAll(lblStatus,progressB,progressI);
        
        container = new GridPane();
        container.setId("popup");
        container.setVgap(5);
        container.setHgap(5);
        //container.autosize();
        
        objConfiguration.setStrGraphSize(12+"x"+(int)((12*objConfiguration.getIntPPI())/objConfiguration.getIntEPI()));
            
        container.add(new Label("Graph Size:"), 0, 0);
        graphSize = new Label(objConfiguration.getStrGraphSize());
        container.add(graphSize, 1, 0);
        container.add(new Label("Artwork Size:"), 0, 1);
        container.add(new Label((int)bufferedImage.getWidth()+" x "+(int)bufferedImage.getHeight()), 1, 1);
        container.add(new Label("Loom Prefrence:"), 0, 2);
        container.add(new Label(objConfiguration.getIntEnds()+" x "+objConfiguration.getIntPixs()), 1, 2);
        container.add(new Label(objConfiguration.getStrClothType()), 2, 2);
        
        Label artworkWidth = new Label(objDictionaryAction.getWord("ARTWORKWIDTH")+" ("+objDictionaryAction.getWord("ENDS")+"):");
        container.add(artworkWidth, 0, 3);
        txtWidth = new TextField(){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        txtWidth.setText(Integer.toString((int)bufferedImage.getWidth()));
        container.add(txtWidth, 1, 3);
        
        Label artworkWidthHooks = new Label(objDictionaryAction.getWord("ARTWORKWIDTH")+" ("+objDictionaryAction.getWord("HOOKS")+"):");
        container.add(artworkWidthHooks, 0, 4);
        txtWidthHooks = new TextField();
        txtWidthHooks.setText(Integer.toString((int) Math.ceil(bufferedImage.getWidth()/objConfiguration.getIntTPD())));
        container.add(txtWidthHooks, 1, 4);
        
        Label artworkHeight = new Label(objDictionaryAction.getWord("ARTWORKLENGTH")+" ("+objDictionaryAction.getWord("PICKS")+"):");
        container.add(artworkHeight, 0, 5);
        txtHeight = new TextField(){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        txtHeight.setText(Integer.toString((int)bufferedImage.getHeight()));
        container.add(txtHeight, 1, 5);
        
        container.add(new Label("Pixels"), 2, 3);
        pixelIV = new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png");
        container.add(pixelIV, 2, 4);
        
        Label artworkWidthMesaure = new Label(objDictionaryAction.getWord("ARTWORKWIDTH")+":");
        container.add(artworkWidthMesaure, 0, 6);
        txtWidthMesaure = new TextField()/*{
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        }*/;
        txtWidthMesaure.setText(String.format("%.3f",bufferedImage.getWidth()/(float)objConfiguration.getIntEPI()));
        txtWidthMesaure.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!t1.matches(regExDouble))
                    txtWidthMesaure.setText(String.format("%.3f",bufferedImage.getWidth()/(float)objConfiguration.getIntEPI()));                
            }
        });
        container.add(txtWidthMesaure, 1, 6);
        
        Label artworkHeightMeasure = new Label(objDictionaryAction.getWord("ARTWORKLENGTH")+":");
        container.add(artworkHeightMeasure, 0, 7);
        txtHeightMesaure = new TextField()/*{
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        }*/;
        txtHeightMesaure.setText(String.format("%.3f",bufferedImage.getHeight()/(float)objConfiguration.getIntPPI())); 
        txtHeightMesaure.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!t1.matches(regExDouble))
                    txtHeightMesaure.setText(String.format("%.3f",bufferedImage.getHeight()/(float)objConfiguration.getIntPPI())); 
            }
        });
        container.add(txtHeightMesaure, 1, 7);
        
        container.add(new Label("Inches"), 2, 6);
        linearIV = new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_lock.png");
        container.add(linearIV, 2, 7);
        
        Label artworkWidthDensity = new Label(objDictionaryAction.getWord("DENSITYWIDTH")+" ("+objDictionaryAction.getWord("EPI")+"):");
        container.add(artworkWidthDensity, 0, 8);
        txtWidthDensity = new TextField(){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        txtWidthDensity.setText(Integer.toString(objConfiguration.getIntEPI()));
        container.add(txtWidthDensity, 1, 8);
        
        Label artworkWidthDensityHooks = new Label(objDictionaryAction.getWord("DENSITYWIDTH")+" ("+objDictionaryAction.getWord("HPI")+"):");
        container.add(artworkWidthDensityHooks, 0, 9);
        txtWidthDensityHooks = new TextField();
        txtWidthDensityHooks.setText(Integer.toString(objConfiguration.getIntHPI()));
        container.add(txtWidthDensityHooks, 1, 9);
        
        Label artworkHeightDensity = new Label(objDictionaryAction.getWord("DENSITYLENGTH")+" ("+objDictionaryAction.getWord("PPI")+"):");
        container.add(artworkHeightDensity, 0, 10);
        txtHeightDensity = new TextField(){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        txtHeightDensity.setText(Integer.toString(objConfiguration.getIntPPI()));
        container.add(txtHeightDensity, 1, 10);
        
        container.add(new Label("Threads per Inch"), 2, 8);
        densityIV = new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png");
        container.add(densityIV, 2, 9);
        
        txtWidthMesaure.setDisable(true);
        txtHeightMesaure.setDisable(true);
        txtWidthHooks.setDisable(true);
        txtWidthDensityHooks.setDisable(true);
        
        aspectRatioCB = new CheckBox(objDictionaryAction.getWord("ASPECTRATIO"));
        aspectRatioCB.setSelected(false);
        aspectRatioCB.selectedProperty().addListener(new ChangeListener<Boolean>() {
           public void changed(ObservableValue<? extends Boolean> ov, Boolean old_val, Boolean new_val) {
          }
        });
        container.add(aspectRatioCB, 1, 11);
        
        pixelIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(resizeLockValue==1){
                    resizeLockValue=2; 
                    pixelIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
                    linearIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_lock.png"));
                    densityIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
                    txtWidth.setDisable(false);
                    txtHeight.setDisable(false);
                    txtWidthMesaure.setDisable(true);
                    txtHeightMesaure.setDisable(true);
                    txtWidthDensity.setDisable(false);
                    txtHeightDensity.setDisable(false);
                }else{
                    resizeLockValue=1;
                    pixelIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_lock.png"));
                    linearIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
                    densityIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
                    txtWidth.setDisable(true);
                    txtHeight.setDisable(true);
                    txtWidthMesaure.setDisable(false);
                    txtHeightMesaure.setDisable(false);
                    txtWidthDensity.setDisable(false);
                    txtHeightDensity.setDisable(false);
                }
            }
        });
        
        linearIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(resizeLockValue==2){
                    resizeLockValue=1; 
                    pixelIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_lock.png"));
                    linearIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
                    densityIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
                    txtWidth.setDisable(true);
                    txtHeight.setDisable(true);
                    txtWidthMesaure.setDisable(false);
                    txtHeightMesaure.setDisable(false);
                    txtWidthDensity.setDisable(false);
                    txtHeightDensity.setDisable(false);
                }else{
                    resizeLockValue=2;
                    pixelIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
                    linearIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_lock.png"));
                    densityIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
                    txtWidth.setDisable(false);
                    txtHeight.setDisable(false);
                    txtWidthMesaure.setDisable(true);
                    txtHeightMesaure.setDisable(true);
                    txtWidthDensity.setDisable(false);
                    txtHeightDensity.setDisable(false);
                }
            }
        });
        
        densityIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(resizeLockValue==3){
                    resizeLockValue=2; 
                    pixelIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
                    linearIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_lock.png"));
                    densityIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
                    txtWidth.setDisable(false);
                    txtHeight.setDisable(false);
                    txtWidthMesaure.setDisable(true);
                    txtHeightMesaure.setDisable(true);
                    txtWidthDensity.setDisable(false);
                    txtHeightDensity.setDisable(false);
                }else{
                    resizeLockValue=3;
                    pixelIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
                    linearIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
                    densityIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_lock.png"));
                    txtWidth.setDisable(false);
                    txtHeight.setDisable(false);
                    txtWidthMesaure.setDisable(false);
                    txtHeightMesaure.setDisable(false);
                    txtWidthDensity.setDisable(true);
                    txtHeightDensity.setDisable(true);
                }
            }
        });
        
        txtWidth.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!txtWidth.isFocused())
                    return;
                try {
                    if(t1.trim().equals("") || Integer.parseInt(t1)<0){
                        lblStatus.setText(objDictionaryAction.getWord("BALNKINPUT"));
                        //txtHeight.setText(t1);
                    } else {
                        if(aspectRatioCB.isSelected()){
                            int intHeight = Integer.parseInt(txtWidth.getText())*bufferedImage.getHeight()/bufferedImage.getWidth();
                            if(intHeight<1){
                                lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
                                intHeight = 1;
                                txtWidth.setText(Integer.toString(intHeight*bufferedImage.getWidth()/bufferedImage.getHeight()));
                            }                            
                            txtHeight.setText(Integer.toString(intHeight));
                        }
                        txtWidthHooks.setText(Integer.toString((int)Math.ceil(Integer.parseInt(txtWidth.getText())/(double)objConfiguration.getIntTPD())));
                        if(resizeLockValue==2){
                            txtWidthDensity.setText(Integer.toString((int)(Integer.parseInt(txtWidth.getText())/Double.parseDouble(txtWidthMesaure.getText()))));
                            txtHeightDensity.setText(Integer.toString((int)(Integer.parseInt(txtHeight.getText())/Double.parseDouble(txtHeightMesaure.getText()))));
                            txtWidthDensityHooks.setText(Integer.toString((int)Math.ceil(Integer.parseInt(txtWidthDensity.getText())/(double)objConfiguration.getIntTPD())));
                            graphSize.setText(12+"x"+(int)((12*Integer.parseInt(txtHeightDensity.getText()))/Integer.parseInt(txtWidthDensity.getText())));
                        }else{
                            txtWidthMesaure.setText(String.format("%.3f",((float)Integer.parseInt(txtWidth.getText())/Integer.parseInt(txtWidthDensity.getText()))));
                            txtHeightMesaure.setText(String.format("%.3f",((float)Integer.parseInt(txtHeight.getText())/Integer.parseInt(txtHeightDensity.getText())))); 
                        }
                        threadIV.setImage(SwingFXUtils.toFXImage(getResizeImage(), null));
                    }
                } catch (Exception ex) {
                    new Logging("SEVERE",ArtworkView.class.getName(),"width property change",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });    
        txtHeight.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!txtHeight.isFocused())
                    return;
                try {
                    if(t1.trim().equals("") || Integer.parseInt(t1)<0){
                        lblStatus.setText(objDictionaryAction.getWord("BALNKINPUT"));
                        //txtWidth.setText(t1);
                    } else {
                        if(aspectRatioCB.isSelected()){
                            int intWidth = Integer.parseInt(txtHeight.getText())*bufferedImage.getWidth()/bufferedImage.getHeight();
                            if(intWidth<1){
                                lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
                                intWidth = 1;
                                txtHeight.setText(Integer.toString(intWidth*bufferedImage.getHeight()/bufferedImage.getWidth()));
                            }                            
                            txtWidth.setText(Integer.toString(intWidth));
                        }
                        txtWidthHooks.setText(Integer.toString((int)Math.ceil(Integer.parseInt(txtWidth.getText())/(double)objConfiguration.getIntTPD())));
                        if(resizeLockValue==2){
                            txtWidthDensity.setText(Integer.toString((int)(Integer.parseInt(txtWidth.getText())/Double.parseDouble(txtWidthMesaure.getText()))));
                            txtHeightDensity.setText(Integer.toString((int)(Integer.parseInt(txtHeight.getText())/Double.parseDouble(txtHeightMesaure.getText()))));
                            txtWidthDensityHooks.setText(Integer.toString((int)Math.ceil(Integer.parseInt(txtWidthDensity.getText())/(double)objConfiguration.getIntTPD())));
                            graphSize.setText(12+"x"+(int)((12*Integer.parseInt(txtHeightDensity.getText()))/Integer.parseInt(txtWidthDensity.getText())));
                        }else{
                            txtWidthMesaure.setText(String.format("%.3f",((float)Integer.parseInt(txtWidth.getText())/Integer.parseInt(txtWidthDensity.getText()))));
                            txtHeightMesaure.setText(String.format("%.3f",((float)Integer.parseInt(txtHeight.getText())/Integer.parseInt(txtHeightDensity.getText()))));
                        }
                        threadIV.setImage(SwingFXUtils.toFXImage(getResizeImage(), null));
                    }
                } catch (Exception ex) {
                    new Logging("SEVERE",ArtworkView.class.getName(),"height property change",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });        

        txtWidthMesaure.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!txtWidthMesaure.isFocused())
                    return;
                try {
                    if(t1.trim().equals("") || Double.parseDouble(t1)<0){
                        lblStatus.setText(objDictionaryAction.getWord("BALNKINPUT"));
                        //txtHeight.setText(t1);
                    } else{
                        if(aspectRatioCB.isSelected()){
                            double dblHeight = Double.parseDouble(txtWidthMesaure.getText())*bufferedImage.getHeight()/bufferedImage.getWidth();
                            if(dblHeight<0){
                                lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
                                dblHeight = 1;
                                txtWidthMesaure.setText(Double.toString(dblHeight*bufferedImage.getWidth()/bufferedImage.getHeight()));
                            }                            
                            txtHeightMesaure.setText(Double.toString(dblHeight));
                        }
                        if(resizeLockValue==1){
                            txtWidthDensity.setText(Integer.toString((int)(Integer.parseInt(txtWidth.getText())/Double.parseDouble(txtWidthMesaure.getText()))));
                            txtHeightDensity.setText(Integer.toString((int)(Integer.parseInt(txtHeight.getText())/Double.parseDouble(txtHeightMesaure.getText()))));
                            txtWidthDensityHooks.setText(Integer.toString((int)Math.ceil(Integer.parseInt(txtWidthDensity.getText())/(double)objConfiguration.getIntTPD())));
                            graphSize.setText(12+"x"+(int)((12*Integer.parseInt(txtHeightDensity.getText()))/Integer.parseInt(txtWidthDensity.getText())));
                        }else{
                            txtWidth.setText(Integer.toString((int)(Double.parseDouble(txtWidthMesaure.getText())*Integer.parseInt(txtWidthDensity.getText()))));
                            txtHeight.setText(Integer.toString((int)(Double.parseDouble(txtHeightMesaure.getText())*Integer.parseInt(txtHeightDensity.getText()))));
                            txtWidthHooks.setText(Integer.toString((int)Math.ceil(Integer.parseInt(txtWidth.getText())/(double)objConfiguration.getIntTPD())));
                        }
                        threadIV.setImage(SwingFXUtils.toFXImage(getResizeImage(), null));
                    }
                } catch (Exception ex) {
                    new Logging("SEVERE",ArtworkView.class.getName(),"width property change",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        txtHeightMesaure.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!txtHeightMesaure.isFocused())
                    return;
                try {
                    if(t1.trim().equals("") || Double.parseDouble(t1)<0){
                        lblStatus.setText(objDictionaryAction.getWord("BALNKINPUT"));
                        //txtWidth.setText(t1);
                    } else {
                        if(aspectRatioCB.isSelected()){
                            double dblWidth = Double.parseDouble(txtHeightMesaure.getText())*bufferedImage.getWidth()/bufferedImage.getHeight();
                            if(dblWidth<0){
                                lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
                                dblWidth = 1;
                                txtHeightMesaure.setText(Double.toString(dblWidth*bufferedImage.getHeight()/bufferedImage.getWidth()));
                            }                            
                            txtWidthMesaure.setText(Double.toString(dblWidth));
                        }
                        if(resizeLockValue==1){
                            txtWidthDensity.setText(Integer.toString((int)(Integer.parseInt(txtWidth.getText())/Double.parseDouble(txtWidthMesaure.getText()))));
                            txtHeightDensity.setText(Integer.toString((int)(Integer.parseInt(txtHeight.getText())/Double.parseDouble(txtHeightMesaure.getText()))));
                            txtWidthDensityHooks.setText(Integer.toString((int)Math.ceil(Integer.parseInt(txtWidthDensity.getText())/(double)objConfiguration.getIntTPD())));
                            graphSize.setText(12+"x"+(int)((12*Integer.parseInt(txtHeightDensity.getText()))/Integer.parseInt(txtWidthDensity.getText())));
                        }else{
                            txtWidth.setText(Integer.toString((int)(Double.parseDouble(txtWidthMesaure.getText())*Integer.parseInt(txtWidthDensity.getText()))));
                            txtHeight.setText(Integer.toString((int)(Double.parseDouble(txtHeightMesaure.getText())*Integer.parseInt(txtHeightDensity.getText()))));
                            txtWidthHooks.setText(Integer.toString((int)Math.ceil(Integer.parseInt(txtWidth.getText())/(double)objConfiguration.getIntTPD())));
                        }
                        threadIV.setImage(SwingFXUtils.toFXImage(getResizeImage(), null));
                    }
                } catch (Exception ex) {
                    new Logging("SEVERE",ArtworkView.class.getName(),"height property change",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });        
        
        txtWidthDensity.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!txtWidthDensity.isFocused())
                    return;
                try {
                    if(t1.trim().equals("") || Integer.parseInt(t1)<0){
                        lblStatus.setText(objDictionaryAction.getWord("BALNKINPUT"));
                        //txtHeight.setText(t1);
                    } else {
                        if(aspectRatioCB.isSelected()){
                            int intHeight = Integer.parseInt(txtWidthDensity.getText())*bufferedImage.getHeight()/bufferedImage.getWidth();
                            if(intHeight<1){
                                lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
                                intHeight = 1;
                                txtWidthDensity.setText(Integer.toString(intHeight*bufferedImage.getWidth()/bufferedImage.getHeight()));
                            }                            
                            txtHeightDensity.setText(Integer.toString(intHeight));
                        }
                        txtWidthDensityHooks.setText(Integer.toString((int)Math.ceil(Integer.parseInt(txtWidthDensity.getText())/(double)objConfiguration.getIntTPD())));
                        graphSize.setText(12+"x"+(int)((12*Integer.parseInt(txtHeightDensity.getText()))/Integer.parseInt(txtWidthDensity.getText())));
                        if(resizeLockValue==1){
                            txtWidthMesaure.setText(String.format("%.3f",((float)Integer.parseInt(txtWidth.getText())/Integer.parseInt(txtWidthDensity.getText()))));
                            txtHeightMesaure.setText(String.format("%.3f",((float)Integer.parseInt(txtHeight.getText())/Integer.parseInt(txtHeightDensity.getText())))); 
                        }else{
                            txtWidth.setText(Integer.toString((int)(Double.parseDouble(txtWidthMesaure.getText())*Integer.parseInt(txtWidthDensity.getText()))));
                            txtHeight.setText(Integer.toString((int)(Double.parseDouble(txtHeightMesaure.getText())*Integer.parseInt(txtHeightDensity.getText()))));
                            txtWidthHooks.setText(Integer.toString((int)Math.ceil(Integer.parseInt(txtWidth.getText())/(double)objConfiguration.getIntTPD())));
                        }
                        threadIV.setImage(SwingFXUtils.toFXImage(getResizeImage(), null));
                    }
                } catch (Exception ex) {
                    new Logging("SEVERE",ArtworkView.class.getName(),"width property change",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });    
        txtHeightDensity.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!txtHeightDensity.isFocused())
                    return;
                try {
                    if(t1.equals("") && Integer.parseInt(t1)<0){
                        lblStatus.setText(objDictionaryAction.getWord("BALNKINPUT"));
                        //txtWidth.setText(t1);
                    } else {
                        if(aspectRatioCB.isSelected()){
                            int intWidth = Integer.parseInt(txtHeightDensity.getText())*bufferedImage.getWidth()/bufferedImage.getHeight();
                            if(intWidth<1){
                                lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
                                intWidth = 1;
                                txtHeightDensity.setText(Integer.toString(intWidth*bufferedImage.getHeight()/bufferedImage.getWidth()));
                            }                            
                            txtWidthDensity.setText(Integer.toString(intWidth));
                        }
                        txtWidthDensityHooks.setText(Integer.toString((int)Math.ceil(Integer.parseInt(txtWidthDensity.getText())/(double)objConfiguration.getIntTPD())));
                        graphSize.setText(12+"x"+(int)((12*Integer.parseInt(txtHeightDensity.getText()))/Integer.parseInt(txtWidthDensity.getText())));
                        if(resizeLockValue==1){
                            txtWidthMesaure.setText(String.format("%.3f",((float)Integer.parseInt(txtWidth.getText())/Integer.parseInt(txtWidthDensity.getText()))));
                            txtHeightMesaure.setText(String.format("%.3f",((float)Integer.parseInt(txtHeight.getText())/Integer.parseInt(txtHeightDensity.getText())))); 
                        }else{
                            txtWidth.setText(Integer.toString((int)(Double.parseDouble(txtWidthMesaure.getText())*Integer.parseInt(txtWidthDensity.getText()))));
                            txtHeight.setText(Integer.toString((int)(Double.parseDouble(txtHeightMesaure.getText())*Integer.parseInt(txtHeightDensity.getText()))));
                            txtWidthHooks.setText(Integer.toString((int)Math.ceil(Integer.parseInt(txtWidth.getText())/(double)objConfiguration.getIntTPD())));
                        }
                        threadIV.setImage(SwingFXUtils.toFXImage(getResizeImage(), null));
                    }
                } catch (Exception ex) {
                    new Logging("SEVERE",ArtworkView.class.getName(),"height property change",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        
        final Separator sepHor1 = new Separator();
        sepHor1.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHor1, 0, 12);
        GridPane.setColumnSpan(sepHor1, 3);
        container.getChildren().add(sepHor1);
        
        btnApply = new Button(objDictionaryAction.getWord("APPLY"));
        btnReset = new Button(objDictionaryAction.getWord("RESET"));
        btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));

        //btnApply.setMaxWidth(Double.MAX_VALUE);
        //btnReset.setMaxWidth(Double.MAX_VALUE);
        //btnCancel.setMaxWidth(Double.MAX_VALUE);        

        btnApply.setGraphic(new ImageView(objArtwork.getObjConfiguration().getStrColour()+"/"+objArtwork.getObjConfiguration().strIconResolution+"/save.png"));
        btnReset.setGraphic(new ImageView(objArtwork.getObjConfiguration().getStrColour()+"/"+objArtwork.getObjConfiguration().strIconResolution+"/clear.png"));
        btnCancel.setGraphic(new ImageView(objArtwork.getObjConfiguration().getStrColour()+"/"+objArtwork.getObjConfiguration().strIconResolution+"/close.png"));
        
        btnApply.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPAPPLY")));
        btnReset.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPRESET")));
        btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
        
        btnCancel.setDefaultButton(true);
        //btnApply.setDisable(false);
        //btnReset.setDisable(false);
        
        btnApply.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                try {
                    int width = Integer.parseInt(txtWidth.getText());
                    int height = Integer.parseInt(txtHeight.getText());
                    //bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
                    BufferedImage bufferedImageesize = new BufferedImage(width, height,BufferedImage.TYPE_INT_RGB);
                    Graphics2D g = bufferedImageesize.createGraphics();
                    g.drawImage(bufferedImage, 0, 0, width, height, null);
                    g.dispose();
                    bufferedImage = bufferedImageesize;
                    bufferedImageesize = null;
                    
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    ImageIO.write(bufferedImage, "png", baos);
                    baos.flush();
                    byte[] imageInByte = baos.toByteArray();
                    objArtwork.setBytArtworkThumbnil(imageInByte);
                    imageInByte = null;
                    baos.close();
                    
                    objConfiguration.setIntPPI(Integer.parseInt(txtHeightDensity.getText()));
                    objConfiguration.setIntEPI(Integer.parseInt(txtWidthDensity.getText()));
                    objConfiguration.setIntHPI(Integer.parseInt(txtWidthDensityHooks.getText()));
                    objConfiguration.setStrGraphSize(12+"x"+(int)((12*objConfiguration.getIntPPI())/objConfiguration.getIntEPI()));
                    lblStatus.setText(objDictionaryAction.getWord("SUCCESS"));
                    densityStage.close();
                    System.gc();
                } catch (IOException ex) {
                    Logger.getLogger(ArtworkDensityView.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        btnReset.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                lblStatus.setText(objDictionaryAction.getWord("ACTIONRESET"));
                txtWidth.setText(Integer.toString(bufferedImage.getWidth()));
                txtHeight.setText(Integer.toString(bufferedImage.getHeight()));
                txtWidthHooks.setText(Integer.toString((int)Math.ceil(bufferedImage.getWidth()/(double)objConfiguration.getIntTPD())));
                txtWidthDensity.setText(Integer.toString(objConfiguration.getIntEPI()));
                txtHeightDensity.setText(Integer.toString(objConfiguration.getIntPPI()));
                txtWidthDensityHooks.setText(Integer.toString(objConfiguration.getIntHPI()));
                txtWidthMesaure.setText(String.format("%.3f",((double)Integer.parseInt(txtWidth.getText())/Integer.parseInt(txtWidthDensity.getText()))));
                txtHeightMesaure.setText(String.format("%.3f",((double)Integer.parseInt(txtHeight.getText())/Integer.parseInt(txtHeightDensity.getText())))); 
                graphSize.setText(12+"x"+(int)((12*objConfiguration.getIntPPI())/objConfiguration.getIntEPI()));
            }
        });
        btnCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                densityStage.close();
            }
        });
        
        container.add(btnApply, 0, 13); 
        container.add(btnReset, 1, 13);
        container.add(btnCancel, 2, 13);
        
        threadIV = new ImageView(objArtwork.getObjConfiguration().getStrColour()+"/"+objArtwork.getObjConfiguration().strIconResolution+"/help.png");
        //threadIV.setFitWidth(111);
        //threadIV.setFitHeight(333);
        
        threadIV.setImage(SwingFXUtils.toFXImage(getResizeImage(), null));
        //container.add(threadIV, 0, 14, 3, 1);
        lblStatus.setText("Most monitors display images at approximately 75DPI");
        
        root.setCenter(container);
        //root.setRight(threadIV);
        root.setBottom(footContainer);
        densityStage = new Stage(); 
        densityStage.setScene(scene);
        densityStage.initModality(Modality.APPLICATION_MODAL);//WINDOW_MODAL
        densityStage.initStyle(StageStyle.UTILITY); 
        densityStage.getIcons().add(new Image("/media/icon.png"));
        densityStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWDENSITY")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
        densityStage.setIconified(false);
        densityStage.setResizable(false);
        densityStage.showAndWait();
    }
    
    public BufferedImage getResizeImage(){
        int intWidth = Integer.parseInt(txtWidth.getText());
        int intLength = Integer.parseInt(txtHeight.getText());
            
        int dpi = objConfiguration.getIntDPI();
        
        BufferedImage bufferedImageResize = new BufferedImage(intLength, intWidth,BufferedImage.TYPE_INT_RGB);
        Graphics2D g = bufferedImageResize.createGraphics();
        g.drawImage(bufferedImage, 0, 0, intLength, intWidth, null);
        g.dispose();
        if(objConfiguration.getBlnMRepeat()){
            try {
                ArtworkAction objArtworkAction = new ArtworkAction();
                bufferedImageResize = objArtworkAction.getImageRepeat(bufferedImageResize, ((int)(333/intLength)>0)?(int)(333/intLength):1, ((int)(111/intWidth)>0)?(int)(111/intWidth):1);
            } catch (SQLException ex) {
                new Logging("SEVERE",ArtworkDensityView.class.getName(),ex.toString(),ex);
                //lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        }
        System.gc();
        return bufferedImageResize;
     }
    
    public void start(Stage stage) throws Exception {
        //stage.initOwner(WindowView.windowStage);
        new ArtworkDensityView(stage);        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    } 
}

