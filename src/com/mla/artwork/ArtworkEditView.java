/*
 * Copyright (C) 2019 Digital India Corporation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mla.artwork;

import com.mla.dictionary.DictionaryAction;
import com.mla.main.Configuration;
import com.mla.main.EncryptZip;
import com.mla.main.Logging;
import com.mla.main.MessageView;
import com.mla.print.PrintView;
import com.mla.utility.Device;
import com.mla.utility.UtilityAction;
import com.sun.media.jai.codec.ByteArraySeekableStream;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.SeekableStream;
import java.awt.BasicStroke;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javax.imageio.ImageIO;
import javax.media.jai.PlanarImage;
/**
 *
 * @Designing GUI window for user preferences
 * @author Amit Kumar Singh
 * 
 */
public class ArtworkEditView extends Application {
    private static Stage artworkStage;
    private BorderPane root;
    private ImageView artwork = new ImageView();         
    private ImageView artworkPreView = new ImageView(); 
    private Label lblStatus;
    private Label lblX = new Label("X:0");
    private Label lblY = new Label("Y:0");
    private Button btnSelect;
    private Button btnUnSelect;
    private Button btnMove;
    private Button btnDelete;
    
    private ProgressBar progressB;
    private ProgressIndicator progressI;
    private ScrollPane container;
    
    Configuration objConfiguration = null;
    DictionaryAction objDictionaryAction = null;
    
    Artwork objArtwork;
    private Graphics2D objEditGraphics2D;
    private BufferedImage objEditBufferedImage = null;
    private BufferedImage motif = null;
    
    private int imageX=0;
    private int imageY=0;
    private int HEIGHT=1000;
    private int WIDTH=1000;
    private int[] data = {1,1};
    private float zoomFactor = 1;
    double startX, startY, stopX, stopY;
    byte plotViewActionMode = 1; //1= composite, 2 = front, 3 = rear
    String filePath;
    int dataIndex;
    boolean isSelected = false;
    private boolean selectdata = true, noselectdata = false, dragselectdata = false;
    List<String[]> lstDatas;
    
    public ArtworkEditView(final Stage primaryStage) {}
    
    public ArtworkEditView(Configuration objConfigurationCall) {
        objConfiguration = objConfigurationCall;
        objDictionaryAction = new DictionaryAction(objConfiguration);

        artworkStage = new Stage(); 
        root = new BorderPane();
        Scene scene = new Scene(root, objConfiguration.WIDTH, objConfiguration.HEIGHT, Color.WHITE);
        scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        resolutionControl(scene);
        
        lstDatas = new ArrayList<String[]>();
        
        int initWidth=(int)(256*12*zoomFactor); //objConfiguration.getIntEnds()
        int initHeight=(int)(48*12*zoomFactor); //objConfiguration.getIntPixs()
        motif = new BufferedImage(initWidth, initHeight, BufferedImage.TYPE_INT_ARGB);
        Graphics2D objGraphics2D = motif.createGraphics();
        objGraphics2D.drawImage(motif, 0, 0, initWidth, initHeight, null);
        objGraphics2D.setColor(java.awt.Color.WHITE);
        objGraphics2D.fillRect(0, 0, initWidth, initHeight);
        objGraphics2D.dispose();
        
        HBox footContainer = new HBox();
        progressB = new ProgressBar(0);
        progressB.setVisible(false);
        progressI = new ProgressIndicator(0);
        progressI.setVisible(false);
        lblStatus = new Label(objDictionaryAction.getWord("WELCOMETOCADTOOL"));
        lblStatus.setId("message");
        footContainer.getChildren().addAll(lblStatus,progressB,progressI);
        footContainer.setId("footContainer");
        root.setBottom(footContainer);        
        
        HBox topContainer = new HBox(); 
        
        ToolBar toolBar = new ToolBar();
        //toolBar.setMinHeight(45);
        FlowPane flow = new FlowPane();  
        flow.setPrefWrapLength(objConfiguration.WIDTH-10);
        flow.getStyleClass().addAll("flow");
        //Add the Buttons to the ToolBar.
        flow.setAlignment(Pos.TOP_LEFT);
        toolBar.getItems().addAll(flow);
        
        MenuButton menuSelected = new MenuButton();
        menuSelected.setGraphic(new ImageView("/media/text_menu.png"));
        menuSelected.setAlignment(Pos.CENTER_RIGHT);
        menuSelected.setPickOnBounds(false);
        menuSelected.setStyle("-fx-background-color: rgba(0,0,0,0);"); 
        
        AnchorPane toolAnchor = new AnchorPane();
        toolAnchor.getChildren().addAll(menuSelected);
        AnchorPane.setRightAnchor(menuSelected, 0.0);
        toolAnchor.setPickOnBounds(false);
        
        StackPane toolPane=new StackPane();
        toolPane.getChildren().addAll(toolBar,toolAnchor);
        
        topContainer.getChildren().add(toolPane); 
        topContainer.setId("topContainer");
        root.setTop(topContainer);
        
        // New file item
        Button newFileBtn = new Button();
        newFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/new.png"));
        newFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("NEWFILE")+" (Ctrl+N)\n"+objDictionaryAction.getWord("TOOLTIPNEWFILE")));
        newFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        newFileBtn.getStyleClass().add("toolbar-button");    
        // import file item
        Button importFileBtn = new Button();        
        importFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/import.png"));
        importFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("IMPORTFILE")+" (Ctrl+I)\n"+objDictionaryAction.getWord("TOOLTIPIMPORTFILE")));
        importFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        importFileBtn.getStyleClass().addAll("toolbar-button");
        // Device file item
        Button applicationFileBtn = new Button();        
        applicationFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/application_integration.png"));
        applicationFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("PAINTAPPLICATION")+" (Ctrl+Shift+N)\n"+objDictionaryAction.getWord("TOOLTIPPAINTAPPLICATION")));
        applicationFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        applicationFileBtn.getStyleClass().addAll("toolbar-button");
        // Refresh file item
        Button refreshFileBtn = new Button();        
        refreshFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/refresh.png"));
        refreshFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("IMPORTREFRESH")+" (Ctrl+Shift+I)\n"+objDictionaryAction.getWord("TOOLTIPIMPORTREFRESH")));
        refreshFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        refreshFileBtn.getStyleClass().addAll("toolbar-button");
        // Zoom-In item
        Button zoomInViewBtn = new Button();
        zoomInViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
        zoomInViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ZOOMINVIEW")+" (Ctrl+Plus)\n"+objDictionaryAction.getWord("TOOLTIPZOOMINVIEW")));
        zoomInViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        zoomInViewBtn.getStyleClass().add("toolbar-button");    
        // Normal item
        Button normalViewBtn = new Button();
        normalViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
        normalViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ZOOMNORMALVIEW")+" (Ctrl+Enter)\n"+objDictionaryAction.getWord("TOOLTIPZOOMNORMALVIEW")));
        normalViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        normalViewBtn.getStyleClass().add("toolbar-button");    
        // Zoom-Out item
        Button zoomOutViewBtn = new Button();
        zoomOutViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
        zoomOutViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ZOOMOUTVIEW")+" (Ctrl+Minus)\n"+objDictionaryAction.getWord("TOOLTIPZOOMOUTVIEW")));
        zoomOutViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        zoomOutViewBtn.getStyleClass().add("toolbar-button");    
        // Save Texture file item
        Button saveTextureFileBtn = new Button();        
        saveTextureFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_texture_as.png"));
        saveTextureFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SAVETEXTUREFILE")+" (Ctrl+Shift+E)\n"+objDictionaryAction.getWord("TOOLTIPSAVETEXTUREFILE")));
        saveTextureFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        saveTextureFileBtn.getStyleClass().addAll("toolbar-button");
        // print File menu
        Button printFileBtn = new Button();        
        printFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/print.png"));
        printFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("PRINTFILE")+" (Ctrl+P)\n"+objDictionaryAction.getWord("TOOLTIPPRINTFILE")));
        printFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        printFileBtn.getStyleClass().addAll("toolbar-button");
        // print File menu
        Button closeBtn = new Button();        
        closeBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        closeBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CLOSE")+" (Esc)\n"+objDictionaryAction.getWord("TOOLTIPCLOSE")));
        closeBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        closeBtn.getStyleClass().addAll("toolbar-button");
        
        MenuItem newFileMI = new MenuItem(objDictionaryAction.getWord("NEWFILE"));
        MenuItem importFileMI = new MenuItem(objDictionaryAction.getWord("IMPORTFILE"));
        MenuItem applicationFileMI = new MenuItem(objDictionaryAction.getWord("PAINTAPPLICATION"));
        MenuItem refreshFileMI = new MenuItem(objDictionaryAction.getWord("IMPORTREFRESH"));
        MenuItem zoomInViewMI = new MenuItem(objDictionaryAction.getWord("ZOOMINVIEW"));
        MenuItem normalViewMI = new MenuItem(objDictionaryAction.getWord("ZOOMNORMALVIEW"));
        MenuItem zoomOutViewMI = new MenuItem(objDictionaryAction.getWord("ZOOMOUTVIEW"));
        MenuItem saveTextureFileMI = new MenuItem(objDictionaryAction.getWord("SAVETEXTUREFILE"));
        MenuItem printFileMI = new MenuItem(objDictionaryAction.getWord("PRINTFILE"));
        MenuItem closeMI = new MenuItem(objDictionaryAction.getWord("CLOSE"));
        
        newFileMI.setAccelerator(new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN));
        importFileMI.setAccelerator(new KeyCodeCombination(KeyCode.I, KeyCombination.CONTROL_DOWN)); 
        applicationFileMI.setAccelerator(new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        refreshFileMI.setAccelerator(new KeyCodeCombination(KeyCode.I, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        zoomInViewMI.setAccelerator(new KeyCodeCombination(KeyCode.PLUS, KeyCombination.CONTROL_DOWN));
        normalViewMI.setAccelerator(new KeyCodeCombination(KeyCode.ENTER, KeyCombination.CONTROL_DOWN));
        zoomOutViewMI.setAccelerator(new KeyCodeCombination(KeyCode.MINUS, KeyCombination.CONTROL_DOWN));
        saveTextureFileMI.setAccelerator(new KeyCodeCombination(KeyCode.E, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        printFileMI.setAccelerator(new KeyCodeCombination(KeyCode.P, KeyCombination.CONTROL_DOWN));
        closeMI.setAccelerator(new KeyCodeCombination(KeyCode.ESCAPE));
        
        newFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/new.png"));
        importFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/import.png"));
        applicationFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/application_integration.png"));
        refreshFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/refresh.png"));
        zoomInViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
        normalViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
        zoomOutViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
        saveTextureFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_texture_as.png"));
        printFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/print.png"));
        closeMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
       
        flow.getChildren().addAll(newFileBtn,importFileBtn,applicationFileBtn,refreshFileBtn,zoomInViewBtn,normalViewBtn,zoomOutViewBtn,saveTextureFileBtn,printFileBtn,closeBtn);
        menuSelected.getItems().addAll(newFileMI,importFileMI,applicationFileMI,refreshFileMI,zoomInViewMI,normalViewMI,zoomOutViewMI,saveTextureFileMI,printFileMI,closeMI);
    
        newFileBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                newFileAction();
            }
        });
        newFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                newFileAction();
            }
        });
        importFileBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                importFileAction();
            }
        });
        importFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                importFileAction();
            }
        });
        applicationFileBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                applicationFileAction();
            }
        });
        applicationFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                applicationFileAction();
            }
        });
        refreshFileBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                refreshFileAction();
            }
        });
        refreshFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                refreshFileAction();
            }
        });
        normalViewBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                zoomNormalAction();
            }
        });
        normalViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                zoomNormalAction();
            }
        });
        zoomInViewBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                zoomInAction();
            }
        });
        zoomInViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                zoomInAction();
            }
        });
        zoomOutViewBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                zoomOutAction();
            }
        });
        zoomOutViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                zoomOutAction();
            }
        });
	saveTextureFileBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                saveTextureFileAction();
            }
        });
        saveTextureFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                saveTextureFileAction();
            }
        });
	printFileBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                printAction();
            }
        });
        printFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                printAction();
            }
        });
        closeBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                closeAction();
            }
        });
        closeMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
               closeAction();
            }
        });
        
        
        
       //adding preview box  
        artworkPreView.setFitWidth(100);
        artworkPreView.setPreserveRatio(true);  
        
        VBox previewPane = new VBox();
        //previewPane.setMaxSize(100, objConfiguration.HEIGHT);
        previewPane.setMaxWidth(100);
        //previewPane.setId("popup");
        previewPane.setStyle("-fx-background-color:#FFEEDD;");
        previewPane.getChildren().add(artworkPreView);
        previewPane.getChildren().add(lblX);
        previewPane.getChildren().add(lblY);
        addButtonAction();
        previewPane.getChildren().add(btnSelect);
        previewPane.getChildren().add(btnUnSelect);
        previewPane.getChildren().add(btnMove);
        previewPane.getChildren().add(btnDelete);
        
        root.setRight(previewPane);
        
        //create scollpane conatiner
        container = new ScrollPane();
        //add scroll Pane
        addScrollPaneContainer();
        addScrollPaneContainer((float)0.0);
        //artworkIV.setPrefSize(objConfiguration.WIDTH, objConfiguration.HEIGHT);
        artwork.setId("container");
        //container.setContent(artwork);
        root.setCenter(container);
                
        //setting Context menu and events
        //addContextMenu();
        // Code Added for ShortCuts
        addAccelratorKey(scene);
        //Manage Events
        addArtworkEventHandler();
        
        artworkStage.getIcons().add(new Image("/media/icon.png"));
        artworkStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWSIMULATION")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
        //artworkStage.setIconified(true);
        artworkStage.setResizable(false);
        artworkStage.setScene(scene);
        artworkStage.setX(0);
        artworkStage.setY(0);
        artworkStage.show();
    }
    
/**
* resolutionControl(Scene scene)
* <p>
* This function is used for reassign width and height. 
*  
* @param       Stage primaryStage
* @author      Amit Kumar Singh
* @version     %I%, %G%
* @since       1.0
* @see         javafx.stage.*;
* @link        WindowView
*/
    private void resolutionControl(Scene scene){
        //windowStage.resizableProperty().addListener(listener);
        scene.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                setGlobalWidthHeight();
                artworkStage.setHeight(objConfiguration.HEIGHT);
                artworkStage.setWidth(objConfiguration.WIDTH);
                //selectedMenu = "FILE";
            }
        });
        scene.heightProperty().addListener(new ChangeListener<Number>() {
            @Override 
            public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneHeight, Number newSceneHeight) {
                setGlobalWidthHeight();
                artworkStage.setHeight(objConfiguration.HEIGHT);
                artworkStage.setWidth(objConfiguration.WIDTH);
                //selectedMenu = "FILE";
                
            }
        });
        setGlobalWidthHeight();
    }
    private void setGlobalWidthHeight(){
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        objConfiguration.WIDTH = screenSize.getWidth();
        objConfiguration.HEIGHT = screenSize.getHeight();
        objConfiguration.strIconResolution = (objConfiguration.WIDTH<1280)?"hd_none":(objConfiguration.WIDTH<1920)?"hd":(objConfiguration.WIDTH<2560)?"hd_full":"hd_quad";
        WIDTH = (int)objConfiguration.WIDTH-100;
        HEIGHT = (int)objConfiguration.HEIGHT;
    }
    private void addButtonAction(){        
        btnSelect = new Button();
        btnUnSelect = new Button();
        btnMove = new Button();
        btnDelete = new Button();  
        
        btnSelect.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/select.png"));
        btnUnSelect.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/crop.png"));
        btnMove.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move.png"));
        btnDelete.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/delete.png"));
        
        btnSelect.setTooltip(new Tooltip(objDictionaryAction.getWord("SELECT")));
        btnUnSelect.setTooltip(new Tooltip(objDictionaryAction.getWord("UNSELECT")));
        btnMove.setTooltip(new Tooltip(objDictionaryAction.getWord("MOVE")));
        btnDelete.setTooltip(new Tooltip(objDictionaryAction.getWord("DELETE")));
        
        btnSelect.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        btnUnSelect.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        btnMove.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        btnDelete.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
              
        btnSelect.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    setCropImage();
                    setCurrentShape();
                    selectdata = true; 
                    isSelected = false;
                    isSelectedButton();
                } catch (Exception ex) {
                     Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                }
            }
        }); 
        btnUnSelect.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    UnSelectDataAction();
                } catch (Exception ex) {
                     Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                }
            }
        }); 
        btnMove.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    setCurrentShape();
                    dragselectdata = true;
                } catch (Exception ex) {
                     Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                }
            }
        });  
        btnDelete.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    deleteDataAction();
                } catch (Exception ex) {
                     Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                }
            }
        }); 
        isSelectedButton();
    }
    private void isSelectedButton(){
        btnUnSelect.setDisable(!isSelected);
        btnMove.setDisable(!isSelected);
        btnDelete.setDisable(!isSelected);
    }
    private void UnSelectDataAction(){
        //System.err.println("unselect");
        setCropImage();
        setCurrentShape();
        noselectdata = true;
        isSelected = false;
        isSelectedButton();
    }
    private void deleteDataAction(){
        lstDatas.remove(dataIndex);
        //reset image
        prepareImage();
        setCropImage();
        setCurrentShape();
        noselectdata = true;
        isSelected = false;
        isSelectedButton();
    }
    /**
     * addContextMenu
     * <p>
     * Function use for drawing menu bar for menu item,
     * and binding events for each menus with style. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void addContextMenu(){
        final ContextMenu contextMenu = new ContextMenu();
        MenuItem select = new MenuItem(objDictionaryAction.getWord("SELECT"));
        select.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/select.png"));
        MenuItem unselect = new MenuItem(objDictionaryAction.getWord("UNSELECT"));
        unselect.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/crop.png"));
        MenuItem move = new MenuItem(objDictionaryAction.getWord("MOVE"));
        move.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move.png"));
        MenuItem delete = new MenuItem(objDictionaryAction.getWord("DELETE"));
        delete.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/delete.png"));
        MenuItem undo = new MenuItem(objDictionaryAction.getWord("UNDO"));
        undo.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/undo.png"));
        MenuItem redo = new MenuItem(objDictionaryAction.getWord("REDO"));
        redo.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/redo.png"));
        
        unselect.setDisable(!isSelected);
        move.setDisable(!isSelected);
        delete.setDisable(!isSelected);
        
        contextMenu.getItems().addAll(select, unselect, new SeparatorMenuItem(), move, delete, new SeparatorMenuItem(), undo, redo);
        select.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                event.consume();
                setCurrentShape();
                selectdata = true;
                setCropImage();
            }
        });
        unselect.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                event.consume();
                setCropImage();
                setCurrentShape();
                noselectdata = true;
                isSelected = false;
            }
        });
        move.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                event.consume();
                setCurrentShape();
                dragselectdata = true;
            }
        });
        delete.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                event.consume();
            }
        });
        undo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                event.consume();
                
            }
        });
        redo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                event.consume();
                
            }
        });
        /*contextMenu.setOnShowing(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent e) {
                System.out.println("showing");
            }
        });
        contextMenu.setOnShown(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent e) {
                System.out.println("shown");
            }
        });
        contextMenu.setOnHiding(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent e) {
                System.out.println("hiding");
            }
        });
        contextMenu.setOnHidden(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent e) {
                System.out.println("hidden");
            }
        });*/
        container.setContextMenu(contextMenu);
        //contextMenu.hide();
        container.setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {
            @Override
            public void handle(ContextMenuEvent event) {
                if(!isSelected){
                    contextMenu.hide();
                    event.consume();
                    //System.err.println(editThreadPrimaryValue+"=isPrimaryButtonDown"+editThreadSecondaryValue);
                }
            }
        });
    }
    /**
     * addAccelratorKey
     * <p>
     * Function use for adding shortcut key combinations. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void addAccelratorKey(Scene scene){
        final KeyCodeCombination newFile = new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN);
        final KeyCodeCombination importFile = new KeyCodeCombination(KeyCode.I, KeyCombination.CONTROL_DOWN); 
        final KeyCodeCombination applicationFile = new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN);
        final KeyCodeCombination refreshFile = new KeyCodeCombination(KeyCode.I, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN);
        final KeyCodeCombination zoomInView = new KeyCodeCombination(KeyCode.PLUS, KeyCombination.CONTROL_DOWN);
        final KeyCodeCombination normalView = new KeyCodeCombination(KeyCode.ENTER, KeyCombination.CONTROL_DOWN);
        final KeyCodeCombination zoomOutView = new KeyCodeCombination(KeyCode.MINUS, KeyCombination.CONTROL_DOWN);
        final KeyCodeCombination saveTextureFile = new KeyCodeCombination(KeyCode.E, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN);
        final KeyCodeCombination printFile = new KeyCodeCombination(KeyCode.P, KeyCombination.CONTROL_DOWN);
        final KeyCodeCombination close = new KeyCodeCombination(KeyCode.ESCAPE);
        
        scene.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler() {
            @Override
            public void handle(Event t) {
                if (newFile.match((KeyEvent) t)) {
                    newFileAction();
                } else if (importFile.match((KeyEvent) t)) {
                    importFileAction();
                } else if (applicationFile.match((KeyEvent) t)) {
                    applicationFileAction();
                } else if (refreshFile.match((KeyEvent) t)) {
                    refreshFileAction();
                } else if (zoomInView.match((KeyEvent) t)) {
                    zoomInAction();
                } else if (zoomOutView.match((KeyEvent) t)) {
                    zoomOutAction();
                } else if (normalView.match((KeyEvent) t)) {
                    zoomNormalAction();
                } else if (saveTextureFile.match((KeyEvent) t)) {
                    saveTextureFileAction();
                } else if (printFile.match((KeyEvent) t)) {
                    printAction();
                } else if (close.match((KeyEvent) t)) {
                    closeAction();
                }
            }
        });
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                KeyCode key = t.getCode();
                if (key == KeyCode.ESCAPE){
                    closeAction();
                } else if (key == KeyCode.DOWN){
                     zoomOutAction();
                } else if (key == KeyCode.UP){
                     zoomInAction();
                } else if (key == KeyCode.ENTER){
                     zoomNormalAction();
                }
            }
        });
    }
    private void addScrollPaneContainer(){        
        container.setContent(artwork);
        // Horizontal & Vertical scroll bar is only displayed when needed
        container.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        container.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        // Pannable.
        container.setPannable(false);        
        /*container.setSkin(new ScrollPaneSkin(container) {
            @Override
            public void onTraverse(Node n, Bounds b) {
            }
        });*/
        /*DoubleProperty hPosition = new SimpleDoubleProperty();
        hPosition.bind(container.hvalueProperty());
        hPosition.addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object old_val, Object new_val) {
                //imageX=(int)((motif.getWidth()-WIDTH)*(double) arg2);
                imageX = (int)(((double) new_val)/(int)Math.round(zoomFactor*data[0]));                
                //System.err.println(container.getHvalue()+" : "+container.getVvalue()+" :H-1: "+container.getHmax()+" : "+container.getVmax());
                getCropImage();
                getImage();
                System.err.println("calling image 1");
            }
        });
        DoubleProperty vPosition = new SimpleDoubleProperty();
        vPosition.bind(container.vvalueProperty());
        vPosition.addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object old_val, Object new_val) {
                //imageY=(int)((motif.getHeight()-HEIGHT)*(double) arg2);
                imageY=(int)(((double) new_val)/(int)Math.round(zoomFactor*data[1]));
                //System.err.println(container.getHvalue()+" : "+container.getVvalue()+" :*V-1*: "+container.getHmax()+" : "+container.getVmax());
                System.err.println("container.vvalueProperty()");
                getCropImage();
                getImage();
                System.err.println("calling image 2");
                
            }
        });*/
        container.hvalueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov,Number old_val, Number new_val) {
                if((double)new_val!=(double)old_val){
                    //imageX=(int)((motif.getWidth()-WIDTH)*(double) arg2);
                    imageX = (int)(((double) new_val)/(int)Math.round(zoomFactor*data[0]));                
                    //System.err.println(container.getHvalue()+" : "+container.getVvalue()+" :H-1: "+container.getHmax()+" : "+container.getVmax());
                    getCropImage();
                    getImage();
                }
          }
        });  
        container.vvalueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov,Number old_val, Number new_val) {
                if((double)new_val!=(double)old_val){
                    //imageY=(int)((motif.getHeight()-HEIGHT)*(double) arg2);
                    imageY=(int)(((double) new_val)/(int)Math.round(zoomFactor*data[1]));
                    //System.err.println(container.getHvalue()+" : "+container.getVvalue()+" :*V-1*: "+container.getHmax()+" : "+container.getVmax());
                    getCropImage();
                    getImage();
                }
            }
        });
        /*container.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                //System.out.println("Clicked");
                event.consume();
            }
        });
        container.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>(){ 
            @Override
            public void handle(MouseEvent event) {
                //System.out.println("Presed");
                event.consume();
            }
        });
        container.addEventHandler(MouseEvent.MOUSE_DRAGGED, new EventHandler<MouseEvent>(){ 
            @Override
            public void handle(MouseEvent event) {
                //System.out.println("Dragged");
                event.consume();
            }
        });
        container.addEventHandler(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>(){ 
            @Override
            public void handle(MouseEvent event) {
                //System.out.println("Relesed");
                event.consume();
            }
        });
        container.addEventHandler(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>(){ 
            @Override
            public void handle(MouseEvent event) {
                //System.out.println("Moved");
                event.consume();
            }
        });*/
    }
    private void addScrollPaneContainer(float fltZoom){
        double hValue = container.getHvalue()/container.getHmax();
        double vValue = container.getVvalue()/container.getVmax();
        double newhValue = ((motif.getWidth()*(int)Math.round(zoomFactor*data[0]))-WIDTH)>0?((motif.getWidth()*(int)Math.round(zoomFactor*data[0]))-WIDTH):(motif.getWidth()*(int)Math.round(zoomFactor*data[0]));
        double newvValue = ((motif.getHeight()*(int)Math.round(zoomFactor*data[1]))-HEIGHT)>0?((motif.getHeight()*(int)Math.round(zoomFactor*data[1]))-HEIGHT):(motif.getHeight()*(int)Math.round(zoomFactor*data[1]));
        //container.setPrefViewportWidth((motif.getWidth()*(int)Math.round(zoomFactor*data[0]))-WIDTH);
        //container.setPrefViewportHeight((motif.getHeight()*(int)Math.round(zoomFactor*data[1]))-HEIGHT);
        
        //System.err.println(container.getHvalue()+" : "+container.getVvalue()+" :*Start*: "+container.getHmax()+" : "+container.getVmax());
            container.setHmax(((motif.getWidth()*(int)Math.round(zoomFactor*data[0]))-WIDTH)>0?(motif.getWidth()*(int)Math.round(zoomFactor*data[0]))-WIDTH:(motif.getWidth()*(int)Math.round(zoomFactor*data[0])));
        //System.err.println(container.getHvalue()+" : "+container.getVvalue()+" :H-2: "+container.getHmax()+" : "+container.getVmax());
            container.setVmax(((motif.getHeight()*(int)Math.round(zoomFactor*data[1]))-HEIGHT)>0?(motif.getHeight()*(int)Math.round(zoomFactor*data[1]))-HEIGHT:(motif.getHeight()*(int)Math.round(zoomFactor*data[1])));
        //System.err.println(container.getHvalue()+" : "+container.getVvalue()+" :V-2: "+container.getHmax()+" : "+container.getVmax());
            container.setHvalue(hValue*newhValue);
        //System.err.println(container.getHvalue()+" : "+container.getVvalue()+" :H-3: "+container.getHmax()+" : "+container.getVmax());
            container.setVvalue(vValue*newvValue); 
        //System.err.println(container.getHvalue()+" : "+container.getVvalue()+" :V-3: "+container.getHmax()+" : "+container.getVmax());
            container.setHvalue(hValue*newhValue);
        //System.err.println(container.getHvalue()+" : "+container.getVvalue()+" :H-4: "+container.getHmax()+" : "+container.getVmax());
            getCropImage();
            getImage();
        //System.err.println(container.getHvalue()+" : "+container.getVvalue()+" :*Stop*: "+container.getHmax()+" : "+container.getVmax());        
    }
    private void setCropImage(){
        //box resized
        int x_inc = (int)Math.round(zoomFactor*data[0]);
        int y_inc = (int)Math.round(zoomFactor*data[1]);  
        //image size
        int intWidth = WIDTH/x_inc;
        int intHeight = HEIGHT/y_inc;
        //validate init points
        if(intWidth>=motif.getWidth()){
            intWidth = motif.getWidth();
            imageX = 0;
        } else{
            imageX = (imageX>=(motif.getWidth()-intWidth))?motif.getWidth()-1-intWidth:imageX;
        }
        if(intHeight>=motif.getHeight()){
            intHeight= motif.getHeight();
            imageY = 0;
        } else{
            imageY = (imageY>=(motif.getHeight()-intHeight))?motif.getHeight()-1-intHeight:imageY;
        }
        //System.err.println(imageX+":"+imageY+"::"+intWidth+":"+intHeight);
        for(int x= imageX, p=0; p<intWidth; x++,p++)
            for(int y= imageY, q=0; q<intHeight; y++, q++)
                motif.setRGB(x, y, objEditBufferedImage.getRGB(p, q)); 
        getCropImage();
    }
    private void getCropImage(){
        //box resized
        int x_inc = (int)Math.round(zoomFactor*data[0]);
        int y_inc = (int)Math.round(zoomFactor*data[1]);  
        //image size
        int intWidth = WIDTH/x_inc;
        int intHeight = HEIGHT/y_inc;
        //validate init points
        if(intWidth>=motif.getWidth()){
            intWidth = motif.getWidth();
            imageX = 0;
        } else{
            imageX = (imageX>=(motif.getWidth()-intWidth))?motif.getWidth()-1-intWidth:imageX;
        }
        if(intHeight>=motif.getHeight()){
            intHeight= motif.getHeight();
            imageY = 0;
        } else{
            imageY = (imageY>=(motif.getHeight()-intHeight))?motif.getHeight()-1-intHeight:imageY;
        }
        //System.err.println(imageX+":"+imageY+":=:"+intWidth+":"+intHeight);
        //create part imge from original image
        BufferedImage partImage = new BufferedImage(intWidth, intHeight, BufferedImage.OPAQUE);
        partImage = motif.getSubimage(imageX, imageY, intWidth, intHeight);        
        //to save the image into file
        try {
            File file = new File(System.getProperty("user.dir") + "/mla/temp/amit_test.png");
            if (file == null) return;
            ImageIO.write(partImage, "png", file);
            //System.out.println( "Image saved to " + file.getAbsolutePath());
        } catch (IOException e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, e);
        }
        partImage = null;
        prepareImage();        
    }
    private void prepareImage(){
        //System.err.println("prepare iamge");
        //box resized
        int x_inc = (int)Math.round(zoomFactor*data[0]);
        int y_inc = (int)Math.round(zoomFactor*data[1]);  
        //image size
        int intWidth = WIDTH/x_inc;
        int intHeight = HEIGHT/y_inc;
        //validate init points
        if(intWidth>=motif.getWidth()){
            intWidth = motif.getWidth();
        } 
        if(intHeight>=motif.getHeight()){
            intHeight= motif.getHeight();
        }
        try {
            File file = new File(System.getProperty("user.dir") + "/mla/temp/amit_test.png");
            if (file == null) return;
            BufferedImage partImage = ImageIO.read(file);
            objEditBufferedImage = new BufferedImage(intWidth, intHeight, BufferedImage.TYPE_INT_ARGB);
            //objEditBufferedImage = ImageIO.read(file);
            //objEditGraphics2D = (Graphics2D) objEditBufferedImage.getGraphics();
            objEditGraphics2D = objEditBufferedImage.createGraphics();
            objEditGraphics2D.drawImage(partImage, 0, 0, intWidth, intHeight, null);
            partImage = null;
        } catch (IOException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void getImage(){
        //System.err.println("get image");
        //box resized
        int x_inc = (int)Math.round(zoomFactor*data[0]);
        int y_inc = (int)Math.round(zoomFactor*data[1]);  
        //image size
        int intWidth = WIDTH/x_inc;
        int intHeight = HEIGHT/y_inc;
        //validate init points
        if(intWidth>=motif.getWidth()){
            intWidth = motif.getWidth();
            imageX = 0;
        } else{
            imageX = (imageX>=(motif.getWidth()-intWidth))?motif.getWidth()-1-intWidth:imageX;
        }
        if(intHeight>=motif.getHeight()){
            intHeight= motif.getHeight();
            imageY = 0;
        } else{
            imageY = (imageY>=(motif.getHeight()-intHeight))?motif.getHeight()-1-intHeight:imageY;
        }
        //System.err.println(imageX+":"+imageY+":=:"+intWidth+":"+intHeight);
        //create graphics image for editing
        BufferedImage partImage = new BufferedImage(intWidth*x_inc, intHeight*y_inc,BufferedImage.TYPE_INT_RGB);
        Graphics2D objGraphics2D = partImage.createGraphics();
        objGraphics2D.drawImage(objEditBufferedImage, 0, 0, intWidth*x_inc, intHeight*y_inc, null);
        if(data[0]*zoomFactor>3 && data[1]*zoomFactor>3){
            objGraphics2D.setColor(java.awt.Color.BLACK); 
            //draw gride lines over the image
            try {
                BasicStroke bs = new BasicStroke(2);
                objGraphics2D.setStroke(bs);
                GridLines gridLines = new GridLines(objGraphics2D, intHeight, intWidth, data, zoomFactor, (byte)3);
                while(gridLines.isAlive()) {
                    java.lang.Thread.sleep(100);
                }
                //System.err.println("Main thread run is over");
                //vlines = null;
            } catch(InterruptedException ex) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            } catch(Exception ex){
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }
        objGraphics2D.dispose();
        //set the image to imageview
        Image image = SwingFXUtils.toFXImage(partImage, null);
        artwork.setImage(image);
        //container.setContent(pane);
        //imageView.relocate(imageX, imageY);
        //to save the image into file
        /*try {
            File file = new File(System.getProperty("user.dir") + "/mla/temp/test_"+new java.util.Date().getTime()+".png");
            if (file == null) return;
            ImageIO.write(objEditBufferedImage, "png", file);
            System.out.println( "getImage saved to " + file.getAbsolutePath());
        } catch (IOException e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, e);
        }*/
        image = null;
        partImage = null;
        getImageArea(intWidth,intHeight);
    } 
    //mark main image
    private void getImageArea(int intWidth, int intHeight){
        int ratio = motif.getWidth()/100;
        BufferedImage partImage = new BufferedImage( motif.getWidth()/ratio, motif.getHeight()/ratio, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = partImage.createGraphics();
        g.drawImage(motif, 0, 0, motif.getWidth()/ratio, motif.getHeight()/ratio, null);
        BasicStroke bs = new BasicStroke(1);
        g.setStroke(bs);
        g.setColor(java.awt.Color.RED);
        g.drawRect(imageX/ratio, imageY/ratio, (intWidth/ratio)-1, (intHeight/ratio)-1);
        g.dispose();
        Image image = SwingFXUtils.toFXImage(partImage, null);
        artworkPreView.setImage(image);
        lblX.setText("X:"+imageX);
        lblY.setText("Y:"+imageY);
        image = null;
        partImage = null;
    }
    private void addArtworkEventHandler(){
        artwork.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                //System.out.println("Clicked");
                onMouseClickedListener(event);
            }
        });
        artwork.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>(){ 
            @Override
            public void handle(MouseEvent event) {
                //System.out.println("Presed");
                onMousePressedListener(event);
            }
        });
        artwork.addEventHandler(MouseEvent.MOUSE_DRAGGED, new EventHandler<MouseEvent>(){ 
            @Override
            public void handle(MouseEvent event) {
                //System.out.println("Dragged");
                onMouseDraggedListener(event);
            }
        });
        artwork.addEventHandler(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>(){ 
            @Override
            public void handle(MouseEvent event) {
                //System.out.println("Relesed");
                onMouseReleaseListener(event);
            }
        });
        artwork.addEventHandler(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>(){ 
            @Override
            public void handle(MouseEvent event) {
                //System.out.println("Moved");
                onMouseMoveListener(event);
            }
        });
    }    
    private void onMouseClickedListener(MouseEvent e){
        //System.err.println("mouse clicked");
        if(isSelected && selectdata)
            return;
        else
            getSelectionDragData(e);
        e.consume();
    }
    private void onMousePressedListener(MouseEvent e){
        //System.err.println("mouse pressed");
        this.startX = e.getX();
        this.startY = e.getY();
        e.consume();
    }
    private void onMouseDraggedListener(MouseEvent e){
        //System.err.println("mouse draged");        
        this.stopX = e.getX();
        this.stopY = e.getY();
        if(selectdata)
            drawSelectionArea();
        else if(dragselectdata)
            drawSelectionData(); 
        else
            e.consume();
    }
    private void onMouseReleaseListener(MouseEvent e){
        //System.err.println("mouse released");        
        if(isSelected && selectdata)
            assignSelectionData();
        else if(isSelected && dragselectdata)
            UnSelectDataAction();
        else
            e.consume();
    }
    private void onMouseMoveListener(MouseEvent e){
        //System.err.println("mouse moved");  
        e.consume();
    }
    //>>>>>>>>>>>>>>>>>>>>>Buttons control<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    private void setCurrentShape()
    {
        selectdata = false;
        noselectdata = false;
        dragselectdata = false;
    }
    //working events
    private void drawSelectionArea(){
        //System.err.println("selection box");
        //box resized
        int x_inc = (int)Math.round(zoomFactor*data[0]);
        int y_inc = (int)Math.round(zoomFactor*data[1]);         
        //reset image
        prepareImage();
        //set color
        objEditGraphics2D.setColor(java.awt.Color.RED);
        BasicStroke bs = new BasicStroke((float)1*zoomFactor);
        objEditGraphics2D.setStroke(bs);        
        double width = stopX - startX;
        double height = stopY - startY;
        lblStatus.setText("Selection Box:"+startX+":"+startY+"::"+stopX+":"+stopY);
        objEditGraphics2D.drawRect((int)(startX/x_inc), (int)(startY/y_inc), (int)(width/x_inc), (int)(height/y_inc));
        //re draw image
        getImage(); 
        isSelected = true;        
    }
    private void drawSelectionData(){
        //System.err.println("selection image");
        //box resized
        int x_inc = (int)Math.round(zoomFactor*data[0]);
        int y_inc = (int)Math.round(zoomFactor*data[1]);         
        //reset image
        prepareImage();
        String[] strData = lstDatas.get(dataIndex);
        //get fill image
        File file = new File(strData[0]);
        //uplaod the image  
        BufferedImage fillImage = null;
        try {
            fillImage = ImageIO.read(file);
        } catch (IOException | OutOfMemoryError ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        lblStatus.setText("Fill Image:"+startX+":"+startY+"::"+stopX+":"+stopY);
        objEditGraphics2D.drawImage(fillImage,(int)(stopX/x_inc), (int)(stopY/y_inc), (int)(fillImage.getWidth()), (int)(fillImage.getHeight()),null);
        //set fill image
        try {
            ImageIO.write(fillImage, "png", file);
        } catch (IOException | OutOfMemoryError ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        //System.out.println( "Image saved to " + file.getAbsolutePath());
        strData[0] = strData[0];
        strData[1] = ""+(imageX+(int)(stopX/x_inc));
        strData[2] = ""+(imageY+(int)(stopY/y_inc));
        strData[3] = ""+(imageX+(int)(stopX/x_inc)+(int)(fillImage.getWidth()));
        strData[4] = ""+(imageY+(int)(stopY/y_inc)+(int)(fillImage.getHeight()));
        lstDatas.set(dataIndex,strData);
        fillImage = null;
        //re draw image
        getImage(); 
        isSelected = true;
    }    
   
    private void getSelectionDragData(MouseEvent e){
        System.err.println("selection image data");
        //box resized
        int x_inc = (int)Math.round(zoomFactor*data[0]);
        int y_inc = (int)Math.round(zoomFactor*data[1]);
        System.err.println("");
        int x= imageX+(int)(e.getX()/x_inc);
        int y= imageY+(int)(e.getY()/y_inc);
        for(int i=lstDatas.size();i>0; i--){
            String[] strData = lstDatas.get(i-1);
            System.err.println(x+":"+y);
            System.err.println(i+"="+strData[1]+":"+strData[2]+"::"+strData[3]+":"+strData[4]);
            if((Integer.parseInt(strData[1].trim())<x && x<Integer.parseInt(strData[3].trim())) 
                    && (Integer.parseInt(strData[2].trim())<y && y<Integer.parseInt(strData[4].trim()))){
                System.err.println("Found");
                int width = Integer.parseInt(strData[3].trim()) - Integer.parseInt(strData[1].trim());
                int height = Integer.parseInt(strData[4].trim()) - Integer.parseInt(strData[2].trim());
                objEditGraphics2D.setColor(java.awt.Color.WHITE);
                objEditGraphics2D.fillRect(Integer.parseInt(strData[1].trim())-imageX
                        , Integer.parseInt(strData[2].trim())-imageY
                        , width, height);
                // re-draw image
                getImage();
                setCropImage();
                dataIndex = i-1;
                setCurrentShape();
                dragselectdata = true;
                isSelected = true;
                isSelectedButton();
                stopX=e.getX();//Double.parseDouble(strData[1].trim())*x_inc;
                stopY=e.getY();//Double.parseDouble(strData[2].trim())*y_inc;
                drawSelectionData();
                break;
            }else{
                System.err.println("Not Found");
            }
        }
    }
    private void assignSelectionData(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONJACQUARDCONVERSIONEDIT"));
        
        final Stage artworkChildStage = new Stage();
        artworkChildStage.initOwner(artworkStage);
        artworkChildStage.initStyle(StageStyle.UTILITY);
        //artworkPopupStage.initModality(Modality.WINDOW_MODAL);
        GridPane popup=new GridPane();
        popup.setId("popup");
        //popup.setAlignment(Pos.CENTER);
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
               
        Label lblNote = new Label(objDictionaryAction.getWord("HELP"));
        lblNote.setWrapText(true);
        lblNote.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        lblNote.setTooltip(new Tooltip(objDictionaryAction.getWord("HELPWEAVEFILLTOOL")));
        //lblNote.setPrefWidth(objConfiguration.WIDTH*0.15);
        lblNote.setAlignment(Pos.CENTER);
        lblNote.setStyle("-fx-font: bold italic 11pt Arial; -fx-text-fill: #FF0000;");
        popup.add(lblNote, 0, 0, 2, 1); 

        Separator sepHor = new Separator();
        sepHor.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHor, 0, 1);
        GridPane.setColumnSpan(sepHor, 2);
        popup.getChildren().add(sepHor);

        Label fillToolArtwork = new Label(objDictionaryAction.getWord("ARTWORK")+" : ");
        fillToolArtwork.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPDEFAULTARTWORK")));
        popup.add(fillToolArtwork, 0, 2);
        final Label artworkDefaultTF = new Label();
        artworkDefaultTF.setText((objArtwork!=null && objArtwork.getStrArtworkId()!="")?objArtwork.getStrArtworkId():objDictionaryAction.getWord("NOITEM"));
        artworkDefaultTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPDEFAULTARTWORK")));
        popup.add(artworkDefaultTF, 1, 2); 
        final ImageView artworkIV = new ImageView(new Image("/media/add_item.png"));
        artworkIV.setFitHeight(50);
        artworkIV.setFitWidth(50);
        popup.add(artworkIV, 0, 3);
        final Hyperlink artworkDefaultHL = new Hyperlink();
        artworkDefaultHL.setText(objDictionaryAction.getWord("CHANGEDEFAULT"));
        artworkDefaultHL.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/browse.png"));
        artworkDefaultHL.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPDEFAULTWEAVE")));
        popup.add(artworkDefaultHL, 1, 3);   
        if(objArtwork!=null && objArtwork.getStrArtworkId()!=null && objArtwork.getStrArtworkId()!=""){
            try {    
                //display artwork name
                artworkDefaultTF.setText(objArtwork.getStrArtworkName());
                //display artwork icon
                byte[] bytArtworkThumbnil=objArtwork.getBytArtworkThumbnil();
                SeekableStream stream = new ByteArraySeekableStream(bytArtworkThumbnil);
                String[] names = ImageCodec.getDecoderNames(stream);
                ImageDecoder dec = ImageCodec.createImageDecoder(names[0], stream, null);
                RenderedImage im = dec.decodeAsRenderedImage();
                artworkIV.setImage(SwingFXUtils.toFXImage(PlanarImage.wrapRenderedImage(im).getAsBufferedImage(), null));
                bytArtworkThumbnil=null;
                System.gc();
            } catch (Exception ex) {
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("INVALIDARTWORK"));
            }
        }else{
            artworkDefaultTF.setText(objDictionaryAction.getWord("NOITEM"));
            artworkIV.setImage(new Image("/media/add_item.png"));
            artwork.setCursor(Cursor.HAND);
            lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
        }
        artworkDefaultHL.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    artwork.setCursor(Cursor.HAND);
                    //artwork.setCursor(new ImageCursor(new Image(objConfiguration.getStrMouse()+"/fill_tool.png")));
                    //artwork.setCursor(new ImageCursor(new Image(objConfiguration.getStrMouse()+"/fill_tool.png"), 4, 4));
                    //artwork.setCursor(Cursor.cursor(objConfiguration.getStrMouse()+"/z_fill_tool.png"));
                    //System.err.println("size+"+ImageCursor.getBestSize(0, 0));
                    objArtwork= new Artwork();
                    objArtwork.setObjConfiguration(objConfiguration);
                    ArtworkImportView objArtworkImportView= new ArtworkImportView(objArtwork);

                    if(objArtwork!=null && objArtwork.getStrArtworkId()!=null && objArtwork.getStrArtworkId()!=""){
                        ArtworkAction objArtworkAction = new ArtworkAction();
                        objArtworkAction.getArtwork(objArtwork);
                        //display artwork name
                        artworkDefaultTF.setText(objArtwork.getStrArtworkName());
                        //display artwork icon
                        byte[] bytArtworkThumbnil=objArtwork.getBytArtworkThumbnil();
                        SeekableStream stream = new ByteArraySeekableStream(bytArtworkThumbnil);
                        String[] names = ImageCodec.getDecoderNames(stream);
                        ImageDecoder dec = ImageCodec.createImageDecoder(names[0], stream, null);
                        RenderedImage im = dec.decodeAsRenderedImage();
                        artworkIV.setImage(SwingFXUtils.toFXImage(PlanarImage.wrapRenderedImage(im).getAsBufferedImage(), null));
                        bytArtworkThumbnil=null;
                        System.gc();
                    }else{
                        artworkDefaultTF.setText(objDictionaryAction.getWord("NOITEM"));
                        artworkIV.setImage(new Image("/media/add_item.png"));
                        artwork.setCursor(Cursor.HAND);
                        lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
                    }
                } catch (Exception ex) {
                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                    new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("INVALIDARTWORK"));
                }
            }
        });     

        Label lblRepeatMode=new Label(objDictionaryAction.getWord("REPEATMODE"));
        lblRepeatMode.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPREPEATMODE")));
        popup.add(lblRepeatMode, 0, 5);
        final ChoiceBox repeatModeCB=new ChoiceBox();
        repeatModeCB.getItems().add(0, "Rectangular (Default)");
        repeatModeCB.getItems().add(1, "1/2 Horizontal");
        repeatModeCB.getItems().add(2, "1/2 Vertical");
        repeatModeCB.getItems().add(3, "1/3 Horizontal");
        repeatModeCB.getItems().add(4, "1/3 Vertical");
        repeatModeCB.getItems().add(5, "1/4 Horizontal");
        repeatModeCB.getItems().add(6, "1/4 Vertical");
        repeatModeCB.getItems().add(7, "1/5 Horizontal");
        repeatModeCB.getItems().add(8, "1/5 Vertical");
        repeatModeCB.getItems().add(9, "1/6 Horizontal");
        repeatModeCB.getItems().add(10, "1/6 Vertical");        
        repeatModeCB.setValue("Rectangular (Default)");
        repeatModeCB.setDisable(!objConfiguration.getBlnMRepeat());
        popup.add(repeatModeCB, 1, 5);
        
        Label lblRepeatMirror=new Label(objDictionaryAction.getWord("MIRROR"));
        lblRepeatMirror.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPMIRROR")));
        popup.add(lblRepeatMirror, 0, 6);
        final ChoiceBox repeatMirrorCB=new ChoiceBox();
        repeatMirrorCB.getItems().add(0, "No Mirroring (Default)");        
        repeatMirrorCB.getItems().add(1, "Vertical Mirror");
        repeatMirrorCB.getItems().add(2, "Horizontal Mirror");
        repeatMirrorCB.getItems().add(3, "Vertical-Horizontal Mirror");
        repeatMirrorCB.setValue("No Mirroring (Default)");
        repeatMirrorCB.setDisable(!objConfiguration.getBlnMRepeat());
        popup.add(repeatMirrorCB, 1, 6);

        Button btnApply = new Button(objDictionaryAction.getWord("APPLY"));
        btnApply.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/update.png"));
        btnApply.setTooltip(new Tooltip(objDictionaryAction.getWord("ACTIONAPPLY")));
        //btnApply.setMaxWidth(Double.MAX_VALUE);
        btnApply.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                //reset image
                prepareImage();
                try {
                    if(objArtwork!=null && objArtwork.getStrArtworkId()!=null && objArtwork.getStrArtworkId()!=""){
                        ArtworkAction objArtworkAction = new ArtworkAction();
                        objArtworkAction.getArtwork(objArtwork);
                        objArtworkAction = new ArtworkAction();
                        
                        byte[] bytArtworkThumbnil=objArtwork.getBytArtworkThumbnil();
                        SeekableStream stream = new ByteArraySeekableStream(bytArtworkThumbnil);
                        String[] names = ImageCodec.getDecoderNames(stream);
                        ImageDecoder dec = ImageCodec.createImageDecoder(names[0], stream, null);
                        RenderedImage im = dec.decodeAsRenderedImage();
                        BufferedImage fillImage = PlanarImage.wrapRenderedImage(im).getAsBufferedImage();
                        bytArtworkThumbnil=null;
                        
                        //box resized
                        int x_inc = (int)Math.round(zoomFactor*data[0]);
                        int y_inc = (int)Math.round(zoomFactor*data[1]); 
                        double width = (stopX - startX)/x_inc;
                        double height = (stopY - startY)/y_inc;
                        
                        int vRepeat = (int)((height/fillImage.getHeight())>1?(height/fillImage.getHeight()):1);
                        int hRepeat = (int)((width/fillImage.getWidth())>1?(width/fillImage.getWidth()):1);

                        if(repeatMirrorCB.getValue().toString().contains("Vertical Mirror")){
                            objArtworkAction = new ArtworkAction(false);
                            fillImage = objArtworkAction.getImageVerticalMirrorRepeats(fillImage);
                            vRepeat = (int)Math.ceil((double)vRepeat/2);
                        } else if(repeatMirrorCB.getValue().toString().contains("Horizontal Mirror")){
                            objArtworkAction = new ArtworkAction(false);
                            fillImage = objArtworkAction.getImageHorizontalMirrorRepeats(fillImage);
                            hRepeat = (int)Math.ceil((double)hRepeat/2);
                        } else if(repeatMirrorCB.getValue().toString().contains("Vertical-Horizontal Mirror")){
                            objArtworkAction = new ArtworkAction(false);
                            fillImage = objArtworkAction.getImageHorizontalMirrorRepeats(fillImage);
                            fillImage = objArtworkAction.getImageVerticalMirrorRepeats(fillImage);
                            hRepeat = (int)Math.ceil((double)hRepeat/2);
                            vRepeat = (int)Math.ceil((double)vRepeat/2);
                        } else {
                            //vRepeat = 1;
                            //hRepeat = 1;
                        }

                        int vAling = 1;
                        int hAling = 1;
                        if(repeatModeCB.getValue().toString().contains("Vertical")){
                            String temp = repeatModeCB.getValue().toString();
                            vAling = Integer.parseInt(temp.substring(0, temp.indexOf("/")).trim());
                            hAling = Integer.parseInt(temp.substring(2, temp.indexOf("Vertical")).trim());
                        }else if(repeatModeCB.getValue().toString().contains("Horizontal")){
                            String temp = repeatModeCB.getValue().toString();
                            hAling = Integer.parseInt(temp.substring(0, temp.indexOf("/")).trim());
                            vAling = Integer.parseInt(temp.substring(2, temp.indexOf("Horizontal")).trim());
                        }else{
                            vAling = 1;
                            hAling = 1;
                        }
                        
                        /*editVerticalRepeatValue = Integer.parseInt(verticalTF.getText());
                        editHorizontalRepeatValue = Integer.parseInt(horizentalTF.getText());
                        editRepeatMode = repeatModeCB.getValue().toString();
                        editMirrorMode = repeatMirrorCB.getValue().toString();*/
                        objArtworkAction = new ArtworkAction(false);
                        fillImage = objArtworkAction.designRepeat(fillImage, vAling, hAling, vRepeat, hRepeat);
                        //objUR.doCommand("Repeats", deepCopy(objBufferedImage));
                        
                        //dump file  
                        fillImage = objArtworkAction.resizeDesign(fillImage, (int)(width), (int)(height));
                        File file = new File(System.getProperty("user.dir") + "/mla/temp/artworkviewer/amit_test_"+(imageX+(int)(startX/x_inc))+"_"+(imageY+(int)(startY/y_inc))+"_"+(imageX+(int)(stopX/x_inc))+"_"+(imageY+(int)(stopY/y_inc))+".png");
                        if (file == null) return;
                        ImageIO.write(fillImage, "png", file);
                        //System.out.println( "Image saved to " + file.getAbsolutePath());
                        String[] strData = new String[5];
                        strData[0] = file.getCanonicalPath();;
                        strData[1] = ""+(imageX+(int)(startX/x_inc));
                        strData[2] = ""+(imageY+(int)(startY/y_inc));
                        strData[3] = ""+(imageX+(int)(stopX/x_inc));
                        strData[4] = ""+(imageY+(int)(stopY/y_inc));
                        dataIndex = lstDatas.size();
                        lstDatas.add(strData);
                        
                        objEditGraphics2D.drawImage(fillImage,(int)(startX/x_inc), (int)(startY/y_inc), (int)(width), (int)(height),null);
                        //drawimage
                        getImage();
                        setCurrentShape();
                        dragselectdata = true;
                        //drawSelectionData();
                        isSelectedButton();
                        //addContextMenu(); 
                        fillImage =null;                        
                        lblStatus.setText(objDictionaryAction.getWord("SUCCESS"));
                    }
                } catch (SQLException ex) {               
                    new Logging("SEVERE",getClass().getName(),"Operation apply",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                } catch (IOException ex) {
                    new Logging("SEVERE",getClass().getName(),"Operation apply",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                } catch(OutOfMemoryError ex){
                    //undoAction();
                }
                        
                System.gc();
                if(artworkChildStage!=null)
                    artworkChildStage.close();
            }
        });
        popup.add(btnApply, 0, 9);
        
        
        artworkChildStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                artworkChildStage.close();
            }
        });
        Scene popupScene = new Scene(popup);
        popupScene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        artworkChildStage.setScene(popupScene);
        artworkChildStage.setResizable(false);
        artworkChildStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("WINDOWWEAVEASSIGNMENT"));
        artworkChildStage.showAndWait();
        
        //apply changes
        //setCropImage();
    }    
    
    private void zoomOutAction(){
        System.err.println("Zoom Out "+zoomFactor);
        zoomFactor-=.5;//zoomfactor/=2;        
        if(minSizeCheck(motif.getWidth(),motif.getHeight())){
            addScrollPaneContainer((float)0.5);
        }else{
            zoomFactor+=.5; //zoomfactor*=2;
            lblStatus.setText(objDictionaryAction.getWord("MAXZOOMOUT"));
        }
        /*if(artwork.getScaleX()==(double)1){
            lblStatus.setText(objDictionaryAction.getWord("MAXZOOMOUT"));
            return;
        }
        artwork.setScaleX(artwork.getScaleX()/2);
        artwork.setScaleY(artwork.getScaleY()/2);
        artwork.setScaleZ(artwork.getScaleZ()/2);
        */
        lblStatus.setText(objDictionaryAction.getWord("ACTIONZOOMOUTVIEW"));
    }
    
    private void zoomInAction(){
        System.err.println("Zoom In "+zoomFactor);
        zoomFactor+=.5; //zoomfactor*=2;
        if(maxSizeCheck(motif.getWidth(),motif.getHeight())){
            addScrollPaneContainer((float)-0.5); 
        }else{
            zoomFactor-=.5;//zoomfactor/=2;
            lblStatus.setText(objDictionaryAction.getWord("MAXZOOMIN"));
        }
        /*if(artwork.getScaleX()==(double)16){
            lblStatus.setText(objDictionaryAction.getWord("MAXZOOMIN"));
            return;
        }
        artwork.setScaleX(artwork.getScaleX()*2);
        artwork.setScaleY(artwork.getScaleY()*2);
        artwork.setScaleZ(artwork.getScaleZ()*2);
        */
        lblStatus.setText(objDictionaryAction.getWord("ACTIONZOOMINVIEW"));
    }
    
    private void zoomNormalAction(){
        zoomFactor=1;
        addScrollPaneContainer((float)0.0);
        /*artwork.setScaleX(1);
        artwork.setScaleY(1);
        artwork.setScaleZ(1);*/
        lblStatus.setText(objDictionaryAction.getWord("ACTIONZOOMNORMALVIEW"));
    }    
    private boolean maxSizeCheck( int intEnds, int intPics){
        int nonGraph=(int)(zoomFactor*2*intEnds*intPics);
        int yesGraph=(int)(zoomFactor*2*intEnds*intPics*data[0]*data[1]);
        return true;//(nonGraph<99999999 && yesGraph<99999999)?true:false;//55574528=95*1024*1024=95MB
    }
    private boolean minSizeCheck( int intEnds, int intPics){
        int nonGraphWarp=(int)(zoomFactor*intEnds/2);
        int nonGraphWeft=(int)(zoomFactor*intPics/2);
        int yesGraphWarp=(int)(zoomFactor*intEnds*data[1]/2);
        int yesGraphWeft=(int)(zoomFactor*intPics*data[0]/2);
        //box resized
        int x_inc = (int)Math.round(zoomFactor*data[1]);
        int y_inc = (int)Math.round(zoomFactor*data[0]);
        
        //return (zoomFactor>0 && nonGraphWarp>0 && nonGraphWeft>0 && x_inc>2 && y_inc>2 && yesGraphWarp>intEnds && yesGraphWeft>intPics)?true:false;
        return zoomFactor>0;
    }
    
    
    private void closeAction(){
        artworkStage.close();
        System.gc();
    }
    
    private void newFileAction(){
        int initWidth=(int)(256*12*zoomFactor); //objConfiguration.getIntEnds()
        int initHeight=(int)(48*12*zoomFactor); //objConfiguration.getIntPixs()
        motif = new BufferedImage(initWidth, initHeight, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = motif.createGraphics();
        g.drawImage(motif, 0, 0, initWidth, initHeight, null);
        g.setColor(java.awt.Color.WHITE);
        g.fillRect(0, 0, initWidth, initHeight);
        g.dispose();
        System.gc();
        System.err.println(motif.getWidth()+"="+motif.getHeight());         
        //add scroll Pane
        addScrollPaneContainer();
        addScrollPaneContainer((float)0.0);
    }
    private void importFileAction(){
        FileChooser fileChooser = new FileChooser();             
        //Set extension filter
        FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
        FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("BMP files (*.bmp)", "*.BMP");
        FileChooser.ExtensionFilter extFilterTIF = new FileChooser.ExtensionFilter("TIFF files (*.tif)", "*.TIF");
        fileChooser.getExtensionFilters().addAll(extFilterPNG, extFilterJPG, extFilterBMP, extFilterTIF);
        //fileChooser.setInitialDirectory(new File(objFabric.getObjConfiguration().strRoot));
        fileChooser.setTitle("Import");
        //Show open file dialog
        File file = fileChooser.showOpenDialog(null);
        if(file!=null){
            System.err.println("file found at "+file.getAbsolutePath());
        }else{
            file = new File(System.getProperty("user.dir") + "/mla/temp/tempdesign.png");
            System.err.println("file not found at "+file.getAbsolutePath());
        }
        //uplaod the image             
        try {
            filePath = file.getCanonicalPath();
            motif = ImageIO.read(file);
        } catch (IOException | OutOfMemoryError ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        file=null;
        System.err.println(motif.getWidth()+"="+motif.getHeight());         
        //add scroll Pane
        addScrollPaneContainer();
        addScrollPaneContainer((float)0.0);
        System.gc();
    }
    private void applicationFileAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONPAINTAPPLICATION"));
        try {
            UtilityAction objUtilityAction = new UtilityAction();
            Device objDevice = new Device(null, "Designing S/W", null, null);
            objDevice.setObjConfiguration(objConfiguration);
            Device[] devices = objUtilityAction.getDevices(objDevice);
            if(devices.length>0){            
                final Stage artworkPopupStage = new Stage();
                artworkPopupStage.initStyle(StageStyle.UTILITY);
                //dialogStage.initModality(Modality.WINDOW_MODAL);
                ComboBox deviceCB = new ComboBox();
                deviceCB.getItems().add("Select");
                for(int i=0; i<devices.length; i++)
                    deviceCB.getItems().add(devices[i].getStrDeviceName());
                deviceCB.setValue("Select");
                deviceCB.valueProperty().addListener(new ChangeListener<String>() {
                    @Override
                    public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                        try {
                            lblStatus.setText(objDictionaryAction.getWord("ARTWORKEDIT"));

                            UtilityAction objUtilityAction = new UtilityAction();
                            Device objDevice = new Device(null, null, t1, null);
                            objDevice.setObjConfiguration(objConfiguration);
                            objUtilityAction.getDevice(objDevice);
                            if(objDevice.getStrDevicePath()!=null){
                                Runtime rt = Runtime.getRuntime();
                                if(objConfiguration.getObjProcessProcessBuilder()!=null)
                                    objConfiguration.getObjProcessProcessBuilder().destroy();
                                File file = new File(objDevice.getStrDevicePath());
                                if(file.exists() && !file.isDirectory()) {
                                    //if no motif present then create new motif
                                    if(motif==null)
                                        newFileAction();
                                    filePath = System.getProperty("user.dir") + "/mla/temp/tempdesign.png";
                                    filePath = filePath.replace("\\","/");
                                    file = new File(filePath);
                                    ImageIO.write(motif, "png", file);
                                    artworkPopupStage.close();
                                    objArtwork.getObjConfiguration().setObjProcessProcessBuilder(new ProcessBuilder(objDevice.getStrDevicePath(),file.getAbsolutePath()).start());                                    
                                }else{
                                    lblStatus.setText(objDictionaryAction.getWord("NOITEM"));
                                }
                            }else{
                                lblStatus.setText(objDictionaryAction.getWord("NOVALUE"));
                            }
                        } catch (SQLException ex) {
                            new Logging("SEVERE",getClass().getName(),"SQLException:Operation edit artwork"+ex.toString(),ex);
                            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                        } catch (IOException ex) {
                            new Logging("SEVERE",getClass().getName(),"IOException:Operation edit artwork"+ex.toString(),ex);
                            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                        } catch (Exception ex) {
                            new Logging("SEVERE",getClass().getName(),"Exception:Operation edit artwork"+ex.toString(),ex);
                            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                        }
                        if(artworkPopupStage!=null)
                            artworkPopupStage.close();
                    }
                });
                artworkPopupStage.setScene(new Scene(deviceCB,200,50));
                artworkPopupStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("SELECTTO")+" "+objDictionaryAction.getWord("RUN"));
                artworkPopupStage.showAndWait();
                lblStatus.setText(objDictionaryAction.getWord("ACTIONIMPORTFILE"));
            }else{
                lblStatus.setText(objDictionaryAction.getWord("NODEVICE"));
            }
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Operation import"+ex.getMessage(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
        System.gc();
    }
    private void refreshFileAction(){
        if(filePath!=null){
            lblStatus.setText(objDictionaryAction.getWord("ACTIONIMPORTREFRESH"));
            try {
                //objURF.store(bufferedImage);
                File file = new File(filePath);
                if(file.exists()){
                    //uplaod the image             
                    filePath = file.getCanonicalPath();
                    motif = ImageIO.read(file);
                    file=null;
                    System.err.println(motif.getWidth()+"="+motif.getHeight());         
                    //add scroll Pane
                    addScrollPaneContainer();
                    addScrollPaneContainer((float)0.0);
                } else {
                    lblStatus.setText(objDictionaryAction.getWord("INVALIDDATA"));
                }
            } catch (IOException ex) {
                new Logging("SEVERE",getClass().getName(),"Operation import",ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            } catch(OutOfMemoryError ex){
                //undoAction();
                lblStatus.setText(objDictionaryAction.getWord("MAXVALUE"));
            }
        } else{
            lblStatus.setText(objDictionaryAction.getWord("INVALIDDATA"));
        }
        System.gc();
    }
    private void saveTextureFileAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONEXPORTFILE"));                
        try {
            FileChooser artworkExport=new FileChooser();
            artworkExport.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTPNG")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
            FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
            artworkExport.getExtensionFilters().add(extFilterPNG);
            File file=artworkExport.showSaveDialog(artworkStage);
            if(file==null)
                return;
            else
                artworkStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+file.getAbsoluteFile().getName()+"]");   
            if (!file.getName().endsWith("png")) 
                file = new File(file.getPath() +".png");
            ImageIO.write(motif, "png", file);    

            if(objConfiguration.getBlnAuthenticateService()){
                ArrayList<File> filesToZip=new ArrayList<>();
                filesToZip.add(file);
                String zipFilePath=file.getAbsolutePath()+".zip";
                String passwordToZip = objConfiguration.getObjUser().getStrAppPassword();//file.getName();
                new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                file.delete();
            }
            lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+file.getCanonicalPath());
        } catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
        System.gc();
    }
    private void printAction(){
        new PrintView(motif, 0, "", objConfiguration.getObjUser().getStrName());
    }

    @Override
    public void start(Stage stage) throws Exception {
        new ArtworkEditView(stage);
        new Logging("WARNING",getClass().getName(),"UnsupportedOperationException",new UnsupportedOperationException("Not supported yet."));
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

class GridLines extends java.lang.Thread {
    Graphics2D objGraphics2D;
    int intHeight, intWidth;
    int[] data;
    float zoomFactor;
    byte type;
    GridLines(Graphics2D g, int intHeight, int intWidth, int[] data, float zoomFactor, byte type) {
        super("my extending thread");
        this.objGraphics2D = g;
        this.intHeight = intHeight;
        this.intWidth = intWidth;
        this.data = data;
        this.zoomFactor = zoomFactor;
        this.type = type;
        //System.out.println("my thread created" + this);
        start();
    }
    public void run() {
        try {
            BasicStroke bs = new BasicStroke(2);
            objGraphics2D.setStroke(bs);
            //box resized
            int x_inc = (int)Math.round(zoomFactor*data[1]);
            int y_inc = (int)Math.round(zoomFactor*data[0]);
            //last line vertical
            objGraphics2D.drawLine(intWidth*x_inc, 0, intWidth*x_inc, intHeight*y_inc);
            //last line horizontal
            objGraphics2D.drawLine(0, intHeight*y_inc, intWidth*x_inc, intHeight*y_inc);
            //For vertical line
            for(int j = 0; j < intWidth; j++) {
                if((j%data[0])==0 && type==3){
                    bs = new BasicStroke(2*zoomFactor);
                    objGraphics2D.setStroke(bs);
                }else{
                    bs = new BasicStroke(1*zoomFactor);
                    objGraphics2D.setStroke(bs);
                }
                objGraphics2D.drawLine(j*x_inc, 0, j*x_inc, intHeight*y_inc);
            }
            //For horizental line
            for(int i = 0; i < intHeight; i++) {
                if((i%data[1])==0 && type==3){
                    bs = new BasicStroke(2*zoomFactor);
                    objGraphics2D.setStroke(bs);
                }else{
                    bs = new BasicStroke(1*zoomFactor);
                    objGraphics2D.setStroke(bs);
                }
                objGraphics2D.drawLine(0, i*y_inc, intWidth*x_inc, i*y_inc);
            }
        } catch(Exception ex) {
            ex.printStackTrace();
            //System.out.println("my thread interrupted"+ex.getCause().toString());
            //Logger.getLogger(VLines.class.getName()).log(Level.SEVERE, null, ex);
        }
        //System.out.println("My thread run is over" );
    }
}