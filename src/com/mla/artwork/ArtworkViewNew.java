/*
 * Copyright (C) Digital India Corporation (Media Lab Asia)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mla.artwork;

import com.mla.cloth.ClothView;
import com.mla.colour.ColourSelector;
import com.mla.dictionary.DictionaryAction;
import com.mla.fabric.FabricView;
import com.mla.pattern.PatternView;
import com.mla.main.Configuration;
import com.mla.main.EncryptZip;
import com.mla.main.Logging;
import com.mla.utility.AboutView;
import com.mla.utility.HelpView;
import com.mla.utility.ContactView;
import com.mla.utility.TechnicalView;
import com.mla.main.WindowView;
import com.mla.main.IDGenerator;
import com.mla.main.MessageView;
import com.mla.main.UndoRedo;
import com.mla.print.PrintView;
//import com.mla.main.UndoRedoFile;
import com.mla.secure.Security;
import com.mla.simulator.MappingEditView;
import com.mla.user.UserAction;
import com.mla.utility.Device;
import com.mla.simulator.SimulatorEditView;
import com.mla.user.UserLoginView;
import com.mla.utility.Palette;
import com.mla.utility.UtilityAction;
import com.mla.weave.Weave;
import com.mla.weave.WeaveAction;
import com.mla.weave.WeaveEditView;
import com.mla.weave.WeaveImportView;
import com.mla.weave.WeaveView;
import com.sun.media.jai.codec.ByteArraySeekableStream;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.SeekableStream;
import java.awt.BasicStroke;
import java.awt.Dimension;
import java.awt.Graphics;
//import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.RenderedImage;
import java.awt.image.WritableRaster;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
//import javafx.scene.canvas.Canvas;
//import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.BoxBlur;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.effect.MotionBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
//import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javax.imageio.ImageIO;
import javax.media.jai.PlanarImage;
/**
 * ArtworkViewNew Class
 * <p>
 * This class is used for defining GUI methods for artwork editor.
 *
 * @author Amit Kumar Singh
 * @version %I%, %G%
 * @since   1.0
 * @date 07/01/2016
 * @Designing GUI method class for artwork editor
 * @see javafx.stage.*;
 */
public class ArtworkViewNew {
    
    Artwork objArtwork;
    ArtworkAction objArtworkAction;
    Configuration objConfiguration;
    DictionaryAction objDictionaryAction = null;
    UndoRedo objUR;
    //UndoRedoFile objURF;
    Weave objWeave;
    
    private String selectedMenu = "FILE";
    
    private Stage artworkStage;
    private Stage artworkChildStage;
    private BorderPane root;    
    final private Scene scene;
    private MenuButton menuSelected;
    private ToolBar toolBar;
    private MenuBar menuBar; 
    //private ScrollPane bodyContainer;
    //private StackPane stackPane;
    //private ScrollPane scrollPane;
    private ScrollPane container;
    private GridPane bodyContainer;
    private Label lblStatus;
    private ProgressBar progressB;
    private ProgressIndicator progressI;
    
    private Menu homeMenu;
    private Menu fileMenu;
    private Menu editMenu;
    private Menu viewMenu;
    private Menu utilityMenu;
    private Menu supportMenu;    
    private Menu runMenu;    
    private Menu transformMenu;
    private Menu paintMenu;
    private Label fileMenuLabel;
    private Label editMenuLabel;
    private Label viewMenuLabel;
    private Label utilityMenuLabel;
    private Label runMenuLabel;    
    private Label transformMenuLabel;
    private Label paintMenuLabel;
    
    private Button refreshFileBtn;
    final String regExDouble="[0-9]{1,13}(\\.[0-9]*)?";
    /* front view = 1, rear view = 2, flip view = 3, front visulaization = 4, rear visuallization = 5, flip visulaization = 6,
    front cross section = 7, rear cross section = 8, flip cross section = 9, grid = 10, graph = 11,  tilled = 12 */
    private byte plotViewActionMode = 0; 
    /* others = 1, editJaqurad = 2, editGraph = 3, editTransform = 4, editColor = 5,  filltool = 7, spraytool = 8, freehanddesign = 9, eraser = 10, 
    drawline = 11, drawarc = 12, beziercurve = 13, bspline = 14, drawcurve = 15, drawcurveClosed = 16, drawpath = 17, drawpathClosed = 18
    drawoval = 21, drawellipse = 22, drawtriangleEqui = 26, drawtriangleRight = 27,
    drawrectangle = 31, drawrectangleRound = 32, drawdiamond = 36, drawrhombus = 37, drawheart = 38,
    drawpentagon = 41, drawhexagone = 42, drawpolygon = 43, drawfourpointstar = 44, drawfivepointstar = 45, drawsixpointstar = 46, drawploygon = 27,
    selectdata = 51, noselectdata = 52, dragselectdata = 53, copydata = 54, pastedata = 55, cutdata = 56, croipdata = 57,
    mirrorverticaldata = 61, mirrorhorizentadata = 62, rotateclockwisedata = 63, rotateanticlockwisedata = 64, tiltleft = 66, tiltright = 67, tiltup = 68, tiltdown = 69,
    moveleft = 71, moveright = 72, moveup = 73, movedown = 74, move8left = 75, move8right = 76, move8up = 77, move8down = 78; */
    private byte plotEditActionMode = 0;
    private byte resizeLockValue = 2; //1=Pixel 2=Inches 3=density
    private String strFillArea = "Color All Instance";
    private String strFillBorder = "All Inner Border";
    private String editRepeatMode = "Rectangular (Default)"; 
    private String editMirrorMode = "No Mirroring (Default)"; 
    private int editVerticalRepeatValue=1;
    private int editHorizontalRepeatValue=1;
    private int editThreadPrimaryValue=1;
    private int editThreadSecondaryValue=1;
    private byte intFillSize = 0;
    private byte intColor=1;
    private String filePath;
    private double[] points;
    private boolean isDrawPath = false;
    private double startX, startY, lastX, lastY, oldX, oldY, width, height, angle;
    private int imageX=0;
    private int imageY=0;
    private float zoomfactor = 1;
    private byte graphFactor = 1;    
    private ArrayList<java.awt.Color> colors = new ArrayList<java.awt.Color>();
    //private ColorPicker colorPick;
    private GridPane editThreadGP;
    /*
    private GraphicsContext backgroundGC,foregroundGC, rulerGC;
    private ScrollPane backgroundSP, foregroundSP, rulerSP;
    private Canvas background,foreground,ruler;
    
    private GraphicsContext editLayerGC,gridLayerGC;
    private ScrollPane editLayerSP, gridLayerSP;
    private Canvas editLayer,gridLayer;
    */
    private ImageView artwork = new ImageView();
    private ImageView artworkPreView = new ImageView();
    private Graphics2D objGraphics2D;
    private Graphics2D objEditGraphics2D;
    private BufferedImage objBufferedImage = null;
    private BufferedImage objEditBufferedImage = null;
    private BufferedImage motif = null;    
    //private Canvas objCanvas;
    //private GraphicsContext objGraphicsContext;
    
    private BufferedImage selecteddata;
    private RadioButton strokeRB, fillRB;
    private Slider sizeSlider;
    private Label hRuler;
    private Label vRuler;
    private Slider hSlider;
    private Slider vSlider;
    private ScrollPane vSliderSP;
    private ScrollPane hSliderSP;
    private ScrollPane designSP;
    private ScrollPane toolSP;
    private GridPane toolGP;
    
    private ContextMenu contextMenu;    
    private StackPane stackPane;
    
    private boolean isNew=true;
    private boolean isEditingMode=false;
    private boolean isWorkingMode = false;
    /**
    * ArtworkViewNew
    * <p>
    * This constructed is used for defining GUI methods for artwork editor.
    *
    * @author       Amit Kumar Singh
    * @version      %I%, %G%
    * @since        1.0
    * @date         07/01/2016
    * @Designing    GUI method class for artwork editor
    * @see          javafx.stage.*;
    * @param        objConfigurationCall Object Configuration 
    */
    public ArtworkViewNew(Configuration objConfigurationCall) {
        objConfiguration = objConfigurationCall;
        //System.err.println("I am in"+objConfiguration.getStrRecentArtwork());
        
        objDictionaryAction = new DictionaryAction(objConfiguration);
        objUR = new UndoRedo();
        //objURF = new UndoRedoFile();
        //objURF.clear();
        
        objArtwork = new Artwork();
        objArtwork.setObjConfiguration(objConfiguration);
        
        artworkStage = new Stage();
        root = new BorderPane();
        scene = new Scene(root, objConfiguration.WIDTH, objConfiguration.HEIGHT, Color.WHITE);
        scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        resolutionControl();
        
        objBufferedImage = new BufferedImage((int)objConfiguration.WIDTH, (int)objConfiguration.HEIGHT, BufferedImage.TYPE_INT_ARGB);
        
        HBox footContainer = new HBox();
        progressB = new ProgressBar(0);
        progressB.setVisible(false);
        progressI = new ProgressIndicator(0);
        progressI.setVisible(false);
        lblStatus = new Label(objDictionaryAction.getWord("WELCOMETOCADTOOL"));
        lblStatus.setId("message");
        footContainer.getChildren().addAll(lblStatus,progressB,progressI);
        footContainer.setId("footContainer");
        root.setBottom(footContainer);
        
        // UI Portion for header content as menu and toolbar
        VBox topContainer = new VBox(); 
        menuBar = new MenuBar();
        menuBar.prefWidthProperty().bind(artworkStage.widthProperty());
        
        toolBar = new ToolBar();
        toolBar.autosize();
        //toolBar.setMinHeight(45);
        menuSelected = new MenuButton();
        menuSelected.setGraphic(new ImageView("/media/text_menu.png"));
        menuSelected.setAlignment(Pos.CENTER_RIGHT);
        menuSelected.setPickOnBounds(false);
        menuSelected.setStyle("-fx-background-color: rgba(0,0,0,0);"); 
        
        AnchorPane toolAnchor = new AnchorPane();
        toolAnchor.getChildren().addAll(menuSelected);
        AnchorPane.setRightAnchor(menuSelected, 1.0);
        toolAnchor.setPickOnBounds(false);
        
        StackPane toolPane=new StackPane();
        toolPane.getChildren().addAll(toolBar,toolAnchor);
        
        topContainer.getChildren().add(menuBar);
        topContainer.getChildren().add(toolPane); 
        topContainer.setId("topContainer");
        root.setTop(topContainer);
        
        //menu bar and events
        homeMenu  = new Menu();
        HBox homeMenuHB = new HBox();
        homeMenuHB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/home.png"),new Label(objDictionaryAction.getWord("HOME")));
        homeMenu.setGraphic(homeMenuHB);
        homeMenu.setAccelerator(new KeyCodeCombination(KeyCode.M, KeyCombination.SHIFT_DOWN));
        homeMenuHB.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                homeMenuAction();  
                me.consume();
            }
        });       
        // File menu - new, save, save as, open, load recent.
        fileMenuLabel = new Label(objDictionaryAction.getWord("FILE"));
        fileMenu = new Menu();
        fileMenu.setGraphic(fileMenuLabel);
        fileMenu.setAccelerator(new KeyCodeCombination(KeyCode.F, KeyCombination.SHIFT_DOWN));
        fileMenuLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                fileMenuAction();            
            }
        });
        // Edit menu - Toolbar, Color, Composite View, Support Lines, Ruler, Zoom-in, zoom-out
        editMenuLabel = new Label(objDictionaryAction.getWord("EDIT"));
        editMenu = new Menu();
        editMenu.setGraphic(editMenuLabel);
        editMenu.setAccelerator(new KeyCodeCombination(KeyCode.E, KeyCombination.SHIFT_DOWN));
        editMenuLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                editMenuAction();
            }
        });  
        // View menu - Toolbar, Color, Composite View, Support Lines, Ruler, Zoom-in, zoom-out
        viewMenuLabel = new Label(objDictionaryAction.getWord("VIEW"));
        viewMenu = new Menu();
        viewMenu.setGraphic(viewMenuLabel);
        viewMenu.setAccelerator(new KeyCodeCombination(KeyCode.V, KeyCombination.SHIFT_DOWN));
        viewMenuLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                viewMenuAction();
            }
        });  
        // Utility menu - Weave, Calculation, Language
        utilityMenuLabel = new Label(objDictionaryAction.getWord("UTILITY"));
        utilityMenu = new Menu();
        utilityMenu.setGraphic(utilityMenuLabel);
        utilityMenu.setAccelerator(new KeyCodeCombination(KeyCode.U, KeyCombination.SHIFT_DOWN));
        utilityMenuLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                utilityMenuAction();
            }
        });
        //Help Menu items
        Label supportMenuLabel = new Label(objDictionaryAction.getWord("SUPPORT"));
        supportMenuLabel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUPPORT")));
        supportMenu = new Menu();
        supportMenu.setGraphic(supportMenuLabel);
        
        MenuItem helpMenuItem = new MenuItem(objDictionaryAction.getWord("HELP"));
        helpMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        helpMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.H, KeyCombination.SHIFT_DOWN));
        helpMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {       
                helpMenuAction();
            }
        });        
        MenuItem technicalMenuItem = new MenuItem(objDictionaryAction.getWord("TECHNICAL"));
        technicalMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/technical_info.png"));
        technicalMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.T, KeyCombination.SHIFT_DOWN));
        technicalMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {       
                technicalMenuAction();
            }
        });
        MenuItem aboutMenuItem = new MenuItem(objDictionaryAction.getWord("ABOUTUS"));
        aboutMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/about_software.png"));
        aboutMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.A, KeyCombination.SHIFT_DOWN));
        aboutMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                aboutMenuAction();
            }
        });
        MenuItem contactMenuItem = new MenuItem(objDictionaryAction.getWord("CONTACTUS"));
        contactMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/contact_us.png"));
        contactMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.SHIFT_DOWN));
        contactMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                contactMenuAction();
            }
        });
        MenuItem logoutMenuItem = new MenuItem(objDictionaryAction.getWord("LOGOUT"));
        logoutMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/logout.png"));
        logoutMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.L, KeyCombination.SHIFT_DOWN));
        logoutMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                logoutMenuAction();
            }
        });
        MenuItem exitMenuItem = new MenuItem(objDictionaryAction.getWord("EXIT"));
        exitMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/quit.png"));
        exitMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.ESCAPE));
        exitMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                exitMenuAction();
            }
        });     
        runMenuLabel = new Label();
        runMenuLabel.setGraphic(new ImageView("/media/run.png"));
        runMenuLabel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPREALTIMEVIEW")));
        runMenu  = new Menu();
        runMenu.setGraphic(runMenuLabel);
        runMenu.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.SHIFT_DOWN));
        runMenuLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                runMenuAction();                
            }
        });
        
        // Weaving Pattern menu - Weave, Calculation, Language
        transformMenuLabel = new Label(objDictionaryAction.getWord("TRANSFORMOPERATIONEDIT"));
        transformMenu = new Menu();
        transformMenu.setGraphic(transformMenuLabel);
        transformMenu.hide();
        transformMenu.setVisible(false);
        transformMenu.setDisable(true);
        transformMenu.setAccelerator(new KeyCodeCombination(KeyCode.U, KeyCombination.SHIFT_DOWN));
        transformMenuLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                transformMenuAction();
            }
        });
        // Paint editing menu - Weave, Calculation, Language
        paintMenuLabel = new Label(objDictionaryAction.getWord("PAINTOPERATIONEDIT"));
        paintMenu = new Menu();
        paintMenu.setGraphic(paintMenuLabel);
        paintMenu.hide();
        paintMenu.setVisible(false);
        paintMenu.setDisable(true);
        paintMenu.setAccelerator(new KeyCodeCombination(KeyCode.U, KeyCombination.SHIFT_DOWN));
        paintMenuLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                paintMenuAction();
            }
        });        
        
        supportMenu.getItems().addAll(helpMenuItem, technicalMenuItem, aboutMenuItem, contactMenuItem, new SeparatorMenuItem(), logoutMenuItem); //exitMenuItem);        
        menuBar.getMenus().addAll(homeMenu, fileMenu, editMenu, viewMenu, utilityMenu, supportMenu, runMenu, transformMenu, paintMenu);
        
        container = new ScrollPane();
        container.setPrefSize(objConfiguration.WIDTH, objConfiguration.HEIGHT);
        //container.setMaxSize(objConfiguration.WIDTH, objConfiguration.HEIGHT);
        
        toolSP = new ScrollPane();
        toolSP.setPrefSize(objConfiguration.WIDTH*.16, objConfiguration.HEIGHT*.80);
        toolSP.getStyleClass().add("subpopup");
        //paletteColorSP.setId("subpopup");
        toolGP=new GridPane();        
        toolGP.setStyle("-fx-background-color:rgb(153, 171, 187); -fx-wrap-text:true;"); 
        //toolGP.setPrefSize(objConfiguration.WIDTH*.11, objConfiguration.HEIGHT*.80);        
        toolSP.setContent(toolGP);
        //SplitPane
        designSP = new ScrollPane();
        designSP.setPrefSize(objConfiguration.WIDTH*.80, objConfiguration.HEIGHT*0.75);
        designSP.getStyleClass().add("subpopup");
        //designSP.setId("subpopup");
        //designSP.setTooltip(new Tooltip(objDictionaryAction.getWord("DESIGNPANE")));
        artwork = new ImageView();
        designSP.setContent(artwork);
        artwork.setPickOnBounds(true);
        
        hSliderSP=new ScrollPane();
        hSliderSP.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        hSliderSP.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        hSlider=new Slider(0, objBufferedImage.getWidth(), 0);
        hSlider.setId("artwork-slider");
        //hSlider.setDisable(true);
        hSlider.setShowTickLabels(true);
        hSlider.setShowTickMarks(true);
        hSlider.setMajorTickUnit(20);
        hSlider.setMinorTickCount(10);
        hSlider.setBlockIncrement(1);
        hSlider.setPrefSize(objBufferedImage.getWidth(), 30);
        hSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                if(t.intValue()==t1.intValue())
                    return;
                if(hSlider.isFocused()){
                    // do not allow drag to zero
                    if(t1.intValue()<1){
                        hSlider.setValue(0);
                        return;
                    }
                    if(t1.intValue()>objBufferedImage.getWidth()){
                        //hSlider.setValue(objBufferedImage.getWidth());
                        return;
                    }                    
                    System.gc();
                }
            }
        });
        //hSlider.setTooltip(new Tooltip(String.valueOf(hSlider.getValue())));
        hSliderSP.setContent(hSlider);
        hSliderSP.setPrefSize(objConfiguration.WIDTH*.80, 30);        
        //hSliderSP.setHmax(objBufferedImage.getWidth());
        
        vSliderSP=new ScrollPane();
        vSliderSP.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        vSliderSP.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        vSlider=new Slider(0, objBufferedImage.getHeight(), 0);
        vSlider.setOrientation(Orientation.VERTICAL);
        vSlider.setId("artwork-slider");
        //vSlider.setDisable(true);
        vSlider.setShowTickLabels(true);
        vSlider.setShowTickMarks(true);
        vSlider.setMajorTickUnit(20);
        vSlider.setMinorTickCount(10);
        vSlider.setBlockIncrement(1);
        vSlider.setPrefSize(30, objBufferedImage.getHeight());
        vSlider.setValue(0);
        vSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                if(t.intValue()==t1.intValue())
                    return;
                if(vSlider.isFocused()){
                    // do not allow drag to zero
                    if(t1.intValue()<1){
                        vSlider.setValue(0);
                        return;
                    }
                    if(t1.intValue()>objBufferedImage.getHeight()){
                        //vSlider.setValue(objBufferedImage.getHeight());
                        return;
                    }
                    System.gc();
                }
            }
        });
        //vSlider.setTooltip(new Tooltip(String.valueOf(vSlider.getValue())));
        vSliderSP.setContent(vSlider);
        vSliderSP.setPrefSize(30, objConfiguration.HEIGHT*0.70);        
        //vSliderSP.setVmax(objBufferedImage.getHeight());
        
        designSP.setVmin(0);
        designSP.setVmax(objBufferedImage.getHeight());
        
        DoubleProperty hdPosition = new SimpleDoubleProperty();
        hdPosition.bind(designSP.hvalueProperty());
        hdPosition.addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                 hSliderSP.setHvalue((double) arg2);
            }
        });
        DoubleProperty hSliderPosition = new SimpleDoubleProperty();
        hSliderPosition.bind(hSliderSP.hvalueProperty());
        hSliderPosition.addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                 designSP.setHvalue((double) arg2);
            }
        });
        DoubleProperty vdPosition = new SimpleDoubleProperty();
        vdPosition.bind(designSP.vvalueProperty());
        vdPosition.addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                 vSliderSP.setVvalue((double) arg2);
            }
        });         
        DoubleProperty vSliderPosition = new SimpleDoubleProperty();
        vSliderPosition.bind(vSliderSP.vvalueProperty());
        vSliderPosition.addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                 designSP.setVvalue((double) arg2);
            }
        });
        
        bodyContainer = new GridPane();
        //bodyContainer.setMinSize(objConfiguration.WIDTH, objConfiguration.HEIGHT);
        bodyContainer.setVgap(0);
        bodyContainer.setHgap(0);
        //bodyContainer.setPadding(new Insets(10));
        
        bodyContainer.add(vSliderSP,0,0,1,1); // row 0 col 0
        bodyContainer.add(hSliderSP,1,1,1,1); // row 1 col 1        
        bodyContainer.add(designSP,1,0,1,1);  // row 0 col 1
        bodyContainer.add(toolSP, 2,0,1,2); /// row 0 col 2
               
        /*
        objCanvas = new Canvas();
        objGraphicsContext = objCanvas.getGraphicsContext2D();
        //scrollPane.setContent(objCanvas);
        
        stackPane = new StackPane();
        stackPane.getChildren().addAll(artwork);//,objCanvas);
        container.setContent(stackPane);
        root.setCenter(container);
        */
        
        //setting context menus
        addContextMenu(); 
        //adding right pane menus
        addContextMenu(); 
        // Code Added for ShortCuts
        addAccelratorKey();
        //Manage Events
        addArtworkEventHandler();
        
        //bodyContainer.setStyle("-fx-padding: 10; -fx-background-color: #5F5E5E;");Id("popup");
        bodyContainer.setAlignment(Pos.CENTER);
        container.setContent(bodyContainer);
        root.setCenter(container);
        
        selectedMenu = "FILE";
        menuHighlight();
        artworkStage.getIcons().add(new Image("/media/icon.png"));
        artworkStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWARTWORKEDITOR")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
        artworkStage.setX(-5);
        artworkStage.setY(0);
        artworkStage.setResizable(false);
        artworkStage.setScene(scene);
        artworkStage.show();
        artworkStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                homeMenuAction(); 
                we.consume();
            }
        });
        //Managing contxt flow
        /*if(objConfiguration.getStrRecentArtwork()!=null && objConfiguration.strWindowFlowContext.equalsIgnoreCase("Dashboard")){
            recentArtwork();
        }else{
            newArtwork();
        }*/
        if(objConfiguration.getStrRecentArtwork()!=null && objConfiguration.strWindowFlowContext.equalsIgnoreCase("Dashboard")){
            //System.err.println("I m in here too");
            objArtwork = new Artwork();
            objArtwork.setStrArtworkId(objConfiguration.getStrRecentArtwork());
            objArtwork.setObjConfiguration(objConfiguration);
            loadArtwork();
        }
    }
/**
* resolutionControl(Stage)
* <p>
* This function is used for reassign width and height. 
*  
* @param       Stage primaryStage
* @author      Amit Kumar Singh
* @version     %I%, %G%
* @since       1.0
* @see         javafx.stage.*;
* @link        WindowView
*/
    private void resolutionControl(){
        //windowStage.resizableProperty().addListener(listener);
        scene.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                setGlobalWidthHeight();
                artworkStage.setHeight(objConfiguration.HEIGHT);
                artworkStage.setWidth(objConfiguration.WIDTH);
                //selectedMenu = "FILE";
                menuHighlight();
            }
        });
        scene.heightProperty().addListener(new ChangeListener<Number>() {
            @Override 
            public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneHeight, Number newSceneHeight) {
                setGlobalWidthHeight();
                artworkStage.setHeight(objConfiguration.HEIGHT);
                artworkStage.setWidth(objConfiguration.WIDTH);
                //selectedMenu = "FILE";
                menuHighlight();
            }
        });
    }
    private void setGlobalWidthHeight(){
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        objConfiguration.WIDTH = screenSize.getWidth();
        objConfiguration.HEIGHT = screenSize.getHeight();
        objConfiguration.strIconResolution = (objConfiguration.WIDTH<1280)?"hd_none":(objConfiguration.WIDTH<1920)?"hd":(objConfiguration.WIDTH<2560)?"hd_full":"hd_quad";
    }
    /**
     * addContextMenu
     * <p>
     * Function use for drawing menu bar for menu item,
     * and binding events for each menus with style. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */
    private void addContextMenu(){
        contextMenu = new ContextMenu();
        try{
            UtilityAction objUtilityAction = new UtilityAction();
            Device objDevice = new Device(null, "Designing S/W", null, null);
            objDevice.setObjConfiguration(objConfiguration);
            Device[] devices = objUtilityAction.getDevices(objDevice);
            contextMenu = new ContextMenu();
            for(int i=0; i<devices.length; i++){
                final MenuItem editApplication = new MenuItem(devices[i].getStrDeviceName());
                editApplication.setUserData(devices[i].getStrDeviceId());
                editApplication.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/application_integration.png"));
                contextMenu.getItems().add(editApplication);
                editApplication.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        lblStatus.setText(objDictionaryAction.getWord("ARTWORKEDIT"));
                        try {
                            UtilityAction objUtilityAction = new UtilityAction();
                            Device objDevice = new Device(editApplication.getUserData().toString(), null, editApplication.getText(), null);
                            objDevice.setObjConfiguration(objConfiguration);
                            objUtilityAction.getDevice(objDevice);
                            if(objDevice.getStrDevicePath()!=null){
                                Runtime rt = Runtime.getRuntime();
                                if(objArtwork.getObjConfiguration().getObjProcessProcessBuilder()!=null)
                                    objArtwork.getObjConfiguration().getObjProcessProcessBuilder().destroy();
                                File file = new File(objDevice.getStrDevicePath());
                                if(file.exists() && !file.isDirectory()) { 
                                    if(objBufferedImage!=null){
                                        filePath = System.getProperty("user.dir") + "/mla/temp/tempdesign.png";
                                        filePath = filePath.replace("\\","/");
                                        refreshFileBtn.setDisable(false);
                                        refreshFileBtn.setCursor(Cursor.HAND);
                                        file = new File(filePath);
                                        ImageIO.write(objBufferedImage, "png", file);
                                        objArtwork.getObjConfiguration().setObjProcessProcessBuilder(new ProcessBuilder(objDevice.getStrDevicePath(),file.getAbsolutePath()).start());
                                    }else{
                                        objArtwork.getObjConfiguration().setObjProcessProcessBuilder(new ProcessBuilder(objDevice.getStrDevicePath()).start());
                                        importArtwork();
                                    }
                                }else{
                                    lblStatus.setText(objDictionaryAction.getWord("NOITEM"));
                                }
                            }else{
                                lblStatus.setText(objDictionaryAction.getWord("NOVALUE"));
                            }
                        } catch (SQLException ex) {
                            new Logging("SEVERE",getClass().getName(),"SQLException:Operation edit artwork"+ex.toString(),ex);
                            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                        } catch (IOException ex) {
                            new Logging("SEVERE",getClass().getName(),"IOException:Operation edit artwork"+ex.toString(),ex);
                            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                        } catch (Exception ex) {
                            new Logging("SEVERE",getClass().getName(),"Exception:Operation edit artwork"+ex.toString(),ex);
                            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                        }
                    }
                });
            }
            container.setContextMenu(contextMenu);
            //contextMenu.hide();
            container.setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {
                @Override
                public void handle(ContextMenuEvent event) {
                    if(objBufferedImage==null || plotEditActionMode > 1){
                        contextMenu.hide();
                        event.consume();
                        //System.err.println(editThreadPrimaryValue+"=isPrimaryButtonDown"+editThreadSecondaryValue);
                    }
                }
            });
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"artwork window close",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"artwork window close",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    /**
     * addAccelratorKey
     * <p>
     * Function use for adding shortcut key combinations. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */
    private void addAccelratorKey(){
        // shortcuts variable names: kc + Alphabet + ALT|CONTROL|SHIFT // ACS alphabetical
        final KeyCodeCombination homeMenu = new KeyCodeCombination(KeyCode.M, KeyCombination.SHIFT_DOWN); // Home Menu
        final KeyCodeCombination fileMenu = new KeyCodeCombination(KeyCode.F, KeyCombination.SHIFT_DOWN); // File Menu
        final KeyCodeCombination editMenu = new KeyCodeCombination(KeyCode.E, KeyCombination.SHIFT_DOWN); // Edit Menu
        final KeyCodeCombination viewMenu = new KeyCodeCombination(KeyCode.V, KeyCombination.SHIFT_DOWN); // View Menu
        final KeyCodeCombination utilityMenu = new KeyCodeCombination(KeyCode.U, KeyCombination.SHIFT_DOWN); // Utility Menu
        //utility menu items
        final KeyCodeCombination weaveUtility = new KeyCodeCombination(KeyCode.W, KeyCombination.SHIFT_DOWN); // weave
        final KeyCodeCombination fabricUtility = new KeyCodeCombination(KeyCode.J, KeyCombination.SHIFT_DOWN); // artwork
        final KeyCodeCombination clothUtility = new KeyCodeCombination(KeyCode.G, KeyCombination.SHIFT_DOWN); // cloth
        final KeyCodeCombination layoutUtility = new KeyCodeCombination(KeyCode.C, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN); // custom layout
        final KeyCodeCombination deviceUtility = new KeyCodeCombination(KeyCode.N, KeyCombination.SHIFT_DOWN); // punch application
        //view menu items
        final KeyCodeCombination frontSideView = new KeyCodeCombination(KeyCode.F1, KeyCombination.CONTROL_DOWN); // front side
        final KeyCodeCombination rearSideView = new KeyCodeCombination(KeyCode.F2, KeyCombination.CONTROL_DOWN); // rear side
        final KeyCodeCombination frontVisualizationView = new KeyCodeCombination(KeyCode.F4, KeyCombination.CONTROL_DOWN); // front visualization
        final KeyCodeCombination rearVisualizationView = new KeyCodeCombination(KeyCode.F5, KeyCombination.CONTROL_DOWN); // rear visualization
        final KeyCodeCombination flipVisualizationView = new KeyCodeCombination(KeyCode.F6, KeyCombination.CONTROL_DOWN); // flip visualization
        final KeyCodeCombination frontCrossSectionView = new KeyCodeCombination(KeyCode.F7, KeyCombination.CONTROL_DOWN); // front cut
        final KeyCodeCombination rearCrossSectionView = new KeyCodeCombination(KeyCode.F8, KeyCombination.CONTROL_DOWN); // rear cut
        final KeyCodeCombination gridView = new KeyCodeCombination(KeyCode.F10, KeyCombination.CONTROL_DOWN); // grid
        final KeyCodeCombination graphView = new KeyCodeCombination(KeyCode.F11, KeyCombination.CONTROL_DOWN); // graph 
        final KeyCodeCombination tilledView = new KeyCodeCombination(KeyCode.F12, KeyCombination.CONTROL_DOWN); // tilled view
        final KeyCodeCombination simulationView = new KeyCodeCombination(KeyCode.F4, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // simulation
        final KeyCodeCombination mappingView = new KeyCodeCombination(KeyCode.F2, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // mapping
        final KeyCodeCombination zoomInView = new KeyCodeCombination(KeyCode.EQUALS, KeyCombination.CONTROL_DOWN); // zoom in
        final KeyCodeCombination normalView = new KeyCodeCombination(KeyCode.ENTER, KeyCombination.CONTROL_DOWN); // zoom normal
        final KeyCodeCombination zoomOutView = new KeyCodeCombination(KeyCode.MINUS, KeyCombination.CONTROL_DOWN); // zoom out
        final KeyCodeCombination zoomInAlternateView = new KeyCodeCombination(KeyCode.ADD, KeyCombination.CONTROL_DOWN); // zoom in
        final KeyCodeCombination zoomOutAlternateView = new KeyCodeCombination(KeyCode.SUBTRACT, KeyCombination.CONTROL_DOWN); // zoom out
        //Edit menu items
        final KeyCodeCombination undoEdit = new KeyCodeCombination(KeyCode.Z, KeyCombination.CONTROL_DOWN); // undo
        final KeyCodeCombination redoEdit = new KeyCodeCombination(KeyCode.Y, KeyCombination.CONTROL_DOWN); // redo        
        final KeyCodeCombination resizeEdit = new KeyCodeCombination(KeyCode.R, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // resize
        final KeyCodeCombination repeatEdit = new KeyCodeCombination(KeyCode.R, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN); // repeats
        final KeyCodeCombination jacquardConversionEdit = new KeyCodeCombination(KeyCode.J, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // artwork assingment
        final KeyCodeCombination graphCorrectionEdit = new KeyCodeCombination(KeyCode.G, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN); // edit graph
        final KeyCodeCombination colorPropertiesEdit = new KeyCodeCombination(KeyCode.K, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // color adjust
        final KeyCodeCombination reduceColorEdit = new KeyCodeCombination(KeyCode.K, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN); // color reduce
        final KeyCodeCombination sketchEdit = new KeyCodeCombination(KeyCode.O, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN); // sketch
        final KeyCodeCombination pencilToolEdit = new KeyCodeCombination(KeyCode.P, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN); // pencil
        final KeyCodeCombination eraserToolEdit = new KeyCodeCombination(KeyCode.E, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN); // eraser
        final KeyCodeCombination sprayToolEdit = new KeyCodeCombination(KeyCode.Q, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN); // spray
        final KeyCodeCombination fillToolEdit = new KeyCodeCombination(KeyCode.F, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN); // fill
        final KeyCodeCombination transformOperationEdit = new KeyCodeCombination(KeyCode.W, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // weaving pattern
        //File menu items
        final KeyCodeCombination newFile = new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN); // new
        final KeyCodeCombination openFile = new KeyCodeCombination(KeyCode.O, KeyCombination.CONTROL_DOWN); // open
        final KeyCodeCombination loadFile = new KeyCodeCombination(KeyCode.L, KeyCombination.CONTROL_DOWN); // load recent
        final KeyCodeCombination importFile = new KeyCodeCombination(KeyCode.I, KeyCombination.CONTROL_DOWN); // import
        final KeyCodeCombination applicationFile = new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // paint application
        final KeyCodeCombination refreshFile = new KeyCodeCombination(KeyCode.I, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // import refresh
        final KeyCodeCombination saveFile = new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN); // save
        final KeyCodeCombination saveAsFile = new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // save as
        final KeyCodeCombination exportFile = new KeyCodeCombination(KeyCode.E, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // export as Texture
        final KeyCodeCombination saveTxtFile = new KeyCodeCombination(KeyCode.F, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // save txt file
        final KeyCodeCombination saveGridFile = new KeyCodeCombination(KeyCode.H, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // save as grid
        final KeyCodeCombination saveGraphFile = new KeyCodeCombination(KeyCode.G, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // save as graph
        final KeyCodeCombination printFile = new KeyCodeCombination(KeyCode.P, KeyCombination.CONTROL_DOWN); // print
        
        scene.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                if(homeMenu.match(t))
                    homeMenuAction();
                else if(fileMenu.match(t) && plotEditActionMode != 4)
                    fileMenuAction();
                else if(editMenu.match(t) && plotEditActionMode != 4)
                    editMenuAction();
                else if(viewMenu.match(t) && plotEditActionMode != 4)
                    viewMenuAction();
                else if(utilityMenu.match(t) && plotEditActionMode != 4)
                    utilityMenuAction();
                else if(weaveUtility.match(t) && isWorkingMode && plotEditActionMode != 4)
                    weaveUtilityAction();
                else if(fabricUtility.match(t) && isWorkingMode && plotEditActionMode != 4)
                    fabricUtilityAction();
                else if(clothUtility.match(t) && isWorkingMode && plotEditActionMode != 4)
                    clothUtilityAction();
                else if(layoutUtility.match(t))
                    layoutUtilityAction();
                else if(deviceUtility.match(t) && isWorkingMode && plotEditActionMode != 4)
                    punchApplicationAction();
                else if(frontSideView.match(t) && isWorkingMode && plotEditActionMode != 4)
                    frontSideViewAction();
                else if(frontVisualizationView.match(t) && isWorkingMode && plotEditActionMode != 4)
                    frontVisualizationViewAction();
                else if(frontCrossSectionView.match(t) && isWorkingMode && plotEditActionMode != 4)
                    frontCutViewAction();
                else if(gridView.match(t) && isWorkingMode && plotEditActionMode != 4)
                    gridViewAction();
                else if(graphView.match(t) && isWorkingMode && plotEditActionMode != 4)
                    graphViewAction();
                else if(tilledView.match(t) && isWorkingMode && plotEditActionMode != 4)
                    tilledViewAction();
                else if(simulationView.match(t) && isWorkingMode && plotEditActionMode != 4)
                    simulationViewAction();
                /*else if(mappingView.match(t) && isWorkingMode && plotEditActionMode != 4)
                    mappingViewAction();*/
                else if(zoomInView.match(t) || zoomInAlternateView.match(t))
                    zoomInAction();
                else if(normalView.match(t))
                    normalAction();
                else if(zoomOutView.match(t) || zoomOutAlternateView.match(t))
                    zoomOutAction();
                else if(undoEdit.match(t) && isWorkingMode && plotEditActionMode != 4)
                    undoAction();
                else if(redoEdit.match(t) && isWorkingMode && plotEditActionMode != 4)
                    redoAction();
                 else if(jacquardConversionEdit.match(t) && isWorkingMode && plotEditActionMode != 4)
                    jacquardConverstionAction();
                else if(graphCorrectionEdit.match(t) && isWorkingMode && plotEditActionMode != 4)
                    graphCorrectionAction();
                else if(resizeEdit.match(t) && isWorkingMode && plotEditActionMode != 4)
                    resizeArtwork();
                else if(repeatEdit.match(t) && isWorkingMode && plotEditActionMode != 4)
                    repeatOrientationAction();
                else if(colorPropertiesEdit.match(t) && isWorkingMode && plotEditActionMode != 4)
                    colorPropertiesAction();
                else if(reduceColorEdit.match(t) && isWorkingMode && plotEditActionMode != 4)
                    colorReductionAction();
                else if(sketchEdit.match(t) && isWorkingMode && plotEditActionMode != 4)
                    artworkSketch();
                else if(pencilToolEdit.match(t) && isWorkingMode && plotEditActionMode != 4)
                    pencilToolAction();
                else if(eraserToolEdit.match(t) && isWorkingMode && plotEditActionMode != 4)
                    eraserToolAction();
                else if(sprayToolEdit.match(t) && isWorkingMode && plotEditActionMode != 4)
                    sprayToolAction();                
                else if(fillToolEdit.match(t) && isWorkingMode && plotEditActionMode != 4)
                    fillToolAction();                
                else if(transformOperationEdit.match(t) && isWorkingMode && plotEditActionMode != 4)
                    transformOperationAction();
                else if(newFile.match(t) && plotEditActionMode != 4)
                    newArtwork();
                else if(openFile.match(t) && plotEditActionMode != 4)
                    openArtwork();
                else if(loadFile.match(t) && plotEditActionMode != 4)
                    recentArtwork();
                else if(importFile.match(t) && plotEditActionMode != 4)
                    importArtwork();
                else if(applicationFile.match(t) && plotEditActionMode != 4)
                    artworkApplication();
                else if(refreshFile.match(t) && plotEditActionMode != 4)
                    refreshArtwork();
                else if(saveFile.match(t) && isWorkingMode && !isNew && plotEditActionMode != 4)
                    saveArtwork();
                else if(saveAsFile.match(t) && isWorkingMode && plotEditActionMode != 4)
                    saveAsArtwork();
                else if(exportFile.match(t) && isWorkingMode && plotEditActionMode != 4)
                    exportArtwork();
                else if(saveTxtFile.match(t) && isWorkingMode && plotEditActionMode != 4)
                    exportText();
                else if(saveGridFile.match(t) && isWorkingMode && plotEditActionMode != 4)
                    exportGrid();
                else if(saveGraphFile.match(t) && isWorkingMode && plotEditActionMode != 4)
                    exportGraph();
                else if(printFile.match(t) && isWorkingMode && plotEditActionMode != 4)
                    printMenuAction();
            }
        });
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                KeyCode key = t.getCode();
                if (key == KeyCode.DOWN){
                     zoomOutAction();
                } else if (key == KeyCode.UP){
                     zoomInAction();
                } else if (key == KeyCode.ENTER){
                     normalAction();
                }
            }
        });    
    }
    private void setCurrentShape() {
        /* others = 1, editJaqurad = 2, editGraph = 3, editTransform = 4, editColor = 5, spraytool = 9, filltool = 10, freehanddesign = 11, eraser = 12, 
        drawline = 13, drawarc = 14, beziercurve = 15, drawoval = 16, drawrectangle = 17, drawtriangle = 18, drawrighttriangle = 19, drawdiamond = 20,
        drawpentagon = 21, drawhexagone = 22, drawheart = 23, drawfourpointstar = 34, drawfivepointstar = 25, drawsixpointstar = 26, drawploygon = 27,
        selectdata = 30, noselectdata = 31, dragselectdata = 32, copydata = 33, pastedata = 34, cutdata = 35,
        mirrorverticaldata = 36, mirrorhorizentadata = 37, rotateclockwisedata = 38, rotateanticlockwisedata = 39;
        moveleft = 40, move right = 41, moveup = 42, movedown = 43, tiltleft = 44, tiltright = 45, tiltup = 46, tiltdown = 47 */
        plotEditActionMode = 0;
    }
    /**
     * addArtworkEventHandler
     * <p>
     * Function use for drawing menu bar for menu item,
     * and binding events for each menus with style. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */
    private void addArtworkEventHandler(){
        artwork.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                //System.out.println("Clicked");
                onMouseClickedListener(event);
            }
        });
        artwork.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>(){ 
            @Override
            public void handle(MouseEvent event) {
                //System.out.println("Presed");
                onMousePressedListener(event);
            }
        });
        artwork.addEventHandler(MouseEvent.MOUSE_DRAGGED, new EventHandler<MouseEvent>(){ 
            @Override
            public void handle(MouseEvent event) {
                //System.out.println("Dragged");
                onMouseDraggedListener(event);
            }
        });
        artwork.addEventHandler(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>(){ 
            @Override
            public void handle(MouseEvent event) {
                //System.out.println("Relesed");
                onMouseReleaseListener(event);
            }
        });
    }    
    private void onMouseClickedListener(MouseEvent e){
        //System.err.println("mouse clicked");
        this.startX = e.getX();
        this.startY = e.getY();
        this.oldX = e.getX();
        this.oldY = e.getY();
        this.lastX = e.getX();
        this.lastY = e.getY();
        
        //artworkEditEvent(e);
        if(plotEditActionMode == 2){ // editJacquard = 2
            //objURF.store(objBufferedImage);
            artworkWeaveAssingmentEvent(e);
            objUR.doCommand("Weave Fill Tool", deepCopy(objBufferedImage));
        } else if(plotEditActionMode == 3){// editGraph = 3
            //objURF.store(objBufferedImage);
            objBufferedImage=deepCopy(objBufferedImage);
            artworkGraphEditEvent(e);
            objUR.doCommand("Edit Graph", deepCopy(objBufferedImage));
        } else if(plotEditActionMode==9){ //spray = 11
            //objURF.store(objBufferedImage);
            objBufferedImage=deepCopy(objBufferedImage);
            plotEditImage();
            artworkSprayToolEvent(e);
            objUR.doCommand("Edit Pencil Tool", deepCopy(objBufferedImage));
        } else if(plotEditActionMode == 10){ // filltool = 10
            //objURF.store(objBufferedImage);
            artworkFillToolEvent(e);
            objUR.doCommand("Edit Color Fill Tool", deepCopy(objBufferedImage));
        } else if(plotEditActionMode==11){ //pencil = 11
            //objURF.store(objBufferedImage);
            objBufferedImage=deepCopy(objBufferedImage);
            plotEditImage();
            artworkPencilToolEvent(e);
            objUR.doCommand("Edit Pencil Tool", deepCopy(objBufferedImage));
        } else if(plotEditActionMode==12){ //eraser = 12
            //objURF.store(objBufferedImage);
            objBufferedImage=deepCopy(objBufferedImage);
            plotEditImage();
            artworkEraserToolEvent(e);
            objUR.doCommand("Edit Eraser Tool", deepCopy(objBufferedImage));
        }
    }
    private void onMousePressedListener(MouseEvent e){
        //System.err.println("mouse pressed");
        this.startX = e.getX();
        this.startY = e.getY();
        this.oldX = e.getX();
        this.oldY = e.getY();
    }
    private void onMouseDraggedListener(MouseEvent e){
        //System.err.println("mouse draged");
        this.lastX = e.getX();
        this.lastY = e.getY();
        
        if(plotEditActionMode == 3){// editGraph = 3
            objBufferedImage=deepCopy(objBufferedImage);
            artworkGraphEditEvent(e);
        } else if(plotEditActionMode==11){ //pencil = 11
            objBufferedImage=deepCopy(objBufferedImage);
            plotEditImage();
            artworkPencilToolEvent(e);
        } else if(plotEditActionMode==12){ //eraser = 12
            objBufferedImage=deepCopy(objBufferedImage);
            plotEditImage();
            artworkEraserToolEvent(e);
        }
    }
    private void onMouseReleaseListener(MouseEvent e){
        //System.err.println("mouse released");
        if(plotEditActionMode == 3){// editGraph = 3
            //objUR.doCommand("Edit Graph", deepCopy(objBufferedImage));            
        } else if(plotEditActionMode==11){ //pencil = 11
            //objUR.doCommand("Edit Graph", deepCopy(objBufferedImage));
        } else if(plotEditActionMode==12){ //eraser = 12
            //objUR.doCommand("Edit Graph", deepCopy(objBufferedImage));
        }
        /*
        if(drawsixpointstar)
            drawSixPointStar();
        if(drawfivepointstar)
            drawFivePointStar();
        if(drawfourpointstar)
            drawFourPointStar();
        if(drawhexagone)
            drawHexagon();
        if(drawpentagon)
            drawPentagon();
        if(drawheart)
            drawHeart();
        if(drawdiamond)
            drawDiamond();
        if(drawtriangle)
            drawTriangle();
        if(drawrectangle)
            drawRect();
        if(drawoval)
            drawOval();
        if(beziercurve)
            drawBezierCurve();
        if(drawarc)
            drawArc();
        if(drawline)
            drawLine();
        
        if(selectdata)
            drawSelectData();
        if(mirrorverticaldata)
            freeDrawing();
        if(mirrorhorizentadata)
            freeDrawing();
        if(rotateclockwisedata)
            drawRotateClockWise();
        if(rotateanticlockwisedata)
            drawRotateAntiClockWise();
        */
    }
    /**
     * homeMenuAction
     * <p>
     * Function use for exit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */
    public void homeMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHOME"));
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.setIconified(false);
        dialogStage.setFullScreen(false);
        dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+":"+objDictionaryAction.getWord("ALERT"));
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 300, 100, Color.WHITE);
        scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        final GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        popup.add(new ImageView("/media/alert_warning.png"), 0, 0); 
        Label lblAlert = new Label(objDictionaryAction.getWord("ALERTCLOSE"));
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(250);
        popup.add(lblAlert, 1, 0);
        Button btnYes = new Button(objDictionaryAction.getWord("YES"));
        btnYes.setPrefWidth(50);
        btnYes.setId("btnYes");
        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                artworkStage.close();
                System.gc();
                WindowView objWindowView = new WindowView(objConfiguration);
            }
        });
        popup.add(btnYes, 0, 1);
        Button btnNo = new Button(objDictionaryAction.getWord("NO"));
        btnNo.setPrefWidth(50);
        btnNo.setId("btnNo");
        btnNo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
            }
        });
        popup.add(btnNo, 1, 1);
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.showAndWait();
    }
    /**
     * fileMenuAction
     * <p>
     * Function use for file menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */
    private void fileMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONFILE"));
        selectedMenu = "FILE";
        menuHighlight();
    }
    /**
     * editMenuAction
     * <p>
     * Function use for edit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */
    private void editMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONEDIT"));
        selectedMenu = "EDIT";
        menuHighlight();
    }          
    /**
     * viewMenuAction
     * <p>
     * Function use for view menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */
    private void viewMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONVIEW"));
        selectedMenu = "VIEW";
        menuHighlight();
    }
    /**
     * utilityMenuAction
     * <p>
     * Function use for utility menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */
    private void utilityMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("TOOLTIPUTILITY"));
        selectedMenu = "UTILITY";
        menuHighlight();
    }
    /**
     * helpMenuAction
     * <p>
     * Function use for help menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */
    private void helpMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHELP"));
        HelpView objHelpView = new HelpView(objConfiguration);
    }
    /**
     * technicalMenuAction
     * <p>
     * Function use for technical menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */
    private void technicalMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONTECHNICAL"));
        TechnicalView objTechnicalView = new TechnicalView(objConfiguration);
    }
    /**
     * aboutMenuAction
     * <p>
     * Function use for about menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */
    private void aboutMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONABOUTUS"));
        AboutView objAboutView = new AboutView(objConfiguration);
    }
    /**
     * contactMenuAction
     * <p>
     * Function use for contact menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */
    public void contactMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCONTACTUS"));
        ContactView objContactView = new ContactView(objConfiguration);
    }
    /**
     * logoutMenuAction
     * <p>
     * Function use for exit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void logoutMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHOME"));
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.setIconified(false);
        dialogStage.setFullScreen(false);
        dialogStage.setTitle(objDictionaryAction.getWord("ALERT"));
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 300, 100, Color.WHITE);
        scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        popup.add(new ImageView("/media/alert_warning.png"), 0, 0);   
        Label lblAlert = new Label(objDictionaryAction.getWord("ALERTCLOSE"));
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(250);
        popup.add(lblAlert, 1, 0);
        Button btnYes = new Button(objDictionaryAction.getWord("YES"));
        btnYes.setPrefWidth(50);
        btnYes.setId("btnYes");
        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                objConfiguration.setStrRecentArtwork(null);
                dialogStage.close();
                artworkStage.close();
                System.gc();
                UserLoginView objUserLoginView = new UserLoginView(objConfiguration);
            }
        });
        popup.add(btnYes, 0, 1);
        Button btnNo = new Button(objDictionaryAction.getWord("NO"));
        btnNo.setPrefWidth(50);
        btnNo.setId("btnNo");
        btnNo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
            }
        });
        popup.add(btnNo, 1, 1);
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.showAndWait();
    }
    /**
     * exitMenuAction
     * <p>
     * Function use for exit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */
    public void exitMenuAction(){
        System.gc();
        artworkStage.close();
    }
    private void runMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONREALTIMEVIEW"));
        if(isWorkingMode){
            try {
                final Stage dialogStage = new Stage();
                dialogStage.initOwner(artworkStage);
                dialogStage.initStyle(StageStyle.UTILITY);
                //dialogStage.initModality(Modality.WINDOW_MODAL);
                GridPane popup=new GridPane();
                popup.setId("popup");
                popup.setAlignment(Pos.CENTER);
                popup.setHgap(5);
                popup.setVgap(5);
                popup.setPadding(new Insets(25, 25, 25, 25));
                
                int intHeight = (int)objBufferedImage.getHeight();
                int intLength = (int)objBufferedImage.getWidth();                
                BufferedImage objBufferedImageResize = new BufferedImage(111, 111, BufferedImage.TYPE_INT_RGB);
                Graphics2D g = objBufferedImageResize.createGraphics();
                objArtworkAction = new ArtworkAction();
                // Front Texture View item;
                g.drawImage(objBufferedImage, 0, 0, 111, 111, null);
                g.dispose();
                //ImageIO.write(objBufferedImageesize, "png", new File(System.getProperty("user.dir")+"/mla/tempmenu_composite.png"));
                VBox frontSideViewVB = new VBox(); 
                frontSideViewVB.getChildren().add(new ImageView(SwingFXUtils.toFXImage(objBufferedImageResize, null)));
                frontSideViewVB.getChildren().add(new Label("Ctrl+F1"));
                frontSideViewVB.setPrefWidth(111);
                frontSideViewVB.getStyleClass().addAll("myBox");    
                Tooltip frontSideViewTT = new Tooltip(objDictionaryAction.getWord("FRONTSIDEVIEW")+" (Ctrl+F1)\n"+objDictionaryAction.getWord("TOOLTIPFRONTSIDEVIEW"));
                frontSideViewTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_view.png"));
                Tooltip.install(frontSideViewVB, frontSideViewTT);
                frontSideViewVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        frontSideViewAction();
                    }
                });
                popup.add(frontSideViewVB, 0, 0);
                
                // Front Visulization View item;
                g = objBufferedImageResize.createGraphics();
                g.drawImage(objArtworkAction.plotVisualizationFrontView(objBufferedImage, intLength, intHeight), 0, 0, 111, 111, null);
                g.dispose();
                //ImageIO.write(objBufferedImageesize, "png", new File(System.getProperty("user.dir")+"/mla/tempmenu_composite.png"));
                VBox frontVisualizationViewVB = new VBox(); 
                frontVisualizationViewVB.getChildren().add(new ImageView(SwingFXUtils.toFXImage(objBufferedImageResize, null)));
                frontVisualizationViewVB.getChildren().add(new Label("Ctrl+F4"));
                frontVisualizationViewVB.setPrefWidth(111);
                frontVisualizationViewVB.getStyleClass().addAll("myBox");        
                Tooltip frontVisualizationViewTT = new Tooltip(objDictionaryAction.getWord("VISUALIZATIONFRONTVIEW")+" (Ctrl+F4)\n"+objDictionaryAction.getWord("TOOLTIPVISUALIZATIONFRONTVIEW"));
                frontVisualizationViewTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_visualization.png"));
                Tooltip.install(frontVisualizationViewVB, frontVisualizationViewTT);
                frontVisualizationViewVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        frontVisualizationViewAction();
                    }
                });
                popup.add(frontVisualizationViewVB, 1, 0);
                
                // cross Section View item;
                List lstLines = new ArrayList();
                ArrayList<Integer> lstEntry = null;
                int lineCount = 0;
                for (int i = 0; i < intHeight; i++){
                    lstEntry = new ArrayList();
                    for (int j = 0; j < intLength; j++){
                        int rgb = objBufferedImage.getRGB(j, i);
                        //add the first color on array
                        if(lstEntry.size()==0)
                            lstEntry.add(rgb);
                        //check for redudancy
                        else {                
                            if(!lstEntry.contains(rgb))
                                lstEntry.add(rgb);
                        }
                    }
                    lstLines.add(lstEntry);
                    lineCount+=lstEntry.size();
                }
                BufferedImage objBufferedImageesize = new BufferedImage(intLength, lineCount, BufferedImage.TYPE_INT_RGB);
                lineCount =0;
                int init = 0;
                for (int i = 0 ; i < intHeight; i++){
                    lstEntry = (ArrayList)lstLines.get(i);
                    for(int k=0; k<lstEntry.size(); k++, lineCount++){
                        init = (Integer)lstEntry.get(k);
                        for (int j = 0; j < intLength; j++){
                            if(init==objBufferedImage.getRGB(j, i)){
                                objBufferedImageesize.setRGB(j, lineCount, init);
                            }else{
                                objBufferedImageesize.setRGB(j, lineCount, -1);
                            }   
                        }
                    }
                }                
                g = objBufferedImageResize.createGraphics();
                g.drawImage(objBufferedImageesize, 0, 0, intLength, intHeight, null);
                g.dispose();
                objBufferedImageesize = null; 
                //ImageIO.write(objBufferedImageesize, "png", new File(System.getProperty("user.dir")+"/mla/tempmenu_composite.png"));
                VBox frontCrossSectionViewVB = new VBox(); 
                frontCrossSectionViewVB.getChildren().add(new ImageView(SwingFXUtils.toFXImage(objBufferedImageResize, null)));
                frontCrossSectionViewVB.getChildren().add(new Label("Ctrl+F7"));
                frontCrossSectionViewVB.setPrefWidth(111);
                frontCrossSectionViewVB.getStyleClass().addAll("myBox");        
                Tooltip frontCrossSectionViewTT = new Tooltip(objDictionaryAction.getWord("CROSSSECTIONFRONTVIEW")+" (Ctrl+F7)\n"+objDictionaryAction.getWord("TOOLTIPCROSSSECTIONFRONTVIEW"));
                frontCrossSectionViewTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_cut.png"));
                Tooltip.install(frontCrossSectionViewVB, frontCrossSectionViewTT);
                frontCrossSectionViewVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        frontCutViewAction();
                    }
                });
                popup.add(frontCrossSectionViewVB, 2, 0);
                                
                // Grid View item
                g = objBufferedImageResize.createGraphics();
                g.drawImage(getArtworkGrid(objBufferedImageResize), 0, 0, 111, 111, null);
                g.dispose();
                VBox gridViewVB = new VBox(); 
                gridViewVB.getChildren().add(new ImageView(SwingFXUtils.toFXImage(objBufferedImageResize, null)));
                gridViewVB.getChildren().add(new Label("Ctrl+F10"));
                gridViewVB.setPrefWidth(111);
                gridViewVB.getStyleClass().addAll("myBox");        
                Tooltip gridViewTT = new Tooltip(objDictionaryAction.getWord("GRIDVIEW")+" (Ctrl+F10)\n"+objDictionaryAction.getWord("TOOLTIPGRIDVIEW"));
                gridViewTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/grid_view.png"));
                Tooltip.install(gridViewVB, gridViewTT);
                gridViewVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        frontVisualizationViewAction();
                    }
                });
                popup.add(gridViewVB, 0, 1);
                
                // Graph View item
                g = objBufferedImageResize.createGraphics();
                g.drawImage(getArtworkGraph(objBufferedImageResize), 0, 0, 111, 111, null);
                g.dispose();
                VBox graphViewVB = new VBox(); 
                graphViewVB.getChildren().add(new ImageView(SwingFXUtils.toFXImage(objBufferedImageResize, null)));
                graphViewVB.getChildren().add(new Label("Ctrl+F11"));
                graphViewVB.setPrefWidth(111);
                graphViewVB.getStyleClass().addAll("myBox");        
                Tooltip graphViewTT = new Tooltip(objDictionaryAction.getWord("GRAPHVIEW")+" (Ctrl+F11)\n"+objDictionaryAction.getWord("TOOLTIPGRAPHVIEW"));
                graphViewTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/graph_view.png"));
                Tooltip.install(graphViewVB, graphViewTT);
                graphViewVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        graphViewAction();
                    }
                });
                popup.add(graphViewVB, 1, 1);
                
                // Tiled View
                g = objBufferedImageResize.createGraphics();
                g.drawImage(objArtworkAction.plotTilledView(objBufferedImage, intLength, intHeight), 0, 0, 111, 111, null);
                g.dispose();
                //ImageIO.write(objBufferedImageesize, "png", new File(System.getProperty("user.dir")+"/mla/tempmenu_composite.png"));
                VBox tilledViewVB = new VBox(); 
                tilledViewVB.getChildren().add(new ImageView(SwingFXUtils.toFXImage(objBufferedImageResize, null)));
                tilledViewVB.getChildren().add(new Label("Ctrl+F12"));
                tilledViewVB.setPrefWidth(111);
                tilledViewVB.getStyleClass().addAll("myBox"); 
                Tooltip tilledViewTT = new Tooltip(objDictionaryAction.getWord("TILLEDVIEW")+" (Ctrl+F12)\n"+objDictionaryAction.getWord("TOOLTIPTILLEDVIEW"));
                tilledViewTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tiled_view.png"));
                Tooltip.install(tilledViewVB, tilledViewTT);                
                tilledViewVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        tilledViewAction();
                    }
                });
                popup.add(tilledViewVB, 2, 1);

                objBufferedImageResize = null;
                System.gc();

                Button btnCancel = new Button(objDictionaryAction.getWord("CLOSE"));
                btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
                btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCLOSE")));
                btnCancel.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent e) {  
                        lblStatus.setText(objDictionaryAction.getWord("ACTIONCLOSE"));
                        dialogStage.close();
                    }
                });
                popup.add(btnCancel, 2, 2);

                Scene scene = new Scene(popup);
                scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
                dialogStage.setScene(scene);
                dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("VIEW"));
                dialogStage.showAndWait();
            } catch(OutOfMemoryError ex){
                zoomOutAction();
                lblStatus.setText(objDictionaryAction.getWord("MAXVALUE"));           
            } catch (SQLException ex) {
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        } else{
            lblStatus.setText(objDictionaryAction.getWord("NOFABRIC"));
        }
    }    
    /**
     * transformMenuAction
     * <p>
     * Function use for weave menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void transformMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONTRANSFORMOPERATIONEDIT"));
        selectedMenu = "TRANSFORMOPERATION";
        transformMenu.show();
        transformMenu.setVisible(true);
        paintMenu.hide();
        paintMenu.setVisible(false);
        fileMenu.setDisable(true);
        editMenu.setDisable(true);
        viewMenu.setDisable(true);
        utilityMenu.setDisable(true);
        runMenu.setDisable(true);
        transformMenu.setDisable(false);
        paintMenu.setDisable(true);
        menuHighlight(); 
    }      
    /**
     * paintMenuAction
     * <p>
     * Function use for weave menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void paintMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONPAINTOPERATIONEDIT"));
        selectedMenu = "PAINTOPERATION";
        transformMenu.hide();
        transformMenu.setVisible(false);
        paintMenu.show();
        paintMenu.setVisible(true);
        fileMenu.setDisable(true);
        editMenu.setDisable(true);
        viewMenu.setDisable(true);
        utilityMenu.setDisable(true);
        runMenu.setDisable(true);
        transformMenu.setDisable(true);
        paintMenu.setDisable(false);
        menuHighlight(); 
    } 
    /**
     * menuHighlight
     * <p>
     * Function use for drawing menu bar for menu item,
     * and binding events for each menus with style. 
     * 
     * @param       String strMenu
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */
    public void menuHighlight(){
        fileMenu.setStyle("-fx-background-color: linear-gradient(#000000,#5C594A);");
        editMenu.setStyle("-fx-background-color: linear-gradient(#000000,#5C594A);");
        viewMenu.setStyle("-fx-background-color: linear-gradient(#000000,#5C594A);");
        utilityMenu.setStyle("-fx-background-color: linear-gradient(#000000,#5C594A);");
        transformMenu.setStyle("-fx-background-color: linear-gradient(#000000,#5C594A);");
        paintMenu.setStyle("-fx-background-color: linear-gradient(#000000,#5C594A);");
        fileMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-font-family: \"Segoe UI Semibold\"; -fx-text-fill: #FFFFFF;");
        editMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-font-family: \"Segoe UI Semibold\"; -fx-text-fill: #FFFFFF;");
        viewMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-font-family: \"Segoe UI Semibold\"; -fx-text-fill: #FFFFFF;");
        utilityMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-font-family: \"Segoe UI Semibold\"; -fx-text-fill: #FFFFFF;");
        transformMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-font-family: \"Segoe UI Semibold\"; -fx-text-fill: #FFFFFF;");
        paintMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-font-family: \"Segoe UI Semibold\"; -fx-text-fill: #FFFFFF;");
        toolBar.getItems().clear();
        menuSelected.getItems().clear();
        
        System.gc();
        if(selectedMenu == "PAINTOPERATION"){
            paintMenu.setStyle("-fx-background-color: linear-gradient(#FFFFFF,#FFFEEE);");
            paintMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-text-fill: #000000;");
            populatePaintToolbar();
            populatePaintToolbarMenu();
        } else if(selectedMenu == "TRANSFORMOPERATION"){
            transformMenu.setStyle("-fx-background-color: linear-gradient(#FFFFFF,#FFFEEE);");
            transformMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-text-fill: #000000;");
            populateTransformToolbar();
            populateTransformToolbarMenu();
        } else if(selectedMenu == "UTILITY"){
            utilityMenu.setStyle("-fx-background-color: linear-gradient(#FFFFFF,#FFFEEE);");
            utilityMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-text-fill: #000000;");
            populateUtilityToolbar();
            populateUtilityToolbarMenu();
        } else if(selectedMenu == "VIEW"){
            viewMenu.setStyle("-fx-background-color: linear-gradient(#FFFFFF,#FFFEEE);");
            viewMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-text-fill: #000000;");
            populateViewToolbar();
            populateViewToolbarMenu();
        } else if(selectedMenu == "EDIT"){
            editMenu.setStyle("-fx-background-color: linear-gradient(#FFFFFF,#FFFEEE);");
            editMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-text-fill: #000000;");
            populateEditToolbar();
            populateEditToolbarMenu();
        } else {
            fileMenu.setStyle("-fx-background-color: linear-gradient(#FFFFFF,#FFFEEE);");
            fileMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-text-fill: #000000;");
            populateFileToolbar();
            populateFileToolbarMenu();
        }
        System.gc();
    }
    /**
    * populateFileToolbar
    * <p>
    * Function use for drawing tool bar for menu item File,
    * and binding events for each tools. 
    * 
    * @exception   (@throws SQLException)
    * @author      Amit Kumar Singh
    * @version     %I%, %G%
    * @since       1.0
    * @see         javafx.event.*;
    * @link        ArtworkViewNew
    */
    private void populateFileToolbarMenu(){
        //For drop down menus
        menuSelected.setTooltip(new Tooltip(objDictionaryAction.getWord("FILE")));
        
        MenuItem newFileMI = new MenuItem(objDictionaryAction.getWord("NEWFILE"));
        MenuItem openFileMI = new MenuItem(objDictionaryAction.getWord("OPENFILE"));
        MenuItem loadFileMI = new MenuItem(objDictionaryAction.getWord("LOADFILE"));
        MenuItem importFileMI = new MenuItem(objDictionaryAction.getWord("IMPORTFILE"));
        MenuItem applicationFileMI = new MenuItem(objDictionaryAction.getWord("PAINTAPPLICATION"));
        MenuItem refreshFileMI = new MenuItem(objDictionaryAction.getWord("IMPORTREFRESH"));
        MenuItem saveFileMI = new MenuItem(objDictionaryAction.getWord("SAVEFILE"));
        MenuItem saveAsFileMI = new MenuItem(objDictionaryAction.getWord("SAVEASFILE"));
        MenuItem saveTextureFileMI = new MenuItem(objDictionaryAction.getWord("SAVETEXTUREFILE"));
        MenuItem saveTxtFileMI = new MenuItem(objDictionaryAction.getWord("SAVETXTFILE"));
        MenuItem saveGridFileMI = new MenuItem(objDictionaryAction.getWord("SAVEGRIDFILE"));
        MenuItem saveGraphFileMI = new MenuItem(objDictionaryAction.getWord("SAVEGRAPHFILE"));
        MenuItem printFileMI = new MenuItem(objDictionaryAction.getWord("PRINTFILE"));
        
        newFileMI.setAccelerator(new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN));
        openFileMI.setAccelerator(new KeyCodeCombination(KeyCode.O, KeyCombination.CONTROL_DOWN)); 
        loadFileMI.setAccelerator(new KeyCodeCombination(KeyCode.L, KeyCombination.CONTROL_DOWN)); 
        importFileMI.setAccelerator(new KeyCodeCombination(KeyCode.I, KeyCombination.CONTROL_DOWN)); 
        applicationFileMI.setAccelerator(new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        refreshFileMI.setAccelerator(new KeyCodeCombination(KeyCode.I, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        saveFileMI.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN)); 
        saveAsFileMI.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN)); 
        saveTextureFileMI.setAccelerator(new KeyCodeCombination(KeyCode.E, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        saveTxtFileMI.setAccelerator(new KeyCodeCombination(KeyCode.F, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        saveGridFileMI.setAccelerator(new KeyCodeCombination(KeyCode.H, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        saveGraphFileMI.setAccelerator(new KeyCodeCombination(KeyCode.G, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN)); 
        printFileMI.setAccelerator(new KeyCodeCombination(KeyCode.P, KeyCombination.CONTROL_DOWN));
        
        newFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/new.png"));
        openFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/open.png"));
        loadFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/open_recent.png"));
        importFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/import.png"));
        applicationFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/application_integration.png"));
        refreshFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/refresh.png"));
        saveFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
        saveAsFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save_as.png"));
        saveTextureFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_texture_as.png"));
        saveTxtFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_txt_as.png"));
        saveGridFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_grid_as.png"));
        saveGraphFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_graph_as.png"));
        printFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/print.png"));
             
        Menu exportMenu = new Menu(objDictionaryAction.getWord("EXPORT"));
        exportMenu.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export.png"));
        exportMenu.getItems().addAll(saveTextureFileMI, saveTxtFileMI, saveGridFileMI, saveGraphFileMI);
        
        //add enable disable condition
        if(!isWorkingMode){
            saveFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
            saveFileMI.setDisable(true);
            saveAsFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save_as.png"));
            saveAsFileMI.setDisable(true);
            refreshFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/refresh.png"));
            refreshFileMI.setDisable(true);
            saveTextureFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_texture_as.png"));
            saveTextureFileMI.setDisable(true);
            saveTxtFileMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/export_txt_as.png"));
            saveTxtFileMI.setDisable(true);
            saveGridFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_grid_as.png"));
            saveGridFileMI.setDisable(true);
            saveGraphFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_graph_as.png"));
            saveGraphFileMI.setDisable(true);
        }else{
            saveFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
            saveFileMI.setDisable(false);
            saveAsFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save_as.png"));
            saveAsFileMI.setDisable(false);
            refreshFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/refresh.png"));
            refreshFileMI.setDisable(false);
            saveTextureFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_texture_as.png"));
            saveTextureFileMI.setDisable(false);
            saveTxtFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_txt_as.png"));
            saveTxtFileMI.setDisable(false);
            saveGridFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_grid_as.png"));
            saveGridFileMI.setDisable(false);
            saveGraphFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_graph_as.png"));
            saveGraphFileMI.setDisable(false);
            if(isNew){
                //saveFileMI.setVisible(false);
                saveFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
                saveFileMI.setDisable(true);
            }
        } 
        //Add the action to Buttons.
        newFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                newArtwork();                
            }
        });        
        openFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                openArtwork();
            }
        });
        loadFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                recentArtwork();
            }
        });
        importFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                importArtwork();
            }
        });        
        applicationFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                artworkApplication();
            }
        });
        refreshFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                refreshArtwork();
            }
        });
        saveFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                saveArtwork();
            }
        });
        saveAsFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                saveAsArtwork();
            }
        });
        saveTextureFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                exportArtwork();
            }
        });
        saveTxtFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                exportText();
            }
        }); 
        saveGridFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                exportGrid();
            }
        });
        saveGraphFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                exportGraph();
            }
        });
        printFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                printMenuAction();
            }
        });        
        menuSelected.getItems().addAll(newFileMI, openFileMI, loadFileMI, importFileMI, applicationFileMI, refreshFileMI, new SeparatorMenuItem(), saveFileMI, saveAsFileMI, new SeparatorMenuItem(), exportMenu, printFileMI);
    }   
    private void populateFileToolbar(){
        // New file item
        Button newFileBtn = new Button();
        newFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/new.png"));
        newFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("NEWFILE")+" (Ctrl+N)\n"+objDictionaryAction.getWord("TOOLTIPNEWFILE")));
        newFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        newFileBtn.getStyleClass().add("toolbar-button");    
        // Open file item
        Button openFileBtn = new Button();
        openFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/open.png"));
        openFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("OPENFILE")+" (Ctrl+O)\n"+objDictionaryAction.getWord("TOOLTIPOPENFILE")));
        openFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        openFileBtn.getStyleClass().add("toolbar-button");
        // load recent file item
        Button loadFileBtn = new Button();        
        loadFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/open_recent.png"));
        loadFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("LOADFILE")+" (Ctrl+L)\n"+objDictionaryAction.getWord("TOOLTIPLOADFILE")));
        loadFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        loadFileBtn.getStyleClass().addAll("toolbar-button");
        // import file item
        Button importFileBtn = new Button();        
        importFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/import.png"));
        importFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("IMPORTFILE")+" (Ctrl+I)\n"+objDictionaryAction.getWord("TOOLTIPIMPORTFILE")));
        importFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        importFileBtn.getStyleClass().addAll("toolbar-button");
        // Device file item
        Button applicationFileBtn = new Button();        
        applicationFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/application_integration.png"));
        applicationFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("PAINTAPPLICATION")+" (Ctrl+Shift+N)\n"+objDictionaryAction.getWord("TOOLTIPPAINTAPPLICATION")));
        applicationFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        applicationFileBtn.getStyleClass().addAll("toolbar-button");
        // Refresh file item
        refreshFileBtn = new Button();        
        refreshFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/refresh.png"));
        refreshFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("IMPORTREFRESH")+" (Ctrl+Shift+I)\n"+objDictionaryAction.getWord("TOOLTIPIMPORTREFRESH")));
        refreshFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        refreshFileBtn.getStyleClass().addAll("toolbar-button");
        // Save file item
        Button saveFileBtn = new Button();        
        saveFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
        saveFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SAVEFILE")+" (Ctrl+S)\n"+objDictionaryAction.getWord("TOOLTIPSAVEFILE")));
        saveFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        saveFileBtn.getStyleClass().addAll("toolbar-button");
        // Save As file item
        Button saveAsFileBtn = new Button();        
        saveAsFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save_as.png"));
        saveAsFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SAVEASFILE")+" (Ctrl+Shift+S)\n"+objDictionaryAction.getWord("TOOLTIPSAVEASFILE")));
        saveAsFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        saveAsFileBtn.getStyleClass().addAll("toolbar-button");
        // Save Texture file item
        Button saveTextureFileBtn = new Button();        
        saveTextureFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_texture_as.png"));
        saveTextureFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SAVETEXTUREFILE")+" (Ctrl+Shift+E)\n"+objDictionaryAction.getWord("TOOLTIPSAVETEXTUREFILE")));
        saveTextureFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        saveTextureFileBtn.getStyleClass().addAll("toolbar-button");
        // Save As file item
        Button saveTxtFileBtn = new Button();        
        saveTxtFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_txt_as.png"));
        saveTxtFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SAVETXTFILE")+" (Ctrl+Shift+F)\n"+objDictionaryAction.getWord("TOOLTIPSAVETXTFILE")));
        saveTxtFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        saveTxtFileBtn.getStyleClass().addAll("toolbar-button");
        // Save grid file item
        Button saveGridFileBtn = new Button();        
        saveGridFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_grid_as.png"));
        saveGridFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SAVEGRIDFILE")+" (Ctrl+Shift+H)\n"+objDictionaryAction.getWord("TOOLTIPSAVEGRIDFILE")));
        saveGridFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        saveGridFileBtn.getStyleClass().addAll("toolbar-button");
        // Save graph file item
        Button saveGraphFileBtn = new Button();        
        saveGraphFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_graph_as.png"));
        saveGraphFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SAVEGRAPHFILE")+" (Ctrl+Shift+G)\n"+objDictionaryAction.getWord("TOOLTIPSAVEGRAPHFILE")));
        saveGraphFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        saveGraphFileBtn.getStyleClass().addAll("toolbar-button");
        // print File menu
        Button printFileBtn = new Button();        
        printFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/print.png"));
        printFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("PRINTFILE")+" (Ctrl+P)\n"+objDictionaryAction.getWord("TOOLTIPPRINTFILE")));
        printFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        printFileBtn.getStyleClass().addAll("toolbar-button"); 
        //add enable disable condition
        if(!isWorkingMode){
            saveFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
            saveFileBtn.setDisable(true);
            saveFileBtn.setCursor(Cursor.WAIT);
            saveAsFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save_as.png"));
            saveAsFileBtn.setDisable(true);
            saveAsFileBtn.setCursor(Cursor.WAIT);
            refreshFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/refresh.png"));
            refreshFileBtn.setDisable(true);
            refreshFileBtn.setCursor(Cursor.WAIT);
            saveTextureFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_texture_as.png"));
            saveTextureFileBtn.setDisable(true);
            saveTextureFileBtn.setCursor(Cursor.WAIT);
            saveTxtFileBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/export_txt_as.png"));
            saveTxtFileBtn.setDisable(true);
            saveTxtFileBtn.setCursor(Cursor.WAIT);
            saveGridFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_grid_as.png"));
            saveGridFileBtn.setDisable(true);
            saveGridFileBtn.setCursor(Cursor.WAIT);
            saveGraphFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_graph_as.png"));
            saveGraphFileBtn.setDisable(true);
            saveGraphFileBtn.setCursor(Cursor.WAIT);
            printFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/print.png"));
            printFileBtn.setDisable(true);
            printFileBtn.setCursor(Cursor.WAIT);
        }else{
            saveFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
            saveFileBtn.setDisable(false);
            saveFileBtn.setCursor(Cursor.HAND); 
            saveAsFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save_as.png"));
            saveAsFileBtn.setDisable(false);
            saveAsFileBtn.setCursor(Cursor.HAND); 
            refreshFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/refresh.png"));
            refreshFileBtn.setDisable(false);
            refreshFileBtn.setCursor(Cursor.HAND); 
            saveTextureFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_texture_as.png"));
            saveTextureFileBtn.setDisable(false);
            saveTextureFileBtn.setCursor(Cursor.HAND); 
            saveTxtFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_txt_as.png"));
            saveTxtFileBtn.setDisable(false);
            saveTxtFileBtn.setCursor(Cursor.HAND); 
            saveGridFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_grid_as.png"));
            saveGridFileBtn.setDisable(false);
            saveGridFileBtn.setCursor(Cursor.HAND);
            saveGraphFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_graph_as.png"));
            saveGraphFileBtn.setDisable(false);
            saveGraphFileBtn.setCursor(Cursor.HAND);
            printFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/print.png"));
            printFileBtn.setDisable(false);
            printFileBtn.setCursor(Cursor.HAND);
        } 
        //Add the action to Buttons.
        newFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                newArtwork();                
            }
        });        
        openFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                openArtwork();
            }
        });
        loadFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                recentArtwork();
            }
        });
        importFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                importArtwork();
            }
        });        
        applicationFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                artworkApplication();
            }
        });
        refreshFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                refreshArtwork();
            }
        });
        saveFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                saveArtwork();
            }
        });
        saveAsFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                saveAsArtwork();
            }
        });
        saveTextureFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                exportArtwork();
            }
        });
        saveTxtFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                exportText();
            }
        });
        saveGridFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                exportGrid();
            }
        });
        saveGraphFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                exportGraph();
            }
        });
        printFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                printMenuAction();
            }
        }); 
        //add button
        FlowPane flow = new FlowPane();        
        flow.setPrefWrapLength(objConfiguration.WIDTH*.95);
        flow.getStyleClass().addAll("flow");
        //Add the Buttons to the ToolBar.
        flow.getChildren().addAll(newFileBtn,openFileBtn,loadFileBtn,importFileBtn,applicationFileBtn,refreshFileBtn,saveFileBtn,saveAsFileBtn,saveTextureFileBtn,saveTxtFileBtn,saveGridFileBtn,saveGraphFileBtn,printFileBtn);
        flow.setAlignment(Pos.TOP_LEFT);
        toolBar.getItems().addAll(flow);
    }    
    //////////////////////////// File Menu Action execution ///////////////////////////
    /**
     * initArtworkValue
     * <p>
     * This method is used for initializing artwork image to panel.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for initializing artwork image to panel.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void initArtworkValue(){
        try {
            //Image image = SwingFXUtils.toFXImage(objBufferedImage, null);
            //artwork.setImage(image);
            //container.setContent(artwork);
            objArtworkAction = new ArtworkAction();
            colors = objArtworkAction.getImageColor(objBufferedImage);
            intColor = (byte)colors.size();
            //contextMenu.show(contextMenu);
            if(isNew){                
                refreshFileBtn.setDisable(false);
                refreshFileBtn.setCursor(Cursor.HAND);
            }
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(objBufferedImage, "png", baos);
            baos.flush();
            byte[] imageInByte = baos.toByteArray();
            objArtwork.setBytArtworkThumbnil(imageInByte);
            imageInByte = null;
            baos.close();
            
            plotViewAction();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (IOException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    /**
 * cloneArtwork
 * <p>
 * Function use for saving artwork as clone.
 * 
 * @exception (@throws )
 * @value
 * @author Amit Kumar Singh
 * @version     %I%, %G%
 * @since 1.0
 */    
    public Artwork cloneArtwork(){
        long cloneTime = 0L;
        long start = System.currentTimeMillis();
        Artwork objArtworkCopy = new Artwork();
        try {
            
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);           
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
        cloneTime += (System.currentTimeMillis() - start);
        //System.out.println("Cloning Time"+cloneTime);
        return objArtworkCopy;
    }
    static BufferedImage deepCopy(BufferedImage bi) {
        ColorModel cm = bi.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = bi.copyData(null);
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
   }
    public static BufferedImage copyImage(BufferedImage source){
        BufferedImage b = new BufferedImage(source.getWidth(), source.getHeight(), source.getType());
        Graphics g = b.getGraphics();
        g.drawImage(source, 0, 0, null);
        g.dispose();
        return b;
    }
    /**
     * newArtwork
     * <p>
     * This method is used for creating GUI for artwork.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for initializing GUI for artwork.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void newArtwork(){
        if(maxSizeCheck(objConfiguration.getIntEnds(),objConfiguration.getIntPixs())){
            lblStatus.setText(objDictionaryAction.getWord("ACTIONNEWFILE"));
            try{
                objBufferedImage = new BufferedImage(objConfiguration.getIntEnds(), objConfiguration.getIntPixs(), BufferedImage.TYPE_INT_ARGB);
                objGraphics2D = objBufferedImage.createGraphics();
                objGraphics2D.drawImage(objBufferedImage, 0, 0, objConfiguration.getIntEnds(), objConfiguration.getIntPixs(), null);
                objGraphics2D.setColor(java.awt.Color.WHITE);
                objGraphics2D.fillRect(0, 0, objConfiguration.getIntEnds(), objConfiguration.getIntPixs());

                //objConfiguration.setStrRecentArtwork(null);
                objArtwork = new Artwork();
                objArtwork.setObjConfiguration(objConfiguration);
                objArtwork.setStrArtworkId(null);

                objUR.clear();
                objUR.doCommand("New Artwork", objBufferedImage);
                //objURF.store(objBufferedImage);
                
                isNew = true;
                isEditingMode = false;
                isWorkingMode = true; 
                selectedMenu = "FILE";
                menuHighlight();
                plotViewActionMode = 1;
                initArtworkValue();
            } catch (Exception ex) {
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                new Logging("SEVERE",getClass().getName(),getClass().getEnclosingMethod().getName()+ex.getMessage(),ex);
            }
        } else{
            //recentArtwork();
            lblStatus.setText(objDictionaryAction.getWord("MAXVALUE"));            
        }
        System.gc();
    }
    /**
     * openArtwork
     * <p>
     * This method is used for initializing GUI for artwork.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for initializing GUI for artwork.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void openArtwork(){
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
        
            lblStatus.setText(objDictionaryAction.getWord("ACTIONOPENFILE"));
            Artwork objArtworkNew = new Artwork();
            objArtworkNew.setObjConfiguration(objConfiguration);
            ArtworkImportView objArtworkImportView= new ArtworkImportView(objArtworkNew);
            if(objArtworkNew.getStrArtworkId()!=null){
                objArtwork = objArtworkNew;
                loadArtwork();
            }
        } else{
            //newArtwork();
            lblStatus.setText(objDictionaryAction.getWord("INVALIDPASSWORD"));            
        }
        System.gc();
    }
    /**
     * recentArtwork
     * <p>
     * This method is used for initializing GUI for artwork.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for initializing GUI for artwork.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void recentArtwork(){
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
            lblStatus.setText(objDictionaryAction.getWord("ACTIONLOADFILE"));
            //newly added code for load recent based on saved data
            try{
                objArtwork.setStrCondition("");
                //objArtwork.setStrSearchBy("All");
                objArtwork.setStrSearchAccess("All User Data");
                objArtwork.setStrOrderBy("Date");
                objArtwork.setStrDirection("Ascending");
        
                objArtworkAction = new ArtworkAction();
                ArrayList lstArtwork = (ArrayList)objArtworkAction.lstImportArtwork(objArtwork).get(0);
                if(lstArtwork!=null)
                    objConfiguration.setStrRecentArtwork(lstArtwork.get(0).toString());
                lstArtwork = null;
            } catch (Exception ex) {
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                new Logging("SEVERE",getClass().getName(),getClass().getEnclosingMethod().getName()+ex.getMessage(),ex);
            }
            //new code end here
            if(objConfiguration.getStrRecentArtwork()!=null){
                objArtwork = new Artwork();
                objArtwork.setObjConfiguration(objConfiguration);
                objArtwork.setStrArtworkId(objConfiguration.getStrRecentArtwork());
                loadArtwork();
            }
        } else{
            //newArtwork();
            lblStatus.setText(objDictionaryAction.getWord("INVALIDPASSWORD"));            
        }
        System.gc();
    }
    /**
     * SaveArtwork
     * <p>
     * This method is used for updating artwork to library.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for updating artwork to library.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void saveArtwork(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONSAVEFILE"));
        int artworkCount = 0;
        if(objArtwork.getStrArtworkId()!=null){
            try {
                objArtworkAction = new ArtworkAction();
                artworkCount = objArtworkAction.countArtworkUsage(objArtwork.getStrArtworkId());
            } catch (SQLException ex) {
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                new Logging("SEVERE",getClass().getName(),"SQL error"+ex.getMessage(),ex);
            }
        }
        if(isNew || artworkCount > 0 || objArtwork.getStrArtworkAccess().equalsIgnoreCase("Public")){
            isNew = true;
        }
        savingArtwork();
        //updateArtwork();
        System.gc();
    }
    /**
     * setArtwork
     * <p>
     * This method is used for inserting artwork to library.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for inserting artwork to library.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void setArtwork(){
        try {
            objArtwork.setObjConfiguration(objConfiguration);
            String strArtworkID = new IDGenerator().getIDGenerator("ARTWORK_LIBRARY", objConfiguration.getObjUser().getStrUserID());
            
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(objBufferedImage, "png", baos);
            baos.flush();
            byte[] imageInByte = baos.toByteArray();  
            objArtwork.setBytArtworkThumbnil(imageInByte);
            imageInByte = null;
            baos.close();
            objArtwork.setStrArtworkId(strArtworkID);
            objArtworkAction = new ArtworkAction();
            if(objArtworkAction.setArtwork(objArtwork)!=0){
                objConfiguration.setStrRecentArtwork(objArtwork.getStrArtworkId());
                lblStatus.setText(objArtwork.getStrArtworkName()+" : "+objDictionaryAction.getWord("DATASAVED"));
                isNew = false;
                selectedMenu = "FILE";
                menuHighlight();
            }else{
                lblStatus.setText(objDictionaryAction.getWord("DATAUNSAVED"));
            }
        } catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),"Operation save"+ex.getMessage(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Operation save"+ex.getMessage(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
        System.gc();    
    }
    /**
     * saveAsArtwork
     * <p>
     * This method is used for inserting artwork to library.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for inserting artwork to library.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void saveAsArtwork(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONSAVEASFILE"));
        objArtwork.setStrArtworkAccess(objConfiguration.getObjUser().getUserAccess("ARTWORK_LIBRARY"));
        isNew=true;
        savingArtwork();
        //saveArtwork();
        System.gc();
    }
    /**
     * resetArtwork
     * <p>
     * This method is used for updating artwork to library.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for updating artwork to library.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void resetArtwork(){
        try {
            String strBackground = String.format("#%02X%02X%02X",colors.get(0).getRed(),colors.get(0).getGreen(),colors.get(0).getBlue());
            objArtwork.setStrArtworkBackground(strBackground);
                        
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(objBufferedImage, "png", baos);
            baos.flush();
            byte[] imageInByte = baos.toByteArray();  
            objArtwork.setBytArtworkThumbnil(imageInByte);
            imageInByte = null;
            baos.close();
            objArtworkAction = new ArtworkAction();
            if(objArtworkAction.resetArtwork(objArtwork)!=0){
                objConfiguration.setStrRecentArtwork(objArtwork.getStrArtworkId());
                lblStatus.setText(objArtwork.getStrArtworkName()+" : "+objDictionaryAction.getWord("DATASAVED"));
            }else{
                lblStatus.setText(objDictionaryAction.getWord("DATAUNSAVED"));
            }
        } catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),"Operation save"+ex.getMessage(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Operation save"+ex.getMessage(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
        System.gc();
    }
    /**
     * savingArtwork
     * <p>
     * This method is used for initializing GUI for artwork.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for initializing GUI for artwork.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    public void savingArtwork(){
        if(colors.size()<=objConfiguration.getIntColorLimit()){
            // if artwork access is Public, we need not show dialog for Save
            if(artworkChildStage!=null){
                artworkChildStage.close();
                artworkChildStage = null;
                System.gc();
            }
            artworkChildStage = new Stage();
            artworkChildStage.initStyle(StageStyle.UTILITY);
            artworkChildStage.initModality(Modality.WINDOW_MODAL);
            GridPane popup=new GridPane();
            popup.setId("popup");
            popup.setAlignment(Pos.CENTER);
            popup.setHgap(10);
            popup.setVgap(10);
            popup.setPadding(new Insets(10, 10, 10, 10));
            
            Label lblName = new Label(objDictionaryAction.getWord("NAME"));
            lblName.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPNAME")));
            popup.add(lblName, 0, 0);
            final TextField txtName = new TextField();
            txtName.setPromptText(objDictionaryAction.getWord("TOOLTIPNAME"));
            txtName.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPNAME")));
            txtName.setDisable(true);
            if(objArtwork.getStrArtworkName()!=null)
                txtName.setText(objArtwork.getStrArtworkName());
            if(isNew)
                txtName.setDisable(false);
            popup.add(txtName, 1, 0, 2, 1);

            Label artworkBackground = new Label(objDictionaryAction.getWord("BACKGROUNDCOLOR")+":");
            artworkBackground.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPBACKGROUNDCOLOR")));
            popup.add(artworkBackground, 0, 1);
            final ComboBox backgroundCB = new ComboBox();            
            backgroundCB.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPBACKGROUNDCOLOR")));
            for(int i=0; i<intColor; i++){
                String webColor = String.format("#%02X%02X%02X",colors.get(i).getRed(),colors.get(i).getGreen(),colors.get(i).getBlue());
                backgroundCB.getItems().add(webColor);
                if(i==0)
                    backgroundCB.setValue(webColor);
            }
            popup.add(backgroundCB, 1, 1, 2, 1);        
        
            final ToggleGroup artworkTG = new ToggleGroup();
            RadioButton artworkPublicRB = new RadioButton(objDictionaryAction.getWord("PUBLIC"));
            artworkPublicRB.setToggleGroup(artworkTG);
            artworkPublicRB.setUserData("Public");
            popup.add(artworkPublicRB, 0, 2);
            RadioButton artworkProtectedRB = new RadioButton(objDictionaryAction.getWord("PROTECTED"));
            artworkProtectedRB.setToggleGroup(artworkTG);
            artworkProtectedRB.setUserData("Protected");
            popup.add(artworkProtectedRB, 1, 2);
            RadioButton artworkPrivateRB = new RadioButton(objDictionaryAction.getWord("PRIVATE"));
            artworkPrivateRB.setToggleGroup(artworkTG);
            artworkPrivateRB.setUserData("Private");
            popup.add(artworkPrivateRB, 2, 2);
            
            if(isNew){
                objArtwork.setStrArtworkAccess(objConfiguration.getObjUser().getUserAccess("ARTWORK_LIBRARY"));
            }           
            if(objArtwork.getStrArtworkAccess().equalsIgnoreCase("Public")){
                artworkTG.selectToggle(artworkPublicRB);
                artworkPublicRB.setDisable(true);
                artworkProtectedRB.setDisable(true);
                artworkPrivateRB.setDisable(true);
            } else if(objArtwork.getStrArtworkAccess().equalsIgnoreCase("Protected")){
                artworkTG.selectToggle(artworkProtectedRB);
            } else{
                artworkTG.selectToggle(artworkPrivateRB);
            }

            final PasswordField passPF= new PasswordField();
            final Label lblAlert = new Label();
            if(objConfiguration.getBlnAuthenticateService()){
                Label lblPassword=new Label(objDictionaryAction.getWord("SERVICEPASSWORD"));
                lblPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                popup.add(lblPassword, 0, 3);
                passPF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                passPF.setPromptText(objDictionaryAction.getWord("PROMPTSERVICEPASSWORD"));
                popup.add(passPF, 1, 3, 2, 1);
            }
            lblAlert.setStyle("-fx-wrap-text:true;");
            lblAlert.setPrefWidth(250);            
            popup.add(lblAlert, 0, 5, 3, 1);

            Button btnSave = new Button(objDictionaryAction.getWord("SAVE"));
            btnSave.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSAVE")));
            btnSave.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
            btnSave.setDefaultButton(true);
            btnSave.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    objArtwork.setStrArtworkAccess(artworkTG.getSelectedToggle().getUserData().toString());
                    objArtwork.setStrArtworkBackground(backgroundCB.getValue().toString());
                    if(txtName.getText().trim().length()==0){
                        lblAlert.setText(objDictionaryAction.getWord("WRONGINPUT"));
                        lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
                    } else{
                        if(objConfiguration.getBlnAuthenticateService()){
                            if(passPF.getText()!=null && passPF.getText()!="" && passPF.getText().trim().length()!=0){
                                if(Security.SecurePassword(passPF.getText(), objConfiguration.getObjUser().getStrUsername()).equals(objConfiguration.getObjUser().getStrAppPassword())){
                                    if(isNew){
                                        if(txtName.getText()!=null && txtName.getText()!="" && txtName.getText().trim().length()!=0)
                                            objArtwork.setStrArtworkName(txtName.getText());
                                        else
                                            objArtwork.setStrArtworkName("Unknown Artwork");
                                        setArtwork();
                                    } else{
                                        resetArtwork();
                                    }
                                    artworkChildStage.close();
                                } else{
                                    lblAlert.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                                    lblStatus.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                                }
                            }else{
                                lblAlert.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                                lblStatus.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                            } 
                        } else{   // service password is disabled
                            if(isNew){
                                if(txtName.getText()!=null && txtName.getText()!="" && txtName.getText().trim().length()!=0)
                                    objArtwork.setStrArtworkName(txtName.getText());
                                else
                                    objArtwork.setStrArtworkName("Unknown Artwork");                                    
                                setArtwork();
                            } else {
                                resetArtwork();
                            }
                            artworkChildStage.close();
                        }
                    }
                }
            });
            popup.add(btnSave, 1, 4);
            Button btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));
            btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
            btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            btnCancel.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {  
                    artworkChildStage.close();
                    lblStatus.setText(objDictionaryAction.getWord("TOOLTIPCANCEL"));
                }
            });
            popup.add(btnCancel, 0, 4);
            artworkChildStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                public void handle(WindowEvent we) {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                    artworkChildStage.close();
                }
            });
            Scene scene = new Scene(popup, 300, 200);
            scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
            artworkChildStage.setScene(scene);
            artworkChildStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("SAVE"));
            artworkChildStage.showAndWait();
        } else {
            lblStatus.setText(objDictionaryAction.getWord("MANYCOLOUR"));
        }
    }
    /**
     * importArtwork
     * <p>
     * This method is used for importing artwork.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for importing artwork.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void importArtwork(){     
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
            lblStatus.setText(objDictionaryAction.getWord("ACTIONIMPORTFILE"));
            try {
                FileChooser fileChooser = new FileChooser();             
                //Set extension filter
                FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
                FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
                FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("BMP files (*.bmp)", "*.BMP");
                FileChooser.ExtensionFilter extFilterTIF = new FileChooser.ExtensionFilter("TIFF files (*.tif)", "*.TIF");
                fileChooser.getExtensionFilters().addAll(extFilterPNG, extFilterJPG, extFilterBMP, extFilterTIF);
                //fileChooser.setInitialDirectory(new File(objFabric.getObjConfiguration().strRoot));
                fileChooser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("SELECTTO")+" "+objDictionaryAction.getWord("DESIGN"));
                //Show open file dialog
                File file = fileChooser.showOpenDialog(null);
                filePath = file.getCanonicalPath();
                objBufferedImage = ImageIO.read(file);

                objArtworkAction = new ArtworkAction(false);
                lblStatus.setText(objDictionaryAction.getWord("COLORINFO"));
                objBufferedImage = objArtworkAction.rectifyImage(objBufferedImage,objConfiguration.getIntColorLimit());
            
                if(objBufferedImage!=null){
                    objUR.clear();
                    objUR.doCommand("import Artwork", objBufferedImage);
                    //objURF.store(objBufferedImage);
                }
                
                objArtwork = new Artwork();
                objArtwork.setObjConfiguration(objConfiguration);
                objArtwork.setStrArtworkId(null);
                isNew = true;
                isEditingMode = false;
                isWorkingMode = true; 
                selectedMenu = "FILE";
                menuHighlight();
                plotViewActionMode = 1;
                initArtworkValue();
                lblStatus.setText(objDictionaryAction.getWord("ACTIONIMPORTFILE"));
            } catch (IOException ex) {
                new Logging("SEVERE",getClass().getName(),"Operation import"+ex.getMessage(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            } catch (SQLException ex) {
                new Logging("SEVERE",getClass().getName(),"Operation import"+ex.getMessage(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            } catch(OutOfMemoryError ex){
                recentArtwork();
                lblStatus.setText(objDictionaryAction.getWord("MAXVALUE"));           
            }
        }
        System.gc();
    }
    /**
     * artworkApplication
     * <p>
     * <p>
     * This method is used for creating GUI and running third party application.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for creating GUI and running third party application.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void artworkApplication(){     
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
            lblStatus.setText(objDictionaryAction.getWord("ACTIONPAINTAPPLICATION"));
            try {
                UtilityAction objUtilityAction = new UtilityAction();
                Device objDevice = new Device(null, "Designing S/W", null, null);
                objDevice.setObjConfiguration(objConfiguration);
                Device[] devices = objUtilityAction.getDevices(objDevice);
                if(devices.length>0){            
                    final Stage artworkPopupStage = new Stage();
                    artworkPopupStage.initStyle(StageStyle.UTILITY);
                    //dialogStage.initModality(Modality.WINDOW_MODAL);
                    ComboBox deviceCB = new ComboBox();
                    deviceCB.getItems().add("Select");
                    for(int i=0; i<devices.length; i++)
                        deviceCB.getItems().add(devices[i].getStrDeviceName());
                    deviceCB.setValue("Select");
                    deviceCB.valueProperty().addListener(new ChangeListener<String>() {
                        @Override
                        public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                            try {
                                lblStatus.setText(objDictionaryAction.getWord("ARTWORKEDIT"));
                        
                                UtilityAction objUtilityAction = new UtilityAction();
                                Device objDevice = new Device(null, null, t1, null);
                                objDevice.setObjConfiguration(objConfiguration);
                                objUtilityAction.getDevice(objDevice);
                                if(objDevice.getStrDevicePath()!=null){
                                    Runtime rt = Runtime.getRuntime();
                                    if(objArtwork.getObjConfiguration().getObjProcessProcessBuilder()!=null)
                                        objArtwork.getObjConfiguration().getObjProcessProcessBuilder().destroy();
                                    File file = new File(objDevice.getStrDevicePath());
                                    if(file.exists() && !file.isDirectory()) {
                                        if(objBufferedImage!=null){
                                            filePath = System.getProperty("user.dir") + "/mla/temp/tempdesign.png";
                                            filePath = filePath.replace("\\","/");
                                            refreshFileBtn.setDisable(false);
                                            refreshFileBtn.setCursor(Cursor.HAND);
                                            file = new File(filePath);
                                            ImageIO.write(objBufferedImage, "png", file);
                                            artworkPopupStage.close();
                                            objArtwork.getObjConfiguration().setObjProcessProcessBuilder(new ProcessBuilder(objDevice.getStrDevicePath(),file.getAbsolutePath()).start());
                                        }else{
                                            artworkPopupStage.close();
                                            objArtwork.getObjConfiguration().setObjProcessProcessBuilder(new ProcessBuilder(objDevice.getStrDevicePath()).start());
                                            importArtwork();
                                        }
                                    }else{
                                        lblStatus.setText(objDictionaryAction.getWord("NOITEM"));
                                    }
                                }else{
                                    lblStatus.setText(objDictionaryAction.getWord("NOVALUE"));
                                }
                            } catch (SQLException ex) {
                                new Logging("SEVERE",getClass().getName(),"SQLException:Operation edit artwork"+ex.toString(),ex);
                                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                            } catch (IOException ex) {
                                new Logging("SEVERE",getClass().getName(),"IOException:Operation edit artwork"+ex.toString(),ex);
                                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                            } catch (Exception ex) {
                                new Logging("SEVERE",getClass().getName(),"Exception:Operation edit artwork"+ex.toString(),ex);
                                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                            }
                            if(artworkPopupStage!=null)
                                artworkPopupStage.close();
                        }
                    });
                    artworkPopupStage.setScene(new Scene(deviceCB,200,50));
                    artworkPopupStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("SELECTTO")+" "+objDictionaryAction.getWord("RUN"));
                    artworkPopupStage.showAndWait();
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONIMPORTFILE"));
                }else{
                    lblStatus.setText(objDictionaryAction.getWord("NODEVICE"));
                }
            } catch (Exception ex) {
                new Logging("SEVERE",getClass().getName(),"Operation import"+ex.getMessage(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        }   
        System.gc();
    }
    /**
     * refreshArtwork
     * <p>
     * This method is used for refreshing or reloading design changed or imported to library.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for refreshing or reloading design changed or imported to library.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void refreshArtwork(){
        if(filePath!=null){
            lblStatus.setText(objDictionaryAction.getWord("ACTIONIMPORTREFRESH"));
            try {
                //objURF.store(objBufferedImage);
                File file = new File(filePath);
                if(file.exists()){
                    objBufferedImage = ImageIO.read(file);
                    objArtworkAction = new ArtworkAction(false);
                    objBufferedImage = objArtworkAction.rectifyImage(objBufferedImage,objConfiguration.getIntColorLimit());
                    objUR.clear();
                    objUR.doCommand("ImportRefresh Artwork", objBufferedImage);
                    isEditingMode = false;
                    isWorkingMode = true; 
                    selectedMenu = "FILE";
                    menuHighlight();
                    plotViewActionMode = 1;
                    initArtworkValue();
                } else {
                    lblStatus.setText(objDictionaryAction.getWord("INVALIDDATA"));
                }
            } catch (IOException ex) {
                new Logging("SEVERE",getClass().getName(),"Operation import",ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            } catch (SQLException ex) {
                new Logging("SEVERE",getClass().getName(),"Operation device",ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            } catch(OutOfMemoryError ex){
                undoAction();
                lblStatus.setText(objDictionaryAction.getWord("MAXVALUE"));
            }
        } else{
            lblStatus.setText(objDictionaryAction.getWord("INVALIDDATA"));
        }
        System.gc();
    }
    /**
     * exportArtwork
     * <p>
     * This method is used for exporting artwork.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for exporting artwork.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void exportArtwork(){
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
            lblStatus.setText(objDictionaryAction.getWord("ACTIONEXPORTFILE"));                
            try {
                FileChooser artworkExport=new FileChooser();
                artworkExport.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTPNG")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
                FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
                artworkExport.getExtensionFilters().add(extFilterPNG);
                File file=artworkExport.showSaveDialog(artworkStage);
                if(file==null)
                    return;
                else
                    artworkStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+file.getAbsoluteFile().getName()+"]");   
                if (!file.getName().endsWith("png")) 
                    file = new File(file.getPath() +".png");
                ImageIO.write(objBufferedImage, "png", file);    

                if(objConfiguration.getBlnAuthenticateService()){
                    ArrayList<File> filesToZip=new ArrayList<>();
                    filesToZip.add(file);
                    String zipFilePath=file.getAbsolutePath()+".zip";
                    String passwordToZip = objConfiguration.getObjUser().getStrAppPassword();//file.getName();
                    new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                    file.delete();
                }
                lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+file.getCanonicalPath());
            } catch (IOException ex) {
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            } catch (Exception ex) {
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        }
        System.gc();
    }
    /**
     * exportGrid
     * <p>
     * This method is used for exporting artwork.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for exporting artwork.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void exportText() { 
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
            lblStatus.setText(objDictionaryAction.getWord("ACTIONSAVETXTFILE"));
            try {
                String strGraphData="";
                int intHeight=(int)objBufferedImage.getHeight();
                int intLength=(int)objBufferedImage.getWidth();                
                int rgb = 0;
                int intBoards=0;
                BufferedImage graphImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
                for(int i=0; i<intHeight; i++){
                    String strLineData = "";
                    for(int j=0; j<intLength; j++){
                        if(objBufferedImage.getRGB(j, i)!=objBufferedImage.getRGB(0, 0)){
                            strLineData +=(j+1)+",";
                            rgb = new java.awt.Color((float)javafx.scene.paint.Color.web("#000000").getRed(),(float)javafx.scene.paint.Color.web("#000000").getGreen(),(float)javafx.scene.paint.Color.web("#000000").getBlue()).getRGB();
                        } else {
                            rgb = new java.awt.Color((float)javafx.scene.paint.Color.web("#FFFFFF").getRed(),(float)javafx.scene.paint.Color.web("#FFFFFF").getGreen(),(float)javafx.scene.paint.Color.web("#FFFFFF").getBlue()).getRGB();
                        } 
                        graphImage.setRGB(j, i, rgb);
                    }
                    if(strLineData.trim().length()>0){
                        intBoards++;
                        strGraphData += "\n"+ intBoards+":>"+strLineData.substring(0, strLineData.length()-1)+"||";
                    }
                    strLineData = null;
                }
                strGraphData = "Thread : "+intLength+" Board : "+intBoards+"\nDescription\n\n"+strGraphData;

                DirectoryChooser directoryChooser=new DirectoryChooser();
                artworkStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTBMP")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
                File selectedDirectory = directoryChooser.showDialog(artworkStage);
                if(selectedDirectory == null)
                    return;
                else
                    artworkStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+selectedDirectory.getAbsolutePath()+"]");

                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
                Date date = new Date();
                String currentDate = dateFormat.format(date);
                String path = selectedDirectory.getPath()+"\\Artwork_"+currentDate+"\\";
                File file = new File(path);            
                if (!file.exists()) {
                    if (!file.mkdir())
                        path = selectedDirectory.getPath();
                }
                ArrayList<File> filesToZip=new ArrayList<>();            
                //adding txt graph file
                file = new File(path +"data.txt");
                //System.err.println(objWeave.getStrWeaveFile());
                FileWriter writer = new FileWriter(file);
                writer.write(strGraphData);
                writer.close();
                filesToZip.add(file);
                //adding image file
                file = new File(path + "graph.bmp");
                ImageIO.write(graphImage, "BMP", file);
                filesToZip.add(file);
                //prepare archive folder
                String zipFilePath=selectedDirectory.getPath()+"\\Artwork_"+currentDate+".zip";
                String passwordToZip = "Artwork_"+currentDate;
                new EncryptZip(zipFilePath, filesToZip, passwordToZip);            
                //delete folder recursively
                recursiveDelete(new File(path));
                objBufferedImage = null;
                lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+path);
            } catch(OutOfMemoryError ex){
                zoomOutAction();
                lblStatus.setText(objDictionaryAction.getWord("MAXVALUE"));           
            } catch (Exception ex) {
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        }
        System.gc();
    }
    /**
     * exportGrid
     * <p>
     * This method is used for exporting artwork.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for exporting artwork.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void exportGrid() { 
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
            lblStatus.setText(objDictionaryAction.getWord("ACTIONSAVEGRIDFILE"));
            try {
                BufferedImage objEditBufferedImage = null;
            
                FileChooser artworkExport=new FileChooser();
                artworkExport.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTPNG")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
                FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
                artworkExport.getExtensionFilters().add(extFilterPNG);
                File file=artworkExport.showSaveDialog(artworkStage);
                if(file==null)
                    return;
                else
                    artworkStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+file.getAbsoluteFile().getName()+"]");   
                if (!file.getName().endsWith("png")) 
                    file = new File(file.getPath() +".png");
                ImageIO.write(getArtworkGrid(objEditBufferedImage), "png", file);    

                if(objConfiguration.getBlnAuthenticateService()){
                    ArrayList<File> filesToZip=new ArrayList<>();
                    filesToZip.add(file);
                    String zipFilePath=file.getAbsolutePath()+".zip";
                    String passwordToZip = objConfiguration.getObjUser().getStrAppPassword();//file.getName();
                    new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                    file.delete();
                }
                objEditBufferedImage = null;
                lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+file.getCanonicalPath());
            } catch (IOException ex) {
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            } catch(OutOfMemoryError ex){
                zoomOutAction();
                lblStatus.setText(objDictionaryAction.getWord("MAXVALUE"));           
            } catch (Exception ex) {
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        }
        System.gc();
    }
    /**
     * getArtworkGraph
     * <p>
     * This method is used for exporting artwork.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for exporting artwork.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private BufferedImage getArtworkGrid(BufferedImage objEditBufferedImage) throws OutOfMemoryError{
        lblStatus.setText(objDictionaryAction.getWord("GETGRIDVIEW"));
        int intHeight=(int)objBufferedImage.getHeight();
        int intLength=(int)objBufferedImage.getWidth();
        String[] data =objArtwork.getObjConfiguration().getStrGraphSize().split("x");
        //box resized
        int x_inc = (int)Math.round(zoomfactor*Integer.parseInt(data[1])/graphFactor);
        int y_inc = (int)Math.round(zoomfactor*Integer.parseInt(data[0])/graphFactor);
        //resized image
        objEditBufferedImage= new BufferedImage(intLength*x_inc, intHeight*y_inc,BufferedImage.TYPE_INT_RGB);
        Graphics2D g = objEditBufferedImage.createGraphics();
        g.drawImage(objBufferedImage, 0, 0, intLength*x_inc, intHeight*y_inc, null);
        g.setColor(java.awt.Color.BLACK);            
        
        try {
            BasicStroke bs = new BasicStroke(2);
            g.setStroke(bs);
            NVLines vlines = new NVLines(g, intHeight, intLength, graphFactor, data, zoomfactor, (byte)2);
            /*
            //For vertical line
            for(int j = 0; j < intLength; j++) {
                bs = new BasicStroke(1*zoomfactor);
                g.setStroke(bs);
                g.drawLine((int)(j*zoomfactor*Integer.parseInt(data[1])/graphFactor), 0,  (int)(j*zoomfactor*Integer.parseInt(data[1])/graphFactor), (int)(intHeight*zoomfactor*Integer.parseInt(data[0])/graphFactor));
            }
            //For horizental line
            for(int i = 0; i < intHeight; i++) {
                bs = new BasicStroke(1*zoomfactor);
                g.setStroke(bs);
                g.drawLine(0, (int)(i*zoomfactor*Integer.parseInt(data[0])/graphFactor), (int)(intLength*zoomfactor*Integer.parseInt(data[1])/graphFactor), (int)(i*zoomfactor*Integer.parseInt(data[0])/graphFactor));
            }
           */
            //vlines.join();
            while(vlines.isAlive()) {
                new Logging("INFO",getClass().getName(),"Main thread will be alive till the child thread is live",null);
                Thread.sleep(100);
            }
            new Logging("INFO",getClass().getName(),"Main thread run is over",null);
            //vlines = null;
        } catch(InterruptedException ex) {
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            new Logging("SEVERE",getClass().getName(),"InterruptedException: Main thread interrupted",ex);
        } catch(Exception ex){
            new Logging("SEVERE",getClass().getName(),"Exception: operation failed",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
        /*
        BasicStroke bs = new BasicStroke(2);
        g.setStroke(bs);

        for(int i = 0; i < intHeight; i++) {
            for(int j = 0; j < intLength; j++) {
                if((j%(Integer.parseInt(data[0])/graphFactor))==0){
                    bs = new BasicStroke(1*(float)zoomfactor);
                    g.setStroke(bs);
                }else{
                    bs = new BasicStroke(1);
                    g.setStroke(bs);
                }
                g.drawLine((int)(j*zoomfactor*Integer.parseInt(data[1])/graphFactor), 0,  (int)(j*zoomfactor*Integer.parseInt(data[1])/graphFactor), (int)(intHeight*Integer.parseInt(data[0])/graphFactor));
            }
            if((i%(Integer.parseInt(data[1])/graphFactor))==0){
                bs = new BasicStroke(1*(float)zoomfactor);
                g.setStroke(bs);
            }else{
                bs = new BasicStroke(1);
                g.setStroke(bs);
            }
            g.drawLine(0, (int)(i*zoomfactor*Integer.parseInt(data[0])/graphFactor), (int)(intLength*Integer.parseInt(data[1])/graphFactor), (int)(i*zoomfactor*Integer.parseInt(data[0])/graphFactor));
        }
        */
        g.dispose();
        lblStatus.setText(objDictionaryAction.getWord("GOTGRIDVIEW"));
        return objEditBufferedImage;
    }
    /**
     * exportGraph
     * <p>
     * This method is used for exporting artwork.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for exporting artwork.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void exportGraph() { 
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
            lblStatus.setText(objDictionaryAction.getWord("ACTIONSAVEGRAPHFILE"));
            try {
                BufferedImage objEditBufferedImage = null;
                
                FileChooser fileChoser=new FileChooser();
                fileChoser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTBMP")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
                FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("BMP (.bmp)", "*.bmp");
                fileChoser.getExtensionFilters().add(extFilterBMP);
                File file=fileChoser.showSaveDialog(artworkStage);
                if(file==null)
                    return;
                else
                    artworkStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+file.getAbsoluteFile().getName()+"]");
                if (!file.getName().endsWith("bmp")) 
                    file = new File(file.getPath() +".bmp");
                ImageIO.write(getArtworkGraph(objEditBufferedImage), "BMP", file);
                
                if(objConfiguration.getBlnAuthenticateService()){
                    ArrayList<File> filesToZip=new ArrayList<>();
                    filesToZip.add(file);
                    String zipFilePath=file.getAbsolutePath()+".zip";
                    String passwordToZip = objConfiguration.getObjUser().getStrAppPassword();//file.getName();
                    new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                    file.delete();
                }
                objEditBufferedImage = null;
                lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+file.getCanonicalPath());
            } catch (IOException ex) {
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            } catch(OutOfMemoryError ex){
                zoomOutAction();
                lblStatus.setText(objDictionaryAction.getWord("MAXVALUE"));           
            } catch (Exception ex) {
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        }
        System.gc();
    }
    /**
     * getArtworkGraph
     * <p>
     * This method is used for exporting artwork.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for exporting artwork.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private BufferedImage getArtworkGraph(BufferedImage objEditBufferedImage) throws OutOfMemoryError {
        lblStatus.setText(objDictionaryAction.getWord("GETGRAPHVIEW"));
        int intHeight=(int)objBufferedImage.getHeight();
        int intLength=(int)objBufferedImage.getWidth();

        String[] data =objArtwork.getObjConfiguration().getStrGraphSize().split("x");            
        //box resized
        int x_inc = (int)Math.round(zoomfactor*Integer.parseInt(data[1])/graphFactor);
        int y_inc = (int)Math.round(zoomfactor*Integer.parseInt(data[0])/graphFactor);
        //resized image
        objEditBufferedImage= new BufferedImage(intLength*x_inc, intHeight*y_inc,BufferedImage.TYPE_INT_RGB);
        Graphics2D g = objEditBufferedImage.createGraphics();
        g.drawImage(objBufferedImage, 0, 0, intLength*x_inc, intHeight*y_inc, null);
        g.setColor(java.awt.Color.BLACK);            

        try {
            BasicStroke bs = new BasicStroke(2);
            g.setStroke(bs);
            g.drawLine(0, intHeight*y_inc, intLength*x_inc, intHeight*y_inc);
            g.drawLine(intLength*x_inc, 0, intLength*x_inc, intHeight*y_inc);
            NVLines vlines = new NVLines(g, intHeight, intLength, graphFactor, data, zoomfactor, (byte)3);
            /*
            //For vertical line
            for(int j = 0; j < intLength; j++) {
                if((j%(Integer.parseInt(data[0])))==0){
                    bs = new BasicStroke(2*zoomfactor);
                    g.setStroke(bs);
                }else{
                    bs = new BasicStroke(1*zoomfactor);
                    g.setStroke(bs);
                }
                g.drawLine((int)(j*zoomfactor*Integer.parseInt(data[1])/graphFactor), 0,  (int)(j*zoomfactor*Integer.parseInt(data[1])/graphFactor), (int)(intHeight*zoomfactor*Integer.parseInt(data[0])/graphFactor));
            }
            //For horizental line
            for(int i = 0; i < intHeight; i++) {
                if((i%(Integer.parseInt(data[1])))==0){
                    bs = new BasicStroke(2*zoomfactor);
                    g.setStroke(bs);
                }else{
                    bs = new BasicStroke(1*zoomfactor);
                    g.setStroke(bs);
                }
                g.drawLine(0, (int)(i*zoomfactor*Integer.parseInt(data[0])/graphFactor), (int)(intLength*zoomfactor*Integer.parseInt(data[1])/graphFactor), (int)(i*zoomfactor*Integer.parseInt(data[0])/graphFactor));
            }    
           */
            //vlines.join();
            while(vlines.isAlive()) {
                new Logging("INFO",getClass().getName(),"Main thread will be alive till the child thread is live",null);
                Thread.sleep(100);
            }
            new Logging("INFO",getClass().getName(),"Main thread run is over",null);
            //vlines = null;
         } catch(InterruptedException ex) {
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            new Logging("SEVERE",getClass().getName(),"InterruptedException: Main thread interrupted",ex);
         } catch(Exception ex){
            new Logging("SEVERE",getClass().getName(),"Exception: operation failed",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
         }
        /*
        BasicStroke bs = new BasicStroke(2*(float)zoomfactor);
        g.setStroke(bs);
        for(int i = 0; i < (int)objBufferedImage.getHeight(); i++) {
            for(int j = 0; j < intLength; j++) {
                if((j%(Integer.parseInt(data[0])/graphFactor))==0){
                    bs = new BasicStroke(2*(float)zoomfactor);
                    g.setStroke(bs);
                }else{
                    bs = new BasicStroke(1*(float)zoomfactor);
                    g.setStroke(bs);
                }
                g.drawLine((int)(j*zoomfactor*objArtwork.getObjConfiguration().getIntBoxSize()*Integer.parseInt(data[1])/graphFactor), 0,  (int)(j*zoomfactor*objArtwork.getObjConfiguration().getIntBoxSize()*Integer.parseInt(data[1])/graphFactor), (int)((int)objBufferedImage.getHeight()*zoomfactor*objArtwork.getObjConfiguration().getIntBoxSize()*Integer.parseInt(data[0])/graphFactor));
            }
            if((i%(Integer.parseInt(data[1])/graphFactor))==0){
                bs = new BasicStroke(2*(float)zoomfactor);
                g.setStroke(bs);
            }else{
                bs = new BasicStroke(1*(float)zoomfactor);
                g.setStroke(bs);
            }
            g.drawLine(0, (int)(i*zoomfactor*objArtwork.getObjConfiguration().getIntBoxSize()*Integer.parseInt(data[0])/graphFactor), (int)(intLength*zoomfactor*objArtwork.getObjConfiguration().getIntBoxSize())*Integer.parseInt(data[1])/graphFactor, (int)(i*zoomfactor*objArtwork.getObjConfiguration().getIntBoxSize()*Integer.parseInt(data[0])/graphFactor));
        }
        */
        g.dispose();
        lblStatus.setText(objDictionaryAction.getWord("GOTGRAPHVIEW"));
        return objEditBufferedImage;              
    }    
    /**
     * loadArtwork
     * <p>
     * This method is used for initializing GUI for artwork.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for initializing GUI for artwork.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void loadArtwork(){
        try{
            objArtworkAction = new ArtworkAction();
            objArtworkAction.getArtwork(objArtwork);
            objArtworkAction = new ArtworkAction();
            if(objArtworkAction.countArtworkUsage(objArtwork.getStrArtworkId())>0 || objArtwork.getStrArtworkAccess().equalsIgnoreCase("Public")){
                isNew = true;                       
            }else{
                isNew = false;                        
            }
            byte[] bytArtworkThumbnil=objArtwork.getBytArtworkThumbnil();
            SeekableStream stream = new ByteArraySeekableStream(bytArtworkThumbnil);
            String[] names = ImageCodec.getDecoderNames(stream);
            ImageDecoder dec = ImageCodec.createImageDecoder(names[0], stream, null);
            RenderedImage im = dec.decodeAsRenderedImage();
            objBufferedImage = PlanarImage.wrapRenderedImage(im).getAsBufferedImage();
            bytArtworkThumbnil=null;
            
            objGraphics2D = objBufferedImage.createGraphics();
            objGraphics2D.drawImage(objBufferedImage, 0, 0, objBufferedImage.getWidth(), objBufferedImage.getHeight(), null);
            objGraphics2D.setColor(java.awt.Color.BLACK);
                        
            objUR.clear();
            objUR.doCommand("Load Artwork", objBufferedImage);
            //objURF.store(objBufferedImage);
                
            System.gc();
            objConfiguration.setStrRecentArtwork(objArtwork.getStrArtworkId());
            isEditingMode = false;
            isWorkingMode = true; 
            selectedMenu = "FILE";
            menuHighlight();
            plotViewActionMode = 1;
            initArtworkValue();
        } catch (Exception ex) {
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
        }
    }
    /**
     * printArtwork
     * <p>
     * <p>
     * This method is used for printing artwork
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for creating GUI and running third party application.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void printMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONPRINTFILE"));
        BufferedImage printImage= new BufferedImage(objBufferedImage.getWidth(), objBufferedImage.getHeight(),BufferedImage.TYPE_INT_RGB);
        try {            
            if(plotViewActionMode==1){
                int intLength = (int)(((objConfiguration.getIntDPI()*objBufferedImage.getWidth())/objConfiguration.getIntEPI())*zoomfactor);
                int intHeight = (int)(((objConfiguration.getIntDPI()*objBufferedImage.getHeight())/objConfiguration.getIntPPI())*zoomfactor);
                BufferedImage tempBI=new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
                Graphics2D g=tempBI.createGraphics();
                g.drawImage(objBufferedImage, 0, 0, intLength, intHeight, null);
                g.dispose();
                printImage=tempBI;
                //Graphics2D g = printImage.createGraphics();
                //g.drawImage(objBufferedImage, 0, 0, objBufferedImage.getWidth(), objBufferedImage.getHeight(), null);
                //g.dispose();
            }else if(plotViewActionMode==10){
                Graphics2D g = printImage.createGraphics();
                g.drawImage(objBufferedImage, 0, 0, objBufferedImage.getWidth(), objBufferedImage.getHeight(), null);
                g.dispose();
            }else if(plotViewActionMode==11){
                Graphics2D g = printImage.createGraphics();
                g.drawImage(objBufferedImage, 0, 0, objBufferedImage.getWidth(), objBufferedImage.getHeight(), null);
                g.dispose();
            }else{
                printImage = SwingFXUtils.fromFXImage(artwork.getImage(), null);
            }
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);  
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
        new PrintView(printImage, plotViewActionMode, objArtwork.getStrArtworkName(), objConfiguration.getObjUser().getStrName(), objConfiguration.getStrGraphSize());
    }
    /**
     * populateEditToolbar
     * <p>
     * Function use for drawing tool bar for menu item Edit,
     * and binding events for each tools. 
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */
    private void populateEditToolbarMenu(){
        //For drop down menus
        menuSelected.setTooltip(new Tooltip(objDictionaryAction.getWord("EDIT")));
        
        MenuItem undoEditMI = new MenuItem(objDictionaryAction.getWord("UNDO"));
        MenuItem redoEditMI = new MenuItem(objDictionaryAction.getWord("REDO"));
        MenuItem jacquardConversionEditMI = new MenuItem(objDictionaryAction.getWord("JACQUARDCONVERSIONEDIT"));
        MenuItem graphCorrectionEditMI = new MenuItem(objDictionaryAction.getWord("GRAPHCORRECTIONEDIT"));
        MenuItem resizeEditMI = new MenuItem(objDictionaryAction.getWord("RESIZESCALE"));
        MenuItem repeatEditMI = new MenuItem(objDictionaryAction.getWord("REPEATORIENTATION"));
        MenuItem colorPropertiesEditMI = new MenuItem(objDictionaryAction.getWord("COLORADJUST"));
        MenuItem reduceColorEditMI = new MenuItem(objDictionaryAction.getWord("REDUCECOLOR"));
        MenuItem sketchEditMI = new MenuItem(objDictionaryAction.getWord("ARTWORKSKETCH"));
        MenuItem pencilToolEditMI = new MenuItem(objDictionaryAction.getWord("PENCILTOOL"));
        MenuItem eraserToolEditMI = new MenuItem(objDictionaryAction.getWord("ERASERTOOL"));
        MenuItem sprayToolEditMI = new MenuItem(objDictionaryAction.getWord("SPRAYTOOL"));
        MenuItem fillToolEditMI = new MenuItem(objDictionaryAction.getWord("FILLTOOL"));
        MenuItem transformOperationEditMI = new MenuItem(objDictionaryAction.getWord("TRANSFORMOPERATIONEDIT"));
        MenuItem paintOperationEditMI = new MenuItem(objDictionaryAction.getWord("PAINTOPERATIONEDIT"));
        
        undoEditMI.setAccelerator(new KeyCodeCombination(KeyCode.Z, KeyCombination.CONTROL_DOWN));
        redoEditMI.setAccelerator(new KeyCodeCombination(KeyCode.Y, KeyCombination.CONTROL_DOWN));
        jacquardConversionEditMI.setAccelerator(new KeyCodeCombination(KeyCode.J, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        graphCorrectionEditMI.setAccelerator(new KeyCodeCombination(KeyCode.G, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        resizeEditMI.setAccelerator(new KeyCodeCombination(KeyCode.R, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        repeatEditMI.setAccelerator(new KeyCodeCombination(KeyCode.R, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        colorPropertiesEditMI.setAccelerator(new KeyCodeCombination(KeyCode.K, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        reduceColorEditMI.setAccelerator(new KeyCodeCombination(KeyCode.K, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        sketchEditMI.setAccelerator(new KeyCodeCombination(KeyCode.O, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));        
        pencilToolEditMI.setAccelerator(new KeyCodeCombination(KeyCode.P, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        eraserToolEditMI.setAccelerator(new KeyCodeCombination(KeyCode.E, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        sprayToolEditMI.setAccelerator(new KeyCodeCombination(KeyCode.Q, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        fillToolEditMI.setAccelerator(new KeyCodeCombination(KeyCode.F, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        transformOperationEditMI.setAccelerator(new KeyCodeCombination(KeyCode.W, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        paintOperationEditMI.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        
        undoEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/undo.png"));
        redoEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/redo.png"));
        jacquardConversionEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/assign_artwork_weave.png"));        
        graphCorrectionEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/graph_editor.png"));
        resizeEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move.png"));
        repeatEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/repeat.png"));
        colorPropertiesEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/color_palette.png"));
        reduceColorEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/color_editor.png"));
        sketchEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/sketch_edittool.png"));
        pencilToolEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/pencil.png"));
        eraserToolEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/eraser.png"));
        sprayToolEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/spray_tool.png"));
        fillToolEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fill_tool.png"));
        transformOperationEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/edit.png"));
        paintOperationEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/technical_info.png"));
        
        //add enable disbale condition
        if(!isWorkingMode){
            undoEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/undo.png"));
            undoEditMI.setDisable(true);
            redoEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/redo.png"));
            redoEditMI.setDisable(true);
            jacquardConversionEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/assign_artwork_weave.png"));
            jacquardConversionEditMI.setDisable(true);
            graphCorrectionEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/graph_editor.png"));
            graphCorrectionEditMI.setDisable(true);
            resizeEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move.png"));
            resizeEditMI.setDisable(true);
            repeatEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/repeat.png"));
            repeatEditMI.setDisable(true);
            colorPropertiesEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/color_palette.png"));
            colorPropertiesEditMI.setDisable(true);
            reduceColorEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/color_editor.png"));
            reduceColorEditMI.setDisable(true);
            sketchEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/sketch_edittool.png"));
            sketchEditMI.setDisable(true);
            pencilToolEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/pencil.png"));
            pencilToolEditMI.setDisable(true);
            eraserToolEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/eraser.png"));
            eraserToolEditMI.setDisable(true);
            sprayToolEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/spray_tool.png"));
            sprayToolEditMI.setDisable(true);
            fillToolEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fill_tool.png"));
            fillToolEditMI.setDisable(true);
            transformOperationEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/edit.png"));
            transformOperationEditMI.setDisable(true);
            paintOperationEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/technical_info.png"));
            paintOperationEditMI.setDisable(true);
        }else{
            undoEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/undo.png"));
            undoEditMI.setDisable(false);
            redoEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/redo.png"));
            redoEditMI.setDisable(false);
            jacquardConversionEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/assign_artwork_weave.png"));
            jacquardConversionEditMI.setDisable(false); 
            graphCorrectionEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/graph_editor.png"));
            graphCorrectionEditMI.setDisable(false);
            resizeEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move.png"));
            resizeEditMI.setDisable(false);
            repeatEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/repeat.png"));
            repeatEditMI.setDisable(false);
            colorPropertiesEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/color_palette.png"));
            colorPropertiesEditMI.setDisable(false);
            reduceColorEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/color_editor.png"));
            reduceColorEditMI.setDisable(false);
            sketchEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/sketch_edittool.png"));
            sketchEditMI.setDisable(false);
            pencilToolEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/pencil.png"));
            pencilToolEditMI.setDisable(false);
            eraserToolEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/eraser.png"));
            eraserToolEditMI.setDisable(false);
            sprayToolEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/spray_tool.png"));
            sprayToolEditMI.setDisable(false);
            fillToolEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fill_tool.png"));
            fillToolEditMI.setDisable(false);
            transformOperationEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/edit.png"));
            transformOperationEditMI.setDisable(false);
            paintOperationEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/technical_info.png"));
            paintOperationEditMI.setDisable(false);
        }
        //Add the action to Buttons.
        undoEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                undoAction();
            }
        });
        redoEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                redoAction();
            }
        });
        jacquardConversionEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                jacquardConverstionAction();
            }
        });
        graphCorrectionEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                graphCorrectionAction();
            }
        });
        resizeEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                resizeArtwork();
            }
        });
        repeatEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                repeatOrientationAction();
            }
        });
        colorPropertiesEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                colorPropertiesAction();
            }
        });
        reduceColorEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                colorReductionAction();
            }
        });
        sketchEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                artworkSketch();
            }
        });
        pencilToolEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {               
                pencilToolAction();
            }
        });        
        eraserToolEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {             
                eraserToolAction();
            }
        });
        sprayToolEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                sprayToolAction();
            }
        });  
        fillToolEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) { 
                fillToolAction();
            }
        });
        transformOperationEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                transformOperationAction();
            }
        });
        paintOperationEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                paintOperationAction();
            }
        });
        menuSelected.getItems().addAll(undoEditMI, redoEditMI, new SeparatorMenuItem(), jacquardConversionEditMI, graphCorrectionEditMI, resizeEditMI, repeatEditMI, colorPropertiesEditMI, reduceColorEditMI, sketchEditMI, pencilToolEditMI, eraserToolEditMI, sprayToolEditMI, fillToolEditMI, transformOperationEditMI);
    }  
    private void populateEditToolbar(){
        // undo edit item
        Button undoEditBtn = new Button(); 
        undoEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/undo.png"));
        undoEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("UNDO")+" (Ctrl+Z)\n"+objDictionaryAction.getWord("TOOLTIPUNDO")));
        undoEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        undoEditBtn.getStyleClass().addAll("toolbar-button");   
        // redo edit item 
        Button redoEditBtn = new Button(); 
        redoEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/redo.png"));
        redoEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("REDO")+" (Ctrl+Y)\n"+objDictionaryAction.getWord("TOOLTIPREDO")));
        redoEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        redoEditBtn.getStyleClass().addAll("toolbar-button");         
        // Pattern edit item
        Button jacquardConversionEditBtn = new Button();
        jacquardConversionEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/assign_artwork_weave.png"));
        jacquardConversionEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("JACQUARDCONVERSIONEDIT")+" (Ctrl+Shift+J)\n"+objDictionaryAction.getWord("TOOLTIPJACQUARDCONVERSIONEDIT")));
        jacquardConversionEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        jacquardConversionEditBtn.getStyleClass().add("toolbar-button");    
        // graph edit item
        Button graphCorrectionEditBtn = new Button();
        graphCorrectionEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/graph_editor.png"));
        graphCorrectionEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("GRAPHCORRECTIONEDIT")+" (Alt+Shift+G)\n"+objDictionaryAction.getWord("TOOLTIPGRAPHCORRECTIONEDIT")));
        graphCorrectionEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        graphCorrectionEditBtn.getStyleClass().add("toolbar-button");    
        // artwork resize edit item
        Button resizeEditBtn = new Button(); 
        resizeEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move.png"));
        resizeEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("RESIZESCALE")+" (Ctrl+Shift+R)\n"+objDictionaryAction.getWord("TOOLTIPRESIZESCALE")));
        resizeEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        resizeEditBtn.getStyleClass().addAll("toolbar-button"); 
        // repaet
        Button repeatEditBtn = new Button(); 
        repeatEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/repeat.png"));
        repeatEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("REPEATORIENTATION")+" (Alt+Shift+R)\n"+objDictionaryAction.getWord("TOOLTIPREPEATORIENTATION")));
        repeatEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        repeatEditBtn.getStyleClass().addAll("toolbar-button"); 
        // color reduce edit item
        Button colorPropertiesEditBtn = new Button(); 
        colorPropertiesEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/color_palette.png"));
        colorPropertiesEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("COLORADJUST")+" (Ctrl+Shift+K)\n"+objDictionaryAction.getWord("TOOLTIPCOLORADJUST")));
        colorPropertiesEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        colorPropertiesEditBtn.getStyleClass().addAll("toolbar-button");
        // color reduce edit item
        Button reduceColorEditBtn = new Button(); 
        reduceColorEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/color_editor.png"));
        reduceColorEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("REDUCECOLOR")+" (Alt+Shift+K)\n"+objDictionaryAction.getWord("TOOLTIPREDUCECOLOR")));
        reduceColorEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        reduceColorEditBtn.getStyleClass().addAll("toolbar-button"); 
        // artwork resize edit item
        Button sketchEditBtn = new Button(); 
        sketchEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/sketch_edittool.png"));
        sketchEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ARTWORKSKETCH")+" (Alt+Shift+O)\n"+objDictionaryAction.getWord("TOOLTIPARTWORKSKETCH")));
        sketchEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        sketchEditBtn.getStyleClass().addAll("toolbar-button"); 
        // Vertical mirror edit item
        Button pencilToolEditBtn = new Button(); 
        pencilToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/pencil.png"));
        pencilToolEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("PENCILTOOL")+" (Alt+Shift+P)\n"+objDictionaryAction.getWord("TOOLTIPPENCILTOOL")));
        pencilToolEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        pencilToolEditBtn.getStyleClass().addAll("toolbar-button"); 
        // horizontal mirros edit item
        Button eraserToolEditBtn = new Button(); 
        eraserToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/eraser.png"));
        eraserToolEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ERASERTOOL")+" (Alt+Shift+E)\n"+objDictionaryAction.getWord("TOOLTIPERASERTOOL")));
        eraserToolEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        eraserToolEditBtn.getStyleClass().addAll("toolbar-button"); 
        // spray tool edit item
        Button sprayToolEditBtn= new Button(); 
        sprayToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/spray_tool.png"));
        sprayToolEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SPRAYTOOL")+" (Alt+Shift+Q)\n"+objDictionaryAction.getWord("TOOLTIPSPRAYTOOL")));
        sprayToolEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        sprayToolEditBtn.getStyleClass().addAll("toolbar-button");                 
        // clockwise rotation edit item
        Button fillToolEditBtn= new Button(); 
        fillToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fill_tool.png"));
        fillToolEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("FILLTOOL")+" (Alt+Shift+F)\n"+objDictionaryAction.getWord("TOOLTIPFILLTOOL")));
        fillToolEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        fillToolEditBtn.getStyleClass().addAll("toolbar-button"); 
        // transform Operation edit item
        Button transformOperationEditBtn = new Button();
        transformOperationEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/edit.png"));
        transformOperationEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("TRANSFORMOPERATIONEDIT")+" (Ctrl+Shift+W)\n"+objDictionaryAction.getWord("TOOLTIPTRANSFORMOPERATIONEDIT")));
        transformOperationEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        transformOperationEditBtn.getStyleClass().add("toolbar-button");    
        // Toolkit group
        Button paintOperationEditBtn = new Button(); 
        paintOperationEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/technical_info.png"));
        paintOperationEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("PAINTOPERATIONEDIT")+" (Ctrl+Alt+A)\n"+objDictionaryAction.getWord("TOOLTIPPAINTOPERATIONEDIT")));
        paintOperationEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        paintOperationEditBtn.getStyleClass().addAll("toolbar-button");
        
        //add enable disbale condition
        if(!isWorkingMode){
            undoEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/undo.png"));
            undoEditBtn.setDisable(true);
            undoEditBtn.setCursor(Cursor.WAIT);
            redoEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/redo.png"));
            redoEditBtn.setDisable(true);
            redoEditBtn.setCursor(Cursor.WAIT);
            jacquardConversionEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/assign_artwork_weave.png"));
            jacquardConversionEditBtn.setDisable(true);
            jacquardConversionEditBtn.setCursor(Cursor.WAIT);
            graphCorrectionEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/graph_editor.png"));
            graphCorrectionEditBtn.setDisable(true);
            graphCorrectionEditBtn.setCursor(Cursor.WAIT);
            resizeEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move.png"));
            resizeEditBtn.setDisable(true);
            resizeEditBtn.setCursor(Cursor.WAIT);
            repeatEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/repeat.png"));
            repeatEditBtn.setDisable(true);
            repeatEditBtn.setCursor(Cursor.WAIT);
            colorPropertiesEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/color_palette.png"));
            colorPropertiesEditBtn.setDisable(true);
            colorPropertiesEditBtn.setCursor(Cursor.WAIT);
            reduceColorEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/color_editor.png"));
            reduceColorEditBtn.setDisable(true);
            reduceColorEditBtn.setCursor(Cursor.WAIT);
            sketchEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/sketch_edittool.png"));
            sketchEditBtn.setDisable(true);
            sketchEditBtn.setCursor(Cursor.WAIT);
            pencilToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/pencil.png"));
            pencilToolEditBtn.setDisable(true);
            pencilToolEditBtn.setCursor(Cursor.WAIT);
            eraserToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/eraser.png"));
            eraserToolEditBtn.setDisable(true);
            eraserToolEditBtn.setCursor(Cursor.WAIT);
            sprayToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/spray_tool.png"));
            sprayToolEditBtn.setDisable(true);
            sprayToolEditBtn.setCursor(Cursor.WAIT);
            fillToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fill_tool.png"));
            fillToolEditBtn.setDisable(true);
            fillToolEditBtn.setCursor(Cursor.WAIT);
            transformOperationEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/edit.png"));
            transformOperationEditBtn.setDisable(true);
            transformOperationEditBtn.setCursor(Cursor.WAIT);              
            paintOperationEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/technical_info.png"));
            paintOperationEditBtn.setDisable(true);
            paintOperationEditBtn.setCursor(Cursor.WAIT);  
        }else{
            undoEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/undo.png"));
            undoEditBtn.setDisable(false);
            undoEditBtn.setCursor(Cursor.HAND); 
            redoEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/redo.png"));
            redoEditBtn.setDisable(false);
            redoEditBtn.setCursor(Cursor.HAND);
            jacquardConversionEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/assign_artwork_weave.png"));
            jacquardConversionEditBtn.setDisable(false);
            jacquardConversionEditBtn.setCursor(Cursor.HAND);
            graphCorrectionEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/graph_editor.png"));
            graphCorrectionEditBtn.setDisable(false);
            graphCorrectionEditBtn.setCursor(Cursor.HAND);
            resizeEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move.png"));
            resizeEditBtn.setDisable(false);
            resizeEditBtn.setCursor(Cursor.HAND); 
            repeatEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/repeat.png"));
            repeatEditBtn.setDisable(false);
            repeatEditBtn.setCursor(Cursor.HAND); 
            colorPropertiesEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/color_palette.png"));
            colorPropertiesEditBtn.setDisable(false);
            colorPropertiesEditBtn.setCursor(Cursor.HAND); 
            reduceColorEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/color_editor.png"));
            reduceColorEditBtn.setDisable(false);
            reduceColorEditBtn.setCursor(Cursor.HAND); 
            sketchEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/sketch_edittool.png"));
            sketchEditBtn.setDisable(false);
            sketchEditBtn.setCursor(Cursor.HAND);
            pencilToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/pencil.png"));
            pencilToolEditBtn.setDisable(false);
            pencilToolEditBtn.setCursor(Cursor.HAND); 
            eraserToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/eraser.png"));
            eraserToolEditBtn.setDisable(false);
            eraserToolEditBtn.setCursor(Cursor.HAND); 
            sprayToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/spray_tool.png"));
            sprayToolEditBtn.setDisable(false);
            sprayToolEditBtn.setCursor(Cursor.HAND); 
            fillToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fill_tool.png"));
            fillToolEditBtn.setDisable(false);
            fillToolEditBtn.setCursor(Cursor.HAND); 
            transformOperationEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/edit.png"));
            transformOperationEditBtn.setDisable(false);
            transformOperationEditBtn.setCursor(Cursor.HAND);            
            paintOperationEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/technical_info.png"));
            paintOperationEditBtn.setDisable(false);
            paintOperationEditBtn.setCursor(Cursor.HAND);
        }
        //Add the action to Buttons.
        undoEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                undoAction();
            }
        });
        redoEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                redoAction();
            }
        });
       jacquardConversionEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                jacquardConverstionAction();
            }
        });
        graphCorrectionEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                graphCorrectionAction();
            }
        });
        resizeEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                resizeArtwork();
            }
        });
        repeatEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                repeatOrientationAction();
            }
        });
        colorPropertiesEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                colorPropertiesAction();
            }
        });
        reduceColorEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                colorReductionAction();
            }
        });
        sketchEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                artworkSketch();
            }
        });
        pencilToolEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {                
                pencilToolAction();
            }
        });        
        eraserToolEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {                
                eraserToolAction();
            }
        });
        sprayToolEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {                
                sprayToolAction();
            }
        });
        fillToolEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {                
                fillToolAction();
            }
        });
        transformOperationEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                transformOperationAction();
            }
        });
        paintOperationEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {                
                paintOperationAction();
            }
        });        
        //Create some Buttons.        
        FlowPane flow = new FlowPane();        
        flow.setPrefWrapLength(objConfiguration.WIDTH*.95);
        flow.getStyleClass().addAll("flow");
        //Add the Buttons to the ToolBar.
        flow.getChildren().addAll(undoEditBtn,redoEditBtn,jacquardConversionEditBtn,graphCorrectionEditBtn,resizeEditBtn,repeatEditBtn,colorPropertiesEditBtn,reduceColorEditBtn,sketchEditBtn,pencilToolEditBtn,eraserToolEditBtn,sprayToolEditBtn,fillToolEditBtn,transformOperationEditBtn);
        flow.setAlignment(Pos.TOP_LEFT);
        toolBar.getItems().addAll(flow);
    }
    
    /**
     * Copies pixels of img1 into img2
     * @param img1
     * @param img2
     * @return 
     */
    private boolean copyBufferedImagePixels(BufferedImage img1, BufferedImage img2){
        if(img1.getWidth()!=img2.getWidth() || img1.getHeight()!=img2.getHeight())
            return false;
        else{
            for(int x=0; x<img1.getWidth(); x++)
                for(int y=0; y<img1.getHeight(); y++)
                    img2.setRGB(x, y, img1.getRGB(x, y));
            return true;
        }
    }
//////////////////////////// Edit Menu Action ///////////////////////////
    /**
     * undoAction
     * <p>
     * This method is used for undo.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for creating GUI of undo.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void undoAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONUNDO"));
        try {
            if(objUR.canUndo()){
            //if(objURF.canUndo()==1){
                objUR.undo();
                objBufferedImage = (BufferedImage)objUR.getObjData();
                BufferedImage newBI=new BufferedImage(objBufferedImage.getWidth(), objBufferedImage.getHeight(), BufferedImage.TYPE_INT_RGB);
                copyBufferedImagePixels(objBufferedImage, newBI);
                objBufferedImage=newBI;
                //objBufferedImage = objURF.undo();
                initArtworkValue();
                lblStatus.setText(objDictionaryAction.getWord("UNDO")+": "+objDictionaryAction.getWord("SUCCESS"));
            }else{
                lblStatus.setText(objDictionaryAction.getWord("MAXUNDO"));
            }
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Operation Undo",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }   
    }
    /**
     * redoAction
     * <p>
     * This method is used for redo.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void redoAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONREDO"));
        try {
            if(objUR.canRedo()){
            //if(objURF.canRedo()==1){
                objUR.redo();
                objBufferedImage = (BufferedImage)objUR.getObjData();
                BufferedImage newBI=new BufferedImage(objBufferedImage.getWidth(), objBufferedImage.getHeight(), BufferedImage.TYPE_INT_RGB);
                copyBufferedImagePixels(objBufferedImage, newBI);
                objBufferedImage=newBI;
                //objBufferedImage = objURF.redo();
                initArtworkValue();
                lblStatus.setText(objDictionaryAction.getWord("REDO")+": "+objDictionaryAction.getWord("SUCCESS"));
            }else{
                lblStatus.setText(objDictionaryAction.getWord("MAXREDO"));
            }
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Operation Redo",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    /**
     * jacquardConverstionAction
     * <p>
     * This method is used for creating GUI of artwork to fill weaves.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for creating GUI of artwork to fill weaves.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void jacquardConverstionAction(){
        frontSideViewAction();
        isEditingMode = true;
        plotEditActionMode = 2;//editJacquard = 2 
        lblStatus.setText(objDictionaryAction.getWord("ACTIONJACQUARDCONVERSIONEDIT"));
        plotEditWeave();
        
        artworkChildStage = new Stage();
        artworkChildStage.initOwner(artworkStage);
        artworkChildStage.initStyle(StageStyle.UTILITY);
        //artworkPopupStage.initModality(Modality.WINDOW_MODAL);
        GridPane popup=new GridPane();
        popup.setId("popup");
        //popup.setAlignment(Pos.CENTER);
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
               
        Label lblNote = new Label(objDictionaryAction.getWord("HELP"));
        lblNote.setWrapText(true);
        lblNote.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        lblNote.setTooltip(new Tooltip(objDictionaryAction.getWord("HELPWEAVEFILLTOOL")));
        //lblNote.setPrefWidth(objConfiguration.WIDTH*0.15);
        lblNote.setAlignment(Pos.CENTER);
        lblNote.setStyle("-fx-font: bold italic 11pt Arial; -fx-text-fill: #FF0000;");
        popup.add(lblNote, 0, 0, 2, 1); 

        Separator sepHor = new Separator();
        sepHor.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHor, 0, 1);
        GridPane.setColumnSpan(sepHor, 2);
        popup.getChildren().add(sepHor);

        Label fillToolWeave = new Label(objDictionaryAction.getWord("WEAVE")+" : ");
        fillToolWeave.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPDEFAULTWEAVE")));
        popup.add(fillToolWeave, 0, 2);
        final Label weaveDefaultTF = new Label();
        weaveDefaultTF.setText((objWeave!=null && objWeave.getStrWeaveID()!="")?objWeave.getStrWeaveID():objDictionaryAction.getWord("NOITEM"));
        weaveDefaultTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPDEFAULTWEAVE")));
        popup.add(weaveDefaultTF, 1, 2); 
        final ImageView weaveIV = new ImageView(new Image("/media/assign_weave.png"));
        weaveIV.setFitHeight(50);
        weaveIV.setFitWidth(50);
        popup.add(weaveIV, 0, 3);
        final Hyperlink weaveDefaultHL = new Hyperlink();
        weaveDefaultHL.setText(objDictionaryAction.getWord("CHANGEDEFAULT"));
        weaveDefaultHL.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/browse.png"));
        weaveDefaultHL.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPDEFAULTWEAVE")));
        popup.add(weaveDefaultHL, 1, 3);   
        if(objWeave!=null && objWeave.getStrWeaveID()!=null && objWeave.getStrWeaveID()!=""){
            try {    
                weaveDefaultTF.setText(objWeave.getStrWeaveName());
                byte[] bytArtworkThumbnil=objWeave.getBytWeaveThumbnil();
                SeekableStream stream = new ByteArraySeekableStream(bytArtworkThumbnil);
                String[] names = ImageCodec.getDecoderNames(stream);
                ImageDecoder dec = ImageCodec.createImageDecoder(names[0], stream, null);
                RenderedImage im = dec.decodeAsRenderedImage();
                weaveIV.setImage(SwingFXUtils.toFXImage(PlanarImage.wrapRenderedImage(im).getAsBufferedImage(), null));
                bytArtworkThumbnil=null;
                System.gc();
            } catch (Exception ex) {
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("INVALIDWEAVE"));
            }
        }else{
            weaveDefaultTF.setText(objDictionaryAction.getWord("NOITEM"));
            weaveIV.setImage(new Image("/media/assign_weave.png"));
            artwork.setCursor(Cursor.HAND);
            lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
        }
        weaveDefaultHL.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    artwork.setCursor(Cursor.HAND);
                    //artwork.setCursor(new ImageCursor(new Image(objConfiguration.getStrMouse()+"/fill_tool.png")));
                    //artwork.setCursor(new ImageCursor(new Image(objConfiguration.getStrMouse()+"/fill_tool.png"), 4, 4));
                    //artwork.setCursor(Cursor.cursor(objConfiguration.getStrMouse()+"/z_fill_tool.png"));
                    //System.err.println("size+"+ImageCursor.getBestSize(0, 0));
                    objWeave= new Weave();
                    objWeave.setObjConfiguration(objArtwork.getObjConfiguration());
                    WeaveImportView objWeaveImportView= new WeaveImportView(objWeave);

                    if(objWeave!=null && objWeave.getStrWeaveID()!=null && objWeave.getStrWeaveID()!=""){
                        weaveDefaultTF.setText(objWeave.getStrWeaveName());

                        byte[] bytArtworkThumbnil=objWeave.getBytWeaveThumbnil();
                        SeekableStream stream = new ByteArraySeekableStream(bytArtworkThumbnil);
                        String[] names = ImageCodec.getDecoderNames(stream);
                        ImageDecoder dec = ImageCodec.createImageDecoder(names[0], stream, null);
                        RenderedImage im = dec.decodeAsRenderedImage();
                        weaveIV.setImage(SwingFXUtils.toFXImage(PlanarImage.wrapRenderedImage(im).getAsBufferedImage(), null));
                        bytArtworkThumbnil=null;
                        System.gc();
                    }else{
                        weaveDefaultTF.setText(objDictionaryAction.getWord("NOITEM"));
                        weaveIV.setImage(new Image("/media/assign_weave.png"));
                        artwork.setCursor(Cursor.HAND);
                        lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
                    }
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("INVALIDWEAVE"));
                }
            }
        });     

        Label fillToolArea = new Label(objDictionaryAction.getWord("FILLTOOLAREA")+" : ");
        fillToolArea.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPFILLTOOLAREA")));
        popup.add(fillToolArea, 0, 4);
        final ComboBox fillToolAreaCB = new ComboBox();
        fillToolAreaCB.getItems().addAll("Color All Instance","Color Single Instance");
        fillToolAreaCB.setValue(strFillArea);
        fillToolAreaCB.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPFILLTOOLAREA")));
        fillToolAreaCB.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(!newValue){
                    strFillArea = fillToolAreaCB.getValue().toString();
                }
            }
        });  
        popup.add(fillToolAreaCB, 1, 4);        

        Label fillToolBorderType = new Label(objDictionaryAction.getWord("FILLTOOLBORDERTYPE")+" : ");
        fillToolBorderType.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPFILLTOOLBORDERTYPE")));
        popup.add(fillToolBorderType, 0, 5);
        final ComboBox fillToolBorderTypeCB = new ComboBox();
        fillToolBorderTypeCB.getItems().addAll("All Inner Border","All Outer Border", "Left-Right Border", "Top-Bottom Border","Left Only Border", "Right Only Border","Top Only Border", "Bottom Only Border");
        fillToolBorderTypeCB.setValue(strFillBorder);
        fillToolBorderTypeCB.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPFILLTOOLBORDERTYPE")));
        fillToolBorderTypeCB.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(!newValue){
                    strFillBorder = fillToolBorderTypeCB.getValue().toString();
                }
            }
        });  
        popup.add(fillToolBorderTypeCB, 1, 5);  

        Label fillToolBorderWidth = new Label(objDictionaryAction.getWord("FILLTOOLBORDERWIDTH")+" : ");
        fillToolBorderWidth.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPFILLTOOLBORDERWIDTH")));
        popup.add(fillToolBorderWidth, 0, 6);
        final TextField fillToolBorderWidthTF= new TextField(String.valueOf(intFillSize)){
            @Override public void replaceText(int start, int end, String text) {
                if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
                if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        fillToolBorderWidthTF.setMinHeight(25);
        fillToolBorderWidthTF.setMaxHeight(50);
        fillToolBorderWidthTF.setPromptText(objDictionaryAction.getWord("TOOLTIPFILLTOOLBORDERWIDTH"));
        fillToolBorderWidthTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPFILLTOOLBORDERWIDTH")));
        fillToolBorderWidthTF.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                // validate name when name TextField loses focus
                if(!newValue){ 
                    if(fillToolBorderWidthTF.getText().trim().length()==0){ // name field empty
                        fillToolBorderWidthTF.setText(String.valueOf(intFillSize));
                        lblStatus.setText(objDictionaryAction.getWord("BALNKINPUT"));
                    } else {   // valid data
                        intFillSize = Byte.parseByte(fillToolBorderWidthTF.getText());
                    }
                }
            }
        }); 
        popup.add(fillToolBorderWidthTF, 1, 6);  

        artworkChildStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                artworkChildStage.close();
                frontSideViewAction();
            }
        });
        Scene popupScene = new Scene(popup);
        popupScene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        artworkChildStage.setScene(popupScene);
        artworkChildStage.setResizable(false);
        artworkChildStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("WINDOWWEAVEASSIGNMENT"));
        artworkChildStage.showAndWait();
    }
    private void artworkWeaveAssingmentEvent(MouseEvent event){
        try {
            artwork.setCursor(Cursor.HAND);
            int evx=(int) (event.getX()/zoomfactor)%objBufferedImage.getWidth();
            int evy=(int) (event.getY()/zoomfactor)%objBufferedImage.getHeight();
            if(plotViewActionMode==10 || plotViewActionMode==11){
                String[] data = objArtwork.getObjConfiguration().getStrGraphSize().split("x");
                evx=(int)(event.getX()/(Integer.parseInt(data[1])*zoomfactor))%objBufferedImage.getWidth();
                evy=(int)(event.getY()/(Integer.parseInt(data[0])*zoomfactor))%objBufferedImage.getHeight();
            }
            int clickColor = objBufferedImage.getRGB(evx, evy);
            byte[][] shapeMatrix = new byte[objBufferedImage.getWidth()][objBufferedImage.getHeight()];

            if(event.isAltDown() || event.isControlDown()){
                Weave objWeaveNew = new Weave();
                objWeaveNew.setObjConfiguration(objWeave.getObjConfiguration());
                objWeaveNew.setStrWeaveID(objWeave.getStrWeaveID());  
                WeaveEditView objWeaveEditView = new WeaveEditView(objWeaveNew);
                if(objWeaveNew.getStrWeaveID()!=null){
                    objWeave.setBytWeaveThumbnil(objWeaveNew.getBytWeaveThumbnil());
                    objWeave.setStrWeaveID(objWeaveNew.getStrWeaveID());
                    objWeaveNew = null;
                    WeaveAction objWeaveAction = new WeaveAction();
                    objWeaveAction.getWeave(objWeave);  
                    objWeaveAction.extractWeaveContent(objWeave);
                } else{
                    lblStatus.setText("weave edit: Your last action to assign weave pattern was not completed"+objWeave.getStrWeaveID());
                }
                System.gc();
            }

            WeaveAction objWeaveAction = new WeaveAction();
            objWeaveAction.extractWeaveContent(objWeave);
            int h= objWeave.getDesignMatrix().length;
            int w= objWeave.getDesignMatrix()[0].length;

            BufferedImage objEditBufferedImage= new BufferedImage((int)(objBufferedImage.getWidth()), (int)(objBufferedImage.getHeight()),BufferedImage.TYPE_INT_RGB);
            Graphics2D g = objEditBufferedImage.createGraphics();
            g.drawImage(objBufferedImage, 0, 0, (int)(objBufferedImage.getWidth()), (int)(objBufferedImage.getHeight()), null);

            if(strFillArea.equalsIgnoreCase("Color Single Instance")){
                objArtworkAction= new ArtworkAction();
                shapeMatrix=objArtworkAction.getArtworkBoundary(evx, evy, objEditBufferedImage);

                objEditBufferedImage = new BufferedImage((int)(objBufferedImage.getWidth()), (int)(objBufferedImage.getHeight()),BufferedImage.TYPE_INT_RGB);
                g = objEditBufferedImage.createGraphics();
                g.drawImage(objBufferedImage, 0, 0, (int)(objBufferedImage.getWidth()), (int)(objBufferedImage.getHeight()), null);
                g.dispose();

                // printing boundary matrix
                for(int x=0; x<shapeMatrix.length; x++){
                    for(int y=0; y<shapeMatrix[0].length; y++){
                        if(shapeMatrix[x][y]==1){
                            if(objWeave.getDesignMatrix()[x%w][y%h]==1)
                                objEditBufferedImage.setRGB(x, y, -1);
                            else
                                objEditBufferedImage.setRGB(x, y, objEditBufferedImage.getRGB(x, y));
                        }
                    }
                }
            }else{
                // printing boundary matrix
                for(int x=0; x<shapeMatrix.length; x++){
                    for(int y=0; y<shapeMatrix[0].length; y++){
                        if(clickColor==objEditBufferedImage.getRGB(x,y)){
                            shapeMatrix[x][y]=1;
                            if(objWeave.getDesignMatrix()[x%w][y%h]==1)
                                objEditBufferedImage.setRGB(x, y, -1);
                            else
                                objEditBufferedImage.setRGB(x, y, objEditBufferedImage.getRGB(x, y));
                        }else{
                            shapeMatrix[x][y]=0;
                        }
                    }
                }
            }
            //overlapping boundary                    
            if(strFillBorder.equalsIgnoreCase("Left-Right Border")){            
                for(int i=0;i<intFillSize;i++) { 
                    for(int y=0; y<shapeMatrix[0].length; y++){
                        boolean check = true;
                        for(int x=0; x<shapeMatrix.length; x++){
                            if(shapeMatrix[x][y]==1){
                                if(check){
                                    shapeMatrix[x][y]=-1;//boundry point
                                    check = false;
                                }
                            }else{
                                check = true;
                            }
                        }
                    }
                    for(int y=0; y<shapeMatrix[0].length; y++){
                        boolean check = true;
                        for(int x=shapeMatrix.length-1; x>=0; x--){
                            if(shapeMatrix[x][y]==1){
                                if(check){
                                    shapeMatrix[x][y]=-1;//boundry point
                                    check = false;
                                }
                            }else{
                                check = true;
                            }
                        }
                    }
                    for(int x=0; x<shapeMatrix.length; x++){
                        for(int y=0; y<shapeMatrix[0].length; y++){
                            if(shapeMatrix[x][y]==-1){
                                shapeMatrix[x][y]=0;
                                objEditBufferedImage.setRGB(x, y, clickColor);
                            }
                        }
                    }
                }
                /*int b=0;
                for(int y=0; y<shapeMatrix[0].length; y++){
                    b=0;
                    for(int x=0; x<shapeMatrix.length; x++){
                        if(shapeMatrix[x][y]==1)
                            b++;
                        else
                            b=0;
                        if(b==1){
                            for(int c=0; (c<intFillBorder)&&((x+c)<shapeMatrix.length); c++){
                                if(shapeMatrix[x+c][y]==1)
                                    shapeMatrix[x+c][y]=-1;
                                else
                                    break;
                            }
                        }
                    }
                }*/
            } else if(strFillBorder.equalsIgnoreCase("Top-Bottom Border")){
                for(int i=0;i<intFillSize;i++) { 
                    for(int x=0; x<shapeMatrix.length; x++){
                        boolean check = true;
                        for(int y=0; y<shapeMatrix[0].length; y++){
                            if(shapeMatrix[x][y]==1){
                                if(check){
                                    shapeMatrix[x][y]=-1;//boundry point
                                    check = false;
                                }
                            }else{
                                check = true;
                            }
                        }                                
                    }
                    for(int x=0; x<shapeMatrix.length; x++){
                        boolean check = true;
                        for(int y=shapeMatrix[0].length-1; y>=0; y--){
                            if(shapeMatrix[x][y]==1){
                                if(check){
                                    shapeMatrix[x][y]=-1;//boundry point
                                    check = false;
                                }
                            }else{
                                check = true;
                            }
                        }
                    }
                    for(int x=0; x<shapeMatrix.length; x++){
                        for(int y=0; y<shapeMatrix[0].length; y++){
                            if(shapeMatrix[x][y]==-1){
                                shapeMatrix[x][y]=0;
                                objEditBufferedImage.setRGB(x, y, clickColor);
                            }
                        }
                    }
                }
            } else if(strFillBorder.equalsIgnoreCase("Left Only Border")){
                for(int i=0;i<intFillSize;i++) { 
                    for(int y=0; y<shapeMatrix[0].length; y++){
                        boolean check = true;
                        for(int x=0; x<shapeMatrix.length; x++){
                            if(shapeMatrix[x][y]==1){
                                if(check){
                                    shapeMatrix[x][y]=-1;//boundry point
                                    check = false;
                                }
                            }else{
                                check = true;
                            }
                        }
                    }
                    for(int x=0; x<shapeMatrix.length; x++){
                        for(int y=0; y<shapeMatrix[0].length; y++){
                            if(shapeMatrix[x][y]==-1){
                                shapeMatrix[x][y]=0;
                                objEditBufferedImage.setRGB(x, y, clickColor);
                            }
                        }
                    }
                }
            } else if(strFillBorder.equalsIgnoreCase("Right Only Border")){
                for(int i=0;i<intFillSize;i++) { 
                    for(int y=0; y<shapeMatrix[0].length; y++){
                        boolean check = true;
                        for(int x=shapeMatrix.length-1; x>=0; x--){
                            if(shapeMatrix[x][y]==1){
                                if(check){
                                    shapeMatrix[x][y]=-1;//boundry point
                                    check = false;
                                }
                            }else{
                                check = true;
                            }
                        }
                    }
                    for(int x=0; x<shapeMatrix.length; x++){
                        for(int y=0; y<shapeMatrix[0].length; y++){
                            if(shapeMatrix[x][y]==-1){
                                shapeMatrix[x][y]=0;
                                objEditBufferedImage.setRGB(x, y, clickColor);
                            }
                        }
                    }
                }
            }else if(strFillBorder.equalsIgnoreCase("Top Only Border")){
                for(int i=0;i<intFillSize;i++) { 
                    for(int x=0; x<shapeMatrix.length; x++){
                        boolean check = true;
                        for(int y=0; y<shapeMatrix[0].length; y++){
                            if(shapeMatrix[x][y]==1){
                                if(check){
                                    shapeMatrix[x][y]=-1;//boundry point
                                    check = false;
                                }
                            }else{
                                check = true;
                            }
                        }                                
                    }
                    for(int x=0; x<shapeMatrix.length; x++){
                        for(int y=0; y<shapeMatrix[0].length; y++){
                            if(shapeMatrix[x][y]==-1){
                                shapeMatrix[x][y]=0;
                                objEditBufferedImage.setRGB(x, y, clickColor);
                            }
                        }
                    }
                }
            } else if(strFillBorder.equalsIgnoreCase("Bottom Only Border")){
                for(int i=0;i<intFillSize;i++) { 
                    for(int x=0; x<shapeMatrix.length; x++){
                        boolean check = true;
                        for(int y=shapeMatrix[0].length-1; y>=0; y--){
                            if(shapeMatrix[x][y]==1){
                                if(check){
                                    shapeMatrix[x][y]=-1;//boundry point
                                    check = false;
                                }
                            }else{
                                check = true;
                            }
                        }
                    }
                    for(int x=0; x<shapeMatrix.length; x++){
                        for(int y=0; y<shapeMatrix[0].length; y++){
                            if(shapeMatrix[x][y]==-1){
                                shapeMatrix[x][y]=0;
                                objEditBufferedImage.setRGB(x, y, clickColor);
                            }
                        }
                    }
                }
            } else if(strFillBorder.equalsIgnoreCase("All Inner Border")){
                for(int i=0;i<intFillSize;i++) { 
                    /// [2] setting a border with bordercolour
                    for(int x=0; x<shapeMatrix.length; x++){
                        for(int y=0; y<shapeMatrix[0].length; y++){
                            if(shapeMatrix[x][y]==1){
                                if((x!=0)&&(y!=0)&&(x!=(int)(objBufferedImage.getWidth()-1))&&(y!=(int)(objBufferedImage.getHeight()-1))) {
                                    if(shapeMatrix[x][y-1]==0 || shapeMatrix[x][y+1]==0 || shapeMatrix[x-1][y]==0 || shapeMatrix[x+1][y]==0) {
                                        shapeMatrix[x][y]=-1;//boundry point
                                    }
                                }
                            }
                        }
                    }
                    for(int x=0; x<shapeMatrix.length; x++){
                        for(int y=0; y<shapeMatrix[0].length; y++){
                            if(shapeMatrix[x][y]==-1){
                                shapeMatrix[x][y]=0;
                                objEditBufferedImage.setRGB(x, y, clickColor);
                            }
                        }
                    }
                }
            } else{
                for(int i=0;i<intFillSize;i++) { 
                    for(int x=0; x<shapeMatrix.length; x++){
                        for(int y=0; y<shapeMatrix[0].length; y++){
                            if(shapeMatrix[x][y]==1){
                                if(x!=0 && y!=0) { //for (x-1.y-1)
                                    if(shapeMatrix[x-1][y-1]==0)
                                        shapeMatrix[x-1][y-1] = -1;
                                }
                                if(y!=0) { //for (x,y-1)
                                    if(shapeMatrix[x][y-1]==0)
                                        shapeMatrix[x][y-1] = -1;
                                }
                                if(x<(int)(objBufferedImage.getWidth()-1) && y!=0) { //for (x+1,y-1)
                                    if(shapeMatrix[x+1][y-1]==0)
                                        shapeMatrix[x+1][y-1] = -1;
                                }
                                if(x!=0) { //for (x-1,y)
                                    if(shapeMatrix[x-1][y]==0)
                                        shapeMatrix[x-1][y] = -1;
                                }
                                if(x<(int)(objBufferedImage.getWidth()-1)) { //for (x+1,y)
                                    if(shapeMatrix[x+1][y]==0)
                                        shapeMatrix[x+1][y] = -1;
                                }
                                if(x!=0 && y<(int)(objBufferedImage.getHeight()-1)) { //for (x-1,y+1)
                                    if(shapeMatrix[x-1][y+1]==0)
                                        shapeMatrix[x-1][y+1] = -1;
                                }
                                if(y<(int)(objBufferedImage.getHeight()-1)) { //for (x,y+1)
                                    if(shapeMatrix[x][y+1]==0)
                                        shapeMatrix[x][y+1] = -1;
                                }
                                if(x<(int)(objBufferedImage.getWidth()-1) && y<(int)(objBufferedImage.getHeight()-1)) { //for (x+1,y+1)
                                    if(shapeMatrix[x+1][y+1]==0)
                                        shapeMatrix[x+1][y+1] = -1;
                                }
                            }
                        }
                    }
                    for(int x=0; x<shapeMatrix.length; x++){
                        for(int y=0; y<shapeMatrix[0].length; y++){
                            if(shapeMatrix[x][y]==-1){
                                shapeMatrix[x][y]=1;
                                objEditBufferedImage.setRGB(x, y, clickColor);
                            }
                        }
                    }
                }
            }
            objBufferedImage = objEditBufferedImage;
            shapeMatrix = null;
            objEditBufferedImage = null;
            System.gc();
            initArtworkValue();
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"SQLException:artworkfilltool() : Error while viewing grid view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }   
    }    
    /**
     * graphCorrectionAction
     * <p>
     * This method is used for creating GUI of artwork reduce color pane.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void graphCorrectionAction(){
        try {
            frontSideViewAction();
            lblStatus.setText(objDictionaryAction.getWord("ACTIONGRAPHCORRECTIONEDIT"));
            //objUR.doCommand("Edit Graph", objBufferedImage);
            //objURF.store(objBufferedImage);            
            objArtworkAction = new ArtworkAction();
            isEditingMode=true;
            plotEditActionMode = 3; //editGraph = 3
            plotEditGraph();
            populateEditGraphPane();
            lblStatus.setText(objDictionaryAction.getWord("GOTGRAPHVIEW"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"editGraph() : Error while editing garph view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void plotEditGraph(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("GETGRIDVIEW"));
            int intHeight=(int)objBufferedImage.getHeight();
            int intLength=(int)objBufferedImage.getWidth();
            /////////////// for error correction  //////////////////
            if(objConfiguration.getBlnCorrectGraph()){
                BufferedImage objEditBufferedImage = new BufferedImage(intLength, intHeight,BufferedImage.TYPE_INT_RGB);
                Graphics2D g = objEditBufferedImage.createGraphics();
                g.drawImage(objBufferedImage, 0, 0, intLength, intHeight, null);
                g.dispose();
                //objURF.store(objEditBufferedImage);
                for(int x = 0; x < intHeight; x++) {
                    int count = 1;
                    int temp = objBufferedImage.getRGB(0, x);
                    for(int y = 0; y < intLength; y++) {
                        if(temp==objBufferedImage.getRGB(y, x))
                            count++;
                        else{
                            count = 1;
                            temp = objBufferedImage.getRGB(y, x);
                        }
                        if(count>objConfiguration.getIntFloatBind() && temp!=objBufferedImage.getRGB(0, 0)) {//colors.get(0).getRGB()
                            objBufferedImage.setRGB(y, x, objBufferedImage.getRGB(0, 0));
                            count = 0;
                        }
                    }
                }
                //objUR.doCommand("CorrectGraph", deepCopy(objEditBufferedImage));
                objConfiguration.setBlnCorrectGraph(false);
            }
            String[] data =objArtwork.getObjConfiguration().getStrGraphSize().split("x");//10x10
            //box resized
            int x_inc = (int)Math.round(zoomfactor*Integer.parseInt(data[1])/graphFactor);
            int y_inc = (int)Math.round(zoomfactor*Integer.parseInt(data[0])/graphFactor);
            //resized image
            objEditBufferedImage= new BufferedImage(intLength*x_inc, intHeight*y_inc,BufferedImage.TYPE_INT_RGB);
            objGraphics2D = objEditBufferedImage.createGraphics();
            objGraphics2D.drawImage(objBufferedImage, 0, 0, intLength*x_inc, intHeight*y_inc, null);
            objGraphics2D.setColor(java.awt.Color.BLACK); 
            System.err.println((intLength*x_inc)+"="+(intHeight*y_inc));
            try {
                BasicStroke bs = new BasicStroke(2);
                objGraphics2D.setStroke(bs);
                NVLines vlines = new NVLines(objGraphics2D, intHeight, intLength, graphFactor, data, zoomfactor, (byte)3);
                /*
                //For vertical line
                for(int j = 0; j < intLength; j++) {
                    if((j%(Integer.parseInt(data[0])))==0){
                        bs = new BasicStroke(2*zoomfactor);
                        objGraphics2D.setStroke(bs);
                    }else{
                        bs = new BasicStroke(1*zoomfactor);
                        objGraphics2D.setStroke(bs);
                    }
                    objGraphics2D.drawLine((int)(j*zoomfactor*Integer.parseInt(data[1])/graphFactor), 0,  (int)(j*zoomfactor*Integer.parseInt(data[1])/graphFactor), (int)(intHeight*zoomfactor*Integer.parseInt(data[0])/graphFactor));
                }
                //For horizental line
                for(int i = 0; i < intHeight; i++) {
                    if((i%(Integer.parseInt(data[1])))==0){
                        bs = new BasicStroke(2*zoomfactor);
                        objGraphics2D.setStroke(bs);
                    }else{
                        bs = new BasicStroke(1*zoomfactor);
                        objGraphics2D.setStroke(bs);
                    }
                    objGraphics2D.drawLine(0, (int)(i*zoomfactor*Integer.parseInt(data[0])/graphFactor), (int)(intLength*zoomfactor*Integer.parseInt(data[1])/graphFactor), (int)(i*zoomfactor*Integer.parseInt(data[0])/graphFactor));
                }
               */
                //vlines.join();
                while(vlines.isAlive()) {
                    new Logging("INFO",getClass().getName(),"Main thread will be alive till the child thread is live",null);
                    Thread.sleep(100);
                }
                new Logging("INFO",getClass().getName(),"Main thread run is over",null);
                //vlines = null;
            } catch(InterruptedException ex) {
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                new Logging("SEVERE",getClass().getName(),"InterruptedException: Main thread interrupted",ex);
            } catch(Exception ex){
                new Logging("SEVERE",getClass().getName(),"Exception: operation failed",ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }            
            /////////////// for error box  //////////////////
            if(objConfiguration.getBlnErrorGraph()){
                BufferedImage errorImage = ImageIO.read(getClass().getResource("/media/error.png"));
                /*for(int x = 0, p = 0; x < intHeight; x++,p+=(Integer.parseInt(data[0])*zoomfactor/graphFactor)) {
                    int count = 1;
                    int temp = objBufferedImage.getRGB(0, x);
                    for(int y = 0, q = 0; y < intLength; y++,q+=(Integer.parseInt(data[1])*zoomfactor/graphFactor)) {
                        if(temp==objBufferedImage.getRGB(y, x))
                            count++;
                        else{
                            count = 1;
                            temp = objBufferedImage.getRGB(y, x);
                        }
                        if(count>objConfiguration.getIntFloatBind() && temp!=objBufferedImage.getRGB(0, 0)) //colors.get(0).getRGB()
                            objGraphics2D.drawImage(errorImage, q, p, (int)(Integer.parseInt(data[1])*zoomfactor/graphFactor), (int)(Integer.parseInt(data[0])*zoomfactor/graphFactor), null);  
                    }
                }*/
                intHeight=(int)objBufferedImage.getHeight();
                intLength=(int)objBufferedImage.getWidth();
                for(int x = 0; x < intHeight; x++) {
                    int count = 1;
                    int temp = objBufferedImage.getRGB(0, x);
                    for(int y = 0; y < intLength; y++) {
                        if(temp==objBufferedImage.getRGB(y, x))
                            count++;
                        else{
                            count = 1;
                            temp = objBufferedImage.getRGB(y, x);
                        }
                        if(count>objConfiguration.getIntFloatBind() && temp!=objBufferedImage.getRGB(0, 0)) {//colors.get(0).getRGB()
                            objGraphics2D.drawImage(errorImage, y*x_inc, x*y_inc, x_inc, y_inc, null);
                            count = 0;
                        }
                    }
                }
            }
            //g.dispose();
            artwork.setImage(SwingFXUtils.toFXImage(objEditBufferedImage, null));
            container.setContent(artwork);
            //objBufferedImageesize = null;
            System.gc();
            lblStatus.setText(objDictionaryAction.getWord("GOTGRIDVIEW"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Exception:plotGridView() : Error while viewing grid view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch(OutOfMemoryError ex){
            new Logging("SEVERE",getClass().getName(),"OutOfMemoryError:plotGridView() : Error while viewing grid view",null);
            //graphFactor = (byte)(graphFactor*2);
            //plotEditGraph();
            zoomOutAction();
            lblStatus.setText(objDictionaryAction.getWord("MAXVALUE"));            
        }
    }
    private void artworkGraphEditEvent(MouseEvent event){
        String[] data = objArtwork.getObjConfiguration().getStrGraphSize().split("x");
        //box resized
        int x_inc = (int)Math.round(zoomfactor*Integer.parseInt(data[1])/graphFactor);
        int y_inc = (int)Math.round(zoomfactor*Integer.parseInt(data[0])/graphFactor);
        //position on actual image
        int x=(int)(event.getX()/x_inc);
        int y=(int)(event.getY()/y_inc);
        if(x>=objBufferedImage.getWidth() || y>=objBufferedImage.getHeight())
            return;
        //fill actual image
        if(event.isSecondaryButtonDown() || (event.getButton() == MouseButton.SECONDARY)){
            objBufferedImage.setRGB(x,y, editThreadSecondaryValue);
            objGraphics2D.setColor(new java.awt.Color(editThreadSecondaryValue));
        } else{
            objBufferedImage.setRGB(x,y, editThreadPrimaryValue);
            objGraphics2D.setColor(new java.awt.Color(editThreadPrimaryValue));
        }
        BasicStroke bs = new BasicStroke(1*zoomfactor);
        objGraphics2D.setStroke(bs);
        //new co-ordinates
        int x1 = x*x_inc;
        int y1 = y*y_inc;
        int x2 = (x+1)*x_inc;
        int y2 = (y+1)*y_inc;
        //draw filled color block using line
        /*
        bs = new BasicStroke(1);
        objGraphics2D.setStroke(bs);
        for(int i=x1; i<=x2;i++){
            objGraphics2D.drawLine(i, y1, i, y2);
        }
        */
        objGraphics2D.fillRect(x1,y1,x_inc,y_inc);
        
        objGraphics2D.setColor(java.awt.Color.BLACK);
        bs = new BasicStroke(2);
        objGraphics2D.setStroke(bs);
        objGraphics2D.drawLine(objBufferedImage.getWidth()*x_inc, 0, objBufferedImage.getWidth()*x_inc, objBufferedImage.getHeight()*y_inc);
        objGraphics2D.drawLine(0, objBufferedImage.getHeight()*y_inc, objBufferedImage.getWidth()*x_inc, objBufferedImage.getHeight()*y_inc);
        //bs = new BasicStroke(1*zoomfactor);
        //objGraphics2D.setStroke(bs);
        if(x%Integer.parseInt(data[0])==0){
            bs = new BasicStroke(2*zoomfactor);
            objGraphics2D.setStroke(bs);
        }else{
            bs = new BasicStroke(1*zoomfactor);
            objGraphics2D.setStroke(bs);
        }
        objGraphics2D.drawLine(x1, y1, x1, y2);
        if(y%Integer.parseInt(data[1])==0){
            bs = new BasicStroke(2*zoomfactor);
            objGraphics2D.setStroke(bs);
        }else{
            bs = new BasicStroke(1*zoomfactor);
            objGraphics2D.setStroke(bs);
        }
        objGraphics2D.drawLine(x1, y1, x2, y1);
        
        if((x+1)%Integer.parseInt(data[0])==0){
            bs = new BasicStroke(2*zoomfactor);
            objGraphics2D.setStroke(bs);
        }else{
            bs = new BasicStroke(1*zoomfactor);
            objGraphics2D.setStroke(bs);
        }
        objGraphics2D.drawLine(x2, y1, x2, y2);
        if((y+1)%Integer.parseInt(data[1])==0){
            bs = new BasicStroke(2*zoomfactor);
            objGraphics2D.setStroke(bs);
        }else{
            bs = new BasicStroke(1*zoomfactor);
            objGraphics2D.setStroke(bs);
        }
        objGraphics2D.drawLine(x1, y2, x2, y2);
                
        //objGraphics2D.fillRect((int)((int)(event.getX()/((Integer.parseInt(data[1])/graphFactor)*zoomfactor))*((Integer.parseInt(data[1])/graphFactor)*zoomfactor)), (int)((int)(event.getY()/((Integer.parseInt(data[0])/graphFactor)*zoomfactor))*((Integer.parseInt(data[0])/graphFactor)*zoomfactor)), (int)((Integer.parseInt(data[1])/graphFactor)*zoomfactor), (int)((Integer.parseInt(data[0])/graphFactor)*zoomfactor));
        //objGraphics2D.setColor(java.awt.Color.BLACK);
        //objGraphics2D.drawRect((int)((int)(event.getX()/((Integer.parseInt(data[1])/graphFactor)*zoomfactor))*((Integer.parseInt(data[1])/graphFactor)*zoomfactor)), (int)((int)(event.getY()/((Integer.parseInt(data[0])/graphFactor)*zoomfactor))*((Integer.parseInt(data[0])/graphFactor)*zoomfactor)), (int)(((Integer.parseInt(data[1])+0)/graphFactor)*zoomfactor), (int)(((Integer.parseInt(data[0])+0)/graphFactor)*zoomfactor));
        //objFabric.getFabricMatrix()[(int)(event.getY()/(Integer.parseInt(data[0])*zoomfactor*objFabric.getObjConfiguration().getIntBoxSize()))][(int)(event.getX()/(Integer.parseInt(data[1])*zoomfactor*objFabric.getObjConfiguration().getIntBoxSize()))] = (byte)editThreadPrimaryValue;
        //plotEditGraph();
        artwork.setImage(SwingFXUtils.toFXImage(objEditBufferedImage, null));
        container.setContent(artwork);
    }
    private void populateEditGraphPane(){
        artworkChildStage = new Stage();
        artworkChildStage.initOwner(artworkStage);
        artworkChildStage.initStyle(StageStyle.UTILITY);
        //dialogStage.initModality(Modality.WINDOW_MODAL);
        GridPane popup=new GridPane();
        popup.setId("popup");
        //popup.setAlignment(Pos.CENTER);
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        
        Label lblNote = new Label(objDictionaryAction.getWord("HELP"));
        lblNote.setWrapText(true);
        lblNote.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        lblNote.setTooltip(new Tooltip(objDictionaryAction.getWord("HELPGRAPHCORRECTION")));
        //lblNote.setPrefWidth(objConfiguration.WIDTH*0.15);
        lblNote.setAlignment(Pos.CENTER);
        lblNote.setStyle("-fx-font: bold italic 11pt Arial; -fx-text-fill: #FF0000;");
        popup.add(lblNote, 0, 0, 1, 1);
        
        String webColor = String.format("#%02X%02X%02X",colors.get(0).getRed(),colors.get(0).getGreen(),colors.get(0).getBlue());
        
        final Label lblLeft = new Label();
        lblLeft.setGraphic(new ImageView("/media/mouse_left.png"));
        lblLeft.setTooltip(new Tooltip(objDictionaryAction.getWord("LEFT")));
        //lblLeft.setPrefWidth(objConfiguration.WIDTH*0.15);
        lblLeft.setAlignment(Pos.CENTER);
        lblLeft.setStyle("-fx-font: bold italic 11pt Arial; -fx-text-fill: #FF0000; -fx-background-color: "+webColor+";");
        popup.add(lblLeft, 1, 0, 1, 1);
        final Label lblRight = new Label();
        lblRight.setGraphic(new ImageView("/media/mouse_right.png"));
        lblRight.setTooltip(new Tooltip(objDictionaryAction.getWord("RIGHT")));
        //lblRight.setPrefWidth(objConfiguration.WIDTH*0.15);
        lblRight.setAlignment(Pos.CENTER);
        lblRight.setStyle("-fx-font: bold italic 11pt Arial; -fx-text-fill: #FF0000; -fx-background-color: "+webColor+";");
        popup.add(lblRight, 2, 0, 1, 1);
        
        Separator sepHor1 = new Separator();
        sepHor1.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHor1, 0, 1);
        GridPane.setColumnSpan(sepHor1, 3);
        popup.getChildren().add(sepHor1);
        
        popup.add(new Label(objDictionaryAction.getWord("EDITTHREADTYPE")+" : "), 0, 2, 3, 1);
        GridPane editThreadCB = new GridPane();
        editThreadCB.setAlignment(Pos.TOP_LEFT);
        editThreadCB.setHgap(5);
        editThreadCB.setVgap(5);
        editThreadCB.setCursor(Cursor.HAND);
        //editThreadCB.getChildren().clear();
        for(int i=0; i<intColor; i++){
            webColor = String.format("#%02X%02X%02X",colors.get(i).getRed(),colors.get(i).getGreen(),colors.get(i).getBlue());
            final Label  lblColor  = new Label ();
            lblColor.setPrefSize(33, 33);
            lblColor.setStyle("-fx-background-color:"+webColor+"; -fx-border-color: #FFFFFF;");
            lblColor.setTooltip(new Tooltip("Color:"+i+"\n"+webColor+"\nR:"+colors.get(i).getRed()+"\nG:"+colors.get(i).getGreen()+"\nB:"+colors.get(i).getBlue()));
            lblColor.setUserData(colors.get(i).getRGB());
            lblColor.setId(webColor);
            lblColor.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if(event.isSecondaryButtonDown() || (event.getButton() == MouseButton.SECONDARY)){
                        editThreadSecondaryValue=Integer.parseInt(lblColor.getUserData().toString());
                        lblRight.setStyle("-fx-font: bold italic 11pt Arial; -fx-text-fill: #FF0000; -fx-background-color:"+lblColor.getId()+";");
                    } else{
                        editThreadPrimaryValue=Integer.parseInt(lblColor.getUserData().toString());
                        lblLeft.setStyle("-fx-font: bold italic 11pt Arial; -fx-text-fill: #FF0000; -fx-background-color:"+lblColor.getId()+";");
                    }
                }
            });
            editThreadCB.add(lblColor, i%6, i/6);
        }
        editThreadPrimaryValue=colors.get(0).getRGB();
        editThreadSecondaryValue=colors.get(0).getRGB();
        popup.add(editThreadCB, 0, 3, 3, 1);
        
        final CheckBox errorGraphCB = new CheckBox(objDictionaryAction.getWord("SHOWERRORGRAPH"));
        errorGraphCB.setSelected(objConfiguration.getBlnErrorGraph());
        errorGraphCB.selectedProperty().addListener(new ChangeListener<Boolean>() {
           public void changed(ObservableValue<? extends Boolean> ov,
             Boolean old_val, Boolean new_val) {
                objConfiguration.setBlnErrorGraph(errorGraphCB.isSelected());
                plotEditGraph();
            }
        });
        popup.add(errorGraphCB, 0, 4, 3, 1);
        
        final CheckBox correctGraphCB = new CheckBox(objDictionaryAction.getWord("SHOWCORRECTGRAPH"));
        correctGraphCB.setWrapText(true);
        correctGraphCB.setSelected(objConfiguration.getBlnCorrectGraph());
        correctGraphCB.selectedProperty().addListener(new ChangeListener<Boolean>() {
           public void changed(ObservableValue<? extends Boolean> ov,
             Boolean old_val, Boolean new_val) {
                objConfiguration.setBlnCorrectGraph(correctGraphCB.isSelected());
                plotEditGraph();
                //objConfiguration.setBlnCorrectGraph(false);
            }
        });
        popup.add(correctGraphCB, 0, 5, 3, 1);
                
        popup.add(new Label(objDictionaryAction.getWord("FLOATBINDSIZE")+" : "), 0, 6, 1, 1);
        final TextField doubleBindTF = new TextField(Integer.toString(objConfiguration.getIntFloatBind())){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        doubleBindTF.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(doubleBindTF.getText()!=null && doubleBindTF.getText()!="" && Integer.parseInt(doubleBindTF.getText())>0){
                    objConfiguration.setIntFloatBind(Integer.parseInt(doubleBindTF.getText()));
                    plotEditGraph();
                }
            }
        });
        doubleBindTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPFLOATBINDSIZE")));
        popup.add(doubleBindTF, 1, 6, 2, 1);
        
        artworkChildStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                artworkChildStage.close();
                graphViewAction();
                objConfiguration.setBlnCorrectGraph(false);
            }
        });
        Scene popupScene = new Scene(popup);
        popupScene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        artworkChildStage.setScene(popupScene);
        artworkChildStage.setResizable(false);
        artworkChildStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("WINDOWEDITGRAPH"));
        artworkChildStage.showAndWait(); 
    } 
    /**
     * resizeArtwork
     * <p>
     * This method is used for creating GUI of artwork resize pane.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for creating GUI of artwork resize pane.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */     
    private void resizeArtwork(){ 
        frontSideViewAction();
        lblStatus.setText(objDictionaryAction.getWord("ACTIONRESIZESCALE"));
        try{
            isEditingMode = true;
            //objURF.store(objBufferedImage);
            //objArtworkAction = new ArtworkAction();
            //objBufferedImage = objArtworkAction.getImageFromMatrix(artworkMatrix, colors);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(objBufferedImage, "png", baos);
            baos.flush();
            byte[] imageInByte = baos.toByteArray();  
            objArtwork.setBytArtworkThumbnil(imageInByte);
            imageInByte = null;
            baos.close();

            ArtworkDensityView objArtworkDensityView = new ArtworkDensityView(objArtwork);

            byte[] bytArtworkThumbnil=objArtwork.getBytArtworkThumbnil();
            SeekableStream stream = new ByteArraySeekableStream(bytArtworkThumbnil);
            String[] names = ImageCodec.getDecoderNames(stream);
            ImageDecoder dec = ImageCodec.createImageDecoder(names[0], stream, null);
            RenderedImage im = dec.decodeAsRenderedImage();
            objBufferedImage = PlanarImage.wrapRenderedImage(im).getAsBufferedImage();
            bytArtworkThumbnil=null;

            objUR.doCommand("Resize", deepCopy(objBufferedImage));
            
            objConfiguration.setIntPPI(objArtwork.getObjConfiguration().getIntPPI());
            objConfiguration.setIntEPI(objArtwork.getObjConfiguration().getIntEPI());
            objConfiguration.setIntHPI(objArtwork.getObjConfiguration().getIntHPI());
            objConfiguration.setStrGraphSize(objArtwork.getObjConfiguration().getStrGraphSize());
            lblStatus.setText(objDictionaryAction.getWord("SUCCESS"));
            isEditingMode = false;
            plotViewActionMode = 1;
            initArtworkValue();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"height property change",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    /**
     * repeatOrientationAction
     * <p>
     * This method is used for creating GUI of artwork resize pane.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for creating GUI of artwork resize pane.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void repeatOrientationAction(){
        frontSideViewAction();
        lblStatus.setText(objDictionaryAction.getWord("ACTIONREPEATORIENTATION")); 
        isEditingMode = true;
        artworkChildStage = new Stage();
        artworkChildStage.initOwner(artworkStage);
        artworkChildStage.initStyle(StageStyle.UTILITY);
        //artworkPopupStage.initModality(Modality.WINDOW_MODAL);
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setAlignment(Pos.CENTER);
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));

        final CheckBox rulerCB = new CheckBox(objDictionaryAction.getWord("RULERVIEW"));
        rulerCB.setSelected(objConfiguration.getBlnPunchCard());
        rulerCB.selectedProperty().addListener(new ChangeListener<Boolean>() {
           public void changed(ObservableValue<? extends Boolean> ov,
             Boolean old_val, Boolean new_val) {
             //objConfiguration.setBlnRuler(rulerCB.isSelected());
            lblStatus.setText(objDictionaryAction.getWord("ACTIONRULERVIEW"));
            hRuler = new Label();
            vRuler = new Label();

            bodyContainer.add(hRuler, 1, 0);
            bodyContainer.add(vRuler, 0, 1);
            //plotRulerLine();
            plotViewAction();
          }
        });
        popup.add(rulerCB, 0, 0, 2, 1);
        rulerCB.setDisable(true);
        
        final CheckBox multipleRepeatCB = new CheckBox(objDictionaryAction.getWord("MREPEAT"));
        multipleRepeatCB.setSelected(objConfiguration.getBlnMRepeat());        
        //multipleRepeatCB.setDisable(!objConfiguration.getBlnMRepeat());
        popup.add(multipleRepeatCB, 0, 2, 2, 1);
        
        Separator sepHor1 = new Separator();
        sepHor1.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHor1, 0, 3);
        GridPane.setColumnSpan(sepHor1, 2);
        popup.getChildren().add(sepHor1);
        
        final CheckBox freezeRepeatCB = new CheckBox(objDictionaryAction.getWord("FREEZEDESIGN"));
        freezeRepeatCB.setSelected(false);        
        freezeRepeatCB.setDisable(!objConfiguration.getBlnMRepeat());
        popup.add(freezeRepeatCB, 0, 4, 2, 1);
        
        Label vertical= new Label(objDictionaryAction.getWord("VREPEAT")+" :");
        //vertical.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        vertical.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPVREPEAT")));
        popup.add(vertical, 0, 5);
        final TextField verticalTF = new TextField(Integer.toString(editVerticalRepeatValue)){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        verticalTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPVREPEAT")));
        verticalTF.setDisable(!objConfiguration.getBlnMRepeat());
        popup.add(verticalTF, 1, 5);
        
        Label horizental= new Label(objDictionaryAction.getWord("HREPEAT")+" :");
        //horizental.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        horizental.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPHREPEAT")));
        popup.add(horizental, 0, 6);
        final TextField horizentalTF = new TextField(Integer.toString(editHorizontalRepeatValue)){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        horizentalTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPHREPEAT")));
        horizentalTF.setDisable(!objConfiguration.getBlnMRepeat());
        popup.add(horizentalTF, 1, 6);
        
        Label lblRepeatMode=new Label(objDictionaryAction.getWord("REPEATMODE"));
        lblRepeatMode.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPREPEATMODE")));
        popup.add(lblRepeatMode, 0, 7);
        final ChoiceBox repeatModeCB=new ChoiceBox();
        repeatModeCB.getItems().add(0, "Rectangular (Default)");
        repeatModeCB.getItems().add(1, "1/2 Horizontal");
        repeatModeCB.getItems().add(2, "1/2 Vertical");
        repeatModeCB.getItems().add(3, "1/3 Horizontal");
        repeatModeCB.getItems().add(4, "1/3 Vertical");
        repeatModeCB.getItems().add(5, "1/4 Horizontal");
        repeatModeCB.getItems().add(6, "1/4 Vertical");
        repeatModeCB.getItems().add(7, "1/5 Horizontal");
        repeatModeCB.getItems().add(8, "1/5 Vertical");
        repeatModeCB.getItems().add(9, "1/6 Horizontal");
        repeatModeCB.getItems().add(10, "1/6 Vertical");        
        repeatModeCB.setValue(editRepeatMode);
        repeatModeCB.setDisable(!objConfiguration.getBlnMRepeat());
        popup.add(repeatModeCB, 1, 7);
        
        Label lblRepeatMirror=new Label(objDictionaryAction.getWord("MIRROR"));
        lblRepeatMirror.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPMIRROR")));
        popup.add(lblRepeatMirror, 0, 8);
        final ChoiceBox repeatMirrorCB=new ChoiceBox();
        repeatMirrorCB.getItems().add(0, "No Mirroring (Default)");        
        repeatMirrorCB.getItems().add(1, "Vertical Mirror");
        repeatMirrorCB.getItems().add(2, "Horizontal Mirror");
        repeatMirrorCB.getItems().add(3, "Vertical-Horizontal Mirror");
        repeatMirrorCB.setValue(editMirrorMode);
        repeatMirrorCB.setDisable(!objConfiguration.getBlnMRepeat());
        popup.add(repeatMirrorCB, 1, 8);
        
        multipleRepeatCB.selectedProperty().addListener(new ChangeListener<Boolean>() {
           public void changed(ObservableValue<? extends Boolean> ov,
             Boolean old_val, Boolean new_val) {
             objConfiguration.setBlnMRepeat(multipleRepeatCB.isSelected());
             verticalTF.setDisable(!objConfiguration.getBlnMRepeat());
             horizentalTF.setDisable(!objConfiguration.getBlnMRepeat());
             repeatModeCB.setDisable(!objConfiguration.getBlnMRepeat());
             repeatMirrorCB.setDisable(!objConfiguration.getBlnMRepeat());
             freezeRepeatCB.setDisable(!objConfiguration.getBlnMRepeat());
             
             //plotViewAction();
          }
        });
        
        Button btnApply = new Button(objDictionaryAction.getWord("APPLY"));
        btnApply.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/update.png"));
        btnApply.setTooltip(new Tooltip(objDictionaryAction.getWord("ACTIONAPPLY")));
        //btnApply.setMaxWidth(Double.MAX_VALUE);
        btnApply.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                try {
                    if(freezeRepeatCB.isSelected() && objConfiguration.getBlnMRepeat()){
                        //objURF.store(objBufferedImage);
                        editHorizontalRepeatValue = Integer.parseInt(horizentalTF.getText());
                        editVerticalRepeatValue = Integer.parseInt(verticalTF.getText());

                        int vRepeat = editVerticalRepeatValue;
                        int hRepeat = editHorizontalRepeatValue;

                        if(repeatMirrorCB.getValue().toString().contains("Vertical Mirror")){
                            objArtworkAction = new ArtworkAction(false);
                            objBufferedImage = objArtworkAction.getImageVerticalMirrorRepeats(objBufferedImage);
                            vRepeat = (int)Math.ceil((double)vRepeat/2);
                        } else if(repeatMirrorCB.getValue().toString().contains("Horizontal Mirror")){
                            objArtworkAction = new ArtworkAction(false);
                            objBufferedImage = objArtworkAction.getImageHorizontalMirrorRepeats(objBufferedImage);
                            hRepeat = (int)Math.ceil((double)hRepeat/2);
                        } else if(repeatMirrorCB.getValue().toString().contains("Vertical-Horizontal Mirror")){
                            objArtworkAction = new ArtworkAction(false);
                            objBufferedImage = objArtworkAction.getImageHorizontalMirrorRepeats(objBufferedImage);
                            objBufferedImage = objArtworkAction.getImageVerticalMirrorRepeats(objBufferedImage);
                            hRepeat = (int)Math.ceil((double)hRepeat/2);
                            vRepeat = (int)Math.ceil((double)vRepeat/2);
                        } else {
                            vRepeat = editVerticalRepeatValue;
                            hRepeat = editHorizontalRepeatValue;
                        }

                        int vAling = 1;
                        int hAling = 1;
                        if(repeatModeCB.getValue().toString().contains("Vertical")){
                            String temp = repeatModeCB.getValue().toString();
                            vAling = Integer.parseInt(temp.substring(0, temp.indexOf("/")).trim());
                            hAling = Integer.parseInt(temp.substring(2, temp.indexOf("Vertical")).trim());
                        }else if(repeatModeCB.getValue().toString().contains("Horizontal")){
                            String temp = repeatModeCB.getValue().toString();
                            hAling = Integer.parseInt(temp.substring(0, temp.indexOf("/")).trim());
                            vAling = Integer.parseInt(temp.substring(2, temp.indexOf("Horizontal")).trim());
                        }else{
                            vAling = 1;
                            hAling = 1;
                        }
                        
                        /*editVerticalRepeatValue = Integer.parseInt(verticalTF.getText());
                        editHorizontalRepeatValue = Integer.parseInt(horizentalTF.getText());
                        editRepeatMode = repeatModeCB.getValue().toString();
                        editMirrorMode = repeatMirrorCB.getValue().toString();*/
                        
                        editRepeatMode = "Rectangular (Default)"; 
                        editMirrorMode = "No Mirroring (Default)"; 
                        editVerticalRepeatValue=1;
                        editHorizontalRepeatValue=1;
                        
                        objArtworkAction = new ArtworkAction(false);
                        objBufferedImage = objArtworkAction.designRepeat(objBufferedImage, vAling, hAling, vRepeat, hRepeat);
                        objUR.doCommand("Repeats", deepCopy(objBufferedImage));
                        
                        //Image image=SwingFXUtils.toFXImage(objBufferedImage, null);  
                        //artwork.setImage(image);
                        plotViewActionMode = 1;
                        initArtworkValue();
                    } else{
                        editVerticalRepeatValue = Integer.parseInt(verticalTF.getText());
                        editHorizontalRepeatValue = Integer.parseInt(horizentalTF.getText());
                        editRepeatMode = repeatModeCB.getValue().toString();
                        editMirrorMode = repeatMirrorCB.getValue().toString();
                        //System.err.println(editVerticalRepeatValue+":"+editHorizontalRepeatValue+"="+editRepeatMode+":"+editMirrorMode);
                        plotViewAction();
                    }                    
                    lblStatus.setText(objDictionaryAction.getWord("SUCCESS"));
                } catch (SQLException ex) {               
                    new Logging("SEVERE",getClass().getName(),"Operation apply",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                } catch (IOException ex) {
                    new Logging("SEVERE",getClass().getName(),"Operation apply",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                } catch(OutOfMemoryError ex){
                    undoAction();
                }
                System.gc();
                if(artworkChildStage!=null)
                    artworkChildStage.close();
            }
        });
        popup.add(btnApply, 0, 9);

        Button btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));
        btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
        btnCancel.setMaxWidth(Double.MAX_VALUE);
        btnCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                artworkChildStage.close();
            }
        });
        popup.add(btnCancel, 1, 9);

        Button btnAdvance = new Button(objDictionaryAction.getWord("ADVANCE"));
        btnAdvance.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        btnAdvance.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
        btnAdvance.setMaxWidth(Double.MAX_VALUE);
        btnAdvance.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                
                artworkChildStage.close();
            }
        });
        //popup.add(btnAdvance, 1, 10);
        
        artworkChildStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                artworkChildStage.close();
                frontSideViewAction();
            }
        });        
        Scene popupScene = new Scene(popup);
        popupScene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        artworkChildStage.setScene(popupScene);
        artworkChildStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("REPEATORIENTATION"));
        artworkChildStage.showAndWait();      
    }
    /**
     * plotRulerLine
     * <p>
     * Function use for plotting ruler View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */    
    private void plotRulerLine(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONRULERVIEW"));
        hRuler = new Label();
        vRuler = new Label();
        //containerGP.add(hRuler, 1, 0);
        //containerGP.add(vRuler, 0, 1);
    }
    private void plotOrientation(BufferedImage artworkBI, int intLength, int intHeight){
        try {            
            BufferedImage objBufferedImageesize = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = objBufferedImageesize.createGraphics();
            g.drawImage(artworkBI, 0, 0, intLength, intHeight, null);
            g.dispose();
            artworkBI = objBufferedImageesize;
            if(objConfiguration.getBlnMRepeat()){
                if(editVerticalRepeatValue>0 && editHorizontalRepeatValue>0){
                //objArtworkAction = new ArtworkAction();
                //objBufferedImageesize = objArtworkAction.getImageRepeat(objBufferedImage, editVerticalRepeatValue, editHorizontalRepeatValue);
                } else {
                    editVerticalRepeatValue=1;
                    editHorizontalRepeatValue=1;
                }
                int vRepeat = editVerticalRepeatValue;
                int hRepeat = editHorizontalRepeatValue;

                if(editMirrorMode.contains("Vertical-Horizontal Mirror")){
                    objArtworkAction = new ArtworkAction(false);
                    objBufferedImageesize = objArtworkAction.getImageHorizontalMirrorRepeats(objBufferedImageesize);
                    objBufferedImageesize = objArtworkAction.getImageVerticalMirrorRepeats(objBufferedImageesize);
                    hRepeat = (int)Math.ceil((double)hRepeat/2);
                    vRepeat = (int)Math.ceil((double)vRepeat/2);
                } else if(editMirrorMode.contains("Vertical Mirror")){
                    objArtworkAction = new ArtworkAction(false);
                    objBufferedImageesize = objArtworkAction.getImageVerticalMirrorRepeats(objBufferedImageesize);
                    vRepeat = (int)Math.ceil((double)vRepeat/2);
                } else if(editMirrorMode.contains("Horizontal Mirror")){
                    objArtworkAction = new ArtworkAction(false);
                    objBufferedImageesize = objArtworkAction.getImageHorizontalMirrorRepeats(objBufferedImageesize);
                    hRepeat = (int)Math.ceil((double)hRepeat/2);
                } else {
                    vRepeat = editVerticalRepeatValue;
                    hRepeat = editHorizontalRepeatValue;
                }

                int vAling = 1;
                int hAling = 1;
                if(editRepeatMode.contains("Vertical")){
                    vAling = Integer.parseInt(editRepeatMode.substring(0, editRepeatMode.indexOf("/")).trim());
                    hAling = Integer.parseInt(editRepeatMode.substring(2, editRepeatMode.indexOf("Vertical")).trim());
                }else if(editRepeatMode.contains("Horizontal")){
                    hAling = Integer.parseInt(editRepeatMode.substring(0, editRepeatMode.indexOf("/")).trim());
                    vAling = Integer.parseInt(editRepeatMode.substring(2, editRepeatMode.indexOf("Horizontal")).trim());
                }else{
                    vAling = 1;
                    hAling = 1;
                }

                //System.err.println(editVerticalRepeatValue+":"+editHorizontalRepeatValue+"="+vRepeat+":"+hRepeat+"="+vAling+":"+hAling);
                objArtworkAction = new ArtworkAction(false);
                objBufferedImageesize = objArtworkAction.designRepeat(objBufferedImageesize, vAling, hAling, vRepeat, hRepeat);
            }
            artwork.setImage(SwingFXUtils.toFXImage(objBufferedImageesize, null));
            container.setContent(artwork);
            objBufferedImageesize = null;
            artworkBI = null;
        } catch (SQLException ex) {               
            new Logging("SEVERE",getClass().getName(),"Operation apply",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (IOException ex) {               
            new Logging("SEVERE",getClass().getName(),"Operation apply",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch(OutOfMemoryError ex){
            //undoAction();
            objConfiguration.setBlnMRepeat(false);            
            lblStatus.setText(objDictionaryAction.getWord("MAXVALUE"));
        }
        System.gc();
    }
    /**
     * colorPropertiesAction
     * <p>
     * This method is used for creating GUI of artwork reduce color pane.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for creating GUI of artwork reduce color pane.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void colorPropertiesAction(){
        frontSideViewAction();
        SnapshotParameters snapshotParameters = new SnapshotParameters();
        snapshotParameters.setFill(Color.TRANSPARENT);
        objEditBufferedImage=SwingFXUtils.fromFXImage(artwork.snapshot(snapshotParameters,null),null);
        
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCOLORADJUST"));
        isEditingMode = true;
        artworkChildStage = new Stage();
        artworkChildStage.initOwner(artworkStage);
        artworkChildStage.initStyle(StageStyle.UTILITY);
        //dialogStage.initModality(Modality.WINDOW_MODAL);
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setAlignment(Pos.CENTER);
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        
        final ColorAdjust colorAdjust = new ColorAdjust();
        
        Label lblColorBrightness= new Label(objDictionaryAction.getWord("BRIGHTNESS")+" :");
        //lblColorBrightness.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        lblColorBrightness.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPBRIGHTNESS")));
        final Slider colorBrightnessSlider = new Slider(-1.0, 1.0, 0.0);    
        colorBrightnessSlider.setShowTickLabels(true);
        colorBrightnessSlider.setShowTickMarks(true);
        colorBrightnessSlider.setBlockIncrement(0.1);
        colorBrightnessSlider.setMajorTickUnit(0.5);
        //colorBrightnessSlider.setMinorTickCount(0.5);
        final Label colorBrightnessTF = new Label();
        colorBrightnessTF.setText(Double.toString(colorBrightnessSlider.getValue()));
        colorBrightnessTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPBRIGHTNESS")));
        popup.add(lblColorBrightness, 0, 0);
        popup.add(colorBrightnessSlider, 1, 0);    
        popup.add(colorBrightnessTF, 2, 0);   
        colorBrightnessSlider.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
                colorBrightnessTF.setText(String.format("%.2f", new_val));
                colorAdjust.setBrightness(new_val.doubleValue());
                artwork.setEffect(colorAdjust);
                //colorPropertiesModel(colorAdjust);
                SnapshotParameters snapshotParameters = new SnapshotParameters();
                snapshotParameters.setFill(Color.TRANSPARENT);
                objEditBufferedImage=SwingFXUtils.fromFXImage(artwork.snapshot(snapshotParameters,null),null);
            }
        });
        
        Label lblColorContrast= new Label(objDictionaryAction.getWord("CONTRAST")+" :");
        //lblColorContrast.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        lblColorContrast.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCONTRAST")));
        final Slider colorContrastSlider = new Slider(-1.0, 1.0, 0.0);    
        colorContrastSlider.setShowTickLabels(true);
        colorContrastSlider.setShowTickMarks(true);
        colorContrastSlider.setBlockIncrement(0.1);
        colorContrastSlider.setMajorTickUnit(0.5);
        //colorContrastSlider.setMinorTickCount(0.1);
        final Label colorContrastTF = new Label();
        colorContrastTF.setText(Double.toString(colorContrastSlider.getValue()));
        colorContrastTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCONTRAST")));
        popup.add(lblColorContrast, 0, 1);
        popup.add(colorContrastSlider, 1, 1);    
        popup.add(colorContrastTF, 2, 1);   
        colorContrastSlider.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
                colorContrastTF.setText(String.format("%.2f", new_val));
                colorAdjust.setContrast(new_val.doubleValue());
                artwork.setEffect(colorAdjust);
                //colorPropertiesModel(colorAdjust);
                SnapshotParameters snapshotParameters = new SnapshotParameters();
                snapshotParameters.setFill(Color.TRANSPARENT);
                objEditBufferedImage=SwingFXUtils.fromFXImage(artwork.snapshot(snapshotParameters,null),null);
            }
        });
        
        Label lblColorHue= new Label(objDictionaryAction.getWord("HUE")+" :");
        //lblColorHue.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        lblColorHue.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPHUE")));
        final Slider colorHueSlider = new Slider(-1.0, 1.0, 0.0);    
        colorHueSlider.setShowTickLabels(true);
        colorHueSlider.setShowTickMarks(true);
        colorHueSlider.setBlockIncrement(0.1);
        colorHueSlider.setMajorTickUnit(0.5);
        //colorHueSlider.setMinorTickCount(0.1);
        final Label colorHueTF = new Label();
        colorHueTF.setText(Double.toString(colorHueSlider.getValue()));
        colorHueTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPHUE")));
        popup.add(lblColorHue, 0, 2);
        popup.add(colorHueSlider, 1, 2);    
        popup.add(colorHueTF, 2, 2);   
        colorHueSlider.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
                colorHueTF.setText(String.format("%.2f", new_val));
                colorAdjust.setHue(new_val.doubleValue());
                artwork.setEffect(colorAdjust);
                //colorPropertiesModel(colorAdjust);
                SnapshotParameters snapshotParameters = new SnapshotParameters();
                snapshotParameters.setFill(Color.TRANSPARENT);
                objEditBufferedImage=SwingFXUtils.fromFXImage(artwork.snapshot(snapshotParameters,null),null);
            }
        });
        
        Label lblColorSaturation= new Label(objDictionaryAction.getWord("SATURATION")+" :");
        //lblColorSaturation.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        lblColorSaturation.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSATURATION")));
        final Slider colorSaturationSlider = new Slider(-1.0, 1.0, 0.0);    
        colorSaturationSlider.setShowTickLabels(true);
        colorSaturationSlider.setShowTickMarks(true);
        colorSaturationSlider.setBlockIncrement(0.1);
        colorSaturationSlider.setMajorTickUnit(0.5);
        //colorSaturationSlider.setMinorTickCount(0.1);
        final Label colorSaturationTF = new Label();
        colorSaturationTF.setText(Double.toString(colorSaturationSlider.getValue()));
        colorSaturationTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSATURATION")));
        popup.add(lblColorSaturation, 0, 3);
        popup.add(colorSaturationSlider, 1, 3);    
        popup.add(colorSaturationTF, 2, 3);   
        colorSaturationSlider.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
                colorSaturationTF.setText(String.format("%.2f", new_val));
                colorAdjust.setSaturation(new_val.doubleValue());
                artwork.setEffect(colorAdjust);
                //colorPropertiesModel(colorAdjust);
                SnapshotParameters snapshotParameters = new SnapshotParameters();
                snapshotParameters.setFill(Color.TRANSPARENT);
                objEditBufferedImage=SwingFXUtils.fromFXImage(artwork.snapshot(snapshotParameters,null),null);
            }
        });

        Separator sepHor1 = new Separator();
        sepHor1.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHor1, 0, 4);
        GridPane.setColumnSpan(sepHor1, 3);
        popup.getChildren().add(sepHor1);
        
        Label lblColorNoise= new Label(objDictionaryAction.getWord("NOISE")+" :");
        //lblColorNoise.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        lblColorNoise.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPNOISE")));
        final Slider colorNoiseSlider = new Slider(0.0, 250.0, 0.0);    
        colorNoiseSlider.setShowTickLabels(true);
        colorNoiseSlider.setShowTickMarks(true);
        colorNoiseSlider.setBlockIncrement(10.0);
        colorNoiseSlider.setMajorTickUnit(25.0);
        //colorSaturationSlider.setMinorTickCount(0.1);
        final Label colorNoiseTF = new Label();
        colorNoiseTF.setText(Double.toString(colorSaturationSlider.getValue()));
        colorNoiseTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPNOISE")));
        popup.add(lblColorNoise, 0, 9);
        popup.add(colorNoiseSlider, 1, 9);    
        popup.add(colorNoiseTF, 2, 9);   
        colorNoiseSlider.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
                colorNoiseTF.setText(String.format("%.2f", new_val));
                //colorPropertiesModel(colorAdjust);
                BufferedImage objBufferedImageesize = new BufferedImage(objEditBufferedImage.getWidth(), objEditBufferedImage.getHeight(),BufferedImage.TYPE_INT_RGB);
                Graphics2D g = objBufferedImageesize.createGraphics();
                g.drawImage(objEditBufferedImage, 0, 0, objEditBufferedImage.getWidth(), objEditBufferedImage.getHeight(), null);
                g.dispose();
                objArtworkAction.noise(objBufferedImageesize, new_val.doubleValue());
                artwork.setImage(SwingFXUtils.toFXImage(objBufferedImageesize, null));
                objBufferedImageesize = null;
                container.setContent(artwork);
            }
        });
                
        Separator sepHor2 = new Separator();
        sepHor2.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHor2, 0, 10);
        GridPane.setColumnSpan(sepHor2, 3);
        popup.getChildren().add(sepHor2);
        
        Label lblNote = new Label(objDictionaryAction.getWord("HELP"));
        lblNote.setWrapText(true);
        lblNote.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        lblNote.setTooltip(new Tooltip(objDictionaryAction.getWord("HELPCOLORADJUST")));
        //lblNote.setPrefWidth(objConfiguration.WIDTH*0.15);
        lblNote.setAlignment(Pos.CENTER);
        lblNote.setStyle("-fx-font: bold italic 11pt Arial; -fx-text-fill: #FF0000;");
        popup.add(lblNote, 0, 11, 2, 1); 
        
        artworkChildStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                colorPropertiesModel(colorAdjust,colorNoiseSlider.getValue());
                artworkChildStage.close();
                plotViewActionMode = 1;
                plotViewAction();
            }
        });
        Scene popupScene = new Scene(popup);
        popupScene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        artworkChildStage.setScene(popupScene);
        artworkChildStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("COLORADJUST"));
        artworkChildStage.showAndWait();
    }
    private void colorPropertiesModel(ColorAdjust colorAdjust, double noise){
        try {
            //objURF.store(objBufferedImage);
            // convert the input buffered image to a JavaFX image and load it into a JavaFX ImageView.
            ImageView imageView = new ImageView(SwingFXUtils.toFXImage(objBufferedImage, null));
            // apply the color adjustment.
            imageView.setEffect(colorAdjust);
            // snapshot the color adjusted JavaFX image, convert it back to a Swing buffered image and return it.
            SnapshotParameters snapshotParameters = new SnapshotParameters();
            snapshotParameters.setFill(Color.TRANSPARENT);
            objBufferedImage=SwingFXUtils.fromFXImage(imageView.snapshot(snapshotParameters,null),null);
                
            int width = objBufferedImage.getWidth();//(int)(objBufferedImage.getWidth()/zoomfactor);
            int height = objBufferedImage.getHeight();//(int)(objBufferedImage.getHeight()/zoomfactor);
            //objBufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            BufferedImage objBufferedImageesize = new BufferedImage(width, height,BufferedImage.TYPE_INT_RGB);
            Graphics2D g = objBufferedImageesize.createGraphics();
            g.drawImage(objBufferedImage, 0, 0, width, height, null);
            g.dispose();
            objBufferedImage = objBufferedImageesize;
            objArtworkAction.noise(objBufferedImage, noise/zoomfactor);            
            objBufferedImageesize = null;
            objUR.doCommand("Colors Properties", deepCopy(objBufferedImage));
            
            //artwork.setImage(SwingFXUtils.toFXImage(objBufferedImage, null));
            //container.setContent(artwork);
            initArtworkValue();
            lblStatus.setText(objDictionaryAction.getWord("SUCCESS"));
            colorAdjust.setBrightness(0);
            colorAdjust.setContrast(0);
            colorAdjust.setHue(0);
            colorAdjust.setSaturation(0);
            artwork.setEffect(colorAdjust);
        } catch (Exception ex) {               
            new Logging("SEVERE",getClass().getName(),"Operation apply",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }  
        System.gc();
    }
    /**
     * colorReductionAction
     * <p>
     * This method is used for creating GUI of artwork reduce color pane.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for creating GUI of artwork reduce color pane.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void populateArtworkColor(){
        //initArtworkValue();
        editThreadGP.getChildren().clear();
        for(int i=0; i<intColor; i++){
            String webColor = String.format("#%02X%02X%02X",colors.get(i).getRed(),colors.get(i).getGreen(),colors.get(i).getBlue());
            final Label  lblColor  = new Label ();
            lblColor.setPrefSize(33, 33);
            lblColor.setStyle("-fx-background-color:rgb("+colors.get(i).getRed()+","+colors.get(i).getGreen()+","+colors.get(i).getBlue()+"); -fx-border-color: #FFFFFF;");
            lblColor.setTooltip(new Tooltip("Color:"+i+"\n"+webColor+"\nR:"+colors.get(i).getRed()+"\nG:"+colors.get(i).getGreen()+"\nB:"+colors.get(i).getBlue()));
            lblColor.setUserData(i);
            lblColor.setOnDragDetected(new EventHandler<MouseEvent>() {
                public void handle(MouseEvent event) {
                    /* drag was detected, start a drag-and-drop gesture*/
                    /* allow any transfer mode */
                    Dragboard db = lblColor.startDragAndDrop(TransferMode.ANY);
                    /* Put a string on a dragboard */
                    ClipboardContent content = new ClipboardContent();
                    int index = Integer.parseInt(lblColor.getUserData().toString());
                    String webColor = String.format("#%02X%02X%02X",colors.get(index).getRed(),colors.get(index).getGreen(),colors.get(index).getBlue());
                    content.putString(webColor);
                    db.setContent(content);
                    event.consume();
                }
            });
            lblColor.setOnDragOver(new EventHandler<DragEvent>() {
                public void handle(DragEvent event) {
                    /* data is dragged over the target */
                    /* accept it only if it is not dragged from the same node 
                     * and if it has a string data */
                    if (event.getGestureSource() != lblColor && event.getDragboard().hasString()) {
                        /* allow for both copying and moving, whatever user chooses */
                        event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                    }
                    event.consume();
                }
            });
            lblColor.setOnDragEntered(new EventHandler<DragEvent>() {
                public void handle(DragEvent event) {
                /* the drag-and-drop gesture entered the target */
                /* show to the user that it is an actual gesture target */
                     if (event.getGestureSource() != lblColor && event.getDragboard().hasString()) {
                         lblColor.setTextFill(Color.GREEN);
                     }
                     event.consume();
                }
            });
            lblColor.setOnDragExited(new EventHandler<DragEvent>() {
                public void handle(DragEvent event) {
                    /* mouse moved away, remove the graphical cues */
                    lblColor.setTextFill(Color.BLACK);
                    event.consume();
                }
            });
            lblColor.setOnDragDropped(new EventHandler<DragEvent>() {
                public void handle(DragEvent event) {
                    /* data dropped */
                    /* if there is a string data on dragboard, read it and use it */
                    Dragboard db = event.getDragboard();
                    boolean success = false;
                    if (db.hasString()) {                    
                       lblColor.setStyle("-fx-background-color: "+db.getString()+";");
                       //lblColor.
                       int index = Integer.parseInt(lblColor.getUserData().toString());
                       colors.set(index, new java.awt.Color((float)Color.web(db.getString()).getRed(),(float)Color.web(db.getString()).getGreen(),(float)Color.web(db.getString()).getBlue()));
                       success = true;                   
                       try{
                            //objURF.store(objBufferedImage);
                            objArtworkAction = new ArtworkAction(false);
                            int [][] artworkMatrix = objArtworkAction.getImageToMatrix(objBufferedImage);   
                            objBufferedImage = objArtworkAction.getImageFromMatrix(artworkMatrix, colors);
                            objUR.doCommand("Apply", deepCopy(objBufferedImage));
                            artworkMatrix = null;
                            initArtworkValue();
                            populateArtworkColor();
                        } catch (SQLException ex) {               
                            new Logging("SEVERE",getClass().getName(),"Operation apply",ex);
                            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                        }
                    }
                    /* let the source know whether the string was successfully 
                     * transferred and used */
                    event.setDropCompleted(success);
                    event.consume();
                 }
            });
            lblColor.setOnDragDone(new EventHandler<DragEvent>() {
                public void handle(DragEvent event) {
                    /* the drag and drop gesture ended */
                    /* if the data was successfully moved, clear it */
                    if (event.getTransferMode() == TransferMode.MOVE) {                    
                    }
                    event.consume();
                }
            });
            editThreadGP.add(lblColor, i%5, i/5);
        }
    }
    private void colorReductionAction(){
        frontSideViewAction();
        lblStatus.setText(objDictionaryAction.getWord("ACTIONREDUCECOLOR"));
        isEditingMode = true;
        plotEditActionMode = 5; //editColor = 5
        artworkChildStage = new Stage();
        artworkChildStage.initOwner(artworkStage);
        artworkChildStage.initStyle(StageStyle.UTILITY);
        //dialogStage.initModality(Modality.WINDOW_MODAL);
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setAlignment(Pos.CENTER);
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
    
        final ToggleGroup colorTG = new ToggleGroup();
        RadioButton byDragRB = new RadioButton(objDictionaryAction.getWord("Reduce using Drag & Drop"));
        byDragRB.setToggleGroup(colorTG);
        byDragRB.setUserData("dragdrop");
        popup.add(byDragRB, 0, 0, 2, 1);
        colorTG.selectToggle(byDragRB);
        
        editThreadGP= new GridPane();
        editThreadGP.setAlignment(Pos.CENTER);
        editThreadGP.setHgap(10);
        editThreadGP.setVgap(10);
        editThreadGP.setCursor(Cursor.HAND);
        popup.add(editThreadGP, 0, 1, 2, 1);
        populateArtworkColor();        
        
        RadioButton byNumberRB = new RadioButton(objDictionaryAction.getWord("Reduce using number of colors"));
        byNumberRB.setToggleGroup(colorTG);
        byNumberRB.setUserData("number");
        popup.add(byNumberRB, 0, 2, 2, 1);
        
        final TextField txtColor = new TextField(Integer.toString(colors.size())){
            @Override public void replaceText(int start, int end, String text) {
                if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
                if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        txtColor.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                // validate name when name TextField loses focus
                if(!newValue){ 
                    if(txtColor.getText().trim().length()==0){ // name field empty
                        txtColor.setText(String.valueOf(intColor));
                        lblStatus.setText(objDictionaryAction.getWord("BALNKINPUT"));
                        if(Integer.parseInt(txtColor.getText())<1)
                            txtColor.setText("1");
                    } else {   // valid data
                        try {
                            //objURF.store(objBufferedImage);
                            objArtworkAction = new ArtworkAction(false);                    
                            objBufferedImage = objArtworkAction.reduceColors(objBufferedImage,Integer.parseInt(txtColor.getText()));
							objUR.doCommand("Apply", deepCopy(objBufferedImage));
                            initArtworkValue();
							populateArtworkColor();
                        } catch (SQLException ex) {               
                            new Logging("SEVERE",getClass().getName(),"Operation apply",ex);
                            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                        }
                    }
                }
            }
        }); 
        txtColor.setPromptText(objDictionaryAction.getWord("COLORLIMIT"));
        txtColor.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCOLORLIMIT")));
        popup.add(txtColor, 0, 3, 2, 1);
        
        RadioButton byGrayRB = new RadioButton(objDictionaryAction.getWord("Convert to Gray Scale"));
        byGrayRB.setToggleGroup(colorTG);
        byGrayRB.setUserData("grayscale");
        popup.add(byGrayRB, 0, 4, 2, 1);
        
        RadioButton byBlackWhiteRB = new RadioButton(objDictionaryAction.getWord("Convert to Balck & White"));
        byBlackWhiteRB.setToggleGroup(colorTG);
        byBlackWhiteRB.setUserData("blackwhite");
        popup.add(byBlackWhiteRB, 0, 5, 2, 1);
        
        editThreadGP.setDisable(false);
        txtColor.setDisable(true);
        colorTG.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            public void changed(ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) {
                if (colorTG.getSelectedToggle() != null) {
                    if(colorTG.getSelectedToggle().getUserData().toString().equalsIgnoreCase("dragdrop")){
                        editThreadGP.setDisable(false);
                        txtColor.setDisable(true);
                    } else if(colorTG.getSelectedToggle().getUserData().toString().equalsIgnoreCase("number")){
                        txtColor.setDisable(false);
                        editThreadGP.setDisable(true);
                    } else {
                        txtColor.setDisable(true);
                        editThreadGP.setDisable(true);
                        try {
                            //objURF.store(objBufferedImage);
                            objArtworkAction = new ArtworkAction(false);                    
                            if(colorTG.getSelectedToggle().getUserData().toString().equalsIgnoreCase("grayscale")){
                                objBufferedImage = objArtworkAction.getImageGray(objBufferedImage);
                            } else if(colorTG.getSelectedToggle().getUserData().toString().equalsIgnoreCase("blackwhite")){
                                objBufferedImage = objArtworkAction.getImageBlackWhite(objBufferedImage);
                            }
                            objUR.doCommand("Apply", deepCopy(objBufferedImage));
                            initArtworkValue();
                            populateArtworkColor();
                        } catch (SQLException ex) {               
                            new Logging("SEVERE",getClass().getName(),"Operation apply",ex);
                            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                        } catch (IOException ex) {
                            new Logging("SEVERE",getClass().getName(),"IOException: operation failed",ex);
                            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                        }  
                        System.gc();
                    }        
                }
            }
        });
        
        Separator sepHor1 = new Separator();
        sepHor1.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHor1, 0, 7);
        GridPane.setColumnSpan(sepHor1, 2);
        popup.getChildren().add(sepHor1);
        
        Label lblNote = new Label(objDictionaryAction.getWord("HELP"));
        lblNote.setWrapText(true);
        lblNote.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        lblNote.setTooltip(new Tooltip(objDictionaryAction.getWord("HELPCOLORREDUCE")+objConfiguration.getIntColorLimit()));
        //lblNote.setPrefWidth(objConfiguration.WIDTH*0.15);
        lblNote.setAlignment(Pos.CENTER);
        lblNote.setStyle("-fx-font: bold italic 11pt Arial; -fx-text-fill: #FF0000;");
        popup.add(lblNote, 0, 8, 2, 1); 
        
        artworkChildStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                artworkChildStage.close();
                frontSideViewAction();
            }
        }); 
        Scene popupScene = new Scene(popup);
        popupScene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        artworkChildStage.setScene(popupScene);
        artworkChildStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("WINDOWREDUCECOLORARTWORK"));
        artworkChildStage.showAndWait();        
    }
    /**
     * artworkSketch
     * <p>
     * This method is used for creating GUI of artwork reduce color pane.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for creating GUI of artwork reduce color pane.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void artworkSketch() {    
        lblStatus.setText(objDictionaryAction.getWord("ACTIONARTWORKSKETCH"));
        try {
            //objURF.store(objBufferedImage);
            objArtworkAction = new ArtworkAction(false);
            objBufferedImage=objArtworkAction.getImageColorBorderSketch(objBufferedImage);
            /*try {
                mageIO.write(artworkBorder, "png", new File(System.getProperty("user.dir")+"/mla/temp/test.png"));
            } catch (IOException ex) {
                Logger.getLogger(ArtworkEditView.class.getName()).log(Level.SEVERE, null, ex);
            }*/
            objUR.doCommand("sketch", deepCopy(objBufferedImage));
            initArtworkValue();
            //plotViewActionMode = 1;
            plotViewAction();
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"Operation sketch",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    
    /**
     * pencilToolAction
     * <p>
     * This method is used for creating GUI of artwork reduce color pane.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for creating GUI of artwork reduce color pane.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */    
    private void pencilToolAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCILTOOL"));
            //objUR.doCommand("Edit Pattern", objBufferedImage);
            //objURF.store(objBufferedImage);            
            objArtworkAction = new ArtworkAction();
            setCurrentShape();
            isEditingMode=true;
            plotEditActionMode = 11; //penciltool = 11
            plotEditImage();
            populateEditColorPane();   
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"editWeave "+ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"editWeave "+ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } 
    } 
    private void eraserToolAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONERASERTOOL"));
            //objUR.doCommand("Edit Pattern", objBufferedImage);
            //objURF.store(objBufferedImage);            
            objArtworkAction = new ArtworkAction();
            setCurrentShape();
            isEditingMode=true;
            plotEditActionMode = 12; //erasertool = 12
            plotEditImage();
            populateEditColorPane();   
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"editWeave "+ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"editWeave "+ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } 
    } 
    private void sprayToolAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONSPRAYTOOL"));
            //objUR.doCommand("Edit Pattern", objBufferedImage);
            //objURF.store(objBufferedImage);            
            objArtworkAction = new ArtworkAction();
            setCurrentShape();
            isEditingMode=true;
            plotEditActionMode = 9; //spraytool = 9
            plotEditImage();
            populateEditColorPane();   
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"editWeave "+ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"editWeave "+ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }    
    private void artworkSprayToolEvent(MouseEvent event){        
        if(event.isSecondaryButtonDown() || (event.getButton() == MouseButton.SECONDARY)){
            objGraphics2D.setColor(new java.awt.Color(editThreadSecondaryValue));
        } else{
            objGraphics2D.setColor(new java.awt.Color(editThreadPrimaryValue));
        }
        BasicStroke bs = new BasicStroke(intFillSize);
        objGraphics2D.setStroke(bs);        
        //code to make spray pattern    
        for(int nPaint = 0; nPaint < 30; nPaint++){        
            //generate a random distance from 0 to 10
            int nRand = (int)(Math.random() * 11 * intFillSize);
            //generate a random angle from 0 to 2 pi
            double dTheta = Math.random() * 628 / 100.0;
            //find x and y of random dot
            int nX = (int)((int)(event.getX()/zoomfactor) + nRand * Math.cos(dTheta));
            int nY = (int)((int)(event.getY()/zoomfactor) + nRand * Math.sin(dTheta));
            nX = nX>0?nX<objBufferedImage.getWidth()?nX:objBufferedImage.getWidth()-1:0;
            nY = nY>0?nY<objBufferedImage.getHeight()?nY:objBufferedImage.getHeight()-1:0;
            
            objGraphics2D.drawLine(nX,nY,nX,nY);            
        }
        //oldX = nX;
        //oldY = nY;
        plotEditImage();
        //artwork.setImage(SwingFXUtils.toFXImage(objEditBufferedImage, null));
        //container.setContent(artwork);    
    }
    private void artworkPencilToolEvent(MouseEvent event){        
        if(event.isSecondaryButtonDown() || (event.getButton() == MouseButton.SECONDARY)){
            objGraphics2D.setColor(new java.awt.Color(editThreadSecondaryValue));
        } else{
            objGraphics2D.setColor(new java.awt.Color(editThreadPrimaryValue));
        }
        BasicStroke bs = new BasicStroke(intFillSize);
        objGraphics2D.setStroke(bs);
        objGraphics2D.drawLine((int)(oldX/zoomfactor), (int)(oldY/zoomfactor), (int)(lastX/zoomfactor), (int)(lastY/zoomfactor));
        oldX = lastX;
        oldY = lastY;
        plotEditImage();
        //artwork.setImage(SwingFXUtils.toFXImage(objEditBufferedImage, null));
        //container.setContent(artwork);    
    }
    private void artworkEraserToolEvent(MouseEvent event){
        objGraphics2D.setColor(java.awt.Color.WHITE);
        BasicStroke bs = new BasicStroke(intFillSize);
        objGraphics2D.setStroke(bs);
        objGraphics2D.drawLine((int)(oldX/zoomfactor), (int)(oldY/zoomfactor), (int)(lastX/zoomfactor), (int)(lastY/zoomfactor));
        oldX = lastX;
        oldY = lastY;
        plotEditImage();
        //artwork.setImage(SwingFXUtils.toFXImage(objEditBufferedImage, null));
        //container.setContent(artwork);           
    }
    private void plotEditImage(){
        try {
            //objUR.doCommand("PlotEditTool", objBufferedImage);
            //objURF.store(objBufferedImage);
            objEditBufferedImage = new BufferedImage(objBufferedImage.getWidth(), objBufferedImage.getHeight(),BufferedImage.TYPE_INT_RGB);
            objGraphics2D = objEditBufferedImage.createGraphics();
            objGraphics2D.drawImage(objBufferedImage, 0, 0, objBufferedImage.getWidth(), objBufferedImage.getHeight(), null);
            objGraphics2D.dispose();
            objBufferedImage =null;
            
            objBufferedImage = new BufferedImage(objEditBufferedImage.getWidth(), objEditBufferedImage.getHeight(),BufferedImage.TYPE_INT_RGB);
            objGraphics2D = objBufferedImage.createGraphics();
            objGraphics2D.drawImage(objEditBufferedImage, 0, 0, objEditBufferedImage.getWidth(), objEditBufferedImage.getHeight(), null);
                        
            objEditBufferedImage = new BufferedImage((int)(objBufferedImage.getWidth()*zoomfactor), (int)(objBufferedImage.getHeight()*zoomfactor),BufferedImage.TYPE_INT_RGB);
            Graphics2D g = objEditBufferedImage.createGraphics();
            g.drawImage(objBufferedImage, 0, 0, (int)(objBufferedImage.getWidth()*zoomfactor), (int)(objBufferedImage.getHeight()*zoomfactor), null);
            g.setColor(java.awt.Color.BLACK);
            g.dispose();
            
            artwork.setImage(SwingFXUtils.toFXImage(objEditBufferedImage, null));
            container.setContent(artwork);            
            System.gc();
            lblStatus.setText(objDictionaryAction.getWord("GOTFRONTSIDEVIEW"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Exception:plotGridView() : Error while viewing grid view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch(OutOfMemoryError ex){
            new Logging("SEVERE",getClass().getName(),"OutOfMemoryError:plotGridView() : Error while viewing grid view",null);            
        }
    } 
    private void fillToolAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONFILLTOOL"));
            //objUR.doCommand("Edit Pattern", objBufferedImage);
            //objURF.store(objBufferedImage);            
            objArtworkAction = new ArtworkAction();
            setCurrentShape();
            isEditingMode=true;
            plotEditActionMode = 10; //filltool = 10
            artwork.setCursor(Cursor.HAND);
            plotEditWeave();
            populateEditColorPane();   
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"editWeave "+ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"editWeave "+ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } 
    } 
    private void artworkFillToolEvent(MouseEvent event){
        try {
            artwork.setCursor(Cursor.HAND);
            int evx=(int) (event.getX()/zoomfactor);
            int evy=(int) (event.getY()/zoomfactor);
            if(plotViewActionMode==10 || plotViewActionMode==11){
                String[] data = objArtwork.getObjConfiguration().getStrGraphSize().split("x");
                evx=(int)(event.getX()/(Integer.parseInt(data[1])*zoomfactor));
                evy=(int)(event.getY()/(Integer.parseInt(data[0])*zoomfactor));
            }
            int clickColor = objBufferedImage.getRGB(evx, evy);
            byte[][] shapeMatrix = new byte[objBufferedImage.getWidth()][objBufferedImage.getHeight()];

            //System.err.println("color+"+editThreadPrimaryValue);
            //lblStatus.setText("color fill"+editThreadPrimaryValue);
            BufferedImage objEditBufferedImage= new BufferedImage((int)(objBufferedImage.getWidth()), (int)(objBufferedImage.getHeight()),BufferedImage.TYPE_INT_RGB);
            Graphics2D g = objEditBufferedImage.createGraphics();
            g.drawImage(objBufferedImage, 0, 0, (int)(objBufferedImage.getWidth()), (int)(objBufferedImage.getHeight()), null);

            if(strFillArea.equalsIgnoreCase("Color Single Instance")){
                objArtworkAction= new ArtworkAction();
                shapeMatrix=objArtworkAction.getArtworkBoundary(evx, evy, objEditBufferedImage);

                objEditBufferedImage = new BufferedImage((int)(objBufferedImage.getWidth()), (int)(objBufferedImage.getHeight()),BufferedImage.TYPE_INT_RGB);
                g = objEditBufferedImage.createGraphics();
                g.drawImage(objBufferedImage, 0, 0, (int)(objBufferedImage.getWidth()), (int)(objBufferedImage.getHeight()), null);
                g.dispose();

                // printing boundary matrix
                for(int x=0; x<shapeMatrix.length; x++){
                    for(int y=0; y<shapeMatrix[0].length; y++){
                        if(shapeMatrix[x][y]==1){
                            if(event.isSecondaryButtonDown() || (event.getButton() == MouseButton.SECONDARY))
                                objEditBufferedImage.setRGB(x, y, editThreadSecondaryValue);
                            else
                                objEditBufferedImage.setRGB(x, y, editThreadPrimaryValue);
                        }
                    }
                }
            }else{                
                // printing boundary matrix
                for(int x=0; x<shapeMatrix.length; x++){
                    for(int y=0; y<shapeMatrix[0].length; y++){
                        if(clickColor==objEditBufferedImage.getRGB(x,y)){
                            shapeMatrix[x][y]=1;
                            if(event.isSecondaryButtonDown() || (event.getButton() == MouseButton.SECONDARY))
                                objEditBufferedImage.setRGB(x, y, editThreadSecondaryValue);
                            else
                                objEditBufferedImage.setRGB(x, y, editThreadPrimaryValue);
                        }else{
                            shapeMatrix[x][y]=0;
                        }
                        //System.out.print(y);
                    }
                    //System.out.println(x);
                }
            }
            objBufferedImage = objEditBufferedImage;
            shapeMatrix = null;
            objEditBufferedImage = null;
            System.gc();
            //initArtworkValue();
            plotEditActionMode = 10;//filltool = 10 
            plotEditWeave();
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"SQLException:artworkfilltool() : Error while viewing grid view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }   
    }    
    private void populateEditColorPane(){
         if(artworkChildStage!=null){
            artworkChildStage.close();
            artworkChildStage = null;
            System.gc();
        }
        artworkChildStage = new Stage();
        artworkChildStage.initOwner(artworkStage);
        artworkChildStage.initStyle(StageStyle.UTILITY);
        //dialogStage.initModality(Modality.WINDOW_MODAL);
        GridPane popup=new GridPane();
        popup.setId("popup");
        //popup.setAlignment(Pos.CENTER);
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
                
        Label lblNote = new Label(objDictionaryAction.getWord("HELP"));
        //lblNote.setWrapText(true);
        lblNote.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        lblNote.setTooltip(new Tooltip(objDictionaryAction.getWord("HELPEDITTOOL")));
        //lblNote.setPrefWidth(objConfiguration.WIDTH*0.15);
        lblNote.setAlignment(Pos.CENTER);
        lblNote.setStyle("-fx-font: bold italic 11pt Arial; -fx-text-fill: #FF0000;");
        popup.add(lblNote, 0, 0, 1, 1);
        
        final Label lblLeft = new Label();
        lblLeft.setGraphic(new ImageView("/media/mouse_left.png"));
        lblLeft.setTooltip(new Tooltip(objDictionaryAction.getWord("LEFT")));
        //lblLeft.setPrefWidth(objConfiguration.WIDTH*0.15);
        lblLeft.setAlignment(Pos.CENTER_RIGHT);
        lblLeft.setStyle("-fx-font: bold italic 11pt Arial; -fx-text-fill: #FF0000; -fx-background-color: #FFFFFF;");
        popup.add(lblLeft, 1, 0, 1, 1);
        final Label lblRight = new Label();
        lblRight.setGraphic(new ImageView("/media/mouse_right.png"));
        lblRight.setTooltip(new Tooltip(objDictionaryAction.getWord("RIGHT")));
        //lblRight.setPrefWidth(objConfiguration.WIDTH*0.15);
        lblRight.setAlignment(Pos.CENTER_RIGHT);
        lblRight.setStyle("-fx-font: bold italic 11pt Arial; -fx-text-fill: #FF0000; -fx-background-color: #FFFFFF;");
        popup.add(lblRight, 2, 0, 1, 1);
        
        Separator sepHor1 = new Separator();
        sepHor1.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHor1, 0, 1);
        GridPane.setColumnSpan(sepHor1, 3);
        popup.getChildren().add(sepHor1);
        
        if(plotEditActionMode!=12){
            popup.add(new Label(objDictionaryAction.getWord("PALETTE")), 0, 2, 1, 1);

            final ComboBox paletteCB = new ComboBox();
            paletteCB.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPPALETTE")));
            try{
                Palette objPalette = new Palette();
                objPalette.setObjConfiguration(objConfiguration);
                objPalette.setStrPaletteType("Colour");
                UtilityAction objUtilityAction = new UtilityAction();
                String[][] paletteData=objUtilityAction.getPaletteName(objPalette);
                if(paletteData!=null){
                    for(String[] data:paletteData){
                        objPalette = new Palette();
                        objPalette.setStrPaletteName(data[1]);
                        objPalette.setStrPaletteID(data[0]);
                        objPalette.setObjConfiguration(objConfiguration);
                        objUtilityAction=new UtilityAction();
                        objUtilityAction.getPalette(objPalette);
                        if(objPalette.getStrThreadPalette()!=null){
                            paletteCB.getItems().add(data[1]);
                            paletteCB.setValue(data[1]);
                            objArtwork.getObjConfiguration().setColourPalette(objPalette.getStrThreadPalette());
                        }
                    }
                }
            } catch(SQLException sqlEx){
                new Logging("SEVERE",getClass().getName(),"loadPaletteNames() : ", sqlEx);
            }
            paletteCB.valueProperty().addListener(new ChangeListener() {
                @Override
                public void changed(ObservableValue ov, Object t, Object t1) {
                    try{
                        UtilityAction objUtilityAction=new UtilityAction();
                        String id=objUtilityAction.getPaletteIdFromName(t1.toString());
                        if(id!=null){
                            Palette objPalette = new Palette();
                            objPalette.setStrPaletteID(id);
                            objPalette.setObjConfiguration(objConfiguration);
                            objUtilityAction=new UtilityAction();
                            objUtilityAction.getPalette(objPalette);
                            if(objPalette.getStrThreadPalette()!=null){
                                objArtwork.getObjConfiguration().setColourPalette(objPalette.getStrThreadPalette());
                            }
                        }
                        populateArtworkColorPalette(lblLeft, lblRight);
                    } catch(SQLException sqlEx){
                        new Logging("SEVERE",getClass().getName(),"loadColorPalette() : ", sqlEx);
                    }
                }
            });
            popup.add(paletteCB, 1, 2, 2, 1);  

            editThreadGP= new GridPane();
            editThreadGP.setAlignment(Pos.CENTER);
            editThreadGP.setHgap(3);
            editThreadGP.setVgap(3);
            editThreadGP.setCursor(Cursor.HAND);
            popup.add(editThreadGP, 0, 3, 3, 1);
            if(objArtwork.getObjConfiguration().getColourPalette()!=null)
                populateArtworkColorPalette(lblLeft, lblRight); 

            /*final ColorPicker fillToolColorPicker = new ColorPicker();
            fillToolColorPicker.setStyle("-fx-color-label-visible: false ;");
            fillToolColorPicker.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPFILLTOOLCOLOR")));
            popup.add(fillToolColorPicker, 1, 5); 
            */
            /*final ComboBox colorCB=new ComboBox();
            colorCB.setOnShown(new EventHandler() {
                @Override
                public void handle(Event t) {
                    try {
                        ColourSelector objColourSelector=new ColourSelector(objArtwork.getObjConfiguration());
                        if(objColourSelector.colorCode!=null&&objColourSelector.colorCode.length()>0){
                            colorCB.setStyle("-fx-background-color:#"+objColourSelector.colorCode+";");
                        }
                        colorCB.hide();
                        t.consume();

                        String hex=colorCB.getStyle().substring(colorCB.getStyle().lastIndexOf("#")+1, colorCB.getStyle().lastIndexOf("#")+7);
                        editThreadPrimaryValue = Integer.parseInt(hex.substring(0, 2), 16);//(int)(fillToolColorPicker.getValue().getRed()*255);
                        editThreadPrimaryValue = (editThreadPrimaryValue << 8) + Integer.parseInt(hex.substring(2, 4), 16);//(int)(fillToolColorPicker.getValue().getGreen()*255);
                        editThreadPrimaryValue = (editThreadPrimaryValue << 8) + Integer.parseInt(hex.substring(4, 6), 16);//(int)(fillToolColorPicker.getValue().getBlue()*255);
                        //System.out.println((int)(fillToolColorPicker.getValue().getRed()*255)+"="+editThreadPrimaryValue+"="+(fillToolColorPicker.getValue().getRed()*255));
                        //System.out.println((int)(fillToolColorPicker.getValue().getGreen()*255)+"="+editThreadPrimaryValue+"="+(fillToolColorPicker.getValue().getGreen()*255));
                        //System.out.println((int)(fillToolColorPicker.getValue().getBlue()*255)+"="+editThreadPrimaryValue+"="+(fillToolColorPicker.getValue().getBlue()*255));
                    } catch (Exception ex) {
                        new Logging("SEVERE",getClass().getName(),"colourPalettePopup: warp",ex);
                    }
                }
            });
            colorCB.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPYARNCOLOR")));
            popup.add(colorCB, 1, 5);*/

            Separator sepHor2 = new Separator();
            sepHor2.setValignment(VPos.CENTER);
            GridPane.setConstraints(sepHor2, 0, 4);
            GridPane.setColumnSpan(sepHor2, 3);
            popup.getChildren().add(sepHor2);
        }
        if(plotEditActionMode==10){ //fill tool
            Label fillToolArea = new Label(objDictionaryAction.getWord("FILLTOOLAREA")+" : ");
            fillToolArea.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPFILLTOOLAREA")));
            popup.add(fillToolArea, 0, 5, 1, 1);
            
            final ComboBox fillToolAreaCB = new ComboBox();
            fillToolAreaCB.getItems().addAll("Color All Instance","Color Single Instance");
            fillToolAreaCB.setValue(strFillArea);
            fillToolAreaCB.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPFILLTOOLAREA")));
            popup.add(fillToolAreaCB, 1, 5, 2, 1); 
            
            fillToolAreaCB.valueProperty().addListener(new ChangeListener() {
                @Override
                public void changed(ObservableValue ov, Object t, Object t1) {
                    strFillArea = t1.toString();
                }
            });
        } else {// if(plotEditActionMode==11) //pencil tool if(plotEditActionMode==12) //eraser tool
            if(intFillSize<1 || intFillSize>10)
                intFillSize = 1;
            
            Label fillToolSize = new Label(objDictionaryAction.getWord("FILLTOOLSIZE")+" : ");
            fillToolSize.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPFILLTOOLSIZE")));
            popup.add(fillToolSize, 0, 5, 1, 1);
        
            final Label sizeTF = new Label();
            sizeTF.setText(Byte.toString(intFillSize));
            sizeTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPFILLTOOLSIZE")));
            popup.add(sizeTF, 1, 5, 2, 1);                        
            
            final Slider sizeSlider = new Slider(1, 10, intFillSize);    
            sizeSlider.setShowTickLabels(true);
            sizeSlider.setShowTickMarks(true);
            sizeSlider.setBlockIncrement(1);
            sizeSlider.setMajorTickUnit(2);
            sizeSlider.setMinorTickCount(1);
            popup.add(sizeSlider, 1, 6, 2, 1);

            sizeSlider.valueProperty().addListener(new ChangeListener<Number>() {
                public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
                    intFillSize = new_val.byteValue();
                    sizeTF.setText(Integer.toString(intFillSize));
                }
            });    
        }
        
        artworkChildStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                artworkChildStage.close();
                plotViewActionMode = 1;
                plotViewAction();
            }
        });
        Scene popupScene = new Scene(popup);
        popupScene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        artworkChildStage.setScene(popupScene);
        artworkChildStage.setResizable(false);
        artworkChildStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("WINDOWCOLORPALETTE"));
        artworkChildStage.showAndWait();   
        System.gc();
    }
    private void populateArtworkColorPalette(final Label lblLeft, final Label lblRight){
        editThreadGP.getChildren().clear();
        for(int i=0; i<objArtwork.getObjConfiguration().getColourPalette().length; i++){
            String webColor = "#"+objArtwork.getObjConfiguration().getColourPalette()[i];//String.format("#%02X%02X%02X",colors.get(i).getRed(),colors.get(i).getGreen(),colors.get(i).getBlue());
            //System.err.println(webColor);
            final Label  lblColor  = new Label ();
            lblColor.setPrefSize(15, 15);
            lblColor.setStyle("-fx-background-color:"+webColor+"; -fx-border-color: #FFFFFF;");
            lblColor.setTooltip(new Tooltip("Color:"+i+"\n"+webColor));
            int rgb = java.awt.Color.decode(webColor).getRGB();
            lblColor.setUserData(rgb);
            lblColor.setId(webColor);
            lblColor.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if(event.getClickCount()>1){
                        try {
                            ColourSelector objColourSelector=new ColourSelector(objArtwork.getObjConfiguration());
                            if(objColourSelector.colorCode!=null&&objColourSelector.colorCode.length()>0){
                                String webColor = "#"+objColourSelector.colorCode;
                                lblColor.setStyle("-fx-background-color:"+webColor+"; -fx-border-color: #FFFFFF;");
                                lblColor.setTooltip(new Tooltip("Color:\n"+webColor));
                                int rgb = java.awt.Color.decode(webColor).getRGB();
                                lblColor.setUserData(rgb);
                                lblColor.setId(webColor);
                            }
                            event.consume();
                        } catch (Exception ex) {
                            new Logging("SEVERE",PatternView.class.getName(),"colourPalettePopup: warp",ex);
                        }
                    }else{
                        if(event.isSecondaryButtonDown() || (event.getButton() == MouseButton.SECONDARY)){
                            editThreadSecondaryValue=Integer.parseInt(lblColor.getUserData().toString());
                            lblRight.setStyle("-fx-font: bold italic 11pt Arial; -fx-text-fill: #FF0000; -fx-background-color:"+lblColor.getId()+";");
                        } else{
                            editThreadPrimaryValue=Integer.parseInt(lblColor.getUserData().toString());
                            lblLeft.setStyle("-fx-font: bold italic 11pt Arial; -fx-text-fill: #FF0000; -fx-background-color:"+lblColor.getId()+";");
                        }
                    }
                }
            });           
            editThreadGP.add(lblColor, i%13, i/13);
            if(i==1){
                editThreadSecondaryValue=rgb;
                lblRight.setStyle("-fx-font: bold italic 11pt Arial; -fx-text-fill: #FF0000; -fx-background-color:"+lblColor.getId()+";");
            }else if(i==0){
                editThreadPrimaryValue=rgb;
                lblLeft.setStyle("-fx-font: bold italic 11pt Arial; -fx-text-fill: #FF0000; -fx-background-color:"+lblColor.getId()+";");                    
            }
        }
    }
    private void plotEditWeave(){
        try {
            //objUR.doCommand("PlotEditTool", objBufferedImage);
            //objURF.store(objBufferedImage);            
            objEditBufferedImage= new BufferedImage((int)(objBufferedImage.getWidth()*zoomfactor), (int)(objBufferedImage.getHeight()*zoomfactor),BufferedImage.TYPE_INT_RGB);
            objGraphics2D = objEditBufferedImage.createGraphics();
            objGraphics2D.drawImage(objBufferedImage, 0, 0, (int)(objBufferedImage.getWidth()*zoomfactor), (int)(objBufferedImage.getHeight()*zoomfactor), null);
            objGraphics2D.setColor(java.awt.Color.BLACK);
            //g.dispose();
            artwork.setImage(SwingFXUtils.toFXImage(objEditBufferedImage, null));
            container.setContent(artwork);            
            System.gc();
            lblStatus.setText(objDictionaryAction.getWord("GOTFRONTSIDEVIEW"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Exception:plotGridView() : Error while viewing grid view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch(OutOfMemoryError ex){
            new Logging("SEVERE",getClass().getName(),"OutOfMemoryError:plotGridView() : Error while viewing grid view",null);            
        }
    }     
    private void transformOperationAction(){
        frontSideViewAction();
        transformMenuAction();
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONTRANSFORMOPERATIONEDIT"));
            //objUR.doCommand("Edit Pattern", objBufferedImage);
            //objURF.store(objBufferedImage);            
            objArtworkAction = new ArtworkAction();
            setCurrentShape();
            isEditingMode=true;
            plotEditActionMode = 4; //editweave = 4
            plotEditWeave();
            //populateEditWeavePane();   
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"editWeave "+ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"editWeave "+ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } 
    }
    private void paintOperationAction(){
        frontSideViewAction();
        paintMenuAction();
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONPAINTOPERATIONEDIT"));
            //objUR.doCommand("Edit Pattern", objBufferedImage);
            //objURF.store(objBufferedImage);            
            objArtworkAction = new ArtworkAction();
            setCurrentShape();
            isEditingMode=true;
            plotEditActionMode = 4; //editweave = 4
            plotEditImage();
            //populateEditWeavePane();   
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"editWeave "+ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"editWeave "+ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } 
    } 
 /**
 * populateTransformToolbar
 * <p>
 * Function use for editing tool bar for menu item Edit,
 * and binding events for each tools. 
 * 
 * @exception   (@throws SQLException)
 * @author      Amit Kumar Singh
 * @version     %I%, %G%
 * @since       1.0
 * @see         javafx.event.*;
 * @link        WeaveView
 */
    private void populateTransformToolbarMenu(){
        //For drop down menus
        menuSelected.setTooltip(new Tooltip(objDictionaryAction.getWord("EDIT")));
       
        MenuItem mirrorVerticalEditMI = new MenuItem(objDictionaryAction.getWord("VERTICALMIRROR"));
        MenuItem mirrorHorizontalEditMI = new MenuItem(objDictionaryAction.getWord("HORIZENTALMIRROR"));
        MenuItem rotateClockwiseEditMI = new MenuItem(objDictionaryAction.getWord("CLOCKROTATION"));
        MenuItem rotateAnticlockwiseEditMI = new MenuItem(objDictionaryAction.getWord("ANTICLOCKROTATION"));
        MenuItem moveRightEditMI = new MenuItem(objDictionaryAction.getWord("MOVERIGHT"));
        MenuItem moveLeftEditMI = new MenuItem(objDictionaryAction.getWord("MOVELEFT"));
        MenuItem moveUpEditMI = new MenuItem(objDictionaryAction.getWord("MOVEUP"));
        MenuItem moveDownEditMI = new MenuItem(objDictionaryAction.getWord("MOVEDOWN"));        
        MenuItem moveRight8EditMI = new MenuItem(objDictionaryAction.getWord("MOVERIGHT8"));
        MenuItem moveLeft8EditMI = new MenuItem(objDictionaryAction.getWord("MOVELEFT8"));
        MenuItem moveUp8EditMI = new MenuItem(objDictionaryAction.getWord("MOVEUP8"));
        MenuItem moveDown8EditMI = new MenuItem(objDictionaryAction.getWord("MOVEDOWN8"));        
        MenuItem tiltRightEditMI = new MenuItem(objDictionaryAction.getWord("TILTRIGHT"));
        MenuItem tiltLeftEditMI = new MenuItem(objDictionaryAction.getWord("TILTLEFT"));
        MenuItem tiltUpEditMI = new MenuItem(objDictionaryAction.getWord("TILTUP"));
        MenuItem tiltDownEditMI = new MenuItem(objDictionaryAction.getWord("TILTDOWN")); 
        MenuItem zoomInViewMI = new MenuItem(objDictionaryAction.getWord("ZOOMINVIEW"));
        MenuItem normalViewMI = new MenuItem(objDictionaryAction.getWord("ZOOMNORMALVIEW"));
        MenuItem zoomOutViewMI = new MenuItem(objDictionaryAction.getWord("ZOOMOUTVIEW"));
        MenuItem closeEditMI = new MenuItem(objDictionaryAction.getWord("CLOSE"));
        
        mirrorVerticalEditMI.setAccelerator(new KeyCodeCombination(KeyCode.V, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        mirrorHorizontalEditMI.setAccelerator(new KeyCodeCombination(KeyCode.H, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        rotateClockwiseEditMI.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        rotateAnticlockwiseEditMI.setAccelerator(new KeyCodeCombination(KeyCode.A, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        moveUpEditMI.setAccelerator(new KeyCodeCombination(KeyCode.UP, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        moveDownEditMI.setAccelerator(new KeyCodeCombination(KeyCode.DOWN, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        moveLeftEditMI.setAccelerator(new KeyCodeCombination(KeyCode.LEFT, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        moveRightEditMI.setAccelerator(new KeyCodeCombination(KeyCode.RIGHT, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        moveUp8EditMI.setAccelerator(new KeyCodeCombination(KeyCode.UP, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        moveDown8EditMI.setAccelerator(new KeyCodeCombination(KeyCode.DOWN, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        moveLeft8EditMI.setAccelerator(new KeyCodeCombination(KeyCode.LEFT, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        moveRight8EditMI.setAccelerator(new KeyCodeCombination(KeyCode.RIGHT, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        tiltUpEditMI.setAccelerator(new KeyCodeCombination(KeyCode.UP, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        tiltDownEditMI.setAccelerator(new KeyCodeCombination(KeyCode.DOWN, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        tiltLeftEditMI.setAccelerator(new KeyCodeCombination(KeyCode.LEFT, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        tiltRightEditMI.setAccelerator(new KeyCodeCombination(KeyCode.RIGHT, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        zoomInViewMI.setAccelerator(new KeyCodeCombination(KeyCode.PLUS, KeyCombination.CONTROL_DOWN));
        normalViewMI.setAccelerator(new KeyCodeCombination(KeyCode.ENTER, KeyCombination.CONTROL_DOWN));
        zoomOutViewMI.setAccelerator(new KeyCodeCombination(KeyCode.MINUS, KeyCombination.CONTROL_DOWN));
        closeEditMI.setAccelerator(new KeyCodeCombination(KeyCode.ESCAPE));
        
        mirrorVerticalEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
        mirrorHorizontalEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
        rotateClockwiseEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_90.png"));
        rotateAnticlockwiseEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_anti_90.png"));
        moveRightEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right.png"));
        moveLeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left.png"));
        moveUpEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up.png"));
        moveDownEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down.png"));
        moveRight8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right_by_8.png"));
        moveLeft8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left_by_8.png"));
        moveUp8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up_by_8.png"));
        moveDown8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down_by_8.png"));        
        tiltRightEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_right.png"));
        tiltLeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_left.png"));
        tiltUpEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_up.png"));
        tiltDownEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_down.png"));
        zoomInViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
        normalViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
        zoomOutViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
        closeEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
             
        Menu mirrorMenu = new Menu(objDictionaryAction.getWord("MIRROR"));
        Menu rotateMenu = new Menu(objDictionaryAction.getWord("ROTATE"));
        Menu moveMenu = new Menu(objDictionaryAction.getWord("MOVE"));
        Menu tiltMenu = new Menu(objDictionaryAction.getWord("TILT"));        
        mirrorMenu.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/symmetry.png"));
        rotateMenu.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/update.png"));
        moveMenu.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/skip.png"));
        tiltMenu.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/diamond.png"));
        mirrorMenu.getItems().addAll(mirrorVerticalEditMI,mirrorHorizontalEditMI);
        rotateMenu.getItems().addAll(rotateClockwiseEditMI,rotateAnticlockwiseEditMI);
        moveMenu.getItems().addAll(moveRightEditMI, moveLeftEditMI, moveUpEditMI, moveDownEditMI, moveRight8EditMI, moveLeft8EditMI, moveUp8EditMI, moveDown8EditMI);
        tiltMenu.getItems().addAll(tiltRightEditMI, tiltLeftEditMI, tiltUpEditMI, tiltDownEditMI);
    
        //Add menu enable disable condition
        if(!isWorkingMode){
            mirrorVerticalEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
            mirrorVerticalEditMI.setDisable(true);
            mirrorHorizontalEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
            mirrorHorizontalEditMI.setDisable(true);
            rotateClockwiseEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_90.png"));
            rotateClockwiseEditMI.setDisable(true);
            rotateAnticlockwiseEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_anti_90.png"));
            rotateAnticlockwiseEditMI.setDisable(true);
            moveRightEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right.png"));
            moveRightEditMI.setDisable(true);
            moveLeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left.png"));
            moveLeftEditMI.setDisable(true);
            moveUpEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up.png"));
            moveUpEditMI.setDisable(true);
            moveDownEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down.png"));
            moveDownEditMI.setDisable(true);
            moveRight8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right_by_8.png"));
            moveRight8EditMI.setDisable(true);
            moveLeft8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left_by_8.png"));
            moveLeft8EditMI.setDisable(true);
            moveUp8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up_by_8.png"));
            moveUp8EditMI.setDisable(true);
            moveDown8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down_by_8.png"));
            moveDown8EditMI.setDisable(true);
            tiltRightEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_right.png"));
            tiltRightEditMI.setDisable(true);
            tiltLeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_left.png"));
            tiltLeftEditMI.setDisable(true);
            tiltUpEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_up.png"));
            tiltUpEditMI.setDisable(true);
            tiltDownEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_down.png"));
            tiltDownEditMI.setDisable(true);
            zoomInViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
            zoomInViewMI.setDisable(true);
            normalViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
            normalViewMI.setDisable(true);
            zoomOutViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
            zoomOutViewMI.setDisable(true); 
            closeEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            closeEditMI.setDisable(true);
        }else{
            mirrorVerticalEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
            mirrorVerticalEditMI.setDisable(false);
            mirrorHorizontalEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
            mirrorHorizontalEditMI.setDisable(false);
            rotateClockwiseEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_90.png"));
            rotateClockwiseEditMI.setDisable(false);
            rotateAnticlockwiseEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_anti_90.png"));
            rotateAnticlockwiseEditMI.setDisable(false);
            moveRightEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right.png"));
            moveRightEditMI.setDisable(false);
            moveLeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left.png"));
            moveLeftEditMI.setDisable(false);
            moveUpEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up.png"));
            moveUpEditMI.setDisable(false);
            moveDownEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down.png"));
            moveDownEditMI.setDisable(false);
            moveRight8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right_by_8.png"));
            moveRight8EditMI.setDisable(false);
            moveLeft8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left_by_8.png"));
            moveLeft8EditMI.setDisable(false);
            moveUp8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up_by_8.png"));
            moveUp8EditMI.setDisable(false);
            moveDown8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down_by_8.png"));
            moveDown8EditMI.setDisable(false);
            tiltRightEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_right.png"));
            tiltRightEditMI.setDisable(false);
            tiltLeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_left.png"));
            tiltLeftEditMI.setDisable(false);
            tiltUpEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_up.png"));
            tiltUpEditMI.setDisable(false);
            tiltDownEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_down.png"));
            tiltDownEditMI.setDisable(false);
            zoomInViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
            zoomInViewMI.setDisable(false);
            normalViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
            normalViewMI.setDisable(false);
            zoomOutViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
            zoomOutViewMI.setDisable(false);
            closeEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            closeEditMI.setDisable(false);
        }        
        //Add the action to Buttons.        
        mirrorVerticalEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                mirrorVerticalAction();
            }
        });  
        mirrorHorizontalEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                mirrorHorizontalAction();
            }
        }); 
        rotateClockwiseEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                rotateClockwiseAction();
            }
        }); 
        rotateAnticlockwiseEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                rotateAntiClockwiseAction();
            }
        }); 
        moveRightEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveRightAction();
            }
        });
        moveLeftEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveLeftAction();
            }
        });
        moveUpEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveUpAction();
            }
        });
        moveDownEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveDownAction();
            }
        });
        moveRight8EditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveRight8Action();
            }
        });
        moveLeft8EditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveLeft8Action();
            }
        });
        moveUp8EditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveUp8Action();
            }
        });
        moveDown8EditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveDown8Action();
            }
        });
        tiltRightEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                tiltRightAction();
            }
        });
        tiltLeftEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                tiltLeftAction();
            }
        });
        tiltUpEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                tiltUpAction();
            }
        });
        tiltDownEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                tiltDownAction();
            }
        });        
        zoomInViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                zoomInAction();
            }
        });
        normalViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                normalAction();
            }
        });
        zoomOutViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                zoomOutAction();
            }
        });
        closeEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                closeAction();
            }
        });
        //undoEditMI, redoEditMI, new SeparatorMenuItem(), 
        menuSelected.getItems().addAll(mirrorMenu, rotateMenu, moveMenu, tiltMenu, new SeparatorMenuItem(), zoomInViewMI, normalViewMI, zoomOutViewMI, closeEditMI);
    }
    private void populateTransformToolbar(){
        // mirror edit item
        Button mirrorVerticalEditBtn = new Button(); 
        mirrorVerticalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
        mirrorVerticalEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("VERTICALMIRROR")+" (Ctrl+Alt+V)\n"+objDictionaryAction.getWord("TOOLTIPVERTICALMIRROR")));
        mirrorVerticalEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        mirrorVerticalEditBtn.getStyleClass().addAll("toolbar-button");   
        // mirror edit item
        Button mirrorHorizontalEditBtn = new Button(); 
        mirrorHorizontalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
        mirrorHorizontalEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("HORIZENTALMIRROR")+" (Ctrl+Alt+H)\n"+objDictionaryAction.getWord("TOOLTIPHORIZENTALMIRROR")));
        mirrorHorizontalEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        mirrorHorizontalEditBtn.getStyleClass().addAll("toolbar-button");   
        // clock Wise edit item
        Button rotateClockwiseEditBtn = new Button(); 
        rotateClockwiseEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_90.png"));
        rotateClockwiseEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CLOCKROTATION")+" (Ctrl+Alt+C)\n"+objDictionaryAction.getWord("TOOLTIPCLOCKROTATION")));
        rotateClockwiseEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rotateClockwiseEditBtn.getStyleClass().addAll("toolbar-button");   
        // Anti clock wise edit item
        Button rotateAnticlockwiseEditBtn = new Button(); 
        rotateAnticlockwiseEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_anti_90.png"));
        rotateAnticlockwiseEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ANTICLOCKROTATION")+" (Ctrl+Alt+A)\n"+objDictionaryAction.getWord("TOOLTIPANTICLOCKROTATION")));
        rotateAnticlockwiseEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rotateAnticlockwiseEditBtn.getStyleClass().addAll("toolbar-button");
        // move Right
        Button moveRightEditBtn = new Button(); 
        moveRightEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right.png"));
        moveRightEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MOVERIGHT")+" (Ctrl+Shift+Right)\n"+objDictionaryAction.getWord("TOOLTIPMOVERIGHT")));
        moveRightEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveRightEditBtn.getStyleClass().addAll("toolbar-button");    
        // move Left
        Button moveLeftEditBtn = new Button(); 
        moveLeftEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left.png"));
        moveLeftEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MOVELEFT")+" (Ctrl+Shift+Left)\n"+objDictionaryAction.getWord("TOOLTIPMOVELEFT")));
        moveLeftEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveLeftEditBtn.getStyleClass().addAll("toolbar-button");    
        // move Up
        Button moveUpEditBtn = new Button(); 
        moveUpEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up.png"));
        moveUpEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MOVEUP")+" (Ctrl+Shift+Up)\n"+objDictionaryAction.getWord("TOOLTIPMOVEUP")));
        moveUpEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveUpEditBtn.getStyleClass().addAll("toolbar-button");    
        // move Down
        Button moveDownEditBtn = new Button(); 
        moveDownEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down.png"));
        moveDownEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MOVEDOWN")+" (Ctrl+Shift+Down)\n"+objDictionaryAction.getWord("TOOLTIPMOVEDOWN")));
        moveDownEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveDownEditBtn.getStyleClass().addAll("toolbar-button");
        // move Right 8
        Button moveRight8EditBtn = new Button(); 
        moveRight8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right_by_8.png"));
        moveRight8EditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MOVERIGHT8")+" (Alt+Shift+Right)\n"+objDictionaryAction.getWord("TOOLTIPMOVERIGHT8")));
        moveRight8EditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveRight8EditBtn.getStyleClass().addAll("toolbar-button");    
        // move Left 8
        Button moveLeft8EditBtn = new Button(); 
        moveLeft8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left_by_8.png"));
        moveLeft8EditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MOVELEFT8")+" (Alt+Shift+Left)\n"+objDictionaryAction.getWord("TOOLTIPMOVELEFT8")));
        moveLeft8EditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveLeft8EditBtn.getStyleClass().addAll("toolbar-button");    
        // move Up 8
        Button moveUp8EditBtn = new Button(); 
        moveUp8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up_by_8.png"));
        moveUp8EditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MOVEUP8")+" (Alt+Shift+Up)\n"+objDictionaryAction.getWord("TOOLTIPMOVEUP8")));
        moveUp8EditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveUp8EditBtn.getStyleClass().addAll("toolbar-button");    
        // move Down 8
        Button moveDown8EditBtn = new Button(); 
        moveDown8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down_by_8.png"));
        moveDown8EditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MOVEDOWN8")+" (Alt+Shift+Down)\n"+objDictionaryAction.getWord("TOOLTIPMOVEDOWN8")));
        moveDown8EditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveDown8EditBtn.getStyleClass().addAll("toolbar-button");
        // Tilt Right
        Button tiltRightEditBtn = new Button(); 
        tiltRightEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_right.png"));
        tiltRightEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("TILTRIGHT")+" (Ctrl+Alt+Right)\n"+objDictionaryAction.getWord("TOOLTIPTILTRIGHT")));
        tiltRightEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tiltRightEditBtn.getStyleClass().addAll("toolbar-button");    
        // Tilt Left
        Button tiltLeftEditBtn = new Button(); 
        tiltLeftEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_left.png"));
        tiltLeftEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("TILTLEFT")+" (Ctrl+Alt+Left)\n"+objDictionaryAction.getWord("TOOLTIPTILTLEFT")));
        tiltLeftEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tiltLeftEditBtn.getStyleClass().addAll("toolbar-button");    
        // Tilt Up
        Button tiltUpEditBtn = new Button(); 
        tiltUpEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_up.png"));
        tiltUpEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("TILTUP")+" (Ctrl+Alt+Up)\n"+objDictionaryAction.getWord("TOOLTIPTILTUP")));
        tiltUpEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tiltUpEditBtn.getStyleClass().addAll("toolbar-button");    
        // Tilt Down
        Button tiltDownEditBtn = new Button(); 
        tiltDownEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_down.png"));
        tiltDownEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("TILTDOWN")+" (Ctrl+Alt+Down)\n"+objDictionaryAction.getWord("TOOLTIPTILTDOWN")));
        tiltDownEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tiltDownEditBtn.getStyleClass().addAll("toolbar-button");
        // Zoom-In item
        Button zoomInViewBtn = new Button();
        zoomInViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
        zoomInViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ZOOMINVIEW")+" (Ctrl+Plus)\n"+objDictionaryAction.getWord("TOOLTIPZOOMINVIEW")));
        zoomInViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        zoomInViewBtn.getStyleClass().add("toolbar-button");    
        // Normal item
        Button normalViewBtn = new Button();
        normalViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
        normalViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ZOOMNORMALVIEW")+" (Ctrl+Enter)\n"+objDictionaryAction.getWord("TOOLTIPZOOMNORMALVIEW")));
        normalViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        normalViewBtn.getStyleClass().add("toolbar-button");    
        // Zoom-Out item
        Button zoomOutViewBtn = new Button();
        zoomOutViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
        zoomOutViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ZOOMOUTVIEW")+" (Ctrl+Minus)\n"+objDictionaryAction.getWord("TOOLTIPZOOMOUTVIEW")));
        zoomOutViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        zoomOutViewBtn.getStyleClass().add("toolbar-button");    
        // Clear
        Button closeEditBtn = new Button(); 
        closeEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        closeEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CLOSE")+" (Esc)\n"+objDictionaryAction.getWord("TOOLTIPCLOSE")));
        closeEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        closeEditBtn.getStyleClass().addAll("toolbar-button"); 
        //Add menu enable disable condition
        if(!isWorkingMode){
            mirrorVerticalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
            mirrorVerticalEditBtn.setDisable(true);
            mirrorVerticalEditBtn.setCursor(Cursor.WAIT);
            mirrorHorizontalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
            mirrorHorizontalEditBtn.setDisable(true);
            mirrorHorizontalEditBtn.setCursor(Cursor.WAIT);            
            rotateClockwiseEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_90.png"));
            rotateClockwiseEditBtn.setDisable(true);
            rotateClockwiseEditBtn.setCursor(Cursor.WAIT);
            rotateAnticlockwiseEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_anti_90.png"));
            rotateAnticlockwiseEditBtn.setDisable(true);
            rotateAnticlockwiseEditBtn.setCursor(Cursor.WAIT);
            moveRightEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right.png"));
            moveRightEditBtn.setDisable(true);
            moveRightEditBtn.setCursor(Cursor.WAIT);
            moveLeftEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left.png"));
            moveLeftEditBtn.setDisable(true);
            moveLeftEditBtn.setCursor(Cursor.WAIT);
            moveUpEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up.png"));
            moveUpEditBtn.setDisable(true);
            moveUpEditBtn.setCursor(Cursor.WAIT);
            moveDownEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down.png"));
            moveDownEditBtn.setDisable(true);
            moveDownEditBtn.setCursor(Cursor.WAIT);
            moveRight8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right_by_8.png"));
            moveRight8EditBtn.setDisable(true);
            moveRight8EditBtn.setCursor(Cursor.WAIT);
            moveLeft8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left_by_8.png"));
            moveLeft8EditBtn.setDisable(true);
            moveLeft8EditBtn.setCursor(Cursor.WAIT);
            moveUp8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up_by_8.png"));
            moveUp8EditBtn.setDisable(true);
            moveUp8EditBtn.setCursor(Cursor.WAIT);
            moveDown8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down_by_8.png"));
            moveDown8EditBtn.setDisable(true);
            moveDown8EditBtn.setCursor(Cursor.WAIT);
            tiltRightEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_right.png"));
            tiltRightEditBtn.setDisable(true);
            tiltRightEditBtn.setCursor(Cursor.WAIT);
            tiltLeftEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_left.png"));
            tiltLeftEditBtn.setDisable(true);
            tiltLeftEditBtn.setCursor(Cursor.WAIT);
            tiltUpEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_up.png"));
            tiltUpEditBtn.setDisable(true);
            tiltUpEditBtn.setCursor(Cursor.WAIT);
            tiltDownEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_down.png"));
            tiltDownEditBtn.setDisable(true);
            tiltDownEditBtn.setCursor(Cursor.WAIT);
            zoomInViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
            zoomInViewBtn.setDisable(true);
            zoomInViewBtn.setCursor(Cursor.WAIT);
            normalViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
            normalViewBtn.setDisable(true);
            normalViewBtn.setCursor(Cursor.WAIT);
            zoomOutViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
            zoomOutViewBtn.setDisable(true);
            zoomOutViewBtn.setCursor(Cursor.WAIT); 
            closeEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            closeEditBtn.setDisable(true);
            closeEditBtn.setCursor(Cursor.WAIT);
        }else{
            mirrorVerticalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
            mirrorVerticalEditBtn.setDisable(false);
            mirrorVerticalEditBtn.setCursor(Cursor.HAND);
            mirrorHorizontalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
            mirrorHorizontalEditBtn.setDisable(false);
            mirrorHorizontalEditBtn.setCursor(Cursor.HAND);
            rotateClockwiseEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_90.png"));
            rotateClockwiseEditBtn.setDisable(false);
            rotateClockwiseEditBtn.setCursor(Cursor.HAND);
            rotateAnticlockwiseEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_anti_90.png"));
            rotateAnticlockwiseEditBtn.setDisable(false);
            rotateAnticlockwiseEditBtn.setCursor(Cursor.HAND);
            moveRightEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right.png"));
            moveRightEditBtn.setDisable(false);
            moveRightEditBtn.setCursor(Cursor.HAND);
            moveLeftEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left.png"));
            moveLeftEditBtn.setDisable(false);
            moveLeftEditBtn.setCursor(Cursor.HAND);
            moveUpEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up.png"));
            moveUpEditBtn.setDisable(false);
            moveUpEditBtn.setCursor(Cursor.HAND);
            moveDownEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down.png"));
            moveDownEditBtn.setDisable(false);
            moveDownEditBtn.setCursor(Cursor.HAND);
            moveRight8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right_by_8.png"));
            moveRight8EditBtn.setDisable(false);
            moveRight8EditBtn.setCursor(Cursor.HAND);
            moveLeft8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left_by_8.png"));
            moveLeft8EditBtn.setDisable(false);
            moveLeft8EditBtn.setCursor(Cursor.HAND);
            moveUp8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up_by_8.png"));
            moveUp8EditBtn.setDisable(false);
            moveUp8EditBtn.setCursor(Cursor.HAND);
            moveDown8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down_by_8.png"));
            moveDown8EditBtn.setDisable(false);
            moveDown8EditBtn.setCursor(Cursor.HAND);
            tiltRightEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_right.png"));
            tiltRightEditBtn.setDisable(false);
            tiltRightEditBtn.setCursor(Cursor.HAND);
            tiltLeftEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_left.png"));
            tiltLeftEditBtn.setDisable(false);
            tiltLeftEditBtn.setCursor(Cursor.HAND);
            tiltUpEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_up.png"));
            tiltUpEditBtn.setDisable(false);
            tiltUpEditBtn.setCursor(Cursor.HAND);
            tiltDownEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_down.png"));
            tiltDownEditBtn.setDisable(false);
            tiltDownEditBtn.setCursor(Cursor.HAND);
            zoomInViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
            zoomInViewBtn.setDisable(false);
            zoomInViewBtn.setCursor(Cursor.HAND); 
            normalViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
            normalViewBtn.setDisable(false);
            normalViewBtn.setCursor(Cursor.HAND); 
            zoomOutViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
            zoomOutViewBtn.setDisable(false);
            zoomOutViewBtn.setCursor(Cursor.HAND); 
            closeEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            closeEditBtn.setDisable(false);
            closeEditBtn.setCursor(Cursor.HAND);         
        }        
        //Add the action to Buttons.
        mirrorVerticalEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mirrorVerticalAction();
            }
        });  
        mirrorHorizontalEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mirrorHorizontalAction();
            }
        }); 
        rotateClockwiseEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rotateClockwiseAction();
            }
        }); 
        rotateAnticlockwiseEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rotateAntiClockwiseAction();
            }
        }); 
        moveRightEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                moveRightAction();
            }
        });
        moveLeftEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                moveLeftAction();
            }
        });
        moveUpEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                moveUpAction();
            }
        });
        moveDownEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                moveDownAction();
            }
        });
        moveRight8EditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                moveRight8Action();
            }
        });
        moveLeft8EditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                moveLeft8Action();
            }
        });
        moveUp8EditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                moveUp8Action();
            }
        });
        moveDown8EditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                moveDown8Action();
            }
        });
        tiltRightEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                tiltRightAction();
            }
        });
        tiltLeftEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                tiltLeftAction();
            }
        });
        tiltUpEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                tiltUpAction();
            }
        });
        tiltDownEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                tiltDownAction();
            }
        });        
        zoomInViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                zoomInAction();
            }
        });
        normalViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                normalAction();
            }
        });
        zoomOutViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                zoomOutAction();
            }
        });
        closeEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                closeAction();
            }
        });
        //Create some Buttons.        
        FlowPane flow = new FlowPane();        
        flow.setPrefWrapLength(objConfiguration.WIDTH*.95);
        flow.getStyleClass().addAll("flow");
        //Add the Buttons to the ToolBar.
        //undoEditBtn,redoEditBtn,
        flow.getChildren().addAll(mirrorVerticalEditBtn,mirrorHorizontalEditBtn,rotateClockwiseEditBtn,rotateAnticlockwiseEditBtn,moveRightEditBtn,moveLeftEditBtn,moveUpEditBtn,moveDownEditBtn,moveRight8EditBtn,moveLeft8EditBtn,moveUp8EditBtn,moveDown8EditBtn,tiltRightEditBtn,tiltLeftEditBtn,tiltUpEditBtn,tiltDownEditBtn,zoomInViewBtn,normalViewBtn,zoomOutViewBtn,closeEditBtn);
        flow.setAlignment(Pos.TOP_LEFT);
        toolBar.getItems().addAll(flow);
    }
    /**
     * artworkVerticalMirror
     * <p>
     * This method is used for creating GUI of artwork reduce color pane.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for creating GUI of artwork reduce color pane.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void mirrorVerticalAction(){
         try {                       
            lblStatus.setText(objDictionaryAction.getWord("ACTIONVERTICALMIRROR"));
            lblStatus.setText(objDictionaryAction.getWord("ACTIONVERTICALMIRROR"));
            //objURF.store(objBufferedImage);
            objArtworkAction = new ArtworkAction();
            objBufferedImage = objArtworkAction.getImageVerticalMirror(objBufferedImage);
            objUR.doCommand("VMirror", deepCopy(objBufferedImage));
            plotEditWeave();
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Mirror Vertical",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }  
    }
    /**
     * artworkHorizentalMirror
     * <p>
     * This method is used for creating GUI of artwork reduce color pane.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for creating GUI of artwork reduce color pane.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void mirrorHorizontalAction(){        
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONHORIZENTALMIRROR"));
            lblStatus.setText(objDictionaryAction.getWord("ACTIONHORIZENTALMIRROR"));
            //objURF.store(objBufferedImage);
            objArtworkAction = new ArtworkAction();
            objBufferedImage = objArtworkAction.getImageHorizontalMirror(objBufferedImage);
            objUR.doCommand("HMirror", deepCopy(objBufferedImage));
            plotEditWeave();
            plotEditWeave();
        } catch (Exception ex) {
          new Logging("SEVERE",getClass().getName(),"Mirror Vertical",ex);
          lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    /**
     * artworkClockRotation
     * <p>
     * This method is used for creating GUI of artwork reduce color pane.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for creating GUI of artwork reduce color pane.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void rotateClockwiseAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONCLOCKROTATION"));
            lblStatus.setText(objDictionaryAction.getWord("ACTIONCLOCKROTATION"));
            //objURF.store(objBufferedImage);
            objArtworkAction = new ArtworkAction();
            objBufferedImage = objArtworkAction.getImageRotation(objBufferedImage,"CLOCKWISE");
            objUR.doCommand("CRotate", deepCopy(objBufferedImage));
            plotEditWeave();
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Rotate Clock Wise ",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    /**
     * artworkAntiClockRotation
     * <p>
     * This method is used for creating GUI of artwork reduce color pane.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for creating GUI of artwork reduce color pane.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void rotateAntiClockwiseAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONANTICLOCKROTATION"));
            lblStatus.setText(objDictionaryAction.getWord("ACTIONANTICLOCKROTATION"));
            //objURF.store(objBufferedImage);
            objArtworkAction = new ArtworkAction();
            objBufferedImage = objArtworkAction.getImageRotation(objBufferedImage,"COUNTERCLOCKWISE");
            objUR.doCommand("Counter Clockwise Rotation", deepCopy(objBufferedImage));
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Anti Rotate Clock Wise ",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void moveRightAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVERIGHT"));
            //objURF.store(objBufferedImage);
            objArtworkAction = new ArtworkAction();
            objBufferedImage = objArtworkAction.moveRight(objBufferedImage,1);
            objUR.doCommand("moveR", deepCopy(objBufferedImage));
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Right",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void moveLeftAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVELEFT"));
            //objURF.store(objBufferedImage);
            objArtworkAction = new ArtworkAction();
            objBufferedImage = objArtworkAction.moveLeft(objBufferedImage,1);
            objUR.doCommand("moveL", deepCopy(objBufferedImage));
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Left",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void moveUpAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVEUP"));
            //objURF.store(objBufferedImage);
            objArtworkAction = new ArtworkAction();
            objBufferedImage = objArtworkAction.moveUp(objBufferedImage,1);
            objUR.doCommand("moveU", deepCopy(objBufferedImage));
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Up",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void moveDownAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVEDOWN"));
            //objURF.store(objBufferedImage);
            objArtworkAction = new ArtworkAction();
            objBufferedImage = objArtworkAction.moveDown(objBufferedImage,1);
            objUR.doCommand("moveD", deepCopy(objBufferedImage));
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Down",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void moveRight8Action(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVERIGHT8"));
            //objURF.store(objBufferedImage);
            objArtworkAction = new ArtworkAction();
            objBufferedImage = objArtworkAction.moveRight(objBufferedImage,8);        
            objUR.doCommand("moveR8", deepCopy(objBufferedImage));
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Right",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void moveLeft8Action(){        
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVELEFT8"));
            //objURF.store(objBufferedImage);
            objArtworkAction = new ArtworkAction();
            objBufferedImage = objArtworkAction.moveLeft(objBufferedImage,8);
            objUR.doCommand("moveL8", deepCopy(objBufferedImage));
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Left",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void moveUp8Action(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVEUP8"));
            //objURF.store(objBufferedImage);
            objArtworkAction = new ArtworkAction();
            objBufferedImage = objArtworkAction.moveUp(objBufferedImage,8);
            objUR.doCommand("moveU8", deepCopy(objBufferedImage));
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Up",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void moveDown8Action(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVEDOWN8"));
            //objURF.store(objBufferedImage);
            objArtworkAction = new ArtworkAction();
            objBufferedImage = objArtworkAction.moveDown(objBufferedImage,8);
            objUR.doCommand("moveD8", deepCopy(objBufferedImage));
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Down",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void tiltRightAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONTILTRIGHT"));
            //objURF.store(objBufferedImage);
            objArtworkAction = new ArtworkAction();
            objBufferedImage = objArtworkAction.tiltRight(objBufferedImage);
            objUR.doCommand("tiltR", deepCopy(objBufferedImage));
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Tilt Right ",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void tiltLeftAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONTILTLEFT"));
            //objURF.store(objBufferedImage);
            objArtworkAction = new ArtworkAction();
            objBufferedImage = objArtworkAction.tiltLeft(objBufferedImage);                     
            objUR.doCommand("tiltL", deepCopy(objBufferedImage));
            plotEditWeave();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Tilt Left ",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void tiltUpAction(){
        try {
             lblStatus.setText(objDictionaryAction.getWord("ACTIONTILTUP"));
             //objURF.store(objBufferedImage);
            objArtworkAction = new ArtworkAction();
             objBufferedImage = objArtworkAction.tiltUp(objBufferedImage);
             objUR.doCommand("tiltU", deepCopy(objBufferedImage));
            plotEditWeave();
         } catch (Exception ex) {
             new Logging("SEVERE",getClass().getName(),"Tilt Up ",ex);
             lblStatus.setText(objDictionaryAction.getWord("ERROR"));
         } 
    }
    private void tiltDownAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONTILTDOWN"));
            //objURF.store(objBufferedImage);
            objArtworkAction = new ArtworkAction();
            objBufferedImage = objArtworkAction.tiltDown(objBufferedImage);
            objUR.doCommand("tiltD", deepCopy(objBufferedImage));
            plotEditWeave();
        } catch (Exception ex) {
           new Logging("SEVERE",getClass().getName(),"Tilt Down ",ex);
           lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void closeAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
        selectedMenu = "EDIT";
        transformMenu.hide();
        transformMenu.setVisible(false);
        paintMenu.hide();
        paintMenu.setVisible(false);        
        fileMenu.setDisable(false);
        editMenu.setDisable(false);
        viewMenu.setDisable(false);
        utilityMenu.setDisable(false);
        runMenu.setDisable(false);
        transformMenu.setDisable(true);
        paintMenu.setDisable(true);
        menuHighlight();       
        
        plotViewActionMode = 12;
        plotViewAction();
    }
    /**
 * populatePaintToolbarMenu
 * <p>
 * Function use for editing tool bar for menu item Edit,
 * and binding events for each tools. 
 * 
 * @exception   (@throws SQLException)
 * @author      Amit Kumar Singh
 * @version     %I%, %G%
 * @since       1.0
 * @see         javafx.event.*;
 * @link        WeaveView
 */
    private void populatePaintToolbarMenu(){
        //For drop down menus
        menuSelected.setTooltip(new Tooltip(objDictionaryAction.getWord("EDIT")));
       
        MenuItem mirrorVerticalEditMI = new MenuItem(objDictionaryAction.getWord("VERTICALMIRROR"));
        MenuItem mirrorHorizontalEditMI = new MenuItem(objDictionaryAction.getWord("HORIZENTALMIRROR"));
        MenuItem rotateClockwiseEditMI = new MenuItem(objDictionaryAction.getWord("CLOCKROTATION"));
        MenuItem rotateAnticlockwiseEditMI = new MenuItem(objDictionaryAction.getWord("ANTICLOCKROTATION"));
        MenuItem moveRightEditMI = new MenuItem(objDictionaryAction.getWord("MOVERIGHT"));
        MenuItem moveLeftEditMI = new MenuItem(objDictionaryAction.getWord("MOVELEFT"));
        MenuItem moveUpEditMI = new MenuItem(objDictionaryAction.getWord("MOVEUP"));
        MenuItem moveDownEditMI = new MenuItem(objDictionaryAction.getWord("MOVEDOWN"));        
        MenuItem moveRight8EditMI = new MenuItem(objDictionaryAction.getWord("MOVERIGHT8"));
        MenuItem moveLeft8EditMI = new MenuItem(objDictionaryAction.getWord("MOVELEFT8"));
        MenuItem moveUp8EditMI = new MenuItem(objDictionaryAction.getWord("MOVEUP8"));
        MenuItem moveDown8EditMI = new MenuItem(objDictionaryAction.getWord("MOVEDOWN8"));        
        MenuItem tiltRightEditMI = new MenuItem(objDictionaryAction.getWord("TILTRIGHT"));
        MenuItem tiltLeftEditMI = new MenuItem(objDictionaryAction.getWord("TILTLEFT"));
        MenuItem tiltUpEditMI = new MenuItem(objDictionaryAction.getWord("TILTUP"));
        MenuItem tiltDownEditMI = new MenuItem(objDictionaryAction.getWord("TILTDOWN")); 
        MenuItem zoomInViewMI = new MenuItem(objDictionaryAction.getWord("ZOOMINVIEW"));
        MenuItem normalViewMI = new MenuItem(objDictionaryAction.getWord("ZOOMNORMALVIEW"));
        MenuItem zoomOutViewMI = new MenuItem(objDictionaryAction.getWord("ZOOMOUTVIEW"));
        MenuItem closeEditMI = new MenuItem(objDictionaryAction.getWord("CLOSE"));
        
        mirrorVerticalEditMI.setAccelerator(new KeyCodeCombination(KeyCode.V, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        mirrorHorizontalEditMI.setAccelerator(new KeyCodeCombination(KeyCode.H, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        rotateClockwiseEditMI.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        rotateAnticlockwiseEditMI.setAccelerator(new KeyCodeCombination(KeyCode.A, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        moveUpEditMI.setAccelerator(new KeyCodeCombination(KeyCode.UP, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        moveDownEditMI.setAccelerator(new KeyCodeCombination(KeyCode.DOWN, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        moveLeftEditMI.setAccelerator(new KeyCodeCombination(KeyCode.LEFT, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        moveRightEditMI.setAccelerator(new KeyCodeCombination(KeyCode.RIGHT, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        moveUp8EditMI.setAccelerator(new KeyCodeCombination(KeyCode.UP, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        moveDown8EditMI.setAccelerator(new KeyCodeCombination(KeyCode.DOWN, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        moveLeft8EditMI.setAccelerator(new KeyCodeCombination(KeyCode.LEFT, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        moveRight8EditMI.setAccelerator(new KeyCodeCombination(KeyCode.RIGHT, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        tiltUpEditMI.setAccelerator(new KeyCodeCombination(KeyCode.UP, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        tiltDownEditMI.setAccelerator(new KeyCodeCombination(KeyCode.DOWN, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        tiltLeftEditMI.setAccelerator(new KeyCodeCombination(KeyCode.LEFT, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        tiltRightEditMI.setAccelerator(new KeyCodeCombination(KeyCode.RIGHT, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        zoomInViewMI.setAccelerator(new KeyCodeCombination(KeyCode.PLUS, KeyCombination.CONTROL_DOWN));
        normalViewMI.setAccelerator(new KeyCodeCombination(KeyCode.ENTER, KeyCombination.CONTROL_DOWN));
        zoomOutViewMI.setAccelerator(new KeyCodeCombination(KeyCode.MINUS, KeyCombination.CONTROL_DOWN));
        closeEditMI.setAccelerator(new KeyCodeCombination(KeyCode.ESCAPE));
        
        mirrorVerticalEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
        mirrorHorizontalEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
        rotateClockwiseEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_90.png"));
        rotateAnticlockwiseEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_anti_90.png"));
        moveRightEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right.png"));
        moveLeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left.png"));
        moveUpEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up.png"));
        moveDownEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down.png"));
        moveRight8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right_by_8.png"));
        moveLeft8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left_by_8.png"));
        moveUp8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up_by_8.png"));
        moveDown8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down_by_8.png"));        
        tiltRightEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_right.png"));
        tiltLeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_left.png"));
        tiltUpEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_up.png"));
        tiltDownEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_down.png"));
        zoomInViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
        normalViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
        zoomOutViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
        closeEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
             
        Menu mirrorMenu = new Menu(objDictionaryAction.getWord("MIRROR"));
        Menu rotateMenu = new Menu(objDictionaryAction.getWord("ROTATE"));
        Menu moveMenu = new Menu(objDictionaryAction.getWord("MOVE"));
        Menu tiltMenu = new Menu(objDictionaryAction.getWord("TILT"));        
        mirrorMenu.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/symmetry.png"));
        rotateMenu.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/update.png"));
        moveMenu.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/skip.png"));
        tiltMenu.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/diamond.png"));
        mirrorMenu.getItems().addAll(mirrorVerticalEditMI,mirrorHorizontalEditMI);
        rotateMenu.getItems().addAll(rotateClockwiseEditMI,rotateAnticlockwiseEditMI);
        moveMenu.getItems().addAll(moveRightEditMI, moveLeftEditMI, moveUpEditMI, moveDownEditMI, moveRight8EditMI, moveLeft8EditMI, moveUp8EditMI, moveDown8EditMI);
        tiltMenu.getItems().addAll(tiltRightEditMI, tiltLeftEditMI, tiltUpEditMI, tiltDownEditMI);
    
        //Add menu enable disable condition
        if(!isWorkingMode){
            mirrorVerticalEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
            mirrorVerticalEditMI.setDisable(true);
            mirrorHorizontalEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
            mirrorHorizontalEditMI.setDisable(true);
            rotateClockwiseEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_90.png"));
            rotateClockwiseEditMI.setDisable(true);
            rotateAnticlockwiseEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_anti_90.png"));
            rotateAnticlockwiseEditMI.setDisable(true);
            moveRightEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right.png"));
            moveRightEditMI.setDisable(true);
            moveLeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left.png"));
            moveLeftEditMI.setDisable(true);
            moveUpEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up.png"));
            moveUpEditMI.setDisable(true);
            moveDownEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down.png"));
            moveDownEditMI.setDisable(true);
            moveRight8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right_by_8.png"));
            moveRight8EditMI.setDisable(true);
            moveLeft8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left_by_8.png"));
            moveLeft8EditMI.setDisable(true);
            moveUp8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up_by_8.png"));
            moveUp8EditMI.setDisable(true);
            moveDown8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down_by_8.png"));
            moveDown8EditMI.setDisable(true);
            tiltRightEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_right.png"));
            tiltRightEditMI.setDisable(true);
            tiltLeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_left.png"));
            tiltLeftEditMI.setDisable(true);
            tiltUpEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_up.png"));
            tiltUpEditMI.setDisable(true);
            tiltDownEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_down.png"));
            tiltDownEditMI.setDisable(true);
            zoomInViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
            zoomInViewMI.setDisable(true);
            normalViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
            normalViewMI.setDisable(true);
            zoomOutViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
            zoomOutViewMI.setDisable(true); 
            closeEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            closeEditMI.setDisable(true);
        }else{
            mirrorVerticalEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
            mirrorVerticalEditMI.setDisable(false);
            mirrorHorizontalEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
            mirrorHorizontalEditMI.setDisable(false);
            rotateClockwiseEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_90.png"));
            rotateClockwiseEditMI.setDisable(false);
            rotateAnticlockwiseEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_anti_90.png"));
            rotateAnticlockwiseEditMI.setDisable(false);
            moveRightEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right.png"));
            moveRightEditMI.setDisable(false);
            moveLeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left.png"));
            moveLeftEditMI.setDisable(false);
            moveUpEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up.png"));
            moveUpEditMI.setDisable(false);
            moveDownEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down.png"));
            moveDownEditMI.setDisable(false);
            moveRight8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right_by_8.png"));
            moveRight8EditMI.setDisable(false);
            moveLeft8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left_by_8.png"));
            moveLeft8EditMI.setDisable(false);
            moveUp8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up_by_8.png"));
            moveUp8EditMI.setDisable(false);
            moveDown8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down_by_8.png"));
            moveDown8EditMI.setDisable(false);
            tiltRightEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_right.png"));
            tiltRightEditMI.setDisable(false);
            tiltLeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_left.png"));
            tiltLeftEditMI.setDisable(false);
            tiltUpEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_up.png"));
            tiltUpEditMI.setDisable(false);
            tiltDownEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_down.png"));
            tiltDownEditMI.setDisable(false);
            zoomInViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
            zoomInViewMI.setDisable(false);
            normalViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
            normalViewMI.setDisable(false);
            zoomOutViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
            zoomOutViewMI.setDisable(false);
            closeEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            closeEditMI.setDisable(false);
        }        
        //Add the action to Buttons.        
        mirrorVerticalEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                mirrorVerticalAction();
            }
        });  
        mirrorHorizontalEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                mirrorHorizontalAction();
            }
        }); 
        rotateClockwiseEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                rotateClockwiseAction();
            }
        }); 
        rotateAnticlockwiseEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                rotateAntiClockwiseAction();
            }
        }); 
        moveRightEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveRightAction();
            }
        });
        moveLeftEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveLeftAction();
            }
        });
        moveUpEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveUpAction();
            }
        });
        moveDownEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveDownAction();
            }
        });
        moveRight8EditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveRight8Action();
            }
        });
        moveLeft8EditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveLeft8Action();
            }
        });
        moveUp8EditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveUp8Action();
            }
        });
        moveDown8EditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveDown8Action();
            }
        });
        tiltRightEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                tiltRightAction();
            }
        });
        tiltLeftEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                tiltLeftAction();
            }
        });
        tiltUpEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                tiltUpAction();
            }
        });
        tiltDownEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                tiltDownAction();
            }
        });        
        zoomInViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                zoomInAction();
            }
        });
        normalViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                normalAction();
            }
        });
        zoomOutViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                zoomOutAction();
            }
        });
        closeEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                closeAction();
            }
        });
        //undoEditMI, redoEditMI, new SeparatorMenuItem(), 
        menuSelected.getItems().addAll(mirrorMenu, rotateMenu, moveMenu, tiltMenu, new SeparatorMenuItem(), zoomInViewMI, normalViewMI, zoomOutViewMI, closeEditMI);
    }
    private void populatePaintToolbar(){
        // pencil edit item
        Button pencilToolEditBtn = new Button(); 
        pencilToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/pencil.png"));
        pencilToolEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("PENCIL")+"\n"+objDictionaryAction.getWord("TOOLTIPPENCIL")));
        pencilToolEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        pencilToolEditBtn.getStyleClass().addAll("toolbar-button");  
        // eraser
        Button eraserToolEditBtn = new Button(); 
        eraserToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/eraser.png"));
        eraserToolEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ERASER")+"\n"+objDictionaryAction.getWord("TOOLTIPERASER")));
        eraserToolEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        eraserToolEditBtn.getStyleClass().addAll("toolbar-button");   
        // line edit item
        Button lineEditBtn = new Button(); 
        lineEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/line.png"));
        lineEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("LINE")+"\n"+objDictionaryAction.getWord("TOOLTIPLINE")));
        lineEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        lineEditBtn.getStyleClass().addAll("toolbar-button"); 
        // arc edit item
        Button arcEditBtn = new Button(); 
        arcEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/arc.png"));
        arcEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ARC")+"\n"+objDictionaryAction.getWord("TOOLTIPARC")));
        arcEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        arcEditBtn.getStyleClass().addAll("toolbar-button");    
        // curve edit item
        Button bezierCurveEditBtn = new Button(); 
        bezierCurveEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/bezier_curve.png"));
        bezierCurveEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("BEZIERCURVE")+"\n"+objDictionaryAction.getWord("TOOLTIPBEZIERCURVE")));
        bezierCurveEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        bezierCurveEditBtn.getStyleClass().addAll("toolbar-button");
        // oval edit item
        Button ovalEditBtn = new Button(); 
        ovalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/oval.png"));
        ovalEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("OVAL")+"\n"+objDictionaryAction.getWord("TOOLTIPOVAL")));
        ovalEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        ovalEditBtn.getStyleClass().addAll("toolbar-button");    
        // oval edit item
        Button ellipseEditBtn = new Button(); 
        ellipseEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/oval.png"));
        ellipseEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ellipse")+"\n"+objDictionaryAction.getWord("TOOLTIPOVAL")));
        ellipseEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        ellipseEditBtn.getStyleClass().addAll("toolbar-button");    
        // rectangle edit item
        Button rectangleEditBtn = new Button(); 
        rectangleEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/rectangle.png"));
        rectangleEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("RECTANGLE")+"\n"+objDictionaryAction.getWord("TOOLTIPRECTANGLE")));
        rectangleEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rectangleEditBtn.getStyleClass().addAll("toolbar-button"); 
        // rectangle edit item
        Button rectangleRoundEditBtn = new Button(); 
        rectangleRoundEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/rectangle.png"));
        rectangleRoundEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("RECTANGLE")+"\n"+objDictionaryAction.getWord("TOOLTIPRECTANGLE")));
        rectangleRoundEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rectangleRoundEditBtn.getStyleClass().addAll("toolbar-button"); 
        // triangle edit item
        Button triangleRightangleEditBtn = new Button(); 
        triangleRightangleEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/triangle.png"));
        triangleRightangleEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("TRIANGLE")+"\n"+objDictionaryAction.getWord("TOOLTIPTRIANGLE")));
        triangleRightangleEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        //triangleRightangleEditBtn.getStyleClass().addAll("toolbar-button");   
        // triangle edit item
        Button triangleEquilateralEditBtn = new Button(); 
        triangleEquilateralEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/triangle.png"));
        triangleEquilateralEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("TRIANGLE")+"\n"+objDictionaryAction.getWord("TOOLTIPTRIANGLE")));
        triangleEquilateralEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        triangleEquilateralEditBtn.getStyleClass().addAll("toolbar-button");   
        // heart edit item
        Button heartEditBtn = new Button(); 
        heartEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/heart.png"));
        heartEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("HEART")+"\n"+objDictionaryAction.getWord("TOOLTIPHEART")));
        heartEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        heartEditBtn.getStyleClass().addAll("toolbar-button");   
        // diamond edit item
        Button diamondEditBtn = new Button(); 
        diamondEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/diamond.png"));
        diamondEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("DIAMOND")+"\n"+objDictionaryAction.getWord("TOOLTIPDIAMOND")));
        diamondEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        diamondEditBtn.getStyleClass().addAll("toolbar-button"); 
        // diamond edit item
        Button rhombusEditBtn = new Button(); 
        rhombusEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/diamond.png"));
        rhombusEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("rhombus")+"\n"+objDictionaryAction.getWord("TOOLTIPDIAMOND")));
        rhombusEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rhombusEditBtn.getStyleClass().addAll("toolbar-button"); 
        // pentagon edit item
        Button pentagonEditBtn = new Button(); 
        pentagonEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/pentagon.png"));
        pentagonEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("PENTAGONE")+"\n"+objDictionaryAction.getWord("TOOLTIPPENTAGONE")));
        pentagonEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        pentagonEditBtn.getStyleClass().addAll("toolbar-button");   
        //  hexagon edit item
        Button hexagonEditBtn = new Button(); 
        hexagonEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/hexagon.png"));
        hexagonEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("HEXAGONE")+"\n"+objDictionaryAction.getWord("TOOLTIPHEXAGONE")));
        hexagonEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        hexagonEditBtn.getStyleClass().addAll("toolbar-button");
        // four point star  edit item
        Button fourPointStarEditBtn = new Button(); 
        fourPointStarEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/four_star.png"));
        fourPointStarEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("FOURPOINTSTAR")+"\n"+objDictionaryAction.getWord("TOOLTIPFOURPOINTSTAR")));
        fourPointStarEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        fourPointStarEditBtn.getStyleClass().addAll("toolbar-button");
        // five point star  edit item
        Button fivePointStarEditBtn = new Button(); 
        fivePointStarEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/five_star.png"));
        fivePointStarEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("FIVEPOINTSTAR")+"\n"+objDictionaryAction.getWord("TOOLTIPFIVEPOINTSTAR")));
        fivePointStarEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        fivePointStarEditBtn.getStyleClass().addAll("toolbar-button");  
        // six point star  edit item
        Button sixPointStarEditBtn = new Button(); 
        sixPointStarEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/six_star.png"));
        sixPointStarEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SIXPOINTSTAR")+"\n"+objDictionaryAction.getWord("TOOLTIPSIXPOINTSTAR")));
        sixPointStarEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        sixPointStarEditBtn.getStyleClass().addAll("toolbar-button");
        // polygon  edit item
        Button ploygonEditBtn = new Button(); 
        ploygonEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/rhombus.png"));
        ploygonEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("POLYGON")+"\n"+objDictionaryAction.getWord("TOOLTIPPOLYGON")));
        ploygonEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        ploygonEditBtn.getStyleClass().addAll("toolbar-button");  
        // Zoom-In item
        Button zoomInViewBtn = new Button();
        zoomInViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
        zoomInViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ZOOMINVIEW")+" (Ctrl+Plus)\n"+objDictionaryAction.getWord("TOOLTIPZOOMINVIEW")));
        zoomInViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        zoomInViewBtn.getStyleClass().add("toolbar-button");    
        // Normal item
        Button normalViewBtn = new Button();
        normalViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
        normalViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ZOOMNORMALVIEW")+" (Ctrl+Enter)\n"+objDictionaryAction.getWord("TOOLTIPZOOMNORMALVIEW")));
        normalViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        normalViewBtn.getStyleClass().add("toolbar-button");    
        // Zoom-Out item
        Button zoomOutViewBtn = new Button();
        zoomOutViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
        zoomOutViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ZOOMOUTVIEW")+" (Ctrl+Minus)\n"+objDictionaryAction.getWord("TOOLTIPZOOMOUTVIEW")));
        zoomOutViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        zoomOutViewBtn.getStyleClass().add("toolbar-button");    
        // Clear
        Button closeEditBtn = new Button(); 
        closeEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        closeEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CLOSE")+" (Esc)\n"+objDictionaryAction.getWord("TOOLTIPCLOSE")));
        closeEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        closeEditBtn.getStyleClass().addAll("toolbar-button"); 
        //Add menu enable disable condition
        if(!isWorkingMode){
            pencilToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/pencil.png"));
            pencilToolEditBtn.setDisable(true);
            pencilToolEditBtn.setCursor(Cursor.WAIT);
            eraserToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/eraser.png"));
            eraserToolEditBtn.setDisable(true);
            eraserToolEditBtn.setCursor(Cursor.WAIT);
            lineEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/line.png"));
            lineEditBtn.setDisable(true);
            lineEditBtn.setCursor(Cursor.WAIT);
            arcEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/arc.png"));
            arcEditBtn.setDisable(true);
            arcEditBtn.setCursor(Cursor.WAIT);
            bezierCurveEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/bezier_curve.png"));
            bezierCurveEditBtn.setDisable(true);
            bezierCurveEditBtn.setCursor(Cursor.WAIT);
            ovalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/oval.png"));
            ovalEditBtn.setDisable(true);
            ovalEditBtn.setCursor(Cursor.WAIT);
            rectangleEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/rectangle.png"));
            rectangleEditBtn.setDisable(true);
            rectangleEditBtn.setCursor(Cursor.WAIT);
            triangleRightangleEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/triangle.png"));
            triangleRightangleEditBtn.setDisable(true);
            triangleRightangleEditBtn.setCursor(Cursor.WAIT);
            heartEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/heart.png"));
            heartEditBtn.setDisable(true);
            heartEditBtn.setCursor(Cursor.WAIT);
            diamondEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/diamond.png"));
            diamondEditBtn.setDisable(true);
            diamondEditBtn.setCursor(Cursor.WAIT);
            pentagonEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/pentagon.png"));
            pentagonEditBtn.setDisable(true);
            pentagonEditBtn.setCursor(Cursor.WAIT);
            hexagonEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/hexagon.png"));
            hexagonEditBtn.setDisable(true);
            hexagonEditBtn.setCursor(Cursor.WAIT);
            fourPointStarEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/four_star.png"));
            fourPointStarEditBtn.setDisable(true);
            fourPointStarEditBtn.setCursor(Cursor.WAIT);
            fivePointStarEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/five_star.png"));
            fivePointStarEditBtn.setDisable(true);
            fivePointStarEditBtn.setCursor(Cursor.WAIT);
            sixPointStarEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/six_star.png"));
            sixPointStarEditBtn.setDisable(true);
            sixPointStarEditBtn.setCursor(Cursor.WAIT);
            ploygonEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/rhombus.png"));
            ploygonEditBtn.setDisable(true);
            ploygonEditBtn.setCursor(Cursor.WAIT);zoomInViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
            zoomInViewBtn.setDisable(true);
            zoomInViewBtn.setCursor(Cursor.WAIT);
            normalViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
            normalViewBtn.setDisable(true);
            normalViewBtn.setCursor(Cursor.WAIT);
            zoomOutViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
            zoomOutViewBtn.setDisable(true);
            zoomOutViewBtn.setCursor(Cursor.WAIT); 
            closeEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            closeEditBtn.setDisable(true);
            closeEditBtn.setCursor(Cursor.WAIT);
        }else{
            pencilToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/pencil.png"));
            pencilToolEditBtn.setDisable(false);
            pencilToolEditBtn.setCursor(Cursor.HAND);
            eraserToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/eraser.png"));
            eraserToolEditBtn.setDisable(false);
            eraserToolEditBtn.setCursor(Cursor.HAND);
            lineEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/line.png"));
            lineEditBtn.setDisable(false);
            lineEditBtn.setCursor(Cursor.HAND);
            arcEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/arc.png"));
            arcEditBtn.setDisable(false);
            arcEditBtn.setCursor(Cursor.HAND);
            bezierCurveEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/bezier_curve.png"));
            bezierCurveEditBtn.setDisable(false);
            bezierCurveEditBtn.setCursor(Cursor.HAND);
            ovalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/oval.png"));
            ovalEditBtn.setDisable(false);
            ovalEditBtn.setCursor(Cursor.HAND);
            rectangleEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/rectangle.png"));
            rectangleEditBtn.setDisable(false);
            rectangleEditBtn.setCursor(Cursor.HAND);
            triangleRightangleEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/triangle.png"));
            triangleRightangleEditBtn.setDisable(false);
            triangleRightangleEditBtn.setCursor(Cursor.HAND);
            heartEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/heart.png"));
            heartEditBtn.setDisable(false);
            heartEditBtn.setCursor(Cursor.HAND);
            diamondEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/diamond.png"));
            diamondEditBtn.setDisable(false);
            diamondEditBtn.setCursor(Cursor.HAND);
            pentagonEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/pentagon.png"));
            pentagonEditBtn.setDisable(false);
            pentagonEditBtn.setCursor(Cursor.HAND);
            hexagonEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/hexagon.png"));
            hexagonEditBtn.setDisable(false);
            hexagonEditBtn.setCursor(Cursor.HAND);
            fourPointStarEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/four_star.png"));
            fourPointStarEditBtn.setDisable(false);
            fourPointStarEditBtn.setCursor(Cursor.HAND);
            fivePointStarEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/five_star.png"));
            fivePointStarEditBtn.setDisable(false);
            fivePointStarEditBtn.setCursor(Cursor.HAND);
            sixPointStarEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/six_star.png"));
            sixPointStarEditBtn.setDisable(false);
            sixPointStarEditBtn.setCursor(Cursor.HAND);
            ploygonEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/rhombus.png"));
            ploygonEditBtn.setDisable(false);
            ploygonEditBtn.setCursor(Cursor.HAND);
            zoomInViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
            zoomInViewBtn.setDisable(false);
            zoomInViewBtn.setCursor(Cursor.HAND); 
            normalViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
            normalViewBtn.setDisable(false);
            normalViewBtn.setCursor(Cursor.HAND); 
            zoomOutViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
            zoomOutViewBtn.setDisable(false);
            zoomOutViewBtn.setCursor(Cursor.HAND); 
            closeEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            closeEditBtn.setDisable(false);
            closeEditBtn.setCursor(Cursor.HAND);
        }
        //Add the action to Buttons.
        pencilToolEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                plotEditActionMode = 11; //freehanddesign = 11
            }
        });
        eraserToolEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 12; //eraser = 12
            }
        });
        lineEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 13; //drawline = 13
            }
        });
        bezierCurveEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 13; //drawline = 13
            }
        });
        ovalEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 14; //drawoval = 14
            }
        });
        ellipseEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 14; //drawoval = 14
            }
        });
        rectangleEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 15; //drawrectangle = 15
            }
        });
        rectangleRoundEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 15; //drawrectangle = 15
            }
        });
        triangleRightangleEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 16; //drawtriangle = 16
            }
        });
        triangleEquilateralEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 16; //drawtriangle = 16
            }
        });
        heartEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 17; //drawheart = 17
            }
        });
        diamondEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 18; //drawdiamond = 18
            }
        });
        rhombusEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 18; //drawdiamond = 18
            }
        });
        pentagonEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 19; //drawpentagon = 19
            }
        });
        hexagonEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 20; //drawhexagone = 20
            }
        });
        fourPointStarEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 21; //drawfourpointstar = 21
            }
        });
        fivePointStarEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 22; //drawfivepointstar = 22
            }
        });
        sixPointStarEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 23; //drawsixpointstar = 23
            }
        });               
        zoomInViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                zoomInAction();
            }
        });
        normalViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                normalAction();
            }
        });
        zoomOutViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                zoomOutAction();
            }
        });
        closeEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                closeAction();
            }
        });
        //Create some Buttons.        
        FlowPane flow = new FlowPane();        
        flow.setPrefWrapLength(objConfiguration.WIDTH*.95);
        flow.getStyleClass().addAll("flow");
        //Add the Buttons to the ToolBar.
        //undoEditBtn,redoEditBtn,
        flow.getChildren().addAll(pencilToolEditBtn,eraserToolEditBtn,lineEditBtn,bezierCurveEditBtn,ovalEditBtn,ellipseEditBtn,rectangleEditBtn,rectangleRoundEditBtn,triangleEquilateralEditBtn,triangleRightangleEditBtn,heartEditBtn,diamondEditBtn,rhombusEditBtn,pentagonEditBtn,hexagonEditBtn,fourPointStarEditBtn,fivePointStarEditBtn,sixPointStarEditBtn,zoomInViewBtn,normalViewBtn,zoomOutViewBtn,closeEditBtn);
        flow.setAlignment(Pos.TOP_LEFT);
        toolBar.getItems().addAll(flow);
    }
    
    /**
     * populateViewToolbar
     * <p>
     * Function use for drawing tool bar for menu item View,
     * and binding events for each tools. 
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */    
    private void populateViewToolbarMenu(){
        //For drop down menus
        menuSelected.setTooltip(new Tooltip(objDictionaryAction.getWord("VIEW")));
        
        MenuItem frontSideViewMI = new MenuItem(objDictionaryAction.getWord("FRONTSIDEVIEW"));
        MenuItem rearSideViewMI = new MenuItem(objDictionaryAction.getWord("REARSIDEVIEW"));
        MenuItem frontVisualizationViewMI = new MenuItem(objDictionaryAction.getWord("VISUALIZATIONFRONTVIEW"));
        MenuItem rearVisualizationViewMI = new MenuItem(objDictionaryAction.getWord("VISUALIZATIONREARVIEW"));
        MenuItem flipVisualizationViewMI = new MenuItem(objDictionaryAction.getWord("VISUALIZATIONFLIPVIEW"));
        MenuItem frontCrossSectionViewMI = new MenuItem(objDictionaryAction.getWord("CROSSSECTIONFRONTVIEW"));
        MenuItem rearCrossSectionViewMI = new MenuItem(objDictionaryAction.getWord("CROSSSECTIONREARVIEW"));
        MenuItem gridViewMI = new MenuItem(objDictionaryAction.getWord("GRIDVIEW"));
        MenuItem graphViewMI = new MenuItem(objDictionaryAction.getWord("GRAPHVIEW"));
        MenuItem tilledViewMI = new MenuItem(objDictionaryAction.getWord("TILLEDVIEW"));
        MenuItem simulationViewMI = new MenuItem(objDictionaryAction.getWord("SIMULATION"));
        MenuItem mappingViewMI = new MenuItem(objDictionaryAction.getWord("MAPPING"));
        MenuItem zoomInViewMI = new MenuItem(objDictionaryAction.getWord("ZOOMINVIEW"));
        MenuItem normalViewMI = new MenuItem(objDictionaryAction.getWord("ZOOMNORMALVIEW"));
        MenuItem zoomOutViewMI = new MenuItem(objDictionaryAction.getWord("ZOOMOUTVIEW"));
        
        frontSideViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F1, KeyCombination.CONTROL_DOWN));
        rearSideViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F2, KeyCombination.CONTROL_DOWN));
        frontVisualizationViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F4, KeyCombination.CONTROL_DOWN));
        rearVisualizationViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F5, KeyCombination.CONTROL_DOWN));
        flipVisualizationViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F6, KeyCombination.CONTROL_DOWN));
        frontCrossSectionViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F7, KeyCombination.CONTROL_DOWN));
        rearCrossSectionViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F8, KeyCombination.CONTROL_DOWN));
        gridViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F10, KeyCombination.CONTROL_DOWN));
        graphViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F11, KeyCombination.CONTROL_DOWN)); 
        tilledViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F12, KeyCombination.CONTROL_DOWN));
        simulationViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F4, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        mappingViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F2, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        zoomInViewMI.setAccelerator(new KeyCodeCombination(KeyCode.PLUS, KeyCombination.CONTROL_DOWN));
        normalViewMI.setAccelerator(new KeyCodeCombination(KeyCode.ENTER, KeyCombination.CONTROL_DOWN));
        zoomOutViewMI.setAccelerator(new KeyCodeCombination(KeyCode.MINUS, KeyCombination.CONTROL_DOWN));
        
        frontSideViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_view.png"));
        rearSideViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_view.png"));
        frontVisualizationViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_visualization.png"));
        rearVisualizationViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_visualization.png"));
        flipVisualizationViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/flip_visualization.png"));
        frontCrossSectionViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_cut.png"));
        rearCrossSectionViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_cut.png"));
        gridViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/grid_view.png"));
        graphViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/graph_view.png"));
        tilledViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tiled_view.png"));
        simulationViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/simulation.png"));
        mappingViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/mapping_view.png"));
        zoomInViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
        normalViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
        zoomOutViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
     
        //enable disable conditions
        if(!isWorkingMode){
            frontSideViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/front_view.png"));
            frontSideViewMI.setDisable(true);
            rearSideViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/rear_view.png"));
            rearSideViewMI.setDisable(true);
            frontVisualizationViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/front_visualization.png"));
            frontVisualizationViewMI.setDisable(true);
            rearVisualizationViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/rear_visualization.png"));
            rearVisualizationViewMI.setDisable(true);
            flipVisualizationViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/flip_visualization.png"));
            flipVisualizationViewMI.setDisable(true);
            frontCrossSectionViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/front_cut.png"));
            frontCrossSectionViewMI.setDisable(true);
            rearCrossSectionViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/rear_cut.png"));
            rearCrossSectionViewMI.setDisable(true);
            gridViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/grid_view.png"));
            gridViewMI.setDisable(true);
            graphViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/graph_view.png"));
            graphViewMI.setDisable(true);
            tilledViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/tiled_view.png"));
            tilledViewMI.setDisable(true);
            simulationViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/simulation.png"));
            simulationViewMI.setDisable(true);
            mappingViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/mapping_view.png"));
            mappingViewMI.setDisable(true);
            zoomInViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
            zoomInViewMI.setDisable(true);
            normalViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
            normalViewMI.setDisable(true);
            zoomOutViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
            zoomOutViewMI.setDisable(true);              
        }else{
            frontSideViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_view.png"));
            frontSideViewMI.setDisable(false);
            rearSideViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_view.png"));
            rearSideViewMI.setDisable(false);
            frontVisualizationViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_visualization.png"));
            frontVisualizationViewMI.setDisable(false);
            rearVisualizationViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_visualization.png"));
            rearVisualizationViewMI.setDisable(false);
            flipVisualizationViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/flip_visualization.png"));
            flipVisualizationViewMI.setDisable(false);          
            frontCrossSectionViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_cut.png"));
            frontCrossSectionViewMI.setDisable(false);
            rearCrossSectionViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_cut.png"));
            rearCrossSectionViewMI.setDisable(false);
            gridViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/grid_view.png"));
            gridViewMI.setDisable(false);
            graphViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/graph_view.png"));
            graphViewMI.setDisable(false);
            tilledViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tiled_view.png"));
            tilledViewMI.setDisable(false);
            simulationViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/simulation.png"));
            simulationViewMI.setDisable(false);
            mappingViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/mapping_view.png"));
            mappingViewMI.setDisable(false);
            zoomInViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
            zoomInViewMI.setDisable(false);
            normalViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
            normalViewMI.setDisable(false);
            zoomOutViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
            zoomOutViewMI.setDisable(false);
        }
        //Add the action to Buttons.
        frontSideViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                frontSideViewAction();
            }
        });
        rearSideViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                rearSideViewAction();
            }
        });
        frontVisualizationViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                frontVisualizationViewAction();
            }
        });
        rearVisualizationViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                rearVisualizationViewAction();
            }
        });
        flipVisualizationViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                flipVisualizationViewAction();
            }
        });
        frontCrossSectionViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                frontCutViewAction();
            }
        });
        rearCrossSectionViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                rearCutViewAction();
            }
        });
        gridViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                gridViewAction();
            }
        });
        graphViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                graphViewAction();
            }
        });
        tilledViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                tilledViewAction();
            }
        });
        simulationViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                simulationViewAction();
            }
        }); 
        mappingViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                mappingViewAction();
            }
        });
        zoomInViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                zoomInAction();
            }
        });
        normalViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                normalAction();
            }
        });
        zoomOutViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                zoomOutAction();
            }
        });
        //mappingViewMI
        menuSelected.getItems().addAll(frontSideViewMI,frontVisualizationViewMI,frontCrossSectionViewMI,gridViewMI,graphViewMI,tilledViewMI,simulationViewMI,new SeparatorMenuItem(), zoomInViewMI,normalViewMI,zoomOutViewMI);
    }    
    private void populateViewToolbar(){
        // Front Texture View item;
        Button frontSideViewBtn = new Button();
        frontSideViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_view.png"));
        frontSideViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("FRONTSIDEVIEW")+" (Ctrl+F1)\n"+objDictionaryAction.getWord("TOOLTIPFRONTSIDEVIEW")));
        frontSideViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        frontSideViewBtn.getStyleClass().add("toolbar-button");    
        // Rear Texture View item
        Button rearSideViewBtn = new Button();
        rearSideViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_view.png"));
        rearSideViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("REARSIDEVIEW")+" (Ctrl+F2)\n"+objDictionaryAction.getWord("TOOLTIPREARSIDEVIEW")));
        rearSideViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rearSideViewBtn.getStyleClass().add("toolbar-button");    
        // Front Visulization View item;
        Button frontVisualizationViewBtn = new Button();
        frontVisualizationViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_visualization.png"));
        frontVisualizationViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("VISUALIZATIONFRONTVIEW")+" (Ctrl+F4)\n"+objDictionaryAction.getWord("TOOLTIPVISUALIZATIONFRONTVIEW")));
        frontVisualizationViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        frontVisualizationViewBtn.getStyleClass().add("toolbar-button");    
        // Rear Visaulization View item;
        Button rearVisualizationViewBtn = new Button();
        rearVisualizationViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_visualization.png"));
        rearVisualizationViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("VISUALIZATIONREARVIEW")+" (Ctrl+F5)\n"+objDictionaryAction.getWord("TOOLTIPVISUALIZATIONREARVIEW")));
        rearVisualizationViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rearVisualizationViewBtn.getStyleClass().add("toolbar-button");    
        // Switch Side View item;
        Button flipVisualizationViewBtn = new Button();
        flipVisualizationViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/flip_visualization.png"));
        flipVisualizationViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("VISUALIZATIONFLIPVIEW")+" (Ctrl+F6)\n"+objDictionaryAction.getWord("TOOLTIPVISUALIZATIONFLIPVIEW")));
        flipVisualizationViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        flipVisualizationViewBtn.getStyleClass().add("toolbar-button");    
        // cross Section View item;
        Button frontCrossSectionViewBtn = new Button();
        frontCrossSectionViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_cut.png"));
        frontCrossSectionViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CROSSSECTIONFRONTVIEW")+" (Ctrl+F7)\n"+objDictionaryAction.getWord("TOOLTIPCROSSSECTIONFRONTVIEW")));
        frontCrossSectionViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        frontCrossSectionViewBtn.getStyleClass().add("toolbar-button");    
        // cross Section View item;
        Button rearCrossSectionViewBtn = new Button();
        rearCrossSectionViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_cut.png"));
        rearCrossSectionViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CROSSSECTIONREARVIEW")+" (Ctrl+F8)\n"+objDictionaryAction.getWord("TOOLTIPCROSSSECTIONREARVIEW")));
        rearCrossSectionViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rearCrossSectionViewBtn.getStyleClass().add("toolbar-button");    
        // Grid View item
        Button gridViewBtn = new Button();
        gridViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/grid_view.png"));
        gridViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("GRIDVIEW")+" (Ctrl+F10)\n"+objDictionaryAction.getWord("TOOLTIPGRIDVIEW")));
        gridViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        gridViewBtn.getStyleClass().add("toolbar-button");    
        // Graph View item
        Button graphViewBtn = new Button();
        graphViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/graph_view.png"));
        graphViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("GRAPHVIEW")+" (Ctrl+F11)\n"+objDictionaryAction.getWord("TOOLTIPGRAPHVIEW")));
        graphViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        graphViewBtn.getStyleClass().add("toolbar-button");    
        // Tiled View
        Button tilledViewBtn = new Button();
        tilledViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tiled_view.png"));
        tilledViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("TILLEDVIEW")+" (Ctrl+F12)\n"+objDictionaryAction.getWord("TOOLTIPTILLEDVIEW")));
        tilledViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tilledViewBtn.getStyleClass().add("toolbar-button");
        // Simulation View menu
        Button simulationViewBtn = new Button();
        simulationViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/simulation.png"));
        simulationViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SIMULATION")+" (Ctrl+Shift+F4)\n"+objDictionaryAction.getWord("TOOLTIPSIMULATION")));
        simulationViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        simulationViewBtn.getStyleClass().add("toolbar-button");    
        // mapping View menu
        Button mappingViewBtn = new Button();
        mappingViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/mapping_view.png"));
        mappingViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MAPPING")+" (Ctrl+Shift+F2)\n"+objDictionaryAction.getWord("TOOLTIPMAPPING")));
        mappingViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        mappingViewBtn.getStyleClass().add("toolbar-button");    
        // Zoom-In item
        Button zoomInViewBtn = new Button();
        zoomInViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
        zoomInViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ZOOMINVIEW")+" (Ctrl+Plus)\n"+objDictionaryAction.getWord("TOOLTIPZOOMINVIEW")));
        zoomInViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        zoomInViewBtn.getStyleClass().add("toolbar-button");    
        // Normal item
        Button normalViewBtn = new Button();
        normalViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
        normalViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ZOOMNORMALVIEW")+" (Ctrl+Enter)\n"+objDictionaryAction.getWord("TOOLTIPZOOMNORMALVIEW")));
        normalViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        normalViewBtn.getStyleClass().add("toolbar-button");    
        // Zoom-Out item
        Button zoomOutViewBtn = new Button();
        zoomOutViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
        zoomOutViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ZOOMOUTVIEW")+" (Ctrl+Minus)\n"+objDictionaryAction.getWord("TOOLTIPZOOMOUTVIEW")));
        zoomOutViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        zoomOutViewBtn.getStyleClass().add("toolbar-button");    
        //enable disable conditions
        if(!isWorkingMode){
            frontSideViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/front_view.png"));
            frontSideViewBtn.setDisable(true);
            frontSideViewBtn.setCursor(Cursor.WAIT);
            rearSideViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/rear_view.png"));
            rearSideViewBtn.setDisable(true);
            rearSideViewBtn.setCursor(Cursor.WAIT);
            frontVisualizationViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/front_visualization.png"));
            frontVisualizationViewBtn.setDisable(true);
            frontVisualizationViewBtn.setCursor(Cursor.WAIT);
            rearVisualizationViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/rear_visualization.png"));
            rearVisualizationViewBtn.setDisable(true);
            rearVisualizationViewBtn.setCursor(Cursor.WAIT);
            flipVisualizationViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/flip_visualization.png"));
            flipVisualizationViewBtn.setDisable(true);
            flipVisualizationViewBtn.setCursor(Cursor.WAIT);
            frontCrossSectionViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/front_cut.png"));
            frontCrossSectionViewBtn.setDisable(true);
            frontCrossSectionViewBtn.setCursor(Cursor.WAIT);
            rearCrossSectionViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/rear_cut.png"));
            rearCrossSectionViewBtn.setDisable(true);
            rearCrossSectionViewBtn.setCursor(Cursor.WAIT);
            gridViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/grid_view.png"));
            gridViewBtn.setDisable(true);
            gridViewBtn.setCursor(Cursor.WAIT);
            graphViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/graph_view.png"));
            graphViewBtn.setDisable(true);
            graphViewBtn.setCursor(Cursor.WAIT);
            tilledViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/tiled_view.png"));
            tilledViewBtn.setDisable(true);
            tilledViewBtn.setCursor(Cursor.WAIT);
            simulationViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/simulation.png"));
            simulationViewBtn.setDisable(true);
            simulationViewBtn.setCursor(Cursor.WAIT);
            mappingViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/mapping_view.png"));
            mappingViewBtn.setDisable(true);
            mappingViewBtn.setCursor(Cursor.WAIT);
            zoomInViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
            zoomInViewBtn.setDisable(true);
            zoomInViewBtn.setCursor(Cursor.WAIT);
            normalViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
            normalViewBtn.setDisable(true);
            normalViewBtn.setCursor(Cursor.WAIT);
            zoomOutViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
            zoomOutViewBtn.setDisable(true);
            zoomOutViewBtn.setCursor(Cursor.WAIT);                
        }else{
            frontSideViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_view.png"));
            frontSideViewBtn.setDisable(false);
            frontSideViewBtn.setCursor(Cursor.HAND);
            rearSideViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_view.png"));
            rearSideViewBtn.setDisable(false);
            rearSideViewBtn.setCursor(Cursor.HAND);
            frontVisualizationViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_visualization.png"));
            frontVisualizationViewBtn.setDisable(false);
            frontVisualizationViewBtn.setCursor(Cursor.HAND);
            rearVisualizationViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_visualization.png"));
            rearVisualizationViewBtn.setDisable(false);
            rearVisualizationViewBtn.setCursor(Cursor.HAND);
            flipVisualizationViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/flip_visualization.png"));
            flipVisualizationViewBtn.setDisable(false);
            flipVisualizationViewBtn.setCursor(Cursor.HAND);            
            frontCrossSectionViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_cut.png"));
            frontCrossSectionViewBtn.setDisable(false);
            frontCrossSectionViewBtn.setCursor(Cursor.HAND);
            rearCrossSectionViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_cut.png"));
            rearCrossSectionViewBtn.setDisable(false);
            rearCrossSectionViewBtn.setCursor(Cursor.HAND);
            gridViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/grid_view.png"));
            gridViewBtn.setDisable(false);
            gridViewBtn.setCursor(Cursor.HAND); 
            graphViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/graph_view.png"));
            graphViewBtn.setDisable(false);
            graphViewBtn.setCursor(Cursor.HAND);
            tilledViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tiled_view.png"));
            tilledViewBtn.setDisable(false);
            tilledViewBtn.setCursor(Cursor.HAND); 
            simulationViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/simulation.png"));
            simulationViewBtn.setDisable(false);
            simulationViewBtn.setCursor(Cursor.HAND);
            mappingViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/mapping_view.png"));
            mappingViewBtn.setDisable(false);
            mappingViewBtn.setCursor(Cursor.HAND);
            zoomInViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
            zoomInViewBtn.setDisable(false);
            zoomInViewBtn.setCursor(Cursor.HAND); 
            normalViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
            normalViewBtn.setDisable(false);
            normalViewBtn.setCursor(Cursor.HAND); 
            zoomOutViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
            zoomOutViewBtn.setDisable(false);
            zoomOutViewBtn.setCursor(Cursor.HAND); 
        }
        //Add the action to Buttons.
        frontSideViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                frontSideViewAction();
            }
        });
        rearSideViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rearSideViewAction();
            }
        });
        frontVisualizationViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                frontVisualizationViewAction();
            }
        });
        rearVisualizationViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rearVisualizationViewAction();
            }
        });
        flipVisualizationViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                flipVisualizationViewAction();
            }
        });
        frontCrossSectionViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                frontCutViewAction();
            }
        });
        rearCrossSectionViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rearCutViewAction();
            }
        });
        gridViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                gridViewAction();
            }
        });
        graphViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                graphViewAction();
            }
        });
        tilledViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                tilledViewAction();
            }
        });
        simulationViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                simulationViewAction();
            }
        }); 
        mappingViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mappingViewAction();
            }
        });
        zoomInViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                zoomInAction();
            }
        });
        normalViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                normalAction();
            }
        });
        zoomOutViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                zoomOutAction();
            }
        });
        //Create some Buttons.        
        FlowPane flow = new FlowPane();        
        flow.setPrefWrapLength(objConfiguration.WIDTH*.95);
        flow.getStyleClass().addAll("flow"); 
        //Add the Buttons to the ToolBar.
        //mappingViewBtn
        flow.getChildren().addAll(frontSideViewBtn,frontVisualizationViewBtn,frontCrossSectionViewBtn,gridViewBtn,graphViewBtn,tilledViewBtn,simulationViewBtn,zoomInViewBtn,normalViewBtn,zoomOutViewBtn);
        flow.setAlignment(Pos.TOP_LEFT);
        toolBar.getItems().addAll(flow);        
    }
    /**
     * frontSideViewAction
     * <p>
     * Function use for plotting front View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */    
    private void frontSideViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONFRONTSIDEVIEW"));
        plotViewActionMode = 1;
        isEditingMode = false;
        plotViewAction();
    }
    /**
     * rearSideViewAction
     * <p>
     * Function use for plotting rear View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */    
    private void rearSideViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONREARSIDEVIEW"));
        plotViewActionMode = 2;
        isEditingMode = false;
        plotViewAction();
    }
    /**
     * frontVisualizationViewAction
     * <p>
     * Function use for plotting front visualization View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */
    private void frontVisualizationViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONVISUALIZATIONFRONTVIEW"));
        plotViewActionMode = 4;
        isEditingMode = false;
        plotViewAction();
    }
    /**
     * rearVisualizationViewAction
     * <p>
     * Function use for plotting rear visualization View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */
    private void rearVisualizationViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONVISUALIZATIONREARVIEW"));
        plotViewActionMode = 5;
        isEditingMode = false;
        plotViewAction();
    }
    /**
     * flipVisualizationViewAction
     * <p>
     * Function use for plotting flip visualization View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */
    private void flipVisualizationViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONVISUALIZATIONFLIPVIEW"));
        plotViewActionMode = 6;
        isEditingMode = false;
        plotViewAction();
    }
    /**
     * frontCutViewAction
     * <p>
     * Function use for plotting front cut View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */
    private void frontCutViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCROSSSECTIONFRONTVIEW"));
        plotViewActionMode = 7;
        isEditingMode = false;
        plotViewAction();
    }
    /**
     * rearCutViewAction
     * <p>
     * Function use for plotting rear cut View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */
    private void rearCutViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCROSSSECTIONREARVIEW"));
        plotViewActionMode = 8;
        isEditingMode = false;
        plotViewAction();
    }
    /**
     * gridViewAction
     * <p>
     * Function use for plotting grid View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */
    private void gridViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONGRIDVIEW"));
        plotViewActionMode = 10;
        isEditingMode = false;
        plotViewAction();
    }
    /**
     * graphViewAction
     * <p>
     * Function use for plotting graph View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */
    private void graphViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONGRAPHVIEW"));
        plotViewActionMode = 11;
        isEditingMode = false;
        plotViewAction();
    }
    /**
     * tilledViewAction
     * <p>
     * Function use for plotting tilled View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */
    private void tilledViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONTILLEDVIEW"));
        plotViewActionMode = 12;
        isEditingMode = false;
        plotViewAction();
    }
    /**
     * plotViewAction
     * <p>
     * Function use for controlling different plots for menu item View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */
    private void plotViewAction(){
        //menuHighlight("VIEW");        
        isWorkingMode = true;
        //isEditingMode = false;
        if(isEditingMode && plotEditActionMode==3) {
            plotEditGraph();
        } else if(isEditingMode &&  (plotEditActionMode==9 || plotEditActionMode==11 || plotEditActionMode==12)){
            plotEditImage();
        } else if(isEditingMode &&  (plotEditActionMode==2 || plotEditActionMode==4 || plotEditActionMode==10)){
            plotEditWeave();
        } else{
            if(!isEditingMode){
                if(artworkChildStage!=null){
                    artworkChildStage.close();
                }
                artworkChildStage=null;
                plotEditActionMode = 0;
                setCurrentShape();
            } else if(plotEditActionMode==5){
                populateArtworkColor();
            }
            if(plotViewActionMode==1) //front side
                plotFrontSideView();
            /*else if(plotViewActionMode==2) //rear side*/
            /*else if(plotViewActionMode==3)//flip side */ 
            else if(plotViewActionMode==4) // front visualization
                plotVisualizationFrontView();
            /*else if(plotViewActionMode==5) // rear visualization*/            
            /*else if(plotViewActionMode==6) // flip visualization*/
            else if(plotViewActionMode==7) // front cross scetion
                plotCrossSectionFrontView();
            /*else if(plotViewActionMode==8) // rear cross scetion*/
            /*else if(plotViewActionMode==9) // flip cross scetion*/
            else if(plotViewActionMode==10) //grid
                plotGridView();        
            else if(plotViewActionMode==11) //graph
                plotGraphView();
            else if(plotViewActionMode==12) //tilled
                plotTilledView();           
            else
                plotFrontSideView();
        }
        System.gc();
    }
//////////////////////////// View Menu Action ///////////////////////////
    
    /**
     * plotFrontSideView
     * <p>
     * Function use for plotting composite View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */    
    private void plotFrontSideView(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("GETFRONTSIDEVIEW"));
            plotOrientation(objBufferedImage, (int)(objBufferedImage.getWidth()*zoomfactor), (int)(objBufferedImage.getHeight()*zoomfactor));
            /*            
            BufferedImage bufferedDataImage;
            objCanvas.setWidth(objBufferedImage.getWidth());
            objCanvas.setHeight(objBufferedImage.getHeight());
            try {
                bufferedDataImage = ImageIO.read(new File(objConfiguration.strSource+"/media/ruler.png"));

                Image image = SwingFXUtils.toFXImage(bufferedDataImage,null);
                System.out.println(bufferedDataImage.getWidth()+"=R="+bufferedDataImage.getHeight());
                objGraphicsContext.drawImage(image, 0, 0, bufferedDataImage.getWidth(), bufferedDataImage.getHeight());
            } catch (IOException ex) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            }        
            */          
            //stackPane.getChildren().addAll(artwork,objCanvas);
            //container.setContent(stackPane);
            lblStatus.setText(objDictionaryAction.getWord("GOTFRONTSIDEVIEW"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"plotFrontSideView() : Error while viewing composte view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
        System.gc();
    }
    /**
     * plotVisualizationFrontView
     * <p>
     * Function use for plotting front side View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */    
    private void plotVisualizationFrontView(){
        try{
            lblStatus.setText(objDictionaryAction.getWord("GETVISUALIZATIONFRONTVIEW"));
            int intHeight=(int)objBufferedImage.getHeight();
            int intLength=(int)objBufferedImage.getWidth();
            
            BufferedImage objBufferedImageResize = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objArtworkAction = new ArtworkAction();
            objBufferedImageResize = objArtworkAction.plotVisualizationFrontView(objBufferedImage, intLength, intHeight);
            //intLength = (int)(((objConfiguration.getIntDPI()*intLength)/objConfiguration.getIntEPI())*zoomfactor*3);
            //intHeight = (int)(((objConfiguration.getIntDPI()*intHeight)/objConfiguration.getIntPPI())*zoomfactor*3);
            intLength = (int)(intLength*3*zoomfactor);
            intHeight = (int)(intHeight*3*zoomfactor);
            plotOrientation(objBufferedImageResize, intLength, intHeight);
            objBufferedImageResize = null;
            System.gc();
            lblStatus.setText(objDictionaryAction.getWord("GOTVISUALIZATIONFRONTVIEW"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"plotVisulizationView() : Error while viewing visulization view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } 
    }
    /**
     * plotCrossSectionFrontView
     * <p>
     * Function use for plotting cross section front side View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */    
    private void plotCrossSectionFrontView(){
        try{
            lblStatus.setText(objDictionaryAction.getWord("GETCROSSSECTIONFRONTVIEW"));
            int intHeight=(int)objBufferedImage.getHeight();
            int intLength=(int)objBufferedImage.getWidth();
            
            List lstLines = new ArrayList();
            ArrayList<Integer> lstEntry = null;
            int lineCount = 0;
            for (int i = 0; i < intHeight; i++){
                lstEntry = new ArrayList();
                for (int j = 0; j < intLength; j++){
                    int rgb = objBufferedImage.getRGB(j, i);
                    //add the first color on array
                    if(lstEntry.size()==0)
                        lstEntry.add(rgb);
                    //check for redudancy
                    else {                
                        if(!lstEntry.contains(rgb))
                            lstEntry.add(rgb);
                    }
                }
                lstLines.add(lstEntry);
                lineCount+=lstEntry.size();
            }
            BufferedImage objBufferedImageResize = new BufferedImage(intLength, lineCount, BufferedImage.TYPE_INT_RGB);
            lineCount =0;
            int init = 0;
            for (int i = 0 ; i < intHeight; i++){
                lstEntry = (ArrayList)lstLines.get(i);
                for(int k=0; k<lstEntry.size(); k++, lineCount++){
                    init = (Integer)lstEntry.get(k);
                    for (int j = 0; j < intLength; j++){
                        if(init==objBufferedImage.getRGB(j, i)){
                            objBufferedImageResize.setRGB(j, lineCount, init);
                        }else{
                            objBufferedImageResize.setRGB(j, lineCount, -1);
                        }   
                    }
                }
            }
            
            intHeight = (int)(lineCount*zoomfactor);
            intLength = (int)(intLength*zoomfactor);
            plotOrientation(objBufferedImageResize, intLength, intHeight);            
            objBufferedImageResize = null;
            System.gc();
            lblStatus.setText(objDictionaryAction.getWord("GETCROSSSECTIONFRONTVIEW"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"plotCrossSectionFrontView() : Error while viewing  cross section view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    /**
     * plotGridView
     * <p>
     * Function use for plotting grid View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */    
    private void plotGridView(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("GETGRIDVIEW"));
            BufferedImage objBufferedImageResize = null;
            artwork.setImage(SwingFXUtils.toFXImage(getArtworkGrid(objBufferedImageResize), null));
            container.setContent(artwork);
            objBufferedImageResize = null;
            lblStatus.setText(objDictionaryAction.getWord("GOTGRIDVIEW"));
        } catch(OutOfMemoryError ex){
            zoomOutAction();
            lblStatus.setText(objDictionaryAction.getWord("MAXVALUE"));           
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"plotGridView() : Error while viewing grid view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
        System.gc();
    }
    /**
     * plotGraphView
     * <p>
     * Function use for plotting graph View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */    
    private void plotGraphView(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("GETGRAPHVIEW"));
            BufferedImage objBufferedImageResize = null;
            artwork.setImage(SwingFXUtils.toFXImage(getArtworkGraph(objBufferedImageResize), null));
            container.setContent(artwork);
            objBufferedImageResize = null;
            lblStatus.setText(objDictionaryAction.getWord("GOTGRAPHVIEW"));
        } catch(OutOfMemoryError ex){
            zoomOutAction();
            lblStatus.setText(objDictionaryAction.getWord("MAXVALUE"));           
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"plotGraphView() : Error while viewing dobby garph view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
        System.gc();
    }
    /**
     * plotGraphView
     * <p>
     * Function use for plotting graph View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */    
    private void plotTilledView(){
        try{
            lblStatus.setText(objDictionaryAction.getWord("GETTILLEDVIEW"));
            int intHeight=(int)objBufferedImage.getHeight();
            int intLength=(int)objBufferedImage.getWidth();
            
            BufferedImage objEditBufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objArtworkAction = new ArtworkAction();
            objEditBufferedImage = objArtworkAction.plotTilledView(objBufferedImage, intLength, intHeight);
            
            //intLength = (int)(((objConfiguration.getIntDPI()*intLength)/objConfiguration.getIntEPI())*zoomfactor*3);
            //intHeight = (int)(((objConfiguration.getIntDPI()*intHeight)/objConfiguration.getIntPPI())*zoomfactor*3);
            intLength = (int)(intLength*3*zoomfactor);
            intHeight = (int)(intHeight*3*zoomfactor);
            BufferedImage objBufferedImageresize = new BufferedImage(intLength, intHeight,BufferedImage.TYPE_INT_RGB);
            Graphics2D g = objBufferedImageresize.createGraphics();
            g.drawImage(objEditBufferedImage, 0, 0, intLength, intHeight, null);
            g.dispose();
            
            artwork.setImage(SwingFXUtils.toFXImage(objBufferedImageresize, null));
            container.setContent(artwork);
            objEditBufferedImage = null;
            objBufferedImageresize = null;
            System.gc();
            lblStatus.setText(objDictionaryAction.getWord("GOTTILLEDVIEW"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"plotVisulizationView() : Error while viewing visulization view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } 
    }
     /**
     * simulationViewAction
     * <p>
     * Function use for plotting simulation View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */    
    private void simulationViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONSIMULATION"));
        try {
            SimulatorEditView objBaseSimulationView = new SimulatorEditView(objConfiguration, objBufferedImage);
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }    
    /**
     * mappingViewAction
     * <p>
     * Function use for plotting mapping View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */    
    private void mappingViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONMAPPING"));
        try {
            int intHeight = (int)objArtwork.getObjConfiguration().HEIGHT;
            int intLength = (int)objArtwork.getObjConfiguration().WIDTH;
            BufferedImage objBufferedImageressize = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objArtworkAction = new ArtworkAction();
            objBufferedImageressize = objArtworkAction.designRepeat(objBufferedImage, 1, 1, intHeight/objBufferedImage.getHeight(), intLength/objBufferedImage.getWidth());
            MappingEditView objMappingEditView = new MappingEditView(objConfiguration,objBufferedImageressize);
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);           
            
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    /**
     * zoomInAction
     * <p>
     * Function use for plotting zoom in View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */    
    private void zoomInAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONZOOMINVIEW"));
        //artwork.setScaleX(zoomfactor*=2);
        //artwork.setScaleY(zoomfactor);
        //container.setContent(artwork);
        zoomfactor+=.1; //zoomfactor*=2;
        if(maxSizeCheck(objBufferedImage.getWidth(),objBufferedImage.getHeight())){
            plotViewAction();
        }else{
            zoomfactor-=.1;//zoomfactor/=2;
            lblStatus.setText(objDictionaryAction.getWord("MAXZOOMIN"));
        }
    }
    /**
     * normalAction
     * <p>
     * Function use for plotting zoom normal View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */    
    private void normalAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONZOOMNORMALVIEW"));
        zoomfactor=1;
        plotViewAction();
    }
     /**
     * zoomOutAction
     * <p>
     * Function use for plotting zoom out View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */    
    private void zoomOutAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONZOOMOUTVIEW"));
        //artwork.setScaleX(zoomfactor/=2);
        //artwork.setScaleY(zoomfactor);
        //container.setContent(artwork);
        zoomfactor-=.1;//zoomfactor/=2;        
        if(minSizeCheck()) {         
            plotViewAction();
        }else{
            zoomfactor+=.1; //zoomfactor*=2;
            lblStatus.setText(objDictionaryAction.getWord("MAXZOOMOUT"));
        }        
    }
    /**
     * maxSizeCheck
     * <p>
     * Function use for check artwork size constraints,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */    
    private boolean maxSizeCheck( int intEnds, int intPics){
        String[] data =objArtwork.getObjConfiguration().getStrGraphSize().split("x");
        int nonGraph=(int)(zoomfactor*2*intEnds*intPics*editVerticalRepeatValue*editHorizontalRepeatValue);
        int yesGraph=(int)(zoomfactor*2*intEnds*intPics*Integer.parseInt(data[0])*Integer.parseInt(data[1])/(graphFactor*graphFactor));
        //System.err.println("yesGraph: "+yesGraph);
        return (nonGraph<99999999 && yesGraph<99999999)?true:false;//55574528=95*1024*1024=95MB
    }
    /**
     * minSizeCheck
     * <p>
     * Function use for check artwork size constraints,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */    
    private boolean minSizeCheck(){
        String[] data =objArtwork.getObjConfiguration().getStrGraphSize().split("x");
        int nonGraphWarp=(int)(zoomfactor*objBufferedImage.getWidth()/2);
        int nonGraphWeft=(int)(zoomfactor*objBufferedImage.getHeight()/2);
        int yesGraphWarp=(int)(zoomfactor*objBufferedImage.getWidth()*Integer.parseInt(data[1])/(2*graphFactor));
        int yesGraphWeft=(int)(zoomfactor*objBufferedImage.getHeight()*Integer.parseInt(data[0])/(2*graphFactor));
        //box resized
        int x_inc = (int)Math.round(zoomfactor*Integer.parseInt(data[1])/graphFactor);
        int y_inc = (int)Math.round(zoomfactor*Integer.parseInt(data[0])/graphFactor);
        
        return (nonGraphWarp>0 && nonGraphWeft>0 && x_inc>2 && y_inc>2 && yesGraphWarp>objBufferedImage.getWidth() && yesGraphWeft>objBufferedImage.getHeight())?true:false;
    }
    /**
     * populateUtilityToolbar
     * <p>
     * Function use for drawing tool bar for menu item Utility,
     * and binding events for each tools. 
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */  
    private void populateUtilityToolbarMenu(){  
        //For drop down menus
        menuSelected.setTooltip(new Tooltip(objDictionaryAction.getWord("UTILITY")));
        
        MenuItem weaveUtilityMI = new MenuItem(objDictionaryAction.getWord("WEAVEEDITOR"));
        MenuItem fabricUtilityMI = new MenuItem(objDictionaryAction.getWord("FABRICEDITOR"));
        MenuItem clothUtilityMI = new MenuItem(objDictionaryAction.getWord("CLOTHEDITOR"));
        MenuItem layoutUtilityMI = new MenuItem(objDictionaryAction.getWord("CUSTOMCLOTH"));
        MenuItem deviceUtilityMI = new MenuItem(objDictionaryAction.getWord("PUNCHAPPLICATION"));
        
        weaveUtilityMI.setAccelerator(new KeyCodeCombination(KeyCode.W, KeyCombination.SHIFT_DOWN));
        fabricUtilityMI.setAccelerator(new KeyCodeCombination(KeyCode.J, KeyCombination.SHIFT_DOWN));
        clothUtilityMI.setAccelerator(new KeyCodeCombination(KeyCode.G, KeyCombination.SHIFT_DOWN));
        layoutUtilityMI.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        deviceUtilityMI.setAccelerator(new KeyCodeCombination(KeyCode.N, KeyCombination.SHIFT_DOWN));
        
        weaveUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/weave_editor.png"));
        fabricUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fabric_editor.png"));
        clothUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_editor.png"));
        layoutUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_layout_custom.png"));
        deviceUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/punch_card.png"));
           
        //add enable disable condition
        if(!isWorkingMode){
            weaveUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/weave_editor.png"));
            weaveUtilityMI.setDisable(true);
            fabricUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fabric_editor.png"));
            fabricUtilityMI.setDisable(true);
            clothUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_editor.png"));
            clothUtilityMI.setDisable(true);
            deviceUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/punch_card.png"));
            deviceUtilityMI.setDisable(true);
        }else{
            weaveUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/weave_editor.png"));
            weaveUtilityMI.setDisable(false);
            fabricUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fabric_editor.png"));
            fabricUtilityMI.setDisable(false);
            clothUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_editor.png"));
            clothUtilityMI.setDisable(false);  
            deviceUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/punch_card.png"));
            deviceUtilityMI.setDisable(false);
        }
        //Add the action to Buttons.
        weaveUtilityMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                weaveUtilityAction();
            }
        });
        fabricUtilityMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                fabricUtilityAction();
            }
        });        
        clothUtilityMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                clothUtilityAction();
            }
        });
        layoutUtilityMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                layoutUtilityAction();
            }
        });
        deviceUtilityMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                punchApplicationAction();
            }
        });
        menuSelected.getItems().addAll(weaveUtilityMI, fabricUtilityMI, clothUtilityMI, layoutUtilityMI, deviceUtilityMI);
    }              
    private void populateUtilityToolbar(){  
        // weave editor item
        Button weaveUtilityBtn = new Button();
        weaveUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/weave_editor.png"));
        weaveUtilityBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("WEAVEEDITOR")+" (Shift+W)\n"+objDictionaryAction.getWord("TOOLTIPWEAVEEDITOR")));
        weaveUtilityBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        weaveUtilityBtn.getStyleClass().add("toolbar-button");    
        // fabric editor item
        Button fabricUtilityBtn = new Button();
        fabricUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fabric_editor.png"));
        fabricUtilityBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("FABRICEDITOR")+" (Shift+J)\n"+objDictionaryAction.getWord("TOOLTIPFABRICEDITOR")));
        fabricUtilityBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        fabricUtilityBtn.getStyleClass().add("toolbar-button");    
        // garment viewer File menu
        Button clothUtilityBtn = new Button();
        clothUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_editor.png"));
        clothUtilityBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CLOTHEDITOR")+" (Shift+G)\n"+objDictionaryAction.getWord("TOOLTIPCLOTHEDITOR")));
        clothUtilityBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        clothUtilityBtn.getStyleClass().add("toolbar-button");    
        // garment viewer File menu
        Button layoutUtilityBtn = new Button();
        layoutUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_layout_custom.png"));
        layoutUtilityBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CUSTOMCLOTH")+" (Alt+Shift+C)\n"+objDictionaryAction.getWord("TOOLTIPCUSTOMCLOTH")));
        layoutUtilityBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        layoutUtilityBtn.getStyleClass().add("toolbar-button"); 
        // punch card File menu
        Button deviceUtilityBtn = new Button();
        deviceUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/punch_card.png"));
        deviceUtilityBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("PUNCHAPPLICATION")+" (Shift+N)\n"+objDictionaryAction.getWord("TOOLTIPPUNCHAPPLICATION")));
        deviceUtilityBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        deviceUtilityBtn.getStyleClass().add("toolbar-button"); 
        
        //add enable disable condition
        if(!isWorkingMode){
            weaveUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/weave_editor.png"));
            weaveUtilityBtn.setDisable(true);
            weaveUtilityBtn.setCursor(Cursor.WAIT);
            fabricUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fabric_editor.png"));
            fabricUtilityBtn.setDisable(true);
            fabricUtilityBtn.setCursor(Cursor.WAIT);
            clothUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_editor.png"));
            clothUtilityBtn.setDisable(true);
            clothUtilityBtn.setCursor(Cursor.WAIT);
            deviceUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/punch_card.png"));
            deviceUtilityBtn.setDisable(true);
            deviceUtilityBtn.setCursor(Cursor.WAIT);
        }else{
            weaveUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/weave_editor.png"));
            weaveUtilityBtn.setDisable(false);
            weaveUtilityBtn.setCursor(Cursor.HAND);
            fabricUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fabric_editor.png"));
            fabricUtilityBtn.setDisable(false);
            fabricUtilityBtn.setCursor(Cursor.HAND); 
            clothUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_editor.png"));
            clothUtilityBtn.setDisable(false);  
            clothUtilityBtn.setCursor(Cursor.HAND);
            deviceUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/punch_card.png"));
            deviceUtilityBtn.setDisable(false);
            deviceUtilityBtn.setCursor(Cursor.HAND);
        }
        //Add the action to Buttons.
        weaveUtilityBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                weaveUtilityAction();
            }
        });
        fabricUtilityBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                fabricUtilityAction();
            }
        });        
        clothUtilityBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                clothUtilityAction();
            }
        });
        layoutUtilityBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                layoutUtilityAction();
            }
        });        
        deviceUtilityBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                punchApplicationAction();
            }
        });
         
        //Create some Buttons. 
        FlowPane flow = new FlowPane();        
        flow.setPrefWrapLength(objConfiguration.WIDTH*.95); // preferred width allows for two columns
        flow.getStyleClass().addAll("flow");                
        //Add the Buttons to the ToolBar.
        flow.getChildren().addAll(weaveUtilityBtn,fabricUtilityBtn,clothUtilityBtn,layoutUtilityBtn,deviceUtilityBtn);
        flow.setAlignment(Pos.TOP_LEFT);
        toolBar.getItems().addAll(flow);
    } 
    private void weaveUtilityAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONWEAVEEDITOR"));
        if(objArtwork.getStrArtworkId()!=null){
            objConfiguration.setStrRecentArtwork(objArtwork.getStrArtworkId());
            objConfiguration.strWindowFlowContext="ArtworkEditor";
            artworkStage.close();
            WeaveView objWeaveView = new WeaveView(objConfiguration);
        } else{
            new MessageView("error", objDictionaryAction.getWord("NOVALUE"), objDictionaryAction.getWord("UNSAVEDDATA"));
            lblStatus.setText(objDictionaryAction.getWord("UNSAVEDDATA"));
        }
    }
    /**
     * fabricUtilityAction
     * <p>
     * This method is used for creating GUI of opening artwork in fabric editor.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for creating GUI of opening artwork in fabric editor.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void fabricUtilityAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONFABRICEDITOR"));
        if(objArtwork.getStrArtworkId()!=null){
            final Stage artworkPopupStage = new Stage();
            artworkPopupStage.initStyle(StageStyle.UTILITY);
            //dialogStage.initModality(Modality.WINDOW_MODAL);
            GridPane popup=new GridPane();
            popup.setId("popup");
            popup.setHgap(10);
            popup.setVgap(10);

            Label clothType = new Label(objDictionaryAction.getWord("CLOTHTYPE")+" : ");
            clothType.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCLOTHTYPE")));
            popup.add(clothType, 0, 0);
            final ComboBox clothTypeCB = new ComboBox();
            clothTypeCB.getItems().addAll("Body","Pallu","Border","Cross Border","Blouse","Skirt","Konia");
            clothTypeCB.setValue(objConfiguration.getStrClothType());
            clothTypeCB.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCLOTHTYPE")));
            popup.add(clothTypeCB, 1, 0);
        
            Button btnApply = new Button(objDictionaryAction.getWord("APPLY"));
            btnApply.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/update.png"));
            btnApply.setTooltip(new Tooltip(objDictionaryAction.getWord("ACTIONAPPLY")));
            btnApply.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    try {
                        objConfiguration.setStrClothType(clothTypeCB.getValue().toString());
                        UserAction objUserAction = new UserAction();
                        objUserAction.getConfiguration(objConfiguration);
                        //objConfiguration.clothRepeat();
                        System.gc();
                        objConfiguration.strWindowFlowContext = "ArtworkEditor";
                        objConfiguration.setStrRecentArtwork(objArtwork.getStrArtworkId());
                        artworkStage.close();
                        System.gc();
                        FabricView objFabricView = new FabricView(objConfiguration);
                    } catch (SQLException ex) {
                        new Logging("SEVERE",getClass().getName(),"Cloth type change",ex);
                        lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                    } catch (Exception ex) {
                        new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                        lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                    }  
                    System.gc();
                    artworkPopupStage.close();
                }
            });
            popup.add(btnApply, 0, 1);

            Button btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));
            btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
            btnCancel.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {  
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                    artworkPopupStage.close();
                }
            });
            popup.add(btnCancel, 1, 1);

            Scene popupScene = new Scene(popup, 300, 200, Color.WHITE);
            popupScene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
            artworkPopupStage.setScene(popupScene);
            artworkPopupStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("FABRICSETTINGS"));
            artworkPopupStage.showAndWait();        
        } else{
            new MessageView("error", objDictionaryAction.getWord("NOVALUE"), objDictionaryAction.getWord("UNSAVEDDATA"));
            lblStatus.setText(objDictionaryAction.getWord("UNSAVEDDATA"));
        }
    }
    private void clothUtilityAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCLOTHEDITOR"));
        if(objArtwork.getStrArtworkId()!=null){
            objConfiguration.setStrRecentArtwork(objArtwork.getStrArtworkId());
            objConfiguration.strWindowFlowContext="ArtworkEditor";
            artworkStage.close();
            ClothView objClothView = new ClothView(objConfiguration);
        } else{
            new MessageView("error", objDictionaryAction.getWord("NOVALUE"), objDictionaryAction.getWord("UNSAVEDDATA"));
            lblStatus.setText(objDictionaryAction.getWord("UNSAVEDDATA"));
        }
    }
    private void layoutUtilityAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCUSTOMCLOTH"));
        if(objArtwork.getStrArtworkId()!=null){
            objConfiguration.setStrRecentArtwork(objArtwork.getStrArtworkId());
            objConfiguration.strWindowFlowContext="ArtworkEditor";
            //artworkStage.close();
        }
        ArtworkEditView objArtworkEditView = new ArtworkEditView(objConfiguration);
    }
    /**
     * punchApplicationAction
     * <p>
     * This method is used for creating GUI of punch application.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for creating GUI  of punch application.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void punchApplicationAction(){     
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONPUNCHAPPLICATION"));
            final Stage artworkPopupStage = new Stage();
            artworkPopupStage.initStyle(StageStyle.UTILITY);
            artworkPopupStage.initModality(Modality.APPLICATION_MODAL);
            artworkPopupStage.setResizable(false);
            artworkPopupStage.setIconified(false);
            artworkPopupStage.setFullScreen(false);
            artworkPopupStage.setTitle(objDictionaryAction.getWord("ALERT"));
            BorderPane root = new BorderPane();
            Scene popupScene = new Scene(root, 300, 220, Color.WHITE);
            popupScene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
            final GridPane popup=new GridPane();
            popup.setId("popup");
            popup.setHgap(5);
            popup.setVgap(5);
            popup.setPadding(new Insets(25, 25, 25, 25));
            
            popup.add(new Label(objDictionaryAction.getWord("SELECT")+" "+objDictionaryAction.getWord("PUNCHTYPE")), 0, 0);
            final ComboBox graphCB = new ComboBox();
            graphCB.getItems().addAll("Single page-Single graph","Multi page- Single graph","Multi page- Multi graph","Superimposed Single graph","Superimposed Single Graph (base)","Superimposed Single Graph (skip card)");
            graphCB.setValue("Single page-Single graph");
            popup.add(graphCB, 1, 0);
            
            popup.add(new Label(objDictionaryAction.getWord("SELECT")+" "+objDictionaryAction.getWord("PUNCHAPPLICATION")), 0, 1);
            final ComboBox deviceCB = new ComboBox();
            deviceCB.setValue("Select");
            
            UtilityAction objUtilityAction = new UtilityAction();
            Device objDevice = new Device(null, "Punching M/C", null, null);
            objDevice.setObjConfiguration(objConfiguration);
            Device[] devices = objUtilityAction.getDevices(objDevice);
            deviceCB.getItems().add("Select");
            for(int i=0; i<devices.length; i++)
                deviceCB.getItems().add(devices[i].getStrDeviceName());
            deviceCB.setValue("Select");
            popup.add(deviceCB, 1, 1);
            
            Label artworkBackground = new Label(objDictionaryAction.getWord("BACKGROUNDCOLOR")+":");
            artworkBackground.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPBACKGROUNDCOLOR")));
            popup.add(artworkBackground, 0, 2);
            final ComboBox backgroundCB = new ComboBox();            
            backgroundCB.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPBACKGROUNDCOLOR")));
            for(int i=0; i<intColor; i++){
                String webColor = String.format("#%02X%02X%02X",colors.get(i).getRed(),colors.get(i).getGreen(),colors.get(i).getBlue());
                backgroundCB.getItems().add(webColor);
                if(i==0)
                    backgroundCB.setValue(webColor);
            }
            popup.add(backgroundCB, 1, 2);        
        
            
            Label lblPassword=new Label(objDictionaryAction.getWord("SERVICEPASSWORD"));
            lblPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
            popup.add(lblPassword, 0, 3);
            final PasswordField passPF =new PasswordField();
            passPF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
            passPF.setPromptText(objDictionaryAction.getWord("PROMPTSERVICEPASSWORD"));
            popup.add(passPF, 1, 3);
            final Label lblAlert=new Label("");
            lblAlert.setStyle("-fx-wrap-text:true;");
            lblAlert.setPrefWidth(250);
            popup.add(lblAlert, 0, 5, 2, 1);
            
            Button btnYes = new Button(objDictionaryAction.getWord("RUN"));
            btnYes.setPrefWidth(50);
            btnYes.setId("btnYes");
            btnYes.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    objArtwork.setStrArtworkBackground(backgroundCB.getValue().toString());
                    if(Security.SecurePassword(passPF.getText(), objConfiguration.getObjUser().getStrUsername()).equals(objConfiguration.getObjUser().getStrAppPassword())){
                        try {
                            if(graphCB.getValue().toString().equalsIgnoreCase("Multi page- Single graph")){
                                getMultiPageSingleGraphMC();
                            } else if(graphCB.getValue().toString().equalsIgnoreCase("Multi page- Multi graph")){
                                getMultiPageMultiGraphMC();
                            } else if(graphCB.getValue().toString().equalsIgnoreCase("Superimposed Single graph")){
                                getSuperGraphMC();
                            } else if(graphCB.getValue().toString().equalsIgnoreCase("Superimposed Single Graph (base)")){
                                getSuperBaseGraphMC();
                            } else if(graphCB.getValue().toString().equalsIgnoreCase("Superimposed Single Graph (skip card)")){
                                getSuperSkipGraphMC();
                            } else { //if(graphCB.getValue().toString().equalsIgnoreCase("Single page-Single graph"))
                                getSinglePageSingleGraphMC();
                            }
                            UtilityAction objUtilityAction = new UtilityAction();
                            Device objDevice = new Device(null, null, deviceCB.getValue().toString(), null);
                            objDevice.setObjConfiguration(objConfiguration);
                            objUtilityAction.getDevice(objDevice);
                            Runtime rt = Runtime.getRuntime();
                            if(objArtwork.getObjConfiguration().getObjProcessProcessBuilder()!=null)
                                objArtwork.getObjConfiguration().getObjProcessProcessBuilder().destroy();
                            //objProcess_ProcessBuilder =new ProcessBuilder(objDevice.getDevicePath()).start();
                            //objProcess_ProcessBuilder=new ProcessBuilder(objDevice.getDevicePath(),filePath).start();
                            String exeDir=objDevice.getStrDevicePath().substring(0, objDevice.getStrDevicePath().lastIndexOf("\\")+1);
                            String exeName=objDevice.getStrDevicePath().substring(objDevice.getStrDevicePath().lastIndexOf("\\")+1,objDevice.getStrDevicePath().length());
                            File projDir = new File(exeDir);
                            Runtime.getRuntime().exec(new String[]{"cmd","/c",exeName}, null, projDir);
                        } catch (SQLException ex) {
                            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                        } catch (Exception ex) {
                            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                        }
                        artworkPopupStage.close();
                    }
                    else if(passPF.getText().length()==0){
                        lblAlert.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                    }
                    else{
                        lblAlert.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                    }
                }
            });
            popup.add(btnYes, 0, 4);
            
            Button btnNo = new Button(objDictionaryAction.getWord("CANCEL"));
            btnNo.setPrefWidth(50);
            btnNo.setId("btnNo");
            btnNo.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    artworkPopupStage.close();
                }
            });
            popup.add(btnNo, 1, 4);
            
            root.setCenter(popup);
            artworkPopupStage.setScene(popupScene);
            artworkPopupStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("SELECTTO")+" "+objDictionaryAction.getWord("RUN")+" "+objDictionaryAction.getWord("PUNCHAPPLICATION"));
            artworkPopupStage.showAndWait();
            lblStatus.setText(objDictionaryAction.getWord("ACTIONPUNCHAPPLICATION"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
        System.gc();
    }    
    private void getSinglePageSingleGraphMC(){
        try {
            FileChooser fileChoser=new FileChooser();
            fileChoser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTBMP")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
            FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("BMP (.bmp)", "*.bmp");
            fileChoser.getExtensionFilters().add(extFilterBMP);
            File file=fileChoser.showSaveDialog(artworkStage);
            if(file==null)
                return;
            else
                artworkStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+file.getAbsoluteFile().getName()+"]");
            if (!file.getName().endsWith("bmp"))
                file = new File(file.getPath() +".bmp");
            ImageIO.write(objBufferedImage, "BMP", file);
            
            ArrayList<File> filesToZip=new ArrayList<>();
            filesToZip.add(file);
            String zipFilePath=file.getAbsolutePath()+".zip";
            String passwordToZip = file.getName();
            new EncryptZip(zipFilePath, filesToZip, passwordToZip);
            file.delete();
                
            lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+file.getCanonicalPath());
        } catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }    
    private void getMultiPageSingleGraphMC(){
        try {
            int intHeight=(int)objBufferedImage.getHeight();
            int intLength=(int)objBufferedImage.getWidth();
            
            BufferedImage graphImage = new BufferedImage(intLength*colors.size(), intHeight,BufferedImage.TYPE_INT_RGB);
            for(int k=0; k<colors.size(); k++){
                for(int i = 0; i < intHeight; i++) {
                    for(int j = 0; j < intLength; j++) {
                        if(objBufferedImage.getRGB(j, i)==colors.get(k).getRGB())
                            graphImage.setRGB(j+(k*intLength), i, colors.get(k).getRGB());
                        else
                            graphImage.setRGB(j+(k*intLength), i, 16777215);
                    }
                }
            }
            
            FileChooser fileChoser=new FileChooser();
            fileChoser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTBMP")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
            FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("BMP (.bmp)", "*.bmp");
            fileChoser.getExtensionFilters().add(extFilterBMP);
            File file=fileChoser.showSaveDialog(artworkStage);
            if(file==null)
                return;
            else
                artworkStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+file.getAbsoluteFile().getName()+"]");
            if (!file.getName().endsWith("bmp"))
                file = new File(file.getPath() +".bmp");
            ImageIO.write(graphImage, "BMP", file);
            graphImage = null;
            
            ArrayList<File> filesToZip=new ArrayList<>();
            filesToZip.add(file);
            String zipFilePath=file.getAbsolutePath()+".zip";
            String passwordToZip = file.getName();
            new EncryptZip(zipFilePath, filesToZip, passwordToZip);
            file.delete();

            lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+file.getCanonicalPath());
        } catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }    
    private void getMultiPageMultiGraphMC(){
        try {
            DirectoryChooser directoryChooser=new DirectoryChooser();
            artworkStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTBMP")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
            File selectedDirectory = directoryChooser.showDialog(artworkStage);
            if(selectedDirectory == null)
                return;
            else
                artworkStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+selectedDirectory.getAbsolutePath()+"]");
            
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
            Date date = new Date();
            String currentDate = dateFormat.format(date);
            String path = selectedDirectory.getPath()+"\\Artwork_"+currentDate+"\\";
            File file = new File(path);
            ArrayList<File> filesToZip=new ArrayList<>();
            if (!file.exists()) {
                if (!file.mkdir())
                    path = selectedDirectory.getPath();
            }
            
            int intHeight=(int)objBufferedImage.getHeight();
            int intLength=(int)objBufferedImage.getWidth();
            
            BufferedImage graphImage;
            int rgb = 0;
            for(int k=0; k<colors.size(); k++){
                graphImage = new BufferedImage(intLength, intHeight,BufferedImage.TYPE_INT_RGB);
                for(int i = 0; i < intHeight; i++) {
                    for(int j = 0; j < intLength; j++) {
                        if(objBufferedImage.getRGB(j, i)==colors.get(k).getRGB())
                            graphImage.setRGB(j, i, colors.get(k).getRGB());
                        else
                            graphImage.setRGB(j, i, 16777215);
                    }
                }
                file = new File(path + k +".bmp");
                ImageIO.write(graphImage, "BMP", file);
                filesToZip.add(file);
            }
            String zipFilePath=selectedDirectory.getPath()+"\\Artwork_"+currentDate+".zip";
            String passwordToZip = "Artwork_"+currentDate;
            new EncryptZip(zipFilePath, filesToZip, passwordToZip);
            
            //delete folder recursively
            recursiveDelete(new File(path));
            graphImage = null;
            lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+path);
        } catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }    
    private void getSuperGraphMC(){
        try {
            int intHeight=(int)objBufferedImage.getHeight();
            int intLength=(int)objBufferedImage.getWidth();
            
            List lstLines = new ArrayList();
            ArrayList<Integer> lstEntry = null;
            int lineCount = 0;
            for (int i = 0; i < intHeight; i++){
                lstEntry = new ArrayList();
                for (int j = 0; j < intLength; j++){
                    //add the first color on array
                    if(lstEntry.size()==0)
                        lstEntry.add(objBufferedImage.getRGB(j, i));
                    //check for redudancy
                    else {                
                        if(!lstEntry.contains(objBufferedImage.getRGB(j, i)))
                            lstEntry.add(objBufferedImage.getRGB(j, i));
                    }
                }
                lstLines.add(lstEntry);
                lineCount+=lstEntry.size();
            }
            BufferedImage graphImage = new BufferedImage(intLength, lineCount,BufferedImage.TYPE_INT_RGB);
            
            lineCount=0;
            int rgb = 0;
            for (int i = 0; i < intHeight; i++){
                lstEntry = (ArrayList)lstLines.get(i);
                for (int k = 0; k < lstEntry.size(); k++, lineCount++){
                    rgb = (int)lstEntry.get(k);
                    for (int j = 0; j < intLength; j++){
                        if(rgb==objBufferedImage.getRGB(j, i))
                            graphImage.setRGB(j, lineCount, rgb);
                        else
                            graphImage.setRGB(j, lineCount, 16777215);
                    }
                }
            }
            FileChooser fileChoser=new FileChooser();
            fileChoser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTBMP")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
            FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("BMP (.bmp)", "*.bmp");
            fileChoser.getExtensionFilters().add(extFilterBMP);
            File file=fileChoser.showSaveDialog(artworkStage);
            if(file==null)
                return;
            else
                artworkStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+file.getAbsoluteFile().getName()+"]");
            if (!file.getName().endsWith("bmp"))
                file = new File(file.getPath() +".bmp");
            ImageIO.write(graphImage, "BMP", file);
            graphImage = null;
            
            ArrayList<File> filesToZip=new ArrayList<>();
            filesToZip.add(file);
            String zipFilePath=file.getAbsolutePath()+".zip";
            String passwordToZip = file.getName();
            new EncryptZip(zipFilePath, filesToZip, passwordToZip);
            file.delete();

            lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+file.getCanonicalPath());
        } catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    } 
    private void getSuperBaseGraphMC(){
        try {
            int intHeight=(int)objBufferedImage.getHeight();
            int intLength=(int)objBufferedImage.getWidth();
            
            List lstLines = new ArrayList();
            ArrayList<Integer> lstEntry = null;
            int lineCount = 0;
            
            for (int i = 0; i < intHeight; i++){
                lstEntry = new ArrayList();
                for (int j = 0; j < intLength; j++){
                    // for ashish
                    if(lstEntry.size()==0){
                        lstEntry.add(16777215);          
                    }
                    //check for redudancy
                    else {                
                        if(!lstEntry.contains(objBufferedImage.getRGB(j, i))){
                            lstEntry.add(objBufferedImage.getRGB(j, i));
                        }
                    }
                    // for ashish
                    /*
                    //add the first color on array
                    if(lstEntry.size()==0)
                        lstEntry.add(objBufferedImage.getRGB(j, i));
                    //check for redudancy
                    else {                
                        if(!lstEntry.contains(objBufferedImage.getRGB(j, i)))
                            lstEntry.add(objBufferedImage.getRGB(j, i));
                    }
                    */
                }
                Collections.sort(lstEntry);
                lstLines.add(lstEntry);
                lineCount+=lstEntry.size();
            }
            BufferedImage graphImage = new BufferedImage(intLength, lineCount,BufferedImage.TYPE_INT_RGB);
            
            lineCount=0;
            int rgb = 0;
            for (int i = 0; i < intHeight; i++){
                lstEntry = (ArrayList)lstLines.get(i);
                for (int k = 0; k < lstEntry.size(); k++, lineCount++){
                    rgb = (int)lstEntry.get(k);
                    for (int j = 0; j < intLength; j++){
                        if(rgb==16777215){
                            //System.out.print(" "+((j+i)%2));
                            if((j+i)%2==0)
                                graphImage.setRGB(j, lineCount, 16777215);
                            else
                                graphImage.setRGB(j, lineCount, 0);
                        } else{
                            if(rgb==objBufferedImage.getRGB(j, i))
                                graphImage.setRGB(j, lineCount, rgb);
                            else
                                graphImage.setRGB(j, lineCount, 16777215);
                        }
                    }
                }
                //System.out.println("");
            }
            /*System.out.println("Row"+lineCount);
            for (int i = 0 ; i < lineCount; i++){
                for (int j = 0; j < intLength; j++){
                    System.err.print(" "+fontMatrix[i][j]);
                }
                System.err.println();
            }*/
            
            FileChooser fileChoser=new FileChooser();
            fileChoser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTBMP")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
            FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("BMP (.bmp)", "*.bmp");
            fileChoser.getExtensionFilters().add(extFilterBMP);
            File file=fileChoser.showSaveDialog(artworkStage);
            if(file==null)
                return;
            else
                artworkStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+file.getAbsoluteFile().getName()+"]");
            if (!file.getName().endsWith("bmp"))
                file = new File(file.getPath() +".bmp");
            ImageIO.write(graphImage, "BMP", file);
            graphImage = null;
            
            ArrayList<File> filesToZip=new ArrayList<>();
            filesToZip.add(file);
            String zipFilePath=file.getAbsolutePath()+".zip";
            String passwordToZip = file.getName();
            new EncryptZip(zipFilePath, filesToZip, passwordToZip);
            file.delete();

            lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+file.getCanonicalPath());
        } catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void getSuperSkipGraphMC(){
        try {
            int intHeight=(int)objBufferedImage.getHeight();
            int intLength=(int)objBufferedImage.getWidth();
            
            List lstLines = new ArrayList();
            ArrayList<Integer> lstEntry = null;
            int lineCount = 0;
            
            for (int i = 0; i < intHeight; i++){
                lstEntry = new ArrayList();
                for (int j = 0; j < intLength; j++){
                    // for ashish
                    int rgb = objBufferedImage.getRGB(j, i);
                    int red   = (rgb & 0x00ff0000) >> 16;
                    int green = (rgb & 0x0000ff00) >> 8;
                    int blue  =  rgb & 0x000000ff;
                    String webColor = String.format("#%02X%02X%02X",red,green,blue);
                
                    if(lstEntry.size()==0 && !webColor.equalsIgnoreCase(objArtwork.getStrArtworkBackground())){
                        lstEntry.add(rgb);
                    }
                    //check for redudancy
                    else {                
                        if(!lstEntry.contains(rgb) && !webColor.equalsIgnoreCase(objArtwork.getStrArtworkBackground())){
                            lstEntry.add(rgb);
                        }
                    }
                    // for ashish
                }
                Collections.sort(lstEntry);
                lstLines.add(lstEntry);
                lineCount+=lstEntry.size();
            }
            BufferedImage graphImage = new BufferedImage(intLength, lineCount,BufferedImage.TYPE_INT_RGB);
            
            lineCount=0;
            int rgb = 0;
            for (int i = 0; i < intHeight; i++){
                lstEntry = (ArrayList)lstLines.get(i);
                for (int k = 0; k < lstEntry.size(); k++, lineCount++){
                    rgb = (int)lstEntry.get(k);
                    for (int j = 0; j < intLength; j++){
                        if(rgb==objBufferedImage.getRGB(j, i))
                            graphImage.setRGB(j, lineCount, rgb);
                        else
                            graphImage.setRGB(j, lineCount, 16777215);
                    }
                }
                //System.out.println("");
            }
            /*System.out.println("Row"+lineCount);
            for (int i = 0 ; i < lineCount; i++){
                for (int j = 0; j < intLength; j++){
                    System.err.print(" "+fontMatrix[i][j]);
                }
                System.err.println();
            }*/
            
            FileChooser fileChoser=new FileChooser();
            fileChoser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTBMP")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
            FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("BMP (.bmp)", "*.bmp");
            fileChoser.getExtensionFilters().add(extFilterBMP);
            File file=fileChoser.showSaveDialog(artworkStage);
            if(file==null)
                return;
            else
                artworkStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+file.getAbsoluteFile().getName()+"]");
            if (!file.getName().endsWith("bmp"))
                file = new File(file.getPath() +".bmp");
            ImageIO.write(graphImage, "BMP", file);
            graphImage = null;
            
            ArrayList<File> filesToZip=new ArrayList<>();
            filesToZip.add(file);
            String zipFilePath=file.getAbsolutePath()+".zip";
            String passwordToZip = file.getName();
            new EncryptZip(zipFilePath, filesToZip, passwordToZip);
            file.delete();

            lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+file.getCanonicalPath());
        } catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private static void recursiveDelete(File file) {
        //to end the recursive loop
        if (!file.exists())
            return;
         
        //if directory, go inside and call recursively
        if (file.isDirectory()) {
            for (File f : file.listFiles()) {
                //call recursively
                recursiveDelete(f);
            }
        }
        //call delete to delete files and empty directory
        file.delete();
        //System.out.println("Deleted file/folder: "+file.getAbsolutePath());
    }
}
///----- U --- N ----- U --- S --- E --- D ----- C --- O --- D --- E -----///
/*
private BufferedImage getArtworkGraphold1(BufferedImage objEditBufferedImage){
        lblStatus.setText(objDictionaryAction.getWord("GETGRAPHVIEW"));
        int intHeight=(int)objBufferedImage.getHeight();
        int intLength=(int)objBufferedImage.getWidth();

        String[] data =objArtwork.getObjConfiguration().getStrGraphSize().split("x");
        objEditBufferedImage = new BufferedImage((int)(intLength*zoomfactor*Integer.parseInt(data[1])/10), (int)(intHeight*zoomfactor*Integer.parseInt(data[0])/10),BufferedImage.TYPE_INT_RGB);
        Graphics2D g = objEditBufferedImage.createGraphics();
        g.drawImage(objBufferedImage, 0, 0, (int)(intLength*zoomfactor*Integer.parseInt(data[1])/10), (int)(intHeight*zoomfactor*Integer.parseInt(data[0])/10), null);
        g.setColor(java.awt.Color.DARK_GRAY);
        BasicStroke bs = new BasicStroke(zoomfactor*2/10);
        g.setStroke(bs);
        for(int j = 0, q = 0; j <= intLength; j++, q+=(int)(zoomfactor*Integer.parseInt(data[1])/10)) {
            if((j%Integer.parseInt(data[0]))==0){
                bs = new BasicStroke(zoomfactor*2/10);
                g.setStroke(bs);
            }else{
                bs = new BasicStroke(zoomfactor*1/10);
                g.setStroke(bs);
            }
            g.drawLine(q, 0, q, (int)(intHeight*zoomfactor*Integer.parseInt(data[0])/10));
        }
        for(int i = 0, p=0; i <= intHeight; i++, p+=(int)(zoomfactor*Integer.parseInt(data[0])/10)) {
            if((i%Integer.parseInt(data[1]))==0){
                bs = new BasicStroke(zoomfactor*2/10);
                g.setStroke(bs);
            }else{
                bs = new BasicStroke(zoomfactor*1/10);
                g.setStroke(bs);
            }
            g.drawLine(0, p, (int)(intLength*zoomfactor*Integer.parseInt(data[1])/10), p);
        }*/
        /*for(int i = 0; i < (int)objBufferedImage.getHeight(); i++) {
            for(int j = 0; j < intLength; j++) {
                if((j%Integer.parseInt(data[0]))==0){
                    bs = new BasicStroke(2*(float)zoomfactor/10);
                    g.setStroke(bs);
                }else{
                    bs = new BasicStroke(1*(float)zoomfactor/10);
                    g.setStroke(bs);
                }
                g.drawLine((int)(j*zoomfactor*Integer.parseInt(data[1])/10), 0,  (int)(j*zoomfactor*Integer.parseInt(data[1])/10), (int)((int)objBufferedImage.getHeight()*zoomfactor*Integer.parseInt(data[0])/10));
            }
            if((i%Integer.parseInt(data[1]))==0){
                bs = new BasicStroke(2*(float)zoomfactor/10);
                g.setStroke(bs);
            }else{
                bs = new BasicStroke(1*(float)zoomfactor/10);
                g.setStroke(bs);
            }
            g.drawLine(0, (int)(i*zoomfactor*Integer.parseInt(data[0])/10), (int)(intLength*zoomfactor*Integer.parseInt(data[1])/10), (int)(i*zoomfactor*Integer.parseInt(data[0])/10));
        }*/
       /* g.dispose();
        lblStatus.setText(objDictionaryAction.getWord("GOTGRAPHVIEW"));
        return objEditBufferedImage;
    }
    private BufferedImage getArtworkGraphold2(BufferedImage objEditBufferedImage){
        lblStatus.setText(objDictionaryAction.getWord("GETGRAPHVIEW"));
        int intHeight=(int)objBufferedImage.getHeight();
        int intLength=(int)objBufferedImage.getWidth();

        String[] data =objArtwork.getObjConfiguration().getStrGraphSize().split("x");
        objEditBufferedImage = new BufferedImage((int)(intLength*zoomfactor*Integer.parseInt(data[1])/10), (int)(intHeight*zoomfactor*Integer.parseInt(data[0])/10),BufferedImage.TYPE_INT_RGB);
        Graphics2D g = objEditBufferedImage.createGraphics();
        g.drawImage(objBufferedImage, 0, 0, (int)(intLength*zoomfactor*Integer.parseInt(data[1])/10), (int)(intHeight*zoomfactor*Integer.parseInt(data[0])/10), null);
        g.setColor(java.awt.Color.DARK_GRAY);
        BasicStroke bs = new BasicStroke(zoomfactor*2/10);
        g.setStroke(bs);
        for(int j = 0, q = 0; j <= intLength; j++, q+=(int)(zoomfactor*Integer.parseInt(data[1])/10)) {
            if((j%Integer.parseInt(data[0]))==0){
                bs = new BasicStroke(zoomfactor*2/10);
                g.setStroke(bs);
            }else{
                bs = new BasicStroke(zoomfactor*1/10);
                g.setStroke(bs);
            }
            g.drawLine(q, 0, q, (int)(intHeight*zoomfactor*Integer.parseInt(data[0])/10));
        }
        for(int i = 0, p=0; i <= intHeight; i++, p+=(int)(zoomfactor*Integer.parseInt(data[0])/10)) {
            if((i%Integer.parseInt(data[1]))==0){
                bs = new BasicStroke(zoomfactor*2/10);
                g.setStroke(bs);
            }else{
                bs = new BasicStroke(zoomfactor*1/10);
                g.setStroke(bs);
            }
            g.drawLine(0, p, (int)(intLength*zoomfactor*Integer.parseInt(data[1])/10), p);
        }*/
        /*for(int i = 0; i < (int)objBufferedImage.getHeight(); i++) {
            for(int j = 0; j < intLength; j++) {
                if((j%Integer.parseInt(data[0]))==0){
                    bs = new BasicStroke(2*(float)zoomfactor/10);
                    g.setStroke(bs);
                }else{
                    bs = new BasicStroke(1*(float)zoomfactor/10);
                    g.setStroke(bs);
                }
                g.drawLine((int)(j*zoomfactor*Integer.parseInt(data[1])/10), 0,  (int)(j*zoomfactor*Integer.parseInt(data[1])/10), (int)((int)objBufferedImage.getHeight()*zoomfactor*Integer.parseInt(data[0])/10));
            }
            if((i%Integer.parseInt(data[1]))==0){
                bs = new BasicStroke(2*(float)zoomfactor/10);
                g.setStroke(bs);
            }else{
                bs = new BasicStroke(1*(float)zoomfactor/10);
                g.setStroke(bs);
            }
            g.drawLine(0, (int)(i*zoomfactor*Integer.parseInt(data[0])/10), (int)(intLength*zoomfactor*Integer.parseInt(data[1])/10), (int)(i*zoomfactor*Integer.parseInt(data[0])/10));
        }*/
       /* g.dispose();
        lblStatus.setText(objDictionaryAction.getWord("GOTGRAPHVIEW"));
        return objEditBufferedImage;
    }*/
/**
     * printSetupAction
     * <p>
     * Function use for print menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkViewNew
     */
    /*public void printSetupAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONPRINTSETUPFILE"));
        objPrinterJob.pageDialog(objPrinterAttribute);
        System.gc();
    }*/
    /**
     * printArtwork
     * <p>
     * <p>
     * This method is used for printing artwork
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for creating GUI and running third party application.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
/*    private void printArtwork(){
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
            lblStatus.setText(objDictionaryAction.getWord("ACTIONPRINTTEXTUREFILE"));
            try {            
                if(objPrinterJob.printDialog()){
                    objPrinterJob.setPrintable(new Printable() {
                        @Override
                        public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
                            if (pageIndex != 0) {
                                return NO_SUCH_PAGE;
                            }
                            int intLength = (int)(((objConfiguration.getIntDPI()*objBufferedImage.getWidth())/objConfiguration.getIntEPI())*zoomfactor);
                            int intHeight = (int)(((objConfiguration.getIntDPI()*objBufferedImage.getHeight())/objConfiguration.getIntPPI())*zoomfactor);
                            Graphics2D g2d=(Graphics2D)graphics;
                            g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
                            graphics.drawImage(objBufferedImage, 0, 0, intLength, intHeight, null);
                            return PAGE_EXISTS;
                        }
                    });     
                    objPrinterJob.print();
                }
            } catch (PrinterException ex) {             
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);  
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        }
    }*/
    /**
     * printGrid
     * <p>
     * This method is used for creating GUI and printing artwork.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for creating GUI and printing artwork.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
   /* private void printGrid() {   
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
            lblStatus.setText(objDictionaryAction.getWord("ACTIONPRINTGRIDFILE"));
            try {            
                if(objPrinterJob.printDialog()){
                    objPrinterJob.setPrintable(new Printable() {
                        @Override
                        public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
                            if (pageIndex != 0) {
                                return NO_SUCH_PAGE;
                            }
                            BufferedImage objEditBufferedImage = null;
                            Graphics2D g2d=(Graphics2D)graphics;
                            g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
                            graphics.drawImage(getArtworkGrid(objEditBufferedImage), 0, 0, null);
                            objEditBufferedImage = null;
                            return PAGE_EXISTS;
                        }
                    });     
                    objPrinterJob.print();
                }
            } catch (PrinterException ex) {             
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);  
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        }        
    }*/
    /**
     * printGraph
     * <p>
     * This method is used for creating GUI of print preview.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for creating GUI  of print preview.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
   /* private void printGraph(){
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
            lblStatus.setText(objDictionaryAction.getWord("ACTIONPRINTGRAPHFILE"));
            try {
                if(objPrinterJob.printDialog()){
                    objPrinterJob.setPrintable(new Printable() {
                        @Override
                        public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
                            if (pageIndex != 0) {
                                return NO_SUCH_PAGE;
                            }
                            BufferedImage objEditBufferedImage = null;
                            Graphics2D g2d=(Graphics2D)graphics;
                            g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
                            graphics.drawImage(getArtworkGraph(objEditBufferedImage), 0, 0, (int)pageFormat.getImageableWidth(), (int)pageFormat.getImageableHeight(), null);
                            objEditBufferedImage = null;
                            return PAGE_EXISTS;
                        }
                    });     
                    objPrinterJob.print();
                }
            } catch (PrinterException ex) {             
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);  
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        }
    }*/
    /**
     * printScreen
     * <p>
     * This method is used for creating GUI of print preview.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for creating GUI  of print preview.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    /*public void printScreen() { 
        lblStatus.setText(objDictionaryAction.getWord("ACTIONPRINTSCREENFILE"));
        final Stage artworkPopupStage = new Stage();
        artworkPopupStage.initStyle(StageStyle.UTILITY);
        //dialogStage.initModality(Modality.WINDOW_MODAL);
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setMinSize(objConfiguration.WIDTH/2, objConfiguration.HEIGHT/2);

        Button btnPrint = new Button(objDictionaryAction.getWord("PRINT"));
        btnPrint.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPPRINT")));
        btnPrint.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/print.png"));
        btnPrint.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                new MessageView(objConfiguration);
                if(objConfiguration.getServicePasswordValid()){
                    objConfiguration.setServicePasswordValid(false);
                    //new PrintView(SwingFXUtils.fromFXImage(artwork.getImage(), null));
                    try {
                        if(objPrinterJob.printDialog()){
                            objPrinterJob.setPrintable(new Printable() {
                                @Override
                                public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
                                    if (pageIndex != 0) {
                                        return NO_SUCH_PAGE;
                                    }
                                    /*
                                    //objPrinterJob.getJobSettings().setPageRanges(new PageRange(1, numPages));
                                    JobSettings js = job.getJobSettings();
                                        for (PageRange pr : js.getPageRanges()) {
                                            for (int p = pr.getStartPage(); p <= pr.getEndPage(); p++) {
                                                boolean ok = job.printPage(...code to get your node for the page...);
                                                ...take action on success/failure etc.
                                            }
                                        }
                                    */
                                    /*BufferedImage texture = SwingFXUtils.fromFXImage(artwork.getImage(), null);
                                    //graphics.drawImage(texture, 0, 0, texture.getWidth(), texture.getHeight(), null);
                                    Graphics2D g2d=(Graphics2D)graphics;
                                    g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
                                    graphics.drawImage(texture, 0, 0, (int)pageFormat.getImageableWidth(), (int)pageFormat.getImageableWidth(), null);
                                    return PAGE_EXISTS;                    
                                }
                            });    
                            objPrinterJob.print();
                        }
                        artworkPopupStage.close();
                    } catch (PrinterException ex) {
                        lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                        new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                    } catch (Exception ex) {
                        new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                        lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                    }
                }
            }
        });
        popup.add(btnPrint, 0, 0);
        Button btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));
        btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
        btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        btnCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                artworkPopupStage.close();
           }
        });
        popup.add(btnCancel, 1, 0);
        
        BufferedImage texture = SwingFXUtils.fromFXImage(artwork.getImage(), null);
        ImageView printImage = new ImageView();
        printImage.setImage(SwingFXUtils.toFXImage(texture, null));        
        popup.add(printImage, 0, 1, 2, 1);

        Scene popupScene = new Scene(popup);
        popupScene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        artworkPopupStage.setScene(popupScene);
        artworkPopupStage.setTitle(objDictionaryAction.getWord("PROJECT")+":"+objDictionaryAction.getWord("PRINT")+" "+objDictionaryAction.getWord("PREVIEW"));
        artworkPopupStage.showAndWait();   
    }*/
/*
public void printDesign() {   
        try {            
            if(objPrinterJob.printDialog()){
                objPrinterJob.setPrintable(new Printable() {
                    @Override
                    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
                        if (pageIndex != 0) {
                            return NO_SUCH_PAGE;
                        }                        
                        return PAGE_EXISTS;                    
                    }
                });     
                objPrinterJob.print();
            }
        } catch (PrinterException ex) {             
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);  
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    public void printPreview() {   
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        //dialogStage.initModality(Modality.WINDOW_MODAL);
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setMinSize(objConfiguration.WIDTH/2, objConfiguration.HEIGHT/2);
        
        Button btnPrint = new Button(objDictionaryAction.getWord("PRINT"));
        btnPrint.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPPRINT")));
        btnPrint.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/print.png"));
        btnPrint.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                try {
                    printDesign();
                    dialogStage.close();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        popup.add(btnPrint, 0, 0);
        Button btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));
        btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
        btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        btnCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                dialogStage.close();
           }
        });
        popup.add(btnCancel, 1, 0);
        
        ImageView printImage = new ImageView();
        printImage.setImage(SwingFXUtils.toFXImage(objBufferedImage, null));        
        popup.add(printImage, 0, 1, 2, 1);

        Scene popupScene = new Scene(popup);
        popupScene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        artworkChildStage.setScene(popupScene);
        dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+":"+objDictionaryAction.getWord("TITLE"));
        dialogStage.showAndWait();   
    }
private void resizeArtworkOld(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONRESIZESCALE"));
        if(artworkChildStage!=null){
            artworkChildStage.close();
            artworkChildStage = null;
            System.gc();
        }
        artworkChildStage = new Stage();
        artworkChildStage.initOwner(artworkStage);
        artworkChildStage.initStyle(StageStyle.UTILITY);
        //artworkPopupStage.initModality(Modality.WINDOW_MODAL);
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(10);
        popup.setVgap(10);

        objConfiguration.setStrGraphSize(12+"x"+(int)((12*objConfiguration.getIntPPI())/objConfiguration.getIntEPI()));
            
        popup.add(new Label("Graph Size:"), 0, 0);
        final Label graphSize = new Label(objConfiguration.getStrGraphSize());
        popup.add(graphSize, 1, 0);
        popup.add(new Label("Artwork Size:"), 0, 1);
        popup.add(new Label((int)objBufferedImage.getWidth()+" x "+(int)objBufferedImage.getHeight()), 1, 1);
        popup.add(new Label("Loom Prefrence:"), 0, 2);
        popup.add(new Label(objConfiguration.getIntEnds()+" x "+objConfiguration.getIntPixs()), 1, 2);
        popup.add(new Label(objConfiguration.getStrClothType()), 2, 2);
        
        Label artworkWidth = new Label(objDictionaryAction.getWord("ARTWORKWIDTH")+" ("+objDictionaryAction.getWord("ENDS")+"):");
        popup.add(artworkWidth, 0, 3);
        final TextField txtWidth = new TextField(){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        txtWidth.setText(Integer.toString((int)objBufferedImage.getWidth()));
        popup.add(txtWidth, 1, 3);
        
        Label artworkWidthHooks = new Label(objDictionaryAction.getWord("ARTWORKWIDTH")+" ("+objDictionaryAction.getWord("HOOKS")+"):");
        popup.add(artworkWidthHooks, 0, 4);
        final TextField txtWidthHooks = new TextField();
        txtWidthHooks.setText(Integer.toString((int) Math.ceil(objBufferedImage.getWidth()/objConfiguration.getIntTPD())));
        popup.add(txtWidthHooks, 1, 4);
        
        Label artworkHeight = new Label(objDictionaryAction.getWord("ARTWORKLENGTH")+" ("+objDictionaryAction.getWord("PICKS")+"):");
        popup.add(artworkHeight, 0, 5);
        final TextField txtHeight = new TextField(){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        txtHeight.setText(Integer.toString((int)objBufferedImage.getHeight()));
        popup.add(txtHeight, 1, 5);
        
        popup.add(new Label("Pixels"), 2, 3);
        final ImageView pixelIV = new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png");
        popup.add(pixelIV, 2, 4);
        
        Label artworkWidthMesaure = new Label(objDictionaryAction.getWord("ARTWORKWIDTH")+":");
        popup.add(artworkWidthMesaure, 0, 6);
        
        
        
        final TextField txtWidthMesaure = new TextField();{
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        txtWidthMesaure.setText(Double.toString(objBufferedImage.getWidth()/(float)objConfiguration.getIntEPI()));
        popup.add(txtWidthMesaure, 1, 6);
        
        Label artworkHeightMeasure = new Label(objDictionaryAction.getWord("ARTWORKLENGTH")+":");
        popup.add(artworkHeightMeasure, 0, 7);
        final TextField txtHeightMesaure = new TextField(){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        txtHeightMesaure.setText(Double.toString(objBufferedImage.getHeight()/(float)objConfiguration.getIntPPI())); 
        popup.add(txtHeightMesaure, 1, 7);
        
        popup.add(new Label("Inches"), 2, 6);
        final ImageView linearIV = new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_lock.png");
        popup.add(linearIV, 2, 7);
        
        Label artworkWidthDensity = new Label(objDictionaryAction.getWord("DENSITYWIDTH")+" ("+objDictionaryAction.getWord("EPI")+"):");
        popup.add(artworkWidthDensity, 0, 8);
        final TextField txtWidthDensity = new TextField(){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        txtWidthDensity.setText(Integer.toString(objConfiguration.getIntEPI()));
        popup.add(txtWidthDensity, 1, 8);
        
        Label artworkWidthDensityHooks = new Label(objDictionaryAction.getWord("DENSITYWIDTH")+" ("+objDictionaryAction.getWord("HPI")+"):");
        popup.add(artworkWidthDensityHooks, 0, 9);
        final TextField txtWidthDensityHooks = new TextField();
        txtWidthDensityHooks.setText(Integer.toString(objConfiguration.getIntHPI()));
        popup.add(txtWidthDensityHooks, 1, 9);
        
        Label artworkHeightDensity = new Label(objDictionaryAction.getWord("DENSITYLENGTH")+" ("+objDictionaryAction.getWord("PPI")+"):");
        popup.add(artworkHeightDensity, 0, 10);
        final TextField txtHeightDensity = new TextField(){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        txtHeightDensity.setText(Integer.toString(objConfiguration.getIntPPI()));
        popup.add(txtHeightDensity, 1, 10);
        
        popup.add(new Label("Threads per Inch"), 2, 8);
        final ImageView densityIV = new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png");
        popup.add(densityIV, 2, 9);
        
        txtWidthMesaure.setDisable(true);
        txtHeightMesaure.setDisable(true);
        txtWidthHooks.setDisable(true);
        txtWidthDensityHooks.setDisable(true);
        
        final CheckBox aspectRatioCB = new CheckBox(objDictionaryAction.getWord("ASPECTRATIO"));
        aspectRatioCB.setSelected(false);
        aspectRatioCB.selectedProperty().addListener(new ChangeListener<Boolean>() {
           public void changed(ObservableValue<? extends Boolean> ov, Boolean old_val, Boolean new_val) {
          }
        });
        popup.add(aspectRatioCB, 1, 11);
        
        pixelIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(resizeLockValue==1){
                    resizeLockValue=2; 
                    pixelIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
                    linearIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_lock.png"));
                    densityIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
                    txtWidth.setDisable(false);
                    txtHeight.setDisable(false);
                    txtWidthMesaure.setDisable(true);
                    txtHeightMesaure.setDisable(true);
                    txtWidthDensity.setDisable(false);
                    txtHeightDensity.setDisable(false);
                }else{
                    resizeLockValue=1;
                    pixelIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_lock.png"));
                    linearIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
                    densityIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
                    txtWidth.setDisable(true);
                    txtHeight.setDisable(true);
                    txtWidthMesaure.setDisable(false);
                    txtHeightMesaure.setDisable(false);
                    txtWidthDensity.setDisable(false);
                    txtHeightDensity.setDisable(false);
                }
            }
        });
        
        linearIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(resizeLockValue==2){
                    resizeLockValue=1; 
                    pixelIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_lock.png"));
                    linearIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
                    densityIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
                    txtWidth.setDisable(true);
                    txtHeight.setDisable(true);
                    txtWidthMesaure.setDisable(false);
                    txtHeightMesaure.setDisable(false);
                    txtWidthDensity.setDisable(false);
                    txtHeightDensity.setDisable(false);
                }else{
                    resizeLockValue=2;
                    pixelIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
                    linearIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_lock.png"));
                    densityIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
                    txtWidth.setDisable(false);
                    txtHeight.setDisable(false);
                    txtWidthMesaure.setDisable(true);
                    txtHeightMesaure.setDisable(true);
                    txtWidthDensity.setDisable(false);
                    txtHeightDensity.setDisable(false);
                }
            }
        });
        
        densityIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(resizeLockValue==3){
                    resizeLockValue=2; 
                    pixelIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
                    linearIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_lock.png"));
                    densityIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
                    txtWidth.setDisable(false);
                    txtHeight.setDisable(false);
                    txtWidthMesaure.setDisable(true);
                    txtHeightMesaure.setDisable(true);
                    txtWidthDensity.setDisable(false);
                    txtHeightDensity.setDisable(false);
                }else{
                    resizeLockValue=3;
                    pixelIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
                    linearIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
                    densityIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_lock.png"));
                    txtWidth.setDisable(false);
                    txtHeight.setDisable(false);
                    txtWidthMesaure.setDisable(false);
                    txtHeightMesaure.setDisable(false);
                    txtWidthDensity.setDisable(true);
                    txtHeightDensity.setDisable(true);
                }
            }
        });
        
        txtWidth.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!txtWidth.isFocused())
                    return;
                try {
                    if(t1.trim().equals("") || Integer.parseInt(t1)<0){
                        lblStatus.setText(objDictionaryAction.getWord("BALNKINPUT"));
                        //txtHeight.setText(t1);
                    } else {
                        if(aspectRatioCB.isSelected()){
                            int intHeight = Integer.parseInt(txtWidth.getText())*objBufferedImage.getHeight()/objBufferedImage.getWidth();
                            if(intHeight<1){
                                lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
                                intHeight = 1;
                                txtWidth.setText(Integer.toString(intHeight*objBufferedImage.getWidth()/objBufferedImage.getHeight()));
                            }                            
                            txtHeight.setText(Integer.toString(intHeight));
                        }
                        txtWidthHooks.setText(Integer.toString((int)Math.ceil(Integer.parseInt(txtWidth.getText())/(double)objConfiguration.getIntTPD())));
                        if(resizeLockValue==2){
                            txtWidthDensity.setText(Integer.toString((int)(Integer.parseInt(txtWidth.getText())/Double.parseDouble(txtWidthMesaure.getText()))));
                            txtHeightDensity.setText(Integer.toString((int)(Integer.parseInt(txtHeight.getText())/Double.parseDouble(txtHeightMesaure.getText()))));
                            txtWidthDensityHooks.setText(Integer.toString((int)Math.ceil(Integer.parseInt(txtWidthDensity.getText())/(double)objConfiguration.getIntTPD())));
                            graphSize.setText(12+"x"+(int)((12*Integer.parseInt(txtHeightDensity.getText()))/Integer.parseInt(txtWidthDensity.getText())));
                        }else{
                            txtWidthMesaure.setText(Double.toString((float)Integer.parseInt(txtWidth.getText())/Integer.parseInt(txtWidthDensity.getText())));
                            txtHeightMesaure.setText(Double.toString((float)Integer.parseInt(txtHeight.getText())/Integer.parseInt(txtHeightDensity.getText()))); 
                        }
                    }
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"width property change",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });    
        txtHeight.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!txtHeight.isFocused())
                    return;
                try {
                    if(t1.trim().equals("") || Integer.parseInt(t1)<0){
                        lblStatus.setText(objDictionaryAction.getWord("BALNKINPUT"));
                        //txtWidth.setText(t1);
                    } else {
                        if(aspectRatioCB.isSelected()){
                            int intWidth = Integer.parseInt(txtHeight.getText())*objBufferedImage.getWidth()/objBufferedImage.getHeight();
                            if(intWidth<1){
                                lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
                                intWidth = 1;
                                txtHeight.setText(Integer.toString(intWidth*objBufferedImage.getHeight()/objBufferedImage.getWidth()));
                            }                            
                            txtWidth.setText(Integer.toString(intWidth));
                        }
                        txtWidthHooks.setText(Integer.toString((int)Math.ceil(Integer.parseInt(txtWidth.getText())/(double)objConfiguration.getIntTPD())));
                        if(resizeLockValue==2){
                            txtWidthDensity.setText(Integer.toString((int)(Integer.parseInt(txtWidth.getText())/Double.parseDouble(txtWidthMesaure.getText()))));
                            txtHeightDensity.setText(Integer.toString((int)(Integer.parseInt(txtHeight.getText())/Double.parseDouble(txtHeightMesaure.getText()))));
                            txtWidthDensityHooks.setText(Integer.toString((int)Math.ceil(Integer.parseInt(txtWidthDensity.getText())/(double)objConfiguration.getIntTPD())));
                            graphSize.setText(12+"x"+(int)((12*Integer.parseInt(txtHeightDensity.getText()))/Integer.parseInt(txtWidthDensity.getText())));
                        }else{
                            txtWidthMesaure.setText(Double.toString((float)Integer.parseInt(txtWidth.getText())/Integer.parseInt(txtWidthDensity.getText())));
                            txtHeightMesaure.setText(Double.toString((float)Integer.parseInt(txtHeight.getText())/Integer.parseInt(txtHeightDensity.getText())));
                        }
                    }
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"height property change",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });        

        txtWidthMesaure.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!txtWidthMesaure.isFocused())
                    return;
                try {
                    if(t1.trim().equals("") || Double.parseDouble(t1)<0){
                        lblStatus.setText(objDictionaryAction.getWord("BALNKINPUT"));
                        //txtHeight.setText(t1);
                    } else if(!t1.matches(regExDouble)) {
                        txtWidthMesaure.setText(String.format("%.3f", (objBufferedImage.getWidth()/(float)objConfiguration.getIntEPI())));
                    } else{
                        if(aspectRatioCB.isSelected()){
                            double dblHeight = Double.parseDouble(txtWidthMesaure.getText())*objBufferedImage.getHeight()/objBufferedImage.getWidth();
                            if(dblHeight<0){
                                lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
                                dblHeight = 1;
                                txtWidthMesaure.setText(Double.toString(dblHeight*objBufferedImage.getWidth()/objBufferedImage.getHeight()));
                            }                            
                            txtHeightMesaure.setText(Double.toString(dblHeight));
                        }
                        if(resizeLockValue==1){
                            txtWidthDensity.setText(Integer.toString((int)(Integer.parseInt(txtWidth.getText())/Double.parseDouble(txtWidthMesaure.getText()))));
                            txtHeightDensity.setText(Integer.toString((int)(Integer.parseInt(txtHeight.getText())/Double.parseDouble(txtHeightMesaure.getText()))));
                            txtWidthDensityHooks.setText(Integer.toString((int)Math.ceil(Integer.parseInt(txtWidthDensity.getText())/(double)objConfiguration.getIntTPD())));
                            graphSize.setText(12+"x"+(int)((12*Integer.parseInt(txtHeightDensity.getText()))/Integer.parseInt(txtWidthDensity.getText())));
                        }else{
                            txtWidth.setText(Integer.toString((int)(Double.parseDouble(txtWidthMesaure.getText())*Integer.parseInt(txtWidthDensity.getText()))));
                            txtHeight.setText(Integer.toString((int)(Double.parseDouble(txtHeightMesaure.getText())*Integer.parseInt(txtHeightDensity.getText()))));
                            txtWidthHooks.setText(Integer.toString((int)Math.ceil(Integer.parseInt(txtWidth.getText())/(double)objConfiguration.getIntTPD())));
                        }
                    }
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"width property change",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        txtHeightMesaure.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!txtHeightMesaure.isFocused())
                    return;
                try {
                    if(t1.trim().equals("") || Double.parseDouble(t1)<0){
                        lblStatus.setText(objDictionaryAction.getWord("BALNKINPUT"));
                        //txtWidth.setText(t1);
                    } else if(!t1.matches(regExDouble)) {
                        txtHeightMesaure.setText(String.format("%.3f", (objBufferedImage.getHeight()/(float)objConfiguration.getIntPPI()))); 
                    } else {
                        if(aspectRatioCB.isSelected()){
                            double dblWidth = Double.parseDouble(txtHeightMesaure.getText())*objBufferedImage.getWidth()/objBufferedImage.getHeight();
                            if(dblWidth<0){
                                lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
                                dblWidth = 1;
                                txtHeightMesaure.setText(Double.toString(dblWidth*objBufferedImage.getHeight()/objBufferedImage.getWidth()));
                            }                            
                            txtWidthMesaure.setText(Double.toString(dblWidth));
                        }
                        if(resizeLockValue==1){
                            txtWidthDensity.setText(Integer.toString((int)(Integer.parseInt(txtWidth.getText())/Double.parseDouble(txtWidthMesaure.getText()))));
                            txtHeightDensity.setText(Integer.toString((int)(Integer.parseInt(txtHeight.getText())/Double.parseDouble(txtHeightMesaure.getText()))));
                            txtWidthDensityHooks.setText(Integer.toString((int)Math.ceil(Integer.parseInt(txtWidthDensity.getText())/(double)objConfiguration.getIntTPD())));
                            graphSize.setText(12+"x"+(int)((12*Integer.parseInt(txtHeightDensity.getText()))/Integer.parseInt(txtWidthDensity.getText())));
                        }else{
                            txtWidth.setText(Integer.toString((int)(Double.parseDouble(txtWidthMesaure.getText())*Integer.parseInt(txtWidthDensity.getText()))));
                            txtHeight.setText(Integer.toString((int)(Double.parseDouble(txtHeightMesaure.getText())*Integer.parseInt(txtHeightDensity.getText()))));
                            txtWidthHooks.setText(Integer.toString((int)Math.ceil(Integer.parseInt(txtWidth.getText())/(double)objConfiguration.getIntTPD())));
                        }
                    }
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"height property change",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });        
        
        txtWidthDensity.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!txtWidthDensity.isFocused())
                    return;
                try {
                    if(t1.trim().equals("") || Integer.parseInt(t1)<0){
                        lblStatus.setText(objDictionaryAction.getWord("BALNKINPUT"));
                        //txtHeight.setText(t1);
                    } else {
                        if(aspectRatioCB.isSelected()){
                            int intHeight = Integer.parseInt(txtWidthDensity.getText())*objBufferedImage.getHeight()/objBufferedImage.getWidth();
                            if(intHeight<1){
                                lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
                                intHeight = 1;
                                txtWidthDensity.setText(Integer.toString(intHeight*objBufferedImage.getWidth()/objBufferedImage.getHeight()));
                            }                            
                            txtHeightDensity.setText(Integer.toString(intHeight));
                        }
                        txtWidthDensityHooks.setText(Integer.toString((int)Math.ceil(Integer.parseInt(txtWidthDensity.getText())/(double)objConfiguration.getIntTPD())));
                        graphSize.setText(12+"x"+(int)((12*Integer.parseInt(txtHeightDensity.getText()))/Integer.parseInt(txtWidthDensity.getText())));
                        if(resizeLockValue==1){
                            txtWidthMesaure.setText(Double.toString((float)Integer.parseInt(txtWidth.getText())/Integer.parseInt(txtWidthDensity.getText())));
                            txtHeightMesaure.setText(Double.toString((float)Integer.parseInt(txtHeight.getText())/Integer.parseInt(txtHeightDensity.getText()))); 
                        }else{
                            txtWidth.setText(Integer.toString((int)(Double.parseDouble(txtWidthMesaure.getText())*Integer.parseInt(txtWidthDensity.getText()))));
                            txtHeight.setText(Integer.toString((int)(Double.parseDouble(txtHeightMesaure.getText())*Integer.parseInt(txtHeightDensity.getText()))));
                            txtWidthHooks.setText(Integer.toString((int)Math.ceil(Integer.parseInt(txtWidth.getText())/(double)objConfiguration.getIntTPD())));
                        }
                    }
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"width property change",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });    
        txtHeightDensity.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!txtHeightDensity.isFocused())
                    return;
                try {
                    if(t1.equals("") && Integer.parseInt(t1)<0){
                        lblStatus.setText(objDictionaryAction.getWord("BALNKINPUT"));
                        //txtWidth.setText(t1);
                    } else {
                        if(aspectRatioCB.isSelected()){
                            int intWidth = Integer.parseInt(txtHeightDensity.getText())*objBufferedImage.getWidth()/objBufferedImage.getHeight();
                            if(intWidth<1){
                                lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
                                intWidth = 1;
                                txtHeightDensity.setText(Integer.toString(intWidth*objBufferedImage.getHeight()/objBufferedImage.getWidth()));
                            }                            
                            txtWidthDensity.setText(Integer.toString(intWidth));
                        }
                        txtWidthDensityHooks.setText(Integer.toString((int)Math.ceil(Integer.parseInt(txtWidthDensity.getText())/(double)objConfiguration.getIntTPD())));
                        graphSize.setText(12+"x"+(int)((12*Integer.parseInt(txtHeightDensity.getText()))/Integer.parseInt(txtWidthDensity.getText())));
                        if(resizeLockValue==1){
                            txtWidthMesaure.setText(Double.toString((float)Integer.parseInt(txtWidth.getText())/Integer.parseInt(txtWidthDensity.getText())));
                            txtHeightMesaure.setText(Double.toString((float)Integer.parseInt(txtHeight.getText())/Integer.parseInt(txtHeightDensity.getText()))); 
                        }else{
                            txtWidth.setText(Integer.toString((int)(Double.parseDouble(txtWidthMesaure.getText())*Integer.parseInt(txtWidthDensity.getText()))));
                            txtHeight.setText(Integer.toString((int)(Double.parseDouble(txtHeightMesaure.getText())*Integer.parseInt(txtHeightDensity.getText()))));
                            txtWidthHooks.setText(Integer.toString((int)Math.ceil(Integer.parseInt(txtWidth.getText())/(double)objConfiguration.getIntTPD())));
                        }
                    }
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"height property change",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });        
        
        Button btnApply = new Button(objDictionaryAction.getWord("APPLY"));
        btnApply.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/update.png"));
        btnApply.setTooltip(new Tooltip(objDictionaryAction.getWord("ACTIONAPPLY")));
        //btnApply.setMaxWidth(Double.MAX_VALUE);
        btnApply.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                objUR.doCommand("Apply", objBufferedImage);
                //objURF.store(objBufferedImage);
                //objArtworkAction = new ArtworkAction();
                //objBufferedImage = objArtworkAction.getImageFromMatrix(artworkMatrix, colors);
                int width = Integer.parseInt(txtWidth.getText());
                int height = Integer.parseInt(txtHeight.getText());
                if(maxSizeCheck(width,height)){
                    //objBufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
                    BufferedImage objBufferedImageesize = new BufferedImage(width, height,BufferedImage.TYPE_INT_RGB);
                    Graphics2D g = objBufferedImageesize.createGraphics();
                    g.drawImage(objBufferedImage, 0, 0, width, height, null);
                    g.dispose();
                    objBufferedImage = objBufferedImageesize;
                    objBufferedImageesize = null;
                    initArtworkValue();
                    objConfiguration.setIntPPI(Integer.parseInt(txtHeightDensity.getText()));
                    objConfiguration.setIntEPI(Integer.parseInt(txtWidthDensity.getText()));
                    objConfiguration.setIntHPI(Integer.parseInt(txtWidthDensityHooks.getText()));
                    objConfiguration.setStrGraphSize(12+"x"+(int)((12*objConfiguration.getIntPPI())/objConfiguration.getIntEPI()));
                    lblStatus.setText(objDictionaryAction.getWord("SUCCESS"));
                    artworkChildStage.close();
                }else{
                    lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
                }
                System.gc();
            }
        });
        popup.add(btnApply, 0, 12);

        Button btnReset = new Button(objDictionaryAction.getWord("RESET"));
        btnReset.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/clear.png"));
        btnReset.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPRESET")));
        //btnReset.setMaxWidth(Double.MAX_VALUE);
        btnReset.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                lblStatus.setText(objDictionaryAction.getWord("ACTIONRESET"));
                txtWidth.setText(Integer.toString(objBufferedImage.getWidth()));
                txtHeight.setText(Integer.toString(objBufferedImage.getHeight()));
                txtWidthHooks.setText(Integer.toString((int)Math.ceil(objBufferedImage.getWidth()/(double)objConfiguration.getIntTPD())));
                txtWidthDensity.setText(Integer.toString(objConfiguration.getIntEPI()));
                txtHeightDensity.setText(Integer.toString(objConfiguration.getIntPPI()));
                txtWidthDensityHooks.setText(Integer.toString(objConfiguration.getIntHPI()));
                txtWidthMesaure.setText(Double.toString((double)Integer.parseInt(txtWidth.getText())/Integer.parseInt(txtWidthDensity.getText())));
                txtHeightMesaure.setText(Double.toString((double)Integer.parseInt(txtHeight.getText())/Integer.parseInt(txtHeightDensity.getText()))); 
                graphSize.setText(12+"x"+(int)((12*objConfiguration.getIntPPI())/objConfiguration.getIntEPI()));
            }
        });
        popup.add(btnReset, 1, 12);

        Button btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));
        btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
        btnCancel.setMaxWidth(Double.MAX_VALUE);
        btnCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                artworkChildStage.close();
            }
        });
        popup.add(btnCancel, 2, 12);

        Scene popupScene = new Scene(popup);
        popupScene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        artworkChildStage.setScene(popupScene);
        artworkChildStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("WINDOWRESIZEARTWORK"));
        artworkChildStage.showAndWait();        
    }   

private void artworkWeaveEditEvent(MouseEvent event){
        if(event.isSecondaryButtonDown() || (event.getButton() == MouseButton.SECONDARY)){
            objBufferedImage.setRGB((int)(event.getX()/zoomfactor), (int)(event.getY()/zoomfactor), editThreadSecondaryValue);
            objGraphics2D.setColor(new java.awt.Color(editThreadSecondaryValue));
        } else{
            objBufferedImage.setRGB((int)(event.getX()/zoomfactor), (int)(event.getY()/zoomfactor), editThreadPrimaryValue);
            objGraphics2D.setColor(new java.awt.Color(editThreadPrimaryValue));
        }
        objGraphics2D.fillRect((int)((int)(event.getX()/zoomfactor)*zoomfactor), (int)((int)(event.getY()/zoomfactor)*zoomfactor), (int)(Math.ceil(zoomfactor)), (int)(Math.ceil(zoomfactor)));
        artwork.setImage(SwingFXUtils.toFXImage(objEditBufferedImage, null));
        container.setContent(artwork);  
    } 
    private void populateEditWeavePane(){
         if(artworkChildStage!=null){
            artworkChildStage.close();
            artworkChildStage = null;
            System.gc();
        }
        artworkChildStage = new Stage();
        artworkChildStage.initOwner(artworkStage);
        artworkChildStage.initStyle(StageStyle.UTILITY);
        //dialogStage.initModality(Modality.WINDOW_MODAL);
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setAlignment(Pos.CENTER);
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        
        popup.add(new Label(objDictionaryAction.getWord("EDITTHREADTYPE")+" : "), 0, 0, 2, 1);
        GridPane editThreadCB = new GridPane();
        editThreadCB.setAlignment(Pos.CENTER);
        editThreadCB.setHgap(5);
        editThreadCB.setVgap(5);
        editThreadCB.setCursor(Cursor.HAND);
        //editThreadCB.getChildren().clear();
        for(int i=0; i<intColor; i++){
            String webColor = String.format("#%02X%02X%02X",colors.get(i).getRed(),colors.get(i).getGreen(),colors.get(i).getBlue());
            final Label  lblColor  = new Label ();
            lblColor.setPrefSize(33, 33);
            lblColor.setStyle("-fx-background-color:"+webColor+"; -fx-border-color: #FFFFFF;");
            lblColor.setTooltip(new Tooltip("Color:"+i+"\n"+webColor+"\nR:"+colors.get(i).getRed()+"\nG:"+colors.get(i).getGreen()+"\nB:"+colors.get(i).getBlue()));
            lblColor.setUserData(colors.get(i).getRGB());
            lblColor.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if(event.isSecondaryButtonDown() || (event.getButton() == MouseButton.SECONDARY)){
                        editThreadSecondaryValue=Integer.parseInt(lblColor.getUserData().toString());
                    } else{
                        editThreadPrimaryValue=Integer.parseInt(lblColor.getUserData().toString());
                    }
                }
            });
            editThreadCB.add(lblColor, i%6, i/6);
        }
        editThreadPrimaryValue=colors.get(0).getRGB();
        editThreadSecondaryValue=colors.get(0).getRGB();
        popup.add(editThreadCB, 0, 1, 2, 1);
                
        popup.add(new Label(objDictionaryAction.getWord("EDITTHREADTOOL")+" : "), 0, 2);
        
        GridPane editToolPane = new GridPane();
        editToolPane.setVgap(0);
        editToolPane.setHgap(0);
                
        Button mirrorVerticalTP = new Button();
        Button mirrorHorizontalTP = new Button();
        Button rotateTP = new Button();
        Button rotateAntiTP = new Button();
        Button moveRightTP = new Button();
        Button moveLeftTP = new Button();
        Button moveUpTP = new Button();
        Button moveDownTP = new Button();
        Button moveRight8TP = new Button();
        Button moveLeft8TP = new Button();
        Button moveUp8TP = new Button();
        Button moveDown8TP = new Button();
        Button tiltRightTP = new Button();
        Button tiltLeftTP = new Button();
        Button tiltUpTP = new Button();
        Button tiltDownTP = new Button();
                
        mirrorVerticalTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
        mirrorHorizontalTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
        rotateTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_90.png"));
        rotateAntiTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_anti_90.png"));
        moveRightTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right.png"));
        moveLeftTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left.png"));
        moveUpTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up.png"));
        moveDownTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down.png"));
        moveRight8TP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right_by_8.png"));
        moveLeft8TP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left_by_8.png"));
        moveUp8TP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up_by_8.png"));
        moveDown8TP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down_by_8.png"));
        tiltRightTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_right.png"));
        tiltLeftTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_left.png"));
        tiltUpTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_up.png"));
        tiltDownTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_down.png"));
        
        mirrorVerticalTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPVERTICALMIRROR")));
        mirrorHorizontalTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPHORIZENTALMIRROR")));
        rotateTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCLOCKROTATION")));
        rotateAntiTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPANTICLOCKROTATION")));
        moveRightTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPMOVERIGHT")));
        moveLeftTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPMOVELEFT")));
        moveUpTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPMOVEUP")));
        moveDownTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPMOVEDOWN")));
        moveRight8TP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPMOVERIGHT8")));
        moveLeft8TP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPMOVELEFT8")));
        moveUp8TP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPMOVEUP8")));
        moveDown8TP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPMOVEDOWN8")));
        tiltRightTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPTILTRIGHT")));
        tiltLeftTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPTILTLEFT")));
        tiltUpTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPTILTUP")));
        tiltDownTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPTILTDOWN")));
        
        mirrorVerticalTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        mirrorHorizontalTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rotateTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rotateAntiTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveRightTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveLeftTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveUpTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveDownTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveRight8TP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveLeft8TP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveUp8TP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveDown8TP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tiltRightTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tiltLeftTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tiltUpTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tiltDownTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);                    
        
        editToolPane.add(mirrorVerticalTP, 0, 0);
        editToolPane.add(mirrorHorizontalTP, 1, 0);
        editToolPane.add(rotateTP, 2, 0);
        editToolPane.add(rotateAntiTP, 3, 0);        
        editToolPane.add(moveRightTP, 0, 1);
        editToolPane.add(moveLeftTP, 1, 1);
        editToolPane.add(moveUpTP, 2, 1);
        editToolPane.add(moveDownTP, 3, 1);        
        editToolPane.add(moveRight8TP, 0, 2);
        editToolPane.add(moveLeft8TP, 1, 2);
        editToolPane.add(moveUp8TP, 2, 2);
        editToolPane.add(moveDown8TP, 3, 2);        
        editToolPane.add(tiltRightTP, 0, 3);
        editToolPane.add(tiltLeftTP, 1, 3);
        editToolPane.add(tiltUpTP, 2, 3);
        editToolPane.add(tiltDownTP, 3, 3);        
                
        mirrorVerticalTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {                       
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONVERTICALMIRROR"));
                    objUR.doCommand("VMirror", objBufferedImage);
                    //objURF.store(objBufferedImage);
                    objArtworkAction = new ArtworkAction();
                    objBufferedImage = objArtworkAction.getImageVerticalMirror(objBufferedImage);
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Mirror Vertical",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });  
        mirrorHorizontalTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONHORIZENTALMIRROR"));
                    objUR.doCommand("HMirror", objBufferedImage);
                    //objURF.store(objBufferedImage);
                    objArtworkAction = new ArtworkAction();
                    objBufferedImage = objArtworkAction.getImageHorizontalMirror(objBufferedImage);
                    plotEditWeave();
                } catch (Exception ex) {
                  new Logging("SEVERE",getClass().getName(),"Mirror Vertical",ex);
                  lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        }); 
        rotateTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONCLOCKROTATION"));
                    objUR.doCommand("CRotate", objBufferedImage);
                    //objURF.store(objBufferedImage);
                    objArtworkAction = new ArtworkAction();
                    objBufferedImage = objArtworkAction.getImageRotation(objBufferedImage,"CLOCKWISE");
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Rotate Clock Wise ",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        }); 
        rotateAntiTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONANTICLOCKROTATION"));
                    objUR.doCommand("Counter Clockwise Rotation", objBufferedImage);
                    //objURF.store(objBufferedImage);
                    objArtworkAction = new ArtworkAction();
                    objBufferedImage = objArtworkAction.getImageRotation(objBufferedImage,"COUNTERCLOCKWISE");
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Anti Rotate Clock Wise ",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        moveRightTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVERIGHT"));
                    //objArtworkAction.moveRight(objWeave);/
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Move Right",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        moveLeftTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVELEFT"));
                    //objArtworkAction.moveLeft(objWeave);
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Move Left",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        moveUpTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVEUP"));
                    //objArtworkAction.moveUp(objWeave);
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Move Up",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        moveDownTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVEDOWN"));
                    //objArtworkAction.moveDown(objWeaveR);
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Move Down",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
         moveRight8TP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVERIGHT8"));
                    //objArtworkAction.moveRight8(objWeaveR);
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Move Right",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        moveLeft8TP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVELEFT8"));
                    //objArtworkAction.moveLeft8(objWeaveR);
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Move Left",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        moveUp8TP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVEUP8"));
                    //objArtworkAction.moveUp8(objWeaveR);
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Move Up",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        moveDown8TP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVEDOWN8"));
                    //objArtworkAction.moveDown8(objWeaveR);
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Move Down",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        tiltRightTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONTILTRIGHT"));
                    //objArtworkAction.tiltRight(objWeaveR);
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Tilt Right ",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        tiltLeftTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONTILTLEFT"));
                    //objArtworkAction.tiltLeft(objWeaveR);
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Tilt Left ",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        tiltUpTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONTILTUP"));
                    //objArtworkAction.tiltUp(objWeaveR);
                    plotEditWeave();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Tilt Up ",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        tiltDownTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONTILTDOWN"));
                    //objArtworkAction.tiltDown(objWeaveR);
                    plotEditWeave();
                } catch (Exception ex) {
                   new Logging("SEVERE",getClass().getName(),"Tilt Down ",ex);
                   lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        popup.add(editToolPane, 0, 3);
                
        Button btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));
        btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
        btnCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                artworkChildStage.close();
                plotViewActionMode = 1;
                plotViewAction();
            }
        });
        popup.add(btnCancel, 0, 4);

        artworkChildStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                artworkChildStage.close();
                plotViewActionMode = 1;
                plotViewAction();
            }
        });
        Scene popupScene = new Scene(popup);
        popupScene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        artworkChildStage.setScene(popupScene);
        artworkChildStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("WINDOWEDITWEAVE"));
        artworkChildStage.showAndWait();   
        System.gc();
    }
//////////////////////////// Toolkit Menu Action ///////////////////////////
    private void editArtworkToolKit(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONGRAPHPATTERNEDIT"));
            ArtworkAction objFabricAction = new ArtworkAction();
            setCurrentShape();
            isEditingMode = true;
            plotEditActionMode = 11; //freehanddrawing = 4
            plotEditWeave();
            populateEditToolKitPane();
            lblStatus.setText(objDictionaryAction.getWord("GOTGRAPHVIEW"));
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"editGraph() : Error while editing garph view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void populateEditToolKitPane(){
        if(artworkChildStage!=null){
            artworkChildStage.close();
            artworkChildStage = null;
            System.gc();
        }
        artworkChildStage = new Stage();
        artworkChildStage.initOwner(artworkStage);
        artworkChildStage.initStyle(StageStyle.UTILITY);
        //dialogStage.initModality(Modality.WINDOW_MODAL);
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setAlignment(Pos.CENTER);
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
                
        popup.add(new Label(objDictionaryAction.getWord("TOOLKIT")+" : "), 0, 0, 2, 1);
               
        GridPane toolkitEditGP=new GridPane();
        toolkitEditGP.setAlignment(Pos.CENTER);
        toolkitEditGP.setHgap(0);
        toolkitEditGP.setVgap(0);
        
        // select edit item
        Button selectEditBtn = new Button(); 
        selectEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/select.png"));
        selectEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SELECT")+"\n"+objDictionaryAction.getWord("TOOLTIPSELECT")));
        selectEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        //selectEditBtn.getStyleClass().addAll("toolbar-button");
        // un select
        Button noSelectEditBtn = new Button(); 
        noSelectEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/crop.png"));
        noSelectEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("NOSELECT")+"\n"+objDictionaryAction.getWord("TOOLTIPNOSELECT")));
        noSelectEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        //noSelectEditBtn.getStyleClass().addAll("toolbar-button");
        // Copy item
        Button copyEditBtn = new Button(); 
        copyEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/copy.png"));
        copyEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("COPY")+"\n"+objDictionaryAction.getWord("TOOLTIPCOPY")));
        copyEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        //copyEditBtn.getStyleClass().addAll("toolbar-button");    
        // Paste item
        Button pasteEditBtn = new Button(); 
        pasteEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/paste.png"));
        pasteEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("PASTE")+"\n"+objDictionaryAction.getWord("TOOLTIPPASTE")));
        pasteEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        //pasteEditBtn.getStyleClass().addAll("toolbar-button"); 
        // cut
        Button cutEditBtn = new Button(); 
        cutEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cut.png"));
        cutEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CUT")+"\n"+objDictionaryAction.getWord("TOOLTIPCUT")));
        cutEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        //cutEditBtn.getStyleClass().addAll("toolbar-button");         
        // fill tool
        Button fillToolEditBtn = new Button(); 
        fillToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/pencil.png"));
        fillToolEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("FILLTOOL")+"\n"+objDictionaryAction.getWord("TOOLTIPPENCIL")));
        fillToolEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        //fillToolEditBtn.getStyleClass().addAll("toolbar-button");  
        // pencil edit item
        Button pencilToolEditBtn = new Button(); 
        pencilToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/pencil.png"));
        pencilToolEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("PENCIL")+"\n"+objDictionaryAction.getWord("TOOLTIPPENCIL")));
        pencilToolEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        //pencilToolEditBtn.getStyleClass().addAll("toolbar-button");  
        // eraser
        Button eraserToolEditBtn = new Button(); 
        eraserToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/eraser.png"));
        eraserToolEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ERASER")+"\n"+objDictionaryAction.getWord("TOOLTIPERASER")));
        eraserToolEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        //eraserToolEditBtn.getStyleClass().addAll("toolbar-button");   
        // line edit item
        Button lineEditBtn = new Button(); 
        lineEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/line.png"));
        lineEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("LINE")+"\n"+objDictionaryAction.getWord("TOOLTIPLINE")));
        lineEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        //lineEditBtn.getStyleClass().addAll("toolbar-button"); 
        // arc edit item
        Button arcEditBtn = new Button(); 
        arcEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/arc.png"));
        arcEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ARC")+"\n"+objDictionaryAction.getWord("TOOLTIPARC")));
        arcEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        //arcEditBtn.getStyleClass().addAll("toolbar-button");    
        // curve edit item
        Button bezierCurveEditBtn = new Button(); 
        bezierCurveEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/bezier_curve.png"));
        bezierCurveEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("BEZIERCURVE")+"\n"+objDictionaryAction.getWord("TOOLTIPBEZIERCURVE")));
        bezierCurveEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        //bezierCurveEditBtn.getStyleClass().addAll("toolbar-button");
        // oval edit item
        Button ovalEditBtn = new Button(); 
        ovalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/oval.png"));
        ovalEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("OVAL")+"\n"+objDictionaryAction.getWord("TOOLTIPOVAL")));
        ovalEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        //ovalEditBtn.getStyleClass().addAll("toolbar-button");    
        // rectangle edit item
        Button rectangleEditBtn = new Button(); 
        rectangleEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/rectangle.png"));
        rectangleEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("RECTANGLE")+"\n"+objDictionaryAction.getWord("TOOLTIPRECTANGLE")));
        rectangleEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        //rectangleEditBtn.getStyleClass().addAll("toolbar-button"); 
        // triangle edit item
        Button triangleEditBtn = new Button(); 
        triangleEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/triangle.png"));
        triangleEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("TRIANGLE")+"\n"+objDictionaryAction.getWord("TOOLTIPTRIANGLE")));
        triangleEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        //triangleEditBtn.getStyleClass().addAll("toolbar-button");   
        // heart edit item
        Button heartEditBtn = new Button(); 
        heartEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/heart.png"));
        heartEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("HEART")+"\n"+objDictionaryAction.getWord("TOOLTIPHEART")));
        heartEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        //heartEditBtn.getStyleClass().addAll("toolbar-button");   
        // diamond edit item
        Button diamondEditBtn = new Button(); 
        diamondEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/diamond.png"));
        diamondEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("DIAMOND")+"\n"+objDictionaryAction.getWord("TOOLTIPDIAMOND")));
        diamondEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        //diamondEditBtn.getStyleClass().addAll("toolbar-button"); 
        // pentagon edit item
        Button pentagonEditBtn = new Button(); 
        pentagonEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/pentagon.png"));
        pentagonEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("PENTAGONE")+"\n"+objDictionaryAction.getWord("TOOLTIPPENTAGONE")));
        pentagonEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        //pentagonEditBtn.getStyleClass().addAll("toolbar-button");   
        //  hexagon edit item
        Button hexagonEditBtn = new Button(); 
        hexagonEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/hexagon.png"));
        hexagonEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("HEXAGONE")+"\n"+objDictionaryAction.getWord("TOOLTIPHEXAGONE")));
        hexagonEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        //hexagonEditBtn.getStyleClass().addAll("toolbar-button");
        // four point star  edit item
        Button fourPointStarEditBtn = new Button(); 
        fourPointStarEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/four_star.png"));
        fourPointStarEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("FOURPOINTSTAR")+"\n"+objDictionaryAction.getWord("TOOLTIPFOURPOINTSTAR")));
        fourPointStarEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        //fourPointStarEditBtn.getStyleClass().addAll("toolbar-button");
        // five point star  edit item
        Button fivePointStarEditBtn = new Button(); 
        fivePointStarEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/five_star.png"));
        fivePointStarEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("FIVEPOINTSTAR")+"\n"+objDictionaryAction.getWord("TOOLTIPFIVEPOINTSTAR")));
        fivePointStarEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        //fivePointStarEditBtn.getStyleClass().addAll("toolbar-button");  
        // six point star  edit item
        Button sixPointStarEditBtn = new Button(); 
        sixPointStarEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/six_star.png"));
        sixPointStarEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SIXPOINTSTAR")+"\n"+objDictionaryAction.getWord("TOOLTIPSIXPOINTSTAR")));
        sixPointStarEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        //sixPointStarEditBtn.getStyleClass().addAll("toolbar-button");
        // polygon  edit item
        Button ploygonEditBtn = new Button(); 
        ploygonEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/rhombus.png"));
        ploygonEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("POLYGON")+"\n"+objDictionaryAction.getWord("TOOLTIPPOLYGON")));
        ploygonEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        //ploygonEditBtn.getStyleClass().addAll("toolbar-button");  
        //add enable disbale condition
        if(!isWorkingMode){
            selectEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/select.png"));
            selectEditBtn.setDisable(true);
            selectEditBtn.setCursor(Cursor.WAIT);
            noSelectEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/crop.png"));
            noSelectEditBtn.setDisable(true);
            noSelectEditBtn.setCursor(Cursor.WAIT);
            copyEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/copy.png"));
            copyEditBtn.setDisable(true);
            copyEditBtn.setCursor(Cursor.WAIT);
            pasteEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/paste.png"));
            pasteEditBtn.setDisable(true);
            pasteEditBtn.setCursor(Cursor.WAIT);
            cutEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cut.png"));
            cutEditBtn.setDisable(true);
            cutEditBtn.setCursor(Cursor.WAIT);
            pencilToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/pencil.png"));
            pencilToolEditBtn.setDisable(true);
            pencilToolEditBtn.setCursor(Cursor.WAIT);
            eraserToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/eraser.png"));
            eraserToolEditBtn.setDisable(true);
            eraserToolEditBtn.setCursor(Cursor.WAIT);
            lineEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/line.png"));
            lineEditBtn.setDisable(true);
            lineEditBtn.setCursor(Cursor.WAIT);
            arcEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/arc.png"));
            arcEditBtn.setDisable(true);
            arcEditBtn.setCursor(Cursor.WAIT);
            bezierCurveEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/bezier_curve.png"));
            bezierCurveEditBtn.setDisable(true);
            bezierCurveEditBtn.setCursor(Cursor.WAIT);
            ovalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/oval.png"));
            ovalEditBtn.setDisable(true);
            ovalEditBtn.setCursor(Cursor.WAIT);
            rectangleEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/rectangle.png"));
            rectangleEditBtn.setDisable(true);
            rectangleEditBtn.setCursor(Cursor.WAIT);
            triangleEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/triangle.png"));
            triangleEditBtn.setDisable(true);
            triangleEditBtn.setCursor(Cursor.WAIT);
            heartEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/heart.png"));
            heartEditBtn.setDisable(true);
            heartEditBtn.setCursor(Cursor.WAIT);
            diamondEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/diamond.png"));
            diamondEditBtn.setDisable(true);
            diamondEditBtn.setCursor(Cursor.WAIT);
            pentagonEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/pentagon.png"));
            pentagonEditBtn.setDisable(true);
            pentagonEditBtn.setCursor(Cursor.WAIT);
            hexagonEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/hexagon.png"));
            hexagonEditBtn.setDisable(true);
            hexagonEditBtn.setCursor(Cursor.WAIT);
            fourPointStarEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/four_star.png"));
            fourPointStarEditBtn.setDisable(true);
            fourPointStarEditBtn.setCursor(Cursor.WAIT);
            fivePointStarEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/five_star.png"));
            fivePointStarEditBtn.setDisable(true);
            fivePointStarEditBtn.setCursor(Cursor.WAIT);
            sixPointStarEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/six_star.png"));
            sixPointStarEditBtn.setDisable(true);
            sixPointStarEditBtn.setCursor(Cursor.WAIT);
            ploygonEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/rhombus.png"));
            ploygonEditBtn.setDisable(true);
            ploygonEditBtn.setCursor(Cursor.WAIT);
        }else{
            selectEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/select.png"));
            selectEditBtn.setDisable(false);
            selectEditBtn.setCursor(Cursor.HAND);
            noSelectEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/crop.png"));
            noSelectEditBtn.setDisable(false);
            noSelectEditBtn.setCursor(Cursor.HAND);
            copyEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/copy.png"));
            copyEditBtn.setDisable(false);
            copyEditBtn.setCursor(Cursor.HAND);
            pasteEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/paste.png"));
            pasteEditBtn.setDisable(false);
            pasteEditBtn.setCursor(Cursor.HAND);
            cutEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cut.png"));
            cutEditBtn.setDisable(false);
            cutEditBtn.setCursor(Cursor.HAND);
            pencilToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/pencil.png"));
            pencilToolEditBtn.setDisable(false);
            pencilToolEditBtn.setCursor(Cursor.HAND);
            eraserToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/eraser.png"));
            eraserToolEditBtn.setDisable(false);
            eraserToolEditBtn.setCursor(Cursor.HAND);
            lineEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/line.png"));
            lineEditBtn.setDisable(false);
            lineEditBtn.setCursor(Cursor.HAND);
            arcEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/arc.png"));
            arcEditBtn.setDisable(false);
            arcEditBtn.setCursor(Cursor.HAND);
            bezierCurveEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/bezier_curve.png"));
            bezierCurveEditBtn.setDisable(false);
            bezierCurveEditBtn.setCursor(Cursor.HAND);
            ovalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/oval.png"));
            ovalEditBtn.setDisable(false);
            ovalEditBtn.setCursor(Cursor.HAND);
            rectangleEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/rectangle.png"));
            rectangleEditBtn.setDisable(false);
            rectangleEditBtn.setCursor(Cursor.HAND);
            triangleEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/triangle.png"));
            triangleEditBtn.setDisable(false);
            triangleEditBtn.setCursor(Cursor.HAND);
            heartEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/heart.png"));
            heartEditBtn.setDisable(false);
            heartEditBtn.setCursor(Cursor.HAND);
            diamondEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/diamond.png"));
            diamondEditBtn.setDisable(false);
            diamondEditBtn.setCursor(Cursor.HAND);
            pentagonEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/pentagon.png"));
            pentagonEditBtn.setDisable(false);
            pentagonEditBtn.setCursor(Cursor.HAND);
            hexagonEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/hexagon.png"));
            hexagonEditBtn.setDisable(false);
            hexagonEditBtn.setCursor(Cursor.HAND);
            fourPointStarEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/four_star.png"));
            fourPointStarEditBtn.setDisable(false);
            fourPointStarEditBtn.setCursor(Cursor.HAND);
            fivePointStarEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/five_star.png"));
            fivePointStarEditBtn.setDisable(false);
            fivePointStarEditBtn.setCursor(Cursor.HAND);
            sixPointStarEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/six_star.png"));
            sixPointStarEditBtn.setDisable(false);
            sixPointStarEditBtn.setCursor(Cursor.HAND);
            ploygonEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tool/rhombus.png"));
            ploygonEditBtn.setDisable(false);
            ploygonEditBtn.setCursor(Cursor.HAND);
        }
        //Add the action to Buttons.
        pencilToolEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                plotEditActionMode = 11; //freehanddesign = 11
            }
        });
        eraserToolEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 12; //eraser = 12
            }
        });
        lineEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 13; //drawline = 13
            }
        });
        ovalEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 14; //drawoval = 14
            }
        });
        rectangleEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 15; //drawrectangle = 15
            }
        });
        triangleEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 16; //drawtriangle = 16
            }
        });
        heartEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 17; //drawheart = 17
            }
        });
        diamondEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 18; //drawdiamond = 18
            }
        });
        pentagonEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 19; //drawpentagon = 19
            }
        });
        hexagonEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 20; //drawhexagone = 20
            }
        });
        fourPointStarEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 21; //drawfourpointstar = 21
            }
        });
        fivePointStarEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 22; //drawfivepointstar = 22
            }
        });
        sixPointStarEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPENCIL"));
                setCurrentShape();
                plotEditActionMode = 23; //drawsixpointstar = 23
            }
        });
        
        //toolkitEditGP.add(selectEditBtn,0,0);
        //toolkitEditGP.add(noSelectEditBtn,1,0);
        //toolkitEditGP.add(copyEditBtn,2,0);
        //toolkitEditGP.add(pasteEditBtn,3,0);
        //toolkitEditGP.add(cutEditBtn,4,0);
        
        toolkitEditGP.add(pencilToolEditBtn,0,1);
        toolkitEditGP.add(eraserToolEditBtn,1,1);
        //toolkitEditGP.add(lineEditBtn,2,1);
        //toolkitEditGP.add(arcEditBtn,3,1);
        //toolkitEditGP.add(bezierCurveEditBtn,4,1);
        
        //toolkitEditGP.add(ovalEditBtn,0,2);
        //toolkitEditGP.add(triangleEditBtn,1,2);
        //toolkitEditGP.add(rectangleEditBtn,2,2);
        //toolkitEditGP.add(heartEditBtn,3,2);
        //toolkitEditGP.add(diamondEditBtn,4,2);
        
        //toolkitEditGP.add(pentagonEditBtn,0,3);
        //toolkitEditGP.add(hexagonEditBtn,1,3);
        //toolkitEditGP.add(fourPointStarEditBtn,2,3);
        //toolkitEditGP.add(sixPointStarEditBtn,3,3);
        //toolkitEditGP.add(fivePointStarEditBtn,4,3);
        
        //toolkitEditGP.add(ploygonEditBtn,0,4);
        
        popup.add(toolkitEditGP,0,1, 2, 1);
        
        Separator sepHor1 = new Separator();
        sepHor1.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHor1, 0, 2);
        GridPane.setColumnSpan(sepHor1, 2);
        popup.getChildren().add(sepHor1);
                
        Label fillToolColor = new Label(objDictionaryAction.getWord("FILLTOOLCOLOR")+" : ");
        fillToolColor.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPFILLTOOLCOLOR")));
        popup.add(fillToolColor, 0, 3);
        
        //final ColorPicker fillToolColorPicker = new ColorPicker();
        //fillToolColorPicker.setStyle("-fx-color-label-visible: false ;");
        //fillToolColorPicker.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPFILLTOOLCOLOR")));
        //popup.add(fillToolColorPicker, 1, 5); 
        
        final ComboBox colorCB=new ComboBox();
        colorCB.setOnShown(new EventHandler() {
            @Override
            public void handle(Event t) {
                try {
                    ColourSelector objColourSelector=new ColourSelector(objArtwork.getObjConfiguration());
                    if(objColourSelector.colorCode!=null&&objColourSelector.colorCode.length()>0){
                        colorCB.setStyle("-fx-background-color:#"+objColourSelector.colorCode+";");
                        
                        String hex=colorCB.getStyle().substring(colorCB.getStyle().lastIndexOf("#")+1, colorCB.getStyle().lastIndexOf("#")+7);
                        editThreadPrimaryValue = Integer.parseInt(hex.substring(0, 2), 16);//(int)(fillToolColorPicker.getValue().getRed()*255);
                        editThreadPrimaryValue = (editThreadPrimaryValue << 8) + Integer.parseInt(hex.substring(2, 4), 16);//(int)(fillToolColorPicker.getValue().getGreen()*255);
                        editThreadPrimaryValue = (editThreadPrimaryValue << 8) + Integer.parseInt(hex.substring(4, 6), 16);//(int)(fillToolColorPicker.getValue().getBlue()*255);
                        //System.out.println((int)(fillToolColorPicker.getValue().getRed()*255)+"="+editThreadPrimaryValue+"="+(fillToolColorPicker.getValue().getRed()*255));
                        //System.out.println((int)(fillToolColorPicker.getValue().getGreen()*255)+"="+editThreadPrimaryValue+"="+(fillToolColorPicker.getValue().getGreen()*255));
                        //System.out.println((int)(fillToolColorPicker.getValue().getBlue()*255)+"="+editThreadPrimaryValue+"="+(fillToolColorPicker.getValue().getBlue()*255));
                        System.err.println(editThreadPrimaryValue);
                    }
                    colorCB.hide();
                    t.consume();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"colourPalettePopup: warp",ex);
                }
            }
        });
        colorCB.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPYARNCOLOR")));
        popup.add(colorCB, 1, 3);

        Label fillToolSize = new Label(objDictionaryAction.getWord("FILLTOOLSIZE")+" : ");
        fillToolSize.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPFILLTOOLSIZE")));
        popup.add(fillToolSize, 0, 4);
            
        if(intFillSize<1 || intFillSize>10)
            intFillSize = 1;
        final Slider sizeSlider = new Slider(1, 10, intFillSize);    
        sizeSlider.setShowTickLabels(true);
        sizeSlider.setShowTickMarks(true);
        sizeSlider.setBlockIncrement(1);
        sizeSlider.setMajorTickUnit(2);
        sizeSlider.setMinorTickCount(1);
        popup.add(sizeSlider, 1, 4);
        
        final Label sizeTF = new Label();
        sizeTF.setText(Byte.toString(intFillSize));
        sizeTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPFILLTOOLSIZE")));
        popup.add(sizeTF, 1, 5);  
        
        sizeSlider.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
                intFillSize = new_val.byteValue();
                sizeTF.setText(Integer.toString(intFillSize));
            }
        });    
            
        Separator sepHor2 = new Separator();
        sepHor2.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHor2, 0, 2);
        GridPane.setColumnSpan(sepHor2, 6);
        popup.getChildren().add(sepHor2);
          
        Button btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));
        btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
        btnCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                artworkChildStage.close();
                initArtworkValue();
                plotViewActionMode = 1;
            plotViewAction();
            }
        });
        popup.add(btnCancel,0,7,2,1);
        
        artworkChildStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                artworkChildStage.close();
                initArtworkValue();
                plotViewActionMode = 1;
            plotViewAction();
            }
        });
        Scene popupScene = new Scene(popup);
        popupScene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        artworkChildStage.setScene(popupScene);
        artworkChildStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("WINDOWEDITTOOLKIT"));
        artworkChildStage.showAndWait();        
    }
    private void freeDrawing() {
        
        //int alpha   = (editThreadPrimaryValue & 0xffff0000) >> 24;
        //int red   = (editThreadPrimaryValue & 0x00ff0000) >> 16;
        //int green = (editThreadPrimaryValue & 0x0000ff00) >> 8;
        //int blue  =  editThreadPrimaryValue & 0x000000ff;     
                
        //System.err.println(editThreadPrimaryValue+"="+new java.awt.Color(editThreadPrimaryValue,true).getRGB());
        //System.err.println(editThreadPrimaryValue+"="+new java.awt.Color(red, green, blue).getRGB());
        //System.err.println(editThreadPrimaryValue+"="+new java.awt.Color(red, green, blue, alpha).getRGB());
        
        //System.err.println(intFillSize+"="+(intFillSize*zoomfactor));
        //System.err.println((int)oldX+":"+(int)oldY+"::"+(int)lastX+":"+(int)lastY);
        
        
        for(int x=-1*(intFillSize/2); x<intFillSize; x++)
            for(int y=-1*(intFillSize/2); y<intFillSize; y++)
                objBufferedImage.setRGB((int)((lastX/zoomfactor)+x)%objBufferedImage.getWidth(), (int)((lastY/zoomfactor)+y)%objBufferedImage.getHeight(), editThreadPrimaryValue);
        
        BasicStroke bs = new BasicStroke(intFillSize*zoomfactor);
        objGraphics2D.setStroke(bs);
        objGraphics2D.setColor(new java.awt.Color(editThreadPrimaryValue, false));
        objGraphics2D.drawLine((int)oldX, (int)oldY, (int)lastX, (int)lastY);
        //objGraphics2D.fillRect((int)(lastX), (int)(lastY), (int)(intFillSize*zoomfactor), (int)(intFillSize*zoomfactor));
        oldX = lastX;
        oldY = lastY;
        //initArtworkValue();
        artwork.setImage(SwingFXUtils.toFXImage(objEditBufferedImage, null));
        container.setContent(artwork);
    }
    
    private void eraserDrawing() {
        for(int x=-1*(intFillSize/2); x<intFillSize; x++)
            for(int y=-1*(intFillSize/2); y<intFillSize; y++)
                objBufferedImage.setRGB((int)((lastX/zoomfactor)+x)%objBufferedImage.getWidth(), (int)((lastY/zoomfactor)+y)%objBufferedImage.getHeight(), -1);
        
        BasicStroke bs = new BasicStroke(intFillSize*zoomfactor);
        objGraphics2D.setStroke(bs);
        objGraphics2D.setColor(java.awt.Color.WHITE);
        objGraphics2D.drawLine((int)oldX, (int)oldY, (int)lastX, (int)lastY);
        //objGraphics2D.fillRect((int)(lastX), (int)(lastY), (int)(intFillSize*zoomfactor), (int)(intFillSize*zoomfactor));
        oldX = lastX;
        oldY = lastY;
        //initArtworkValue();
        artwork.setImage(SwingFXUtils.toFXImage(objEditBufferedImage, null));
        container.setContent(artwork);
    }

private void plotColorPalette(){
        final GridPane paletteColorGP = new GridPane();
        paletteColorGP.getChildren().clear();
        paletteColorGP.setVgap(5);
        paletteColorGP.setHgap(5);
        //paletteColorGP.autosize();
        //paletteColorGP.setId("leftPane");
        
        paletteColorGP.add(new Label(objDictionaryAction.getWord("PALETTE")), 0, 0, 1, 1);
        
        final ComboBox paletteCB = new ComboBox();
        paletteCB.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPPALETTE")));
        try{
            Palette objPalette = new Palette();
            objPalette.setObjConfiguration(objConfiguration);
            objPalette.setStrPaletteType("Colour");
            UtilityAction objUtilityAction = new UtilityAction();
            String[][] paletteData=objUtilityAction.getPaletteName(objPalette);
            if(paletteData!=null){
                for(String[] data:paletteData){
                    objPalette = new Palette();
                    objPalette.setStrPaletteName(data[1]);
                    objPalette.setStrPaletteID(data[0]);
                    objPalette.setObjConfiguration(objConfiguration);
                    objUtilityAction=new UtilityAction();
                    objUtilityAction.getPalette(objPalette);
                    if(objPalette.getStrThreadPalette()!=null){
                        paletteCB.getItems().add(data[1]);
                        paletteCB.setValue(data[1]);
                        objArtwork.getObjConfiguration().setColourPalette(objPalette.getStrThreadPalette());
                    }
                }
            }
        } catch(SQLException sqlEx){
            new Logging("SEVERE",getClass().getName(),"loadPaletteNames() : ", sqlEx);
        }
        paletteColorGP.add(paletteCB, 0, 1, 1, 1);
        
        paletteCB.valueProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                try{
                    UtilityAction objUtilityAction=new UtilityAction();
                    String id=objUtilityAction.getPaletteIdFromName(t1.toString());
                    if(id!=null){
                        Palette objPalette = new Palette();
                        objPalette.setStrPaletteID(id);
                        objPalette.setObjConfiguration(objConfiguration);
                        objUtilityAction=new UtilityAction();
                        objUtilityAction.getPalette(objPalette);
                        if(objPalette.getStrThreadPalette()!=null){
                            objArtwork.getObjConfiguration().setColourPalette(objPalette.getStrThreadPalette());
                        }
                    }
                    //String[] tPalette=objColorPaletteAction.getPaletteFromName(paletteName);
                    //if(tPalette!=null)
                    //    threadPaletes=tPalette;
                    populateYarnPalette();
                } catch(SQLException sqlEx){
                    new Logging("SEVERE",getClass().getName(),"loadColorPalette() : ", sqlEx);
                }
            }
        });
        
        editThreadGP = new GridPane();
        editThreadGP.setAlignment(Pos.CENTER);
        editThreadGP.setHgap(5);
        editThreadGP.setVgap(5);
        editThreadGP.setCursor(Cursor.HAND);
        //editThreadGP.getChildren().clear();
        loadColorPalettes(); // it sets colorPalates[][] using threadPalettes
        paletteColorGP.add(editThreadGP, 0, 2, 1, 1);
    }
    
    //Sets weave's configuration's Yarn Palette Color to weave's configuration's color palette
     
    private void populateYarnPalette(){
        for(int i=0; i<52; i++){
            objArtwork.getObjConfiguration().getYarnPalette()[i].setStrYarnColor("#"+objArtwork.getObjConfiguration().getColourPalette()[i]);
        }
    }
    private void populateColorPalette(){
        for(int i=0; i<52; i++){
            objArtwork.getObjConfiguration().getColourPalette()[i]=objArtwork.getObjConfiguration().getYarnPalette()[i].getStrYarnColor().substring(1, 7);
        }
    }
    
   private void loadColorPalettes(){

        //objArtwork.getObjConfiguration().strThreadPalettes = objArtwork.getObjConfiguration().strThreadPalettes;
        String[][] colorPaletes = new String[52][2];
        colorPaletes[0][0]="A";
        colorPaletes[0][1]=objArtwork.getObjConfiguration().getColourPalette()[0];
        colorPaletes[1][0]="B";
        colorPaletes[1][1]=objArtwork.getObjConfiguration().getColourPalette()[1];
        colorPaletes[2][0]="C";
        colorPaletes[2][1]=objArtwork.getObjConfiguration().getColourPalette()[2];
        colorPaletes[3][0]="D";
        colorPaletes[3][1]=objArtwork.getObjConfiguration().getColourPalette()[3];
        colorPaletes[4][0]="E";
        colorPaletes[4][1]=objArtwork.getObjConfiguration().getColourPalette()[4];
        colorPaletes[5][0]="F";
        colorPaletes[5][1]=objArtwork.getObjConfiguration().getColourPalette()[5];
        colorPaletes[6][0]="G";
        colorPaletes[6][1]=objArtwork.getObjConfiguration().getColourPalette()[6];
        colorPaletes[7][0]="H";
        colorPaletes[7][1]=objArtwork.getObjConfiguration().getColourPalette()[7];
        colorPaletes[8][0]="I";
        colorPaletes[8][1]=objArtwork.getObjConfiguration().getColourPalette()[8];
        colorPaletes[9][0]="J";
        colorPaletes[9][1]=objArtwork.getObjConfiguration().getColourPalette()[9];
        colorPaletes[10][0]="K";
        colorPaletes[10][1]=objArtwork.getObjConfiguration().getColourPalette()[10];
        colorPaletes[11][0]="L";
        colorPaletes[11][1]=objArtwork.getObjConfiguration().getColourPalette()[11];
        colorPaletes[12][0]="M";
        colorPaletes[12][1]=objArtwork.getObjConfiguration().getColourPalette()[12];
        colorPaletes[13][0]="N";
        colorPaletes[13][1]=objArtwork.getObjConfiguration().getColourPalette()[13];
        colorPaletes[14][0]="O";
        colorPaletes[14][1]=objArtwork.getObjConfiguration().getColourPalette()[14];
        colorPaletes[15][0]="P";
        colorPaletes[15][1]=objArtwork.getObjConfiguration().getColourPalette()[15];
        colorPaletes[16][0]="Q";
        colorPaletes[16][1]=objArtwork.getObjConfiguration().getColourPalette()[16];
        colorPaletes[17][0]="R";
        colorPaletes[17][1]=objArtwork.getObjConfiguration().getColourPalette()[17];
        colorPaletes[18][0]="S";
        colorPaletes[18][1]=objArtwork.getObjConfiguration().getColourPalette()[18];
        colorPaletes[19][0]="T";
        colorPaletes[19][1]=objArtwork.getObjConfiguration().getColourPalette()[19];
        colorPaletes[20][0]="U";
        colorPaletes[20][1]=objArtwork.getObjConfiguration().getColourPalette()[20];
        colorPaletes[21][0]="V";
        colorPaletes[21][1]=objArtwork.getObjConfiguration().getColourPalette()[21];
        colorPaletes[22][0]="W";
        colorPaletes[22][1]=objArtwork.getObjConfiguration().getColourPalette()[22];
        colorPaletes[23][0]="X";
        colorPaletes[23][1]=objArtwork.getObjConfiguration().getColourPalette()[23];
        colorPaletes[24][0]="Y";
        colorPaletes[24][1]=objArtwork.getObjConfiguration().getColourPalette()[24];
        colorPaletes[25][0]="Z";
        colorPaletes[25][1]=objArtwork.getObjConfiguration().getColourPalette()[25];
        colorPaletes[26][0]="a";
        colorPaletes[26][1]=objArtwork.getObjConfiguration().getColourPalette()[26];
        colorPaletes[27][0]="b";
        colorPaletes[27][1]=objArtwork.getObjConfiguration().getColourPalette()[27];
        colorPaletes[28][0]="c";
        colorPaletes[28][1]=objArtwork.getObjConfiguration().getColourPalette()[28];
        colorPaletes[29][0]="d";
        colorPaletes[29][1]=objArtwork.getObjConfiguration().getColourPalette()[29];
        colorPaletes[30][0]="e";
        colorPaletes[30][1]=objArtwork.getObjConfiguration().getColourPalette()[30];
        colorPaletes[31][0]="f";
        colorPaletes[31][1]=objArtwork.getObjConfiguration().getColourPalette()[31];
        colorPaletes[32][0]="g";
        colorPaletes[32][1]=objArtwork.getObjConfiguration().getColourPalette()[32];
        colorPaletes[33][0]="h";
        colorPaletes[33][1]=objArtwork.getObjConfiguration().getColourPalette()[33];
        colorPaletes[34][0]="i";
        colorPaletes[34][1]=objArtwork.getObjConfiguration().getColourPalette()[34];
        colorPaletes[35][0]="j";
        colorPaletes[35][1]=objArtwork.getObjConfiguration().getColourPalette()[35];
        colorPaletes[36][0]="k";
        colorPaletes[36][1]=objArtwork.getObjConfiguration().getColourPalette()[36];
        colorPaletes[37][0]="l";
        colorPaletes[37][1]=objArtwork.getObjConfiguration().getColourPalette()[37];
        colorPaletes[38][0]="m";
        colorPaletes[38][1]=objArtwork.getObjConfiguration().getColourPalette()[38];
        colorPaletes[39][0]="n";
        colorPaletes[39][1]=objArtwork.getObjConfiguration().getColourPalette()[39];
        colorPaletes[40][0]="o";
        colorPaletes[40][1]=objArtwork.getObjConfiguration().getColourPalette()[40];
        colorPaletes[41][0]="p";
        colorPaletes[41][1]=objArtwork.getObjConfiguration().getColourPalette()[41];
        colorPaletes[42][0]="q";
        colorPaletes[42][1]=objArtwork.getObjConfiguration().getColourPalette()[42];
        colorPaletes[43][0]="r";
        colorPaletes[43][1]=objArtwork.getObjConfiguration().getColourPalette()[43];
        colorPaletes[44][0]="s";
        colorPaletes[44][1]=objArtwork.getObjConfiguration().getColourPalette()[44];
        colorPaletes[45][0]="t";
        colorPaletes[45][1]=objArtwork.getObjConfiguration().getColourPalette()[45];
        colorPaletes[46][0]="u";
        colorPaletes[46][1]=objArtwork.getObjConfiguration().getColourPalette()[46];
        colorPaletes[47][0]="v";
        colorPaletes[47][1]=objArtwork.getObjConfiguration().getColourPalette()[47];
        colorPaletes[48][0]="w";
        colorPaletes[48][1]=objArtwork.getObjConfiguration().getColourPalette()[48];
        colorPaletes[49][0]="x";
        colorPaletes[49][1]=objArtwork.getObjConfiguration().getColourPalette()[49];
        colorPaletes[50][0]="y";
        colorPaletes[50][1]=objArtwork.getObjConfiguration().getColourPalette()[50];
        colorPaletes[51][0]="z";
        colorPaletes[51][1]=objArtwork.getObjConfiguration().getColourPalette()[51];

        editThreadGP.getChildren().clear();
        
        Separator sepVertFabric = new Separator();
        sepVertFabric.setOrientation(Orientation.VERTICAL);
        sepVertFabric.setValignment(VPos.CENTER);
        sepVertFabric.setPrefHeight(80);
        GridPane.setConstraints(sepVertFabric, 2, 0);
        GridPane.setRowSpan(sepVertFabric, 14);
        editThreadGP.getChildren().add(sepVertFabric);
        
        int boxSize = 10;
        for(int i=0; i<(int)objArtwork.getObjConfiguration().getColourPalette().length; i++){
            final Label lblC= new Label(colorPaletes[i][0]);
            int rgb = ((Integer.valueOf( objArtwork.getObjConfiguration().getColourPalette()[i].substring( 0, 2 ), 16 )&0x0ff)<<16)|((Integer.valueOf( objArtwork.getObjConfiguration().getColourPalette()[i].substring( 2, 4 ), 16 )&0x0ff)<<8)|(Integer.valueOf( objArtwork.getObjConfiguration().getColourPalette()[i].substring( 4, 6 ), 16 )&0x0ff);
            lblC.setUserData(rgb);
            lblC.setPrefSize(boxSize,boxSize);
            lblC.setStyle("-fx-background-color: #"+objArtwork.getObjConfiguration().getColourPalette()[i]+"; -fx-font-size: 10; -fx-width:25px; -fx-height:25px;-fx-border-width: 1;  -fx-border-color: black;");
			lblC.setTooltip(new Tooltip("Hex Code: #"+objArtwork.getObjConfiguration().getColourPalette()[i]+"\nRGB: "+
                                                    Integer.valueOf( objArtwork.getObjConfiguration().getColourPalette()[i].substring( 0, 2 ), 16 )+","+
                                                    Integer.valueOf( objArtwork.getObjConfiguration().getColourPalette()[i].substring( 2, 4 ), 16 )+","+
                                                    Integer.valueOf( objArtwork.getObjConfiguration().getColourPalette()[i].substring( 4, 6 ), 16 )));
            lblC.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if (event.getClickCount() >1) {
                        //open clor browser
                    } else if(event.isSecondaryButtonDown() || (event.getButton() == MouseButton.SECONDARY)){
                        String strWeftColor = lblC.getStyle().substring(lblC.getStyle().lastIndexOf("-fx-background-color:")+21,lblC.getStyle().indexOf(";")).trim();
                        editThreadSecondaryValue=Integer.parseInt(lblC.getUserData().toString());
                    }else{
                        String strWeftColor = lblC.getStyle().substring(lblC.getStyle().lastIndexOf("-fx-background-color:")+21,lblC.getStyle().indexOf(";")).trim();
                        editThreadPrimaryValue=Integer.parseInt(lblC.getUserData().toString());
                    }
               }
            });
            int colp = (i>25)?(i/13)+1:(i/13);
            editThreadGP.add(lblC, colp, (i%13)+1);
        }
   }
}


//////////////////////////// toolbox Menu Action ///////////////////////////
private void artworkVerticalMirror() {    
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHORIZENTALMIRROR"));
        try {
            objUR.doCommand("VMirror", objBufferedImage);
            //objURF.store(objBufferedImage);
            objArtworkAction = new ArtworkAction();
            objBufferedImage = objArtworkAction.getImageVerticalMirror(objBufferedImage);
            initArtworkValue();
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"Operation Black and white",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }  catch(OutOfMemoryError ex){
            undoAction();
        }
    }    
    private void artworkHorizentalMirror() {    
        lblStatus.setText(objDictionaryAction.getWord("ACTIONVERTICALMIRROR"));
        try {
            objUR.doCommand("HMirror", objBufferedImage);
            //objURF.store(objBufferedImage);
            objArtworkAction = new ArtworkAction();
            objBufferedImage = objArtworkAction.getImageHorizontalMirror(objBufferedImage);
            initArtworkValue();
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"Operation Black and white",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch(OutOfMemoryError ex){
            undoAction();
        }
    }
    private void artworkClockRotation() {    
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCLOCKROTATION"));
        try {
            objUR.doCommand("CRotate", objBufferedImage);
            //objURF.store(objBufferedImage);
            objArtworkAction = new ArtworkAction();
            objBufferedImage = objArtworkAction.getImageRotation(objBufferedImage,"CLOCKWISE");
            initArtworkValue();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Operation clockwise rotation",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void artworkAntiClockRotation() {    
        lblStatus.setText(objDictionaryAction.getWord("ACTIONANTICLOCKROTATION"));
        try {
            objUR.doCommand("Counter Clockwise Rotation", objBufferedImage);
            //objURF.store(objBufferedImage);
            objArtworkAction = new ArtworkAction();
            objBufferedImage = objArtworkAction.getImageRotation(objBufferedImage,"COUNTERCLOCKWISE");
            initArtworkValue();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Operation counter clockwise rotation",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    } 
*/
///----- U --- N ----- U --- S --- E --- D ----- C --- O --- D --- E -----///
class NVLines extends Thread
{
    Graphics2D objGraphics2D;
    int intHeight, intLength, graphFactor;
    String [] data;
    float zoomfactor;
    byte type;
    NVLines(Graphics2D g, int intHeight, int intLength, int graphFactor, String[] data, float zoomfactor, byte type)
    {
        super("my extending thread");
        this.objGraphics2D = g;
        this.intHeight = intHeight;
        this.intLength = intLength;
        this.graphFactor = graphFactor;
        this.data = data;
        this.zoomfactor = zoomfactor;
        this.type = type;
        //System.out.println("my thread created" + this);
        start();
    }
    public void run()
    {
        try
        {
            BasicStroke bs = new BasicStroke(2);
            objGraphics2D.setStroke(bs);
            //box resized
            int x_inc = (int)Math.round(zoomfactor*Integer.parseInt(data[1])/graphFactor);
            int y_inc = (int)Math.round(zoomfactor*Integer.parseInt(data[0])/graphFactor);
            //last line vertical
            objGraphics2D.drawLine(intLength*x_inc, 0, intLength*x_inc, intHeight*y_inc);
            //last line horizontal
            objGraphics2D.drawLine(0, intHeight*y_inc, intLength*x_inc, intHeight*y_inc);
            //For vertical line
            for(int j = 0; j < intLength; j++) {
                if((j%(Integer.parseInt(data[0])))==0 && type==3){
                    bs = new BasicStroke(2*zoomfactor);
                    objGraphics2D.setStroke(bs);
                }else{
                    bs = new BasicStroke(1*zoomfactor);
                    objGraphics2D.setStroke(bs);
                }
                objGraphics2D.drawLine(j*x_inc, 0, j*x_inc, intHeight*y_inc);
            }
            //For horizental line
            for(int i = 0; i < intHeight; i++) {
                if((i%(Integer.parseInt(data[1])))==0 && type==3){
                    bs = new BasicStroke(2*zoomfactor);
                    objGraphics2D.setStroke(bs);
                }else{
                    bs = new BasicStroke(1*zoomfactor);
                    objGraphics2D.setStroke(bs);
                }
                objGraphics2D.drawLine(0, i*y_inc, intLength*x_inc, i*y_inc);
            }
        } catch(Exception ex) {
            ex.printStackTrace();
            //System.out.println("my thread interrupted"+ex.getCause().toString());
            //Logger.getLogger(NVLines.class.getName()).log(Level.SEVERE, null, ex);
        }
        //System.out.println("My thread run is over" );
    }
}