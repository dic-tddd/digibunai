/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mla.dictionary;

import com.sun.speech.freetts.audio.JavaClipAudioPlayer;
import java.util.Locale; 
import javax.speech.Central; 
import javax.speech.synthesis.Synthesizer; 
import javax.speech.synthesis.SynthesizerModeDesc; 
import javax.speech.*;    
import java.util.*;    
import javax.speech.synthesis.*;  
/**
 *
 * @author acer
 */
public class TextToSpeech  {    
    static String speaktext; 
    static String voiceName;
    static{
            System.setProperty("FreeTTSSynthEngineCentral", "com.sun.speech.freetts.jsapi.FreeTTSEngineCentral");
            System.setProperty("freetts.voices", "com.sun.speech.freetts.en.us.cmu_us_kal.KevinVoiceDirectory");      
    }
    
    public static void dospeak(String text) {
        if (text == null || text.trim().isEmpty()) return;

        voiceName = "kevin16";
        try {     
            Central.registerEngineCentral("com.sun.speech.freetts.jsapi.FreeTTSEngineCentral");
            SynthesizerModeDesc desc = new SynthesizerModeDesc(null, "general", Locale.US, null, null);

            Synthesizer synth = Central.createSynthesizer(desc);        
            synth.allocate();
            desc = (SynthesizerModeDesc) synth.getEngineModeDesc();
            Voice[] voices = desc.getVoices();
            Voice voice = null;
            for (Voice entry : voices) {
                System.err.println(""+entry.getName());
                System.err.println(""+entry.getAge());
                System.err.println(""+entry.getGender());
                System.err.println(""+entry.getStyle());
                if(entry.getName().equals(voiceName)) {
                    voice = entry;
                    break;
                }
            }
            synth.getSynthesizerProperties().setVoice(voice);
            synth.getSynthesizerProperties().setPitch(150.0F); 
            //synth.getSynthesizerProperties().setVolume(1.0F);  
            synth.getSynthesizerProperties().setSpeakingRate(100.0F);
            synth.resume();
            synth.speakPlainText(text, null);
            synth.waitEngineState(Synthesizer.QUEUE_EMPTY);
            synth.deallocate();

        } catch(Exception ex) {
            String message = " missing speech.properties in " + System.getProperty("user.home") + "\n";
            System.out.println("" + ex);
            System.out.println(message);
            ex.printStackTrace();
        }
    } 

}