/*
 * Copyright (C) Digital India Corporation (Media Lab Asia)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.mla.dictionary;

import com.mla.utility.AboutView;
import com.mla.main.Configuration;
import com.mla.utility.ContactView;
import com.mla.utility.HelpView;
import com.mla.main.Logging;
import com.mla.main.WindowView;
import com.mla.user.UserLoginView;
import com.mla.utility.TechnicalView;
import com.mla.utility.UtilityView;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Separator;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
/**
 *
 * @Designing GUI window for fabric preferences
 * @author Amit Kumar Singh
 * 
 */
public class DictionaryView extends Application {
    private static Stage dictionaryStage;
    private BorderPane root;
    private Label lblStatus;
    private ProgressBar progressB;
    private ProgressIndicator progressI;
    
    GridPane dictionaryGP;
    GridPane translatorGP;
    
    Configuration objConfiguration = null;
    DictionaryAction objDictionaryAction = null;
    Task copyWorker;
    private Button btnNext;
    private Button btnPrev;
    private int currentPage;
    private int maxPage;
    
    boolean isNew = false;
    String currentLanguage="ENGLISH";
    
    HashMap keyValueHM;
    boolean languageLoadedFirstTime;
    boolean newLanguage;

    public DictionaryView(final Stage primaryStage) {}
    
    public DictionaryView(Configuration objConfigurationCall) {
        objConfiguration = objConfigurationCall;
        objDictionaryAction = new DictionaryAction(objConfiguration);

        dictionaryStage = new Stage(); 
        root = new BorderPane();
        Scene scene = new Scene(root, objConfiguration.WIDTH, objConfiguration.HEIGHT, Color.WHITE);
        scene.getStylesheets().add(DictionaryView.class.getResource(objConfiguration.getStrTemplate()+"/setting.css").toExternalForm());
        
        HBox footContainer = new HBox();
        progressB = new ProgressBar(0);
        progressB.setVisible(false);
        progressI = new ProgressIndicator(0);
        progressI.setVisible(false);
        lblStatus = new Label(objDictionaryAction.getWord("WELCOMETOCADTOOL"));
        lblStatus.setId("message");
        footContainer.getChildren().addAll(lblStatus,progressB,progressI);
        footContainer.setId("footContainer");
        root.setBottom(footContainer);
        
        MenuBar menuBar = new MenuBar();
        menuBar.prefWidthProperty().bind(dictionaryStage.widthProperty());
        
        Menu homeMenu  = new Menu();
        HBox homeMenuHB = new HBox();
        homeMenuHB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/home.png"),new Label(objDictionaryAction.getWord("HOME")));
        homeMenu.setGraphic(homeMenuHB);
        homeMenu.setAccelerator(new KeyCodeCombination(KeyCode.M, KeyCombination.SHIFT_DOWN));
        homeMenuHB.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                homeMenuAction();  
                me.consume();
            }
        }); 
        Menu parentMenu  = new Menu();
        HBox parentMenuHB = new HBox();
        parentMenuHB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/settings.png"),new Label(objDictionaryAction.getWord("PARENT")));
        parentMenu.setGraphic(parentMenuHB);
        parentMenu.setAccelerator(new KeyCodeCombination(KeyCode.Z, KeyCombination.SHIFT_DOWN));
        parentMenuHB.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                parentMenuAction();                 
                me.consume();
            }
        }); 
        //Help Menu items
        Label supportMenuLabel = new Label(objDictionaryAction.getWord("SUPPORT"));
        supportMenuLabel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUPPORT")));
        Menu supportMenu = new Menu();
        supportMenu.setGraphic(supportMenuLabel);
        
        MenuItem helpMenuItem = new MenuItem(objDictionaryAction.getWord("HELP"));
        helpMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        helpMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.H, KeyCombination.SHIFT_DOWN));
        helpMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {       
                helpMenuAction();
            }
        });        
        MenuItem technicalMenuItem = new MenuItem(objDictionaryAction.getWord("TECHNICAL"));
        technicalMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/technical_info.png"));
        technicalMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.T, KeyCombination.SHIFT_DOWN));
        technicalMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {       
                technicalMenuAction();
            }
        });
        MenuItem aboutMenuItem = new MenuItem(objDictionaryAction.getWord("ABOUTUS"));
        aboutMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/about_software.png"));
        aboutMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.A, KeyCombination.SHIFT_DOWN));
        aboutMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                aboutMenuAction();
            }
        });
        MenuItem contactMenuItem = new MenuItem(objDictionaryAction.getWord("CONTACTUS"));
        contactMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/contact_us.png"));
        contactMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.SHIFT_DOWN));
        contactMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                contactMenuAction();
            }
        });
        MenuItem logoutMenuItem = new MenuItem(objDictionaryAction.getWord("LOGOUT"));
        logoutMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/logout.png"));
        logoutMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.L, KeyCombination.SHIFT_DOWN));
        logoutMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                logoutMenuAction();
            }
        });
        MenuItem exitMenuItem = new MenuItem(objDictionaryAction.getWord("EXIT"));
        exitMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/quit.png"));
        exitMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.ESCAPE));
        exitMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                exitMenuAction();
            }
        });       
        supportMenu.getItems().addAll(helpMenuItem, technicalMenuItem, aboutMenuItem, contactMenuItem, new SeparatorMenuItem(), logoutMenuItem);
        menuBar.getMenus().addAll(homeMenu, parentMenu, supportMenu);
        root.setTop(menuBar);
        
        TabPane tabPane = new TabPane();

        Tab dictionaryTab = new Tab();
        
        dictionaryTab.setClosable(false);
        
        dictionaryTab.setText(objDictionaryAction.getWord("TRANSLATOR"));
        dictionaryTab.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPTRANSLATOR")));
        //dictionaryTab.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/translation.png"));
        
        dictionaryGP = new GridPane();
        dictionaryGP.setId("container");
        //dictionaryGP.setPrefSize(objConfiguration.WIDTH, objConfiguration.HEIGHT);
        //dictionaryGP.setAlignment(Pos.TOP_CENTER);
             
        dictionaryTab.setContent(dictionaryGP);
        tabPane.getTabs().add(dictionaryTab);
        
        // New file item
        Button newFileBtn = new Button(objDictionaryAction.getWord("NEWFILE"));
        newFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/new.png"));
        newFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("NEWFILE")+" \n"+objDictionaryAction.getWord("TOOLTIPNEWFILE")));
        newFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        // Open file item
        Button openFileBtn = new Button(objDictionaryAction.getWord("OPENFILE"));
        openFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/open.png"));
        openFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("OPENFILE")+" \n"+objDictionaryAction.getWord("TOOLTIPOPENFILE")));
        openFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        // Save file item
        Button saveFileBtn = new Button(objDictionaryAction.getWord("SAVEFILE"));        
        saveFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
        saveFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SAVEFILE")+" \n"+objDictionaryAction.getWord("TOOLTIPSAVEFILE")));
        saveFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        // Save As file item
        Button saveAsFileBtn = new Button(objDictionaryAction.getWord("SAVEASFILE"));        
        saveAsFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save_as.png"));
        saveAsFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SAVEASFILE")+" \n"+objDictionaryAction.getWord("TOOLTIPSAVEASFILE")));
        saveAsFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        //Action
        newFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {              
                newLanguageLibrary();
            }
        });
        openFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {              
                openLanguageLibrary();
            }
        });
        saveFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) { 
                saveLanguageLibrary();
            }
        });
        saveAsFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {              
                saveAsLanguageLibrary();
            }
        });
        
        dictionaryGP.add(newFileBtn, 1, 0);
        dictionaryGP.add(openFileBtn, 2, 0);
        dictionaryGP.add(saveFileBtn, 3, 0);
        dictionaryGP.add(saveAsFileBtn, 4, 0);
        
        btnPrev = new Button(objDictionaryAction.getWord("<"));
        btnNext = new Button(objDictionaryAction.getWord(">"));
        btnPrev.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if(currentPage>0){
                    currentPage--;
                    updateCurrentKeyValueMap();
                    populateLibraryLanguage(currentLanguage);
                }
            }
        });
        btnNext.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if(currentPage<maxPage){
                    currentPage++;
                    updateCurrentKeyValueMap();
                    populateLibraryLanguage(currentLanguage);
                }
            }
        });
        dictionaryGP.add(btnPrev, 0, 1);
        dictionaryGP.add(btnNext, 5, 1);
        
        Separator sepHor = new Separator();
        sepHor.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHor, 0, 2);
        GridPane.setColumnSpan(sepHor, 6);
        dictionaryGP.getChildren().add(sepHor);
    
        translatorGP = new GridPane();         
        translatorGP.setAlignment(Pos.TOP_LEFT);
        translatorGP.setHgap(2);
        translatorGP.setVgap(2);
        translatorGP.setPadding(new Insets(1, 1, 1, 1));
        dictionaryGP.add(translatorGP,0,3,6,1);
        
        keyValueHM=new HashMap();
        populateLibraryLanguage("ENGLISH");
        
        GridPane bodyContainer = new GridPane();
        bodyContainer.setPrefSize(objConfiguration.WIDTH, objConfiguration.HEIGHT);
        bodyContainer.setId("container");
        
        Label caption = new Label(objDictionaryAction.getWord("TRANSLATOR")+" "+objDictionaryAction.getWord("UTILITY"));
        caption.setId("caption");
        bodyContainer.add(caption, 0, 0, 1, 1);
        
        bodyContainer.add(tabPane, 0, 1, 1, 1);
        /*
        final Separator sepHor = new Separator();
        sepHor.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHor, 0, 2);
        GridPane.setColumnSpan(sepHor, 1);
        bodyContainer.getChildren().add(sepHor);
        */
        root.setCenter(bodyContainer);
        
        dictionaryStage.getIcons().add(new Image("/media/icon.png"));
        dictionaryStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWTRANSLATOR")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
        //dictionaryStage.setIconified(true);
        dictionaryStage.setResizable(false);
        dictionaryStage.setScene(scene);
        dictionaryStage.setX(0);
        dictionaryStage.setY(0);
        dictionaryStage.show();
        dictionaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                parentMenuAction();  
                we.consume();
            }
        });
        final KeyCodeCombination homeKCC = new KeyCodeCombination(KeyCode.M, KeyCombination.SHIFT_DOWN); // Home Menu]
        final KeyCodeCombination parentKCC = new KeyCodeCombination(KeyCode.Z, KeyCombination.SHIFT_DOWN); // Home Menu]
        scene.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                if(homeKCC.match(t)){
                    homeMenuAction();
                } else if(parentKCC.match(t)){
                    parentMenuAction();
                }
            }
        });
    }
    /**
     * homeMenuAction
     * <p>
     * Function use for help menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        DictionaryView
     */
    private void homeMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHOME"));
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.setIconified(false);
        dialogStage.setFullScreen(false);
        dialogStage.setTitle(objDictionaryAction.getWord("ALERT"));
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 300, 100, Color.WHITE);
        scene.getStylesheets().add(DictionaryView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        popup.add(new ImageView("/media/alert_warning.png"), 0, 0);  
        Label lblAlert = new Label(objDictionaryAction.getWord("ALERTCLOSE"));
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(250);
        popup.add(lblAlert, 1, 0);
        Button btnYes = new Button(objDictionaryAction.getWord("YES"));
        btnYes.setPrefWidth(50);
        btnYes.setId("btnYes");
        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();                
                dictionaryStage.close();
                System.gc();
                WindowView objWindoeView = new WindowView(objConfiguration);
            }
        });
        popup.add(btnYes, 0, 1);
        Button btnNo = new Button(objDictionaryAction.getWord("NO"));
        btnNo.setPrefWidth(50);
        btnNo.setId("btnNo");
        btnNo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
            }
        });
        popup.add(btnNo, 1, 1);
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.showAndWait(); 
    }
    /**
     * parentMenuAction
     * <p>
     * Function use for help menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        DictionaryView
     */
    private void parentMenuAction(){
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.setIconified(false);
        dialogStage.setFullScreen(false);
        dialogStage.setTitle(objDictionaryAction.getWord("ALERT"));
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 300, 100, Color.WHITE);
        scene.getStylesheets().add(DictionaryView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        final GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        popup.add(new ImageView("/media/alert_warning.png"), 0, 0);
        Label lblAlert = new Label(objDictionaryAction.getWord("ALERTCLOSE"));
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(250);
        popup.add(lblAlert, 1, 0);
        Button btnYes = new Button(objDictionaryAction.getWord("YES"));
        btnYes.setPrefWidth(50);
        btnYes.setId("btnYes");
        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();                
                dictionaryStage.close();
                System.gc();
                UtilityView objUtilityView = new UtilityView(objConfiguration);                
            }
        });
        popup.add(btnYes, 0, 1);
        Button btnNo = new Button(objDictionaryAction.getWord("NO"));
        btnNo.setPrefWidth(50);
        btnNo.setId("btnNo");
        btnNo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
            }
        });
        popup.add(btnNo, 1, 1);
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.showAndWait();
    }
    /**
     * helpMenuAction
     * <p>
     * Function use for help menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        DictionaryView
     */
    private void helpMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHELP"));
        HelpView objHelpView = new HelpView(objConfiguration);
    }
    /**
     * technicalMenuAction
     * <p>
     * Function use for technical menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        DictionaryView
     */
    private void technicalMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONTECHNICAL"));
        TechnicalView objTechnicalView = new TechnicalView(objConfiguration);
    }
    /**
     * aboutMenuAction
     * <p>
     * Function use for about menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        DictionaryView
     */
    private void aboutMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONABOUTUS"));
        AboutView objAboutView = new AboutView(objConfiguration);
    }
    /**
     * contactMenuAction
     * <p>
     * Function use for contact menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        DictionaryView
     */
    private void contactMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCONTACTUS"));
        ContactView objContactView = new ContactView(objConfiguration);
    }
    /**
     * logoutMenuAction
     * <p>
     * Function use for exit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void logoutMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHOME"));
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.setIconified(false);
        dialogStage.setFullScreen(false);
        dialogStage.setTitle(objDictionaryAction.getWord("ALERT"));
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 300, 100, Color.WHITE);
        scene.getStylesheets().add(DictionaryView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        popup.add(new ImageView("/media/alert_warning.png"), 0, 0);  
        Label lblAlert = new Label(objDictionaryAction.getWord("ALERTCLOSE"));
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(250);
        popup.add(lblAlert, 1, 0);
        Button btnYes = new Button(objDictionaryAction.getWord("YES"));
        btnYes.setPrefWidth(50);
        btnYes.setId("btnYes");
        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                objConfiguration.setStrRecentWeave(null);
                dialogStage.close();
                dictionaryStage.close();
                System.gc();
                UserLoginView objUserLoginView = new UserLoginView(objConfiguration);
            }
        });
        popup.add(btnYes, 0, 1);
        Button btnNo = new Button(objDictionaryAction.getWord("NO"));
        btnNo.setPrefWidth(50);
        btnNo.setId("btnNo");
        btnNo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
            }
        });
        popup.add(btnNo, 1, 1);
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.showAndWait();
    }
    /**
     * exitMenuAction
     * <p>
     * Function use for exit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        DictionaryView
     */
    private void exitMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONEXIT"));
        System.gc();
        dictionaryStage.close();
    }
    private void newLanguageLibrary(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONNEWFILE"));
        try{
            isNew=true;
            newLanguage=true;
            translatorGP.getChildren().clear();
            List lstLanguage=null;
            List lstLanguageDeatails = new ArrayList();
            lstLanguageDeatails = new DictionaryAction().lstImportLanguage("ENGLISH");
            if(lstLanguageDeatails.size()==0){
                translatorGP.getChildren().add(new Text(objDictionaryAction.getWord("LANGUAGE")+" - "+objDictionaryAction.getWord("NOVALUE")));
            }else{            
                for (int i=0, k=0; i<lstLanguageDeatails.size();i++, k+=2){
                    lstLanguage = (ArrayList)lstLanguageDeatails.get(i);
                    
                    Label lbl_key=new Label(lstLanguage.get(0).toString());
                    lbl_key.setTextFill(Color.BLUE);
                    lbl_key.setPrefWidth(200);

                    Label lbl_unikey=new Label(objDictionaryAction.getWord(lstLanguage.get(0).toString()));
                    lbl_unikey.setFont(Font.font("Tahoma", FontWeight.NORMAL, 11));
                    lbl_unikey.setTextFill(Color.MAGENTA);
                    lbl_unikey.setPrefWidth(200);

                    TextField txt_key=new TextField("");
                    txt_key.setPrefWidth(200);
                    translatorGP.add(lbl_key, 1, i+1);
                    translatorGP.add(txt_key, 2, i+1);
                    translatorGP.add(lbl_unikey, 3, i+1);
                }
            }
        } catch (Exception ex) {
            new Logging("SEVERE",DictionaryView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void openLanguageLibrary(){     
        lblStatus.setText(objDictionaryAction.getWord("ACTIONOPENFILE"));
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.setIconified(false);
        dialogStage.setFullScreen(false);
        dialogStage.setTitle(objDictionaryAction.getWord("ALERT"));
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 300, 100, Color.WHITE);
        scene.getStylesheets().add(DictionaryView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        
        popup.add(new Text(objDictionaryAction.getWord("LANGUAGE")+" "+objDictionaryAction.getWord("NAME")), 0, 0);  
        
        final ComboBox languageCB = new ComboBox();
        try{
            InputStream resourceAsStream = new FileInputStream(System.getProperty("user.dir")+"/mla/language/langs.properties"); //DictionaryView.class.getClassLoader().getResourceAsStream("language/langs.properties");;
            Properties properties=new Properties();
            if (resourceAsStream != null) {
                properties.load(resourceAsStream);
                languageCB.getItems().clear();
                for(Object k:properties.keySet()){
                    String langName=(String)k;
                    languageCB.getItems().add(langName);
                }
            }
        }catch(Exception ex){
            languageCB.getItems().clear();
            languageCB.getItems().addAll("HINDI","ENGLISH");
        }
        languageCB.setPromptText(objDictionaryAction.getWord("SEARCHBY"));
        languageCB.setEditable(false); 
        currentLanguage="ENGLISH";
        languageCB.setValue("ENGLISH"); 
        languageCB.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String oldValue, String newValue) {
                newLanguage=false;
                currentLanguage=newValue;
                languageLoadedFirstTime=true;
                populateLibraryLanguage(newValue);
            }    
        });
        popup.add(languageCB, 1, 0);
        
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("SELECTTO")+" "+objDictionaryAction.getWord("RUN"));
        dialogStage.showAndWait();
        System.gc();
    }
    private void saveLanguageLibrary(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONSAVEFILE"));
        if(!isNew){
            Properties properties=new Properties();
            /*String key="", value="";
            for(int a=0; a<translatorGP.getChildren().size(); a++){
                if(translatorGP.getChildren().get(a) instanceof TextField){
                    value=((TextField)translatorGP.getChildren().get(a)).getText();
                    if(value.trim().length()!=0){
                        key=((Label)translatorGP.getChildren().get(a-1)).getText();
                    }
                    else{
                        key=((Label)translatorGP.getChildren().get(a-1)).getText();
                        value=objDictionaryAction.getWord(key);
                    }
                    properties.setProperty(key, value);
                }
            }*/
            updateCurrentKeyValueMap();
            try{
                Object[] o=keyValueHM.keySet().toArray();
                for(int a=0; a<o.length; a++){
                    properties.setProperty(o[a].toString(), (String) keyValueHM.get(o[a].toString()));
                }
                File f=new File(System.getProperty("user.dir")+"/mla/language/"+currentLanguage+".properties");
                properties.store(new FileOutputStream(f), null);
            }catch(Exception ex){
                new Logging("SEVERE",DictionaryView.class.getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        }else{
            saveAsLanguageLibrary();
        }
    }
    
    private void saveAsLanguageLibrary(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONSAVEASFILE"));
        // popup enter new language name
        final Stage popup=new Stage();
        popup.setTitle("Save Language");
        GridPane popupGP=new GridPane();
        popupGP.setAlignment(Pos.CENTER);
        popupGP.setHgap(5);
        popupGP.setVgap(10);
        Label lblLang=new Label("Enter Language Name");
        Button btnOK=new Button("Save");
        Button btnCancel=new Button("Cancel");
        final TextField txtLang=new TextField("");
        popupGP.add(lblLang, 0, 0, 2, 1);
        popupGP.add(txtLang, 0, 1, 2, 1);
        popupGP.add(btnOK, 0, 2, 1, 1);
        popupGP.add(btnCancel, 1, 2, 1, 1);
        final Properties properties=new Properties();
        btnOK.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(txtLang.getText().length()==0)
                    return;
                /*String key="", value="";
                for(int a=0; a<translatorGP.getChildren().size(); a++){
                    if(translatorGP.getChildren().get(a) instanceof TextField){
                        value=((TextField)translatorGP.getChildren().get(a)).getText();
                        if(value.trim().length()!=0){
                            key=((Label)translatorGP.getChildren().get(a-1)).getText();
                        }
                        else{
                            key=((Label)translatorGP.getChildren().get(a-1)).getText();
                            value=objDictionaryAction.getWord(key);
                        }
                        properties.setProperty(key, value);
                    }
                }*/
                updateCurrentKeyValueMap();
                try{
                    Object[] o=keyValueHM.keySet().toArray();
                    for(int a=0; a<o.length; a++){
                        properties.setProperty(o[a].toString(), (String) keyValueHM.get(o[a].toString()));
                    }
                    File f=new File(System.getProperty("user.dir")+"/mla/language/"+txtLang.getText().toUpperCase()+".properties");
                    f.createNewFile();
                    //properties.store(new FileOutputStream(DictionaryView.class.getClassLoader().getResource("language/"+txtLang.getText()+".properties").getPath()), null);
                    properties.store(new FileOutputStream(f), null);
                    Files.write(Paths.get(System.getProperty("user.dir")+"/mla/language/langs.properties"), ("\n"+txtLang.getText().toUpperCase()).getBytes(), StandardOpenOption.APPEND);
                    newLanguage=false;
                    popup.close();
                }catch(Exception ex){
                    new Logging("SEVERE",DictionaryView.class.getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                    popup.close();
                }
            }
        });
        btnCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                popup.close();
            }
        });
        Scene scene=new Scene(popupGP, 300, 200);
        popup.setScene(scene);
        popup.showAndWait();
    }
    
    private void updateCurrentKeyValueMap(){
        String key="", value="";
        for(int a=0; a<translatorGP.getChildren().size(); a++){
            if(translatorGP.getChildren().get(a) instanceof TextField){
                value=((TextField)translatorGP.getChildren().get(a)).getText();
                if(value.trim().length()!=0){
                    key=((Label)translatorGP.getChildren().get(a-1)).getText();
                }
                else{
                    key=((Label)translatorGP.getChildren().get(a-1)).getText();
                    value=objDictionaryAction.getWord(key);
                }
                keyValueHM.put(key, value);
            }
        }
    }
    
    private void populateLibraryLanguage(String strLanguage){
        try{
            if(strLanguage=="ENGLISH" || strLanguage=="HINDI")
                isNew = true;
            else
                isNew = false;
            translatorGP.getChildren().clear();
            List lstLanguage=null;
            List lstLanguageDeatails = new ArrayList();
            lstLanguageDeatails = new DictionaryAction().lstImportLanguage(strLanguage);
            
            maxPage=lstLanguageDeatails.size()/20;
            if(lstLanguageDeatails.size()==0){
                translatorGP.getChildren().add(new Text(objDictionaryAction.getWord("LANGUAGE")+" - "+objDictionaryAction.getWord("NOVALUE")));
            }else{
                if(keyValueHM.isEmpty()&&strLanguage=="ENGLISH"){
                    // load all default values to keyValueHM map
                    for(Object oList:lstLanguageDeatails){
                        keyValueHM.put(((ArrayList)oList).get(0).toString(), ((ArrayList)oList).get(1).toString());
                    }
                }
                if(languageLoadedFirstTime){
                    // once language changes, update key values with language specific terms
                    for(Object oList:lstLanguageDeatails){
                        keyValueHM.put(((ArrayList)oList).get(0).toString(), ((ArrayList)oList).get(1).toString());
                    }
                    languageLoadedFirstTime=false;
                }
                for (int i=(currentPage*20), k=0; i<((currentPage*20)+20) ;i++, k+=2){
                    if(i>=lstLanguageDeatails.size())
                        break;
                    lstLanguage = (ArrayList)lstLanguageDeatails.get(i);
                    
                    Label lbl_key=new Label(lstLanguage.get(0).toString());
                    lbl_key.setTextFill(Color.BLUE);
                    lbl_key.setPrefWidth(200);

                    Label lbl_unikey=new Label(objDictionaryAction.getWord(lstLanguage.get(0).toString()));
                    lbl_unikey.setFont(Font.font("Tahoma", FontWeight.NORMAL, 11));
                    lbl_unikey.setTextFill(Color.MAGENTA);
                    lbl_unikey.setPrefWidth(200);

                    TextField txt_key=new TextField();
                    // if newLanguage text field should be empty
                    if(!newLanguage)
                        txt_key.setText(lstLanguage.get(1).toString());
                    txt_key.setPrefWidth(200);
                    
                    /*char[] the_unicode_char = new char[1];
                    try {
                        the_unicode_char[0] = (char)Integer.parseInt((String)objDictionaryAction.dictionary.get(key),16);
                        System.out.println(the_unicode_char);
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                    }
                    lbl_unikey.setText(new String(the_unicode_char));
                    */
                    translatorGP.add(lbl_key, 1, i-(currentPage*20)+1);
                    translatorGP.add(txt_key, 2, i-(currentPage*20)+1);
                    translatorGP.add(lbl_unikey, 3, i-(currentPage*20)+1);
                }
            }
        } catch (Exception ex) {
            new Logging("SEVERE",DictionaryView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    @Override
    public void start(Stage stage) throws Exception {
        new DictionaryView(stage);
        new Logging("WARNING",DictionaryView.class.getName(),"UnsupportedOperationException",new UnsupportedOperationException("Not supported yet."));
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    } 
}