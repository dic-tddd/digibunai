/*
 * Copyright (C) Digital India Corporation ( Media Lab Asia )
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mla.pattern;

import com.mla.main.DbConnect;
import com.mla.main.IDGenerator;
import com.mla.main.Logging;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * PatternAction Class
 * <p>
 * This class is used for defining model methods for pattern properties.
 *
 * @author Amit Kumar Singh
 * @version %I%, %G%
 * @since   1.0
 * @date 07/01/2016
 * @Designing model method class for fabric
 * @see java.sql.*;
 * @link com.mla.main.DbConnect
 */
public class PatternAction {
    
    Connection connection = null; //DbConnect.getConnection();
    
    /**
     * PatternAction
     * <p>
     * This constructor is used for creating database connection.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   constructor is used for creating database connection.
     * @see         java.sql.*;
     * @link        com.mla.main.DbConnect
     * @throws      SQLException
     */
    public PatternAction() throws SQLException{
        connection = DbConnect.getConnection();
    }
    /**
     * Fabric Class
     * <p>
     * This constructor is used for creating database connection.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   constructor is used for creating database connection.
     * @see         java.sql.*;
     * @link        com.mla.main.DbConnect
     * @throws      SQLException
     * @param       isDB boolean <code>true</code> if need to get access database connection
     *              <code>false</code> otherwise.
     */
    public PatternAction(boolean isDB) throws SQLException{
        if(isDB)
            connection = DbConnect.getConnection();        
    }
    /**
     * Fabric Class
     * <p>
     * This constructor is used for creating database connection.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   constructor is used for creating database connection.
     * @see         java.sql.*;
     * @link        com.mla.main.DbConnect
     * @throws      SQLException
     * @param       objPatternCall Pattern object
     * @param       isDB boolean <code>true</code> if need to get access database connection
     *              <code>false</code> otherwise.
     */
    public PatternAction(Pattern objPatternCall, boolean isDB) throws SQLException{
        if(isDB)
            connection = DbConnect.getConnection();
        //objPattern = objPatternCall;
    }    
    /**
     * close
     * <p>
     * This method is used for destroying database connection.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for closing database connection.
     * @see         java.sql.*;
     * @throws      SQLException
     */
    public void close() throws SQLException{
        connection.close();
    }     
    /**
    * lstImportFabric
    * <p>
    * This method is used for accessing pattern from library based on conditions.
    *
    * @author      Amit Kumar Singh
    * @version     %I%, %G%
    * @since       1.0
    * @date        07/01/2016
    * @Designing   method is used for accessing pattern from library.
    * @see         java.sql.*;
    * @link        com.mla.main.DbConnect
    * @link        com.mla.main.Logging
    * @exception   Exception
    * @param       type byte
    * @return      lstFabricDeatails List field of fabric
    *              <code>null</code> otherwise.
    */
    public ArrayList<Pattern> getAllPattern(byte type) {
        Statement oStatement =null;
        ResultSet oResultSet= null;
        String strQuery=null, cond = "1";
        String code = null, pattern = null, access = null, user = null, date = null;
        int repeat = 0, used = 0;
        ArrayList<Pattern> lstPattern = null;  
        Pattern objPattern=null;
        try {
            System.out.println("<<<<<< getAllPattern >>>>>>");
            if(type>0) cond += " AND `type`="+type;
            strQuery = "SELECT * FROM `mla_pattern_library` WHERE "+cond+";";
            oStatement = connection.createStatement();
            oResultSet = oStatement.executeQuery(strQuery);
            lstPattern = new ArrayList<Pattern>();            
            while(oResultSet.next()) {
                code = oResultSet.getString("ID").toString();
                pattern = oResultSet.getString("PATTERN").toString();
                repeat = oResultSet.getInt("REPEAT");
                used = oResultSet.getInt("USED");
                access = oResultSet.getString("ACCESS");
                user = oResultSet.getString("USERID");
                date = oResultSet.getString("UDATE").toString();
                lstPattern.add(new Pattern(code, pattern, (int)type, repeat, used, access, user, date));
            }
            //objFabric.setLstPattern(lstPattern);
        } catch (Exception ex) {
            new Logging("SEVERE",PatternAction.class.getName(),"getAllPattern : "+strQuery,ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oStatement!=null) {
                    oStatement.close();
                    oStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",PatternAction.class.getName(),"getAllPattern() : Error while closing connection"+e,ex);
                }
            }
        }
        System.out.println("<<<<<< getAllPattern >>>>>>");
        return lstPattern;
    }
    
    public Pattern getPattern(String strPatternID) {
        Statement oStatement =null;
        ResultSet oResultSet= null;
        String strQuery=null;
        Pattern objPattern = null;
        try {           
            System.out.println("<<<<<< getPattern >>>>>>");
            strQuery = "select * from mla_pattern_library WHERE ID='"+strPatternID+"';";
            oStatement = connection.createStatement();
            oResultSet = oStatement.executeQuery(strQuery);  
            if(oResultSet.next())
                objPattern = new Pattern(oResultSet.getString("ID"),oResultSet.getString("PATTERN"),oResultSet.getInt("TYPE"),oResultSet.getInt("REPEAT"),oResultSet.getInt("USED"),oResultSet.getString("ACCESS"),oResultSet.getString("USERID"),oResultSet.getString("UDATE"));
            
        } catch (Exception ex) {
            new Logging("SEVERE",PatternAction.class.getName(),"getPattern : "+strQuery,ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oStatement!=null) {
                    oStatement.close();
                    oStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",PatternAction.class.getName(),"getPattern() : Error while closing connection"+e,ex);
                }
            }
        }
        System.out.println("<<<<<< getPattern >>>>>>");
        return objPattern;
    }
    
    public boolean setPattern(Pattern objPattern) {
        Statement oStatement =null;
        String strQuery=null;
        byte oResult = 0;
        String id = null, pattern = null;
        int repeat = 0, used = 0, type = 0;
        try {          
            id = objPattern.getStrPatternID();
            pattern = objPattern.getStrPattern();
            type = objPattern.getIntPatternType();
            repeat = objPattern.getIntPatternRepeat();
            used = objPattern.getIntPatternUsed();
            System.out.println("<<<<<< addPattern >>>>>>");
            strQuery = "INSERT INTO `mla_pattern_library` (`ID`, `TYPE`, `PATTERN`, `REPEAT`, `USED`, `ACCESS`, `USERID`) "
                    + "SELECT * FROM "
                    + "(SELECT '"+id+"' as a, '"+type+"' as b, '"+pattern+"' as c, '"+repeat+"' as d, '"+used+"' as e, '"+new IDGenerator().setUserAcessKey("PATTERN_LIBRARY",objPattern.getObjConfiguration().getObjUser())+"' as f, '"+objPattern.getObjConfiguration().getObjUser().getStrUserID()+"' as g) AS tmp WHERE NOT EXISTS "
                    + "(SELECT `type`, `pattern` FROM mla_pattern_library WHERE pattern = '"+pattern+"' AND type = '"+type+"') LIMIT 1;";
            oStatement = connection.createStatement();
            oResult = (byte)oStatement.executeUpdate(strQuery);
        } catch (Exception ex) {
            new Logging("SEVERE",PatternAction.class.getName(),"addPattern : "+strQuery,ex);
        } finally {
            try {                
                if(oStatement!=null) {
                    oStatement.close();
                    oStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",PatternAction.class.getName(),"addPattern() : Error while closing connection"+e,ex);
                }
            }
        }
        System.out.println("<<<<<< addPattern >>>>>>");
        if(oResult==0)
            return false;
        else
            return true;
    }
    
    public String getPatternAvibilityID(String strPattern, int type) {
        Statement oStatement =null;
        ResultSet oResultSet= null;
        String strQuery=null;
        String strPatternID = null;
        try {           
            System.out.println("<<<<<< getPatternID >>>>>>");
            strQuery = "SELECT ID FROM mla_pattern_library WHERE PATTERN='"+strPattern+"' AND TYPE='"+type+"';";
            oStatement = connection.createStatement();
            oResultSet = oStatement.executeQuery(strQuery);  
            if(oResultSet.next())
                strPatternID = oResultSet.getString("ID");
        } catch (Exception ex) {
            new Logging("SEVERE",PatternAction.class.getName(),"getPatternID : "+strQuery,ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oStatement!=null) {
                    oStatement.close();
                    oStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",PatternAction.class.getName(),"getPatternID() : Error while closing connection"+e,ex);
                }
            }
        }
        System.out.println("<<<<<< getPatternID >>>>>>"+strPatternID);
        return strPatternID;
    }
}