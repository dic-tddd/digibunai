/*
 * Copyright (C) 2017 Media Lab Asia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.mla.cloth;

import com.mla.fabric.Fabric;
import com.mla.main.DbConnect;
import com.mla.main.IDGenerator;
import com.mla.main.Logging;
import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Amit Kumar Singh
 */
public class ClothAction {
    Connection connection = null;
    
    public ClothAction() throws SQLException{
        connection = DbConnect.getConnection();
    }
    
    /**
     * countClothAccess
     * <p>
     * This method is used for counting access in Cloth.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for counting access in Cloth.
     * @see         java.sql.*;
     * @link        com.mla.main.DbConnect
     * @link        com.mla.main.Logging
     * @exception   Exception
     * @return      countAccess [MAP<STRING,Integer>] return total number of items in each access group 
     *              <code>null</code> otherwise.
     */
    public Map countClothAccess(){
        Statement oStatement =null;
        ResultSet oResultSet= null;
        String strQuery=null;
        Map countAccess = null;
        new Logging("INFO",this.getClass().getName(),"<<<<<<<<<<< countClothAccess() >>>>>>>>>>>",null);
        try {           
            strQuery = "SELECT ACCESS, COUNT(*) AS COUNTING from mla_cloth_library GROUP BY ACCESS;";
            oStatement = connection.createStatement();
            oResultSet = oStatement.executeQuery(strQuery);
            countAccess = new TreeMap();
            while(oResultSet.next()) {
                countAccess.put(new IDGenerator().getUserAcessValueData("CLOTH_LIBRARY",oResultSet.getString("ACCESS")), oResultSet.getInt("COUNTING"));
            }            
        } catch (Exception ex) {
            new Logging("SEVERE",this.getClass().getName(),"countClothAccess() : "+strQuery,ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oStatement!=null) {
                    oStatement.close();
                    oStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",this.getClass().getName(),"countClothAccess() : Error while closing connection"+e,ex);
                }
            }
        }
        new Logging("INFO",this.getClass().getName(),">>>>>>>>>>> countClothAccess() <<<<<<<<<<<"+countAccess,null);
        return countAccess;
    }
    /**
     * clearCloth
     * <p>
     * This method is used for deleting cloth from library.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for deleting cloth from library.
     * @see         java.sql.*;
     * @link        com.mla.main.DbConnect
     * @link        com.mla.main.Logging
     * @exception   Exception
     * @param       strClothID String Cloth unique id
     * @return      oResult boolean field <code>true</code> if the cloth is removed 
     *              <code>false</code> otherwise.
     */
    public boolean clearCloth(String strClothID) {
        PreparedStatement oPreparedStatement =null;
        ResultSet oResultSet= null;
        boolean oResult= false;
        String strQuery=null;
        new Logging("INFO",ClothAction.class.getName(),"<<<<<<<<<<< clearCloth() >>>>>>>>>>>",null);
        try {           
            strQuery = "DELETE FROM mla_cloth_library WHERE `ID` = ?;";
            oPreparedStatement = connection.prepareStatement(strQuery);
            oPreparedStatement.setString(1, strClothID);
            oResult = oPreparedStatement.execute();           
        } catch (Exception ex) {
            new Logging("SEVERE",ClothAction.class.getName(),"clearCloth() : "+strQuery,ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oPreparedStatement!=null) {
                    oPreparedStatement.close();
                    oPreparedStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",ClothAction.class.getName(),"clearCloth() : Error while closing connection"+e,ex);
                }
            }
        }
        new Logging("INFO",ClothAction.class.getName(),">>>>>>>>>>> clearCloth() <<<<<<<<<<<"+oResult,null);
        return oResult;
    }
    public boolean clearClothFabric(String strClothID) {
        PreparedStatement oPreparedStatement =null;
        ResultSet oResultSet= null;
        boolean oResult= false;
        String strQuery=null;
        new Logging("INFO",ClothAction.class.getName(),"<<<<<<<<<<< clearClothFabric() >>>>>>>>>>>",null);
        try {           
            strQuery = "DELETE FROM `mla_cloth_fabric` WHERE `CLOTHID` = ?;";
            oPreparedStatement = connection.prepareStatement(strQuery);
            oPreparedStatement.setString(1, strClothID);
            oResult = oPreparedStatement.execute();
        } catch (Exception ex) {
            new Logging("SEVERE",ClothAction.class.getName(),"clearClothFabric : "+strQuery,ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oPreparedStatement!=null) {
                    oPreparedStatement.close();
                    oPreparedStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",ClothAction.class.getName(),"clearClothFabric() : Error while closing connection"+e,ex);
                }
            }
        }
        new Logging("INFO",ClothAction.class.getName(),">>>>>>>>>>> clearClothFabric() <<<<<<<<<<<"+oResult,null);
        return oResult;
    }
    /**
    * lstImportCloth
    * <p>
    * This method is used for accessing cloth from library based on conditions.
    *
    * @author      Amit Kumar Singh
    * @version     %I%, %G%
    * @since       1.0
    * @date        07/01/2016
    * @Designing   method is used for accessing cloth from library.
    * @see         java.sql.*;
    * @link        com.mla.main.DbConnect
    * @link        com.mla.main.Logging
    * @exception   Exception
    * @param       objCloth Object Cloth
    * @return      lstClothDeatails List field of cloth
    *              <code>null</code> otherwise.
    */
    public List lstImportCloth(Cloth objCloth) {
        Statement oStatement =null;
        ResultSet oResultSet= null;
        String strQuery=null;
        List lstClothDeatails=null, lstCloth;
        new Logging("INFO",ClothAction.class.getName(),">>>>>>>>>>> lstImportCloth()",null);            
        try {           
            String cond =  "1 ";
            String orderBy ="NAME ";
            if(!objCloth.getObjConfiguration().getObjUser().getStrUserID().equals("ADMIN")){
                if(objCloth.getStrSearchAccess().trim().equalsIgnoreCase("Public")) {
                    cond += "AND `ACCESS`='"+new IDGenerator().setUserAcessValueData("CLOTH_LIBRARY",objCloth.getStrSearchAccess())+"'";
                }else if(objCloth.getStrSearchAccess().trim().equalsIgnoreCase("Protected")) {
                    cond += "AND (USERID = '"+objCloth.getObjConfiguration().getObjUser().getStrUserID()+"' AND `ACCESS`='"+new IDGenerator().setUserAcessValueData("CLOTH_LIBRARY",objCloth.getStrSearchAccess())+"') ";
                }else if(objCloth.getStrSearchAccess().trim().equalsIgnoreCase("Private")) {
                    cond += "AND (USERID = '"+objCloth.getObjConfiguration().getObjUser().getStrUserID()+"' AND `ACCESS`='"+new IDGenerator().setUserAcessValueData("CLOTH_LIBRARY",objCloth.getStrSearchAccess())+"') ";
                }else{
                    cond += "AND (USERID = '"+objCloth.getObjConfiguration().getObjUser().getStrUserID()+"' OR `ACCESS`='"+new IDGenerator().getUserAcess("CLOTH_LIBRARY")+"') ";
                }
            }   
            if(!(objCloth.getStrCondition().trim().equals(""))) {
                cond += " AND NAME  LIKE '%"+objCloth.getStrCondition().trim()+"%'";
            }
            if(!(objCloth.getStrSearchBy().trim().equals("")) && !(objCloth.getStrSearchBy().trim().equalsIgnoreCase("All"))) {
                cond += " AND `GARMENTTYPE` LIKE '"+objCloth.getStrSearchBy().trim()+"%'";
            }
            if(objCloth.getStrOrderBy().equals("Name")) {
                orderBy = "NAME ";
            } else if(objCloth.getStrOrderBy().equals("Date")) {
                orderBy = "UDATE";
            }
            if(objCloth.getStrDirection().equals("Ascending")) {
                orderBy += " ASC";
            } else if(objCloth.getStrDirection().equals("Descending")) {
                orderBy += " DESC";
            }
            strQuery = "select * from mla_cloth_library WHERE "+cond+" ORDER BY "+orderBy;
    
            if(objCloth.getStrLimit()!=null)
                strQuery+=" LIMIT "+objCloth.getStrLimit();
            oStatement = connection.createStatement();
            oResultSet = oStatement.executeQuery(strQuery);    
            lstClothDeatails = new ArrayList();            
            while(oResultSet.next()) {
                lstCloth = new ArrayList();                
                lstCloth.add(oResultSet.getString("ID"));
                lstCloth.add(oResultSet.getString("NAME"));
                lstCloth.add(oResultSet.getString("GARMENTTYPE"));
                lstCloth.add(oResultSet.getString("DESCRIPTION"));
                lstCloth.add(oResultSet.getBytes("SNAPSHOT"));
                lstCloth.add(oResultSet.getString("REGION"));
                lstCloth.add(new IDGenerator().getUserAcessValueData("CLOTH_LIBRARY",oResultSet.getString("ACCESS")));
                lstCloth.add(oResultSet.getString("USERID"));
                lstCloth.add(oResultSet.getTimestamp("UDATE"));
                lstClothDeatails.add(lstCloth);
            }
        } catch (Exception ex) {
            new Logging("SEVERE",ClothAction.class.getName(),"lstImportCloth : "+strQuery,ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oStatement!=null) {
                    oStatement.close();
                    oStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",ClothAction.class.getName(),"lstImportCloth() : Error while closing connection"+e,ex);
                }
            }
        }
        new Logging("INFO",ClothAction.class.getName(),">>>>>>>>>>> lstImportCloth() <<<<<<<<<<<"+strQuery,null);
        return lstClothDeatails;
    }
    
    public void getCloth(Cloth objCloth) {
        Statement oStatement =null;
        ResultSet oResultSet= null;
        ResultSet resultSet=null;
        String strQuery=null;
        new Logging("INFO",ClothAction.class.getName(),">>>>>>>>>>> getCloth()",null);
        try {           
            String cond = " 1";
            if(objCloth.getStrClothId()!="" && objCloth.getStrClothId()!=null)
                cond += " AND ID='"+objCloth.getStrClothId()+"'";
            strQuery = "select * from mla_cloth_library WHERE "+cond+" LIMIT 1;";
            oStatement = connection.createStatement();
            oResultSet = oStatement.executeQuery(strQuery);  
            if(oResultSet.next()){
                objCloth.setStrClothId(oResultSet.getString("ID"));
                objCloth.setStrClothName(oResultSet.getString("NAME"));
                objCloth.setStrClothType(oResultSet.getString("GARMENTTYPE"));
                objCloth.setStrClothDescription(oResultSet.getString("DESCRIPTION"));
                objCloth.setBytClothIcon(oResultSet.getBytes("SNAPSHOT"));
                objCloth.setStrClothRegion(oResultSet.getString("REGION"));
                objCloth.setStrClothAccess(new IDGenerator().getUserAcessValueData("CLOTH_LIBRARY",oResultSet.getString("ACCESS")));
                objCloth.setStrClothDate(oResultSet.getTimestamp("UDATE").toString());
                new Logging("INFO",ClothAction.class.getName(),"getCloth(): entry fetched from library for "+objCloth.getStrClothName(),null);
                strQuery = "select * from `mla_cloth_fabric` WHERE `CLOTHID`='"+oResultSet.getString("ID").toString()+"';";
                oStatement = connection.createStatement();
                resultSet = oStatement.executeQuery(strQuery);
                while(resultSet.next()){
                    if(resultSet.getString("FABRICID")!=null){
                        Fabric objFabric = new Fabric();
                        objFabric.setObjConfiguration(objCloth.getObjConfiguration());
                        objFabric.setStrFabricID(resultSet.getString("FABRICID").toString());
                        objCloth.mapClothFabric.put(resultSet.getString("CLOTHTYPE").toString(), objFabric);
                        //MAINFABRICID, ROTATIONANGLE, REPEATMODE, FABRICREPEAT, WIDTH, HEIGHT
                        String[] clothFabric = new String[6];
                        clothFabric[0]=resultSet.getString("MAINFABRICID");
                        clothFabric[1]=String.valueOf(resultSet.getInt("ROTATIONANGLE"));
                        clothFabric[2]=resultSet.getString("REPEATMODE");
                        clothFabric[3]=String.valueOf(resultSet.getInt("FABRICREPEAT"));
                        clothFabric[4]=String.valueOf(resultSet.getInt("ENDS"));
                        clothFabric[5]=String.valueOf(resultSet.getInt("PICKS"));
                        objCloth.mapClothFabricDetails.put(resultSet.getString("CLOTHTYPE").toString(), clothFabric);
                    }else{
                        objCloth.mapClothFabric.put(resultSet.getString("CLOTHTYPE").toString(), null);
                        objCloth.mapClothFabricDetails.put(resultSet.getString("CLOTHTYPE").toString(),  new String[]{"","90","Rectangular (Default)","1","0","0"});
                    }                    
                    new Logging("INFO",ClothAction.class.getName(),"getCloth(): entry fetched from cloth fabric for "+resultSet.getString("CLOTHTYPE").toString(),null);
                }
            }
        } catch (Exception ex) {
            new Logging("SEVERE",ClothAction.class.getName(),"getCloth: error while fetching from library caused by "+ex.getCause(),ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oStatement!=null) {
                    oStatement.close();
                    oStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",ClothAction.class.getName(),"getCloth() : error while closing connection"+e,ex);
                }
            }
        }
        new Logging("INFO",ClothAction.class.getName(),"getCloth() >>>>>>>>>>>",null);
        return;
    }
    
    public byte setCloth(Cloth objCloth) {
        PreparedStatement oPreparedStatement =null;
        ResultSet oResultSet= null;
        byte oResult= 0;
        String strQuery=null;
        new Logging("INFO",ClothAction.class.getName(),">>>>>>>>>>> setCloth()",null);
        try {           
            strQuery = "INSERT INTO `mla_cloth_library` (`ID`, `NAME`, `GARMENTTYPE`, `DESCRIPTION`, `SNAPSHOT`, `REGION`, `ACCESS`, `USERID`) VALUES (?,?,?,?,?,?,?,?);";
            oPreparedStatement = connection.prepareStatement(strQuery);
            oPreparedStatement.setString(1, objCloth.getStrClothId());
            oPreparedStatement.setString(2, objCloth.getStrClothName());
            oPreparedStatement.setString(3, objCloth.getStrClothType());
            oPreparedStatement.setString(4, objCloth.getStrClothDescription());
            oPreparedStatement.setBinaryStream(5,new ByteArrayInputStream(objCloth.getBytClothIcon()),objCloth.getBytClothIcon().length);
            oPreparedStatement.setString(6, objCloth.getStrClothRegion());
            oPreparedStatement.setString(7, new IDGenerator().setUserAcessValueData("CLOTH_LIBRARY",objCloth.getStrClothAccess()));
            oPreparedStatement.setString(8, objCloth.getObjConfiguration().getObjUser().getStrUserID());
            oResult = (byte)oPreparedStatement.executeUpdate();
        } catch (Exception ex) {
            new Logging("SEVERE",ClothAction.class.getName(),"setCloth: error while inserting into library caused by "+ex.getCause(),ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oPreparedStatement!=null) {
                    oPreparedStatement.close();
                    oPreparedStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",ClothAction.class.getName(),"setCloth(): error while closing connection"+e,ex);
                }
            }
        }
        new Logging("INFO",ClothAction.class.getName(),"setCloth() >>>>>>>>>>>",null);
        return oResult;
    }
    
    public byte resetCloth(Cloth objCloth) {
        PreparedStatement oPreparedStatement =null;
        ResultSet oResultSet= null;
        byte oResult= 0;
        String strQuery=null;
        new Logging("INFO",ClothAction.class.getName(),"<<<<<<<<<<< resetCloth() >>>>>>>>>>>",null);
        try {
            strQuery = "UPDATE `mla_cloth_library` SET `NAME` = ?, `GARMENTTYPE` = ?, `DESCRIPTION` = ?, `SNAPSHOT` = ?, `REGION` = ?, `ACCESS` = ? WHERE `ID`= ?;";
            oPreparedStatement = connection.prepareStatement(strQuery);
            oPreparedStatement.setString(1, objCloth.getStrClothName());
            oPreparedStatement.setString(2, objCloth.getStrClothType());
            oPreparedStatement.setString(3, objCloth.getStrClothDescription());
            oPreparedStatement.setBinaryStream(4,new ByteArrayInputStream(objCloth.getBytClothIcon()),objCloth.getBytClothIcon().length);
            oPreparedStatement.setString(5, objCloth.getStrClothRegion());
            oPreparedStatement.setString(6, new IDGenerator().setUserAcessValueData("CLOTH_LIBRARY",objCloth.getStrClothAccess()));
            oPreparedStatement.setString(7, objCloth.getStrClothId());
            oResult = (byte)oPreparedStatement.executeUpdate();              
        } catch (Exception ex) {
            new Logging("SEVERE",ClothAction.class.getName(),"resetCloth : "+strQuery,ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oPreparedStatement!=null) {
                    oPreparedStatement.close();
                    oPreparedStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",ClothAction.class.getName(),"resetCloth() : Error while closing connection"+e,ex);
                }
            }
        }
        new Logging("INFO",ClothAction.class.getName(),">>>>>>>>>>> resetCloth() <<<<<<<<<<<"+strQuery,null);
        return oResult;
    }
    
    public byte resetClothPermission(String strClothID, String strAccess) {
        PreparedStatement oPreparedStatement =null;
        ResultSet oResultSet= null;
        byte oResult= 0;
        String strQuery=null;
        new Logging("INFO",ClothAction.class.getName(),"<<<<<<<<<<< resetClothPermission() >>>>>>>>>>>",null);
        try {
            strQuery = "UPDATE `mla_cloth_library` SET `ACCESS` = ? WHERE `ID`= ?;";
            oPreparedStatement = connection.prepareStatement(strQuery);
            oPreparedStatement.setString(1, strAccess);
            oPreparedStatement.setString(2, strClothID);
            oResult = (byte)oPreparedStatement.executeUpdate();              
        } catch (Exception ex) {
            new Logging("SEVERE",ClothAction.class.getName(),"resetClothPermission : "+strQuery,ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oPreparedStatement!=null) {
                    oPreparedStatement.close();
                    oPreparedStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",ClothAction.class.getName(),"resetClothPermission() : Error while closing connection"+e,ex);
                }
            }
        }
        new Logging("INFO",ClothAction.class.getName(),">>>>>>>>>>> resetClothPermission() <<<<<<<<<<<"+strQuery,null);
        return oResult;
    }
    
    public byte setClothFabric(String strClothID, String strClothType, String strFabricID, String[] fabricDetails){
        PreparedStatement oPreparedStatement =null;
        ResultSet oResultSet= null;
        byte oResult= 0;
        String strQuery=null;
        new Logging("INFO",ClothAction.class.getName(),">>>>>>>>>>> setClothFabric()",null);
        try {    
            strQuery = "INSERT INTO `mla_cloth_fabric` (`CLOTHID`, `CLOTHTYPE`, `FABRICID`, `MAINFABRICID`, `ROTATIONANGLE`, `REPEATMODE`, `FABRICREPEAT`, `ENDS`, `PICKS`) VALUES (?,?,?,?,?,?,?,?,?);";
            oPreparedStatement = connection.prepareStatement(strQuery);
            oPreparedStatement.setString(1, strClothID);
            oPreparedStatement.setString(2, strClothType);
            oPreparedStatement.setString(3, strFabricID);
            //MAINFABRICID, ROTATIONANGLE, REPEATMODE, FABRICREPEAT, ENDS, PICKS
            oPreparedStatement.setString(4, fabricDetails[0]);
            oPreparedStatement.setInt(5, Integer.parseInt(fabricDetails[1]));
            oPreparedStatement.setString(6, fabricDetails[2]);
            oPreparedStatement.setInt(7, Integer.parseInt(fabricDetails[3]));
            oPreparedStatement.setInt(8, Integer.parseInt(fabricDetails[4]));
            oPreparedStatement.setInt(9, Integer.parseInt(fabricDetails[5]));
            oResult = (byte)oPreparedStatement.executeUpdate();
        } catch (Exception ex) {
            new Logging("SEVERE",ClothAction.class.getName(),"setClothFabric: error while inserting into library caused by "+ex.getCause(),ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oPreparedStatement!=null) {
                    oPreparedStatement.close();
                    oPreparedStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",ClothAction.class.getName(),"setClothFabric(): error while closing connection"+e,ex);
                }
            }
        }
        new Logging("INFO",ClothAction.class.getName(),"setCloth() >>>>>>>>>>>",null);
        return oResult;
    }
    
    
}