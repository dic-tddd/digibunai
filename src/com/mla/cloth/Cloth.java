/*
 * Copyright (C) 2017 HP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.mla.cloth;

import com.mla.fabric.Fabric;
import com.mla.main.Configuration;
import java.util.HashMap;
import java.util.Map;
/**
 * Cloth Class
 * <p>
 * This class is used for defining accessor and mutator methods for cloth.
 *
 * @author Aatif Ahmad Khan
 * @added 27 Oct 2017
 */
public class Cloth {
    
    private Configuration objConfiguration;
    
    private String strSearchBy;
    private String strCondition;
    private String strSearchAccess;
    private String strOrderBy;
    private String strDirection;
    private String strLimit;
    
    private String strClothId;
    private String strClothName;
    private String strClothType;
    private String strClothDescription;
    private byte[] bytClothIcon;
    private String strClothRegion;
    private String strClothAccess;
    private String strClothDate;
    

    //public Map<String,Integer> intRepeatClothFabric=new HashMap<String,Integer>();;
    
    public double zoomFactor;
    public int inchFactor;
    public Map<String,Fabric> mapClothFabric=new HashMap<String,Fabric>();
    //MAINFABRICID, ROTATIONANGLE, REPEATMODE, FABRICREPEAT, ENDS, PICKS
    public Map<String,String[]> mapClothFabricDetails=new HashMap<String,String[]>();
    
    public Cloth(){
        this.inchFactor=12;
        this.zoomFactor=1.0;
        this.strClothType="Saree";
        /*
            0=Left Border
            1=Blouse
            2=Leftside Left Cross Border
            3=Left Pallu
            4=Leftside Right Cross Border
            5=Body
            6=Skirt
            7=Rightside Left Cross Border
            8=Right Pallu
            9=Rightside Right Cross Border
            10=Right Border
        */
        //  this.intRepeatClothFabric = new int[11]; // 0 means not present, 1 & more present
        //this.mapClothFabric=new HashMap<String,Fabric>();
        
        this.mapClothFabric.put("Left Border", null);
        //MAINFABRICID, ROTATIONANGLE, REPEATMODE, FABRICREPEAT, ENDS, PICKS
        this.mapClothFabricDetails.put("Left Border", new String[]{"","0","Rectangular (Default)","0","0","0"});
        this.mapClothFabric.put("Blouse", null);
        this.mapClothFabricDetails.put("Blouse", new String[]{"","0","Rectangular (Default)","0","0","0"});
        this.mapClothFabric.put("Leftside Left Cross Border", null);
        this.mapClothFabricDetails.put("Leftside Left Cross Border", new String[]{"","0","Rectangular (Default)","0","0","0"});
        this.mapClothFabric.put("Left Pallu", null);
        this.mapClothFabricDetails.put("Left Pallu", new String[]{"","0","Rectangular (Default)","0","0","0"});
        this.mapClothFabric.put("Leftside Right Cross Border", null);
        this.mapClothFabricDetails.put("Leftside Right Cross Border", new String[]{"","0","Rectangular (Default)","0","0","0"});
        this.mapClothFabric.put("Body", null);
        this.mapClothFabricDetails.put("Body", new String[]{"","0","Rectangular (Default)","0","0","0"});
        this.mapClothFabric.put("Skirt", null);
        this.mapClothFabricDetails.put("Skirt", new String[]{"","0","Rectangular (Default)","0","0","0"});
        this.mapClothFabric.put("Rightside Left Cross Border", null);
        this.mapClothFabricDetails.put("Rightside Left Cross Border", new String[]{"","0","Rectangular (Default)","0","0","0"});
        this.mapClothFabric.put("Right Pallu", null);
        this.mapClothFabricDetails.put("Right Pallu", new String[]{"","0","Rectangular (Default)","0","0","0"});
        this.mapClothFabric.put("Rightside Right Cross Border", null);
        this.mapClothFabricDetails.put("Rightside Right Cross Border", new String[]{"","0","Rectangular (Default)","0","0","0"});
        this.mapClothFabric.put("Right Border", null);
        this.mapClothFabricDetails.put("Right Border", new String[]{"","0","Rectangular (Default)","0","0","0"});
    }
    
    public void setClothInchFactor(int inchFactor) {
        this.inchFactor = inchFactor;
    }
    public int getClothInchFactor() {
        return inchFactor;
    }
    public void setClothZoomFactor(double garmentZoom) {
        this.zoomFactor = zoomFactor;
    }
    public double getClothZoomFactor() {
        return zoomFactor;
    }
    
    public void setObjConfiguration(Configuration objConfiguration) {
        this.objConfiguration = objConfiguration;
    }
    public Configuration getObjConfiguration() {
        return objConfiguration;
    }
    
    public String getStrClothId() {
        return strClothId;
    }
    public void setStrClothId(String strClothId) {
        this.strClothId=strClothId;		
    }
    
    public String getStrClothName() {
        return strClothName;
    }
    public void setStrClothName(String strClothName) {
        this.strClothName=strClothName;		
    }
    
    public String getStrClothType() {
        return strClothType;
    }
    public void setStrClothType(String strClothType) {
        this.strClothType=strClothType;		
    }
    
    public String getStrClothDescription() {
        return strClothDescription;
    }
    public void setStrClothDescription(String strClothDescription) {
        this.strClothDescription=strClothDescription;		
    }
    
    public byte[] getBytClothIcon() {
        return bytClothIcon;
    }
    public void setBytClothIcon(byte[] bytClothIcon) {
        this.bytClothIcon=bytClothIcon;		
    }
    
    public String getStrClothRegion() {
        return strClothRegion;
    }
    public void setStrClothRegion(String strClothRegion) {
        this.strClothRegion=strClothRegion;		
    }
    
    public String getStrClothAccess() {
        return strClothAccess;
    }
    public void setStrClothAccess(String strClothAccess) {
        this.strClothAccess=strClothAccess;		
    }
    
    public String getStrClothDate() {
        return strClothDate;
    }
    public void setStrClothDate(String strClothDate) {
        this.strClothDate=strClothDate;		
    }
    
    public String getStrCondition() {
        return strCondition;
    }
    public void setStrCondition(String strCondition) {
        this.strCondition=strCondition;		
    }
    public String getStrSearchBy() {
        return strSearchBy;
    }
    public void setStrSearchBy(String strSearchBy) {
        this.strSearchBy=strSearchBy;		
    }
    public String getStrSearchAccess() {
        return strSearchAccess;
    }
    public void setStrSearchAccess(String strSearchAccess) {
        this.strSearchAccess=strSearchAccess;		
    }
    public String getStrOrderBy() {
        return strOrderBy;
    }
    public void setStrOrderBy(String strOrderBy) {
        this.strOrderBy=strOrderBy;		
    }
    public String getStrDirection() {
        return strDirection;
    }
    public void setStrDirection(String strDirection) {
        this.strDirection=strDirection;		
    }
    public void setStrLimit(String strLimit){
        this.strLimit=strLimit;
    }
    public String getStrLimit(){
        return strLimit;
    }
}