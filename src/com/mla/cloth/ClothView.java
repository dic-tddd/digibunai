/*
 * Copyright (C) Digital India Corporation ( Media Lab Asia)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mla.cloth;

import com.mla.artwork.Artwork;
import com.mla.artwork.ArtworkView;
import com.mla.artwork.ArtworkAction;
import com.mla.artwork.ArtworkEditView;
import com.mla.dictionary.DictionaryAction;
import com.mla.fabric.FabricDensityView;
import com.mla.fabric.Fabric;
import com.mla.fabric.FabricAction;
import com.mla.fabric.FabricEditView;
import com.mla.fabric.FabricImportView;
import com.mla.fabric.FabricView;
import com.mla.pattern.Pattern;
import com.mla.pattern.PatternView;
import com.mla.pattern.PatternAction;
import com.mla.yarn.Yarn;
import com.mla.utility.AboutView;
import com.mla.main.Configuration;
import com.mla.utility.ContactView;
import com.mla.main.EncryptZip;
import com.mla.utility.HelpView;
import com.mla.main.IDGenerator;
import com.mla.main.Logging;
import com.mla.main.MessageView;
import com.mla.utility.TechnicalView;
import com.mla.main.UndoRedo;
import com.mla.main.WindowView;
import com.mla.print.PrintView;
import com.mla.secure.Security;
import com.mla.user.UserAction;
import com.mla.simulator.SimulatorEditView;
import com.mla.user.UserLoginView;
import com.mla.weave.Weave;
import com.mla.weave.WeaveView;
import com.mla.weave.WeaveAction;
import com.mla.weave.WeaveImportView;
import com.mla.yarn.YarnAction;
import com.mla.yarn.YarnEditView;
import com.sun.media.jai.codec.ByteArraySeekableStream;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.SeekableStream;
import static flashplayer.application.demo.SimpleFlashPlayer.newFunction;
import java.awt.BasicStroke;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import static java.awt.print.Printable.NO_SUCH_PAGE;
import static java.awt.print.Printable.PAGE_EXISTS;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javax.imageio.ImageIO;
import javax.media.jai.PlanarImage;
/**
 *
 * @Designing GUI window for cloth editor
 * @since 0.1
 * @author Amit Kumar Singh
 * @since 0.7
 * @update Aatif Ahmad Khan
 * @since 0.9.1
 * @update Amit Kumar Singh
 * 
 */
public class ClothView  extends Application {
    
    PrinterJob objPrinterJob = PrinterJob.getPrinterJob();
    javax.print.attribute.HashPrintRequestAttributeSet objPrinterAttribute = new javax.print.attribute.HashPrintRequestAttributeSet();
    
    Configuration objConfiguration = null;
    DictionaryAction objDictionaryAction = null;
    Cloth objCloth=null;
    UndoRedo objUR;
    ClothAction objClothAction = null;
    FabricAction objFabricAction = null;
    ArtworkAction objArtworkAction = null;
    PatternAction objPatternAction = null;
      
    private String selectedMenu = "File";
    
    private static Stage clothStage;
    private BorderPane root;
    private Scene scene;
    private ScrollPane container;
    private MenuButton menuSelected;
    private ToolBar toolBar;
    private MenuBar menuBar;
    private Label lblStatus;
    private ProgressBar progressB;
    private ProgressIndicator progressI;
    
    private GridPane bodyContainer;
    private Menu homeMenu;
    private Menu fileMenu;
    private Menu editMenu;
    private Menu viewMenu;
    private Menu utilityMenu;
    private Menu runMenu;
    private Menu supportMenu;
    private Label fileMenuLabel;
    private Label editMenuLabel;
    private Label viewMenuLabel;
    private Label utilityMenuLabel;
    private Label runMenuLabel;
    
    ScrollPane lefttopKoniaPane;
    ScrollPane leftbottomKoniaPane;
    ScrollPane righttopKoniaPane;
    ScrollPane rightbottomKoniaPane;
    ScrollPane leftsideLeftCrossBorderPane;
    ScrollPane leftsideRightCrossBorderPane;
    ScrollPane rightsideLeftCrossBorderPane;
    ScrollPane rightsideRightCrossBorderPane;
    ScrollPane leftBorderPane;
    ScrollPane rightBorderPane;
    ScrollPane leftPalluPane;
    ScrollPane rightPalluPane;
    ScrollPane skirtPane;
    ScrollPane blousePane;
    ScrollPane bodyPane;
    
    ImageView lefttopKonia;
    ImageView leftbottomKonia;
    ImageView righttopKonia;
    ImageView rightbottomKonia;
    ImageView leftsideLeftCrossBorder;
    ImageView leftsideRightCrossBorder;
    ImageView rightsideLeftCrossBorder;
    ImageView rightsideRightCrossBorder;
    ImageView leftBorder;
    ImageView rightBorder;
    ImageView skirt;
    ImageView blouse;
    ImageView leftPallu;
    ImageView rightPallu;
    ImageView body;
    
    // these arrays will contain current grid position of components
    byte[][] positionGP;
    // these arrays will contain current height/width of components
    int[] widths;
    int[] heights;
    // this array will contain user input #repeats of components
    int[] ends;
    int[] picks;
    int[] repeats;
    int[] rotations;
    String[] repeatMode;
    
    boolean leftBorderEnabled;
    boolean rightBorderEnabled;
    boolean leftsideLeftCrossBorderEnabled;
    boolean leftsideRightCrossBorderEnabled;
    boolean rightsideLeftCrossBorderEnabled;
    boolean rightsideRightCrossBorderEnabled;
    boolean leftPalluEnabled;
    boolean rightPalluEnabled;
    boolean blouseEnabled;
    boolean skirtEnabled;
    
    boolean borderLinkPolicy;
    boolean crossBorderLinkPolicy;
    boolean palluLinkPolicy;
    
    ArrayList<ScrollPane> upper;
    ArrayList<ScrollPane> lower;
    ArrayList<ScrollPane> right;
    ArrayList<ScrollPane> left;
    boolean drag, BorderX, BorderY;
    private boolean isPaneDragable;
    private double xInitialPane;
    private double yInitialPane;
    double xGP;
    double yGP;
    final int RESIZE_MARGIN=5;
    final int MARGIN=5;
    
    boolean locked = true;
    boolean isNew = true;
    byte plotViewActionMode = 1; //1= visulization, 2 = flip, 3 = switch
    double zoomFactor=1;
    int inchFactor=12;
    final int MINSIZE=3; // minimum size of components in pixels
    final int MAXBORDER=12*inchFactor; // maximum size of Borders, Cross Borders, Skirt (in pixels)
    final int MAXPALLU=48*inchFactor; // maximum size of Pallu(s), Blouse (in pixels)
    
    public ClothView(final Stage primaryStage) { }
  
    /**
    * ClothView(Configuration)
    * <p>
    * This class is used for prompting cloth editor. 
    * 
    * @param       Configuration objConfigurationCall
    * @author      Amit Kumar Singh
    * @version     %I%, %G%
    * @since       1.0
    * @see         javafx.event.*;
    * @link        WindowView
    */
    public ClothView(Configuration objConfigurationCall) {
        objConfiguration = objConfigurationCall;
        objDictionaryAction = new DictionaryAction(objConfiguration);
        objUR = new UndoRedo();
        
        objCloth= objConfiguration.getObjRecentCloth();
        initCloth();
        
        clothStage = new Stage();    
        root = new BorderPane();
        scene = new Scene(root, objConfiguration.WIDTH, objConfiguration.HEIGHT, Color.WHITE);
        scene.getStylesheets().add(ClothView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        resolutionControl();
        
        HBox footContainer = new HBox();
        progressB = new ProgressBar(0);
        progressB.setVisible(false);
        progressI = new ProgressIndicator(0);
        progressI.setVisible(false);
        lblStatus = new Label(objDictionaryAction.getWord("WELCOMETOCADTOOL"));
        lblStatus.setId("message");
        footContainer.getChildren().addAll(lblStatus,progressB,progressI);
        footContainer.setId("footContainer");
        root.setBottom(footContainer);
        
        VBox topContainer = new VBox(); 
        menuBar = new MenuBar();
        menuBar.prefWidthProperty().bind(clothStage.widthProperty());
        
        toolBar = new ToolBar();
        //toolBar.setMinHeight(45);
        menuSelected = new MenuButton();
        menuSelected.setGraphic(new ImageView("/media/text_menu.png"));
        menuSelected.setAlignment(Pos.CENTER_RIGHT);
        menuSelected.setPickOnBounds(false);
        //menuSelected.setStyle("-fx-background-color: rgba(0,0,0,0);"); 
        
        AnchorPane toolAnchor = new AnchorPane();
        toolAnchor.getChildren().addAll(menuSelected);
        AnchorPane.setRightAnchor(menuSelected, 1.0);
        toolAnchor.setPickOnBounds(false);
        
        StackPane toolPane=new StackPane();
        toolPane.getChildren().addAll(toolBar,toolAnchor);
        
        topContainer.getChildren().add(menuBar);
        topContainer.getChildren().add(toolPane); 
        topContainer.setId("topContainer");
        root.setTop(topContainer);
        
         //menu bar and events
        homeMenu  = new Menu();
        HBox homeMenuHB = new HBox();
        homeMenuHB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/home.png"),new Label(objDictionaryAction.getWord("HOME")));
        homeMenu.setGraphic(homeMenuHB);
        homeMenu.setAccelerator(new KeyCodeCombination(KeyCode.M, KeyCombination.SHIFT_DOWN));
        homeMenuHB.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                homeMenuAction();  
                me.consume();
            }
        });       
        // File menu - new, save, save as, open, load recent.
        fileMenuLabel = new Label(objDictionaryAction.getWord("FILE"));
        fileMenu = new Menu();
        fileMenu.setGraphic(fileMenuLabel);
        fileMenu.setAccelerator(new KeyCodeCombination(KeyCode.F, KeyCombination.SHIFT_DOWN));
        fileMenuLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                fileMenuAction();            
            }
        });
        // Edit menu - Toolbar, Color, Composite View, Support Lines, Ruler, Zoom-in, zoom-out
        editMenuLabel = new Label(objDictionaryAction.getWord("EDIT"));
        editMenu = new Menu();
        editMenu.setGraphic(editMenuLabel);
        editMenu.setAccelerator(new KeyCodeCombination(KeyCode.E, KeyCombination.SHIFT_DOWN));
        editMenuLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                editMenuAction();
            }
        });  
        // View menu - Toolbar, Color, Composite View, Support Lines, Ruler, Zoom-in, zoom-out
        viewMenuLabel = new Label(objDictionaryAction.getWord("VIEW"));
        viewMenu = new Menu();
        viewMenu.setGraphic(viewMenuLabel);
        viewMenu.setAccelerator(new KeyCodeCombination(KeyCode.V, KeyCombination.SHIFT_DOWN));
        viewMenuLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                viewMenuAction();
            }
        });  
        // Utility menu - Weave, Calculation, Language
        utilityMenuLabel = new Label(objDictionaryAction.getWord("UTILITY"));
        utilityMenu = new Menu();
        utilityMenu.setGraphic(utilityMenuLabel);
        utilityMenu.setAccelerator(new KeyCodeCombination(KeyCode.U, KeyCombination.SHIFT_DOWN));
        utilityMenuLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                utilityMenuAction();
            }
        });
        //Help Menu items
        Label supportMenuLabel = new Label(objDictionaryAction.getWord("SUPPORT"));
        supportMenuLabel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUPPORT")));
        supportMenu = new Menu();
        supportMenu.setGraphic(supportMenuLabel);
        
        MenuItem helpMenuItem = new MenuItem(objDictionaryAction.getWord("HELP"));
        helpMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        helpMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.H, KeyCombination.SHIFT_DOWN));
        helpMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {       
                helpMenuAction();
            }
        });        
        MenuItem technicalMenuItem = new MenuItem(objDictionaryAction.getWord("TECHNICAL"));
        technicalMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/technical_info.png"));
        technicalMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.T, KeyCombination.SHIFT_DOWN));
        technicalMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {       
                technicalMenuAction();
            }
        });
        MenuItem aboutMenuItem = new MenuItem(objDictionaryAction.getWord("ABOUTUS"));
        aboutMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/about_software.png"));
        aboutMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.A, KeyCombination.SHIFT_DOWN));
        aboutMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                aboutMenuAction();
            }
        });
        MenuItem contactMenuItem = new MenuItem(objDictionaryAction.getWord("CONTACTUS"));
        contactMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/contact_us.png"));
        contactMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.SHIFT_DOWN));
        contactMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                contactMenuAction();
            }
        });
        MenuItem logoutMenuItem = new MenuItem(objDictionaryAction.getWord("LOGOUT"));
        logoutMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/logout.png"));
        logoutMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.L, KeyCombination.SHIFT_DOWN));
        logoutMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                logoutMenuAction();
            }
        });
        MenuItem exitMenuItem = new MenuItem(objDictionaryAction.getWord("EXIT"));
        exitMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/quit.png"));
        exitMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.ESCAPE));
        exitMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                exitMenuAction();
            }
        });     
        //run menu: view all in one go
        runMenuLabel = new Label();
        runMenuLabel.setGraphic(new ImageView("/media/run.png"));
        runMenuLabel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPREALTIMEVIEW")));
        //runMenuLabel.setStyle("-fx-background-image:url('/media/run.png');-fx-background-repeat: no-repeat;-fx-background-size: 10, 10;-fx-pref-height:10;");
        runMenu  = new Menu();
        runMenu.setGraphic(runMenuLabel);
        runMenu.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.SHIFT_DOWN));
        runMenuLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                runMenuAction();
            }
        });
        supportMenu.getItems().addAll(helpMenuItem, technicalMenuItem, aboutMenuItem, contactMenuItem, new SeparatorMenuItem(), logoutMenuItem);//exitMenuItem);
        menuBar.getMenus().addAll(homeMenu,fileMenu, editMenu, viewMenu, utilityMenu, supportMenu);
        
        bodyContainer = new GridPane();
        //bodyContainer.setMinSize(objConfiguration.WIDTH, objConfiguration.HEIGHT);
        bodyContainer.setVgap(0);
        bodyContainer.setHgap(0);
        
        container = new ScrollPane();
        container.setPrefSize(objConfiguration.WIDTH, objConfiguration.HEIGHT);
        container.setContent(bodyContainer);
        root.setCenter(container);

        // Code Added for ShortCuts
        addAccelratorKey();
        selectedMenu = "FILE";
        menuHighlight();
        
        clothStage.getIcons().add(new Image("/media/icon.png"));
        clothStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWCLOTHEDITOR")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
        clothStage.setX(-5);
        clothStage.setY(0);
        clothStage.setResizable(false);
        clothStage.setScene(scene);
        clothStage.show();
        clothStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                homeMenuAction();
                we.consume();
            }
        });
    
        for(String key: objCloth.mapClothFabricDetails.keySet()){
            int index = getComponentIndex(key);
            //MAINFABRICID, ROTATIONANGLE, REPEATMODE, FABRICREPEAT, WIDTH, HEIGHT
            String[] clothFabric = objCloth.mapClothFabricDetails.get(key);
            rotations[index]=Integer.parseInt(clothFabric[1]);
            repeatMode[index]=clothFabric[2];
            repeats[index]=Integer.parseInt(clothFabric[3]);
            ends[index]=Integer.parseInt(clothFabric[4]);
            picks[index]=Integer.parseInt(clothFabric[5]);
            
            //widths[index]=Integer.parseInt(clothFabric[4]);
            //heights[index]=Integer.parseInt(clothFabric[5]);
        }

        //enable all components
        enableAllComponent();
        // setting initial heights and widths
        setWidthHeight();
        // initialize components and Panes
        initializePane();
        //set component sizes
        setComponentWidthHeight();
        //prepare components
        plotContainerGP();
        
        //defaultLayout();
        
        //ckeck for context avilable
        boolean allLayoutEmpty= true;
        for(String str: objCloth.mapClothFabric.keySet()){
            if(objCloth.mapClothFabric.get(str)!=null){
                allLayoutEmpty=false;
                break;
            }
        }
        if(objConfiguration.getStrRecentCloth()!=null && objConfiguration.strWindowFlowContext.equalsIgnoreCase("Dashboard")){
            try {
                objCloth = new Cloth();
                objCloth.setObjConfiguration(objConfiguration);
                objCloth.setStrClothId(objConfiguration.getStrRecentCloth());
                isNew = false;
                
                disableAllComponent();
                // setting heights and widths
                setWidthHeight();
                
                objClothAction = new ClothAction();
                objClothAction.getCloth(objCloth);
                for(String str: objCloth.mapClothFabric.keySet()){
                    if(objCloth.mapClothFabric.get(str)!=null){
                        loadFabric(objCloth.mapClothFabric.get(str), "Main");
                    }
                }
                
                loadCloth(objCloth);
                // setting heights and widths
                setWidthHeight();
                //set component sizes
                setComponentWidthHeight();
                //prepare components
                plotContainerGP();    
            } catch (SQLException ex) {
                new Logging("SEVERE",ClothView.class.getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        } else if(!allLayoutEmpty && (objConfiguration.strWindowFlowContext.equalsIgnoreCase("Dashboard") || objConfiguration.strWindowFlowContext.equalsIgnoreCase("FabricEditor"))){
            if(objCloth.getStrClothId()!=null && objCloth.getStrClothId()!="")
                isNew = false;
            objCloth.setObjConfiguration(objConfiguration);
            //if(objConfiguration.getStrRecentFabric()!=null && objConfiguration.strWindowFlowContext.equalsIgnoreCase("FabricEditor")){
            disableAllComponent();
            // setting heights and widths
            setWidthHeight();
            loadCloth(objCloth);
            // setting heights and widths
            setWidthHeight();
            //set component sizes
            setComponentWidthHeight();
            //prepare components
            plotContainerGP();    
        } else {
            isNew = true;
            objCloth=new Cloth();
            objCloth.setObjConfiguration(objConfiguration);
            objCloth.setStrClothType("Saree");
        
            disableAllComponent();
            leftBorderEnabled = true;
            rightBorderEnabled = true;
            leftsideLeftCrossBorderEnabled = true;
            leftsideRightCrossBorderEnabled = true;
            leftPalluEnabled = true;
            resetClothLayout();
            // setting heights and widths
            setWidthHeight();
            //set component sizes
            setComponentWidthHeight();
            //prepare components
            plotContainerGP();    
        }
    }
    /**
* resolutionControl(Stage)
* <p>
* This function is used for reassign width and height. 
*  
* @param       Stage primaryStage
* @author      Amit Kumar Singh
* @version     %I%, %G%
* @since       1.0
* @see         javafx.stage.*;
* @link        WindowView
*/
    private void resolutionControl(){
        //windowStage.resizableProperty().addListener(listener);
        scene.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                setGlobalWidthHeight();
                clothStage.setHeight(objConfiguration.HEIGHT);
                clothStage.setWidth(objConfiguration.WIDTH);
                //selectedMenu = "FILE";
                menuHighlight();
            }
        });
        scene.heightProperty().addListener(new ChangeListener<Number>() {
            @Override 
            public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneHeight, Number newSceneHeight) {
                setGlobalWidthHeight();
                clothStage.setHeight(objConfiguration.HEIGHT);
                clothStage.setWidth(objConfiguration.WIDTH);
                //selectedMenu = "FILE";
                menuHighlight();
            }
        });
    }
    private void setGlobalWidthHeight(){
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        objConfiguration.WIDTH = screenSize.getWidth();
        objConfiguration.HEIGHT = screenSize.getHeight();
        objConfiguration.strIconResolution = (objConfiguration.WIDTH<1280)?"hd_none":(objConfiguration.WIDTH<1920)?"hd":(objConfiguration.WIDTH<2560)?"hd_full":"hd_quad";
    }
    /**
     * homeMenuAction
     * <p>
     * Function use for exit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ClothView
     */
    private void homeMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHOME"));
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.setIconified(false);
        dialogStage.setFullScreen(false);
        dialogStage.setTitle(objDictionaryAction.getWord("ALERT"));
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 300, 100, Color.WHITE);
        scene.getStylesheets().add(ClothView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setAlignment(Pos.CENTER);
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        
        popup.add(new ImageView("/media/alert_warning.png"), 0, 0);   
        Label lblAlert = new Label(objDictionaryAction.getWord("ALERTCLOSE"));
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(250);
        popup.add(lblAlert, 1, 0);
        Button btnYes = new Button(objDictionaryAction.getWord("YES"));
        btnYes.setPrefWidth(50);
        btnYes.setId("btnYes");
        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                saveContext();
                dialogStage.close();
                clothStage.close();
                System.gc();
                WindowView objWindowView = new WindowView(objConfiguration);
            }
        });
        popup.add(btnYes, 0, 1);
        Button btnNo = new Button(objDictionaryAction.getWord("NO"));
        btnNo.setPrefWidth(50);
        btnNo.setId("btnNo");
        btnNo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
            }
        });
        popup.add(btnNo, 1, 1);
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.showAndWait();
    }
    /**
     * fileMenuAction
     * <p>
     * Function use for file menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ClothView
     */
    private void fileMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONFILE"));
        selectedMenu = "FILE";
        menuHighlight();
    }
    /**
     * editMenuAction
     * <p>
     * Function use for edit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ClothView
     */
    private void editMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONEDIT"));
        selectedMenu = "EDIT";
        menuHighlight();
    }
    /**
     * viewMenuAction
     * <p>
     * Function use for view menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ClothView
     */
    private void viewMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONVIEW"));
        selectedMenu = "VIEW";
        menuHighlight();
    }
    /**
     * utilityMenuAction
     * <p>
     * Function use for utility menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ClothView
     */
    private void utilityMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("TOOLTIPUTILITY"));
        selectedMenu = "UTILITY";
        menuHighlight();
    }
    /**
     * helpMenuAction
     * <p>
     * Function use for help menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void helpMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHELP"));
        HelpView objHelpView = new HelpView(objConfiguration);
    }
    /**
     * technicalMenuAction
     * <p>
     * Function use for technical menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void technicalMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONTECHNICAL"));
        TechnicalView objTechnicalView = new TechnicalView(objConfiguration);
    }
    /**
     * aboutMenuAction
     * <p>
     * Function use for about menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void aboutMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONABOUTUS"));
        AboutView objAboutView = new AboutView(objConfiguration);
    }
    /**
     * contactMenuAction
     * <p>
     * Function use for contact menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void contactMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCONTACTUS"));
        ContactView objContactView = new ContactView(objConfiguration);
    }
    /**
     * logoutMenuAction
     * <p>
     * Function use for exit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void logoutMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHOME"));
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.setIconified(false);
        dialogStage.setFullScreen(false);
        dialogStage.setTitle(objDictionaryAction.getWord("ALERT"));
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 300, 100, Color.WHITE);
        scene.getStylesheets().add(ClothView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setAlignment(Pos.CENTER);
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        
        popup.add(new ImageView("/media/alert_warning.png"), 0, 0);   
        Label lblAlert = new Label(objDictionaryAction.getWord("ALERTCLOSE"));
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(250);
        popup.add(lblAlert, 1, 0);
        Button btnYes = new Button(objDictionaryAction.getWord("YES"));
        btnYes.setPrefWidth(50);
        btnYes.setId("btnYes");
        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                objConfiguration.setStrRecentCloth(null);
                dialogStage.close();
                clothStage.close();
                System.gc();
                UserLoginView objUserLoginView = new UserLoginView(objConfiguration);
            }
        });
        popup.add(btnYes, 0, 1);
        Button btnNo = new Button(objDictionaryAction.getWord("NO"));
        btnNo.setPrefWidth(50);
        btnNo.setId("btnNo");
        btnNo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
            }
        });
        popup.add(btnNo, 1, 1);
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.showAndWait();
    }
    /**
     * exitMenuAction
     * <p>
     * Function use for exit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void exitMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONEXIT"));
        System.gc();
        clothStage.close();
    }
    private void runMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONREALTIMEVIEW"));
        ScreenShoot("Simulation");
     }
    /**
     * addAccelratorKey
     * <p>
     * Function use for adding shortcut key combinations. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ClothView
     */
    private void addAccelratorKey(){
        // shortcuts variable names: kc + Alphabet + ALT|CONTROL|SHIFT // ACS alphabetical
        final KeyCodeCombination homeMenu = new KeyCodeCombination(KeyCode.M, KeyCombination.SHIFT_DOWN); // Home Menu
        final KeyCodeCombination fileMenu = new KeyCodeCombination(KeyCode.F, KeyCombination.SHIFT_DOWN); // File Menu
        final KeyCodeCombination editMenu = new KeyCodeCombination(KeyCode.E, KeyCombination.SHIFT_DOWN); // Edit Menu
        final KeyCodeCombination viewMenu = new KeyCodeCombination(KeyCode.V, KeyCombination.SHIFT_DOWN); // View Menu
        final KeyCodeCombination utilityMenu = new KeyCodeCombination(KeyCode.U, KeyCombination.SHIFT_DOWN); // Utility Menu
        //utility menu items
        final KeyCodeCombination weaveUtility = new KeyCodeCombination(KeyCode.W, KeyCombination.SHIFT_DOWN); // weave
        final KeyCodeCombination artworkUtility = new KeyCodeCombination(KeyCode.D, KeyCombination.SHIFT_DOWN); // artwork
        final KeyCodeCombination fabricUtility = new KeyCodeCombination(KeyCode.J, KeyCombination.SHIFT_DOWN); // fabric
        //view menu items
        final KeyCodeCombination frontSideView = new KeyCodeCombination(KeyCode.F1, KeyCombination.CONTROL_DOWN); // front side
        final KeyCodeCombination rearSideView = new KeyCodeCombination(KeyCode.F2, KeyCombination.CONTROL_DOWN); // switch side
        final KeyCodeCombination simulationView = new KeyCodeCombination(KeyCode.F4, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // simulation
        final KeyCodeCombination zoomInView = new KeyCodeCombination(KeyCode.PLUS, KeyCombination.CONTROL_DOWN); // zoom in
        final KeyCodeCombination normalView = new KeyCodeCombination(KeyCode.ENTER, KeyCombination.CONTROL_DOWN); // zoom normal
        final KeyCodeCombination zoomOutView = new KeyCodeCombination(KeyCode.MINUS, KeyCombination.CONTROL_DOWN); // zoom out
        final KeyCodeCombination zoomInAlternateView = new KeyCodeCombination(KeyCode.ADD, KeyCombination.CONTROL_DOWN); // zoom in
        final KeyCodeCombination zoomOutAlternateView = new KeyCodeCombination(KeyCode.SUBTRACT, KeyCombination.CONTROL_DOWN); // zoom out
         //Edit menu items
        final KeyCodeCombination undoEdit = new KeyCodeCombination(KeyCode.Z, KeyCombination.CONTROL_DOWN); // undo
        final KeyCodeCombination redoEdit = new KeyCodeCombination(KeyCode.Y, KeyCombination.CONTROL_DOWN); // redo
        final KeyCodeCombination symmetryEdit = new KeyCodeCombination(KeyCode.S, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN); // symmetry
        final KeyCodeCombination repeatEdit = new KeyCodeCombination(KeyCode.R, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN); // repeats
        final KeyCodeCombination definedEdit = new KeyCodeCombination(KeyCode.D, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN); // defined layout
        final KeyCodeCombination customEdit = new KeyCodeCombination(KeyCode.C, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN); // custom layout
        final KeyCodeCombination clearEdit = new KeyCodeCombination(KeyCode.BACK_SPACE, KeyCombination.CONTROL_DOWN); // clear
        //File menu items
        final KeyCodeCombination newFile = new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN); // new
        final KeyCodeCombination openFile = new KeyCodeCombination(KeyCode.O, KeyCombination.CONTROL_DOWN); // open
        final KeyCodeCombination loadFile = new KeyCodeCombination(KeyCode.L, KeyCombination.CONTROL_DOWN); // load recent
        final KeyCodeCombination saveFile = new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN); // save
        final KeyCodeCombination saveAsFile = new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // save as
        final KeyCodeCombination saveXMLFile = new KeyCodeCombination(KeyCode.X, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // save xml
        final KeyCodeCombination saveTextureFile = new KeyCodeCombination(KeyCode.E, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // save as texture
        final KeyCodeCombination saveGraphFile = new KeyCodeCombination(KeyCode.H, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // save as graph
        final KeyCodeCombination printFile = new KeyCodeCombination(KeyCode.P, KeyCombination.CONTROL_DOWN); // print
        
        scene.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                if(homeMenu.match(t))
                    homeMenuAction();
                else if(fileMenu.match(t))
                    fileMenuAction();
                else if(editMenu.match(t))
                    editMenuAction();
                else if(viewMenu.match(t))
                    viewMenuAction();
                else if(utilityMenu.match(t))
                    utilityMenuAction();
                else if(weaveUtility.match(t))
                    weaveUtilityAction();
                else if(artworkUtility.match(t))
                    artworkUtilityAction();
                else if(fabricUtility.match(t))
                    fabricUtilityAction();
                else if(frontSideView.match(t))
                    frontSideViewAction();
                else if(rearSideView.match(t))
                    rearSideViewAction();
                else if(simulationView.match(t))
                    simulationViewAction();
                else if(zoomInView.match(t) || zoomInAlternateView.match(t))
                    zoomInAction();
                else if(normalView.match(t))
                    zoomNormalAction();
                else if(zoomOutView.match(t) || zoomOutAlternateView.match(t))
                    zoomOutAction();
                else if(undoEdit.match(t))
                    undoAction();
                else if(redoEdit.match(t))
                    redoAction();
                else if(symmetryEdit.match(t))
                    symmetryMenuAction();
                else if(repeatEdit.match(t))
                    repeatsMenuAction();
                else if(definedEdit.match(t))
                    definedLayoutMenuAction();
                else if(customEdit.match(t))
                    customLayoutMenuAction();
                else if(clearEdit.match(t))
                    clearMenuAction();
                else if(newFile.match(t))
                    createMenuAction();
                else if(openFile.match(t))
                    openMenuAction();
                else if(loadFile.match(t))
                    loadRecentMenuAction();
                else if(saveFile.match(t) && !isNew)
                    saveMenuAction();
                else if(saveAsFile.match(t))
                    saveAsMenuAction();
                else if(saveXMLFile.match(t))
                    exportHtmlAction();
                else if(saveTextureFile.match(t))
                    exportTextureAction();
                else if(saveGraphFile.match(t))
                    exportGraphAction();
                else if(printFile.match(t))
                    printMenuAction();
            }
        });
    }
/**
 * menuHighlight
 * <p>
 * Function use for drawing menu bar for menu item,
 * and binding events for each menus with style. 
 * 
 * @param       String strMenu
 * @author      Amit Kumar Singh
 * @version     %I%, %G%
 * @since       1.0
 * @see         javafx.event.*;
 * @link        ClothView
 */
    private void menuHighlight(){
        /*fileMenu.getStyleClass().clear();
        editMenu.getStyleClass().clear();
        viewMenu.getStyleClass().clear();
        utilityMenu.getStyleClass().clear();
        fileMenuLabel.getStyleClass().clear();
        editMenuLabel.getStyleClass().clear();
        viewMenuLabel.getStyleClass().clear();
        utilityMenuLabel.getStyleClass().clear();
        fileMenu.getStyleClass().add("menu-unselected");
        editMenu.getStyleClass().add("menu-unselected");
        viewMenu.getStyleClass().add("menu-unselected");
        utilityMenu.getStyleClass().add("menu-unselected");        
        fileMenuLabel.getStyleClass().add("menu-unselected-label");
        editMenuLabel.getStyleClass().add("menu-unselected-label");
        viewMenuLabel.getStyleClass().add("menu-unselected-label");
        utilityMenuLabel.getStyleClass().add("menu-unselected-label");
        */
        fileMenu.setStyle("-fx-background-color: linear-gradient(#000000,#5C594A);");
        editMenu.setStyle("-fx-background-color: linear-gradient(#000000,#5C594A);");
        viewMenu.setStyle("-fx-background-color: linear-gradient(#000000,#5C594A);");
        utilityMenu.setStyle("-fx-background-color: linear-gradient(#000000,#5C594A);");        
        fileMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-font-family: \"Segoe UI Semibold\"; -fx-text-fill: #FFFFFF;");
        editMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-font-family: \"Segoe UI Semibold\"; -fx-text-fill: #FFFFFF;");
        viewMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-font-family: \"Segoe UI Semibold\"; -fx-text-fill: #FFFFFF;");
        utilityMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-font-family: \"Segoe UI Semibold\"; -fx-text-fill: #FFFFFF;");
        
        toolBar.getItems().clear();
        menuSelected.getItems().clear();
        
        System.gc();
        if(selectedMenu == "UTILITY"){
            /*utilityMenu.getStyleClass().clear();
            utilityMenuLabel.getStyleClass().clear();
            utilityMenu.getStyleClass().add("menu-selected");
            utilityMenuLabel.getStyleClass().add("menu-selected-label");*/
            utilityMenu.setStyle("-fx-background-color: linear-gradient(#FFFFFF,#FFFEEE);");
            utilityMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-text-fill: #000000;");
            populateUtilityToolbar();
            populateUtilityToolbarMenu();
        } else if(selectedMenu == "VIEW"){
            /*viewMenu.getStyleClass().clear();
            viewMenuLabel.getStyleClass().clear();
            viewMenu.getStyleClass().add("menu-selected");
            viewMenuLabel.getStyleClass().add("menu-selected-label");*/
            viewMenu.setStyle("-fx-background-color: linear-gradient(#FFFFFF,#FFFEEE);");
            viewMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-text-fill: #000000;");
            populateViewToolbar();
            populateViewToolbarMenu();
        } else if(selectedMenu == "EDIT"){
            /*editMenu.getStyleClass().clear();
            editMenuLabel.getStyleClass().clear();
            editMenu.getStyleClass().add("menu-selected");
            editMenuLabel.getStyleClass().add("menu-selected-label");*/
            editMenu.setStyle("-fx-background-color: linear-gradient(#FFFFFF,#FFFEEE);");
            editMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-text-fill: #000000;");
            populateEditToolbar();
            populateEditToolbarMenu();
        } else{
            /*fileMenu.getStyleClass().clear();
            fileMenuLabel.getStyleClass().clear();
            fileMenu.getStyleClass().add("menu-selected");
            fileMenuLabel.getStyleClass().add("menu-selected-label");
            System.err.println("i am up here");*/
            fileMenu.setStyle("-fx-background-color: linear-gradient(#FFFFFF,#FFFEEE);");
            fileMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-text-fill: #000000;");
            populateFileToolbar();
            populateFileToolbarMenu();
        }               
    } 
    /**
 * populateFileToolbar
 * <p>
 * Function use for drawing tool bar for menu item File,
 * and binding events for each tools. 
 * 
 * @exception   (@throws SQLException)
 * @author      Amit Kumar Singh
 * @version     %I%, %G%
 * @since       1.0
 * @see         javafx.event.*;
 * @link        ClothView
 */
     private void populateFileToolbarMenu(){         
         //For drop down menus
        menuSelected.setTooltip(new Tooltip(objDictionaryAction.getWord("FILE")));
        
        MenuItem newFileMI = new MenuItem(objDictionaryAction.getWord("NEWFILE"));
        MenuItem openFileMI = new MenuItem(objDictionaryAction.getWord("OPENFILE"));
        MenuItem loadFileMI = new MenuItem(objDictionaryAction.getWord("LOADFILE"));
        MenuItem saveFileMI = new MenuItem(objDictionaryAction.getWord("SAVEFILE"));
        MenuItem saveAsFileMI = new MenuItem(objDictionaryAction.getWord("SAVEASFILE"));
        MenuItem saveXMLFileMI = new MenuItem(objDictionaryAction.getWord("SAVEXMLFILE"));
        MenuItem saveTextureFileMI = new MenuItem(objDictionaryAction.getWord("SAVETEXTUREFILE"));
        MenuItem saveGraphFileMI = new MenuItem(objDictionaryAction.getWord("SAVEGRAPHFILE"));
        MenuItem printFileMI = new MenuItem(objDictionaryAction.getWord("PRINTFILE"));
        
        newFileMI.setAccelerator(new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN));
        openFileMI.setAccelerator(new KeyCodeCombination(KeyCode.O, KeyCombination.CONTROL_DOWN));
        loadFileMI.setAccelerator(new KeyCodeCombination(KeyCode.L, KeyCombination.CONTROL_DOWN));
        saveFileMI.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN));
        saveAsFileMI.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        saveXMLFileMI.setAccelerator(new KeyCodeCombination(KeyCode.X, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        saveTextureFileMI.setAccelerator(new KeyCodeCombination(KeyCode.E, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        saveGraphFileMI.setAccelerator(new KeyCodeCombination(KeyCode.H, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        printFileMI.setAccelerator(new KeyCodeCombination(KeyCode.P, KeyCombination.CONTROL_DOWN));
        
        newFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/new.png"));
        openFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/open.png"));
        loadFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/open_recent.png"));
        saveFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
        saveAsFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save_as.png"));
        saveXMLFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_html_as.png"));
        saveTextureFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_texture_as.png"));
        saveGraphFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_graph_as.png"));
        printFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/print.png"));
             
        Menu exportMenu = new Menu(objDictionaryAction.getWord("EXPORT"));
        exportMenu.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export.png"));
        exportMenu.getItems().addAll(saveXMLFileMI, saveTextureFileMI, saveGraphFileMI);
        
        //add enable disable condition
        //Add the action to Buttons.
        newFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                createMenuAction();
            }
        });        
        openFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                openMenuAction();
            }
        });
        loadFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                loadRecentMenuAction();
            }
        });
        saveFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                saveMenuAction();
            }
        });
        saveAsFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                saveAsMenuAction();
            }
        });
        saveXMLFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                exportHtmlAction();
            }
        }); 
        saveTextureFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                exportTextureAction();
            }
        });
        saveGraphFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                exportGraphAction();
            }
        });
        printFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                printMenuAction();
            }
        }); 
        
        menuSelected.getItems().addAll(newFileMI, openFileMI, loadFileMI, new SeparatorMenuItem(), saveFileMI, saveAsFileMI, new SeparatorMenuItem(), exportMenu, printFileMI);
     }
     private void populateFileToolbar(){         
        // New file item
        Button newFileBtn = new Button();
        newFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/new.png"));
        newFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("NEWFILE")+" (Ctrl+N)\n"+objDictionaryAction.getWord("TOOLTIPNEWFILE")));
        newFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        newFileBtn.getStyleClass().add("toolbar-button");   
        // Open file item
        Button openFileBtn = new Button();
        openFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/open.png"));
        openFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("OPENFILE")+" (Ctrl+O)\n"+objDictionaryAction.getWord("TOOLTIPOPENFILE")));
        openFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        openFileBtn.getStyleClass().add("toolbar-button");
        // load recent file item
        Button loadFileBtn = new Button();        
        loadFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/open_recent.png"));
        loadFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("LOADFILE")+" (Ctrl+L)\n"+objDictionaryAction.getWord("TOOLTIPLOADFILE")));
        loadFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        loadFileBtn.getStyleClass().addAll("toolbar-button");
        // Save file item
        Button saveFileBtn = new Button();        
        saveFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
        saveFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SAVEFILE")+" (Ctrl+S)\n"+objDictionaryAction.getWord("TOOLTIPSAVEFILE")));
        saveFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        saveFileBtn.getStyleClass().addAll("toolbar-button");
        // Save As file item
        Button saveAsFileBtn = new Button();        
        saveAsFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save_as.png"));
        saveAsFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SAVEASFILE")+" (Ctrl+Shift+S)\n"+objDictionaryAction.getWord("TOOLTIPSAVEASFILE")));
        saveAsFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        saveAsFileBtn.getStyleClass().addAll("toolbar-button");        
        // Save As file item
        Button saveXMLFileBtn = new Button();        
        saveXMLFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_html_as.png"));
        saveXMLFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SAVEXMLFILE")+" (Ctrl+Shift+X)\n"+objDictionaryAction.getWord("TOOLTIPSAVEXMLFILE")));
        saveXMLFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        saveXMLFileBtn.getStyleClass().addAll("toolbar-button");
        // Save Texture file item
        Button saveTextureFileBtn = new Button();        
        saveTextureFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_texture_as.png"));
        saveTextureFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SAVETEXTUREFILE")+" (Ctrl+Shift+E)\n"+objDictionaryAction.getWord("TOOLTIPSAVETEXTUREFILE")));
        saveTextureFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        saveTextureFileBtn.getStyleClass().addAll("toolbar-button");
        // Save graph file item
        Button saveGraphFileBtn = new Button();        
        saveGraphFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_graph_as.png"));
        saveGraphFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SAVEGRAPHFILE")+" (Ctrl+Shift+H)\n"+objDictionaryAction.getWord("TOOLTIPSAVEGRAPHFILE")));
        saveGraphFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        saveGraphFileBtn.getStyleClass().addAll("toolbar-button");
        // print File menu
        Button printFileBtn = new Button();        
        printFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/print.png"));
        printFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("PRINTFILE")+" (Ctrl+P)\n"+objDictionaryAction.getWord("TOOLTIPPRINTFILE")));
        printFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        printFileBtn.getStyleClass().addAll("toolbar-button");  
        //add enable disable condition
        //Add the action to Buttons.
        newFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                createMenuAction();
            }
        });        
        openFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                openMenuAction();
            }
        });
        loadFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadRecentMenuAction();
            }
        });
        saveFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                saveMenuAction();
            }
        });
        saveAsFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                saveAsMenuAction();
            }
        });
        saveXMLFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                exportHtmlAction();
            }
        }); 
        saveTextureFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                exportTextureAction();
            }
        });
        saveGraphFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                exportGraphAction();
            }
        });
        printFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                printMenuAction();
            }
        }); 
        //Create some Buttons.        
        FlowPane flow = new FlowPane();        
        flow.setPrefWrapLength(objConfiguration.WIDTH*.95);
        flow.getStyleClass().addAll("flow");
        //Add the Buttons to the ToolBar.
        flow.getChildren().addAll(newFileBtn,openFileBtn,loadFileBtn,saveFileBtn,saveAsFileBtn,saveXMLFileBtn,saveTextureFileBtn,saveGraphFileBtn,printFileBtn);
        flow.setAlignment(Pos.TOP_LEFT);
        toolBar.getItems().addAll(flow);
    }
 
 /**
 * populateEditToolbar
 * <p>
 * Function use for drawing tool bar for menu item Edit,
 * and binding events for each tools. 
 * 
 * @exception   (@throws SQLException)
 * @author      Amit Kumar Singh
 * @version     %I%, %G%
 * @since       1.0
 * @see         javafx.event.*;
 * @link        ClothView
 */
    private void populateEditToolbarMenu(){
        //For drop down menus
        menuSelected.setTooltip(new Tooltip(objDictionaryAction.getWord("EDIT")));
        
        MenuItem undoEditMI = new MenuItem(objDictionaryAction.getWord("UNDO"));
        MenuItem redoEditMI = new MenuItem(objDictionaryAction.getWord("REDO"));
        MenuItem symmetryEditMI = new MenuItem(objDictionaryAction.getWord("SYMMETRY"));
        MenuItem repeatEditMI = new MenuItem(objDictionaryAction.getWord("REPEATS"));
        MenuItem definedEditMI = new MenuItem(objDictionaryAction.getWord("DEFINEDCLOTH"));
        MenuItem customEditMI = new MenuItem(objDictionaryAction.getWord("CUSTOMCLOTH"));
        MenuItem clearEditMI = new MenuItem(objDictionaryAction.getWord("CLEAR"));
        
        undoEditMI.setAccelerator(new KeyCodeCombination(KeyCode.Z, KeyCombination.CONTROL_DOWN));
        redoEditMI.setAccelerator(new KeyCodeCombination(KeyCode.Y, KeyCombination.CONTROL_DOWN));
        symmetryEditMI.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        repeatEditMI.setAccelerator(new KeyCodeCombination(KeyCode.R, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        definedEditMI.setAccelerator(new KeyCodeCombination(KeyCode.D, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        customEditMI.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        clearEditMI.setAccelerator(new KeyCodeCombination(KeyCode.BACK_SPACE, KeyCombination.CONTROL_DOWN));
        
        undoEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/undo.png"));
        redoEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/redo.png"));
        symmetryEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/symmetry.png"));
        repeatEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/repeat.png"));
        definedEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_layout_predefine.png"));
        customEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_layout_custom.png"));
        clearEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/clear.png"));
            
        //Add the action to Buttons.
        undoEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                undoAction();
            }
        });
        redoEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                redoAction();
            }
        });
        symmetryEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                symmetryMenuAction();
            }
        });
        repeatEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                repeatsMenuAction();
            }
        });
        definedEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                definedLayoutMenuAction();
            }
        });
        customEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                customLayoutMenuAction();
            }
        });
        clearEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                clearMenuAction();
            }
        });
        
        menuSelected.getItems().addAll(undoEditMI, redoEditMI, new SeparatorMenuItem(), symmetryEditMI, repeatEditMI, definedEditMI, customEditMI, clearEditMI);
    }
    private void populateEditToolbar(){
        // Undo edit item
        Button undoEditBtn = new Button();
        undoEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/undo.png"));
        undoEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("UNDO")+" (Ctrl+Z)\n"+objDictionaryAction.getWord("TOOLTIPUNDO")));
        undoEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        undoEditBtn.getStyleClass().add("toolbar-button");    
        // Redo edit item 
        Button redoEditBtn = new Button();
        redoEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/redo.png"));
        redoEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("REDO")+" (Ctrl+Y)\n"+objDictionaryAction.getWord("TOOLTIPREDO")));
        redoEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        redoEditBtn.getStyleClass().add("toolbar-button");  
        //symmetry configuration
        Button symmetryEditBtn = new Button();
        symmetryEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/symmetry.png"));
        symmetryEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SYMMETRY")+" (Alt+Shift+S)\n"+objDictionaryAction.getWord("TOOLTIPSYMMETRY")));
        symmetryEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        symmetryEditBtn.getStyleClass().add("toolbar-button");  
        //repeats configuration
        Button repeatEditBtn = new Button();
        repeatEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/repeat.png"));
        repeatEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("REPEATS")+" (Alt+Shift+R)\n"+objDictionaryAction.getWord("TOOLTIPREPEATS")));
        repeatEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        repeatEditBtn.getStyleClass().add("toolbar-button");  
        // pre defined layout item
        Button definedEditBtn = new Button(); 
        definedEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_layout_predefine.png"));
        definedEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("DEFINEDCLOTH")+" (Alt+Shift+D)\n"+objDictionaryAction.getWord("TOOLTIPDEFINEDCLOTH")));
        definedEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        definedEditBtn.getStyleClass().add("toolbar-button");  
        // custom layout item
        Button customEditBtn = new Button(); 
        customEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_layout_custom.png"));
        customEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CUSTOMCLOTH")+" (Alt+Shift+C)\n"+objDictionaryAction.getWord("TOOLTIPCUSTOMCLOTH")));
        customEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        customEditBtn.getStyleClass().add("toolbar-button");  
        //clear canvas
        Button clearEditBtn = new Button();
        clearEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/clear.png"));
        clearEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CLEAR")+" (Ctrl+Backspace)\n"+objDictionaryAction.getWord("TOOLTIPCLEAR")));
        clearEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        clearEditBtn.getStyleClass().add("toolbar-button");  
        //Add the action to Buttons.
        undoEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                undoAction();
            }
        });
        redoEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                redoAction();
            }
        });
        symmetryEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                symmetryMenuAction();
            }
        });
        repeatEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                repeatsMenuAction();
            }
        });
        definedEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                definedLayoutMenuAction();
            }
        });
        customEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                customLayoutMenuAction();
            }
        });
        clearEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                clearMenuAction();
            }
        });
        //Create some Buttons.        
        FlowPane flow = new FlowPane();        
        flow.setPrefWrapLength(objConfiguration.WIDTH*.95);
        flow.getStyleClass().addAll("flow");
        //Add the Buttons to the ToolBar.
        flow.getChildren().addAll(undoEditBtn,redoEditBtn,symmetryEditBtn,repeatEditBtn,definedEditBtn,customEditBtn,clearEditBtn);
        flow.setAlignment(Pos.TOP_LEFT);
        toolBar.getItems().addAll(flow);
    }

/**
 * populateViewToolbar
 * <p>
 * Function use for drawing tool bar for menu item View,
 * and binding events for each tools. 
 * 
 * @exception   (@throws SQLException)
 * @author      Amit Kumar Singh
 * @version     %I%, %G%
 * @since       1.0
 * @see         javafx.event.*;
 * @link        ClothView
 */    
    private void populateViewToolbarMenu(){
        //For drop down menus
        menuSelected.setTooltip(new Tooltip(objDictionaryAction.getWord("VIEW")));
        
        MenuItem frontSideViewMI = new MenuItem(objDictionaryAction.getWord("FRONTSIDEVIEW"));
        MenuItem rearSideViewMI = new MenuItem(objDictionaryAction.getWord("REARSIDEVIEW"));
        MenuItem simulationViewMI = new MenuItem(objDictionaryAction.getWord("SIMULATION"));
        MenuItem zoomInViewMI = new MenuItem(objDictionaryAction.getWord("ZOOMINVIEW"));
        MenuItem normalViewMI = new MenuItem(objDictionaryAction.getWord("ZOOMNORMALVIEW"));
        MenuItem zoomOutViewMI = new MenuItem(objDictionaryAction.getWord("ZOOMOUTVIEW"));
        
        frontSideViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F1, KeyCombination.CONTROL_DOWN));
        rearSideViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F2, KeyCombination.CONTROL_DOWN));
        simulationViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F4, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        zoomInViewMI.setAccelerator(new KeyCodeCombination(KeyCode.PLUS, KeyCombination.CONTROL_DOWN));
        normalViewMI.setAccelerator(new KeyCodeCombination(KeyCode.ENTER, KeyCombination.CONTROL_DOWN));
        zoomOutViewMI.setAccelerator(new KeyCodeCombination(KeyCode.MINUS, KeyCombination.CONTROL_DOWN));
        
        frontSideViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_view.png"));
        rearSideViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_view.png"));
        simulationViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/simulation.png"));
        zoomInViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
        normalViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
        zoomOutViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
        
        //Add the action to Buttons.
        frontSideViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                frontSideViewAction();
            }
        }); 
        rearSideViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                rearSideViewAction();
            }
        }); 
        simulationViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                simulationViewAction();
            }
        }); 
        zoomInViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                zoomInAction();
            }
        });
        zoomOutViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                zoomOutAction();
            }
        });
        normalViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                zoomNormalAction();
            }
        });
        menuSelected.getItems().addAll(frontSideViewMI, rearSideViewMI, simulationViewMI, new SeparatorMenuItem(), zoomInViewMI, normalViewMI, zoomOutViewMI);
    }
    private void populateViewToolbar(){
        // front side View item;
        Button frontSideViewBtn = new Button();
        frontSideViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_view.png"));
        frontSideViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("FRONTSIDEVIEW")+" (Ctrl+F1)\n"+objDictionaryAction.getWord("TOOLTIPFRONTSIDEVIEW")));
        frontSideViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        frontSideViewBtn.getStyleClass().add("toolbar-button");    
        // Switch Side View item;
        Button rearSideViewBtn = new Button();
        rearSideViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_view.png"));
        rearSideViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("REARSIDEVIEW")+" (Ctrl+F2)\n"+objDictionaryAction.getWord("TOOLTIPREARSIDEVIEW")));
        rearSideViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rearSideViewBtn.getStyleClass().add("toolbar-button");  
        // Simulation View menu
        Button simulationViewBtn = new Button();
        simulationViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/simulation.png"));
        simulationViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SIMULATION")+" (Ctrl+Shift+F4)\n"+objDictionaryAction.getWord("TOOLTIPSIMULATION")));
        simulationViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        simulationViewBtn.getStyleClass().add("toolbar-button"); 
        /*
        // full view
        Button fullViewBtn = new Button();
        fullViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_layout.png"));
        fullViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("FULLVIEW")+"\n"+objDictionaryAction.getWord("TOOLTIPFULLVIEW")));
        fullViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        fullViewBtn.getStyleClass().add("toolbar-button");         
        */
        // Zoom-In item
        Button zoomInViewBtn = new Button();
        zoomInViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
        zoomInViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ZOOMINVIEW")+" (Ctrl+Plus)\n"+objDictionaryAction.getWord("TOOLTIPZOOMINVIEW")));
        zoomInViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        zoomInViewBtn.getStyleClass().add("toolbar-button");    
        // Normal item
        Button normalViewBtn = new Button();
        normalViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
        normalViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ZOOMNORMALVIEW")+" (Ctrl+Enter)\n"+objDictionaryAction.getWord("TOOLTIPZOOMNORMALVIEW")));
        normalViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        normalViewBtn.getStyleClass().add("toolbar-button");    
        // Zoom-Out item
        Button zoomOutViewBtn = new Button();
        zoomOutViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
        zoomOutViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ZOOMOUTVIEW")+" (Ctrl+Minus)\n"+objDictionaryAction.getWord("TOOLTIPZOOMOUTVIEW")));
        zoomOutViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        zoomOutViewBtn.getStyleClass().add("toolbar-button");        
        //Add the action to Buttons.
        frontSideViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                frontSideViewAction();
            }
        }); 
        rearSideViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rearSideViewAction();
            }
        }); 
        simulationViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                simulationViewAction();
            }
        }); 
        /*
        fullViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONFULLVIEW"));
                //clothLayout("FullView");
            }
        });
        */
        zoomInViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                zoomInAction();
            }
        });
        zoomOutViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                zoomOutAction();
            }
        });
        normalViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                zoomNormalAction();
            }
        });
        
        //Create some Buttons.        
        FlowPane flow = new FlowPane();        
        flow.setPrefWrapLength(objConfiguration.WIDTH*.95);
        flow.getStyleClass().addAll("flow"); 
        //Add the Buttons to the ToolBar.
        flow.getChildren().addAll(frontSideViewBtn,rearSideViewBtn,simulationViewBtn,zoomInViewBtn,normalViewBtn,zoomOutViewBtn);
        flow.setAlignment(Pos.TOP_LEFT);
        toolBar.getItems().addAll(flow);        
    }

/**
 * populateUtilityToolbar
 * <p>
 * Function use for drawing tool bar for menu item Utility,
 * and binding events for each tools. 
 * 
 * @exception   (@throws SQLException)
 * @author      Amit Kumar Singh
 * @version     %I%, %G%
 * @since       1.0
 * @see         javafx.event.*;
 * @link        ClothView
 */  
    private void populateUtilityToolbarMenu(){   
        //For drop down menus
        menuSelected.setTooltip(new Tooltip(objDictionaryAction.getWord("UTILITY")));
        
        MenuItem weaveUtilityMI = new MenuItem(objDictionaryAction.getWord("WEAVEEDITOR"));
        MenuItem artworkUtilityMI = new MenuItem(objDictionaryAction.getWord("ARTWORKEDITOR"));
        MenuItem fabricUtilityMI = new MenuItem(objDictionaryAction.getWord("FABRICEDITOR"));
        
        weaveUtilityMI.setAccelerator(new KeyCodeCombination(KeyCode.W, KeyCombination.SHIFT_DOWN));
        artworkUtilityMI.setAccelerator(new KeyCodeCombination(KeyCode.D, KeyCombination.SHIFT_DOWN));
        fabricUtilityMI.setAccelerator(new KeyCodeCombination(KeyCode.J, KeyCombination.SHIFT_DOWN));
        
        weaveUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/weave_editor.png"));
        artworkUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/artwork_editor.png"));
        fabricUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fabric_editor.png"));
        
        //Add the action to Buttons.
        weaveUtilityMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                weaveUtilityAction();
            }
        });
        artworkUtilityMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                artworkUtilityAction();
            }
        });
        fabricUtilityMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                fabricUtilityAction();
            }
        });
        menuSelected.getItems().addAll(weaveUtilityMI, artworkUtilityMI, fabricUtilityMI);
    }          
    private void populateUtilityToolbar(){   
        // weave editor item
        Button weaveUtilityBtn = new Button();
        weaveUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/weave_editor.png"));
        weaveUtilityBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("WEAVEEDITOR")+" (Shift+W)\n"+objDictionaryAction.getWord("TOOLTIPWEAVEEDITOR")));
        weaveUtilityBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        weaveUtilityBtn.getStyleClass().add("toolbar-button");    
        // artwork editor item
        Button artworkUtilityBtn = new Button();
        artworkUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/artwork_editor.png"));
        artworkUtilityBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ARTWORKEDITOR")+" (Shift+D)\n"+objDictionaryAction.getWord("TOOLTIPARTWORKEDITOR")));
        artworkUtilityBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        artworkUtilityBtn.getStyleClass().add("toolbar-button");    
        // garment viewer File menu
        Button fabricUtilityBtn = new Button();
        fabricUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fabric_editor.png"));
        fabricUtilityBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("FABRICEDITOR")+" (Shift+J)\n"+objDictionaryAction.getWord("TOOLTIPFABRICEDITOR")));
        fabricUtilityBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        fabricUtilityBtn.getStyleClass().add("toolbar-button");  
        //Add the action to Buttons.
        weaveUtilityBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                weaveUtilityAction();
            }
        });
        artworkUtilityBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                artworkUtilityAction();
            }
        });
        fabricUtilityBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                fabricUtilityAction();
            }
        });
        //Create some Buttons. 
        FlowPane flow = new FlowPane();        
        flow.setPrefWrapLength(objConfiguration.WIDTH*.95); // preferred width allows for two columns
        flow.getStyleClass().addAll("flow");                
        //Add the Buttons to the ToolBar.
        flow.getChildren().addAll(weaveUtilityBtn,artworkUtilityBtn,fabricUtilityBtn);
        flow.setAlignment(Pos.TOP_LEFT);
        toolBar.getItems().addAll(flow);
    }
    
    private void createMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONNEWFILE"));
        final Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.initStyle(StageStyle.UTILITY);
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 250, 150, Color.WHITE);
        scene.getStylesheets().add(WeaveView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());

        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setAlignment(Pos.CENTER);
        popup.setHgap(10);
        popup.setVgap(10);
        popup.setPadding(new Insets(25, 25, 25, 25));
        
        Label lblName = new Label(objDictionaryAction.getWord("NAME"));
        lblName.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPNAME")));
        lblName.setPrefWidth(scene.getWidth()/3);
        popup.add(lblName, 0, 0);
        final TextField txtName = new TextField();
        txtName.setPromptText(objDictionaryAction.getWord("TOOLTIPNAME"));
        txtName.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPNAME")));
        txtName.setPrefWidth(2*scene.getWidth()/3);
        //txtName.setMinSize(150, 25);
        //txtName.setMaxSize(150, 50);
        popup.add(txtName, 1, 0);
        
        Label lblType = new Label(objDictionaryAction.getWord("CLOTHTYPE"));
        lblType.setTooltip(new Tooltip(objDictionaryAction.getWord("CLOTHTYPE")));
        lblType.setPrefWidth(scene.getWidth()/3);
        popup.add(lblType, 0, 1);
        final ComboBox cbType = new ComboBox();
        cbType.setTooltip(new Tooltip(objDictionaryAction.getWord("CLOTHTYPE")));
        cbType.setPrefWidth(2*scene.getWidth()/3);
        cbType.getItems().addAll("Saree","Dress Material","Stole","Shawl","Gaisar Brocade");
        cbType.setValue(objCloth.getStrClothType());
        popup.add(cbType, 1, 1);
        
        Button btnSave = new Button(objDictionaryAction.getWord("SUBMIT"));
        btnSave.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save_as.png"));
        btnSave.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUBMIT")));
        btnSave.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONSUBMIT"));
                if(txtName.getText().trim().length()==0){
                    lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
                } else {
                    objCloth = new Cloth();
                    objCloth.setObjConfiguration(objConfiguration);
                    objCloth.setStrClothName(txtName.getText());
                    objCloth.setStrClothType(cbType.getValue().toString());
                    isNew = true;
                    
                    disableAllComponent();
                    initCloth();
                    // setting heights and widths
                    setWidthHeight();
                    // initialize components and Panes
                    initializePane();
                    //set component sizes
                    setComponentWidthHeight();
                    //prepare components
                    plotContainerGP();
                    objUR.clear();
                    Cloth objClothCopy = new Cloth();
                    cloneCloth(objClothCopy);
                    objUR.doCommand("Initial Create Cloth", objClothCopy);
                    dialogStage.close();
                }
                System.gc();
            }
        });
        popup.add(btnSave, 1, 2);
        /*
        Button btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));
        btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
        btnCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                dialogStage.close();
                System.gc();
            }
        });
        popup.add(btnCancel, 1, 2);
        */
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("NEWFILE"));
        dialogStage.showAndWait();
    }
    private void openMenuAction(){
        try{
            lblStatus.setText(objDictionaryAction.getWord("ACTIONOPENFILE"));
            Cloth objClothNew = new Cloth();
            objClothNew.setObjConfiguration(objConfiguration);
            ClothImportView objClothImportView= new ClothImportView(objClothNew);
            if(objClothNew.getStrClothId()!=null){
                isNew = false;
                
                initCloth();
                objCloth = objClothNew;
                disableAllComponent();
                // setting heights and widths
                setWidthHeight();
                objClothAction = new ClothAction();
                objClothAction.getCloth(objCloth);
                for(String str: objCloth.mapClothFabric.keySet()){
                    if(objCloth.mapClothFabric.get(str)!=null){
                        loadFabric(objCloth.mapClothFabric.get(str), "Main");
                    }
                }
                //setting the symmtery link policy
                setSymmetryPolicy();
                //loading cloth data
                loadCloth(objCloth);
                // setting heights and widths
                setWidthHeight();
                //set component sizes
                setComponentWidthHeight();
                //prepare components
                plotContainerGP();    
                objUR.clear();
                Cloth objClothCopy = new Cloth();
                cloneCloth(objClothCopy);
                objUR.doCommand("Initial Create Cloth", objClothCopy);
            }
        } catch (SQLException ex) {
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            new Logging("SEVERE",ClothView.class.getName(),ex.toString(),ex);
        } catch (Exception ex) {
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            new Logging("SEVERE",ClothView.class.getName(),ex.toString(),ex);
        }
        System.gc();
    }    
    private void loadRecentMenuAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONLOADFILE"));
            
            Cloth objClothNew = new Cloth();
            objClothNew.setObjConfiguration(objConfiguration);
            objClothNew.setStrCondition("");
            objClothNew.setStrSearchBy("All");
            objClothNew.setStrSearchAccess("All User Data");
            objClothNew.setStrOrderBy("Date");
            objClothNew.setStrDirection("Descending");
            objClothAction = new ClothAction();
            List lstCloth = (ArrayList)objClothAction.lstImportCloth(objClothNew).get(0);
            if(lstCloth!=null && lstCloth.size()>0)
                objClothNew.setStrClothId(lstCloth.get(0).toString());
            
            if(objClothNew.getStrClothId()!=null){
                isNew = false;
                initCloth();
                objCloth = objClothNew;
                disableAllComponent();
                // setting heights and widths
                setWidthHeight();
                objClothAction = new ClothAction();
                objClothAction.getCloth(objCloth);
                for(String str: objCloth.mapClothFabric.keySet()){
                    if(objCloth.mapClothFabric.get(str)!=null){
                        loadFabric(objCloth.mapClothFabric.get(str), "Main");
                    }
                }
                loadCloth(objCloth);
                // setting heights and widths
                setWidthHeight();
                //set component sizes
                setComponentWidthHeight();
                //prepare components
                plotContainerGP();
                objClothNew = null;
            }
        } catch (SQLException ex) {
            new Logging("SEVERE",ClothView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",ClothView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
        System.gc();
    }
    private void saveMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONSAVEFILE"));
        if(isNew || objCloth.getStrClothAccess().equalsIgnoreCase("Public")){
            isNew = true;
        }
        saveUpdateAction();
        System.gc();
    }
    private void saveAsMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONSAVEASFILE"));
        isNew=true;
        saveUpdateAction();
    }
    /**
     * Cloth Name, Region, Description, Access (Public/Private/Protected) is set here
     */
    private void saveUpdateAction(){
        saveContext();
        boolean noLayoutEmpty= false;
        for(String str: objCloth.mapClothFabric.keySet()){
            if(objCloth.mapClothFabric.get(str)!=null && (objCloth.mapClothFabric.get(str) instanceof Fabric)){
                noLayoutEmpty=true;
                break;
            }
        }
        if(noLayoutEmpty){
            // if cloth access is Public, we need not show dialog for Save
            if(!isNew && objCloth.getStrClothAccess().equalsIgnoreCase("Public")){
                //updateCloth();
            } else {
                final Stage dialogStage = new Stage();
                dialogStage.initStyle(StageStyle.UTILITY);
                dialogStage.initModality(Modality.WINDOW_MODAL);
                
                GridPane popup=new GridPane();
                popup.setId("popup");
                popup.setAlignment(Pos.CENTER);
                popup.setHgap(5);
                popup.setVgap(5);
                popup.setPadding(new Insets(25, 25, 25, 25));

                final TextField txtName = new TextField();
                if(objCloth.getStrClothName()!=null)
                    txtName.setText(objCloth.getStrClothName());
                if(isNew){
                    Label lblName = new Label(objDictionaryAction.getWord("NAME"));
                    lblName.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPNAME")));
                    popup.add(lblName, 0, 0);
                    txtName.setPromptText(objDictionaryAction.getWord("TOOLTIPNAME"));
                    txtName.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPNAME")));
                    popup.add(txtName, 1, 0, 2, 1);
                }

                final TextField txtDescription = new TextField();
                if(isNew){
                    Label lblDescription = new Label(objDictionaryAction.getWord("DESCRIPTION"));
                    lblDescription.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPDESCRIPTION")));
                    popup.add(lblDescription, 0, 1);
                    txtDescription.setPromptText(objDictionaryAction.getWord("TOOLTIPDESCRIPTION"));
                    txtDescription.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPDESCRIPTION")));
                    popup.add(txtDescription, 1, 1, 2, 1);
                }

                final TextField txtCity = new TextField();
                if(isNew){
                    Label lblCity = new Label(objDictionaryAction.getWord("CITY"));
                    lblCity.setTooltip(new Tooltip(objDictionaryAction.getWord("CITY")));
                    popup.add(lblCity, 0, 2);
                    txtCity.setPromptText(objDictionaryAction.getWord("CITY"));
                    txtCity.setTooltip(new Tooltip(objDictionaryAction.getWord("CITY")));
                    txtCity.textProperty().addListener(new ChangeListener<String>() {
                        @Override
                        public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                            if(!t1.matches("[a-zA-Z ]*")) // if not alphabets, set old text
                                txtCity.setText(t);
                        }
                    });
                    popup.add(txtCity, 1, 2, 2, 1);
                }

                final ToggleGroup clothTG = new ToggleGroup();
                RadioButton clothPublicRB = new RadioButton(objDictionaryAction.getWord("PUBLIC"));
                clothPublicRB.setToggleGroup(clothTG);
                clothPublicRB.setUserData("Public");
                popup.add(clothPublicRB, 0, 3);
                RadioButton clothPrivateRB = new RadioButton(objDictionaryAction.getWord("PRIVATE"));
                clothPrivateRB.setToggleGroup(clothTG);
                clothPrivateRB.setUserData("Private");
                popup.add(clothPrivateRB, 1, 3);
                RadioButton clothProtectedRB = new RadioButton(objDictionaryAction.getWord("PROTECTED"));
                clothProtectedRB.setToggleGroup(clothTG);
                clothProtectedRB.setUserData("Protected");
                popup.add(clothProtectedRB, 2, 3);
                if(objConfiguration.getObjUser().getUserAccess("CLOTH_LIBRARY").equalsIgnoreCase("Public"))
                    clothTG.selectToggle(clothPublicRB);
                else if(objConfiguration.getObjUser().getUserAccess("CLOTH_LIBRARY").equalsIgnoreCase("Protected"))
                    clothTG.selectToggle(clothProtectedRB);
                else
                    clothTG.selectToggle(clothPrivateRB);
                
                if(!isNew && objCloth.getStrClothAccess()!=null){
                    if(objCloth.getStrClothAccess().equalsIgnoreCase("Public"))
                        clothTG.selectToggle(clothPublicRB);
                    else if(objCloth.getStrClothAccess().equalsIgnoreCase("Protected"))
                        clothTG.selectToggle(clothProtectedRB);
                    else
                        clothTG.selectToggle(clothPrivateRB);
                }

                final PasswordField passPF= new PasswordField();
                final Label lblAlert = new Label();
                if(objConfiguration.getBlnAuthenticateService()){
                    Label lblPassword=new Label(objDictionaryAction.getWord("SERVICEPASSWORD"));
                    lblPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                    popup.add(lblPassword, 0, 4);
                    passPF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                    passPF.setPromptText(objDictionaryAction.getWord("PROMPTSERVICEPASSWORD"));
                    popup.add(passPF, 1, 4, 2, 1);
                    lblAlert.setStyle("-fx-wrap-text:true;");
                    lblAlert.setPrefWidth(250);
                    popup.add(lblAlert, 0, 6, 3, 1);
                }

                Button btnSave = new Button(objDictionaryAction.getWord("SAVE"));
                btnSave.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSAVE")));
                btnSave.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
                btnSave.setDefaultButton(true);
                btnSave.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent e) {  
                        objCloth.setStrClothAccess(clothTG.getSelectedToggle().getUserData().toString());
                        //objCloth.setStrClothType("SAREE");
                        if(txtName.getText().trim().length()==0){
                            lblAlert.setText(objDictionaryAction.getWord("WRONGINPUT"));
                        } else{
                            if(objConfiguration.getBlnAuthenticateService()){
                                if(passPF.getText()!=null && passPF.getText()!="" && passPF.getText().trim().length()!=0){
                                    if(Security.SecurePassword(passPF.getText(), objConfiguration.getObjUser().getStrUsername()).equals(objConfiguration.getObjUser().getStrAppPassword())){
                                        saveUpdateCloth(txtName.getText(), txtCity.getText(), txtDescription.getText());
                                        dialogStage.close();
                                    } else{
                                        lblAlert.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                                    }
                                } else {                            
                                    lblAlert.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                                }
                            } else{   // service password is disabled
                                saveUpdateCloth(txtName.getText(), txtCity.getText(), txtDescription.getText());
                                dialogStage.close();
                            }
                        }
                    }
                });
                popup.add(btnSave, 1, 5);
                Button btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));
                btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
                btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
                btnCancel.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent e) {  
                        dialogStage.close();
                        lblStatus.setText(objDictionaryAction.getWord("TOOLTIPCANCEL"));
                   }
                });
                popup.add(btnCancel, 0, 5);
                Scene scene = new Scene(popup, 300, 220);
                scene.getStylesheets().add(ClothView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
                dialogStage.setScene(scene);
                dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("SAVE"));
                dialogStage.showAndWait();
            }
        }else{
            lblStatus.setText(objDictionaryAction.getWord("INVALIDCLOTHDATA"));
        }
    }
    /**
    * saveUpdateCloth
    * <p>
    * Function use for saving cloth
    * 
    * @exception (@throws )
    * @value
    * @author Amit Kumar Singh
    * @version     %I%, %G%
    * @since 1.0
    */    
    private void saveUpdateCloth(String strName,String strCity, String strDescription){
        try{
            byte oResult=0;
            //saveContext();
            getClothIcon();
            objClothAction=new ClothAction();                
            if(isNew){
                if(strName!=null && strName!="" && strName.trim().length()!=0)
                    objCloth.setStrClothName(strName.toString());
                else
                    objCloth.setStrClothName("My Cloth");
                if(strCity!=null && strCity!="" && strCity.trim().length()!=0)
                    objCloth.setStrClothRegion(strCity.toString());
                else
                    objCloth.setStrClothRegion("Varanasi");
                if(strDescription!=null && strDescription!="" && strDescription.trim().length()!=0)
                    objCloth.setStrClothDescription(strDescription.toString());
                else
                    objCloth.setStrClothDescription("No Description Available");
                String strClothId=new IDGenerator().getIDGenerator("CLOTH_LIBRARY", objConfiguration.getObjUser().getStrUserID());
                objCloth.setStrClothId(strClothId);
                
                oResult=objClothAction.setCloth(objCloth);
            } else{
                oResult=objClothAction.resetCloth(objCloth);
            }
            objConfiguration.setObjRecentCloth(objCloth);
            if(oResult==1){
                // clear and save components one by one
                if(!isNew){
                    objClothAction = new ClothAction();
                    objClothAction.clearClothFabric(objCloth.getStrClothId());                
                }
                //left border
                if(leftBorderEnabled && leftBorder.getUserData()!=null && (leftBorder.getUserData() instanceof Fabric)){
                    Fabric objFabric = (Fabric)leftBorder.getUserData();
                    objFabric.setStrClothType("Border");
                    saveFabric(objFabric,"Main");
                    objCloth.mapClothFabric.put("Left Border", objFabric);
                    objClothAction = new ClothAction();
                    objClothAction.setClothFabric(objCloth.getStrClothId(), "Left Border", objFabric.getStrFabricID(), objCloth.mapClothFabricDetails.get("Left Border"));
                } else if(leftBorderEnabled){
                    objCloth.mapClothFabric.put("Left Border", null);                    
                    objCloth.mapClothFabricDetails.put("Left Border", new String[]{"","90","Rectangular (Default)","1","0","0"});            
                    objClothAction = new ClothAction();
                    objClothAction.setClothFabric(objCloth.getStrClothId(), "Left Border", null, objCloth.mapClothFabricDetails.get("Left Border"));                    
                }
                //blouse
                if(blouseEnabled && blouse.getUserData()!=null && (blouse.getUserData() instanceof Fabric)){
                    Fabric objFabric = (Fabric)blouse.getUserData();
                    objFabric.setStrClothType("Blouse");
                    saveFabric(objFabric,"Main");
                    objCloth.mapClothFabric.put("Blouse", objFabric);
                    objClothAction = new ClothAction();
                    objClothAction.setClothFabric(objCloth.getStrClothId(), "Blouse", objFabric.getStrFabricID(), objCloth.mapClothFabricDetails.get("Blouse"));
                } else if(blouseEnabled){
                    objCloth.mapClothFabric.put("Blouse", null);
                    objCloth.mapClothFabricDetails.put("Blouse", new String[]{"","90","Rectangular (Default)","1","0","0"});            
                    objClothAction = new ClothAction();
                    objClothAction.setClothFabric(objCloth.getStrClothId(), "Blouse", null, objCloth.mapClothFabricDetails.get("Blouse"));                    
                }
                //Left side Left Cross Border
                if(leftsideLeftCrossBorderEnabled && leftsideLeftCrossBorder.getUserData()!=null && (leftsideLeftCrossBorder.getUserData() instanceof Fabric)){
                    Fabric objFabric = (Fabric)leftsideLeftCrossBorder.getUserData();
                    objFabric.setStrClothType("Cross Border");
                    saveFabric(objFabric,"Main");
                    objCloth.mapClothFabric.put("Leftside Left Cross Border", objFabric);
                    objClothAction = new ClothAction();
                    objClothAction.setClothFabric(objCloth.getStrClothId(), "Leftside Left Cross Border", objFabric.getStrFabricID(), objCloth.mapClothFabricDetails.get("Leftside Left Cross Border"));
                } else if(leftsideLeftCrossBorderEnabled){
                    objCloth.mapClothFabric.put("Leftside Left Cross Border", null);
                    objCloth.mapClothFabricDetails.put("Leftside Left Cross Border", new String[]{"","90","Rectangular (Default)","1","0","0"});            
                    objClothAction = new ClothAction();
                    objClothAction.setClothFabric(objCloth.getStrClothId(), "Leftside Left Cross Border", null, objCloth.mapClothFabricDetails.get("Leftside Left Cross Border"));
                }
                //left pallu
                if(leftPalluEnabled && leftPallu.getUserData()!=null && (leftPallu.getUserData() instanceof Fabric)){
                    Fabric objFabric = (Fabric)leftPallu.getUserData();
                    objFabric.setStrClothType("Pallu");
                    saveFabric(objFabric,"Main");
                    objCloth.mapClothFabric.put("Left Pallu", objFabric);
                    objClothAction = new ClothAction();
                    objClothAction.setClothFabric(objCloth.getStrClothId(), "Left Pallu", objFabric.getStrFabricID(), objCloth.mapClothFabricDetails.get("Left Pallu"));
                } else if(leftPalluEnabled){
                    objCloth.mapClothFabric.put("Left Pallu", null);            
                    objCloth.mapClothFabricDetails.put("Left Pallu", new String[]{"","90","Rectangular (Default)","1","0","0"});            
                    objClothAction = new ClothAction();
                    objClothAction.setClothFabric(objCloth.getStrClothId(), "Left Pallu", null, objCloth.mapClothFabricDetails.get("Left Pallu"));
                }
                //Left side Right Cross Border
                if(leftsideRightCrossBorderEnabled && leftsideRightCrossBorder.getUserData()!=null && (leftsideRightCrossBorder.getUserData() instanceof Fabric)){
                    Fabric objFabric = (Fabric)leftsideRightCrossBorder.getUserData();
                    objFabric.setStrClothType("Cross Border");
                    saveFabric(objFabric,"Main");
                    objCloth.mapClothFabric.put("Leftside Right Cross Border", objFabric);
                    objClothAction = new ClothAction();
                    objClothAction.setClothFabric(objCloth.getStrClothId(), "Leftside Right Cross Border", objFabric.getStrFabricID(), objCloth.mapClothFabricDetails.get("Leftside Right Cross Border"));
                } else if(leftsideRightCrossBorderEnabled){
                    objCloth.mapClothFabric.put("Leftside Right Cross Border", null);
                    objCloth.mapClothFabricDetails.put("Leftside Right Cross Border", new String[]{"","90","Rectangular (Default)","1","0","0"});            
                    objClothAction = new ClothAction();
                    objClothAction.setClothFabric(objCloth.getStrClothId(), "Leftside Right Cross Border", null, objCloth.mapClothFabricDetails.get("Leftside Right Cross Border"));
                }
                //Skirt
                if(skirtEnabled && skirt.getUserData()!=null && (skirt.getUserData() instanceof Fabric)){
                    Fabric objFabric = (Fabric)skirt.getUserData();
                    objFabric.setStrClothType("Skirt");
                    saveFabric(objFabric,"Main");
                    objCloth.mapClothFabric.put("Skirt", objFabric);
                    objClothAction = new ClothAction();
                    objClothAction.setClothFabric(objCloth.getStrClothId(), "Skirt", objFabric.getStrFabricID(), objCloth.mapClothFabricDetails.get("Skirt"));
                } else if(skirtEnabled){
                    objCloth.mapClothFabric.put("Skirt", null);            
                    objCloth.mapClothFabricDetails.put("Skirt", new String[]{"","90","Rectangular (Default)","1","0","0"});            
                    objClothAction = new ClothAction();
                    objClothAction.setClothFabric(objCloth.getStrClothId(), "Skirt", null, objCloth.mapClothFabricDetails.get("Skirt"));
                }
                //Right side Left Cross Border
                if(rightsideLeftCrossBorderEnabled && rightsideLeftCrossBorder.getUserData()!=null && (rightsideLeftCrossBorder.getUserData() instanceof Fabric)){
                    Fabric objFabric = (Fabric)rightsideLeftCrossBorder.getUserData();
                    objFabric.setStrClothType("Cross Border");
                    saveFabric(objFabric,"Main");
                    objCloth.mapClothFabric.put("Rightside Left Cross Border", objFabric);
                    objClothAction = new ClothAction();
                    objClothAction.setClothFabric(objCloth.getStrClothId(), "Rightside Left Cross Border", objFabric.getStrFabricID(), objCloth.mapClothFabricDetails.get("Rightside Left Cross Border"));
                } else if(rightsideLeftCrossBorderEnabled){
                    objCloth.mapClothFabric.put("Rightside Left Cross Border", null);            
                    objCloth.mapClothFabricDetails.put("Rightside Left Cross Border", new String[]{"","90","Rectangular (Default)","1","0","0"});            
                    objClothAction = new ClothAction();
                    objClothAction.setClothFabric(objCloth.getStrClothId(), "Rightside Left Cross Border", null, objCloth.mapClothFabricDetails.get("Rightside Left Cross Border"));
                }
                //Right Pallu
                if(rightPalluEnabled && rightPallu.getUserData()!=null && (rightPallu.getUserData() instanceof Fabric)){
                    Fabric objFabric = (Fabric)rightPallu.getUserData();
                    objFabric.setStrClothType("Pallu");
                    saveFabric(objFabric,"Main");
                    objCloth.mapClothFabric.put("Right Pallu", objFabric);
                    objClothAction = new ClothAction();
                    objClothAction.setClothFabric(objCloth.getStrClothId(), "Right Pallu", objFabric.getStrFabricID(), objCloth.mapClothFabricDetails.get("Right Pallu"));
                } else if(rightPalluEnabled){
                    objCloth.mapClothFabric.put("Right Pallu", null);            
                    objCloth.mapClothFabricDetails.put("Right Pallu", new String[]{"","90","Rectangular (Default)","1","0","0"});            
                    objClothAction = new ClothAction();
                    objClothAction.setClothFabric(objCloth.getStrClothId(), "Right Pallu", null, objCloth.mapClothFabricDetails.get("Right Pallu"));
                }
                //Right side Right Cross Border
                if(rightsideRightCrossBorderEnabled && rightsideRightCrossBorder.getUserData()!=null && (rightsideRightCrossBorder.getUserData() instanceof Fabric)){
                    Fabric objFabric = (Fabric)rightsideRightCrossBorder.getUserData();
                    objFabric.setStrClothType("Cross Border");
                    saveFabric(objFabric,"Main");
                    objCloth.mapClothFabric.put("Rightside Right Cross Border", objFabric);
                    objClothAction = new ClothAction();
                    objClothAction.setClothFabric(objCloth.getStrClothId(), "Rightside Right Cross Border", objFabric.getStrFabricID(), objCloth.mapClothFabricDetails.get("Rightside Right Cross Border"));
                } else if(rightsideRightCrossBorderEnabled){
                    objCloth.mapClothFabric.put("Rightside Right Cross Border", null);            
                    objCloth.mapClothFabricDetails.put("Rightside Right Cross Border", new String[]{"","90","Rectangular (Default)","1","0","0"});            
                    objClothAction = new ClothAction();
                    objClothAction.setClothFabric(objCloth.getStrClothId(), "Rightside Right Cross Border", null, objCloth.mapClothFabricDetails.get("Rightside Right Cross Border"));
                }
                //Right Border
                if(rightBorderEnabled && rightBorder.getUserData()!=null && (rightBorder.getUserData() instanceof Fabric)){
                    Fabric objFabric = (Fabric)rightBorder.getUserData();
                    objFabric.setStrClothType("Border");
                    saveFabric(objFabric,"Main");
                    objCloth.mapClothFabric.put("Right Border", objFabric);
                    objClothAction = new ClothAction();
                    objClothAction.setClothFabric(objCloth.getStrClothId(), "Right Border", objFabric.getStrFabricID(), objCloth.mapClothFabricDetails.get("Right Border"));
                } else if(rightBorderEnabled){
                    objCloth.mapClothFabric.put("Right Border", null);
                    objCloth.mapClothFabricDetails.put("Right Border", new String[]{"","90","Rectangular (Default)","1","0","0"});            
                    objClothAction = new ClothAction();
                    objClothAction.setClothFabric(objCloth.getStrClothId(), "Right Border", null, objCloth.mapClothFabricDetails.get("Right Border"));
                }
                //body always enable
                if(body.getUserData()!=null && (body.getUserData() instanceof Fabric)){
                    Fabric objFabric = (Fabric)body.getUserData();
                    objFabric.setStrClothType("Body");
                    saveFabric(objFabric,"Main");
                    objCloth.mapClothFabric.put("Body", objFabric);
                    objClothAction = new ClothAction();
                    objClothAction.setClothFabric(objCloth.getStrClothId(), "Body", objFabric.getStrFabricID(), objCloth.mapClothFabricDetails.get("Body"));
                } else{
                    objCloth.mapClothFabric.put("Body", null);
                    objCloth.mapClothFabricDetails.put("Body", new String[]{"","90","Rectangular (Default)","1","0","0"});
                    objClothAction = new ClothAction();
                    objClothAction.setClothFabric(objCloth.getStrClothId(), "Body", null, objCloth.mapClothFabricDetails.get("Body"));
                }        
                lblStatus.setText(objCloth.getStrClothName()+" : "+objDictionaryAction.getWord("DATASAVED"));
            } else{
                //objClothAction = new ClothAction();
                //objClothAction.clearClothFabric(objCloth.getStrClothId());
                lblStatus.setText(objCloth.getStrClothName()+" : "+objDictionaryAction.getWord("DATAUNSAVED"));
            }
            isNew = false;
        } catch (SQLException ex) {
            Logger.getLogger(ClothView.class.getName()).log(Level.SEVERE, null, ex);
            new Logging("SEVERE",ClothView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            Logger.getLogger(ClothView.class.getName()).log(Level.SEVERE, null, ex);
            new Logging("SEVERE",ClothView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void getClothIcon(){
        try{
            WritableImage image = bodyContainer.snapshot(new SnapshotParameters(), null);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", baos);
            baos.flush();
            byte[] imageInByte = baos.toByteArray();
            objCloth.setBytClothIcon(imageInByte);
        } catch (Exception ex) {
            new Logging("SEVERE",ClothView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void exportHtmlAction(){
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
            lblStatus.setText(objDictionaryAction.getWord("ACTIONSAVEXMLFILE"));
            
            FileChooser fileChoser=new FileChooser();
            fileChoser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTHTML")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
            FileChooser.ExtensionFilter extFilterHTML = new FileChooser.ExtensionFilter("HTML (*.html)", "*.html");
            fileChoser.getExtensionFilters().add(extFilterHTML);
            File ifile=fileChoser.showSaveDialog(clothStage);
            if(ifile==null)
                return;
            else
                clothStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+ifile.getAbsoluteFile().getName()+"]");
            
            if(leftBorderEnabled && (leftBorder.getUserData()!=null && (leftBorder.getUserData() instanceof Fabric)))
                saveAsHtml((Fabric)leftBorder.getUserData(),ifile);
            if(blouseEnabled && (blouse.getUserData()!=null && (blouse.getUserData() instanceof Fabric)))
                saveAsHtml((Fabric)blouse.getUserData(),ifile);
            if(leftsideLeftCrossBorderEnabled && (leftsideLeftCrossBorder.getUserData()!=null && (leftsideLeftCrossBorder.getUserData() instanceof Fabric)))
                saveAsHtml((Fabric)leftsideLeftCrossBorder.getUserData(),ifile);
            if(leftPalluEnabled && (leftPallu.getUserData()!=null && (leftPallu.getUserData() instanceof Fabric)))
                saveAsHtml((Fabric)leftPallu.getUserData(),ifile);
            if(leftsideRightCrossBorderEnabled && (leftsideRightCrossBorder.getUserData()!=null && (leftsideRightCrossBorder.getUserData() instanceof Fabric)))
                saveAsHtml((Fabric)leftsideRightCrossBorder.getUserData(),ifile);
            if(body.getUserData()!=null && (body.getUserData() instanceof Fabric))
                saveAsHtml((Fabric)body.getUserData(),ifile);
            if(skirtEnabled && (skirt.getUserData()!=null && (skirt.getUserData() instanceof Fabric)))
                saveAsHtml((Fabric)skirt.getUserData(),ifile);
            if(rightsideLeftCrossBorderEnabled && (rightsideLeftCrossBorder.getUserData()!=null && (rightsideLeftCrossBorder.getUserData() instanceof Fabric)))
                saveAsHtml((Fabric)rightsideLeftCrossBorder.getUserData(),ifile);
            if(rightPalluEnabled && (rightPallu.getUserData()!=null && (rightPallu.getUserData() instanceof Fabric)))
                saveAsHtml((Fabric)rightPallu.getUserData(),ifile);
            if(rightsideRightCrossBorderEnabled && (rightsideRightCrossBorder.getUserData()!=null && (rightsideRightCrossBorder.getUserData() instanceof Fabric)))
                saveAsHtml((Fabric)rightsideRightCrossBorder.getUserData(),ifile);
            if(rightBorderEnabled && (rightBorder.getUserData()!=null && (rightBorder.getUserData() instanceof Fabric)))
                saveAsHtml((Fabric)rightBorder.getUserData(),ifile);
            lblStatus.setText(objDictionaryAction.getWord("DATASAVED"));
        }
    }
    private void exportTextureAction(){
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
            lblStatus.setText(objDictionaryAction.getWord("ACTIONSAVETEXTUREFILE"));
            ScreenShoot("export");
            lblStatus.setText(objDictionaryAction.getWord("DATASAVED"));
        }
    }    
    private void exportGraphAction(){
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
            lblStatus.setText(objDictionaryAction.getWord("ACTIONSAVEGRAPHFILE"));
            
            FileChooser fileChoser=new FileChooser();
            fileChoser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTBMP")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
            FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("BMP (.bmp)", "*.bmp");
            fileChoser.getExtensionFilters().add(extFilterBMP);
            File ifile=fileChoser.showSaveDialog(clothStage);
            if(ifile==null)
                return;
            else
                clothStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+ifile.getAbsoluteFile().getName()+"]");
            
            if(leftBorderEnabled && (leftBorder.getUserData()!=null && (leftBorder.getUserData() instanceof Fabric)))
                saveAsGraph((Fabric)leftBorder.getUserData(),ifile, 1);
            if(blouseEnabled && (blouse.getUserData()!=null && (blouse.getUserData() instanceof Fabric)))
                saveAsGraph((Fabric)blouse.getUserData(),ifile, 2);
            if(leftsideLeftCrossBorderEnabled && (leftsideLeftCrossBorder.getUserData()!=null && (leftsideLeftCrossBorder.getUserData() instanceof Fabric)))
                saveAsGraph((Fabric)leftsideLeftCrossBorder.getUserData(),ifile, 3);
            if(leftPalluEnabled && (leftPallu.getUserData()!=null && (leftPallu.getUserData() instanceof Fabric)))
                saveAsGraph((Fabric)leftPallu.getUserData(),ifile, 4);
            if(leftsideRightCrossBorderEnabled && (leftsideRightCrossBorder.getUserData()!=null && (leftsideRightCrossBorder.getUserData() instanceof Fabric)))
                saveAsGraph((Fabric)leftsideRightCrossBorder.getUserData(),ifile, 5);
            if(body.getUserData()!=null && (body.getUserData() instanceof Fabric))
                saveAsGraph((Fabric)body.getUserData(),ifile, 6);
            if(skirtEnabled && (skirt.getUserData()!=null && (skirt.getUserData() instanceof Fabric)))
                saveAsGraph((Fabric)skirt.getUserData(),ifile, 7);
            if(rightsideLeftCrossBorderEnabled && (rightsideLeftCrossBorder.getUserData()!=null && (rightsideLeftCrossBorder.getUserData() instanceof Fabric)))
                saveAsGraph((Fabric)rightsideLeftCrossBorder.getUserData(),ifile, 8);
            if(rightPalluEnabled && (rightPallu.getUserData()!=null && (rightPallu.getUserData() instanceof Fabric)))
                saveAsGraph((Fabric)rightPallu.getUserData(),ifile, 9);
            if(rightsideRightCrossBorderEnabled && (rightsideRightCrossBorder.getUserData()!=null && (rightsideRightCrossBorder.getUserData() instanceof Fabric)))
                saveAsGraph((Fabric)rightsideRightCrossBorder.getUserData(),ifile, 10);
            if(rightBorderEnabled && (rightBorder.getUserData()!=null && (rightBorder.getUserData() instanceof Fabric)))
                saveAsGraph((Fabric)rightBorder.getUserData(),ifile, 11);
            lblStatus.setText(objDictionaryAction.getWord("DATASAVED"));
        }
    }    
    private void printSetupAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONPRINTSETUPFILE"));
        objPrinterJob.pageDialog(objPrinterAttribute);
    }    
    private void printMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONPRINTFILE"));
        ScreenShoot("print");
    }
    private void undoAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONUNDO"));
        try {
            if(objUR.canUndo()){
                objUR.undo();
                objCloth = (Cloth)objUR.getObjData();
                Cloth clonedCloth = new Cloth();
                cloneCloth(clonedCloth);
                objCloth=clonedCloth;
                objCloth.setObjConfiguration(objConfiguration);
                
                disableAllComponent();
                // setting heights and widths
                setWidthHeight();
                // initialize components and Panes
                initializePane();
                loadCloth(objCloth);
                // setting heights and widths
                setWidthHeight();
                //set component sizes
                setComponentWidthHeight();
                //prepare components
                plotContainerGP();    
                
                lblStatus.setText(objDictionaryAction.getWord("UNDO")+":"+objDictionaryAction.getWord("SUCCESS"));
            }else{
                lblStatus.setText(objDictionaryAction.getWord("MAXUNDO"));
            }
        } catch (Exception ex) {
            new Logging("SEVERE",ClothView.class.getName(),"Operation Undo",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void redoAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONREDO"));
        try {
            if(objUR.canRedo()){
                objUR.redo();
                objCloth = (Cloth)objUR.getObjData();
                //Cloth clonedCloth = new Cloth();
                //cloneCloth(clonedCloth);
                //objCloth=clonedCloth;
                objCloth.setObjConfiguration(objConfiguration);
                
                disableAllComponent();
                // setting heights and widths
                setWidthHeight();
                // initialize components and Panes
                initializePane();
                loadCloth(objCloth);
                // setting heights and widths
                setWidthHeight();
                //set component sizes
                setComponentWidthHeight();
                //prepare components
                plotContainerGP();
                
                lblStatus.setText(objDictionaryAction.getWord("REDO")+":"+objDictionaryAction.getWord("SUCCESS"));
            }else{
                lblStatus.setText(objDictionaryAction.getWord("MAXREDO"));
            }
        } catch (Exception ex) {
            new Logging("SEVERE",ClothView.class.getName(),"Operation Redo",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void symmetryMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONSYMMETRY"));
        
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setAlignment(Pos.CENTER);
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        
        popup.add(new Label(objDictionaryAction.getWord("SYMMETRY")), 0, 0, 2, 1);
        
        final CheckBox linkPalluCB = new CheckBox(objDictionaryAction.getWord("PALLU"));
        linkPalluCB.setSelected(palluLinkPolicy);
        final CheckBox leftPalluCB = new CheckBox(objDictionaryAction.getWord("LEFTPALUCB"));
        leftPalluCB.setSelected(palluLinkPolicy);
        leftPalluCB.setDisable(true);
        final CheckBox rightPalluCB = new CheckBox(objDictionaryAction.getWord("RIGHTPALUCB"));
        rightPalluCB.setSelected(palluLinkPolicy);
        rightPalluCB.setDisable(true);
        if(isPalluContextValid()){
            popup.add(linkPalluCB, 0, 1, 1, 1);
            popup.add(leftPalluCB, 1, 2, 1, 1);
            popup.add(rightPalluCB, 1, 3, 1, 1);
        }
        
        final CheckBox linkBorderCB = new CheckBox(objDictionaryAction.getWord("BORDER"));
        linkBorderCB.setSelected(borderLinkPolicy);
        final CheckBox leftBorderCB = new CheckBox(objDictionaryAction.getWord("LEFTBORDERCB"));
        leftBorderCB.setSelected(borderLinkPolicy);
        leftBorderCB.setDisable(true);
        final CheckBox rightBorderCB = new CheckBox(objDictionaryAction.getWord("RIGHTBORDERCB"));
        rightBorderCB.setSelected(borderLinkPolicy);
        rightBorderCB.setDisable(true);
        if(isBorderContextValid()){
            popup.add(linkBorderCB, 0, 4, 1, 1);
            popup.add(leftBorderCB, 1, 5, 1, 1);
            popup.add(rightBorderCB, 1, 6, 1, 1);
        }
        
        final CheckBox linkCrossBorderCB = new CheckBox(objDictionaryAction.getWord("CROSSBORDER"));
        linkCrossBorderCB.setSelected(crossBorderLinkPolicy);
        final CheckBox leftsideLeftCrossBorderCB = new CheckBox(objDictionaryAction.getWord("LEFTSIDELEFTCROSSBORDERCB"));
        leftsideLeftCrossBorderCB.setSelected(crossBorderLinkPolicy);
        leftsideLeftCrossBorderCB.setDisable(true);
        final CheckBox leftsideRightCrossBorderCB = new CheckBox(objDictionaryAction.getWord("LEFTSIDERIGHTCROSSBORDERCB"));
        leftsideRightCrossBorderCB.setSelected(crossBorderLinkPolicy);
        leftsideRightCrossBorderCB.setDisable(true);
        final CheckBox rightsideLeftCrossBorderCB = new CheckBox(objDictionaryAction.getWord("RIGHTSIDELEFTCROSSBORDERCB"));
        rightsideLeftCrossBorderCB.setSelected(crossBorderLinkPolicy);
        rightsideLeftCrossBorderCB.setDisable(true);
        final CheckBox rightsideRightCrossBorderCB = new CheckBox(objDictionaryAction.getWord("RIGHTSIDERIGHTCROSSBORDERCB"));
        rightsideRightCrossBorderCB.setSelected(crossBorderLinkPolicy);
        rightsideRightCrossBorderCB.setDisable(true);
        if(isCrossBorderContextValid()){
            popup.add(linkCrossBorderCB, 0, 7, 1, 1);
            popup.add(leftsideLeftCrossBorderCB, 1, 8, 1, 1);
            popup.add(leftsideRightCrossBorderCB, 1, 9, 1, 1);
            popup.add(rightsideLeftCrossBorderCB, 1, 10, 1, 1);
            popup.add(rightsideRightCrossBorderCB, 1, 11, 1, 1);
        }
        
        final CheckBox linkKoniaCB = new CheckBox(objDictionaryAction.getWord("KONIA"));
        linkKoniaCB.setSelected(false);
        final CheckBox leftTopKoniaCB = new CheckBox(objDictionaryAction.getWord("LEFTTOPKONIACB"));
        leftTopKoniaCB.setSelected(false);
        final CheckBox rightTopKoniaCB = new CheckBox(objDictionaryAction.getWord("RIGHTTOPKONIACB"));
        rightTopKoniaCB.setSelected(false);
        final CheckBox leftBottomKoniaCB = new CheckBox(objDictionaryAction.getWord("LEFTBOTTOMKONIACB"));
        leftBottomKoniaCB.setSelected(false);
        final CheckBox rightBottomKoniaCB = new CheckBox(objDictionaryAction.getWord("RIGHTBOTTOMKONIACB"));
        rightBottomKoniaCB.setSelected(false);
        
        popup.add(linkKoniaCB, 0, 12, 1, 1);
        popup.add(leftTopKoniaCB, 1, 13, 1, 1);
        popup.add(rightTopKoniaCB, 1, 14, 1, 1);
        popup.add(leftBottomKoniaCB, 1, 15, 1, 1);
        popup.add(rightBottomKoniaCB, 1, 16, 1, 1);
        
        leftTopKoniaCB.setDisable(true);
        rightTopKoniaCB.setDisable(true);
        leftBottomKoniaCB.setDisable(true);
        rightBottomKoniaCB.setDisable(true);
        
        if(objCloth.getStrClothType().equalsIgnoreCase("SAREE")){
            rightsideLeftCrossBorderCB.setDisable(true);
            rightsideRightCrossBorderCB.setDisable(true);
            rightPalluCB.setDisable(true);
            
            rightsideLeftCrossBorderCB.setSelected(false);
            rightsideRightCrossBorderCB.setSelected(false);
            rightPalluCB.setSelected(false);
        }
                
        linkPalluCB.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                leftPalluCB.setSelected(t1);
                if(objCloth.getStrClothType().equalsIgnoreCase("SAREE"))
                    rightPalluCB.setSelected(false);
                else
                    rightPalluCB.setSelected(t1);
            }
        });
        linkBorderCB.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                leftBorderCB.setSelected(t1);
                rightBorderCB.setSelected(t1);
            }
        });
        linkCrossBorderCB.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                leftsideLeftCrossBorderCB.setSelected(t1);
                leftsideRightCrossBorderCB.setSelected(t1);
                if(objCloth.getStrClothType().equalsIgnoreCase("SAREE")){
                    rightsideLeftCrossBorderCB.setSelected(false);
                    rightsideRightCrossBorderCB.setSelected(false);
                } else {
                    rightsideLeftCrossBorderCB.setSelected(t1);
                    rightsideRightCrossBorderCB.setSelected(t1);
                }
            }
        });
        
        Button btnApply = new Button(objDictionaryAction.getWord("APPLY"));
        btnApply.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save_as.png"));
        btnApply.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPAPPLY")));
        //btnApply.setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);       
        btnApply.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e){
                palluLinkPolicy=linkPalluCB.isSelected();
                borderLinkPolicy=linkBorderCB.isSelected();
                crossBorderLinkPolicy=linkCrossBorderCB.isSelected();
    
                lblStatus.setText(objDictionaryAction.getWord("ACTIONAPPLY"));
                System.gc();
                dialogStage.close();
            }
        });
        popup.add(btnApply, 0, 17);

        Button btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));
        btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
        btnCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                dialogStage.close();
            }
        });
        popup.add(btnCancel, 1, 17);

        Scene scene = new Scene(popup);
        scene.getStylesheets().add(ClothView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        dialogStage.setScene(scene);
        dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("SYMMETRY"));
        dialogStage.showAndWait();
    }
    
    private void repeatsMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONREPEATS"));
        
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setAlignment(Pos.CENTER);
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));

        popup.add(new Label(objDictionaryAction.getWord("REPEATS")), 0, 0, 2, 1);
        
        Label lblLeftBorder= new Label(objDictionaryAction.getWord("LEFTBORDER"));
        final TextField txtLeftBorder = new TextField(String.valueOf(repeats[0])){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        popup.add(lblLeftBorder, 0, 1);
        popup.add(txtLeftBorder, 1, 1);
        txtLeftBorder.setDisable(!leftBorderEnabled);
        
        Label lblRightBorder= new Label(objDictionaryAction.getWord("RIGHTBORDER"));
        final TextField txtRightBorder = new TextField(String.valueOf(repeats[10])){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        popup.add(lblRightBorder, 0, 2);
        popup.add(txtRightBorder, 1, 2);
        txtRightBorder.setDisable(!rightBorderEnabled);
        
        Label lblLeftSideLeftCrossBorder= new Label(objDictionaryAction.getWord("LEFTSIDELEFTCROSSBORDER"));
        final TextField txtLeftSideLeftCrossBorder = new TextField(String.valueOf(repeats[2])){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        popup.add(lblLeftSideLeftCrossBorder, 0, 3);
        popup.add(txtLeftSideLeftCrossBorder, 1, 3);
        txtLeftSideLeftCrossBorder.setDisable(!leftsideLeftCrossBorderEnabled);
        
        Label lblLeftSideRightCrossBorder= new Label(objDictionaryAction.getWord("LEFTSIDERIGHTCROSSBORDER"));
        final TextField txtLeftSideRightCrossBorder = new TextField(String.valueOf(repeats[4])){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        popup.add(lblLeftSideRightCrossBorder, 0, 4);
        popup.add(txtLeftSideRightCrossBorder, 1, 4);
        txtLeftSideRightCrossBorder.setDisable(!leftsideRightCrossBorderEnabled);
        
        Label lblRightSideLeftCrossBorder= new Label(objDictionaryAction.getWord("RIGHTSIDELEFTCROSSBORDER"));
        final TextField txtRightSideLeftCrossBorder = new TextField(String.valueOf(repeats[7])){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        popup.add(lblRightSideLeftCrossBorder, 0, 5);
        popup.add(txtRightSideLeftCrossBorder, 1, 5);
        txtRightSideLeftCrossBorder.setDisable(!rightsideLeftCrossBorderEnabled);
        
        Label lblRightSideRightCrossBorder= new Label(objDictionaryAction.getWord("RIGHTSIDERIGHTCROSSBORDER"));
        final TextField txtRightSideRightCrossBorder = new TextField(String.valueOf(repeats[9])){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        popup.add(lblRightSideRightCrossBorder, 0, 6);
        popup.add(txtRightSideRightCrossBorder, 1, 6);
        txtRightSideRightCrossBorder.setDisable(!rightsideRightCrossBorderEnabled);
        
        Label lblLeftPallu= new Label(objDictionaryAction.getWord("LEFTPALLU"));
        final TextField txtLeftPallu = new TextField(String.valueOf(repeats[3])){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        popup.add(lblLeftPallu, 0, 7);
        popup.add(txtLeftPallu, 1, 7);
        txtLeftPallu.setDisable(!leftPalluEnabled);
        
        Label lblRightPallu= new Label(objDictionaryAction.getWord("RIGHTPALLU"));
        final TextField txtRightPallu = new TextField(String.valueOf(repeats[8])){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        popup.add(lblRightPallu, 0, 8);
        popup.add(txtRightPallu, 1, 8);
        txtRightPallu.setDisable(!rightPalluEnabled);
        
        Label lblBlouse= new Label(objDictionaryAction.getWord("BLOUSE"));
        final TextField txtBlouse = new TextField(String.valueOf(repeats[1])){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        popup.add(lblBlouse, 0, 9);
        popup.add(txtBlouse, 1, 9);
        txtBlouse.setDisable(!blouseEnabled);
        
        Label lblSkirt= new Label(objDictionaryAction.getWord("SKIRT"));
        final TextField txtSkirt = new TextField(String.valueOf(repeats[6])){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        popup.add(lblSkirt, 0, 10);
        popup.add(txtSkirt, 1, 10);
        txtSkirt.setDisable(!skirtEnabled);

        txtLeftBorder.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                //if(!txtLeftBorder.isFocused()){
                    if(txtLeftBorder.getText()!=null && txtLeftBorder.getText().trim()!="" && txtLeftBorder.getText().trim().length()>0){
                        if(Integer.parseInt(txtLeftBorder.getText().trim())<1)
                            txtLeftBorder.setText(t);
                        else if((heights[0]*Integer.parseInt(t1))/Integer.parseInt(t)>MAXBORDER)
                            txtLeftBorder.setText(t);
                    } else{
                        txtLeftBorder.setText(t);
                    }
                //}
            }
        });
        txtRightBorder.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                //if(!txtRightBorder.isFocused()){
                    if(txtRightBorder.getText()!=null && txtRightBorder.getText().trim()!="" && txtRightBorder.getText().trim().length()>0){
                        if(Integer.parseInt(txtRightBorder.getText().trim())<1)
                            txtRightBorder.setText(t);
                        else if((heights[10]*Integer.parseInt(t1))/Integer.parseInt(t)>MAXBORDER)
                            txtRightBorder.setText(t);
                    } else{
                        txtRightBorder.setText(t);
                    }
                //}
            }
        });
        txtLeftSideLeftCrossBorder.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                //if(!txtLeftSideLeftCrossBorder.isFocused()){
                    if(txtLeftSideLeftCrossBorder.getText()!=null && txtLeftSideLeftCrossBorder.getText().trim()!="" && txtLeftSideLeftCrossBorder.getText().trim().length()>0){
                        if(Integer.parseInt(txtLeftSideLeftCrossBorder.getText().trim())<1)
                            txtLeftSideLeftCrossBorder.setText(t);
                        else if((widths[2]*Integer.parseInt(t1))/Integer.parseInt(t)>MAXBORDER)
                            txtLeftSideLeftCrossBorder.setText(t);
                    } else {
                        txtLeftSideLeftCrossBorder.setText(t);
                    }
                //}
            }
        });        
        txtLeftSideRightCrossBorder.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                //if(!txtLeftSideRightCrossBorder.isFocused()){
                    if(txtLeftSideRightCrossBorder.getText()!=null && txtLeftSideRightCrossBorder.getText().trim()!="" && txtLeftSideRightCrossBorder.getText().trim().length()>0){
                        if(Integer.parseInt(txtLeftSideRightCrossBorder.getText().trim())<1)
                            txtLeftSideRightCrossBorder.setText(t);
                         else if((widths[4]*Integer.parseInt(t1))/Integer.parseInt(t)>MAXBORDER)
                            txtLeftSideRightCrossBorder.setText(t);
                    } else {
                        txtLeftSideRightCrossBorder.setText(t);
                    }
                //}
            }
        });
        txtRightSideLeftCrossBorder.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                //if(!txtRightSideLeftCrossBorder.isFocused()){
                    if(txtRightSideLeftCrossBorder.getText()!=null && txtLeftSideLeftCrossBorder.getText().trim()!="" && txtRightSideLeftCrossBorder.getText().trim().length()>0){
                        if(Integer.parseInt(txtRightSideLeftCrossBorder.getText().trim())<1)
                            txtRightSideLeftCrossBorder.setText(t);
                         else if((widths[7]*Integer.parseInt(t1))/Integer.parseInt(t)>MAXBORDER)
                            txtRightSideLeftCrossBorder.setText(t);
                    } else {
                        txtRightSideLeftCrossBorder.setText(t);
                    }
                //}
            }
        });
        txtRightSideRightCrossBorder.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                //if(!txtRightSideRightCrossBorder.isFocused()){
                    if(txtRightSideRightCrossBorder.getText()!=null && txtRightSideRightCrossBorder.getText().trim()=="" && txtRightSideRightCrossBorder.getText().trim().length()>0){
                        if(Integer.parseInt(txtRightSideRightCrossBorder.getText().trim())<1)
                            txtRightSideRightCrossBorder.setText(t);
                         else if((widths[9]*Integer.parseInt(t1))/Integer.parseInt(t)>MAXBORDER)
                            txtRightSideRightCrossBorder.setText(t);
                    } else {
                        txtRightSideRightCrossBorder.setText(t);
                    }
                //}
            }
        });
        txtLeftPallu.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                //if(!txtLeftPallu.isFocused()){
                    if(txtLeftPallu.getText()!=null && txtLeftPallu.getText().trim()!="" && txtLeftPallu.getText().trim().length()>0){
                        if(Integer.parseInt(txtLeftPallu.getText().trim())<1)
                            txtLeftPallu.setText(t);
                         else if((widths[3]*Integer.parseInt(t1))/Integer.parseInt(t)>MAXPALLU)
                            txtLeftPallu.setText(t);
                    } else {
                        txtLeftPallu.setText(t);
                    }
                //}
            }
        });
        txtRightPallu.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                //if(!txtRightPallu.isFocused()){
                    if(txtRightPallu.getText()!=null && txtRightPallu.getText().trim()!="" && txtRightPallu.getText().trim().length()>0){
                        if(Integer.parseInt(txtRightPallu.getText().trim())<1)
                            txtRightPallu.setText(t);
                         else if((widths[8]*Integer.parseInt(t1))/Integer.parseInt(t)>MAXPALLU)
                            txtRightPallu.setText(t);
                    } else {
                        txtRightPallu.setText(t);
                    }
                //}
            }
        });
        txtBlouse.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                //if(!txtBlouse.isFocused()){
                    if(txtBlouse.getText()!=null && txtBlouse.getText().trim()!="" && txtBlouse.getText().trim().length()>0){
                        if(Integer.parseInt(txtBlouse.getText().trim())<1)
                            txtBlouse.setText(t);
                         else if((widths[1]*Integer.parseInt(t1))/Integer.parseInt(t)>MAXPALLU)
                            txtBlouse.setText(t);
                    } else {
                        txtBlouse.setText(t);
                    }
                //}
            }
        });
        txtSkirt.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                //if(!txtSkirt.isFocused()){
                    if(txtSkirt.getText()!=null && txtSkirt.getText().trim()!="" && txtSkirt.getText().trim().length()>0){
                        if(Integer.parseInt(txtSkirt.getText().trim())<1)
                            txtSkirt.setText(t);
                        else if((heights[6]*Integer.parseInt(t1))/Integer.parseInt(t)>MAXBORDER)
                            txtSkirt.setText(t);
                    } else{
                        txtSkirt.setText(t);
                    }
                //}
            }
        });
        
        Button btnApply = new Button(objDictionaryAction.getWord("APPLY"));
        btnApply.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
        btnApply.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPAPPLY")));
        btnApply.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e){
                try{
                    if(leftBorderEnabled)
                        repeats[0]=(Integer.parseInt(txtLeftBorder.getText())>0?Integer.parseInt(txtLeftBorder.getText()):1);
                    if(rightBorderEnabled)
                        repeats[10]=(Integer.parseInt(txtRightBorder.getText())>0?Integer.parseInt(txtRightBorder.getText()):1);
                    if(leftsideLeftCrossBorderEnabled)
                        repeats[2]=(Integer.parseInt(txtLeftSideLeftCrossBorder.getText())>0?Integer.parseInt(txtLeftSideLeftCrossBorder.getText()):1);
                    if(leftsideRightCrossBorderEnabled)
                        repeats[4]=(Integer.parseInt(txtLeftSideRightCrossBorder.getText())>0?Integer.parseInt(txtLeftSideRightCrossBorder.getText()):1);
                    if(rightsideLeftCrossBorderEnabled)
                        repeats[7]=(Integer.parseInt(txtRightSideLeftCrossBorder.getText())>0?Integer.parseInt(txtRightSideLeftCrossBorder.getText()):1);
                    if(rightsideRightCrossBorderEnabled)
                        repeats[9]=(Integer.parseInt(txtRightSideRightCrossBorder.getText())>0?Integer.parseInt(txtRightSideRightCrossBorder.getText()):1);
                    if(leftPalluEnabled)
                        repeats[3]=(Integer.parseInt(txtLeftPallu.getText())>0?Integer.parseInt(txtLeftPallu.getText()):1);
                    if(rightPalluEnabled)
                        repeats[8]=(Integer.parseInt(txtRightPallu.getText())>0?Integer.parseInt(txtRightPallu.getText()):1);
                    if(blouseEnabled)
                        repeats[1]=(Integer.parseInt(txtBlouse.getText())>0?Integer.parseInt(txtBlouse.getText()):1);
                    if(skirtEnabled)
                        repeats[6]=(Integer.parseInt(txtSkirt.getText())>0?Integer.parseInt(txtSkirt.getText()):1);

                    saveContext();
                    //disableAllComponent();
                    // setting heights and widths
                    //setWidthHeight();
                    plotCloth();
                    // setting heights and widths
                    setWidthHeight();
                    //set component sizes
                    setComponentWidthHeight();
                    //prepare components
                    plotContainerGP();
                    
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONAPPLY"));
                    System.gc();
                    dialogStage.close();
                } catch(Exception ex){
                    new Logging("SEVERE",ClothView.class.getName(),"setRepeats(): "+ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        popup.add(btnApply, 0, 11);

        Button btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));
        btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
        //btnCancel.setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);
        btnCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                dialogStage.close();
            }
        });
        popup.add(btnCancel, 1, 11);

        Scene scene = new Scene(popup);
        scene.getStylesheets().add(ClothView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        dialogStage.setScene(scene);
        dialogStage.setResizable(false);
        dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("REPEATS"));
        dialogStage.showAndWait();        
    }
    
    private void definedLayoutMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONDEFINEDCLOTH"));
        
        final Stage dialogStage = new Stage();
        dialogStage.initOwner(clothStage);
        dialogStage.initStyle(StageStyle.UTILITY);
        //dialogStage.initModality(Modality.WINDOW_MODAL);
        
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setAlignment(Pos.CENTER);
        popup.setHgap(0);
        popup.setVgap(0);
        popup.setPadding(new Insets(25, 25, 25, 25));
        
        Label lblSaree= new Label(objDictionaryAction.getWord("SAREE"));
        lblSaree.setId("heading");
        popup.add(lblSaree, 0, 0, 5, 1);
        
        //Deafult Layout, Border across cloth
        VBox paluSareeVB = new VBox();
        Label paluSareeLbl= new Label(objDictionaryAction.getWord("BORDERACROSSSAREE"));
        paluSareeLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPBORDERACROSSSAREE")));
        paluSareeVB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zyx_cloth_border_across_pallu.png"), paluSareeLbl);
        paluSareeVB.setPrefWidth(80);
        paluSareeVB.getStyleClass().addAll("VBox");    
        paluSareeVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONBORDERACROSSSAREE"));
                objCloth.setStrClothType("Saree");
                clothLayout("BorderAcrossCloth");
            }
        });        
        popup.add(paluSareeVB, 0, 1);
        // Single left side left cross border
        VBox crossBorderSareeVB = new VBox(); 
        Label crossBorderSareeLbl= new Label(objDictionaryAction.getWord("SINGLECROSSBORDER"));
        crossBorderSareeLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSINGLECROSSBORDER")));
        crossBorderSareeVB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zyx_cloth_single_cross_border.png"), crossBorderSareeLbl);
        crossBorderSareeVB.setPrefWidth(80);
        crossBorderSareeVB.getStyleClass().addAll("VBox");
        crossBorderSareeVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONSINGLECROSSBORDER"));
                objCloth.setStrClothType("Saree");
                clothLayout("LeftSideLeftCrossBorderCloth");
            }
        });
        popup.add(crossBorderSareeVB, 1, 1);
        // Open file item
        VBox skirtSareeVB = new VBox(); 
        Label skirtSareeLbl= new Label(objDictionaryAction.getWord("SKIRTBORDER"));
        skirtSareeLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSKIRTBORDER")));
        skirtSareeVB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zyx_cloth_border_with_skirt.png"), skirtSareeLbl);
        skirtSareeVB.setPrefWidth(80);
        skirtSareeVB.getStyleClass().addAll("VBox");
        skirtSareeVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONSKIRTBORDER"));
                objCloth.setStrClothType("Saree");
                clothLayout("SkirtCloth");
            }
        });
        popup.add(skirtSareeVB, 2, 1);
        // Open file item
        VBox blouseSareeVB = new VBox(); 
        Label blouseSareeLbl= new Label(objDictionaryAction.getWord("BLOUSESAREE"));
        blouseSareeLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPBLOUSESAREE")));
        blouseSareeVB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zyx_cloth_with_blouse.png"), blouseSareeLbl);
        blouseSareeVB.setPrefWidth(80);
        blouseSareeVB.getStyleClass().addAll("VBox");
        blouseSareeVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONBLOUSESAREE"));
                objCloth.setStrClothType("Saree");
                clothLayout("BlouseCloth");
            }
        });
        popup.add(blouseSareeVB, 3, 1);
        
        Separator sepHorSaree = new Separator();
        sepHorSaree.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHorSaree, 0, 2);
        GridPane.setColumnSpan(sepHorSaree, 4);
        popup.getChildren().add(sepHorSaree);        
        
        Label lblDress= new Label(objDictionaryAction.getWord("DRESS"));
        lblDress.setId("heading");
        popup.add(lblDress, 0, 3, 4, 1);
        
        // Open file item
        VBox bodyDressVB = new VBox();
        Label bodyDressLbl= new Label(objDictionaryAction.getWord("BODYDRESS"));
        bodyDressLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPBODYDRESS")));
        bodyDressVB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zyx_cloth_body_only.png"), bodyDressLbl);
        bodyDressVB.setPrefWidth(80);
        bodyDressVB.getStyleClass().addAll("VBox");    
        bodyDressVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONBODYDRESS"));
                objCloth.setStrClothType("Dress Material");
                clothLayout("BodyCloth");
            }
        });        
        popup.add(bodyDressVB, 0, 4);
        // Open file item
        VBox borderSingleDressVB = new VBox(); 
        Label borderSingleDressLbl= new Label(objDictionaryAction.getWord("BORDERSINGLEDRESS"));
        borderSingleDressLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPBORDERSINGLEDRESS")));
        borderSingleDressVB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zyx_cloth_body_with_single_border.png"), borderSingleDressLbl);
        borderSingleDressVB.setPrefWidth(80);
        borderSingleDressVB.getStyleClass().addAll("VBox");
        borderSingleDressVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONBORDERDRESS"));
                objCloth.setStrClothType("Dress Material");
                clothLayout("RightBorderCloth");
            }
        });
        popup.add(borderSingleDressVB, 1, 4);
        // Open file item
        VBox borderDressVB = new VBox(); 
        Label borderDressLbl= new Label(objDictionaryAction.getWord("BORDERDRESS"));
        borderDressLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPBORDERDRESS")));
        borderDressVB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zyx_cloth_body_with_borders.png"), borderDressLbl);
        borderDressVB.setPrefWidth(80);
        borderDressVB.getStyleClass().addAll("VBox");
        borderDressVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONBORDERSINGLEDRESS"));
                objCloth.setStrClothType("Dress Material");
                clothLayout("BorderCloth");
            }
        });
        popup.add(borderDressVB, 2, 4);
        
        Separator sepHorDress = new Separator();
        sepHorDress.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHorDress, 0, 5);
        GridPane.setColumnSpan(sepHorDress, 4);
        popup.getChildren().add(sepHorDress);
        
        Label lblStole= new Label(objDictionaryAction.getWord("STOLE"));
        lblStole.setId("heading");
        popup.add(lblStole, 0, 6, 4, 1);
        
        // New file item
        VBox bodyStoleVB = new VBox();
        Label bodyStoleLbl= new Label(objDictionaryAction.getWord("BODYSTOLE"));
        bodyStoleLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPBODYSTOLE")));
        bodyStoleVB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zyx_cloth_body_without_crossborder.png"), bodyStoleLbl);
        bodyStoleVB.setPrefWidth(80);
        bodyStoleVB.getStyleClass().addAll("VBox");    
        bodyStoleVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONBODYSTOLE"));
                objCloth.setStrClothType("Stole");
                clothLayout("PalluCloth");
            }
        });        
        popup.add(bodyStoleVB, 0, 7);
        // New file item
        VBox paluStoleVB = new VBox();
        Label paluStoleLbl= new Label(objDictionaryAction.getWord("PALUSTOLE"));
        paluStoleLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPPALUSTOLE")));
        paluStoleVB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zyx_cloth_without_pallu.png"), paluStoleLbl);
        paluStoleVB.setPrefWidth(80);
        paluStoleVB.getStyleClass().addAll("VBox");    
        paluStoleVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPALUSTOLE"));
                objCloth.setStrClothType("Stole");
                clothLayout("BorderCloth");
            }
        });
        popup.add(paluStoleVB, 1, 7);
        // Open file item
        VBox crossBorderLeftStoleVB = new VBox(); 
        Label crossBorderLeftStoleLbl= new Label(objDictionaryAction.getWord("CROSSBORDERLEFTSTOLE"));
        crossBorderLeftStoleLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCROSSBORDERLEFTSTOLE")));
        crossBorderLeftStoleVB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zyx_cloth_inner_cross_border.png"), crossBorderLeftStoleLbl);
        crossBorderLeftStoleVB.setPrefWidth(80);
        crossBorderLeftStoleVB.getStyleClass().addAll("VBox");
        crossBorderLeftStoleVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCROSSBORDERLEFTSTOLE"));
                objCloth.setStrClothType("Stole");
                clothLayout("InnerCrossBorderCloth");
            }
        });        
        popup.add(crossBorderLeftStoleVB, 2, 7);
        // Open file item
        VBox crossBorderRightStoleVB = new VBox(); 
        Label crossBorderRightStoleLbl= new Label(objDictionaryAction.getWord("CROSSBORDERRIGHTSTOLE"));
        crossBorderRightStoleLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCROSSBORDERRIGHTSTOLE")));
        crossBorderRightStoleVB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zyx_cloth_outer_cross_border.png"), crossBorderRightStoleLbl);
        crossBorderRightStoleVB.setPrefWidth(80);
        crossBorderRightStoleVB.getStyleClass().addAll("VBox");
        crossBorderRightStoleVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCROSSBORDERRIGHTSTOLE"));
                objCloth.setStrClothType("Stole");
                clothLayout("OuterCrossBorderCloth");
            }
        });        
        popup.add(crossBorderRightStoleVB, 3, 7);
        
        Separator sepHorStole = new Separator();
        sepHorStole.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHorStole, 0, 8);
        GridPane.setColumnSpan(sepHorStole, 4);
        popup.getChildren().add(sepHorStole);
        
        Label lblDupatta= new Label(objDictionaryAction.getWord("DUPATTA"));
        lblDupatta.setId("heading");
        popup.add(lblDupatta, 0, 9, 4, 1);
        
        // New file item
        VBox bodyDupattaVB = new VBox();
        Label bodyDupattaLbl= new Label(objDictionaryAction.getWord("BODYDUPATTA"));
        bodyDupattaLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPBODYDUPATTA")));
        bodyDupattaVB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zyx_cloth_body_without_crossborder.png"), bodyDupattaLbl);
        bodyDupattaVB.setPrefWidth(80);
        bodyDupattaVB.getStyleClass().addAll("VBox");    
        bodyDupattaVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONBODYDUPATTA"));
                objCloth.setStrClothType("Dupatta");
                clothLayout("PalluCloth");
            }
        });        
        popup.add(bodyDupattaVB, 0, 10);
        // New file item
        VBox paluDupattaVB = new VBox();
        Label paluDupattaLbl= new Label(objDictionaryAction.getWord("PALUDUPATTA"));
        paluDupattaLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPPALUDUPATTA")));
        paluDupattaVB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zyx_cloth_without_pallu.png"), paluDupattaLbl);
        paluDupattaVB.setPrefWidth(80);
        paluDupattaVB.getStyleClass().addAll("VBox");    
        paluDupattaVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPALUDUPATTA"));
                objCloth.setStrClothType("Dupatta");
                clothLayout("BorderCloth");
            }
        });        
        popup.add(paluDupattaVB, 1, 10);
        // Open file item
        VBox crossBorderLeftDupattaVB = new VBox(); 
        Label crossBorderLeftDupattaLbl= new Label(objDictionaryAction.getWord("CROSSBORDERLEFTDUPATTA"));
        crossBorderLeftDupattaLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCROSSBORDERLEFTDUPATTA")));
        crossBorderLeftDupattaVB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zyx_cloth_inner_cross_border.png"), crossBorderLeftDupattaLbl);
        crossBorderLeftDupattaVB.setPrefWidth(80);
        crossBorderLeftDupattaVB.getStyleClass().addAll("VBox");
        crossBorderLeftDupattaVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCRSSBORDERLEFTDUPATTA"));
                objCloth.setStrClothType("Dupatta");
                clothLayout("InnerCrossBorderCloth");
            }
        });        
        popup.add(crossBorderLeftDupattaVB, 2, 10);
        // Open file item
        VBox crossBorderRightDupattaVB = new VBox(); 
        Label crossBorderRightDupattaLbl= new Label(objDictionaryAction.getWord("CROSSBORDERRIGHTDUPATTA"));
        crossBorderRightDupattaLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCROSSBORDERRIGHTDUPATTA")));
        crossBorderRightDupattaVB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zyx_cloth_outer_cross_border.png"), crossBorderRightDupattaLbl);
        crossBorderRightDupattaVB.setPrefWidth(80);
        crossBorderRightDupattaVB.getStyleClass().addAll("VBox");
        crossBorderRightDupattaVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCRSSBORDERRIGHTDUPATTA"));
                objCloth.setStrClothType("Dupatta");
                clothLayout("OuterCrossBorderCloth");
            }
        });        
        popup.add(crossBorderRightDupattaVB, 3, 10);
        
        Separator sepHorDupatta = new Separator();
        sepHorDupatta.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHorDupatta, 0, 11);
        GridPane.setColumnSpan(sepHorDupatta, 4);
        popup.getChildren().add(sepHorDupatta);
        
        Label lblShawl= new Label(objDictionaryAction.getWord("SHAWL"));
        lblShawl.setId("heading");
        popup.add(lblShawl, 0, 12, 2, 1);
        
        // New file item
        VBox bodyShawlVB = new VBox();
        Label bodyShawlLbl= new Label(objDictionaryAction.getWord("BODYSHAWL"));
        bodyShawlLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPBODYSHAWL")));
        bodyShawlVB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zyx_cloth_body_only.png"), bodyShawlLbl);
        bodyShawlVB.setPrefWidth(80);
        bodyShawlVB.getStyleClass().addAll("VBox");    
        bodyShawlVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONBODYSHAWL"));
                objCloth.setStrClothType("Shawl");
                clothLayout("BodyCloth");
            }
        });
        popup.add(bodyShawlVB, 0, 13);
        /*
        Separator sepHorShawl = new Separator();
        sepHorShawl.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHorShawl, 0, 14);
        GridPane.setColumnSpan(sepHorShawl, 4);
        popup.getChildren().add(sepHorShawl);
        */
        Label lblBrocade= new Label(objDictionaryAction.getWord("GAISARBROCADE"));
        lblBrocade.setId("heading");
        popup.add(lblBrocade, 2, 12, 2, 1);
        
        // New file item
        VBox bodyBrocadeVB = new VBox();
        Label bodyBrocadeLbl= new Label(objDictionaryAction.getWord("BODYBROCADE"));
        bodyBrocadeLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPBODYBROCADE")));
        bodyBrocadeVB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zyx_cloth_body_only.png"), bodyBrocadeLbl);
        bodyBrocadeVB.setPrefWidth(80);
        bodyBrocadeVB.getStyleClass().addAll("VBox");    
        bodyBrocadeVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONBODYBROCADE"));
                objCloth.setStrClothType("Brocade");
                clothLayout("BodyCloth");
            }
        });
        popup.add(bodyBrocadeVB, 2, 13);
                
        /*
        Separator sepHorBrocade = new Separator();
        sepHorBrocade.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHorBrocade, 0, 14);
        GridPane.setColumnSpan(sepHorBrocade, 4);
        popup.getChildren().add(sepHorBrocade);
        */
        
        Separator sepVert = new Separator();
        sepVert.setOrientation(Orientation.VERTICAL);
        sepVert.setValignment(VPos.CENTER);
        sepVert.setPrefHeight(80);
        GridPane.setConstraints(sepVert, 1, 12);
        GridPane.setRowSpan(sepVert, 2);
        popup.getChildren().add(sepVert);
        
        Separator sepHor = new Separator();
        sepHor.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHor, 0, 14);
        GridPane.setColumnSpan(sepHor, 4);
        popup.getChildren().add(sepHor);
        
        Button btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));
        btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
        btnCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                dialogStage.close();
            }
        });
        popup.add(btnCancel, 2, 15, 2, 1);

        Scene scene = new Scene(popup);
        scene.getStylesheets().add(ClothView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        dialogStage.setScene(scene);
        dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("VIEW"));
        dialogStage.showAndWait();        
    }
    
    
    
    private void clothLayout(String strLayout){
        if(strLayout.equalsIgnoreCase("BorderAcrossCloth")){
            leftBorderEnabled=true;
            rightBorderEnabled=true;
            leftPalluEnabled=true;
            rightPalluEnabled=false;
            leftsideLeftCrossBorderEnabled=true;
            leftsideRightCrossBorderEnabled=true;
            rightsideLeftCrossBorderEnabled=false;
            rightsideRightCrossBorderEnabled=false;
            blouseEnabled=false;
            skirtEnabled=false;
        } else if(strLayout.equalsIgnoreCase("LeftSideLeftCrossBorderCloth")){
            leftBorderEnabled=true;
            rightBorderEnabled=true;
            leftPalluEnabled=true;
            rightPalluEnabled=false;
            leftsideLeftCrossBorderEnabled=true;
            leftsideRightCrossBorderEnabled=false;
            rightsideLeftCrossBorderEnabled=false;
            rightsideRightCrossBorderEnabled=false;
            blouseEnabled=false;
            skirtEnabled=false;
        } else if(strLayout.equalsIgnoreCase("SkirtCloth")){
            leftBorderEnabled=true;
            rightBorderEnabled=true;
            leftPalluEnabled=true;
            rightPalluEnabled=false;
            leftsideLeftCrossBorderEnabled=true;
            leftsideRightCrossBorderEnabled=true;
            rightsideLeftCrossBorderEnabled=false;
            rightsideRightCrossBorderEnabled=false;
            blouseEnabled=false;
            skirtEnabled=true;
        } else if(strLayout.equalsIgnoreCase("BlouseCloth")){
            leftBorderEnabled=true;
            rightBorderEnabled=true;
            leftPalluEnabled=true;
            rightPalluEnabled=false;
            leftsideLeftCrossBorderEnabled=true;
            leftsideRightCrossBorderEnabled=true;
            rightsideLeftCrossBorderEnabled=false;
            rightsideRightCrossBorderEnabled=false;
            blouseEnabled=true;
            skirtEnabled=false;
        } else if(strLayout.equalsIgnoreCase("BodyCloth")){
            leftBorderEnabled=false;
            rightBorderEnabled=false;
            leftPalluEnabled=false;
            rightPalluEnabled=false;
            leftsideLeftCrossBorderEnabled=false;
            leftsideRightCrossBorderEnabled=false;
            rightsideLeftCrossBorderEnabled=false;
            rightsideRightCrossBorderEnabled=false;
            blouseEnabled=false;
            skirtEnabled=false;
        } else if(strLayout.equalsIgnoreCase("RightBorderCloth")){
            leftBorderEnabled=false;
            rightBorderEnabled=true;
            leftPalluEnabled=false;
            rightPalluEnabled=false;
            leftsideLeftCrossBorderEnabled=false;
            leftsideRightCrossBorderEnabled=false;
            rightsideLeftCrossBorderEnabled=false;
            rightsideRightCrossBorderEnabled=false;
            blouseEnabled=false;
            skirtEnabled=false;
        } else if(strLayout.equalsIgnoreCase("BorderCloth")){
            leftBorderEnabled=true;
            rightBorderEnabled=true;
            leftPalluEnabled=false;
            rightPalluEnabled=false;
            leftsideLeftCrossBorderEnabled=false;
            leftsideRightCrossBorderEnabled=false;
            rightsideLeftCrossBorderEnabled=false;
            rightsideRightCrossBorderEnabled=false;
            blouseEnabled=false;
            skirtEnabled=false;
        } else if(strLayout.equalsIgnoreCase("PalluCloth")){
            leftBorderEnabled=false;
            rightBorderEnabled=false;
            leftPalluEnabled=true;
            rightPalluEnabled=true;
            leftsideLeftCrossBorderEnabled=false;
            leftsideRightCrossBorderEnabled=false;
            rightsideLeftCrossBorderEnabled=false;
            rightsideRightCrossBorderEnabled=false;
            blouseEnabled=false;
            skirtEnabled=false;
        } else if(strLayout.equalsIgnoreCase("InnerCrossBorderCloth")){
            leftBorderEnabled=true;
            rightBorderEnabled=true;
            leftPalluEnabled=true;
            rightPalluEnabled=true;
            leftsideLeftCrossBorderEnabled=false;
            leftsideRightCrossBorderEnabled=true;
            rightsideLeftCrossBorderEnabled=true;
            rightsideRightCrossBorderEnabled=false;
            blouseEnabled=false;
            skirtEnabled=false;
        } else if(strLayout.equalsIgnoreCase("OuterCrossBorderCloth")){
            leftBorderEnabled=true;
            rightBorderEnabled=true;
            leftPalluEnabled=true;
            rightPalluEnabled=true;
            leftsideLeftCrossBorderEnabled=true;
            leftsideRightCrossBorderEnabled=false;
            rightsideLeftCrossBorderEnabled=false;
            rightsideRightCrossBorderEnabled=true;
            blouseEnabled=false;
            skirtEnabled=false;
        }
        resetClothLayout();
    }
    
    private void customLayoutMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCUSTOMCLOTH"));
                
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setAlignment(Pos.CENTER);
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        
        popup.add(new Label(objDictionaryAction.getWord("CLOTHTYPE")), 0, 0, 2, 1);
        
        final CheckBox bodyCB = new CheckBox(objDictionaryAction.getWord("BODYCB"));
        bodyCB.setSelected(true);
        bodyCB.setDisable(true);
        popup.add(bodyCB, 0, 1, 2, 1);
        
        final CheckBox blouseCB = new CheckBox(objDictionaryAction.getWord("BLOUSECB"));
        blouseCB.setSelected(blouseEnabled);
        popup.add(blouseCB, 0, 2, 2, 1);
        
        final CheckBox skirtCB = new CheckBox(objDictionaryAction.getWord("SKIRTCB"));
        skirtCB.setSelected(skirtEnabled);
        popup.add(skirtCB, 0, 3, 2, 1);
       
        popup.add(new Label(objDictionaryAction.getWord("PALLUCB")), 0, 4, 2, 1);
        
        final CheckBox leftPalluCB = new CheckBox(objDictionaryAction.getWord("LEFTPALUCB"));
        leftPalluCB.setSelected(leftPalluEnabled);
        popup.add(leftPalluCB, 1, 5, 1, 1);
        
        final CheckBox rightPalluCB = new CheckBox(objDictionaryAction.getWord("RIGHTPALUCB"));
        rightPalluCB.setSelected(rightPalluEnabled);
        popup.add(rightPalluCB, 1, 6, 1, 1);
        
        popup.add(new Label(objDictionaryAction.getWord("BORDERCB")), 0, 7, 2, 1);
        
        final CheckBox leftBorderCB = new CheckBox(objDictionaryAction.getWord("LEFTBORDERCB"));
        leftBorderCB.setSelected(leftBorderEnabled);
        popup.add(leftBorderCB, 1, 8, 1, 1);
        
        final CheckBox rightBorderCB = new CheckBox(objDictionaryAction.getWord("RIGHTBORDERCB"));
        rightBorderCB.setSelected(rightBorderEnabled);
        popup.add(rightBorderCB, 1, 9, 1, 1);

        popup.add(new Label(objDictionaryAction.getWord("CROSSBORDERCB")), 0, 10, 2, 1);
        
        final CheckBox leftsideLeftCrossBorderCB = new CheckBox(objDictionaryAction.getWord("LEFTSIDELEFTCROSSBORDERCB"));
        leftsideLeftCrossBorderCB.setSelected(leftsideLeftCrossBorderEnabled);
        popup.add(leftsideLeftCrossBorderCB, 1, 11, 1, 1);
        
        final CheckBox leftsideRightCrossBorderCB = new CheckBox(objDictionaryAction.getWord("LEFTSIDERIGHTCROSSBORDERCB"));
        leftsideRightCrossBorderCB.setSelected(leftsideRightCrossBorderEnabled);
        popup.add(leftsideRightCrossBorderCB, 1, 12, 1, 1);
        
        final CheckBox rightsideLeftCrossBorderCB = new CheckBox(objDictionaryAction.getWord("RIGHTSIDELEFTCROSSBORDERCB"));
        rightsideLeftCrossBorderCB.setSelected(rightsideLeftCrossBorderEnabled);
        popup.add(rightsideLeftCrossBorderCB, 1, 13, 1, 1);
        
        final CheckBox rightsideRightCrossBorderCB = new CheckBox(objDictionaryAction.getWord("RIGHTSIDERIGHTCROSSBORDERCB"));
        rightsideRightCrossBorderCB.setSelected(rightsideRightCrossBorderEnabled);
        popup.add(rightsideRightCrossBorderCB, 1, 14, 1, 1);
        
        popup.add(new Label(objDictionaryAction.getWord("KONIACB")), 0, 15, 2, 1);
        
        final CheckBox leftTopKoniaCB = new CheckBox(objDictionaryAction.getWord("LEFTTOPKONIACB"));
        leftTopKoniaCB.setSelected(false);
        popup.add(leftTopKoniaCB, 1, 16, 1, 1);
        leftTopKoniaCB.setDisable(true);
        
        final CheckBox rightTopKoniaCB = new CheckBox(objDictionaryAction.getWord("RIGHTTOPKONIACB"));
        rightTopKoniaCB.setSelected(false);
        popup.add(rightTopKoniaCB, 1, 17, 1, 1);
        rightTopKoniaCB.setDisable(true);
        
        final CheckBox leftBottomKoniaCB = new CheckBox(objDictionaryAction.getWord("LEFTBOTTOMKONIACB"));
        leftBottomKoniaCB.setSelected(false);
        popup.add(leftBottomKoniaCB, 1, 18, 1, 1);
        leftBottomKoniaCB.setDisable(true);
        
        final CheckBox rightBottomKoniaCB = new CheckBox(objDictionaryAction.getWord("RIGHTBOTTOMKONIACB"));
        rightBottomKoniaCB.setSelected(false);
        popup.add(rightBottomKoniaCB, 1, 19, 1, 1);
        rightBottomKoniaCB.setDisable(true);
        
        if(objCloth.getStrClothType().equalsIgnoreCase("SAREE")){
            rightsideLeftCrossBorderCB.setDisable(true);
            rightsideRightCrossBorderCB.setDisable(true);
            rightPalluCB.setDisable(true);
            
            rightsideLeftCrossBorderCB.setSelected(false);
            rightsideRightCrossBorderCB.setSelected(false);
            rightPalluCB.setSelected(false);
        }else{
            blouseCB.setDisable(true);
            skirtCB.setDisable(true);
            
            blouseCB.setSelected(false);
            skirtCB.setSelected(false);
        }
        
        Button btnApply = new Button(objDictionaryAction.getWord("SAVEAS"));
        btnApply.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save_as.png"));
        btnApply.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSAVEAS")));
        btnApply.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e){
                lblStatus.setText(objDictionaryAction.getWord("ACTIONAPPLY"));
                
                leftBorderEnabled=leftBorderCB.isSelected();
                rightBorderEnabled=rightBorderCB.isSelected();
                leftPalluEnabled=leftPalluCB.isSelected();
                rightPalluEnabled=rightPalluCB.isSelected();
                leftsideLeftCrossBorderEnabled=leftsideLeftCrossBorderCB.isSelected();
                leftsideRightCrossBorderEnabled=leftsideRightCrossBorderCB.isSelected();
                rightsideLeftCrossBorderEnabled=rightsideLeftCrossBorderCB.isSelected();
                rightsideRightCrossBorderEnabled=rightsideRightCrossBorderCB.isSelected();
                blouseEnabled=blouseCB.isSelected();
                skirtEnabled=skirtCB.isSelected();
                
                dialogStage.close();
                System.gc();
        
                resetClothLayout();
            }
        });
        popup.add(btnApply, 0, 20);

        Button btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));
        btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
        btnCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                dialogStage.close();
            }
        });
        popup.add(btnCancel, 1, 20);

        Scene scene = new Scene(popup);
        scene.getStylesheets().add(ClothView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        dialogStage.setScene(scene);
        dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("CUSTOMCLOTH"));
        dialogStage.showAndWait();        
    }
    
    private void clearMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCLEAR"));
        //disableAllComponent();
        initCloth();
        clearContext();
        
        System.gc();
        
        //plotCloth();
        // setting heights and widths
        setWidthHeight();
        // initialize components and Panes
        initializePane();
        //set component sizes
        setComponentWidthHeight();
        //prepare components
        plotContainerGP();
        
        resetClothLayout();
        // setting heights and widths
        setWidthHeight();
        //set component sizes
        setComponentWidthHeight();
        //prepare components
        plotContainerGP();
    }
    

    private boolean isBorderContextValid(){
        return (leftBorderEnabled||rightBorderEnabled);
    }
    
    private boolean isCrossBorderContextValid(){
        return (leftsideLeftCrossBorderEnabled||leftsideRightCrossBorderEnabled||rightsideLeftCrossBorderEnabled||rightsideRightCrossBorderEnabled);
    }
    
    private boolean isPalluContextValid(){
        return (leftPalluEnabled||rightPalluEnabled);
    }
    
    private void frontSideViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONFRONTSIDEVIEW"));
        plotViewActionMode = 1;
        plotCloth();
    }
    
    private void rearSideViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONREARSIDEVIEW"));
        plotViewActionMode = 2;
        plotCloth();
    }
    
    private void simulationViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONSIMULATION"));
        ScreenShoot("ClothSimulation");
    }
    
    private void saveAsHtml(Fabric objFabric, File ifile) {   
        try {
            BufferedImage texture = new BufferedImage(objFabric.getIntWarp(), objFabric.getIntWeft(),BufferedImage.TYPE_INT_RGB);
            for(int x = 0; x < objFabric.getIntWeft(); x++) {
                for(int y = 0; y < objFabric.getIntWarp(); y++) {
                    if(objFabric.getFabricMatrix()[x][y]==0)
                        texture.setRGB(y, x, java.awt.Color.WHITE.getRGB());
                    else
                        texture.setRGB(y, x, java.awt.Color.BLACK.getRGB());
                }
            }
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
            Date date = new Date();
            String currentDate = dateFormat.format(date);
            String path = ifile.getPath()+"Fabric_"+objFabric.getStrFabricID()+"_"+currentDate;
            File pngFile = new File(path+".png");
            ImageIO.write(texture, "png", pngFile);
            File htmlFile = new File(path+".html");
            
            String INITIAL_TEXT = " This is coumputer generated, So no signature required";
            String IMAGE_TEXT = "<img src=\"file:\\\\" + 
                path+".png" + 
             "\" width=\""+objFabric.getIntWeft()+"\" height=\""+objFabric.getIntWarp()+"\" >";
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(htmlFile, true))) {
                bw.write("<table border=0 width=\"100%\">");
                    bw.newLine();
                    bw.write("<tr>");
                        bw.newLine();
                        bw.write("<td><h1>Digital India Corporation (Media Lab Asia)</h1></td>");
                        bw.newLine();
                        bw.write("<td><h6 align=right>");
                            bw.newLine();
                            bw.write(objDictionaryAction.getWord("PROJECT")+": Fabric_"+objFabric.getStrFabricID()+"_"+currentDate+"<br>");
                            bw.newLine();
                            bw.write("<a href=\"http://www.medialabasia.in\">http://www.medialabasia.in</a><br>");
                            bw.newLine();
                            bw.write("&copy; Digital India Corporation (Media Lab Asia); New Delhi, India<br><br>"+date);
                            bw.newLine();
                        bw.write("</h6></td>");
                    bw.newLine();
                    bw.write("</tr>");
                bw.newLine();
                bw.write("</table>");
                bw.newLine();
                bw.write("<table border=1 cellspacing=0>");
                bw.newLine();
                bw.write("<tr align=right><th>Fabric Category</th><td>"+objFabric.getStrClothType()+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("FABRICTYPE")+"</th><td>"+objFabric.getStrFabricType()+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("NAME")+"</th><td>"+objFabric.getStrFabricName()+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("FABRICLENGTH")+"</th><td>"+objFabric.getDblFabricLength()+" inch</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("FABRICWIDTH")+"</th><td>"+objFabric.getDblFabricWidth()+" inch</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("REEDCOUNT")+"</th><td>"+objFabric.getIntReedCount()+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("DENTS")+"</th><td>"+objFabric.getIntDents()+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("TPD")+"</th><td>"+objFabric.getIntTPD()+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("PICKS")+"</th><td>"+objFabric.getIntWeft()+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("HOOKS")+"</th><td>"+objFabric.getIntHooks()+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("ENDS")+"</th><td>"+objFabric.getIntWarp()+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("PPI")+"</th><td>"+objFabric.getIntPPI()+" / inch</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("HPI")+"</th><td>"+objFabric.getIntHPI()+" / inch</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("EPI")+"</th><td>"+objFabric.getIntEPI()+" / inch</td></tr>");
                bw.newLine();
                if(objFabric.getStrArtworkID()!=null){
                    bw.newLine();
                    objFabric.setDblArtworkLength(Double.parseDouble(String.format("%.3f",objFabric.getDblArtworkLength())));
                    bw.write("<tr align=right><th>"+objDictionaryAction.getWord("ARTWORKLENGTH")+"</th><td>"+objFabric.getDblArtworkLength()+" inch</td></tr>");
                    bw.newLine();
                    objFabric.setDblArtworkWidth(Double.parseDouble(String.format("%.3f",objFabric.getDblArtworkWidth())));
                    bw.write("<tr align=right><th>"+objDictionaryAction.getWord("ARTWORKWIDTH")+"</th><td>"+objFabric.getDblArtworkWidth()+" inch</td></tr>");
                }   
                bw.newLine();
                objFabricAction = new FabricAction(false);
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("WARPYARNCONSUMPTION")+"</th><td>"+objFabricAction.getWarpNumber(objFabric)+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("WARPYARNWEIGHT")+"</th><td>"+Double.parseDouble(String.format("%.3f",objFabricAction.getWarpWeight(objFabric)))+" gram</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("WARPYARNLONG")+"</th><td>"+Double.parseDouble(String.format("%.3f",objFabricAction.getWarpLong(objFabric)))+" meter</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("WEFTYARNCONSUMPTION")+"</th><td>"+objFabricAction.getWeftNumber(objFabric)+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("WEFTYARNWEIGHT")+"</th><td>"+Double.parseDouble(String.format("%.3f",objFabricAction.getWeftWeight(objFabric)))+" gram</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("WEFTYARNLONG")+"</th><td>"+Double.parseDouble(String.format("%.3f",objFabricAction.getWeftLong(objFabric)))+" meter</td></tr>");
                bw.newLine();
                bw.write("</table>");                
            bw.newLine();
            bw.write("");
            if(objFabric.getWarpYarn().length>0){
                bw.newLine();
                bw.write("<table border=1 cellspacing=0>");
                    bw.newLine();
                    objPatternAction = new PatternAction();
                    bw.write("<caption>"+objDictionaryAction.getWord("WARP")+" "+objDictionaryAction.getWord("YARNPATTERN")+" \t"+objPatternAction.getPattern(objFabric.getStrWarpPatternID()).getStrPattern()+"</caption>");
                    bw.newLine();
                    bw.write("<tr align=right><th>"+objDictionaryAction.getWord("WARP")+"</th><th>"+objDictionaryAction.getWord("NAME")+"</th><th>"+objDictionaryAction.getWord("YARNTYPE")+"</th><th>"+objDictionaryAction.getWord("YARNREPEAT")+"</th><th>"+objDictionaryAction.getWord("YARNCOUNT")+"</th><th>"+objDictionaryAction.getWord("YARNUNIT")+"</th><th>"+objDictionaryAction.getWord("YARNPLY")+"</th><th>"+objDictionaryAction.getWord("YARNFACTOR")+"</th><th>"+objDictionaryAction.getWord("YARNDIAMETER")+"(mm)</th><th>"+objDictionaryAction.getWord("YARNWEIGHT")+"(kg)</th><th>"+objDictionaryAction.getWord("YARNPRICE")+"(kg)</th><th>"+objDictionaryAction.getWord("YARNTWIST")+"/ inch</th><th>"+objDictionaryAction.getWord("YARNSENCE")+"</th><th>"+objDictionaryAction.getWord("YARNHAIRNESS")+"</th><th>"+objDictionaryAction.getWord("YARNDISTRIBUTION")+"</th><th>"+objDictionaryAction.getWord("YARNCOLOR")+"</th></tr>");
                    for(int i = 0; i<objFabric.getWarpYarn().length;i++){
                        bw.newLine();
                        bw.write("<tr align=right><td>"+objFabric.getWarpYarn()[i].getStrYarnSymbol()+"</td><td>"+objFabric.getWarpYarn()[i].getStrYarnName()+"</td><td>"+objFabric.getWarpYarn()[i].getStrYarnType()+"</td><td>"+objFabric.getWarpYarn()[i].getIntYarnRepeat()+"</td><td>"+objFabric.getWarpYarn()[i].getIntYarnCount()+"</td><td>"+objFabric.getWarpYarn()[i].getStrYarnCountUnit()+"</td><td>"+objFabric.getWarpYarn()[i].getIntYarnPly()+"</td><td>"+objFabric.getWarpYarn()[i].getIntYarnDFactor()+"</td><td>"+objFabric.getWarpYarn()[i].getDblYarnDiameter()+"</td><td>-</td><td>"+objFabric.getWarpYarn()[i].getDblYarnPrice()+"</td><td>"+objFabric.getWarpYarn()[i].getIntYarnTwist()+"</td><td>"+objFabric.getWarpYarn()[i].getStrYarnTModel()+"</td><td>"+objFabric.getWarpYarn()[i].getIntYarnHairness()+"</td><td>"+objFabric.getWarpYarn()[i].getIntYarnHProbability()+"</td><th bgcolor=\""+objFabric.getWarpYarn()[i].getStrYarnColor()+"\">"+objFabric.getWarpYarn()[i].getStrYarnColor()+"</th></tr>");
                    }
                bw.newLine();
                bw.write("</table>");
            }
            if(objFabric.getWeftYarn().length>0){
                bw.newLine();
                bw.write("<table border=1 cellspacing=0>");
                    bw.newLine();
                    objPatternAction = new PatternAction();
                    bw.write("<caption>"+objDictionaryAction.getWord("WEFT")+" "+objDictionaryAction.getWord("YARNPATTERN")+" \t "+objPatternAction.getPattern(objFabric.getStrWeftPatternID()).getStrPattern()+"</caption>");
                    bw.newLine();
                    bw.write("<tr align=right><th>"+objDictionaryAction.getWord("WEFT")+"</th><th>"+objDictionaryAction.getWord("NAME")+"</th><th>"+objDictionaryAction.getWord("YARNTYPE")+"</th><th>"+objDictionaryAction.getWord("YARNREPEAT")+"</th><th>"+objDictionaryAction.getWord("YARNCOUNT")+"</th><th>"+objDictionaryAction.getWord("YARNUNIT")+"</th><th>"+objDictionaryAction.getWord("YARNPLY")+"</th><th>"+objDictionaryAction.getWord("YARNFACTOR")+"</th><th>"+objDictionaryAction.getWord("YARNDIAMETER")+"(mm)</th><th>"+objDictionaryAction.getWord("YARNWEIGHT")+"(kg)</th><th>"+objDictionaryAction.getWord("YARNPRICE")+"(kg)</th><th>"+objDictionaryAction.getWord("YARNTWIST")+"/ inch</th><th>"+objDictionaryAction.getWord("YARNSENCE")+"</th><th>"+objDictionaryAction.getWord("YARNHAIRNESS")+"</th><th>"+objDictionaryAction.getWord("YARNDISTRIBUTION")+"</th><th>"+objDictionaryAction.getWord("YARNCOLOR")+"</th></tr>");
                    for(int i = 0; i<objFabric.getWeftYarn().length;i++){
                        bw.newLine();
                        bw.write("<tr align=right><td>"+objFabric.getWeftYarn()[i].getStrYarnSymbol()+"</td><td>"+objFabric.getWeftYarn()[i].getStrYarnName()+"</td><td>"+objFabric.getWeftYarn()[i].getStrYarnType()+"</td><td>"+objFabric.getWeftYarn()[i].getIntYarnRepeat()+"</td><td>"+objFabric.getWeftYarn()[i].getIntYarnCount()+"</td><td>"+objFabric.getWeftYarn()[i].getStrYarnCountUnit()+"</td><td>"+objFabric.getWeftYarn()[i].getIntYarnPly()+"</td><td>"+objFabric.getWeftYarn()[i].getIntYarnDFactor()+"</td><td>"+objFabric.getWeftYarn()[i].getDblYarnDiameter()+"</td><td>-</td><td>"+objFabric.getWeftYarn()[i].getDblYarnPrice()+"</td><td>"+objFabric.getWeftYarn()[i].getIntYarnTwist()+"</td><td>"+objFabric.getWeftYarn()[i].getStrYarnTModel()+"</td><td>"+objFabric.getWeftYarn()[i].getIntYarnHairness()+"</td><td>"+objFabric.getWeftYarn()[i].getIntYarnHProbability()+"</td><th bgcolor=\""+objFabric.getWeftYarn()[i].getStrYarnColor()+"\">"+objFabric.getWeftYarn()[i].getStrYarnColor()+"</th></tr>");
                    }
                bw.newLine();
                bw.write("</table>");
            }
            if(objFabric.getWarpExtraYarn().length>0){
                bw.newLine();
                bw.write("<table border=1 cellspacing=0>");
                    bw.newLine();
                    bw.write("<caption>"+objDictionaryAction.getWord("EXTRA")+" "+objDictionaryAction.getWord("WARP")+"</caption>");
                    bw.newLine();
                    bw.write("<tr align=right><th>"+objDictionaryAction.getWord("EXTRA")+" "+objDictionaryAction.getWord("WARP")+"</th><th>"+objDictionaryAction.getWord("NAME")+"</th><th>"+objDictionaryAction.getWord("YARNTYPE")+"</th><th>"+objDictionaryAction.getWord("YARNREPEAT")+"</th><th>"+objDictionaryAction.getWord("YARNCOUNT")+"</th><th>"+objDictionaryAction.getWord("YARNUNIT")+"</th><th>"+objDictionaryAction.getWord("YARNPLY")+"</th><th>"+objDictionaryAction.getWord("YARNFACTOR")+"</th><th>"+objDictionaryAction.getWord("YARNDIAMETER")+"(mm)</th><th>"+objDictionaryAction.getWord("YARNWEIGHT")+"(kg)</th><th>"+objDictionaryAction.getWord("YARNPRICE")+"(kg)</th><th>"+objDictionaryAction.getWord("YARNTWIST")+"/ inch</th><th>"+objDictionaryAction.getWord("YARNSENCE")+"</th><th>"+objDictionaryAction.getWord("YARNHAIRNESS")+"</th><th>"+objDictionaryAction.getWord("YARNDISTRIBUTION")+"</th><th>"+objDictionaryAction.getWord("YARNCOLOR")+"</th></tr>");
                    for(int i=0; i<objFabric.getIntExtraWarp(); i++){
                        bw.newLine();
                        bw.write("<tr align=right><td>"+objFabric.getWarpExtraYarn()[i].getStrYarnSymbol()+"</td><td>"+objFabric.getWarpExtraYarn()[i].getStrYarnName()+"</td><td></td><td>"+objFabric.getWarpExtraYarn()[i].getStrYarnType()+"</td><td>"+objFabric.getWarpExtraYarn()[i].getIntYarnRepeat()+"</td><td>"+objFabric.getWarpExtraYarn()[i].getIntYarnCount()+"</td><td>"+objFabric.getWarpExtraYarn()[i].getStrYarnCountUnit()+"</td><td>"+objFabric.getWarpExtraYarn()[i].getIntYarnPly()+"</td><td>"+objFabric.getWarpExtraYarn()[i].getIntYarnDFactor()+"</td><td>"+objFabric.getWarpExtraYarn()[i].getDblYarnDiameter()+"</td><td>-</td><td>"+objFabric.getWarpExtraYarn()[i].getDblYarnPrice()+"</td><td>"+objFabric.getWarpExtraYarn()[i].getIntYarnTwist()+"</td><td>"+objFabric.getWarpExtraYarn()[i].getStrYarnTModel()+"</td><td>"+objFabric.getWarpExtraYarn()[i].getIntYarnHairness()+"</td><td>"+objFabric.getWarpExtraYarn()[i].getIntYarnHProbability()+"</td><th bgcolor=\""+objFabric.getWarpExtraYarn()[i].getStrYarnColor()+"\">"+objFabric.getWarpExtraYarn()[i].getStrYarnColor()+"</th></tr>");
                    }
                bw.newLine();
                bw.write("</table>");
            }
            if(objFabric.getWeftExtraYarn().length>0){
                bw.newLine();
                bw.write("<table border=1 cellspacing=0>");
                    bw.newLine();
                    bw.write("<caption>"+objDictionaryAction.getWord("EXTRA")+" "+objDictionaryAction.getWord("WEFT")+"</caption>");
                    bw.newLine();
                    bw.write("<tr align=right><th>"+objDictionaryAction.getWord("EXTRA")+" "+objDictionaryAction.getWord("WEFT")+"</th><th>"+objDictionaryAction.getWord("NAME")+"</th><th>"+objDictionaryAction.getWord("YARNTYPE")+"</th><th>"+objDictionaryAction.getWord("YARNREPEAT")+"</th><th>"+objDictionaryAction.getWord("YARNCOUNT")+"</th><th>"+objDictionaryAction.getWord("YARNUNIT")+"</th><th>"+objDictionaryAction.getWord("YARNPLY")+"</th><th>"+objDictionaryAction.getWord("YARNFACTOR")+"</th><th>"+objDictionaryAction.getWord("YARNDIAMETER")+"(mm)</th><th>"+objDictionaryAction.getWord("YARNWEIGHT")+"(kg)</th><th>"+objDictionaryAction.getWord("YARNPRICE")+"(kg)</th><th>"+objDictionaryAction.getWord("YARNTWIST")+"/ inch</th><th>"+objDictionaryAction.getWord("YARNSENCE")+"</th><th>"+objDictionaryAction.getWord("YARNHAIRNESS")+"</th><th>"+objDictionaryAction.getWord("YARNDISTRIBUTION")+"</th><th>"+objDictionaryAction.getWord("YARNCOLOR")+"</th></tr>");
                    for(int i=0; i<objFabric.getIntExtraWeft(); i++){
                        bw.newLine();
                        bw.write("<tr align=right><td>"+objFabric.getWeftExtraYarn()[i].getStrYarnSymbol()+"</td><td>"+objFabric.getWeftExtraYarn()[i].getStrYarnName()+"</td><td>"+objFabric.getWeftExtraYarn()[i].getStrYarnType()+"</td><td>"+objFabric.getWeftExtraYarn()[i].getIntYarnRepeat()+"</td><td>"+objFabric.getWeftExtraYarn()[i].getIntYarnCount()+"</td><td>"+objFabric.getWeftExtraYarn()[i].getStrYarnCountUnit()+"</td><td>"+objFabric.getWeftExtraYarn()[i].getIntYarnPly()+"</td><td>"+objFabric.getWeftExtraYarn()[i].getIntYarnDFactor()+"</td><td>"+objFabric.getWeftExtraYarn()[i].getDblYarnDiameter()+"</td><td>-</td><td>"+objFabric.getWeftExtraYarn()[i].getDblYarnPrice()+"</td><td>"+objFabric.getWeftExtraYarn()[i].getIntYarnTwist()+"</td><td>"+objFabric.getWeftExtraYarn()[i].getStrYarnTModel()+"</td><td>"+objFabric.getWeftExtraYarn()[i].getIntYarnHairness()+"</td><td>"+objFabric.getWeftExtraYarn()[i].getIntYarnHProbability()+"</td><th bgcolor=\""+objFabric.getWeftExtraYarn()[i].getStrYarnColor()+"\">"+objFabric.getWeftExtraYarn()[i].getStrYarnColor()+"</th></tr>");
                    }
                bw.newLine();
                bw.write("</table>");
            }
            bw.newLine();
            bw.write("<br><b>"+objDictionaryAction.getWord("GRAPHVIEW")+"<b><br>");
            bw.newLine();
            bw.write(IMAGE_TEXT);
            bw.newLine();
            bw.write("<br>"+INITIAL_TEXT+"<br>");
            bw.newLine();
            } catch (IOException ex) {
                new Logging("SEVERE",ClothView.class.getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            } catch (SQLException ex) {
                new Logging("SEVERE",ClothView.class.getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            } catch (Exception ex) {
                new Logging("SEVERE",ClothView.class.getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
            
            if(objConfiguration.getBlnAuthenticateService()){
                ArrayList<File> filesToZip=new ArrayList<>();
                filesToZip.add(pngFile);
                filesToZip.add(htmlFile);
                String zipFilePath=path+".zip";
                String passwordToZip = ifile.getName()+"Fabric_"+objFabric.getStrFabricID()+"_"+currentDate;
                new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                pngFile.delete();
                htmlFile.delete();
            }
            
            lblStatus.setText("HTML "+objDictionaryAction.getWord("EXPORTEDTO")+" "+path+".html");
        } catch (IOException ex) {
            new Logging("SEVERE",ClothView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    
    private void printGraph(Fabric objFabric){
        try{
            int intLength = objFabric.getIntWarp();
            int intHeight = objFabric.getIntWeft(); 
            lblStatus.setText(objDictionaryAction.getWord("GETGRAPHVIEW"));
            BufferedImage bufferedImage = new BufferedImage(objFabric.getIntWarp(), objFabric.getIntWeft(),BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            
            //================================ For Outer to be square ===============================
            if(objFabric.getIntExtraWarp()>0 || objFabric.getIntExtraWeft()>0){
                if(objConfiguration.getBlnPunchCard()){
                    bufferedImage = objFabricAction.plotGraphJaquardMachineView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                    intLength=objFabric.getIntWarp()*(objFabric.getIntExtraWeft()+2);
                } else{
                    bufferedImage = objFabricAction.plotGraphJaquardView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                }
            }else{
                bufferedImage = objFabricAction.plotGraphDobbyView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
            }
            
            new PrintView(bufferedImage, 3, objFabric.getStrFabricName(), objFabric.getObjConfiguration().getObjUser().getStrName(), objFabric.getObjConfiguration().getStrGraphSize());
            lblStatus.setText("");
        } catch (SQLException ex) {
            new Logging("SEVERE",ClothView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    
    private void printGraphOld(Fabric objFabric){
        try {
            int intLength = objFabric.getIntWarp();
            int intHeight = objFabric.getIntWeft(); 
            lblStatus.setText(objDictionaryAction.getWord("GETGRAPHVIEW"));
            BufferedImage bufferedImage = new BufferedImage(objFabric.getIntWarp(), objFabric.getIntWeft(),BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            
            //================================ For Outer to be square ===============================
            if(objFabric.getIntExtraWarp()>0 || objFabric.getIntExtraWeft()>0){
                if(objConfiguration.getBlnPunchCard()){
                    bufferedImage = objFabricAction.plotGraphJaquardMachineView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                    intLength=objFabric.getIntWarp()*(objFabric.getIntExtraWeft()+2);
                } else{
                    bufferedImage = objFabricAction.plotGraphJaquardView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                }
            }else{
                bufferedImage = objFabricAction.plotGraphDobbyView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
            }
            
            String[] data = objFabric.getObjConfiguration().getStrGraphSize().split("x");
            final BufferedImage texture = new BufferedImage((int)(intLength*zoomFactor*Integer.parseInt(data[1])), (int)(objFabric.getIntWeft()*zoomFactor*Integer.parseInt(data[0]))+100,BufferedImage.TYPE_INT_RGB);
            Graphics2D g = texture.createGraphics();
            g.drawImage(bufferedImage, 0, 0, (int)(intLength*zoomFactor*Integer.parseInt(data[1])), (int)(objFabric.getIntWeft()*zoomFactor*Integer.parseInt(data[0])), null);
            g.setColor(java.awt.Color.BLACK);
            BasicStroke bs = new BasicStroke(2);
            g.setStroke(bs);
            
            for(int i = 0; i < objFabric.getIntWeft(); i++) {
                for(int j = 0; j < intLength; j++) {
                    if((j%Integer.parseInt(data[0]))==0){
                        bs = new BasicStroke(2);
                        g.setStroke(bs);
                    }else{
                        bs = new BasicStroke(1);
                        g.setStroke(bs);
                    }
                    g.drawLine((int)(j*zoomFactor*Integer.parseInt(data[1])), 0,  (int)(j*zoomFactor*Integer.parseInt(data[1])), (int)(objFabric.getIntWeft()*zoomFactor*Integer.parseInt(data[0])));
                }
                if((i%Integer.parseInt(data[1]))==0){
                    bs = new BasicStroke(2);
                    g.setStroke(bs);
                }else{
                    bs = new BasicStroke(1);
                    g.setStroke(bs);
                }
                g.drawLine(0, (int)(i*zoomFactor*Integer.parseInt(data[0])), (int)(intLength*zoomFactor)*Integer.parseInt(data[1]), (int)(i*zoomFactor*Integer.parseInt(data[0])));
            }
            /*
            ============================= For Inner to be square ===============================
            if(objFabric.getIntExtraWarp()>0 || objFabric.getIntExtraWeft()>0){
                bufferedImage = objFabricAction.plotGraphJaquardView(objFabric, intWarp, intWeft);
            }else{
                bufferedImage = objFabricAction.plotGraphDobbyView(objFabric, intWarp, intWeft);
            }
            final BufferedImage texture = new BufferedImage((int)(intWeft*objConfiguration.getIntBoxSize()), (int)(intWarp*objConfiguration.getIntBoxSize())+100,BufferedImage.TYPE_INT_RGB);
            Graphics2D g = texture.createGraphics();
            g.drawImage(bufferedImage, 0, 0, (int)(intWeft*objConfiguration.getIntBoxSize()), (int)(intWarp*objConfiguration.getIntBoxSize()), null);
            g.setColor(java.awt.Color.BLACK);
            BasicStroke bs = new BasicStroke(2);
            g.setStroke(bs);
            String[] data = objConfiguration.getStrGraphSize().split("x");
            for(int x = 0; x < (int)(intWarp*objConfiguration.getIntBoxSize()); x+=(int)(objConfiguration.getIntBoxSize())) {
                for(int y = 0; y < (int)(intWeft*objConfiguration.getIntBoxSize()); y+=(int)(objConfiguration.getIntBoxSize())) {
                    if(y%(int)(Integer.parseInt(data[0])*objConfiguration.getIntBoxSize())==0){
                        bs = new BasicStroke(2);
                        g.setStroke(bs);
                    }else{
                        bs = new BasicStroke(1);
                        g.setStroke(bs);
                    }
                    g.drawLine(y, 0,  y, (int)(intWarp*objConfiguration.getIntBoxSize()));
                }
                if(x%(int)(Integer.parseInt(data[1])*objConfiguration.getIntBoxSize())==0){
                    bs = new BasicStroke(2);
                    g.setStroke(bs);
                }else{
                    bs = new BasicStroke(1);
                    g.setStroke(bs);
                }
                g.drawLine(0, x, (int)(intWeft*objConfiguration.getIntBoxSize()), x);
            }
            */
            g.setColor(java.awt.Color.WHITE);
            bs = new BasicStroke(3);
            g.setStroke(bs);
            g.drawString(objDictionaryAction.getWord("LOOM")+" "+objDictionaryAction.getWord("SPECIFICATIONS")+" : ", 10, (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(1*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("REEDCOUNT")+" : "+objFabric.getIntReedCount(), (int)(intLength*Integer.parseInt(data[0])/2), (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(1*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("DENTS")+" : "+objFabric.getIntDents(), 10, (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(2*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("PPI")+" : "+objFabric.getIntPPI(), (int)(intLength*Integer.parseInt(data[0])/2), (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(2*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("PICKS")+" : "+objFabric.getIntWeft(), 10, (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(3*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("HPI")+" : "+objFabric.getIntHPI(), (int)(intLength*Integer.parseInt(data[0])/2), (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(3*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("HOOKS")+" : "+objFabric.getIntHooks(), 10, (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(4*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("EPI")+" : "+objFabric.getIntEPI(), (int)(intLength*Integer.parseInt(data[0])/2), (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(4*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("ENDS")+" : "+objFabric.getIntWarp(), 10, (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(5*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("TPD")+" : "+objFabric.getIntTPD(), (int)(intLength*Integer.parseInt(data[0])/2), (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(5*g.getFontMetrics().getHeight()));
            g.dispose();
            bufferedImage = null;
            if(objPrinterJob.printDialog()){                
                if(objConfiguration.getBlnMPrint()){
                    double h = objPrinterJob.getPageFormat(objPrinterAttribute).getImageableHeight();
                    double w = objPrinterJob.getPageFormat(objPrinterAttribute).getImageableWidth();
                    BufferedImage texture1 = new BufferedImage((int)(w*Math.ceil(texture.getWidth()/w)), (int)(h*Math.ceil(texture.getHeight()/h)),BufferedImage.TYPE_INT_RGB);
                    Graphics2D g1 = texture1.createGraphics();
                    g1.drawImage(texture, 0, 0, (int)(w*Math.ceil(texture.getWidth()/w)), (int)(h*Math.ceil(texture.getHeight()/h)), null);
                    g1.dispose();
                    
                    final BufferedImage page = new BufferedImage((int)w, (int)h, BufferedImage.TYPE_INT_RGB);
                    for(int i=0; i<Math.ceil(texture.getHeight()/h);i++){
                        for(int j=0; j<Math.ceil(texture.getWidth()/w);j++){
                            page.getGraphics().drawImage(texture1, 0, 0, (int)w, (int)h, (int)(j*w), (int)(i*h), (int)((j+1)*w), (int)((i+1)*h), null);
                            objPrinterJob.setPrintable(new Printable() {
                                @Override
                                public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
                                    if (pageIndex != 0) {
                                        return NO_SUCH_PAGE;
                                    }
                                    //graphics.drawImage(texture, 0, 0, texture.getWidth(), texture.getHeight(), null);
                                    Graphics2D g2d=(Graphics2D)graphics;
                                    g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
                                    graphics.drawImage(page, 0, 0, (int)pageFormat.getImageableWidth(), (int)pageFormat.getImageableHeight(), null);
                                    return PAGE_EXISTS;                    
                                }
                            });     
                            objPrinterJob.print();
                        }
                    }
                }else{
                    objPrinterJob.setPrintable(new Printable() {
                        @Override
                        public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
                            if (pageIndex != 0) {
                                return NO_SUCH_PAGE;
                            }
                            //graphics.drawImage(texture, 0, 0, texture.getWidth(), texture.getHeight(), null);
                            Graphics2D g2d=(Graphics2D)graphics;
                            g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
                            graphics.drawImage(texture, 0, 0, (int)pageFormat.getImageableWidth(), (int)pageFormat.getImageableHeight(), null);
                            return PAGE_EXISTS;                    
                        }
                    });     
                    objPrinterJob.print();
                }
            }
        } catch (PrinterException ex) {             
            new Logging("SEVERE",ClothView.class.getName(),ex.toString(),ex);  
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (SQLException ex) {
            new Logging("SEVERE",ClothView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void saveAsGraph(Fabric objFabric, File file, int index){
        try {
            int intLength = objFabric.getIntWarp();
            int intHeight = objFabric.getIntWeft();
            lblStatus.setText(objDictionaryAction.getWord("GETGRAPHVIEW"));
            BufferedImage bufferedImage = new BufferedImage(intHeight, intLength, BufferedImage.TYPE_INT_RGB);
            objFabricAction = new FabricAction();
            
            //================================ For Outer to be square ===============================
            if(objFabric.getIntExtraWarp()>0 || objFabric.getIntExtraWeft()>0){
                if(objConfiguration.getBlnPunchCard()){
                    bufferedImage = objFabricAction.plotGraphJaquardMachineView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                    intLength=objFabric.getIntWarp()*(objFabric.getIntExtraWeft()+2);
                } else{
                    bufferedImage = objFabricAction.plotGraphJaquardView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                }
            }else{
                bufferedImage = objFabricAction.plotGraphDobbyView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
            }
            
            String[] data = objFabric.getObjConfiguration().getStrGraphSize().split("x");
            BufferedImage texture = new BufferedImage((int)(intLength*zoomFactor*Integer.parseInt(data[1])), (int)(objFabric.getIntWeft()*zoomFactor*Integer.parseInt(data[0])),BufferedImage.TYPE_INT_RGB);
            //BufferedImage texture = new BufferedImage((int)(intLength*zoomFactor*Integer.parseInt(data[1])), (int)(objFabric.getIntWeft()*zoomFactor*Integer.parseInt(data[0]))+100,BufferedImage.TYPE_INT_RGB);
            Graphics2D g = texture.createGraphics();
            g.drawImage(bufferedImage, 0, 0, (int)(intLength*zoomFactor*Integer.parseInt(data[1])), (int)(objFabric.getIntWeft()*zoomFactor*Integer.parseInt(data[0])), null);
            g.setColor(java.awt.Color.BLACK);
            BasicStroke bs = new BasicStroke(2);
            g.setStroke(bs);
            
            for(int i = 0; i < objFabric.getIntWeft(); i++) {
                for(int j = 0; j < intLength; j++) {
                    if((j%Integer.parseInt(data[0]))==0){
                        bs = new BasicStroke(2);
                        g.setStroke(bs);
                    }else{
                        bs = new BasicStroke(1);
                        g.setStroke(bs);
                    }
                    g.drawLine((int)(j*zoomFactor*Integer.parseInt(data[1])), 0,  (int)(j*zoomFactor*Integer.parseInt(data[1])), (int)(objFabric.getIntWeft()*zoomFactor*Integer.parseInt(data[0])));
                }
                if((i%Integer.parseInt(data[1]))==0){
                    bs = new BasicStroke(2);
                    g.setStroke(bs);
                }else{
                    bs = new BasicStroke(1);
                    g.setStroke(bs);
                }
                g.drawLine(0, (int)(i*zoomFactor*Integer.parseInt(data[0])), (int)(intLength*zoomFactor)*Integer.parseInt(data[1]), (int)(i*zoomFactor*Integer.parseInt(data[0])));
            }
            // cover right and bottom boundary lines
            g.drawLine((int)(intLength*zoomFactor*Integer.parseInt(data[1]))-1, 0,  (int)(intLength*zoomFactor*Integer.parseInt(data[1]))-1, (int)(objFabric.getIntWeft()*zoomFactor*Integer.parseInt(data[0])));
            g.drawLine(0, (int)(objFabric.getIntWeft()*zoomFactor*Integer.parseInt(data[0]))-1, (int)(intLength*zoomFactor)*Integer.parseInt(data[1]), (int)(objFabric.getIntWeft()*zoomFactor*Integer.parseInt(data[0]))-1);
            /*
            ============================= For Inner to be square ===============================
            if(objFabric.getIntExtraWarp()>0 || objFabric.getIntExtraWeft()>0){
                bufferedImage = objFabricAction.plotGraphJaquardView(objFabric, intWarp, intWeft);
            }else{
                bufferedImage = objFabricAction.plotGraphDobbyView(objFabric, intWarp, intWeft);
            }
            BufferedImage texture = new BufferedImage((int)(intWeft*objConfiguration.getIntBoxSize()), (int)(intWarp*objConfiguration.getIntBoxSize())+100,BufferedImage.TYPE_INT_RGB);
            Graphics2D g = texture.createGraphics();
            g.drawImage(bufferedImage, 0, 0, (int)(intWeft*objConfiguration.getIntBoxSize()), (int)(intWarp*objConfiguration.getIntBoxSize()), null);
            g.setColor(java.awt.Color.BLACK);
            BasicStroke bs = new BasicStroke(2);
            g.setStroke(bs);
            String[] data = objConfiguration.getStrGraphSize().split("x");
            for(int x = 0; x < (int)(intWarp*objConfiguration.getIntBoxSize()); x+=(int)(objConfiguration.getIntBoxSize())) {
                for(int y = 0; y < (int)(intWeft*objConfiguration.getIntBoxSize()); y+=(int)(objConfiguration.getIntBoxSize())) {
                    if(y%(int)(Integer.parseInt(data[0])*objConfiguration.getIntBoxSize())==0){
                        bs = new BasicStroke(2);
                        g.setStroke(bs);
                    }else{
                        bs = new BasicStroke(1);
                        g.setStroke(bs);
                    }
                    g.drawLine(y, 0,  y, (int)(intWarp*objConfiguration.getIntBoxSize()));
                }
                if(x%(int)(Integer.parseInt(data[1])*objConfiguration.getIntBoxSize())==0){
                    bs = new BasicStroke(2);
                    g.setStroke(bs);
                }else{
                    bs = new BasicStroke(1);
                    g.setStroke(bs);
                }
                g.drawLine(0, x, (int)(intWeft*objConfiguration.getIntBoxSize()), x);
            }
            */
            /* //for adding informations 
            g.setColor(new java.awt.Color(java.awt.Color.WHITE.getRGB()));
            bs = new BasicStroke(3);
            g.setStroke(bs);
            g.setColor(java.awt.Color.WHITE);
            g.drawString(objDictionaryAction.getWord("LOOM")+" "+objDictionaryAction.getWord("SPECIFICATIONS")+" : ", 10, (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(1*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("REEDCOUNT")+" : "+objFabric.getIntReedCount(), (int)(intLength*Integer.parseInt(data[0])/2), (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(1*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("DENTS")+" : "+objFabric.getIntDents(), 10, (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(2*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("PPI")+" : "+objFabric.getIntPPI(), (int)(intLength*Integer.parseInt(data[0])/2), (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(2*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("PICKS")+" : "+objFabric.getIntWeft(), 10, (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(3*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("HPI")+" : "+objFabric.getIntHPI(), (int)(intLength*Integer.parseInt(data[0])/2), (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(3*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("HOOKS")+" : "+objFabric.getIntHooks(), 10, (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(4*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("EPI")+" : "+objFabric.getIntEPI(), (int)(intLength*Integer.parseInt(data[0])/2), (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(4*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("ENDS")+" : "+objFabric.getIntWarp(), 10, (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(5*g.getFontMetrics().getHeight()));
            g.drawString(objDictionaryAction.getWord("TPD")+" : "+objFabric.getIntTPD(), (int)(intLength*Integer.parseInt(data[0])/2), (int)(objFabric.getIntWeft()*Integer.parseInt(data[1]))+(5*g.getFontMetrics().getHeight()));
            */
            g.dispose();
            bufferedImage = null;

            if (file.getPath().endsWith(".bmp"))
                file = new File(file.getPath().substring(0, file.getPath().lastIndexOf(".bmp"))+"_"+objFabric.getStrClothType()+"_"+index+".bmp");
            else
                file = new File(file.getPath()+"_"+objFabric.getStrClothType()+"_"+index+".bmp");
            ImageIO.write(texture, "BMP", file);
            
            if(objConfiguration.getBlnAuthenticateService()){
                ArrayList<File> filesToZip=new ArrayList<>();
                filesToZip.add(file);
                String zipFilePath=file.getAbsolutePath()+".zip";
                String passwordToZip = file.getName();
                new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                file.delete();
            }

            lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+file.getCanonicalPath());
        } catch (IOException ex) {
            new Logging("SEVERE",ClothView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (SQLException ex) {
            new Logging("SEVERE",ClothView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch(OutOfMemoryError ex){
            new Logging("SEVERE",FabricView.class.getName(),ex.toString(),null);
            lblStatus.setText(objDictionaryAction.getWord("MAXVALUE"));
        }
    }
    
    private void ScreenShoot(String actionName) {
        try {
            final WritableImage image = bodyContainer.snapshot(new SnapshotParameters(), null);
            if(actionName=="print"){
                new PrintView(SwingFXUtils.fromFXImage(image, null), 0, objCloth.getStrClothName(), objCloth.getObjConfiguration().getObjUser().getStrName());
                lblStatus.setText("");
                /*PrinterJob objPrinterJob = PrinterJob.getPrinterJob();
                if(objPrinterJob.printDialog()){
                    objPrinterJob.setPrintable(new Printable() {
                        @Override
                        public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
                            if (pageIndex != 0) {
                                return NO_SUCH_PAGE;
                            }
                            Graphics2D g2d=(Graphics2D)graphics;
                            g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
                            BufferedImage texture = SwingFXUtils.fromFXImage(image, null);
                            int wd= (texture.getWidth()<(int)pageFormat.getImageableWidth())?texture.getWidth():(int)pageFormat.getImageableWidth();
                            int ht= (texture.getHeight()<(int)pageFormat.getImageableHeight())?texture.getHeight():(int)pageFormat.getImageableHeight();
                            if((texture.getWidth()>(int)pageFormat.getImageableWidth())||(texture.getHeight()>(int)pageFormat.getImageableHeight())){
                                BufferedImage newBI = new BufferedImage(wd, ht, BufferedImage.TYPE_INT_ARGB);
                                for(int a=0; a<newBI.getWidth(); a++)
                                    for(int b=0; b<newBI.getHeight(); b++)
                                        newBI.setRGB(a, b, texture.getRGB(a, b));
                                texture=newBI;
                            }
                            graphics.drawImage(texture, 0, 0, wd, ht, null);
                            return PAGE_EXISTS;                    
                        }
                    });    
                    objPrinterJob.print();
                }
                objPrinterJob = null;*/
            } else if(actionName=="export"){
                FileChooser fileChoser=new FileChooser();
                fileChoser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTPNG")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
                FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
                fileChoser.getExtensionFilters().add(extFilterPNG);
                File file=fileChoser.showSaveDialog(clothStage);
                if(file==null)
                    return;
                else
                    clothStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+file.getAbsoluteFile().getName()+"]");   
                if (!file.getName().endsWith("png")) 
                    file = new File(file.getPath() +".png");
                ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
                
                if(objConfiguration.getBlnAuthenticateService()){
                    ArrayList<File> filesToZip=new ArrayList<>();
                    filesToZip.add(file);
                    String zipFilePath=file.getAbsolutePath()+".zip";
                    String passwordToZip = file.getName();
                    new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                    file.delete();
                }
                
                lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+file.getCanonicalPath());
            } else if(actionName == "Simulation"){
                BufferedImage myimage = SwingFXUtils.fromFXImage(image, null);
                int w = myimage.getWidth();
                int h = myimage.getHeight();
                int type = BufferedImage.TYPE_INT_RGB; 
                BufferedImage DaImage = new BufferedImage(h, w, type);
                Graphics2D g2 = DaImage.createGraphics();
                double x = (h - w)/2.0;
                double y = (w - h)/2.0;
                int angle = 270;
                AffineTransform at = AffineTransform.getTranslateInstance(x, y);
                at.rotate(Math.toRadians(angle), w/2.0, h/2.0);
                g2.drawImage(myimage, at, null);
                g2.dispose();
            
                File file = new File(System.getProperty("user.dir")+"\\mla\\Untitled12.png");
                file.delete();
                file = new File(System.getProperty("user.dir")+"\\mla\\Untitled12.png");
                ImageIO.write(DaImage, "png", file);
                newFunction(System.getProperty("user.dir")+"\\mla\\EnvelopeDistordTest.swf");
                lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+file.getCanonicalPath());
            }  else if(actionName == "ClothSimulation"){
                SimulatorEditView objSimulatorEditView = new SimulatorEditView(objConfiguration, SwingFXUtils.fromFXImage(image, null));
                lblStatus.setText(objDictionaryAction.getWord("ACTIONSIMULATION"));
            }   else if(actionName == "save"){
                
            }            
        } catch (IOException ex) {
            new Logging("SEVERE",ClothView.class.getName(),"ScreenShoot(): "+actionName+": "+ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } /*catch (PrinterException ex) {
            new Logging("SEVERE",ClothView.class.getName(),"ScreenShoot(): "+actionName+": "+ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }*/
        System.gc();
    }
    
    private void zoomInAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONZOOMINVIEW"));
        
        if((zoomFactor+.5)<=3){ //max 300%           
            zoomFactor=zoomFactor+.5;
            //inchFactor = (int)(12*zoomFactor);
            objCloth.setClothZoomFactor(zoomFactor);
            objCloth.setClothInchFactor(inchFactor);
            //first step
            for(int i =0; i<11; i++){
                widths[i]=(int)(widths[i]*(zoomFactor/(zoomFactor-.5)));
                heights[i]=(int)(heights[i]*(zoomFactor/(zoomFactor-.5)));
            }
            //second step (optional for cleraity)
            plotCloth();
            
            // setting heights and widths
            setWidthHeight();
            //set component sizes
            setComponentWidthHeight();
            //prepare components
            plotContainerGP();
        } else{
            lblStatus.setText(objDictionaryAction.getWord("MAXZOOMIN"));
        }
    }
    
    private void zoomNormalAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONZOOMNORMALVIEW"));
        
        //first step
        for(int i =0; i<11; i++){
            widths[i]=(int)(widths[i]/zoomFactor);
            heights[i]=(int)(heights[i]/zoomFactor);
        }

        zoomFactor=1;
        //inchFactor = (int)(12*zoomFactor);
        objCloth.setClothZoomFactor(zoomFactor);
        objCloth.setClothInchFactor(inchFactor);
        
        //second step (optional for cleraity)
        plotCloth();
        
        // setting heights and widths
        setWidthHeight();
        //set component sizes
        setComponentWidthHeight();
        //prepare components
        plotContainerGP();
    }
    
    private void zoomOutAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONZOOMOUTVIEW"));
        if((zoomFactor-.5)>=1){ //max 300%
            zoomFactor=zoomFactor-.5;
            //inchFactor = (int)(12*zoomFactor);
            objCloth.setClothZoomFactor(zoomFactor);
            objCloth.setClothInchFactor(inchFactor);
            
            //first step
            for(int i =0; i<11; i++){
                widths[i]=(int)(widths[i]*(zoomFactor/(zoomFactor+.5)));
                heights[i]=(int)(heights[i]*(zoomFactor/(zoomFactor+.5)));
            }
            
            //second step (optional for cleraity)
            plotCloth();
            
            // setting heights and widths
            setWidthHeight();
            //set component sizes
            setComponentWidthHeight();
            //prepare components
            plotContainerGP();
        } else{
            lblStatus.setText(objDictionaryAction.getWord("MAXZOOMOUT"));
        }
    }
    

    private void weaveUtilityAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONWEAVEEDITOR"));
        if(objCloth.getStrClothId()!=null && objCloth.getStrClothType()!=null){
            objConfiguration.setStrRecentCloth(objCloth.getStrClothId());
            objConfiguration.strWindowFlowContext="ClothEditor";
            saveContext();
            clothStage.close();
            WeaveView objWeaveView = new WeaveView(objConfiguration);
        } else{
            new MessageView("error", objDictionaryAction.getWord("NOVALUE"), objDictionaryAction.getWord("UNSAVEDDATA"));
            lblStatus.setText(objDictionaryAction.getWord("UNSAVEDDATA"));
        }
    }
    private void artworkUtilityAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONARTWORKEDITOR"));
        if(objCloth.getStrClothId()!=null && objCloth.getStrClothType()!=null){
            objConfiguration.setStrRecentCloth(objCloth.getStrClothId());
            objConfiguration.strWindowFlowContext="ClothEditor";
            saveContext();
            clothStage.close();
            ArtworkView objArtworkView = new ArtworkView(objConfiguration);
        } else{
            new MessageView("error", objDictionaryAction.getWord("NOVALUE"), objDictionaryAction.getWord("UNSAVEDDATA"));
            lblStatus.setText(objDictionaryAction.getWord("UNSAVEDDATA"));
        }  
    }
    private void fabricUtilityAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONFABRICEDITOR"));
        if(objCloth.getStrClothId()!=null && objCloth.getStrClothType()!=null){
            objConfiguration.setStrRecentCloth(objCloth.getStrClothId());
            objConfiguration.strWindowFlowContext="ClothEditor";
            saveContext();
            clothStage.close();
            FabricView objFabricView = new FabricView(objConfiguration);
        } else{
            new MessageView("error", objDictionaryAction.getWord("NOVALUE"), objDictionaryAction.getWord("UNSAVEDDATA"));
            lblStatus.setText(objDictionaryAction.getWord("UNSAVEDDATA"));
        }  
    }
    /**
     * Returns actual (formal name) of a garment component.
     * @since 0.8.3
     * @added 21 August 2017
     * @author Aatif Ahmad Khan
     * @param component - ImageView of a garment component
     * @return  Formal Name of that garment component
     */
    private String getComponentName(int index){
        if(index==0)
            return "Left Border";
        else if(index==1)
            return "Blouse";//"Blouse";
        else if(index==2)
            return "Leftside Left Cross Border";
        else if(index==3)
            return "Left Pallu";
        else if(index==4)
            return "Leftside Right Cross Border";
        else if(index==5)
            return "Body";//"Body";
        else if(index==6)
            return "Skirt";//"Skirt";
        else if(index==7)
            return "Rightside Left Cross Border";
        else if(index==8)
            return "Right Pallu";
        else if(index==9)
            return "Rightside Right Cross Border";
        else if(index==10)
            return "Right Border";
        else
            return null;
    }
    private String getComponentName(ImageView component){
        if(component.equals(leftBorder))
            return "Left Border";
        else if(component.equals(blouse))
            return "Blouse";//"Blouse";
        else if(component.equals(leftsideLeftCrossBorder))
            return "Leftside Left Cross Border";
        else if(component.equals(leftPallu))
            return "Left Pallu";
        else if(component.equals(leftsideRightCrossBorder))
            return "Leftside Right Cross Border";
        else if(component.equals(body))
            return "Body";//"Body";
        else if(component.equals(skirt))
            return "Skirt";//"Skirt";
        else if(component.equals(rightsideLeftCrossBorder))
            return "Rightside Left Cross Border";
        else if(component.equals(rightPallu))
            return "Right Pallu";
        else if(component.equals(rightsideRightCrossBorder))
            return "Rightside Right Cross Border";
        else if(component.equals(rightBorder))
            return "Right Border";
        else
            return null;
    }
    
    
 private int getComponentIndex(String component){
        if(component.equalsIgnoreCase("Left Border"))
            return 0;
        else if(component.equalsIgnoreCase("Blouse"))
            return 1;
        else if(component.equalsIgnoreCase("Leftside Left Cross Border"))
            return 2;
        else if(component.equalsIgnoreCase("Left Pallu"))
            return 3;
        else if(component.equalsIgnoreCase("Leftside Right Cross Border"))
            return 4;
        else if(component.equalsIgnoreCase("Body"))
            return 5;
        else if(component.equalsIgnoreCase("Skirt"))
            return 6;
        else if(component.equalsIgnoreCase("Rightside Left Cross Border"))
            return 7;
        else if(component.equalsIgnoreCase("Right Pallu"))
            return 8;
        else if(component.equalsIgnoreCase("Rightside Right Cross Border"))
            return 9;
        else if(component.equalsIgnoreCase("Right Border"))
            return 10;
        else
            return -1;
    }
    
    private int getComponentIndex(ImageView component){
        if(component.equals(leftBorder))
            return 0;
        else if(component.equals(blouse))
            return 1;
        else if(component.equals(leftsideLeftCrossBorder))
            return 2;
        else if(component.equals(leftPallu))
            return 3;
        else if(component.equals(leftsideRightCrossBorder))
            return 4;
        else if(component.equals(body))
            return 5;
        else if(component.equals(skirt))
            return 6;
        else if(component.equals(rightsideLeftCrossBorder))
            return 7;
        else if(component.equals(rightPallu))
            return 8;
        else if(component.equals(rightsideRightCrossBorder))
            return 9;
        else if(component.equals(rightBorder))
            return 10;
        else
            return -1;
    }
    
    private void enableAllComponent(){
        leftBorderEnabled=true;
        rightBorderEnabled=true;
        leftsideLeftCrossBorderEnabled=true;
        leftsideRightCrossBorderEnabled=true;
        rightsideLeftCrossBorderEnabled=true;
        rightsideRightCrossBorderEnabled=true;
        leftPalluEnabled=true;
        rightPalluEnabled=true;
        blouseEnabled=true;
        skirtEnabled=true;
    }

    private void disableAllComponent(){
        leftBorderEnabled=false;
        rightBorderEnabled=false;
        leftsideLeftCrossBorderEnabled=false;
        leftsideRightCrossBorderEnabled=false;
        rightsideLeftCrossBorderEnabled=false;
        rightsideRightCrossBorderEnabled=false;
        leftPalluEnabled=false;
        rightPalluEnabled=false;
        blouseEnabled=false;
        skirtEnabled=false;
    }
    
    private void loadComponent(){
        // body is always enabled, hence no variable bodyEnabled
        leftBorderEnabled=false;
        rightBorderEnabled=false;
        leftsideLeftCrossBorderEnabled=false;
        leftsideRightCrossBorderEnabled=false;
        rightsideLeftCrossBorderEnabled=false;
        rightsideRightCrossBorderEnabled=false;
        leftPalluEnabled=false;
        rightPalluEnabled=false;
        blouseEnabled=false;
        skirtEnabled=false;
        
        /*
        objCloth.mapClothFabric.get(fabric type)=null //not the part of layout
        objCloth.mapClothFabric.get(fabric type)=fabric name //part of layout but does not has fabric
        objCloth.mapClothFabric.get("Body")=Instance of Cloth //part of layout and has fabric
        
        get components from MapRecent on layout, and load their context
        */
        if(objCloth.mapClothFabric.get("Body")!=null){
            // body is always enabled
        }
        if(objCloth.mapClothFabric.get("Left Border")!=null){
            leftBorderEnabled=true;
        }
        if(objCloth.mapClothFabric.get("Right Border")!=null){
            rightBorderEnabled=true;
        }
        if(objCloth.mapClothFabric.get("Leftside Left Cross Border")!=null){
            leftsideLeftCrossBorderEnabled=true;
        }
        if(objCloth.mapClothFabric.get("Letside Right Cross Border")!=null){
            leftsideRightCrossBorderEnabled=true;
        }
        if(objCloth.mapClothFabric.get("Rightside Left Cross Border")!=null){
            rightsideLeftCrossBorderEnabled=true;
        }
        if(objCloth.mapClothFabric.get("Rightside Right Cross Border")!=null){
            rightsideRightCrossBorderEnabled=true;
        }
        if(objCloth.mapClothFabric.get("Left Pallu")!=null){
            leftPalluEnabled=true;
        }
        if(objCloth.mapClothFabric.get("Right Pallu")!=null){
            rightPalluEnabled=true;
        }
        if(objCloth.mapClothFabric.get("Blouse")!=null){
            blouseEnabled=true;
        }
        if(objCloth.mapClothFabric.get("Skirt")!=null){
            skirtEnabled=true;
        }
        /*if(objCloth.mapClothFabric.get("Konia")!=null){
            koniaEnabled=true;
        }*/
    }
    private void setSymmetryPolicy(){
        if(objCloth.mapClothFabricDetails.get("Left Border")[0].equalsIgnoreCase(objCloth.mapClothFabricDetails.get("Right Border")[0])){
            borderLinkPolicy = true;
        }else{
            borderLinkPolicy = false;
        }
        if(objCloth.mapClothFabricDetails.get("Left Pallu")[0].equalsIgnoreCase(objCloth.mapClothFabricDetails.get("Right Pallu")[0])){
            palluLinkPolicy = true;
        } else{
            palluLinkPolicy = false;
        }
        if(objCloth.getStrClothType().equalsIgnoreCase("SAREE")){
            if(objCloth.mapClothFabricDetails.get("Leftside Left Cross Border")[0].equalsIgnoreCase(objCloth.mapClothFabricDetails.get("Leftside Right Cross Border")[0])){
                crossBorderLinkPolicy = true;
            } else{
                crossBorderLinkPolicy = false;
            }
        } else {
            if(objCloth.mapClothFabricDetails.get("Leftside Left Cross Border")[0].equalsIgnoreCase(objCloth.mapClothFabricDetails.get("Leftside Right Cross Border")[0]) 
                    && objCloth.mapClothFabricDetails.get("Rightside Left Cross Border")[0].equalsIgnoreCase(objCloth.mapClothFabricDetails.get("Rightside Right Cross Border")[0])){
                crossBorderLinkPolicy = true;
            } else{
                crossBorderLinkPolicy = false;
            }
        }
    }
    /**
     * Setting initial widths & heights of all components as per full custom view
     * Note: this function modifies arrays only (not W/H of actual components)
     * @added 29 August 2017
     * @author Aatif Ahmad Khan
     * @since 0.8.3
     */
    private void setWidthHeight(){
        // widths & heights are subject to availability of componets (i.e. whether they are enabled or not)
        
        /* Needed Parameters */
        int totalWidth=(int)objConfiguration.WIDTH;
        int totalHeight=(int)(48*inchFactor*zoomFactor);
        
        // Left Border
        if(leftBorderEnabled){
            if(widths[0]==0 && heights[0]==0){
                widths[0]=totalWidth;
                heights[0]=(int)(3*inchFactor*zoomFactor);
            }
            widths[0]=totalWidth;
        } else{
            widths[0]=0;
            heights[0]=0;
        }
        // Right Border
        if(rightBorderEnabled){
            if(widths[10]==0 && heights[10]==0){
                widths[10]=totalWidth;
                heights[10]=(int)(3*inchFactor*zoomFactor);
            }
            widths[10]=totalWidth;
        } else{
            widths[10]=0;
            heights[10]=0;
        }
        
        // Blouse
        if(blouseEnabled){
            if(widths[1]==0 && heights[1]==0){
                widths[1]=(int)(24*inchFactor*zoomFactor);
                heights[1]=totalHeight-(heights[0]+heights[10]);
            }
            heights[1]=totalHeight-(heights[0]+heights[10]);
        }else{
            widths[1]=0;
            heights[1]=0;
        }
        // Left Side Left Cross Border
        if(leftsideLeftCrossBorderEnabled){
            if(widths[2]==0 && heights[2]==0){
                widths[2]=(int)(3*inchFactor*zoomFactor);
                heights[2]=totalHeight-(heights[0]+heights[10]);
            }
            heights[2]=totalHeight-(heights[0]+heights[10]);
        } else{
            widths[2]=0;
            heights[2]=0;
        }
        // Left side Pallu
        if(leftPalluEnabled){
            if(widths[3]==0 && heights[3]==0){
                widths[3]=(int)(24*inchFactor*zoomFactor);
                heights[3]=totalHeight-(heights[0]+heights[10]);
            }
            heights[3]=totalHeight-(heights[0]+heights[10]);
        }else{
            widths[3]=0;
            heights[3]=0;
        }
        //Left Side Right Cross Border
        if(leftsideRightCrossBorderEnabled){
            if(widths[4]==0 && heights[4]==0){
                widths[4]=(int)(3*inchFactor*zoomFactor);
                heights[4]=totalHeight-(heights[0]+heights[10]);
            }
            heights[4]=totalHeight-(heights[0]+heights[10]);
        }else{
            widths[4]=0;
            heights[4]=0;
        }
        //Right side Left cross border
        if(rightsideLeftCrossBorderEnabled){
            if(widths[7]==0 && heights[7]==0){
                widths[7]=(int)(3*inchFactor*zoomFactor);
                heights[7]=totalHeight-(heights[0]+heights[10]);
            }
            heights[7]=totalHeight-(heights[0]+heights[10]);
        }else{
            widths[7]=0;
            heights[7]=0;
        }
        // Right side Pallu
        if(rightPalluEnabled){
            if(widths[8]==0 && heights[8]==0){
                widths[8]=(int)(24*inchFactor*zoomFactor);
                heights[8]=totalHeight-(heights[0]+heights[10]);
            }
            heights[8]=totalHeight-(heights[0]+heights[10]);
        }else{
            widths[8]=0;
            heights[8]=0;
        }
        //Right side Right cross border
        if(rightsideRightCrossBorderEnabled){
            if(widths[9]==0 && heights[9]==0){
                widths[9]=(int)(3*inchFactor*zoomFactor);
                heights[9]=totalHeight-(heights[0]+heights[10]);
            }
            heights[9]=totalHeight-(heights[0]+heights[10]);
        } else {
            widths[9]=0;
            heights[9]=0;
        }
        
        /* Calculated Parameters */        
        // Skirt
        if(skirtEnabled){
            if(widths[6]==0 && heights[6]==0){
                widths[6]=totalWidth-(widths[1]+widths[2]+widths[3]+widths[4]+widths[7]+widths[8]+widths[9]);
                heights[6]=(int)(6*inchFactor*zoomFactor);
            }
            widths[6]=totalWidth-(widths[1]+widths[2]+widths[3]+widths[4]+widths[7]+widths[8]+widths[9]);
        } else {
            widths[6]=0;
            heights[6]=0;
        }
        
        // Body (always enabled)
        heights[5]=totalHeight-(heights[0]+heights[10]+heights[6]);
        widths[5]=totalWidth-(widths[1]+widths[2]+widths[3]+widths[4]+widths[7]+widths[8]+widths[9]);
    }
    private void resetClothLayout(){
        setWidthHeight();
        for(int a=0; a<bodyContainer.getChildren().size(); a++){
            ScrollPane comp=(ScrollPane)bodyContainer.getChildren().get(a);
            symmetryAssignment((ImageView)comp.getContent());
        }
        
        System.gc();
        saveContext();
        
        plotCloth();
        // setting heights and widths
        setWidthHeight();
        //set component sizes
        setComponentWidthHeight();
        //prepare components
        plotContainerGP();
    }
    private void symmetryAssignment(ImageView component){
        //resetWidthHeight();
        if(component.getUserData()!=null && (component.getUserData() instanceof Fabric)){
            Fabric objFabric = (Fabric) component.getUserData();
            int index = getComponentIndex(component);
            String key =  getComponentName(component);
            //MAINFABRICID, ROTATIONANGLE, REPEATMODE, FABRICREPEAT, WIDTH, HEIGHT
            String[] clothFabric = objCloth.mapClothFabricDetails.get(key);
            /*
            rotations[index]=Integer.parseInt(clothFabric[1]);
            repeatMode[index]=clothFabric[2];
            repeats[index]=Integer.parseInt(clothFabric[3]);
            ends[index]=Integer.parseInt(clothFabric[4]);
            picks[index]=Integer.parseInt(clothFabric[5]);
            */
            if(component.equals(leftBorder)|| component.equals(rightBorder)){
                if(borderLinkPolicy){
                    leftBorder.setUserData(objFabric);
                    setComponentProperty(leftBorder, clothFabric);
                    rightBorder.setUserData(objFabric);
                    setComponentProperty(rightBorder, clothFabric);//rotate right border by 180
                } else{
                    if(component.equals(leftBorder)){
                        leftBorder.setUserData(objFabric);
                        setComponentProperty(leftBorder, clothFabric);
                    } else{
                        rightBorder.setUserData(objFabric);
                        setComponentProperty(rightBorder, clothFabric);//rotate right border by 180
                    }
                }
            } else if( component.equals(leftsideLeftCrossBorder)|| component.equals(leftsideRightCrossBorder)|| component.equals(rightsideLeftCrossBorder)|| component.equals(rightsideRightCrossBorder)){
                if(crossBorderLinkPolicy){
                    leftsideLeftCrossBorder.setUserData(objFabric);
                    setComponentProperty(leftsideLeftCrossBorder, clothFabric);
                    leftsideRightCrossBorder.setUserData(objFabric);
                    setComponentProperty(leftsideRightCrossBorder, clothFabric);
                    rightsideLeftCrossBorder.setUserData(objFabric);
                    setComponentProperty(rightsideLeftCrossBorder, clothFabric);
                    rightsideRightCrossBorder.setUserData(objFabric);
                    setComponentProperty(rightsideRightCrossBorder, clothFabric);
                } else{
                    component.setUserData(objFabric);
                    setComponentProperty(component, clothFabric);
                }
            } else if( component.equals(leftPallu)|| component.equals(rightPallu)){
                if(palluLinkPolicy){
                    leftPallu.setUserData(objFabric);
                    setComponentProperty(leftPallu, clothFabric);
                    rightPallu.setUserData(objFabric);
                    setComponentProperty(rightPallu, clothFabric);//rotate by 180
                }  else{
                    if(component.equals(leftPallu)){
                        leftPallu.setUserData(objFabric);
                        setComponentProperty(leftPallu, clothFabric);
                    } else{
                        rightPallu.setUserData(objFabric);
                        setComponentProperty(rightPallu, clothFabric);//rotate by 180
                    }
                }
            } else{
                 component.setUserData(objFabric);
                 setComponentProperty(component, clothFabric);
            }
        }
    }
    private void setComponentProperty(ImageView component, String[] clothFabric){
        int index = getComponentIndex(component);
        String key =  getComponentName(component);
                
        component.setId(clothFabric[0]);
        rotations[index]=Integer.parseInt(clothFabric[1]);
        repeatMode[index]=clothFabric[2];
        repeats[index]=Integer.parseInt(clothFabric[3]);
        ends[index]=Integer.parseInt(clothFabric[4]);
        picks[index]=Integer.parseInt(clothFabric[5]);
    }
    private void initCloth(){
        //initailize symmetry properties
        palluLinkPolicy = true;
        borderLinkPolicy = true;
        crossBorderLinkPolicy = true;

        // initially setting all widths/heights to zero
        widths  = new int[11];
        heights = new int[11];
        ends = new int[11];
        picks = new int[11];
        repeats = new int[11];
        rotations = new int[11];
        repeatMode = new String[11];        
    }      
    /**
     * This function initialize components and assign custom functions
     * @added 30 August 2017
     * @author Amit Kumar Singh
     * @since 0.1.0
     */
    private void initializePane(){
        lefttopKoniaPane = new ScrollPane();
        leftbottomKoniaPane = new ScrollPane();
        righttopKoniaPane = new ScrollPane();
        rightbottomKoniaPane = new ScrollPane();
        leftsideLeftCrossBorderPane = new ScrollPane();
        leftsideRightCrossBorderPane = new ScrollPane();
        rightsideLeftCrossBorderPane=new ScrollPane();
        rightsideRightCrossBorderPane=new ScrollPane();
        leftBorderPane = new ScrollPane();
        rightBorderPane = new ScrollPane();
        leftPalluPane = new ScrollPane();
        rightPalluPane = new ScrollPane();
        skirtPane = new ScrollPane();
        blousePane = new ScrollPane();
        bodyPane = new ScrollPane();
        
        leftsideLeftCrossBorderPane.setMinWidth(5);
        leftsideLeftCrossBorderPane.setMinHeight(5);
        leftsideRightCrossBorderPane.setMinWidth(5);
        leftsideRightCrossBorderPane.setMinHeight(5);
        rightsideLeftCrossBorderPane.setMinWidth(5);
        rightsideLeftCrossBorderPane.setMinHeight(5);
        rightsideRightCrossBorderPane.setMinWidth(5);
        rightsideRightCrossBorderPane.setMinHeight(5);
        leftBorderPane.setMinWidth(5);
        leftBorderPane.setMinHeight(5);
        rightBorderPane.setMinWidth(5);
        rightBorderPane.setMinHeight(5);
        leftPalluPane.setMinWidth(5);
        leftPalluPane.setMinHeight(5);
        rightPalluPane.setMinWidth(5);
        rightPalluPane.setMinHeight(5);
        blousePane.setMinWidth(5);
        blousePane.setMinHeight(5);
        skirtPane.setMinWidth(5);
        skirtPane.setMinHeight(5);
        bodyPane.setMinWidth(5);
        bodyPane.setMinHeight(5);
                
        lefttopKoniaPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        leftbottomKoniaPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        righttopKoniaPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        rightbottomKoniaPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        leftsideLeftCrossBorderPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        leftsideRightCrossBorderPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        rightsideLeftCrossBorderPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        rightsideRightCrossBorderPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        leftBorderPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        rightBorderPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        leftPalluPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        rightPalluPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        skirtPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        blousePane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        bodyPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        
        lefttopKoniaPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        leftbottomKoniaPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        righttopKoniaPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        rightbottomKoniaPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        leftsideLeftCrossBorderPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        leftsideRightCrossBorderPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        rightsideLeftCrossBorderPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        rightsideRightCrossBorderPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        leftBorderPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        rightBorderPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        leftPalluPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        rightPalluPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        skirtPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        blousePane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        bodyPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        
        componentContextMenu(lefttopKoniaPane,"Konia");
        componentContextMenu(leftbottomKoniaPane,"Konia");
        componentContextMenu(righttopKoniaPane,"Konia");
        componentContextMenu(rightbottomKoniaPane,"Konia");
        componentContextMenu(leftsideLeftCrossBorderPane,"Cross Border");
        componentContextMenu(leftsideRightCrossBorderPane,"Cross Border");
        componentContextMenu(rightsideLeftCrossBorderPane,"Cross Border");
        componentContextMenu(rightsideRightCrossBorderPane,"Cross Border");
        componentContextMenu(leftBorderPane,"Border");
        componentContextMenu(rightBorderPane,"Border");
        componentContextMenu(leftPalluPane,"Pallu");
        componentContextMenu(rightPalluPane,"Pallu");
        componentContextMenu(skirtPane,"Skirt");
        componentContextMenu(blousePane,"Blouse");
        componentContextMenu(bodyPane,"Body");
        
        lefttopKonia = new ImageView("/media/cloth_konia.png");
        leftbottomKonia = new ImageView("/media/cloth_konia.png");
        righttopKonia = new ImageView("/media/cloth_konia.png");
        rightbottomKonia = new ImageView("/media/cloth_konia.png");
        leftsideLeftCrossBorder = new ImageView("/media/cloth_crossborder.png");
        leftsideRightCrossBorder = new ImageView("/media/cloth_crossborder.png");
        rightsideLeftCrossBorder = new ImageView("/media/cloth_crossborder.png");
        rightsideRightCrossBorder = new ImageView("/media/cloth_crossborder.png");
        leftBorder = new ImageView("/media/cloth_border.png");
        rightBorder = new ImageView("/media/cloth_border.png");
        leftPallu = new ImageView("/media/cloth_pallu.png");
        rightPallu = new ImageView("/media/cloth_pallu.png");
        skirt = new ImageView("/media/cloth_skirt.png");
        blouse = new ImageView("/media/cloth_blouse.png");
        body = new ImageView("/media/cloth_body.png");
        
        isPaneDragable=false;
        xInitialPane=0;
        yInitialPane=0;
        
        //componentDragAndSave(lefttopKonia);
        //componentDragAndSave(leftbottomKonia);
        //componentDragAndSave(righttopKonia);
        //componentDragAndSave(rightbottomKonia);
        //componentDragAndSave(leftsideLeftCrossBorder);
        //componentDragAndSave(leftsideRightCrossBorder);
        //componentDragAndSave(rightsideLeftCrossBorder);
        //componentDragAndSave(rightsideRightCrossBorder);
        //componentDragAndSave(leftBorder);
        //componentDragAndSave(rightBorder);
        //componentDragAndSave(leftPallu);
        //componentDragAndSave(rightPallu);
        //componentDragAndSave(skirt);
        //componentDragAndSave(blouse);
        //componentDragAndSave(body);
        
        lefttopKonia.setId(null);
        leftbottomKonia.setId(null);
        righttopKonia.setId(null);
        rightbottomKonia.setId(null);
        leftsideLeftCrossBorder.setId(null);
        leftsideRightCrossBorder.setId(null);
        rightsideLeftCrossBorder.setId(null);
        rightsideRightCrossBorder.setId(null);
        leftBorder.setId(null);
        rightBorder.setId(null);
        leftPallu.setId(null);
        rightPallu.setId(null);
        skirt.setId(null);
        blouse.setId(null);
        body.setId(null);
        
        lefttopKoniaPane.setContent(lefttopKonia);
        leftbottomKoniaPane.setContent(leftbottomKonia);
        righttopKoniaPane.setContent(lefttopKonia);
        rightbottomKoniaPane.setContent(leftbottomKonia);
        leftsideLeftCrossBorderPane.setContent(leftsideLeftCrossBorder);
        leftsideRightCrossBorderPane.setContent(leftsideRightCrossBorder);
        rightsideLeftCrossBorderPane.setContent(rightsideLeftCrossBorder);
        rightsideRightCrossBorderPane.setContent(rightsideRightCrossBorder);
        leftBorderPane.setContent(leftBorder);
        rightBorderPane.setContent(rightBorder);
        leftPalluPane.setContent(leftPallu);
        rightPalluPane.setContent(rightPallu);
        skirtPane.setContent(skirt);
        blousePane.setContent(blouse);
        bodyPane.setContent(body);
    }

    /**
     * This function actually sets Fit/Preferred W/H of components based on widths & heights array
     * @added 30 August 2017
     * @author Aatif Ahamed Khan
     * @since 0.8.3
     * @author Amit Kumar Singh
     * @since 0.9.0
     */
    private void setComponentWidthHeight(){
        leftBorder.setFitWidth(widths[0]);
        leftBorder.setFitHeight(heights[0]);
        blouse.setFitWidth(widths[1]);
        blouse.setFitHeight(heights[1]);
        leftsideLeftCrossBorder.setFitWidth(widths[2]);
        leftsideLeftCrossBorder.setFitHeight(heights[2]);
        leftPallu.setFitWidth(widths[3]);
        leftPallu.setFitHeight(heights[3]);
        leftsideRightCrossBorder.setFitWidth(widths[4]);
        leftsideRightCrossBorder.setFitHeight(heights[4]);
        body.setFitWidth(widths[5]);
        body.setFitHeight(heights[5]);
        skirt.setFitWidth(widths[6]);
        skirt.setFitHeight(heights[6]);
        rightsideLeftCrossBorder.setFitWidth(widths[7]);
        rightsideLeftCrossBorder.setFitHeight(heights[7]);
        rightPallu.setFitWidth(widths[8]);
        rightPallu.setFitHeight(heights[8]);
        rightsideRightCrossBorder.setFitWidth(widths[9]);
        rightsideRightCrossBorder.setFitHeight(heights[9]);
        rightBorder.setFitWidth(widths[10]);
        rightBorder.setFitHeight(heights[10]);
        
        leftBorderPane.setPrefWidth(widths[0]);
        leftBorderPane.setPrefHeight(heights[0]);
        blousePane.setPrefWidth(widths[1]);
        blousePane.setPrefHeight(heights[1]);
        leftsideLeftCrossBorderPane.setPrefWidth(widths[2]);
        leftsideLeftCrossBorderPane.setPrefHeight(heights[2]);
        leftPalluPane.setPrefWidth(widths[3]);
        leftPalluPane.setPrefHeight(heights[3]);
        leftsideRightCrossBorderPane.setPrefWidth(widths[4]);
        leftsideRightCrossBorderPane.setPrefHeight(heights[4]);
        bodyPane.setPrefWidth(widths[5]);
        bodyPane.setPrefHeight(heights[5]);
        skirtPane.setPrefWidth(widths[6]);
        skirtPane.setPrefHeight(heights[6]);
        rightsideLeftCrossBorderPane.setPrefWidth(widths[7]);
        rightsideLeftCrossBorderPane.setPrefHeight(heights[7]);
        rightPalluPane.setPrefWidth(widths[8]);
        rightPalluPane.setPrefHeight(heights[8]);
        rightsideRightCrossBorderPane.setPrefWidth(widths[9]);
        rightsideRightCrossBorderPane.setPrefHeight(heights[9]);
        rightBorderPane.setPrefWidth(widths[10]);
        rightBorderPane.setPrefHeight(heights[10]);
    }
    private void plotContainerGP(){
        // plot layout
    	positionGP=new byte[11][4];
        byte rowCount=0;
        byte columnCount=0;
        /**
         * positionGP  StartColumn StartRow    ColumnSpan  RowSpan
         * leftBorder (0)
	 * blouse (1)
         * leftsideleftCrossBorder (2)
         * leftPallu (3)
         * leftsiderightCrossBorder (4)
         * body (5)
	 * skirt (6)
         * rightsideLeftCrossBorder (7)
         * rightPallu (8)
         * rightsideRightCrossBorder (9)
         * rightBorder (10)
         */
        
        //adding body to initial position
	positionGP[5][0]=0;
        positionGP[5][1]=0;
        positionGP[5][2]=1;
        positionGP[5][3]=1;
		
	if(leftBorderEnabled){
            // add leftBorder pos
            positionGP[0][0]=columnCount;
            positionGP[0][1]=rowCount;
            positionGP[0][2]=(byte)(columnCount+1);
            positionGP[0][3]=(byte)(rowCount+1);
            
            rowCount++;
            // update body pos
            positionGP[5][1]=rowCount;
        }
        if(blouseEnabled){
            // add blouse pos
            positionGP[1][0]=columnCount;
            positionGP[1][1]=rowCount;
            positionGP[1][2]=1;
            positionGP[1][3]=1;
            
            columnCount++;
            // update column span of leftBorder
            if(leftBorderEnabled)
                positionGP[0][2]=(byte)(columnCount+1);
            // update body position
            positionGP[5][0]=columnCount;
        }
        if(leftsideLeftCrossBorderEnabled){
            // add leftCrossBorder pos
            positionGP[2][0]=columnCount;
            positionGP[2][1]=rowCount;
            positionGP[2][2]=1;
            positionGP[2][3]=1;
            
            columnCount++;
            // update column span of leftBorder
            if(leftBorderEnabled)
                positionGP[0][2]=(byte)(columnCount+1);
            // update body pos
            positionGP[5][0]=columnCount;
        }
        if(leftPalluEnabled){
            // add leftPallu pos
            positionGP[3][0]=columnCount;
            positionGP[3][1]=rowCount;
            positionGP[3][2]=1;
            positionGP[3][3]=1;
            columnCount++;
            // update column span of leftBorder
            if(leftBorderEnabled)
                positionGP[0][2]=(byte)(columnCount+1);
            // update body pos
            positionGP[5][0]=columnCount;
        }
        if(leftsideRightCrossBorderEnabled){
            // add rightCrossBorder pos
            positionGP[4][0]=columnCount;
            positionGP[4][1]=rowCount;
            positionGP[4][2]=1;
            positionGP[4][3]=1;
            
            columnCount++;
            // update column span of leftBorder
            if(leftBorderEnabled)
                positionGP[0][2]=(byte)(columnCount+1);
            // update body pos
            positionGP[5][0]=columnCount;
        }
        columnCount++;
        if(rightsideLeftCrossBorderEnabled){
            // add rightsideLeftCrossBorder pos
            positionGP[7][0]=columnCount;
            positionGP[7][1]=rowCount;
            positionGP[7][2]=1;
            positionGP[7][3]=1;
        
            columnCount++;
            // update column span of leftBorder
            if(leftBorderEnabled)
                positionGP[0][2]=(byte)(columnCount+1);
        }
        if(rightPalluEnabled){
            // add rightsideLeftCrossBorder pos
            positionGP[8][0]=columnCount;
            positionGP[8][1]=rowCount;
            positionGP[8][2]=1;
            positionGP[8][3]=1;
        
            columnCount++;
            // update column span of leftBorder
            if(leftBorderEnabled)
                positionGP[0][2]=(byte)(columnCount+1);
        }
        if(rightsideRightCrossBorderEnabled){
            // add rightsideLeftCrossBorder pos
            positionGP[9][0]=columnCount;
            positionGP[9][1]=rowCount;
            positionGP[9][2]=1;
            positionGP[9][3]=1;
        
            columnCount++;
            // update column span of leftBorder
            if(leftBorderEnabled)
                positionGP[0][2]=(byte)(columnCount+1);
        }
        rowCount++;
	if(skirtEnabled){
            // add skirt pos
            positionGP[6][0]=positionGP[5][0];
            positionGP[6][1]=(byte)(positionGP[5][1]+1);
            positionGP[6][2]=1;
            positionGP[6][3]=1;
            
            rowCount++;
            // update row span of blouse
            if(blouseEnabled)
                positionGP[1][3]=2;
            // update row span of leftCrossBorder
            if(leftsideLeftCrossBorderEnabled)
                positionGP[2][3]=2;
            // update row span of leftPallu
            if(leftPalluEnabled)
                positionGP[3][3]=2;
            // update row span of righrCrossBorder
            if(leftsideRightCrossBorderEnabled)
                positionGP[4][3]=2;
            // update row span of leftCrossBorder
            if(rightsideLeftCrossBorderEnabled)
                positionGP[7][3]=2;
            // update row span of leftPallu
            if(rightPalluEnabled)
                positionGP[8][3]=2;
            // update row span of righrCrossBorder
            if(rightsideRightCrossBorderEnabled)
                positionGP[9][3]=2;
        }	
	if(rightBorderEnabled){
            positionGP[10][0]=0;
            positionGP[10][1]=rowCount;
            positionGP[10][2]=(byte)(columnCount+1);
            positionGP[10][3]=1;
        }
	
        //adding to gridpane
        bodyContainer.getChildren().clear();
        if(leftBorderEnabled)
            bodyContainer.add(leftBorderPane, positionGP[0][0], positionGP[0][1], positionGP[0][2], positionGP[0][3]);
        if(blouseEnabled)
            bodyContainer.add(blousePane, positionGP[1][0], positionGP[1][1], positionGP[1][2], positionGP[1][3]);
        if(leftsideLeftCrossBorderEnabled)
            bodyContainer.add(leftsideLeftCrossBorderPane, positionGP[2][0], positionGP[2][1], positionGP[2][2], positionGP[2][3]);
        if(leftPalluEnabled)
            bodyContainer.add(leftPalluPane, positionGP[3][0], positionGP[3][1], positionGP[3][2], positionGP[3][3]);
        if(leftsideRightCrossBorderEnabled)
            bodyContainer.add(leftsideRightCrossBorderPane, positionGP[4][0], positionGP[4][1], positionGP[4][2], positionGP[4][3]);
        //body always enabled
        bodyContainer.add(bodyPane, positionGP[5][0], positionGP[5][1], positionGP[5][2], positionGP[5][3]);
        if(skirtEnabled)
            bodyContainer.add(skirtPane, positionGP[6][0], positionGP[6][1], positionGP[6][2], positionGP[6][3]);
        if(rightsideLeftCrossBorderEnabled)
            bodyContainer.add(rightsideLeftCrossBorderPane, positionGP[7][0], positionGP[7][1], positionGP[7][2], positionGP[7][3]);
	if(rightPalluEnabled)
            bodyContainer.add(rightPalluPane, positionGP[8][0], positionGP[8][1], positionGP[8][2], positionGP[8][3]);
	if(rightsideRightCrossBorderEnabled)
            bodyContainer.add(rightsideRightCrossBorderPane, positionGP[9][0], positionGP[9][1], positionGP[9][2], positionGP[9][3]);
        if(rightBorderEnabled)
            bodyContainer.add(rightBorderPane, positionGP[10][0], positionGP[10][1], positionGP[10][2], positionGP[10][3]);
    }
 
    private void clearContext(){
        /*
        objConfiguration.mapRecentFabric.put("Body", null);
        objConfiguration.mapRecentFabric.put("Blouse", null);
        objConfiguration.mapRecentFabric.put("Skirt", null);
        objConfiguration.mapRecentFabric.put("Border", null);
        objConfiguration.mapRecentFabric.put("Pallu", null);
        objConfiguration.mapRecentFabric.put("Cross Border", null);
        objConfiguration.mapRecentFabric.put("Konia", null);
        */
        /*
        objConfiguration.mapRecentFabric.put("Left Border", null);
        objConfiguration.mapRecentFabric.put("Right Border", null);
        objConfiguration.mapRecentFabric.put("Leftside Left Cross Border", null);
        objConfiguration.mapRecentFabric.put("Leftside Right Cross Border", null);
        objConfiguration.mapRecentFabric.put("Rightside Left Cross Border", null);
        objConfiguration.mapRecentFabric.put("Rightside Right Cross Border", null);
        objConfiguration.mapRecentFabric.put("Left Pallu", null);
        objConfiguration.mapRecentFabric.put("Right Pallu", null);
        objConfiguration.mapRecentFabric.put("Blouse", null);
        objConfiguration.mapRecentFabric.put("Skirt", null);
        objConfiguration.mapRecentFabric.put("Body", null);
        */
        //MAINFABRICID, ROTATIONANGLE, REPEATMODE, FABRICREPEAT, ENDS, PICKS
        objCloth.mapClothFabric.put("Left Border", null);
        objCloth.mapClothFabricDetails.put("Left Border", new String[]{"","0","Rectangular (Default)","0","0","0"});
        objCloth.mapClothFabric.put("Blouse", null);
        objCloth.mapClothFabricDetails.put("Blouse", new String[]{"","0","Rectangular (Default)","0","0","0"});
        objCloth.mapClothFabric.put("Leftside Left Cross Border", null);
        objCloth.mapClothFabricDetails.put("Leftside Left Cross Border", new String[]{"","0","Rectangular (Default)","0","0","0"});
        objCloth.mapClothFabric.put("Left Pallu", null);
        objCloth.mapClothFabricDetails.put("Left Pallu", new String[]{"","0","Rectangular (Default)","0","0","0"});
        objCloth.mapClothFabric.put("Leftside Right Cross Border", null);
        objCloth.mapClothFabricDetails.put("Leftside Right Cross Border", new String[]{"","0","Rectangular (Default)","0","0","0"});
        objCloth.mapClothFabric.put("Body", null);
        objCloth.mapClothFabricDetails.put("Body", new String[]{"","0","Rectangular (Default)","0","0","0"});
        objCloth.mapClothFabric.put("Skirt", null);
        objCloth.mapClothFabricDetails.put("Skirt", new String[]{"","0","Rectangular (Default)","0","0","0"});
        objCloth.mapClothFabric.put("Rightside Left Cross Border", null);
        objCloth.mapClothFabricDetails.put("Rightside Left Cross Border", new String[]{"","0","Rectangular (Default)","0","0","0"});
        objCloth.mapClothFabric.put("Right Pallu", null);
        objCloth.mapClothFabricDetails.put("Right Pallu", new String[]{"","0","Rectangular (Default)","0","0","0"});
        objCloth.mapClothFabric.put("Rightside Right Cross Border", null);
        objCloth.mapClothFabricDetails.put("Rightside Right Cross Border", new String[]{"","0","Rectangular (Default)","0","0","0"});
        objCloth.mapClothFabric.put("Right Border", null);
        objCloth.mapClothFabricDetails.put("Right Border", new String[]{"","0","Rectangular (Default)","0","0","0"});
    }
 
    private void saveContext(){
        if(leftBorderEnabled && leftBorder.getUserData()!=null && (leftBorder.getUserData() instanceof Fabric)){
            objCloth.mapClothFabric.put("Left Border", (Fabric)leftBorder.getUserData());
            //MAINFABRICID, ROTATIONANGLE, REPEATMODE, FABRICREPEAT, ENDS, PICKS
            String[] fabricDetails = new String[]{"","0","Rectangular (Default)","0","0","0"};
            if(leftBorder.getId()!=null && leftBorder.getId()!="")
                fabricDetails[0]=leftBorder.getId();
            fabricDetails[1]=Integer.toString(rotations[0]);
            fabricDetails[2]=repeatMode[0];
            fabricDetails[3]=Integer.toString(repeats[0]);
            fabricDetails[4]=Integer.toString(ends[0]);
            fabricDetails[5]=Integer.toString(picks[0]);
            objCloth.mapClothFabricDetails.put("Left Border", fabricDetails);
        } else {
            objCloth.mapClothFabric.put("Left Border", null);
            if(leftBorderEnabled)
                objCloth.mapClothFabricDetails.put("Left Border", new String[]{"","90","Rectangular (Default)","1","0","0"});            
            else
                objCloth.mapClothFabricDetails.put("Left Border", new String[]{"","0","Rectangular (Default)","0","0","0"});
        }
        
        if(blouseEnabled && blouse.getUserData()!=null && (blouse.getUserData() instanceof Fabric)){
            objCloth.mapClothFabric.put("Blouse", (Fabric)blouse.getUserData());
            //MAINFABRICID, ROTATIONANGLE, REPEATMODE, FABRICREPEAT, ENDS, PICKS
            String[] fabricDetails = new String[]{"","0","Rectangular (Default)","0","0","0"};
            if(blouse.getId()!=null && blouse.getId()!="")
                fabricDetails[0]=blouse.getId();
            fabricDetails[1]=Integer.toString(rotations[1]);
            fabricDetails[2]=repeatMode[1];
            fabricDetails[3]=Integer.toString(repeats[1]);
            fabricDetails[4]=Integer.toString(ends[1]);
            fabricDetails[5]=Integer.toString(picks[1]);
            objCloth.mapClothFabricDetails.put("Blouse", fabricDetails);
        } else {
            objCloth.mapClothFabric.put("Blouse", null);
            if(blouseEnabled)
                objCloth.mapClothFabricDetails.put("Blouse", new String[]{"","90","Rectangular (Default)","1","0","0"});            
            else
                objCloth.mapClothFabricDetails.put("Blouse", new String[]{"","0","Rectangular (Default)","0","0","0"});
        }
        
        if(leftsideLeftCrossBorderEnabled && leftsideLeftCrossBorder.getUserData()!=null && (leftsideLeftCrossBorder.getUserData() instanceof Fabric)){
            objCloth.mapClothFabric.put("Leftside Left Cross Border", (Fabric)leftsideLeftCrossBorder.getUserData());
            //MAINFABRICID, ROTATIONANGLE, REPEATMODE, FABRICREPEAT, ENDS, PICKS
            String[] fabricDetails = new String[]{"","0","Rectangular (Default)","0","0","0"};
            if(leftsideLeftCrossBorder.getId()!=null && leftsideLeftCrossBorder.getId()!="")
                fabricDetails[0]=leftsideLeftCrossBorder.getId();
            fabricDetails[1]=Integer.toString(rotations[2]);
            fabricDetails[2]=repeatMode[2];
            fabricDetails[3]=Integer.toString(repeats[2]);
            fabricDetails[4]=Integer.toString(ends[2]);
            fabricDetails[5]=Integer.toString(picks[2]);
            objCloth.mapClothFabricDetails.put("Leftside Left Cross Border", fabricDetails);
        } else {
            objCloth.mapClothFabric.put("Leftside Left Cross Border", null);
            if(leftsideLeftCrossBorderEnabled)
                objCloth.mapClothFabricDetails.put("Leftside Left Cross Border", new String[]{"","90","Rectangular (Default)","1","0","0"});            
            else
                objCloth.mapClothFabricDetails.put("Leftside Left Cross Border", new String[]{"","0","Rectangular (Default)","0","0","0"});
        }
        
        if(leftPalluEnabled && leftPallu.getUserData()!=null && (leftPallu.getUserData() instanceof Fabric)){
            objCloth.mapClothFabric.put("Left Pallu", (Fabric)leftPallu.getUserData());
            //MAINFABRICID, ROTATIONANGLE, REPEATMODE, FABRICREPEAT, ENDS, PICKS
            String[] fabricDetails = new String[]{"","0","Rectangular (Default)","0","0","0"};
            if(leftPallu.getId()!=null && leftPallu.getId()!="")
                fabricDetails[0]=leftPallu.getId();
            fabricDetails[1]=Integer.toString(rotations[3]);
            fabricDetails[2]=repeatMode[3];
            fabricDetails[3]=Integer.toString(repeats[3]);
            fabricDetails[4]=Integer.toString(ends[3]);
            fabricDetails[5]=Integer.toString(picks[3]);
            objCloth.mapClothFabricDetails.put("Left Pallu", fabricDetails);
        } else {
            objCloth.mapClothFabric.put("Left Pallu", null);
            if(leftPalluEnabled)
                objCloth.mapClothFabricDetails.put("Left Pallu", new String[]{"","90","Rectangular (Default)","1","0","0"});            
            else
                objCloth.mapClothFabricDetails.put("Left Pallu", new String[]{"","0","Rectangular (Default)","0","0","0"});
        }
        
        if(leftsideRightCrossBorderEnabled && leftsideRightCrossBorder.getUserData()!=null && (leftsideRightCrossBorder.getUserData() instanceof Fabric)){
            objCloth.mapClothFabric.put("Leftside Right Cross Border", (Fabric)leftsideRightCrossBorder.getUserData());
            //MAINFABRICID, ROTATIONANGLE, REPEATMODE, FABRICREPEAT, ENDS, PICKS
            String[] fabricDetails = new String[]{"","0","Rectangular (Default)","0","0","0"};
            if(leftsideRightCrossBorder.getId()!=null && leftsideRightCrossBorder.getId()!="")
                fabricDetails[0]=leftsideRightCrossBorder.getId();
            fabricDetails[1]=Integer.toString(rotations[4]);
            fabricDetails[2]=repeatMode[4];
            fabricDetails[3]=Integer.toString(repeats[4]);
            fabricDetails[4]=Integer.toString(ends[4]);
            fabricDetails[5]=Integer.toString(picks[4]);
            objCloth.mapClothFabricDetails.put("Leftside Right Cross Border", fabricDetails);
        } else {
            objCloth.mapClothFabric.put("Leftside Right Cross Border", null);
            if(leftsideRightCrossBorderEnabled)
                objCloth.mapClothFabricDetails.put("Leftside Right Cross Border", new String[]{"","90","Rectangular (Default)","1","0","0"});            
            else
                objCloth.mapClothFabricDetails.put("Leftside Right Cross Border", new String[]{"","0","Rectangular (Default)","0","0","0"});
        }
        
        if(skirtEnabled && skirt.getUserData()!=null && (skirt.getUserData() instanceof Fabric)){
            objCloth.mapClothFabric.put("Skirt", (Fabric)skirt.getUserData());
            //MAINFABRICID, ROTATIONANGLE, REPEATMODE, FABRICREPEAT, ENDS, PICKS
            String[] fabricDetails = new String[]{"","0","Rectangular (Default)","0","0","0"};
            if(skirt.getId()!=null && skirt.getId()!="")
                fabricDetails[0]=skirt.getId();
            fabricDetails[1]=Integer.toString(rotations[6]);
            fabricDetails[2]=repeatMode[6];
            fabricDetails[3]=Integer.toString(repeats[6]);
            fabricDetails[4]=Integer.toString(ends[6]);
            fabricDetails[5]=Integer.toString(picks[6]);
            objCloth.mapClothFabricDetails.put("Skirt", fabricDetails);
        } else {
            objCloth.mapClothFabric.put("Skirt", null);
            if(skirtEnabled)
                objCloth.mapClothFabricDetails.put("Skirt", new String[]{"","90","Rectangular (Default)","1","0","0"});            
            else
                objCloth.mapClothFabricDetails.put("Skirt", new String[]{"","0","Rectangular (Default)","0","0","0"});
        }
        
        if(rightsideLeftCrossBorderEnabled && rightsideLeftCrossBorder.getUserData()!=null && (rightsideLeftCrossBorder.getUserData() instanceof Fabric)){
            objCloth.mapClothFabric.put("Rightside Left Cross Border", (Fabric)rightsideLeftCrossBorder.getUserData());
            //MAINFABRICID, ROTATIONANGLE, REPEATMODE, FABRICREPEAT, ENDS, PICKS
            String[] fabricDetails = new String[]{"","0","Rectangular (Default)","0","0","0"};
            if(rightsideLeftCrossBorder.getId()!=null && rightsideLeftCrossBorder.getId()!="")
                fabricDetails[0]=rightsideLeftCrossBorder.getId();
            fabricDetails[1]=Integer.toString(rotations[7]);
            fabricDetails[2]=repeatMode[7];
            fabricDetails[3]=Integer.toString(repeats[7]);
            fabricDetails[4]=Integer.toString(ends[7]);
            fabricDetails[5]=Integer.toString(picks[7]);
            objCloth.mapClothFabricDetails.put("Rightside Left Cross Border", fabricDetails);
        } else {
            objCloth.mapClothFabric.put("Rightside Left Cross Border", null);
            if(rightsideLeftCrossBorderEnabled)
                objCloth.mapClothFabricDetails.put("Rightside Left Cross Border", new String[]{"","90","Rectangular (Default)","1","0","0"});            
            else
                objCloth.mapClothFabricDetails.put("Rightside Left Cross Border", new String[]{"","0","Rectangular (Default)","0","0","0"});
        }
        
        if(rightPalluEnabled && rightPallu.getUserData()!=null && (rightPallu.getUserData() instanceof Fabric)){
            objCloth.mapClothFabric.put("Right Pallu", (Fabric)rightPallu.getUserData());
            //MAINFABRICID, ROTATIONANGLE, REPEATMODE, FABRICREPEAT, ENDS, PICKS
            String[] fabricDetails = new String[]{"","0","Rectangular (Default)","0","0","0"};
            if(rightPallu.getId()!=null && rightPallu.getId()!="")
                fabricDetails[0]=rightPallu.getId();
            fabricDetails[1]=Integer.toString(rotations[8]);
            fabricDetails[2]=repeatMode[8];
            fabricDetails[3]=Integer.toString(repeats[8]);
            fabricDetails[4]=Integer.toString(ends[8]);
            fabricDetails[5]=Integer.toString(picks[8]);
            objCloth.mapClothFabricDetails.put("Right Pallu", fabricDetails);
        } else {
            objCloth.mapClothFabric.put("Right Pallu", null);
            if(rightPalluEnabled)
                objCloth.mapClothFabricDetails.put("Right Pallu", new String[]{"","90","Rectangular (Default)","1","0","0"});            
            else
                objCloth.mapClothFabricDetails.put("Right Pallu", new String[]{"","0","Rectangular (Default)","0","0","0"});
        }
        
        if(rightsideRightCrossBorderEnabled && rightsideRightCrossBorder.getUserData()!=null && (rightsideRightCrossBorder.getUserData() instanceof Fabric)){
            objCloth.mapClothFabric.put("Rightside Right Cross Border", (Fabric)rightsideRightCrossBorder.getUserData());
            //MAINFABRICID, ROTATIONANGLE, REPEATMODE, FABRICREPEAT, ENDS, PICKS
            String[] fabricDetails = new String[]{"","0","Rectangular (Default)","0","0","0"};
            if(rightsideRightCrossBorder.getId()!=null && rightsideRightCrossBorder.getId()!="")
                fabricDetails[0]=rightsideRightCrossBorder.getId();
            fabricDetails[1]=Integer.toString(rotations[9]);
            fabricDetails[2]=repeatMode[9];
            fabricDetails[3]=Integer.toString(repeats[9]);
            fabricDetails[4]=Integer.toString(ends[9]);
            fabricDetails[5]=Integer.toString(picks[9]);
            objCloth.mapClothFabricDetails.put("Rightside Right Cross Border", fabricDetails);
        } else {
            objCloth.mapClothFabric.put("Rightside Right Cross Border", null);
            if(rightsideRightCrossBorderEnabled)
                objCloth.mapClothFabricDetails.put("Rightside Right Cross Border", new String[]{"","90","Rectangular (Default)","1","0","0"});            
            else
                objCloth.mapClothFabricDetails.put("Rightside Right Cross Border", new String[]{"","0","Rectangular (Default)","0","0","0"});
        }
        
        if(rightBorderEnabled && rightBorder.getUserData()!=null && (rightBorder.getUserData() instanceof Fabric)){
            objCloth.mapClothFabric.put("Right Border", (Fabric)rightBorder.getUserData());
            //MAINFABRICID, ROTATIONANGLE, REPEATMODE, FABRICREPEAT, ENDS, PICKS
            String[] fabricDetails = new String[]{"","0","Rectangular (Default)","0","0","0"};
            if(rightBorder.getId()!=null && rightBorder.getId()!="")
                fabricDetails[0]=rightBorder.getId();
            fabricDetails[1]=Integer.toString(rotations[10]);
            fabricDetails[2]=repeatMode[10];
            fabricDetails[3]=Integer.toString(repeats[10]);
            fabricDetails[4]=Integer.toString(ends[10]);
            fabricDetails[5]=Integer.toString(picks[10]);
            objCloth.mapClothFabricDetails.put("Right Border", fabricDetails);
        } else {
            objCloth.mapClothFabric.put("Right Border", null);
            if(rightBorderEnabled)
                objCloth.mapClothFabricDetails.put("Right Border", new String[]{"","90","Rectangular (Default)","1","0","0"});            
            else
                objCloth.mapClothFabricDetails.put("Right Border", new String[]{"","0","Rectangular (Default)","0","0","0"});
        }
        
        //body always enable
        if(body.getUserData()!=null && (body.getUserData() instanceof Fabric)){
            objCloth.mapClothFabric.put("Body", (Fabric)body.getUserData());
            //MAINFABRICID, ROTATIONANGLE, REPEATMODE, FABRICREPEAT, ENDS, PICKS
            String[] fabricDetails = new String[]{"","0","Rectangular (Default)","0","0","0"};
            if(body.getId()!=null && body.getId()!="")
                fabricDetails[0]=body.getId();
            fabricDetails[1]=Integer.toString(rotations[5]);
            fabricDetails[2]=repeatMode[5];
            fabricDetails[3]=Integer.toString(repeats[5]);
            fabricDetails[4]=Integer.toString(ends[5]);
            fabricDetails[5]=Integer.toString(picks[5]);
            objCloth.mapClothFabricDetails.put("Body", fabricDetails);
        }else{
            objCloth.mapClothFabric.put("Body", null);
            objCloth.mapClothFabricDetails.put("Body", new String[]{"","90","Rectangular (Default)","1","0","0"});
        }       
        objConfiguration.setObjRecentCloth(objCloth);
        objConfiguration.setStrRecentCloth(null);
        objCloth.setClothZoomFactor(zoomFactor);
        objCloth.setClothInchFactor(inchFactor);
    }
    /**
    * cloneCloth
    * <p>
    * Function use for saving cloth as clone.
    * 
    * @exception (@throws )
    * @value
    * @author Amit Kumar Singh
    * @version     %I%, %G%
    * @since 1.0
    */    
    private void cloneCloth(Cloth objCCloth){
        long cloneTime = 0L;
        long start = System.currentTimeMillis();
        try {
            if(objCloth.getStrClothId()!=null)
                objCCloth.setStrClothId(objCloth.getStrClothId());
            objCCloth.setStrClothName(objCloth.getStrClothName());
            objCCloth.setStrClothType(objCloth.getStrClothType());
            objCCloth.setStrClothDescription(objCloth.getStrClothDescription());
            objCCloth.setStrClothRegion(objCloth.getStrClothRegion());
            objCCloth.setStrClothAccess(objCloth.getStrClothAccess());
            objCCloth.setStrClothDate(objCloth.getStrClothDate());
            objCCloth.setClothInchFactor(objCloth.getClothInchFactor());
            objCCloth.setClothZoomFactor(objCloth.getClothZoomFactor());
            
            if(objCloth.getBytClothIcon()==null)
                getClothIcon();
            //objCCloth.setBytClothIcon(objCloth.getBytClothIcon());
            if(objCloth.getBytClothIcon().length>0){
                byte[] bytClothIcon = new byte[objCloth.getBytClothIcon().length];
                for(int i=0; i<objCloth.getBytClothIcon().length; i++)
                    bytClothIcon[i] = objCloth.getBytClothIcon()[i];
                objCCloth.setBytClothIcon(bytClothIcon);
            }
            
            //objCCloth.mapClothFabric(objCloth.mapClothFabric);
            for(String key: objCloth.mapClothFabric.keySet()){
                Fabric objFabricCopy = new Fabric();
                if(objFabricCopy!=null && objCloth.mapClothFabric.get(key)!=null)
                    cloneFabric(objCloth.mapClothFabric.get(key),objFabricCopy);
                objFabricCopy.setStrFabricName("Clon Fabric");
                objCCloth.mapClothFabric.put(key,objFabricCopy);
            }
            
            //objCCloth.mapClothFabricDetails(objCloth.mapClothFabricDetails);
            for(String key: objCloth.mapClothFabricDetails.keySet()){
                int index = getComponentIndex(key);
                String[] clothFabric = new String[objCloth.mapClothFabricDetails.get(key).length];
                for(int i=0; i<objCloth.mapClothFabricDetails.get(key).length; i++)
                    clothFabric[i] = objCloth.mapClothFabricDetails.get(key)[i];
                /*
                for(String str:objCloth.mapClothFabricDetails.get(key)){
                    //MAINFABRICID, ROTATIONANGLE, REPEATMODE, FABRICREPEAT, ENDS, PICKS
                    System.out.println(str);
                }     
                */
                objCCloth.mapClothFabricDetails.put(key,clothFabric);
            }
            
            objCCloth.setObjConfiguration(objConfiguration);
            
        } catch (Exception ex) {
            new Logging("SEVERE",ClothView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
        cloneTime += (System.currentTimeMillis() - start);
        new Logging("INFO",ClothView.class.getName(),"Fabric cloning Time"+cloneTime,null);
    }
/**
 * loadCloth
 * <p>
 * Function use for loading cloth data into editor.
 * 
 * @exception (@throws )
 * @value
 * @author Amit Kumar Singh
 * @version     %I%, %G%
 * @since 1.0
 */    
    private void loadCloth(Cloth objCloth){
        disableAllComponent();
        inchFactor=objCloth.getClothInchFactor();
        zoomFactor=objCloth.getClothZoomFactor();

        for(String key: objCloth.mapClothFabricDetails.keySet()){
            int index = getComponentIndex(key);
            /*for(String str:objCloth.mapClothFabricDetails.get(key)){
                //MAINFABRICID, ROTATIONANGLE, REPEATMODE, FABRICREPEAT, ENDS, PICKS
                System.out.println(str);
            }*/     
            //if(objCloth.mapClothFabric.get(key)!=null){
                String[] clothFabric = objCloth.mapClothFabricDetails.get(key);
                rotations[index]=Integer.parseInt(clothFabric[1]);
                repeatMode[index]=clothFabric[2];
                repeats[index]=Integer.parseInt(clothFabric[3]);
                ends[index]=Integer.parseInt(clothFabric[4]);
                picks[index]=Integer.parseInt(clothFabric[5]);
            //}
            //System.err.println(key+":"+index+":"+rotations[index]+":"+repeatMode[index]+":"+repeats[index]+":"+ends[index]+":"+picks[index]);
        }
                
        if(repeats[0]>0)
            leftBorderEnabled=true;
        if(repeats[1]>0)
            blouseEnabled=true;
        if(repeats[2]>0)
            leftsideLeftCrossBorderEnabled=true;
        if(repeats[3]>0)
            leftPalluEnabled=true;
        if(repeats[4]>0)
            leftsideRightCrossBorderEnabled=true;
        if(repeats[6]>0)
            skirtEnabled=true;
        if(repeats[7]>0)
            rightsideLeftCrossBorderEnabled=true;
        if(repeats[8]>0)
            rightPalluEnabled=true;
        if(repeats[9]>0)
            rightsideRightCrossBorderEnabled=true;
        if(repeats[10]>0)
            rightBorderEnabled=true;
        
        // setting heights and widths
        setWidthHeight();
        //set component sizes
        setComponentWidthHeight();
        //prepare components
        plotContainerGP();       
        
        plotCloth();
    }
    
    private void plotCloth(){
        // component was enabled
        if(leftBorderEnabled && objCloth.mapClothFabric.get("Left Border")!=null){
            leftBorder.setUserData(objCloth.mapClothFabric.get("Left Border"));
            leftBorder.setId(objCloth.mapClothFabricDetails.get("Left Border")[0]);
            resizeFabric(leftBorder);
        }
        if(blouseEnabled && objCloth.mapClothFabric.get("Blouse")!=null){
            blouse.setUserData(objCloth.mapClothFabric.get("Blouse"));
            blouse.setId(objCloth.mapClothFabricDetails.get("Blouse")[0]);
            resizeFabric(blouse);
        }
        if(leftsideLeftCrossBorderEnabled && objCloth.mapClothFabric.get("Leftside Left Cross Border")!=null){
            leftsideLeftCrossBorder.setUserData(objCloth.mapClothFabric.get("Leftside Left Cross Border"));
            leftsideLeftCrossBorder.setId(objCloth.mapClothFabricDetails.get("Leftside Left Cross Border")[0]);
            resizeFabric(leftsideLeftCrossBorder);
        }
        if(leftPalluEnabled && objCloth.mapClothFabric.get("Left Pallu")!=null){
            leftPallu.setUserData(objCloth.mapClothFabric.get("Left Pallu"));
            leftPallu.setId(objCloth.mapClothFabricDetails.get("Left Pallu")[0]);
            resizeFabric(leftPallu);
        }
        if(leftsideRightCrossBorderEnabled && objCloth.mapClothFabric.get("Leftside Right Cross Border")!=null){
            leftsideRightCrossBorder.setUserData(objCloth.mapClothFabric.get("Leftside Right Cross Border"));
            leftsideRightCrossBorder.setId(objCloth.mapClothFabricDetails.get("Leftside Right Cross Border")[0]);
            resizeFabric(leftsideRightCrossBorder);
        }
        if(skirtEnabled && objCloth.mapClothFabric.get("Skirt")!=null){
            skirt.setUserData(objCloth.mapClothFabric.get("Skirt"));
            skirt.setId(objCloth.mapClothFabricDetails.get("Skirt")[0]);
            resizeFabric(skirt);
        }
        if(rightsideLeftCrossBorderEnabled && objCloth.mapClothFabric.get("Rightside Left Cross Border")!=null){
            rightsideLeftCrossBorder.setUserData(objCloth.mapClothFabric.get("Rightside Left Cross Border"));
            rightsideLeftCrossBorder.setId(objCloth.mapClothFabricDetails.get("Rightside Left Cross Border")[0]);
            resizeFabric(rightsideLeftCrossBorder);
        }
        if(rightPalluEnabled && objCloth.mapClothFabric.get("Right Pallu")!=null){
            rightPallu.setUserData(objCloth.mapClothFabric.get("Right Pallu"));
            rightPallu.setId(objCloth.mapClothFabricDetails.get("Right Pallu")[0]);
            resizeFabric(rightPallu);
        }
        if(rightsideRightCrossBorderEnabled && objCloth.mapClothFabric.get("Rightside Right Cross Border")!=null){
            rightsideRightCrossBorder.setUserData(objCloth.mapClothFabric.get("Rightside Right Cross Border"));
            rightsideRightCrossBorder.setId(objCloth.mapClothFabricDetails.get("Rightside Right Cross Border")[0]);
            resizeFabric(rightsideRightCrossBorder);
        }
        if(rightBorderEnabled && objCloth.mapClothFabric.get("Right Border")!=null){
            rightBorder.setUserData(objCloth.mapClothFabric.get("Right Border"));
            rightBorder.setId(objCloth.mapClothFabricDetails.get("Right Border")[0]);
            resizeFabric(rightBorder);
        }

        //body always enable
        if(objCloth.mapClothFabric.get("Body")!=null){
            body.setUserData(objCloth.mapClothFabric.get("Body"));
            body.setId(objCloth.mapClothFabricDetails.get("Body")[0]);
            resizeFabric(body);
        }
    }
    
    private void resizeFabric(ImageView iView){
        int index = getComponentIndex(iView);
        Fabric objFabric = (Fabric)iView.getUserData();
        if(objFabric!=null){
            try {
                objFabric.setDblArtworkLength(Double.parseDouble(String.format("%.3f",objFabric.getDblArtworkLength())));
                objFabric.setDblArtworkWidth(Double.parseDouble(String.format("%.3f",objFabric.getDblArtworkWidth())));
                
                //create one repeat image with warp and weft of fabric
                BufferedImage bufferedImage = new BufferedImage(objFabric.getIntWeft(), objFabric.getIntWarp(), BufferedImage.TYPE_INT_RGB);
                objFabricAction = new FabricAction();
                if(plotViewActionMode==2)
                    bufferedImage = objFabricAction.plotRearSideView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), objFabric.getIntWarp(), objFabric.getIntWeft());
                    //BufferedImage bufferedImage = plotCompositeView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                else 
                    bufferedImage = objFabricAction.plotFrontSideView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), objFabric.getIntWarp(), objFabric.getIntWeft());
                    //BufferedImage bufferedImage = plotCompositeView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft());
                
                objArtworkAction = new ArtworkAction();
                //resize image with resized ends and picks
                ends[index]=(int)(objFabric.getDblArtworkLength()*objFabric.getIntEPI());
                picks[index]=(int)(objFabric.getDblArtworkWidth()*objFabric.getIntPPI());
                bufferedImage = objArtworkAction.resizeDesign(bufferedImage, ends[index], picks[index]);
                
                //rotate image by angle
                for(int a=0; a<(rotations[index]/90); a++){
                    bufferedImage=objArtworkAction.getImageRotation(bufferedImage, "CLOCKWISE");
                }
                //resize image with resized ends and picks
                //ends[index]=bufferedImage.getWidth();
                //picks[index]=bufferedImage.getHeight();                
                //bufferedImage = objArtworkAction.resizeDesign(bufferedImage, ends[index], picks[index]);
                //resize image based on inch-pixel factor and zoom size
                //objFabric.setDblArtworkLength(((double) bufferedImage.getHeight())/objFabric.getIntEPI());//objConfiguration.getDblArtworkLength());
                //objFabric.setDblArtworkWidth(((double) bufferedImage.getWidth())/objFabric.getIntPPI());//objConfiguration.getDblArtworkWidth());
                widths[index]=(int)((bufferedImage.getHeight()*inchFactor*zoomFactor)/objFabric.getIntEPI());
                heights[index]=(int)((bufferedImage.getWidth()*inchFactor*zoomFactor)/objFabric.getIntPPI());
                bufferedImage = objArtworkAction.resizeDesign(bufferedImage, heights[index], widths[index]);
                if(iView.equals(leftBorder) || iView.equals(rightBorder) || iView.equals(skirt)){
                    widths[index]=(int)(objFabric.getDblArtworkLength()*inchFactor*zoomFactor);
                    heights[index]=(int)(objFabric.getDblArtworkWidth()*repeats[index]*inchFactor*zoomFactor);
                }else{
                    widths[index]=(int)(objFabric.getDblArtworkLength()*repeats[index]*inchFactor*zoomFactor);
                    heights[index]=(int)(objFabric.getDblArtworkWidth()*inchFactor*zoomFactor);
                }
                
                setWidthHeight();
                //repeat image based on repeat space and orientation
                int vAling = 1;
                int hAling = 1;
                if(repeatMode[index].contains("Vertical")){
                    String temp = repeatMode[index];
                    vAling = Integer.parseInt(temp.substring(0, temp.indexOf("/")).trim());
                    hAling = Integer.parseInt(temp.substring(2, temp.indexOf("Vertical")).trim());
                } else if(repeatMode[index].contains("Horizontal")){
                    String temp = repeatMode[index];
                    hAling = Integer.parseInt(temp.substring(0, temp.indexOf("/")).trim());
                    vAling = Integer.parseInt(temp.substring(2, temp.indexOf("Horizontal")).trim());
                } else{
                    vAling = 1;
                    hAling = 1;
                }
                
                //File file = new File(System.getProperty("user.dir")+"\\mla\\Untitled"+index+".png");
                //ImageIO.write(bufferedImage, "png", file);
                
                if(iView.equals(leftBorder) || iView.equals(rightBorder) || iView.equals(skirt)){
                    bufferedImage=objArtworkAction.designRepeat(bufferedImage, vAling, hAling, repeats[index], widths[index]/bufferedImage.getHeight());
                } else if(iView.equals(body)){
                    bufferedImage=objArtworkAction.designRepeat(bufferedImage, vAling, hAling, heights[index]/bufferedImage.getWidth(), widths[index]/bufferedImage.getHeight());
                } else{
                    bufferedImage=objArtworkAction.designRepeat(bufferedImage, vAling, hAling, heights[index]/bufferedImage.getWidth(), repeats[index]);
                }
                iView.setImage(SwingFXUtils.toFXImage(bufferedImage, null));                
            } catch (SQLException ex) {
                new Logging("SEVERE",ClothView.class.getName(),"getFabricIcon(): "+ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            } catch (Exception ex) {
                new Logging("SEVERE",ClothView.class.getName(),"getFabricIcon(): "+ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        }
    }
    
/**
 * newFabric
 * <p>
 * Function use for drawing tool bar for menu item File,
 * and binding events for each tools.
 * 
 * @param  
 * @return
 * @exception (@throws )
 * @value
 * @author Amit Kumar Singh
 * @version     %I%, %G%
 * @since 1.0
 * @see java.awt
 * @link
 * @serial (or @serialField or @serialData)
 * @serialField
 * @serialData  
 * //@deprecated (see How and When To Deprecate APIs)
 */    
    private void newFabric(Fabric objFabric, String strTableType){
        try {
            objFabricAction = new FabricAction();
            objFabric.setStrFabricName("My Fabric");
            //objFabric.setStrFabricID(new IDGenerator().getIDGenerator("FABRIC_LIBRARY", objConfiguration.getObjUser().getStrUserID()));
            objFabric.setStrFabricType(objConfiguration.getStrFabricType());
            objFabric.setStrColourType(objConfiguration.getStrColourType());
            objFabric.setStrClothType(objConfiguration.getStrClothType());
            objFabric.setDblFabricLength(objConfiguration.getDblFabricLength());
            objFabric.setDblFabricWidth(objConfiguration.getDblFabricWidth());
            objFabric.setDblArtworkLength(((double) objConfiguration.getIntPixs())/objConfiguration.getIntPPI());//objConfiguration.getDblArtworkLength());
            objFabric.setDblArtworkWidth(((double) objConfiguration.getIntEnds())/objConfiguration.getIntEPI());//objConfiguration.getDblArtworkWidth());
            objFabric.setIntWarp(objConfiguration.getIntEnds());
            objFabric.setIntWeft(objConfiguration.getIntPixs());
            objFabric.setIntHooks(objConfiguration.getIntHooks());
            objFabric.setIntShaft(0);
            objFabric.setIntExtraWarp(objConfiguration.getIntExtraWarp());
            objFabric.setIntExtraWeft(objConfiguration.getIntExtraWeft());
            objFabric.setIntReedCount(objConfiguration.getIntReedCount());
            objFabric.setIntDents(objConfiguration.getIntDents());
            objFabric.setIntTPD(objConfiguration.getIntTPD());
            objFabric.setIntEPI(objConfiguration.getIntEPI());
            objFabric.setIntHPI(objConfiguration.getIntHPI());
            objFabric.setIntPPI(objConfiguration.getIntPPI());
            objFabric.setIntProtection(objConfiguration.getIntProtection());
            objFabric.setIntBinding(objConfiguration.getIntBinding());
            objFabric.setBlnArtworkAssingmentSize(objConfiguration.getBlnArtworkAssingmentSize());
            objFabric.setBlnArtworkOutline(objConfiguration.getBlnArtworkOutline());
            objFabric.setBlnArtworkAspectRatio(objConfiguration.getBlnArtworkAspectRatio());
            //intVRepeat = (int)(objConfiguration.getDblFabricLength()/objConfiguration.getDblArtworkLength());
            //intHRepeat = (int)(objConfiguration.getDblFabricWidth()/objConfiguration.getDblArtworkWidth());
            objFabric.setStrArtworkID(null);
            Weave objWeave = new Weave();
            objWeave.setObjConfiguration(objFabric.getObjConfiguration());
            objWeave.setStrWeaveID(objFabric.getStrBaseWeaveID());
            WeaveAction objWeaveAction = new WeaveAction(); 
            objWeaveAction.getWeave(objWeave);
            objWeaveAction = new WeaveAction(objWeave,true); 
            objWeaveAction.extractWeaveContent(objWeave);
            objFabric.setBaseWeaveMatrix(objWeave.getDesignMatrix());
            objArtworkAction= new ArtworkAction();            
            byte[][] fabricMatrix = new byte[objFabric.getIntWeft()][objFabric.getIntWarp()];
            fabricMatrix = objArtworkAction.repeatMatrix(objWeave.getDesignMatrix(),objFabric.getIntWeft(),objFabric.getIntWarp());
            byte[][] reverseMatrix = new byte[objFabric.getIntWeft()][objFabric.getIntWarp()];
            for(int i = 0; i < objFabric.getIntWeft(); i++) {
                for(int j = 0; j < objFabric.getIntWarp(); j++) {
                    if(fabricMatrix[i][j]==0){
                         reverseMatrix[i][j] = 1;  
                    }else{
                         reverseMatrix[i][j] = 0;
                    }                
                }
            } 
            objFabric.setFabricMatrix(fabricMatrix);
            objFabric.setReverseMatrix(reverseMatrix);
            reverseMatrix=null;
            fabricMatrix = null;            
            objFabric.setColourPalette(objConfiguration.getColourPalette());
            Yarn objYarn = null;
            objYarn = new Yarn(null, "Warp", objConfiguration.getStrWarpName(), "#"+objConfiguration.getStrWarpColor(), objConfiguration.getIntWarpRepeat(), "A", objConfiguration.getIntWarpCount(), objConfiguration.getStrWarpUnit(), objConfiguration.getIntWarpPly(), objConfiguration.getIntWarpFactor(), objConfiguration.getDblWarpDiameter(), objConfiguration.getIntWarpTwist(), objConfiguration.getStrWarpSence(), objConfiguration.getIntWarpHairness(), objConfiguration.getIntWarpDistribution(), objFabric.getObjConfiguration().getDblWarpPrice(), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);
            objYarn.setObjConfiguration(objConfiguration);
            Yarn[] warpYarn = new Yarn[]{objYarn};
            objYarn = new Yarn(null, "Weft", objConfiguration.getStrWeftName(), "#"+objConfiguration.getStrWeftColor(), objConfiguration.getIntWeftRepeat(), "a", objConfiguration.getIntWeftCount(), objConfiguration.getStrWeftUnit(), objConfiguration.getIntWeftPly(), objConfiguration.getIntWeftFactor(), objConfiguration.getDblWeftDiameter(), objConfiguration.getIntWeftTwist(), objConfiguration.getStrWeftSence(), objConfiguration.getIntWeftHairness(), objConfiguration.getIntWeftDistribution(), objFabric.getObjConfiguration().getDblWeftPrice(), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);
            objYarn.setObjConfiguration(objConfiguration);
            Yarn[] weftYarn = new Yarn[]{objYarn};
            Yarn[] weftExtraYarn = null;
            Yarn[] warpExtraYarn = null;
            objFabric.setWarpYarn(warpYarn);
            objFabric.setWeftYarn(weftYarn);
            objFabric.setWarpExtraYarn(warpExtraYarn);
            objFabric.setWeftExtraYarn(weftExtraYarn);
            objPatternAction = new PatternAction();
            objFabric.setStrWarpPatternID(objPatternAction.getPatternAvibilityID(objConfiguration.getStrWarpPattern(),(byte)1));
            objPatternAction = new PatternAction();
            objFabric.setStrWeftPatternID(objPatternAction.getPatternAvibilityID(objConfiguration.getStrWeftPattern(),(byte)2));
            objFabricAction = new FabricAction();
            objFabricAction.fabricImage(objFabric, objFabric.getIntWeft(), objFabric.getIntWarp());
            objFabric.setObjConfiguration(objConfiguration);
            objFabric.getObjConfiguration().setStrGraphSize(12+"x"+(int)((12*objFabric.getIntPPI())/objFabric.getIntEPI()));
        } catch (SQLException ex) {
            new Logging("SEVERE",ClothView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",ClothView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    /**
    * cloneFabric
    * <p>
    * Function use for saving fabric as clone.
    * 
    * @exception (@throws )
    * @value
    * @author Amit Kumar Singh
    * @version     %I%, %G%
    * @since 1.0
    */    
    private void cloneFabric(Fabric objFabric, Fabric objCFabric){
        long cloneTime = 0L;
        long start = System.currentTimeMillis();
        try {
            if(objFabric.getStrFabricID()!=null)
                objCFabric.setStrFabricID(objFabric.getStrFabricID());
            objCFabric.setStrFabricName(objFabric.getStrFabricName());
            objCFabric.setStrFabricType(objFabric.getStrFabricType());
            objCFabric.setStrClothType(objFabric.getStrClothType());
            objCFabric.setStrColourType(objFabric.getStrColourType());
            objCFabric.setDblFabricLength(objFabric.getDblFabricLength());
            objCFabric.setDblFabricWidth(objFabric.getDblFabricWidth());
            objCFabric.setDblArtworkLength(objFabric.getDblArtworkLength());
            objCFabric.setDblArtworkWidth(objFabric.getDblArtworkWidth());
            objCFabric.setStrArtworkID(objFabric.getStrArtworkID());
            objCFabric.setStrBaseWeaveID(objFabric.getStrBaseWeaveID());
            objCFabric.setIntWeft(objFabric.getIntWeft());
            objCFabric.setIntWarp(objFabric.getIntWarp());
            objCFabric.setIntExtraWeft(objFabric.getIntExtraWeft());
            objCFabric.setIntExtraWarp(objFabric.getIntExtraWarp());
            objCFabric.setIntPPI(objFabric.getIntPPI());
            objCFabric.setIntEPI(objFabric.getIntEPI());
            objCFabric.setIntHooks(objFabric.getIntHooks());
            objCFabric.setIntHPI(objFabric.getIntHPI());
            objCFabric.setIntReedCount(objFabric.getIntReedCount());
            objCFabric.setIntDents(objFabric.getIntDents());
            objCFabric.setIntTPD(objFabric.getIntTPD());
            objCFabric.setIntShaft(objFabric.getIntShaft());
            objCFabric.setIntProtection(objFabric.getIntProtection());
            objCFabric.setIntBinding(objFabric.getIntBinding());
            objCFabric.setBlnArtworkAssingmentSize(objFabric.getBlnArtworkAssingmentSize());
            objCFabric.setBlnArtworkOutline(objFabric.getBlnArtworkOutline());
            objCFabric.setBlnArtworkAspectRatio(objFabric.getBlnArtworkAspectRatio());
            objCFabric.setStrWarpPatternID(objFabric.getStrWarpPatternID());
            objCFabric.setStrWeftPatternID(objFabric.getStrWeftPatternID());
            objCFabric.setStrFabricFile(objFabric.getStrFabricFile());
            objCFabric.setStrFabricRData(objFabric.getStrFabricRData());
            objCFabric.setStrFabricDate(objFabric.getStrFabricDate());
            objCFabric.setColorArtwork(objFabric.getColorArtwork());
            objCFabric.setColorCountArtwork(objFabric.getColorCountArtwork());
            objCFabric.setStrFabricAccess(objFabric.getStrFabricAccess());
            
            byte[] bytFabricIcon = new byte[objFabric.getBytFabricIcon().length];
            for(int i=0; i<objFabric.getBytFabricIcon().length; i++)
                bytFabricIcon[i] = objFabric.getBytFabricIcon()[i];
            objCFabric.setBytFabricIcon(bytFabricIcon);
            
            byte[][] baseWeaveMatrix = new byte[objFabric.getBaseWeaveMatrix().length][objFabric.getBaseWeaveMatrix()[0].length];
            for(int i=0; i<objFabric.getBaseWeaveMatrix().length; i++)
                for(int j=0; j<objFabric.getBaseWeaveMatrix()[0].length; j++)
                    baseWeaveMatrix[i][j] = objFabric.getBaseWeaveMatrix()[i][j];
            objCFabric.setBaseWeaveMatrix(baseWeaveMatrix);
            
            if(objFabric.getArtworkMatrix()!=null){
                byte[][] artworkMatrix = new byte[objFabric.getArtworkMatrix().length][objFabric.getArtworkMatrix()[0].length];
                for(int i=0; i<objFabric.getArtworkMatrix().length; i++)
                    for(int j=0; j<objFabric.getArtworkMatrix()[0].length; j++)
                        artworkMatrix[i][j] = objFabric.getArtworkMatrix()[i][j];
                objCFabric.setArtworkMatrix(artworkMatrix);
            }
            byte[][] reverseMatrix = new byte[objFabric.getReverseMatrix().length][objFabric.getReverseMatrix()[0].length];
            for(int i=0; i<objFabric.getReverseMatrix().length; i++)
                for(int j=0; j<objFabric.getReverseMatrix()[0].length; j++)
                    reverseMatrix[i][j] = objFabric.getReverseMatrix()[i][j];
            objCFabric.setReverseMatrix(reverseMatrix);
            
            byte[][] fabricMatrix = new byte[objFabric.getFabricMatrix().length][objFabric.getFabricMatrix()[0].length];
            for(int i=0; i<objFabric.getFabricMatrix().length; i++)
                for(int j=0; j<objFabric.getFabricMatrix()[0].length; j++)
                    fabricMatrix[i][j] = objFabric.getFabricMatrix()[i][j];
            objCFabric.setFabricMatrix(fabricMatrix);

            Yarn[] warpYarn = new Yarn[objFabric.getWarpYarn().length];
            for(int i=0; i<objFabric.getWarpYarn().length; i++)
                warpYarn[i] = new Yarn(objFabric.getWarpYarn()[i].getStrYarnId(), objFabric.getWarpYarn()[i].getStrYarnType(), objFabric.getWarpYarn()[i].getStrYarnName(), objFabric.getWarpYarn()[i].getStrYarnColor(), objFabric.getWarpYarn()[i].getIntYarnRepeat(), objFabric.getWarpYarn()[i].getStrYarnSymbol(), objFabric.getWarpYarn()[i].getIntYarnCount(), objFabric.getWarpYarn()[i].getStrYarnCountUnit(), objFabric.getWarpYarn()[i].getIntYarnPly(), objFabric.getWarpYarn()[i].getIntYarnDFactor(), objFabric.getWarpYarn()[i].getDblYarnDiameter(), objFabric.getWarpYarn()[i].getIntYarnTwist(), objFabric.getWarpYarn()[i].getStrYarnTModel(), objFabric.getWarpYarn()[i].getIntYarnHairness(), objFabric.getWarpYarn()[i].getIntYarnHProbability(), objFabric.getWarpYarn()[i].getDblYarnPrice(), objFabric.getWarpYarn()[i].getStrYarnAccess(), objFabric.getWarpYarn()[i].getStrYarnUser(), objFabric.getWarpYarn()[i].getStrYarnDate());
            objCFabric.setWarpYarn(warpYarn);
            
            Yarn[] weftYarn = new Yarn[objFabric.getWeftYarn().length];
            for(int i=0; i<objFabric.getWeftYarn().length; i++)
                weftYarn[i] = new Yarn(objFabric.getWeftYarn()[i].getStrYarnId(), objFabric.getWeftYarn()[i].getStrYarnType(), objFabric.getWeftYarn()[i].getStrYarnName(), objFabric.getWeftYarn()[i].getStrYarnColor(), objFabric.getWeftYarn()[i].getIntYarnRepeat(), objFabric.getWeftYarn()[i].getStrYarnSymbol(), objFabric.getWeftYarn()[i].getIntYarnCount(), objFabric.getWeftYarn()[i].getStrYarnCountUnit(), objFabric.getWeftYarn()[i].getIntYarnPly(), objFabric.getWeftYarn()[i].getIntYarnDFactor(), objFabric.getWeftYarn()[i].getDblYarnDiameter(), objFabric.getWeftYarn()[i].getIntYarnTwist(), objFabric.getWeftYarn()[i].getStrYarnTModel(), objFabric.getWeftYarn()[i].getIntYarnHairness(), objFabric.getWeftYarn()[i].getIntYarnHProbability(), objFabric.getWeftYarn()[i].getDblYarnPrice(), objFabric.getWeftYarn()[i].getStrYarnAccess(), objFabric.getWeftYarn()[i].getStrYarnUser(), objFabric.getWeftYarn()[i].getStrYarnDate());
            objCFabric.setWeftYarn(weftYarn);
            
            if(objFabric.getWarpExtraYarn()!=null){
                Yarn[] warpExtraYarn = new Yarn[objFabric.getWarpExtraYarn().length];
                for(int i=0; i<objFabric.getWarpExtraYarn().length; i++)
                    warpExtraYarn[i] = new Yarn(objFabric.getWarpExtraYarn()[i].getStrYarnId(), objFabric.getWarpExtraYarn()[i].getStrYarnType(), objFabric.getWarpExtraYarn()[i].getStrYarnName(), objFabric.getWarpExtraYarn()[i].getStrYarnColor(), objFabric.getWarpExtraYarn()[i].getIntYarnRepeat(), objFabric.getWarpExtraYarn()[i].getStrYarnSymbol(), objFabric.getWarpExtraYarn()[i].getIntYarnCount(), objFabric.getWarpExtraYarn()[i].getStrYarnCountUnit(), objFabric.getWarpExtraYarn()[i].getIntYarnPly(), objFabric.getWarpExtraYarn()[i].getIntYarnDFactor(), objFabric.getWarpExtraYarn()[i].getDblYarnDiameter(), objFabric.getWarpExtraYarn()[i].getIntYarnTwist(), objFabric.getWarpExtraYarn()[i].getStrYarnTModel(), objFabric.getWarpExtraYarn()[i].getIntYarnHairness(), objFabric.getWarpExtraYarn()[i].getIntYarnHProbability(), objFabric.getWarpExtraYarn()[i].getDblYarnPrice(), objFabric.getWarpExtraYarn()[i].getStrYarnAccess(), objFabric.getWarpExtraYarn()[i].getStrYarnUser(), objFabric.getWarpExtraYarn()[i].getStrYarnDate());
                objCFabric.setWarpExtraYarn(warpExtraYarn);
            }
            
            if(objFabric.getWeftExtraYarn()!=null){            
                Yarn[] weftExtraYarn = new Yarn[objFabric.getWeftExtraYarn().length];
                for(int i=0; i<objFabric.getWeftExtraYarn().length; i++)
                    weftExtraYarn[i] = new Yarn(objFabric.getWeftExtraYarn()[i].getStrYarnId(), objFabric.getWeftExtraYarn()[i].getStrYarnType(), objFabric.getWeftExtraYarn()[i].getStrYarnName(), objFabric.getWeftExtraYarn()[i].getStrYarnColor(), objFabric.getWeftExtraYarn()[i].getIntYarnRepeat(), objFabric.getWeftExtraYarn()[i].getStrYarnSymbol(), objFabric.getWeftExtraYarn()[i].getIntYarnCount(), objFabric.getWeftExtraYarn()[i].getStrYarnCountUnit(), objFabric.getWeftExtraYarn()[i].getIntYarnPly(), objFabric.getWeftExtraYarn()[i].getIntYarnDFactor(), objFabric.getWeftExtraYarn()[i].getDblYarnDiameter(), objFabric.getWeftExtraYarn()[i].getIntYarnTwist(), objFabric.getWeftExtraYarn()[i].getStrYarnTModel(), objFabric.getWeftExtraYarn()[i].getIntYarnHairness(), objFabric.getWeftExtraYarn()[i].getIntYarnHProbability(), objFabric.getWeftExtraYarn()[i].getDblYarnPrice(), objFabric.getWeftExtraYarn()[i].getStrYarnAccess(), objFabric.getWeftExtraYarn()[i].getStrYarnUser(), objFabric.getWeftExtraYarn()[i].getStrYarnDate());
                objCFabric.setWeftExtraYarn(weftExtraYarn);
            }
            
            if(objFabric.getColorWeave()!=null){
                String[][] colorWeave = new String[objFabric.getColorWeave().length][objFabric.getColorWeave()[0].length];
                for(int i=0; i<objFabric.getColorWeave().length; i++)
                    for(int j=0; j<objFabric.getColorWeave()[0].length; j++)
                        colorWeave[i][j] = objFabric.getColorWeave()[i][j];
                objCFabric.setColorWeave(colorWeave);
            }
            String[] threadPaletes = new String[objFabric.getColourPalette().length];
            for(int i=0; i<objFabric.getColourPalette().length; i++)
                threadPaletes[i] = objFabric.getColourPalette()[i];
            objCFabric.setColourPalette(threadPaletes);
            
            if(objFabric.getExtraColourPalette()!=null){
                String[] threadExtraPaletes = new String[objFabric.getExtraColourPalette().length];
                for(int i=0; i<objFabric.getExtraColourPalette().length; i++)
                    threadExtraPaletes[i] = objFabric.getExtraColourPalette()[i];
                objCFabric.setExtraColourPalette(threadExtraPaletes);
            }
            if(objFabric.getLstYarn()!=null){
                ArrayList<Yarn> lstYarn = new ArrayList<Yarn>();
                for(int i=0; i<objFabric.getLstYarn().size(); i++)
                    lstYarn.set(i, new Yarn(objFabric.getLstYarn().get(i).getStrYarnId(), objFabric.getLstYarn().get(i).getStrYarnType(), objFabric.getLstYarn().get(i).getStrYarnName(), objFabric.getLstYarn().get(i).getStrYarnColor(), objFabric.getLstYarn().get(i).getIntYarnRepeat(), objFabric.getLstYarn().get(i).getStrYarnSymbol(), objFabric.getLstYarn().get(i).getIntYarnCount(), objFabric.getLstYarn().get(i).getStrYarnCountUnit(), objFabric.getLstYarn().get(i).getIntYarnPly(), objFabric.getLstYarn().get(i).getIntYarnDFactor(), objFabric.getLstYarn().get(i).getDblYarnDiameter(), objFabric.getLstYarn().get(i).getIntYarnTwist(), objFabric.getLstYarn().get(i).getStrYarnTModel(), objFabric.getLstYarn().get(i).getIntYarnHairness(), objFabric.getLstYarn().get(i).getIntYarnHProbability(), objFabric.getLstYarn().get(i).getDblYarnPrice(), objFabric.getLstYarn().get(i).getStrYarnAccess(), objFabric.getLstYarn().get(i).getStrYarnUser(), objFabric.getLstYarn().get(i).getStrYarnDate()));
                objCFabric.setLstYarn(lstYarn);
            }
            if(objFabric.getLstPattern()!=null){
                ArrayList<Pattern> lstPattern = new ArrayList<Pattern>();
                for(int i=0; i<objFabric.getLstPattern().size(); i++)
                    lstPattern.set(i, new Pattern(objFabric.getLstPattern().get(i).getStrPatternID(), objFabric.getLstPattern().get(i).getStrPattern(), objFabric.getLstPattern().get(i).getIntPatternType(), objFabric.getLstPattern().get(i).getIntPatternRepeat(), objFabric.getLstPattern().get(i).getIntPatternUsed(), objFabric.getLstPattern().get(i).getStrPatternAccess(), objFabric.getLstPattern().get(i).getStrPatternUser(), objFabric.getLstPattern().get(i).getStrPatternDate()));
                objCFabric.setLstPattern(lstPattern);
            }
            if(objFabric.getYarnBI()!=null){
                BufferedImage yarnBI = new BufferedImage(objFabric.getYarnBI().getWidth(), objFabric.getYarnBI().getHeight(), objFabric.getYarnBI().getType());
                Graphics2D g = yarnBI.createGraphics();
                g.drawImage(objFabric.getYarnBI(), 0, 0, objFabric.getYarnBI().getWidth(), objFabric.getYarnBI().getHeight(), null);
                g.dispose();
                objCFabric.setYarnBI(yarnBI);
            }
            objCFabric.setObjConfiguration(objConfiguration);
            
        } catch (Exception ex) {
            Logger.getLogger(ClothView.class.getName()).log(Level.SEVERE, null, ex);
            
            new Logging("SEVERE",ClothView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
        cloneTime += (System.currentTimeMillis() - start);
        new Logging("INFO",ClothView.class.getName(),"Cloth cloning Time"+cloneTime,null);
        //System.out.println("Cloning Time"+cloneTime);
    }
    /**
     * loadFabric
     * <p>
     * Function use for saving fabric.
     * 
     * @exception (@throws )
     * @value
     * @author Amit Kumar Singh
     * @version     %I%, %G%
     * @since 1.0
     */ 
    private void loadFabric(Fabric objFabric, String strTableType){
        try {
            objFabricAction = new FabricAction();
            objFabricAction.getFabric(objFabric,strTableType);
            //intVRepeat = (int)(objConfiguration.getDblFabricLength()/objConfiguration.getDblArtworkLength());
            //intHRepeat = (int)(objConfiguration.getDblFabricWidth()/objConfiguration.getDblArtworkWidth());
            Weave objWeave = new Weave();
            objWeave.setObjConfiguration(objFabric.getObjConfiguration());
            objWeave.setStrWeaveID(objFabric.getStrBaseWeaveID());
            WeaveAction objWeaveAction = new WeaveAction(); 
            objWeaveAction.getWeave(objWeave);
            objWeaveAction = new WeaveAction(); 
            objWeaveAction.extractWeaveContent(objWeave);
            objFabric.setBaseWeaveMatrix(objWeave.getDesignMatrix());
            
            if(objFabric.getStrArtworkID()!=null){                
                Artwork objArtwork = new Artwork();
                objArtwork.setObjConfiguration(objFabric.getObjConfiguration());
                objArtwork.setStrArtworkId(objFabric.getStrArtworkID());
                objFabric.setArtworkMatrix(objFabric.getFabricMatrix());
                objFabricAction = new FabricAction();
                objFabricAction.getFabricArtwork(objFabric,strTableType);
                YarnAction objYarnAction = new YarnAction();                
                Yarn[] warpExtraYarn = objYarnAction.getFabricYarn(objFabric.getStrFabricID(), "Extra Warp",strTableType);
                objFabric.setWarpExtraYarn(warpExtraYarn);
                objYarnAction = new YarnAction();
                Yarn[] weftExtraYarn = objYarnAction.getFabricYarn(objFabric.getStrFabricID(), "Extra Weft",strTableType);
                objFabric.setWeftExtraYarn(weftExtraYarn);
            }else{
                Yarn[] warpExtraYarn = null;
                objFabric.setWarpExtraYarn(warpExtraYarn);
                objFabric.setIntExtraWarp(objConfiguration.getIntExtraWarp());
                Yarn[] weftExtraYarn = null;
                objFabric.setWeftExtraYarn(weftExtraYarn);
                objFabric.setIntExtraWeft(objConfiguration.getIntExtraWeft());
            }
            objFabricAction = new FabricAction();
            String[] threadPaletes = objFabricAction.getFabricPallets(objFabric,strTableType);
            objFabric.setColourPalette(threadPaletes);
            
            YarnAction objYarnAction = new YarnAction();                
            Yarn[] warpYarn = objYarnAction.getFabricYarn(objFabric.getStrFabricID(), "Warp",strTableType);
            objFabric.setWarpYarn(warpYarn);
            objYarnAction = new YarnAction();
            Yarn[] weftYarn = objYarnAction.getFabricYarn(objFabric.getStrFabricID(), "Weft",strTableType);
            objFabric.setWeftYarn(weftYarn);
            
            if(objFabric.getStrArtworkID()!=null){
                Artwork objArtwork=new Artwork();
                objArtwork.setObjConfiguration(objFabric.getObjConfiguration());
                objArtwork.setStrArtworkId(objFabric.getStrArtworkID());
                objArtworkAction = new ArtworkAction();
                objArtworkAction.getArtwork(objArtwork);
                byte[] bytArtworkThumbnil=objArtwork.getBytArtworkThumbnil();
                SeekableStream stream = new ByteArraySeekableStream(bytArtworkThumbnil);
                String[] names = ImageCodec.getDecoderNames(stream);
                ImageDecoder dec = ImageCodec.createImageDecoder(names[0], stream, null);
                RenderedImage im = dec.decodeAsRenderedImage();
                BufferedImage bufferedImage = PlanarImage.wrapRenderedImage(im).getAsBufferedImage();
                bytArtworkThumbnil=null;
                System.gc();                        
                ArrayList<java.awt.Color> colors = objArtworkAction.getImageColor(bufferedImage);
                objFabric.setColorCountArtwork(colors.size());
                String[][] colorWeave = new String[colors.size()*colors.size()][4];
                for(int i=0;i<objFabric.getColorWeave().length;i++){
                    colorWeave[i][0] = objFabric.getColorWeave()[i][0];
                    colorWeave[i][1] = objFabric.getColorWeave()[i][1];
                    colorWeave[i][2] = objFabric.getColorWeave()[i][2];
                    if(objFabric.getColorWeave()[i][1]!=null)
                        colorWeave[i][3] = Double.toString(objArtworkAction.getImageColorPercentage(bufferedImage,objFabric.getColorWeave()[i][0]));
                    else
                        colorWeave[i][3] = null;
                }
                objFabric.setColorWeave(colorWeave);
                colorWeave = null;
            }
            objFabric.setObjConfiguration(objConfiguration);
            objFabric.getObjConfiguration().setStrGraphSize(12+"x"+(int)((12*objFabric.getIntPPI())/objFabric.getIntEPI()));
            
        } catch (SQLException ex) {
            new Logging("SEVERE",ClothView.class.getName(),"loadFabric(): "+ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",ClothView.class.getName(),"loadFabric(): "+ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
        lblStatus.setText(objDictionaryAction.getWord("SUCCESS"));
    }
    
    /**
     * saveFabric
     * <p>
     * Function use for saving fabric.
     * 
     * @exception (@throws )
     * @value
     * @author Amit Kumar Singh
     * @version     %I%, %G%
     * @since 1.0
     */    
    private void saveFabric(Fabric objFabric, String strTableType){
        try{
            objFabric.setObjConfiguration(objConfiguration);
            objFabric.setStrFabricName(objCloth.getStrClothName()+"_"+objFabric.getStrClothType());
            objFabric.setStrFabricAccess(objCloth.getStrClothAccess());
            objFabric.setStrFabricID(new IDGenerator().getIDGenerator("FABRIC_LIBRARY", objConfiguration.getObjUser().getStrUserID()));
            locked = true;
            objFabricAction = new FabricAction();
            objFabricAction.fabricImage(objFabric,objFabric.getIntWeft(),objFabric.getIntWarp());
            objFabricAction = new FabricAction();
            if(objFabricAction.setFabric(objFabric, strTableType)>0){
                objFabricAction = new FabricAction();
                objFabricAction.setFabricPallets(objFabric, strTableType);
                //objFabricAction = new FabricAction();
                //objFabricAction.setFabricYarn(objFabric);
                //objFabricAction.clearFabricYarn(objFabric.getStrFabricID());
                String yarnId = null;
                YarnAction objYarnAction;
                for(int i = 0; i<objFabric.getWarpYarn().length; i++){
                    yarnId = new IDGenerator().getIDGenerator("YARN_LIBRARY", objConfiguration.getObjUser().getStrUserID());
                    objFabric.getWarpYarn()[i].setStrYarnId(yarnId);
                    objFabric.getWarpYarn()[i].setObjConfiguration(objConfiguration);
                    objYarnAction = new YarnAction();
                    objYarnAction.setYarn(objFabric.getWarpYarn()[i]);
                    /*objFabricAction = new FabricAction();
                    yarnId = objFabricAction.getYarnID(objFabric.getWarpYarn()[i]);
                    objFabric.getWarpYarn()[i].setThreadId(yarnId);
                    */objYarnAction = new YarnAction();
                    objYarnAction.setFabricYarn(objFabric.getStrFabricID(), objFabric.getWarpYarn()[i],i, strTableType);
                }
                for(int i = 0; i<objFabric.getWeftYarn().length; i++){
                    yarnId = new IDGenerator().getIDGenerator("YARN_LIBRARY", objConfiguration.getObjUser().getStrUserID());
                    objFabric.getWeftYarn()[i].setStrYarnId(yarnId);
                    objFabric.getWeftYarn()[i].setObjConfiguration(objConfiguration);
                    objYarnAction = new YarnAction();
                    objYarnAction.setYarn(objFabric.getWeftYarn()[i]);
                    /*objYarnAction = new YarnAction();
                    yarnId = objYarnAction.getYarnID(objFabric.getWarpYarn()[i]);
                    objFabric.getWarpYarn()[i].setThreadId(yarnId);
                    */objYarnAction = new YarnAction();
                    objYarnAction.setFabricYarn(objFabric.getStrFabricID(), objFabric.getWeftYarn()[i],i, strTableType);
                }
                if(objFabric.getWarpExtraYarn()!=null){
                    for(int i = 0; i<objFabric.getWarpExtraYarn().length; i++){
                        yarnId = new IDGenerator().getIDGenerator("YARN_LIBRARY", objConfiguration.getObjUser().getStrUserID());
                        objFabric.getWarpExtraYarn()[i].setStrYarnId(yarnId);
                        objFabric.getWarpExtraYarn()[i].setObjConfiguration(objConfiguration);
                        objYarnAction = new YarnAction();
                        objYarnAction.setYarn(objFabric.getWarpExtraYarn()[i]);
                        /*objFabricAction = new FabricAction();
                        yarnId = objFabricAction.getYarnID(objFabric.getWarpYarn()[i]);
                        objFabric.getWarpYarn()[i].setThreadId(yarnId);
                        */objYarnAction = new YarnAction();
                        objYarnAction.setFabricYarn(objFabric.getStrFabricID(), objFabric.getWarpExtraYarn()[i],i, strTableType);
                    }
                }
                if(objFabric.getWeftExtraYarn()!=null){
                    for(int i = 0; i<objFabric.getWeftExtraYarn().length; i++){
                        yarnId = new IDGenerator().getIDGenerator("YARN_LIBRARY", objConfiguration.getObjUser().getStrUserID());
                        objFabric.getWeftExtraYarn()[i].setStrYarnId(yarnId);
                        objFabric.getWeftExtraYarn()[i].setObjConfiguration(objConfiguration);
                        objYarnAction = new YarnAction();
                        objYarnAction.setYarn(objFabric.getWeftExtraYarn()[i]);
                        /*objFabricAction = new FabricAction();
                        yarnId = objFabricAction.getYarnID(objFabric.getWarpYarn()[i]);
                        objFabric.getWarpYarn()[i].setThreadId(yarnId);
                        */objYarnAction = new YarnAction();
                        objYarnAction.setFabricYarn(objFabric.getStrFabricID(), objFabric.getWeftExtraYarn()[i],i, strTableType);
                    }
                }                        
                if(objFabric.getStrArtworkID()!=null){
                    objFabricAction = new FabricAction();
                    objFabricAction.setFabricArtwork(objFabric, strTableType);
                }
            }
            isNew = false;
            lblStatus.setText(objFabric.getStrFabricName()+" : "+objDictionaryAction.getWord("DATASAVED"));
        } catch (SQLException ex) {
            Logger.getLogger(ClothView.class.getName()).log(Level.SEVERE, null, ex);
            new Logging("SEVERE",ClothView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            Logger.getLogger(ClothView.class.getName()).log(Level.SEVERE, null, ex);
            new Logging("SEVERE",ClothView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
 
    private void componentContextMenu(final ScrollPane component,final String strFabricType){
        ContextMenu contextMenu = new ContextMenu();
        MenuItem componentFabricAssignment = new MenuItem(objDictionaryAction.getWord("FABRICASSINGMENTEDIT"));
        componentFabricAssignment.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/open.png"));
        MenuItem componentFabricEditor = new MenuItem(objDictionaryAction.getWord("FABRICEDITORUTILITY"));
        componentFabricEditor.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fabric_editor.png"));
        
        MenuItem componentFabricCreator = new MenuItem(objDictionaryAction.getWord("CREATEFABRIC"));
        componentFabricCreator.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/new.png"));
        MenuItem componentArtworkAssignment = new MenuItem(objDictionaryAction.getWord("JACQUARDCONVERSIONEDIT"));
        componentArtworkAssignment.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/assign_artwork_weave.png"));
        MenuItem componentYarnAssignment = new MenuItem(objDictionaryAction.getWord("YARNEDIT"));
        componentYarnAssignment.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/yarn.png"));
        MenuItem componentThreadPattern = new MenuItem(objDictionaryAction.getWord("THREADPATTERNEDIT"));
        componentThreadPattern.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/thread_pattern.png"));
        MenuItem componentThreadDensity = new MenuItem(objDictionaryAction.getWord("DENSITYEDIT"));
        componentThreadDensity.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/density.png"));
        MenuItem componentOrientation = new MenuItem(objDictionaryAction.getWord("REPEATORIENTATION")); //alt
        componentOrientation.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/repeat.png"));
        
        MenuItem componentFabricTransfer = new MenuItem(objDictionaryAction.getWord("TRANSFERLIBRARY"));
        componentFabricTransfer.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export.png"));
        MenuItem componentPrintGraph = new MenuItem(objDictionaryAction.getWord("PRINTGRAPHFILE"));
        componentPrintGraph.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/print.png"));
        MenuItem componentExportGraph = new MenuItem(objDictionaryAction.getWord("SAVEGRAPHFILE"));
        componentExportGraph.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_graph_as.png"));
        MenuItem componentExportXML = new MenuItem(objDictionaryAction.getWord("SAVEXMLFILE")); 
        componentExportXML.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_html_as.png"));
        MenuItem componentDescription = new MenuItem(objDictionaryAction.getWord("INFOSETTINGSUTILITY")); //alt
        componentDescription.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/settings.png"));
        
        Menu editingMenu = new Menu(objDictionaryAction.getWord("EDITFABRIC"));
        editingMenu.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/edit.png"));
        editingMenu.getItems().addAll(componentFabricCreator,componentArtworkAssignment,componentYarnAssignment,componentThreadPattern,componentThreadDensity,componentOrientation);
        editingMenu.getStyleClass().clear();
        editingMenu.getStyleClass().add("sub-menu");
        
        //contextMenu.getItems().addAll(componentFabricAssignment, componentFabricEditor, componentFabricCreator, componentArtworkAssignment, componentYarnAssignment, componentThreadPattern, componentThreadDensity, componentOrientation, componentFabricTransfer, componentPrintGraph, componentExportXML, componentDescription);
        contextMenu.getItems().addAll(componentFabricAssignment, componentFabricEditor, new SeparatorMenuItem(), editingMenu, new SeparatorMenuItem(), componentPrintGraph, componentExportGraph, componentExportXML, componentDescription);
        
        component.setContextMenu(contextMenu);
        
        componentFabricAssignment.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONFABRICASSINGMENTEDIT"));
                componentFabricAssignment((ImageView)component.getContent(),strFabricType);
            }
        });
        componentFabricEditor.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONFABRICEDITOR"));
                componentFabricEditor((ImageView)component.getContent(),strFabricType);
            }
        });
        componentFabricCreator.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCREATEFABRIC"));
                componentFabricCreation((ImageView)component.getContent(),strFabricType);
            }
        });
        componentFabricTransfer.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONTRANSFERLIBRARYEDIT"));
                componentFabricTransfer((ImageView)component.getContent(),strFabricType);
            }
        });
        componentArtworkAssignment.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONJACQUARDCONVERSIONEDIT"));
                componentFabricProperty((ImageView)component.getContent(),strFabricType, "Artwork");
            }
        });
        componentYarnAssignment.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONYARNEDIT"));
                componentFabricProperty((ImageView)component.getContent(),strFabricType, "Yarn");
            }
        });
        componentThreadPattern.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONTHREADPATTERNEDIT"));
                componentFabricProperty((ImageView)component.getContent(),strFabricType, "Pattern");
            }
        });
        componentThreadDensity.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONTHREADPATTERNEDIT"));
                componentFabricProperty((ImageView)component.getContent(),strFabricType, "Density");
            }
        });
        componentOrientation.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONREPEATORIENTATION"));
                componentOrientation((ImageView)component.getContent(),strFabricType);
            }
        });    
        componentPrintGraph.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPRINTGRAPHFILE"));
                componentFabricOutput((ImageView)component.getContent(),strFabricType,"PrintGraph");
            }
        });    
        componentExportGraph.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONSAVEGRAPHFILE"));
                componentFabricOutput((ImageView)component.getContent(),strFabricType,"ExportGraph");
            }
        });    
        componentExportXML.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONSAVEXMLFILE"));
                componentFabricOutput((ImageView)component.getContent(),strFabricType,"HTML");
            }
        });    
        componentDescription.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONINFOSETTINGSUTILITY"));
                componentDescription((ImageView)component.getContent(),strFabricType);
            }
        });    
    }

    private void componentFabricAssignment(ImageView component, String strClothType){
        try{
            objConfiguration.setStrClothType(strClothType);
            UserAction objUserAction = new UserAction();
            objUserAction.getConfiguration(objConfiguration);
            //objConfiguration.clothRepeat();
                
            Fabric objFabric = new Fabric();
            objFabric.setObjConfiguration(objConfiguration);
            objFabric.setStrClothType(strClothType);
            FabricImportView objFabricImportView= new FabricImportView(objFabric,"ClothEditor");
            if(component.getUserData()!=null && objFabric.getStrFabricID()==null){
                /* This is the case when a fabric was previously assigned and
                during re-assignment user closes/cancels the dialog without assigning a fabric.
                In such a case previous fabric should be retained as it is. */
            } else if(component.getUserData()==null && objFabric.getStrFabricID()==null){
                /* DO NOTHING */
                /* This is the case when no fabric was assigned previously and
                during assignment user closes/cancels the dialog without assigning a fabric.
                In such a case no fabric should be assigned */
            } else{
                loadFabric(objFabric, "Main");
                if(objFabric.getBytFabricIcon()==null || !objFabric.getStrClothType().equalsIgnoreCase(strClothType))
                    return;
        
                component.setUserData(objFabric);
                component.setId(objFabric.getStrFabricID());
                String key=getComponentName(component);
                int index=getComponentIndex(component);
                repeats[index]=(repeats[index]>0)?repeats[index]:1;
                objCloth.mapClothFabric.put(key, objFabric);
                //MAINFABRICID, ROTATIONANGLE, REPEATMODE, FABRICREPEAT, WIDTH, HEIGHT
                objCloth.mapClothFabricDetails.put(key, new String[]{objFabric.getStrFabricID(),"90","Rectangular (Default)",Integer.toString(repeats[index]),Integer.toString(objFabric.getIntWarp()),Integer.toString(objFabric.getIntWeft())});
                rotations[index]=90;
                repeatMode[index]="Rectangular (Default)";
                //repeats[index]=1;
                ends[index]=objFabric.getIntWarp();
                picks[index]=objFabric.getIntWeft();

                //widths[index]=objFabric.getIntWarp();
                //heights[index]=objFabric.getIntWeft();
                loadCloth(objCloth);
                //resetClothLayout();
                symmetryAssignment(component);
                System.gc();
                saveContext();

                plotCloth();
                // setting heights and widths
                setWidthHeight();
                //set component sizes
                setComponentWidthHeight();
                //prepare components
                plotContainerGP();
            }
            Cloth objClothCopy = new Cloth();
            cloneCloth(objClothCopy);
            //objClothCopy.setStrClothName("EditArtwork");
            objUR.doCommand("component Fabric Assignment", objClothCopy);
            System.gc();
        } catch(Exception ex){
            new Logging("SEVERE",ClothView.class.getName(),"componentFabricAssignment(): "+getComponentName(component),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    /**
     * @author Amit Kumar Singh
     * Migrating fabric assigned to a garment component to Fabric Editor
     * @param component ImageView for garment component
     * @param strSearchBy  String description of cloth type to put in Configuration MapRecent
     */
    private void componentFabricEditor(ImageView component, String strClothType){
        try{
            if(component.getUserData()!=null && component.getId()!=null){
                objConfiguration.setStrClothType(strClothType);
                UserAction objUserAction = new UserAction();
                objUserAction.getConfiguration(objConfiguration);
                //objConfiguration.clothRepeat();
                // storing id before generating a new (for temp database)
                saveContext();
                objConfiguration.strWindowFlowContext = "ClothEditor";                        
                objConfiguration.setStrRecentFabric(component.getId());
                clothStage.close();
                System.gc();
                FabricView objFabricView = new FabricView(objConfiguration);
            } else{
                final Stage dialogStage = new Stage();
                dialogStage.initModality(Modality.APPLICATION_MODAL);
                GridPane popup=new GridPane();
                popup.setHgap(5);
                popup.setVgap(5);
                Label lblMsg = new Label(objDictionaryAction.getWord("NOCLOTHPART"));
                popup.add(lblMsg, 0, 0);
                Button btnYes = new Button(objDictionaryAction.getWord("CANCEL"));
                btnYes.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent t) {
                        dialogStage.close  ();
                        System.gc();
                    }
                });
                popup.add(btnYes, 1, 1);
                Scene scene = new Scene(popup);
                scene.getStylesheets().add(ClothView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
                dialogStage.setScene(scene);
                dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("FABRICEDITORUTILITY"));
                dialogStage.showAndWait();
                lblStatus.setText(objDictionaryAction.getWord("NOCLOTHPART"));
            }
        } catch(SQLException ex){
            new Logging("SEVERE",ClothView.class.getName(),"componentFabricEditor(): ",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",ClothView.class.getName(),"componentFabricEditor(): ",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void componentFabricCreation(ImageView component, String strClothType){
        try{
            objConfiguration.setStrClothType(strClothType);
            UserAction objUserAction = new UserAction();
            objUserAction.getConfiguration(objConfiguration);
            //objConfiguration.clothRepeat();
                
            Fabric objFabric = new Fabric();
            objFabric.setObjConfiguration(objConfiguration);
            objFabric.setStrClothType(strClothType);
         
            Weave objWeave = new Weave();
            objWeave.setObjConfiguration(objConfiguration);
            WeaveImportView objWeaveImportView= new WeaveImportView(objWeave);
            if(component.getUserData()!=null && objWeave.getStrWeaveID()==null){
                /* This is the case when a fabric was previously assigned and
                during re-assignment user closes/cancels the dialog without assigning a fabric.
                In such a case previous fabric should be retained as it is. */
            } else if(component.getUserData()==null && objWeave.getStrWeaveID()==null){
                /* DO NOTHING */
                /* This is the case when no fabric was assigned previously and
                during assignment user closes/cancels the dialog without assigning a fabric.
                In such a case no fabric should be assigned */
            } else{
                
                objFabric.setStrBaseWeaveID(objWeave.getStrWeaveID());
                newFabric(objFabric, "Main");
                objWeave = null;
                
                component.setUserData(objFabric);
                component.setId(null);
                String key=getComponentName(component);
                int index=getComponentIndex(component);
                repeats[index]=(repeats[index]>0)?repeats[index]:1;
                objCloth.mapClothFabric.put(key, objFabric);
                //MAINFABRICID, ROTATIONANGLE, REPEATMODE, FABRICREPEAT, WIDTH, HEIGHT
                objCloth.mapClothFabricDetails.put(key, new String[]{objFabric.getStrFabricID(),"90","Rectangular (Default)",Integer.toString(repeats[index]),Integer.toString(objFabric.getIntWarp()),Integer.toString(objFabric.getIntWeft())});
                rotations[index]=90;
                repeatMode[index]="Rectangular (Default)";
                //repeats[index]=1;
                ends[index]=objFabric.getIntWarp();
                picks[index]=objFabric.getIntWeft();

                //widths[index]=objFabric.getIntWarp();
                //heights[index]=objFabric.getIntWeft();
                loadCloth(objCloth);
                //resetClothLayout();
                symmetryAssignment(component);
                System.gc();
                saveContext();

                plotCloth();
                // setting heights and widths
                setWidthHeight();
                //set component sizes
                setComponentWidthHeight();
                //prepare components
                plotContainerGP();
            }
            Cloth objClothCopy = new Cloth();
            cloneCloth(objClothCopy);
            //objClothCopy.setStrClothName("EditArtwork");
            objUR.doCommand("component Fabric Creation", objClothCopy);
            System.gc();
        } catch(Exception ex){
            new Logging("SEVERE",ClothView.class.getName(),"componentFabricAssignment(): "+getComponentName(component),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void componentFabricTransfer(ImageView component, String strSearchBy){
        try{
            if(component.getUserData()!=null && (component.getUserData() instanceof Fabric)){
                Fabric objFabric = (Fabric)component.getUserData();
                objConfiguration.setStrClothType(strSearchBy);
            } else{
                final Stage dialogStage = new Stage();
                dialogStage.initModality(Modality.APPLICATION_MODAL);
                GridPane popup=new GridPane();
                popup.setHgap(5);
                popup.setVgap(5);
                Label lblMsg = new Label(objDictionaryAction.getWord("NOCLOTHPART"));
                popup.add(lblMsg, 0, 0);
                Button btnYes = new Button(objDictionaryAction.getWord("CANCEL"));
                btnYes.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent t) {
                        dialogStage.close  ();
                        System.gc();
                    }
                });
                popup.add(btnYes, 1, 1);
                Scene scene = new Scene(popup);
                scene.getStylesheets().add(ClothView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
                dialogStage.setScene(scene);
                dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("ORIENTATION"));
                dialogStage.showAndWait();
                lblStatus.setText(objDictionaryAction.getWord("NOCLOTHPART"));
            }
            System.gc();
        } catch(Exception ex){
            new Logging("SEVERE",ClothView.class.getName(),"componentFabricTransfer(): "+ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    // mode Artwork, Yarn, Pattern, Density
    private void componentFabricProperty(ImageView component, String strSearchBy, String actionMode){
        try{
            if(component.getUserData()!=null && (component.getUserData() instanceof Fabric)){
                Fabric objFabric = (Fabric)component.getUserData();
                objConfiguration.setStrClothType(strSearchBy);
                if(actionMode.equalsIgnoreCase("Artwork")){
                    FabricEditView objFabricEditView= new FabricEditView(objFabric);
                    if(objFabric.getStrArtworkID()==null||objFabric.getStrArtworkID().equalsIgnoreCase("null")){
                        byte[][] baseWeaveMatrix = objFabric.getBaseWeaveMatrix();
                        try {
                            objArtworkAction= new ArtworkAction();
                            objFabric.setFabricMatrix(objArtworkAction.repeatMatrix(baseWeaveMatrix,objFabric.getIntWeft(),objFabric.getIntWarp()));
                            objArtworkAction= new ArtworkAction();
                            objFabric.setReverseMatrix(objArtworkAction.invertMatrix(objFabric.getFabricMatrix()));
                        } catch (SQLException ex) {
                            new Logging("SEVERE",ClothView.class.getName(),ex.toString(),ex);
                            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                        }
                        objFabric.setWarpExtraYarn(null);
                        objFabric.setIntExtraWarp(objConfiguration.getIntExtraWarp());
                        objFabric.setWeftExtraYarn(null);
                        objFabric.setIntExtraWeft(objConfiguration.getIntExtraWeft());
                    }else{
                        objFabric.setIntWeft(objFabric.getArtworkMatrix().length);        
                        objFabric.setIntWarp(objFabric.getArtworkMatrix()[0].length);
                        objFabric.setIntHooks((int) Math.ceil(objFabric.getIntWarp()/(double)objFabric.getIntTPD()));
                        objFabric.setFabricMatrix(objFabric.getArtworkMatrix());
                        objConfiguration.setStrRecentArtwork(objFabric.getObjConfiguration().getStrRecentArtwork());
                    }
                } else if(actionMode.equalsIgnoreCase("Yarn")){
                    objFabric.getObjConfiguration().setWarpYarn(objFabric.getWarpYarn());
                    objFabric.getObjConfiguration().setWeftYarn(objFabric.getWeftYarn());
                    objFabric.getObjConfiguration().setWarpExtraYarn(objFabric.getWarpExtraYarn());
                    objFabric.getObjConfiguration().setWeftExtraYarn(objFabric.getWeftExtraYarn());
                    objFabric.getObjConfiguration().setIntExtraWeft(objFabric.getIntExtraWeft());
                    objFabric.getObjConfiguration().setIntExtraWarp(objFabric.getIntExtraWarp());
                    objFabric.getObjConfiguration().setColourPalette(objFabric.getColourPalette());
                    try{
                        YarnEditView objYarnEditView = new YarnEditView(objFabric.getObjConfiguration());
                    } catch(Exception ex){
                        new Logging("SEVERE",ClothView.class.getName(),ex.toString(),ex);
                        lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                    }
                    objFabric.setWarpYarn(objFabric.getObjConfiguration().getWarpYarn());
                    objFabric.setWeftYarn(objFabric.getObjConfiguration().getWeftYarn());
                    objFabric.setWarpExtraYarn(objFabric.getObjConfiguration().getWarpExtraYarn());
                    objFabric.setWeftExtraYarn(objFabric.getObjConfiguration().getWeftExtraYarn());
                    objFabric.setIntExtraWeft(objFabric.getObjConfiguration().getIntExtraWeft());
                    objFabric.setIntExtraWarp(objFabric.getObjConfiguration().getIntExtraWarp());
                    objFabric.setColourPalette(objFabric.getObjConfiguration().getColourPalette());
                } else if(actionMode.equalsIgnoreCase("Pattern")){
                    try {
                        objFabric.getObjConfiguration().setColourPalette(objFabric.getColourPalette());
                        objFabric.getObjConfiguration().setStrWarpPatternID(objFabric.getStrWarpPatternID());
                        objFabric.getObjConfiguration().setStrWeftPatternID(objFabric.getStrWeftPatternID());
                        //objConfiguration.setWarpYarn(objFabric.getWarpYarn());
                        //objConfiguration.setWeftYarn(objFabric.getWeftYarn());
                        PatternView objPatternView = new PatternView(objFabric.getObjConfiguration());            
                        objFabric.setColourPalette(objFabric.getObjConfiguration().getColourPalette());
                        objFabric.setStrWarpPatternID(objFabric.getObjConfiguration().getStrWarpPatternID());
                        objFabric.setStrWeftPatternID(objFabric.getObjConfiguration().getStrWeftPatternID());
                        objFabric.setWarpYarn(objFabric.getObjConfiguration().getWarpYarn());
                        objFabric.setWeftYarn(objFabric.getObjConfiguration().getWeftYarn());            
                    } catch (Exception ex) {
                        new Logging("SEVERE",ClothView.class.getName(),ex.toString(),ex);
                        lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                    }
                } else if(actionMode.equalsIgnoreCase("Density")){
                    FabricDensityView objFabricDensityView = new FabricDensityView(objFabric);
                    objFabric.setIntHPI((int) Math.ceil(objFabric.getIntEPI()/(double)objFabric.getIntTPD()));
                    objFabric.getObjConfiguration().setStrGraphSize(12+"x"+(int)((12*objFabric.getIntPPI())/objFabric.getIntEPI()));
                }
                resetClothLayout();
                // setting heights and widths
                setWidthHeight();
                //set component sizes
                setComponentWidthHeight();
                //prepare components
                plotContainerGP();
            } else{
                final Stage dialogStage = new Stage();
                dialogStage.initModality(Modality.APPLICATION_MODAL);
                GridPane popup=new GridPane();
                popup.setHgap(5);
                popup.setVgap(5);
                Label lblMsg = new Label(objDictionaryAction.getWord("NOCLOTHPART"));
                popup.add(lblMsg, 0, 0);
                Button btnYes = new Button(objDictionaryAction.getWord("CANCEL"));
                btnYes.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent t) {
                        dialogStage.close  ();
                        System.gc();
                    }
                });
                popup.add(btnYes, 1, 1);
                Scene scene = new Scene(popup);
                scene.getStylesheets().add(ClothView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
                dialogStage.setScene(scene);
                dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("ORIENTATION"));
                dialogStage.showAndWait();
                lblStatus.setText(objDictionaryAction.getWord("NOCLOTHPART"));
            }
            Cloth objClothCopy = new Cloth();
            cloneCloth(objClothCopy);
            //objClothCopy.setStrClothName("EditArtwork");
            objUR.doCommand("component Fabric Property", objClothCopy);
            System.gc();
        } catch(Exception ex){
            if(actionMode.equalsIgnoreCase("Artwork"))
                new Logging("SEVERE",ClothView.class.getName(),"editing artwork assignment",ex);
            else if(actionMode.equalsIgnoreCase("Yarn"))
                new Logging("SEVERE",ClothView.class.getName(),"editing yarn properties",ex);
            else if(actionMode.equalsIgnoreCase("Thread"))
                new Logging("SEVERE",ClothView.class.getName(),"editing thread sequence",ex);
            else if(actionMode.equalsIgnoreCase("Density"))
                new Logging("SEVERE",ClothView.class.getName(),"editing density",ex);
            else
                new Logging("SEVERE",ClothView.class.getName(),"componentProperty(): "+ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void componentOrientation(final ImageView component, String strSearchBy){
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setAlignment(Pos.CENTER);
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        
        Scene scene = new Scene(popup, 300, 200, Color.WHITE);
        scene.getStylesheets().add(ClothView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        
        if(component.getUserData()!=null && (component.getUserData() instanceof Fabric)){
            objConfiguration.setStrClothType(strSearchBy);
            ///objFabricAction.getFabric(objFabric);
            final int index  = getComponentIndex(component);

            Label lblWidthNew = new Label(objDictionaryAction.getWord("LENGTH"));
            lblWidthNew.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPLENGTH")));
            lblWidthNew.setPrefWidth(scene.getWidth()/2);
            popup.add(lblWidthNew, 0, 0);
            final TextField txtWidth = new TextField(Integer.toString(ends[index])){
                @Override public void replaceText(int start, int end, String text) {
                if (text.matches("[0-9]*")) {
                        super.replaceText(start, end, text);
                    }
                }
                @Override public void replaceSelection(String text) {
                if (text.matches("[0-9]*")) {
                        super.replaceSelection(text);
                    }
                }
            };
            txtWidth.setPromptText(objDictionaryAction.getWord("TOOLTIPLENGTH"));
            txtWidth.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPLENGTH")));
            txtWidth.setPrefWidth(scene.getWidth()/2);
            popup.add(txtWidth, 1, 0);

            Label lblHeightNew = new Label(objDictionaryAction.getWord("WIDTH"));
            lblHeightNew.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPWIDTH")));
            lblHeightNew.setPrefWidth(scene.getWidth()/2);
            popup.add(lblHeightNew, 0, 1);
            final TextField txtHeight = new TextField(Integer.toString(picks[index])){
                @Override public void replaceText(int start, int end, String text) {
                if (text.matches("[0-9]*")) {
                        super.replaceText(start, end, text);
                    }
                }
                @Override public void replaceSelection(String text) {
                if (text.matches("[0-9]*")) {
                        super.replaceSelection(text);
                    }
                }
            };
            txtHeight.setPromptText(objDictionaryAction.getWord("TOOLTIPWIDTH"));
            txtHeight.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPWIDTH")));
            txtHeight.setPrefWidth(scene.getWidth()/2);
            popup.add(txtHeight, 1, 1);

            Label lblRotationNew = new Label(objDictionaryAction.getWord("ROTATION"));
            lblRotationNew.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPROTATION")));
            lblRotationNew.setPrefWidth(scene.getWidth()/2);
            popup.add(lblRotationNew, 0, 2);
            final ChoiceBox rotationCB=new ChoiceBox();
            rotationCB.getItems().add(0, "0");
            rotationCB.getItems().add(1, "90");
            rotationCB.getItems().add(2, "180");
            rotationCB.getItems().add(3, "270");
            rotationCB.setValue(Integer.toString(rotations[index]));
            rotationCB.setPrefWidth(scene.getWidth()/2);
            popup.add(rotationCB, 1, 2);

            Label lblRepeatMode=new Label(objDictionaryAction.getWord("REPEATMODE"));
            lblRepeatMode.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPREPEATMODE")));
            lblRepeatMode.setPrefWidth(scene.getWidth()/2);
            popup.add(lblRepeatMode, 0, 3);
            final ChoiceBox repeatModeCB=new ChoiceBox();
            repeatModeCB.getItems().add(0, "Rectangular (Default)");
            repeatModeCB.getItems().add(1, "1/2 Horizontal");
            repeatModeCB.getItems().add(2, "1/2 Vertical");
            repeatModeCB.getItems().add(3, "1/3 Horizontal");
            repeatModeCB.getItems().add(4, "1/3 Vertical");
            repeatModeCB.getItems().add(5, "1/4 Horizontal");
            repeatModeCB.getItems().add(6, "1/4 Vertical");
            repeatModeCB.getItems().add(7, "1/5 Horizontal");
            repeatModeCB.getItems().add(8, "1/5 Vertical");
            repeatModeCB.getItems().add(9, "1/6 Horizontal");
            repeatModeCB.getItems().add(10, "1/6 Vertical");
            repeatModeCB.setValue(repeatMode[index]);
            repeatModeCB.setPrefWidth(scene.getWidth()/2);
            popup.add(repeatModeCB, 1, 3);
            /*
            final double aspectRatio=ends[index]/picks[index];
            txtWidth.textProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                    if(!txtWidth.isFocused()||txtWidth.getText().trim().length()==0)
                        return;
                    int ends = Integer.parseInt(t1);
                    int picks= (int)(ends*aspectRatio);
                    if(ends>MINSIZE && picks>MINSIZE){
                        for(int a=0; a<(rotations[index]/90); a++){
                            int temp = ends;
                            ends = picks;
                            picks = temp;
                        }
                        if(component.equals(leftBorder) || component.equals(rightBorder) || component.equals(skirt)){
                            widths[index]=ends;
                            heights[index]=picks*repeats[index];
                        }else{
                            widths[index]=ends*repeats[index];
                            heights[index]=picks;
                        }
                        //validate sizes
                        if(component.equals(leftBorder) || component.equals(rightBorder) || component.equals(skirt)){
                            if(heights[index]>MAXBORDER)
                                txtWidth.setText(t);
                            else
                                txtHeight.setText(String.valueOf((int)(Double.parseDouble(txtWidth.getText())*aspectRatio)));
                        } else if(component.equals(leftsideLeftCrossBorder) || component.equals(leftsideRightCrossBorder) || component.equals(rightsideLeftCrossBorder) || component.equals(rightsideRightCrossBorder)){
                            if(widths[index]>MAXBORDER)
                                txtWidth.setText(t);
                            else
                                txtHeight.setText(String.valueOf((int)(Double.parseDouble(txtWidth.getText())*aspectRatio)));
                        } else if(component.equals(leftPallu) || component.equals(rightPallu) || component.equals(blouse)){
                            if(widths[index]>MAXPALLU)
                                txtWidth.setText(t);
                            else
                                txtHeight.setText(String.valueOf((int)(Double.parseDouble(txtWidth.getText())*aspectRatio)));
                        }
                    }else{
                        txtWidth.setText(t);
                    }
                }
            });
            txtHeight.textProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                    if(!txtHeight.isFocused()||txtHeight.getText().length()==0)
                        return;
                    int picks = Integer.parseInt(t1);
                    int ends= (int)(picks*aspectRatio);
                    if(widths[index]>MINSIZE && heights[index]>MINSIZE){
                        for(int a=0; a<(rotations[index]/90); a++){
                            int temp = ends;
                            ends = picks;
                            picks = temp;
                        }
                        if(component.equals(leftBorder) || component.equals(rightBorder) || component.equals(skirt)){
                            widths[index]=ends;
                            heights[index]=picks*repeats[index];
                        }else{
                            widths[index]=ends*repeats[index];
                            heights[index]=picks;
                        }
                        
                        if(component.equals(leftBorder) || component.equals(rightBorder) || component.equals(skirt)){
                            if(heights[index]>MAXBORDER)
                                txtHeight.setText(t);
                            else
                                txtWidth.setText(String.valueOf((int)(Double.parseDouble(txtWidth.getText())*aspectRatio)));
                        } else if(component.equals(leftsideLeftCrossBorder) || component.equals(leftsideRightCrossBorder) || component.equals(rightsideLeftCrossBorder) || component.equals(rightsideRightCrossBorder)){
                            if(widths[index]>MAXBORDER)
                                txtHeight.setText(t);
                            else
                                txtWidth.setText(String.valueOf((int)(Double.parseDouble(txtWidth.getText())*aspectRatio)));
                        } else if(component.equals(leftPallu) || component.equals(rightPallu) || component.equals(blouse)){
                            if(widths[index]>MAXPALLU)
                                txtHeight.setText(t);
                            else
                                txtWidth.setText(String.valueOf((int)(Double.parseDouble(txtHeight.getText())*aspectRatio)));
                        }
                    }else{
                        txtHeight.setText(t);
                    }
                }
            });
            */
            Button btnApply = new Button(objDictionaryAction.getWord("APPLY"));
            btnApply.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPAPPLY")));
            btnApply.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
            btnApply.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
            btnApply.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {  
                    lblStatus.setText(objDictionaryAction.getWord("Process")+" "+objDictionaryAction.getWord("Completed."));
                    
                    double warp = Integer.parseInt(txtWidth.getText());
                    double weft= Integer.parseInt(txtHeight.getText());
                    if(warp>MINSIZE && weft>MINSIZE){
                        Fabric objFabric  = (Fabric)component.getUserData();
                        for(int a=0; a<(rotations[index]/90); a++){
                            double temp = warp;
                            warp = weft;
                            weft = temp;
                        }
                        warp = Double.parseDouble(String.format("%.3f",warp/(double)objFabric.getIntEPI()));
                        weft = Double.parseDouble(String.format("%.3f",weft/(double)objFabric.getIntPPI()));
                        
                        final int MINSIZE=3; // minimum size of components in pixels
                        final int MAXBORDER=12*inchFactor; // maximum size of Borders, Cross Borders, Skirt (in pixels)
                        final int MAXPALLU=48*inchFactor; // maximum size of Pallu(s), Blouse (in pixels)
    
                        //validate sizes
                        if((weft*repeats[index]<MAXBORDER) && (component.equals(leftBorder) || component.equals(rightBorder) || component.equals(skirt))){
                            ends[index]=Integer.parseInt(txtWidth.getText());
                            picks[index]=Integer.parseInt(txtHeight.getText());
                            rotations[index]=Integer.parseInt(rotationCB.getValue().toString());
                            repeatMode[index]=repeatModeCB.getValue().toString();
                        } else if((warp*repeats[index]<MAXBORDER) && (component.equals(leftsideLeftCrossBorder) || component.equals(leftsideRightCrossBorder) || component.equals(rightsideLeftCrossBorder) || component.equals(rightsideRightCrossBorder))){
                            ends[index]=Integer.parseInt(txtWidth.getText());
                            picks[index]=Integer.parseInt(txtHeight.getText());
                            rotations[index]=Integer.parseInt(rotationCB.getValue().toString());
                            repeatMode[index]=repeatModeCB.getValue().toString();
                        } else if((warp*repeats[index]<MAXPALLU) && (component.equals(leftPallu) || component.equals(rightPallu) || component.equals(blouse))){
                            ends[index]=Integer.parseInt(txtWidth.getText());
                            picks[index]=Integer.parseInt(txtHeight.getText());
                            rotations[index]=Integer.parseInt(rotationCB.getValue().toString());
                            repeatMode[index]=repeatModeCB.getValue().toString();
                        } else if(component.equals(body)){
                            ends[index]=Integer.parseInt(txtWidth.getText());
                            picks[index]=Integer.parseInt(txtHeight.getText());
                            rotations[index]=Integer.parseInt(rotationCB.getValue().toString());
                            repeatMode[index]=repeatModeCB.getValue().toString();
                        }else{
                            lblStatus.setText(objDictionaryAction.getWord("TOOLARGEFABRIC"));
                        }
                    } else{
                        lblStatus.setText(objDictionaryAction.getWord("TOOSMALLFABRIC"));
                    }
                    Fabric objFabric  = (Fabric)component.getUserData();
                    objFabric.setDblArtworkLength(Double.parseDouble(String.format("%.3f",ends[index]/(double)objFabric.getIntEPI())));
                    objFabric.setDblArtworkWidth(Double.parseDouble(String.format("%.3f",picks[index]/(double)objFabric.getIntPPI())));
                    
                    
                    dialogStage.close();
                    System.gc();
                    
                    saveContext();
                    symmetryAssignment(component);
                    saveContext();

                    plotCloth();
                    // setting heights and widths
                    setWidthHeight();
                    //set component sizes
                    setComponentWidthHeight();
                    //prepare components
                    plotContainerGP();
                }
            });
            popup.add(btnApply, 0, 5);

            Button btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));
            btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
            btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            btnCancel.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
            btnCancel.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {  
                    lblStatus.setText(objDictionaryAction.getWord("TOOLTIPCANCEL"));
                    dialogStage.close();
                }
            });
            popup.add(btnCancel, 1, 5);
        } else{
            Label lblMsg = new Label(objDictionaryAction.getWord("NOCLOTHPART"));
            popup.add(lblMsg, 0, 0);
            Button btnYes = new Button(objDictionaryAction.getWord("CANCEL"));
            btnYes.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    dialogStage.close  ();
                    System.gc();
                }
            });
            popup.add(btnYes, 1, 1);
            lblStatus.setText(objDictionaryAction.getWord("NOCLOTHPART"));
        }
        Cloth objClothCopy = new Cloth();
        cloneCloth(objClothCopy);
        //objClothCopy.setStrClothName("EditArtwork");
        objUR.doCommand("component Orientation", objClothCopy);
        dialogStage.setScene(scene);
        dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("REPEATORIENTATION"));
        dialogStage.showAndWait();        
    }
    private void componentFabricOutput(ImageView component, String strSearchBy, String actionMode){
        if(component.getUserData()!=null && (component.getUserData() instanceof Fabric)){
            objConfiguration.setStrClothType(strSearchBy);
            Fabric objFabric = (Fabric)component.getUserData();

            if(actionMode.equalsIgnoreCase("PrintGraph")){
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPRINTGRAPHFILE"));
                printGraph(objFabric);
            }else if(actionMode.equalsIgnoreCase("ExportGraph")){
                lblStatus.setText(objDictionaryAction.getWord("ACTIONSAVEGRAPHFILE"));

                FileChooser fileChoser=new FileChooser();
                fileChoser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTBMP")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
                FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("BMP (.bmp)", "*.bmp");
                fileChoser.getExtensionFilters().add(extFilterBMP);
                File ifile=fileChoser.showSaveDialog(clothStage);
                if(ifile==null)
                    return;
                else
                    clothStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+ifile.getAbsoluteFile().getName()+"]");
                saveAsGraph(objFabric,ifile,0);
            }else{
                lblStatus.setText(objDictionaryAction.getWord("ACTIONSAVEXMLFILE"));

                FileChooser fileChoser=new FileChooser();
                fileChoser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTHTML")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
                FileChooser.ExtensionFilter extFilterHTML = new FileChooser.ExtensionFilter("HTML (*.html)", "*.html");
                fileChoser.getExtensionFilters().add(extFilterHTML);
                File ifile=fileChoser.showSaveDialog(clothStage);
                if(ifile==null)
                    return;
                else
                    clothStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+ifile.getAbsoluteFile().getName()+"]");
                saveAsHtml(objFabric, ifile);
            }
            lblStatus.setText(objDictionaryAction.getWord("DATASAVED"));
        } else{
            final Stage dialogStage = new Stage();
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            GridPane popup=new GridPane();
            popup.setHgap(5);
            popup.setVgap(5);
            Label lblMsg = new Label(objDictionaryAction.getWord("NOCLOTHPART"));
            popup.add(lblMsg, 0, 0);
            Button btnYes = new Button(objDictionaryAction.getWord("CANCEL"));
            btnYes.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    dialogStage.close  ();
                    System.gc();
                }
            });
            popup.add(btnYes, 1, 1);
            Scene scene = new Scene(popup);
            scene.getStylesheets().add(ClothView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
            dialogStage.setScene(scene);
            dialogStage.setTitle(objDictionaryAction.getWord("PROJECT"));
            dialogStage.showAndWait();
            lblStatus.setText(objDictionaryAction.getWord("NOCLOTHPART"));
        }
    }
    private void componentDescription(ImageView component, String strSearchBy){
        final Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setAlignment(Pos.CENTER);
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        
        Label lblMsg = new Label();
        lblMsg.setAlignment(Pos.TOP_LEFT);
        if(component.getUserData()!=null && (component.getUserData() instanceof Fabric)){
            objConfiguration.setStrClothType(strSearchBy);
            Fabric objFabric = (Fabric)component.getUserData();
            objFabric.setStrSearchBy(strSearchBy);
            objFabric.setObjConfiguration(objConfiguration);
            //FabricAction objFabricAction = new FabricAction();
            ///objFabricAction.getFabric(objFabric);
            objFabric.getObjConfiguration().setStrGraphSize(12+"x"+(int)((12*objFabric.getIntPPI())/objFabric.getIntHPI()));
            lblMsg.setText(objDictionaryAction.getWord("NAME")+": "+objFabric.getStrFabricName()
                    +"\n"+objDictionaryAction.getWord("CLOTHTYPE")+": "+objFabric.getStrClothType()
                    +"\n"+objDictionaryAction.getWord("FABRICTYPE")+": "+objFabric.getStrFabricType()
                    +"\n"+objDictionaryAction.getWord("FABRICLENGTH")+"(inch): "+objFabric.getDblFabricLength()
                    +"\n"+objDictionaryAction.getWord("FABRICWIDTH")+"(inch): "+objFabric.getDblFabricWidth()
                    +"\n"+objDictionaryAction.getWord("HOOKS")+": "+objFabric.getIntHooks()
                    +"\n"+objDictionaryAction.getWord("PICKS")+": "+objFabric.getIntWeft()
                    +"\n"+objDictionaryAction.getWord("SHAFT")+": "+objFabric.getIntShaft()
                    +"\n"+objDictionaryAction.getWord("REEDCOUNT")+": "+objFabric.getIntReedCount()
                    +"\n"+objDictionaryAction.getWord("DENTS")+": "+objFabric.getIntDents()
                    +"\n"+objDictionaryAction.getWord("TPD")+": "+objFabric.getIntTPD()
                    +"\n"+objDictionaryAction.getWord("HPI")+": "+objFabric.getIntHPI()
                    +"\n"+objDictionaryAction.getWord("EPI")+": "+objFabric.getIntEPI()
                    +"\n"+objDictionaryAction.getWord("PPI")+": "+objFabric.getIntPPI()
                    +"\n"+objDictionaryAction.getWord("GRAPHSIZE")+": "+objFabric.getObjConfiguration().getStrGraphSize()
                    +"\n"+objDictionaryAction.getWord("DATE")+": "+objFabric.getStrFabricDate()
            );
            lblStatus.setText(objDictionaryAction.getWord("ACTIONINFODESCRIPTIONUTILITY"));
        } else{
            lblMsg = new Label(objDictionaryAction.getWord("NOCLOTHPART"));
            lblStatus.setText(objDictionaryAction.getWord("NOCLOTHPART"));
        }
        lblMsg.setAlignment(Pos.TOP_LEFT);
        popup.add(lblMsg, 0, 0);
        Button btnYes = new Button(objDictionaryAction.getWord("CANCEL"));
        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close  ();
                System.gc();
            }
        });
        popup.add(btnYes, 1, 1);
        Scene scene = new Scene(popup);
        scene.getStylesheets().add(ClothView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        dialogStage.setScene(scene);
        dialogStage.setTitle(objDictionaryAction.getWord("PROJECT"));
        dialogStage.showAndWait();
        System.gc();
    }
 
    @Override
    public void start(Stage stage) throws Exception {
        stage.initOwner(WindowView.windowStage);
        new ClothView(stage); 
        new Logging("WARNING",ClothView.class.getName(),"UnsupportedOperationException",new UnsupportedOperationException("Not supported yet."));
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
/*=========== U N U S E D ===== C O D E ===== S T A R T ===== H E R E ===========*/    
    private void resetWidthHeight(String componentName){
        int totalWidth=(int)(objConfiguration.WIDTH);
        int totalHeight=(int)(48*inchFactor*zoomFactor);
        
        // if H of Left/Right Border Changes
        if(borderLinkPolicy){
            if(componentName.equalsIgnoreCase("Left Border"))
                // left border height is modified, ripple effect on Right Border if enabled
                if(rightBorderEnabled)
                    heights[10]=heights[0];
            if(componentName.equalsIgnoreCase("Right Border"))
                // right border height is modified, ripple effect on Left Border if enabled
                if(leftBorderEnabled)
                    heights[0]=heights[10];
        }
        
        if(crossBorderLinkPolicy){
            if(componentName.equalsIgnoreCase("Leftside Left Cross Border")){
                // left Cross Border width is modified, ripple effect on other Cross Borders
                if(leftsideRightCrossBorderEnabled)
                    widths[4]=widths[2];
                if(rightsideLeftCrossBorderEnabled)
                    widths[7]=widths[2];
                if(rightsideRightCrossBorderEnabled)
                    widths[9]=widths[2];
            }
            if(componentName.equalsIgnoreCase("Leftside Right Cross Border")){
                // right Cross Border width is modified, ripple effect on other Cross Borders
                if(leftsideLeftCrossBorderEnabled)
                    widths[2]=widths[4];
                if(rightsideLeftCrossBorderEnabled)
                    widths[7]=widths[4];
                if(rightsideRightCrossBorderEnabled)
                    widths[9]=widths[4];
            }
            if(componentName.equalsIgnoreCase("Rightside Left Cross Border")){
                // Rightside Left Cross Border width is modified, ripple effect on other Cross Borders
                if(leftsideLeftCrossBorderEnabled)
                    widths[2]=widths[7];
                if(leftsideRightCrossBorderEnabled)
                    widths[4]=widths[7];
                if(rightsideRightCrossBorderEnabled)
                    widths[9]=widths[7];
            }
            if(componentName.equalsIgnoreCase("Rightside Right Cross Border")){
                // left Cross Border width is modified, ripple effect on other Cross Borders
                if(leftsideLeftCrossBorderEnabled)
                    widths[2]=widths[9];
                if(leftsideRightCrossBorderEnabled)
                    widths[4]=widths[9];
                if(rightsideLeftCrossBorderEnabled)
                    widths[7]=widths[9];
            }
        }
        
        if(palluLinkPolicy){
            if(componentName.equalsIgnoreCase("Left Pallu"))
                // left pallu width is modified, ripple effect on Right Pallu if enabled
                if(rightPalluEnabled)
                    widths[8]=widths[3];
            if(componentName.equalsIgnoreCase("Right Border"))
                // right pallu width is modified, ripple effect on Left Pallu if enabled
                if(leftPalluEnabled)
                    widths[3]=widths[8];
        }
        
        if(blouseEnabled)
            heights[1]=totalHeight-(heights[0]+heights[10]);
        if(leftsideLeftCrossBorderEnabled)
            heights[2]=totalHeight-(heights[0]+heights[10]);
        if(leftPalluEnabled)
            heights[3]=totalHeight-(heights[0]+heights[10]);
        if(leftsideRightCrossBorderEnabled)
            heights[4]=totalHeight-(heights[0]+heights[10]);
        if(rightsideLeftCrossBorderEnabled)
            heights[7]=totalHeight-(heights[0]+heights[10]);
        if(rightPalluEnabled)
            heights[8]=totalHeight-(heights[0]+heights[10]);
        if(rightsideRightCrossBorderEnabled)
            heights[9]=totalHeight-(heights[0]+heights[10]);
        
        // Body (always enabled)
        heights[5]=totalHeight-(heights[0]+heights[10]+heights[6]);
        widths[5]=totalWidth-(widths[1]+widths[2]+widths[3]+widths[4]+widths[7]+widths[8]+widths[9]);
        
        if(skirtEnabled)
            widths[6]=widths[5];
    }
    private void recalculateBody(){
        int totalWidth=(int)objConfiguration.WIDTH;
        int totalHeight=(int)(48*inchFactor*zoomFactor);
        // Body (always enabled)
        heights[5]=totalHeight-(heights[0]+heights[10]+heights[6]);
        widths[5]=totalWidth-(widths[1]+widths[2]+widths[3]+widths[4]+widths[7]+widths[8]+widths[9]);
        
        if(skirtEnabled)
            widths[6]=widths[5];
        if(leftBorderEnabled)
            widths[0]=totalWidth;
        if(rightBorderEnabled)
            widths[10]=totalWidth;
    }
    /**
     * This functions sets ripple effect on other components W/H, based on updated W/H of a given component
     * @added 30 August 2017
     * @author Aatif Ahmad Khan
     * @since 0.8.3
     */
    private boolean validateWidthHeight(){
        /* initial widths and heights values are already set
        , now as one or more W/H changes, these values are recalculated
        This function assumes W/H of disabled components is 0 */
        
        // min/max height of Borders (width will always be objConfiguration.WIDTH if enabled)
        if(leftBorderEnabled){
            if(heights[0]<MINSIZE||heights[0]>MAXBORDER){
                return false;
            }
        }
        if(rightBorderEnabled){
            if(heights[10]<MINSIZE||heights[10]>MAXBORDER){
                return false;
            }
        }
        // min/max height of Skirt (width will be calculated dynamically)
        if(skirtEnabled){
            if(heights[6]<MINSIZE||heights[6]>MAXBORDER){
                return false;
            }
            if(widths[6]<MINSIZE){
                return false;
            }
        }
        
        // min/max width of Cross Borders (height will be calculated dynamically)
        if(leftsideLeftCrossBorderEnabled){
            if(widths[2]<MINSIZE||widths[2]>MAXBORDER){
                return false;
            }
        }
        if(leftsideRightCrossBorderEnabled){
            if(widths[4]<MINSIZE||widths[4]>MAXBORDER){
                return false;
            }
        }
        if(rightsideLeftCrossBorderEnabled){
            if(widths[7]<MINSIZE||widths[7]>MAXBORDER){
                return false;
            }
        }
        if(rightsideRightCrossBorderEnabled){
            if(widths[9]<MINSIZE||widths[9]>MAXBORDER){
                return false;
            }
        }
        
        // min/max width of Pallus (height will be calculated dynamically)
        if(leftPalluEnabled){
            if(widths[3]<MINSIZE||widths[3]>MAXPALLU){
                return false;
            }
        }
        if(rightPalluEnabled){
            if(widths[8]<MINSIZE||widths[8]>MAXPALLU){
                return false;
            }
        }
        // min/max width of blouse (height will be calculated dynamically)
        if(blouseEnabled){
            if(widths[1]<MINSIZE||widths[1]>MAXPALLU){
                return false;
            }
        }
        /* body<MINSIZE
        if(heights[5]<100||widths[5]<100){
            System.err.println("Body size violation, it's width or height should not be less than 100 pixel.");
            return;
        }*/
        return true;
    }
/**
     * This function is needed to eliminate fractional repeats in components.
     * @param newSize this is width/height of a repeat from assignFabricToComponent()
     * @param componentSize this is width/height of component
     * @return updated width/height so as to produce integral repeats
     */
    private int roundIntegralRepeatSize(int newSize, int componentSize){
        if(newSize > componentSize){
            /* If size of repeat exceeds component, fit one repeat only to component */
            return componentSize;
        }
        else{
            /* As repeats > 1 (may be 1.1 etc.), need to round to nearest integer */
            double dblRepeats = (double)componentSize/(double)newSize;
            int intRepeats = (int) Math.round(dblRepeats);
            return (int)(componentSize/intRepeats);
        }
    }
    private BufferedImage plotCompositeView(Fabric objFabric, int intHeight, int intLength){
        BufferedImage bufferedImage = new BufferedImage(objFabric.getIntWeft(), objFabric.getIntWarp(), BufferedImage.TYPE_INT_RGB);
        try {
            lblStatus.setText(objDictionaryAction.getWord("GETCOMPOSITEVIEW"));
            objFabricAction = new FabricAction();
            bufferedImage = objFabricAction.plotFrontSideView(objFabric, objFabric.getIntWarp(), objFabric.getIntWeft(), intLength, intHeight);
            /*if((objFabric.getIntEPI()/objFabric.getIntPPI())<=0){
                intHeight = (int)(intLength*(objFabric.getIntPPI()/objFabric.getIntEPI())*zoomFactor);
                intLength = (int)(intLength*zoomFactor);
            }else{
                intLength = (int)(intHeight*(objFabric.getIntEPI()/objFabric.getIntPPI())*zoomFactor);
                intHeight = (int)(intHeight*zoomFactor);
            }*/
            intLength = (int)(((objConfiguration.getIntDPI()*intLength)/objFabric.getIntEPI()));
            intHeight = (int)(((objConfiguration.getIntDPI()*intHeight)/objFabric.getIntPPI()));
            BufferedImage bufferedImageesize = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bufferedImageesize.createGraphics();
            g.drawImage(bufferedImage, 0, 0, intLength, intHeight, null);
            g.dispose();
            bufferedImage = bufferedImageesize;
        } catch (SQLException ex) {
            new Logging("SEVERE",ClothView.class.getName(),"SQLException: plotCompositeView() : Error while viewing composte view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",ClothView.class.getName(),"Exception: plotCompositeView() : Error while viewing composte view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
        return bufferedImage;
    }
/*=========== U N U S E D ===== C O D E ===== E N D ===== H E R E ===========*/    
}