/*
 * Copyright (C) 2018 HP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.mla.simulator;

import com.mla.main.DbConnect;
import com.mla.main.IDGenerator;
import com.mla.main.Logging;
import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @Designing model function for utility
 * @author Amit Kumar Singh
 * 
 */
public class SimulatorAction {
 
    Connection connection = null; //DbConnect.getConnection();
    
    public SimulatorAction() throws SQLException{
        connection = DbConnect.getConnection();
    }
    
    public void close() throws SQLException{
        connection.close();
    }
    
    /**************************** Fabric Simulation Base ******************************************/
    public boolean setBaseFabricSimultion(Simulator objSimulator) {
        PreparedStatement oPreparedStatement =null;
        ResultSet oResultSet= null;
        byte oResult= 0;
        String strQuery=null;
        new Logging("INFO",SimulatorAction.class.getName(),"<<<<<<<<<<< setBaseFabricSimultion() >>>>>>>>>>>",null);
        try {           
            strQuery = "INSERT INTO `mla_fabric_base_library` (`ID`, `NAME`, `FABRICTYPE`, `EPI`, `PPI`, `DPI`, `YARNID`, `ICON`, `ACCESS`, `USERID`) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
            oPreparedStatement = connection.prepareStatement(strQuery);
            oPreparedStatement.setString(1, objSimulator.getStrBaseFSID());
            oPreparedStatement.setString(2, objSimulator.getStrBaseFSName());
            oPreparedStatement.setString(3, objSimulator.getStrBaseFSType());
            oPreparedStatement.setInt(4, objSimulator.getIntEPI());
            oPreparedStatement.setInt(5, objSimulator.getIntPPI());
            oPreparedStatement.setInt(6, objSimulator.getIntDPI());
            oPreparedStatement.setString(7, objSimulator.getStrYarnID());
            oPreparedStatement.setBinaryStream(8,new ByteArrayInputStream(objSimulator.getBytBaseFSIcon()),objSimulator.getBytBaseFSIcon().length);
            oPreparedStatement.setString(9, new IDGenerator().setUserAcessKey("FABRIC_BASE_LIBRARY",objSimulator.getObjConfiguration().getObjUser()));
            oPreparedStatement.setString(10, objSimulator.getObjConfiguration().getObjUser().getStrUserID());
            oResult = (byte)oPreparedStatement.executeUpdate();
        } catch (Exception ex) {
            new Logging("SEVERE",SimulatorAction.class.getName(),"setBaseFabricSimultion : "+strQuery,ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oPreparedStatement!=null) {
                    oPreparedStatement.close();
                    oPreparedStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",SimulatorAction.class.getName(),"setBaseFabricSimultion() : Error while closing connection"+e,ex);
                }
            }
        }
        System.out.println("<<<<<< setBaseFabricSimultion >>>>>>");
        if(oResult==0)
            return false;
        else
            return true;
    }
    
    public boolean resetBaseFabricSimultion(Simulator objSimulator) {
        PreparedStatement oPreparedStatement =null;
        ResultSet oResultSet= null;
        byte oResult= 0;
        String strQuery=null;
        new Logging("INFO",SimulatorAction.class.getName(),"<<<<<<<<<<< resetBaseFabricSimultion() >>>>>>>>>>>",null);
        try {           
            strQuery = "UPDATE `mla_fabric_base_library` SET `NAME`=?, `FABRICTYPE`=?, `EPI`=?, `PPI`=?, `DPI`=?, `YARNID`=?, `ICON`=?, `ACCESS`=? WHERE `ID`=?;";
            oPreparedStatement = connection.prepareStatement(strQuery);
            oPreparedStatement.setString(1, objSimulator.getStrBaseFSName());
            oPreparedStatement.setString(2, objSimulator.getStrBaseFSType());
            oPreparedStatement.setInt(3, objSimulator.getIntEPI());
            oPreparedStatement.setInt(4, objSimulator.getIntPPI());
            oPreparedStatement.setInt(5, objSimulator.getIntDPI());
            oPreparedStatement.setString(6, objSimulator.getStrYarnID());
            oPreparedStatement.setBinaryStream(7,new ByteArrayInputStream(objSimulator.getBytBaseFSIcon()),objSimulator.getBytBaseFSIcon().length);
            //oPreparedStatement.setString(8, new IDGenerator().setUserAcessKey("FABRIC_LIBRARY",objSimulator.getObjConfiguration().getObjUser()));
            oPreparedStatement.setString(8, objSimulator.getStrBaseFSAccess());
            oPreparedStatement.setString(9, objSimulator.getStrBaseFSID());
            oResult = (byte)oPreparedStatement.executeUpdate();
        } catch (Exception ex) {
            new Logging("SEVERE",SimulatorAction.class.getName(),"resetBaseFabricSimultion : "+strQuery,ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oPreparedStatement!=null) {
                    oPreparedStatement.close();
                    oPreparedStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",SimulatorAction.class.getName(),"resetBaseFabricSimultion() : Error while closing connection"+e,ex);
                }
            }
        }
        System.out.println("<<<<<< resetBaseFabricSimultion >>>>>>");
        if(oResult==0)
            return false;
        else
            return true;
    }
    public void getBaseFabricSimultion(Simulator objSimulator) {
        Statement oStatement =null;
        ResultSet oResultSet= null;
        String strQuery=null;
        try {           
            System.out.println("<<<<<< getBaseFabricSimultion >>>>>>");
            strQuery = "select * from mla_fabric_base_library WHERE ID='"+objSimulator.getStrBaseFSID()+"';";
            System.out.println(strQuery);
            oStatement = connection.createStatement();
            oResultSet = oStatement.executeQuery(strQuery);  
            oResultSet.next();
            objSimulator.setStrBaseFSID(oResultSet.getString("ID"));
            objSimulator.setStrBaseFSName(oResultSet.getString("NAME"));            
            objSimulator.setStrBaseFSType(oResultSet.getString("FABRICTYPE"));
            objSimulator.setIntEPI(oResultSet.getInt("EPI"));
            objSimulator.setIntPPI(oResultSet.getInt("PPI"));
            objSimulator.setIntDPI(oResultSet.getInt("DPI"));
            objSimulator.setStrYarnID(oResultSet.getString("YARNID"));
            objSimulator.setBytBaseFSIcon(oResultSet.getBytes("ICON"));
            objSimulator.setStrBaseFSDate(oResultSet.getString("UDATE"));
            objSimulator.setStrBaseFSAccess(oResultSet.getString("ACCESS"));
        } catch (Exception ex) {
            new Logging("SEVERE",SimulatorAction.class.getName(),"getBaseFabricSimultion : "+strQuery,ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oStatement!=null) {
                    oStatement.close();
                    oStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",SimulatorAction.class.getName(),"getBaseFabricSimultion() : Error while closing connection"+e,ex);
                }
            }
        }
        System.out.println("<<<<<< getBaseFabricSimultion >>>>>>");
        return;
    }
    public boolean clearBaseFabricSimultion(String strBaseFSID) {
        PreparedStatement oPreparedStatement =null;
        ResultSet oResultSet= null;
        boolean oResult= false;
        String strQuery=null;
        String yarnId = null;
        try {           
            System.out.println("<<<<<< clearBaseFabricSimultion >>>>>>");
            strQuery = "DELETE FROM `mla_fabric_base_library` WHERE `ID` = ?;";
            oPreparedStatement = connection.prepareStatement(strQuery);
            oPreparedStatement.setString(1, strBaseFSID);
            oResult = oPreparedStatement.execute();
        } catch (Exception ex) {
            new Logging("SEVERE",SimulatorAction.class.getName(),"clearBaseFabricSimultion : "+strQuery,ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oPreparedStatement!=null) {
                    oPreparedStatement.close();
                    oPreparedStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",SimulatorAction.class.getName(),"clearBaseFabricSimultion() : Error while closing connection"+e,ex);
                }
            }
        }
        System.out.println("<<<<<< clearBaseFabricSimultion >>>>>>");
        return oResult;
    }
    public Simulator[] lstBaseFabricSimultion(Simulator objSimulator) {
        Statement oStatement =null;
        ResultSet oResultSet= null;
        String strQuery=null;
        Simulator[] lstBaseFSDeatails=null;
        new Logging("INFO",SimulatorAction.class.getName(),"<<<<<<<<<<< lstBaseFabricSimultion() >>>>>>>>>>>",null);
        try {
            String cond = "(USERID = '"+objSimulator.getObjConfiguration().getObjUser().getStrUserID()+"' OR `ACCESS`='"+new IDGenerator().getUserAcess("FABRIC_BASE_LIBRARY")+"') ";
            String orderBy ="NAME ";
            if(!(objSimulator.getStrBaseFSName().trim().equals(""))) {
                cond += " AND `NAME` LIKE '"+objSimulator.getStrBaseFSName().trim()+"%'";
            }
            if(objSimulator.getIntEPI()>0) {
                cond += " AND `EPI` LIKE '"+objSimulator.getIntEPI()+"%'";
            }
            if(objSimulator.getIntPPI()>0) {
                cond += " AND `PPI` LIKE '"+objSimulator.getIntPPI()+"%'";
            }
            if(!(objSimulator.getStrBaseFSType().trim().equals("")) && !(objSimulator.getStrBaseFSType().trim().equalsIgnoreCase("All"))) {
                cond += " AND `FABRICTYPE` LIKE '%"+objSimulator.getStrSearchBy().trim()+"%'";
            }
            if(objSimulator.getStrOrderBy().equals("Name")) {
                orderBy = "`NAME`";
            } else if(objSimulator.getStrOrderBy().equals("Date")) {
                orderBy = "`UDATE`";
            }
            if(objSimulator.getStrDirection().equals("Ascending")) {
                orderBy += " ASC";
            } else if(objSimulator.getStrDirection().equals("Descending")) {
                orderBy += " DESC";
            }
            strQuery = "select * from mla_fabric_base_library WHERE "+cond+" ORDER BY "+orderBy;
            //System.out.println(strQuery);
            oStatement = connection.createStatement();
            oResultSet = oStatement.executeQuery(strQuery);
            oResultSet.last();
            System.out.println("Row= "+oResultSet.getRow());
            lstBaseFSDeatails = new Simulator[oResultSet.getRow()];
            oResultSet.beforeFirst();
            int count=0;
            while(oResultSet.next()) {
                objSimulator = new Simulator(
                oResultSet.getString("ID").toString(),
                oResultSet.getString("NAME"),
                oResultSet.getString("FABRICTYPE"),
                oResultSet.getString("YARNID"),
                oResultSet.getInt("PPI"),
                oResultSet.getInt("EPI"),
                oResultSet.getInt("DPI"),
                oResultSet.getBytes("ICON"),
                oResultSet.getTimestamp("UDATE").toString(),
                oResultSet.getString("USERID").toString(),
                oResultSet.getString("ACCESS"));
                lstBaseFSDeatails[count++]=objSimulator;
              //  System.out.println("Fabric Data Model : "+oResultSet.getString("ID").toString());
            }
        } catch (Exception ex) {
            //Logger.getLogger(SimulatorImportView.class.getName()).log(Level.SEVERE, null, ex);
            new Logging("SEVERE",SimulatorAction.class.getName(),"lstBaseFabricSimultion : "+strQuery,ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oStatement!=null) {
                    oStatement.close();
                    oStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",SimulatorAction.class.getName(),"lstBaseFabricSimultion() : Error while closing connection"+e,ex);
                }
            }
        }
        new Logging("INFO",SimulatorAction.class.getName(),">>>>>>>>>>> lstBaseFabricSimultion() <<<<<<<<<<<"+strQuery,null);
        return lstBaseFSDeatails;
    }
    
    public Simulator[] retBaseFabricSimultion(Simulator objSimulator) {
        Statement oStatement =null;
        ResultSet oResultSet= null;
        String strQuery=null;
        String id=null, name=null, fabType=null, yarn=null, access=null, date=null, userid=null;
        int pp=0, ep=0, dp=0;
        byte[] icon=null;
        Simulator[] fbs=null;
        new Logging("INFO",SimulatorAction.class.getName(),"<<<<<<<<<<< lstBaseFabricSimultion() >>>>>>>>>>>",null);
        try {
            /*String cond = "(USERID = '"+objSimulator.getObjConfiguration().getObjUser().getStrUserID()+"' OR `ACCESS`='"+new IDGenerator().getUserAcess("BASE_SIMULATION_LIBRARY")+"') ";
            String orderBy ="NAME ";
            if(!(objSimulator.getStrCondition().trim().equals(""))) {
                cond += " AND `NAME` LIKE '"+objSimulator.getStrCondition().trim()+"%'";
            }
            if(!(objSimulator.getStrSearchBy().trim().equals("")) && !(objSimulator.getStrSearchBy().trim().equalsIgnoreCase("All"))) {
                cond += " AND `FABRICTYPE` LIKE '%"+objSimulator.getStrSearchBy().trim()+"%'";
            }
            if(objSimulator.getStrOrderBy().equals("Name")) {
                orderBy = "`NAME`";
            } else if(objSimulator.getStrOrderBy().equals("Date")) {
                orderBy = "`UDATE`";
            }
            if(objSimulator.getStrDirection().equals("Ascending")) {
                orderBy += " ASC";
            } else if(objSimulator.getStrDirection().equals("Descending")) {
                orderBy += " DESC";
            }
            strQuery = "select * from mla_fabric_base_library WHERE "+cond+" ORDER BY "+orderBy;*/
            String cond = "(USERID = '"+objSimulator.getObjConfiguration().getObjUser().getStrUserID()+"' OR `ACCESS`='"+new IDGenerator().getUserAcess("FABRIC_BASE_LIBRARY")+"') ";
            String orderBy ="NAME ";
            if(!(objSimulator.getStrBaseFSName().trim().equals(""))) {
                cond += " AND `NAME` LIKE '"+objSimulator.getStrBaseFSName().trim()+"%'";
            }
            if(objSimulator.getIntEPI()>0) {
                cond += " AND `EPI` LIKE '"+objSimulator.getIntEPI()+"%'";
            }
            if(objSimulator.getIntPPI()>0) {
                cond += " AND `PPI` LIKE '"+objSimulator.getIntPPI()+"%'";
            }
            if(!(objSimulator.getStrBaseFSType().trim().equals("")) && !(objSimulator.getStrBaseFSType().trim().equalsIgnoreCase("All"))) {
                cond += " AND `FABRICTYPE` LIKE '%"+objSimulator.getStrSearchBy().trim()+"%'";
            }
            if(objSimulator.getStrOrderBy().equals("Name")) {
                orderBy = "`NAME`";
            } else if(objSimulator.getStrOrderBy().equals("Date")) {
                orderBy = "`UDATE`";
            }
            if(objSimulator.getStrDirection().equals("Ascending")) {
                orderBy += " ASC";
            } else if(objSimulator.getStrDirection().equals("Descending")) {
                orderBy += " DESC";
            }
            strQuery = "select * from mla_fabric_base_library WHERE "+cond+" ORDER BY "+orderBy;
            oStatement = connection.createStatement();
            oResultSet = oStatement.executeQuery(strQuery);
            oResultSet.last();
            System.out.println("Row= "+oResultSet.getRow());
            fbs = new Simulator[oResultSet.getRow()];
            oResultSet.beforeFirst();
            int i = 0;
            while(oResultSet.next()) {
                id=oResultSet.getString("ID").toString();
                name=oResultSet.getString("NAME");
                fabType=oResultSet.getString("FABRICTYPE");
                ep=oResultSet.getInt("EPI");
                pp=oResultSet.getInt("PPI");
                dp=oResultSet.getInt("DPI");
                yarn=oResultSet.getString("YARNID");
                icon=oResultSet.getBytes("ICON");
                access=oResultSet.getString("ACCESS");
                date=oResultSet.getTimestamp("UDATE").toString();
                userid=oResultSet.getString("USERID").toString();
                fbs[i]=new Simulator(id, name, fabType, yarn, pp, ep, dp, icon, date, userid, access);
                i++;
                System.out.println("Fabric Data Model : "+oResultSet.getString("ID").toString());
            }
        } catch (Exception ex) {
            new Logging("SEVERE",SimulatorAction.class.getName(),"lstBaseFabricSimultion : "+strQuery,ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oStatement!=null) {
                    oStatement.close();
                    oStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",SimulatorAction.class.getName(),"lstBaseFabricSimultion() : Error while closing connection"+e,ex);
                }
            }
        }
        new Logging("INFO",SimulatorAction.class.getName(),">>>>>>>>>>> lstBaseFabricSimultion() <<<<<<<<<<<"+strQuery,null);
        return fbs;
    }
}