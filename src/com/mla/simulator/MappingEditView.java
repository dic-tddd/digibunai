/*
 * Copyright (C) Digital India Corporation (Media Lab Asia)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.mla.simulator;

import com.mla.artwork.ArtworkAction;
import com.mla.dictionary.DictionaryAction;
import com.mla.main.Configuration;
import com.mla.main.Logging;
import com.sun.media.jai.codec.ByteArraySeekableStream;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.SeekableStream;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.Bloom;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.effect.SepiaTone;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.imageio.ImageIO;
import javax.media.jai.PlanarImage;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;
/**
 * MappingEditView Class
 * <p>
 * This class is used for defining UI for artwork assignment in fabric editor.
 *
 * @author Amit Kumar Singh
 * @version %I%, %G%
 * @since   1.0
 * @date 07/01/2016
 * @Designing UI class for artwork assignment in fabric editor.
 * @see java.stage.*;
 * @link com.mla.fabric.FabricView
 */
public class MappingEditView {
 
    Simulator objSimulator;
    DictionaryAction objDictionaryAction;
    Configuration objConfiguration;
    
    private Stage mappingStage;
    private BorderPane root;    
    private Scene scene;    
    private Label lblStatus;
    ProgressBar progressB;
    ProgressIndicator progressI;
    
    private StackPane container;
    private ImageView mappingIV;
    private HBox editToolPane;
    private ScrollPane rightPane;
    
    BufferedImage bufferedImage;
    /**
     * MappingEditView
     * <p>
     * This constructor is used for UX initialization.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   constructor is used for UX initialization.
     * @see         javafx.stage.*;
     * @link        com.mla.fabric.FabricView
     * @throws      SQLException
     * @param       objConfigurationCall Object of Configuration Class
     */        
    public MappingEditView(Configuration objConfigurationCall, BufferedImage objBufferedImage) {   
        this.objConfiguration = objConfigurationCall;
        this.bufferedImage = objBufferedImage;
        
        objDictionaryAction = new DictionaryAction(objConfiguration);

        mappingStage = new Stage();
        mappingStage.initModality(Modality.APPLICATION_MODAL);//WINDOW_MODAL
        mappingStage.initStyle(StageStyle.UTILITY);
        
        root = new BorderPane();
        //root.setId("popup");
        scene = new Scene(root, objConfiguration.WIDTH, objConfiguration.HEIGHT, Color.WHITE);
        scene.getStylesheets().add(MappingEditView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        
        HBox footContainer = new HBox();
        progressB = new ProgressBar(0);
        progressB.setVisible(false);
        progressI = new ProgressIndicator(0);
        progressI.setVisible(false);
        lblStatus = new Label(objDictionaryAction.getWord("WELCOMETOCADTOOL"));
        lblStatus.setId("message");
        footContainer.getChildren().addAll(lblStatus,progressB,progressI);
        footContainer.setId("footContainer");
        root.setBottom(footContainer);
        
        container = new StackPane();
        //container.setId("subpopup");
        //container.setPrefSize(objConfiguration.WIDTH,objConfiguration.HEIGHT*2/3);
        container.setPrefWidth(objConfiguration.WIDTH);
        //container.setPrefSize(root.getWidth(),root.getHeight());
        mappingIV = new ImageView();
        //mappingIV.setImage(SwingFXUtils.toFXImage(bufferedImage, null));
        mappingIV.setFitHeight(objConfiguration.HEIGHT-173);//editToolPane(123)+bottom+space(40)
        mappingIV.setPreserveRatio(true);
        //mappingIV.setFitWidth(objConfiguration.WIDTH);
        container.getChildren().add(mappingIV);
        root.setCenter(container);
        
        ScrollPane rightPane = new ScrollPane();
        //rightPane.setPrefWidth(objConfiguration.WIDTH);
        rightPane.setPrefSize(objConfiguration.WIDTH,123);
        editToolPane = new HBox();
        editToolPane.setSpacing(5);
        editToolPane.setPrefWidth(objConfiguration.WIDTH);
        editToolPane.setPrefHeight(123);
        
        rightPane.setContent(editToolPane);
        root.setTop(rightPane);
        
        populateEditPane();
        
        mappingStage.setScene(scene);
        mappingStage.getIcons().add(new Image("/media/icon.png"));
        mappingStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("MAPPING")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
        mappingStage.setIconified(false);
        mappingStage.setResizable(false);        
        mappingStage.setX(-5);
        mappingStage.setY(0);
        mappingStage.showAndWait();
    } 
    
    public void plotSimulationView(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("GETSIMULATION2DVIEW"));
            int intHeight = bufferedImage.getHeight();
            int intLength = bufferedImage.getWidth();
            
            BufferedImage srcImage = ImageIO.read(new File(System.getProperty("user.dir")+"/mla/dobby/red_tshirt.jpg")); //colored red_tshirt white-cloth green-fabric
                                
            // resize image by tilling
            BufferedImage myImage = new BufferedImage((int)(intLength), (int)(intHeight),BufferedImage.TYPE_INT_RGB);
            /*
            Graphics2D g = myImage.createGraphics();
            g.drawImage(srcImage, 0, 0, (int)(intLength), (int)(intHeight), null);
            g.dispose();
            srcImage = null;
            */            
            for(int i = 0; i < intHeight; i++) {
                for(int j = 0; j < intLength; j++) {
                    if(i>=srcImage.getHeight() && j<srcImage.getWidth()){
                        myImage.setRGB(j, i, srcImage.getRGB(j, i%srcImage.getHeight()));
                    }else if(i<srcImage.getHeight() && j>=srcImage.getWidth()){
                        myImage.setRGB(j, i, srcImage.getRGB(j%srcImage.getWidth(), i));
                    }else if(i>=srcImage.getHeight() && j>=srcImage.getWidth()){
                        myImage.setRGB(j, i, srcImage.getRGB(j%srcImage.getWidth(), i%srcImage.getHeight()));
                    }else{
                        myImage.setRGB(j, i, srcImage.getRGB(j, i));
                    }
                }
            }
            srcImage = null;
            
            BufferedImage mappingImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            int rgb = 0;
            for(int x = 0; x < intHeight; x++) {
                for(int y = 0; y < intLength; y++) {
                    int pixel = myImage.getRGB(y, x);
                    int alpha = (pixel >> 24) & 0xff;
                    int red   = (pixel >>16) & 0xff;
                    int green = (pixel >> 8) & 0xff;
                    int blue  =  pixel & 0xff;
                    //if(red!=255 || green!=255 || blue!=255){
                    // Convert RGB to HSB
                    float[] hsb = java.awt.Color.RGBtoHSB(red, green, blue, null);
                    float hue = hsb[0];
                    float saturation = hsb[1];
                    float brightness = hsb[2];
                    hsb = null;

                    int npixel = bufferedImage.getRGB(y, x);
                    int nalpha = (npixel >> 24) & 0xff;
                    int nred   = (npixel >>16) & 0xff;
                    int ngreen = (npixel >> 8) & 0xff;
                    int nblue  =  npixel & 0xff;

                    hsb = java.awt.Color.RGBtoHSB(nred, ngreen, nblue, null);

                    hue = hsb[0];
                    saturation = hsb[1];
                    // Convert HSB to RGB value
                    rgb = java.awt.Color.HSBtoRGB(hue, saturation, brightness);
                    //}else{
                    //  rgb = pixel;
                    //}
                    mappingImage.setRGB(y, x, rgb);
                    /*
                    int pixels = mappingImage.getRGB(y,x);
                    pixels = (pixels & 0xff) >>> 6;
                    mappingImage.setRGB(y, x, pixels);
                     */
                }
            }
            
            ImageIO.write(mappingImage, "png", new File(System.getProperty("user.dir")+"/mla/temp/mapping.png"));

            mappingIV.setImage(SwingFXUtils.toFXImage(mappingImage, null));
            mappingImage=null;
            lblStatus.setText(objDictionaryAction.getWord("GOTSIMULATION2DVIEW"));                
            
        } catch (IOException ex) {
            new Logging("SEVERE",MappingEditView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",MappingEditView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }     
    }
    
    
    /**
     * colorPanel
     * <p>
     * This method is used for creating UX for color panel.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for creating UX for color panel.
     * @link        com.mla.fabric.Fabric
     * @link        com.mla.main.Logging
     */
    public void populateEditPane(){
        editToolPane.setVisible(true);
        editToolPane.getChildren().clear();
        System.gc();
        try {
            File[] files = new File(System.getProperty("user.dir")+"/mla/dobby").listFiles();
            if(files.length==0){
                editToolPane.getChildren().add(new Label(objDictionaryAction.getWord("NOVALUE")));
            }else{
                for (int i=0, j = files.length; i<j; i++){
                    //System.out.println(i+":"+j);
                    //System.err.println("name: "+files[i].getName());
                    File file = files[i];
                    BufferedImage srcImage = ImageIO.read(file);
                    Image image=SwingFXUtils.toFXImage(srcImage, null);
                    final ImageView imageView = new ImageView(image);
                    imageView.setFitHeight(111);
                    imageView.setFitWidth(111);
                    imageView.setId(file.getAbsolutePath());
                    imageView.setUserData(file);
                    String strTooltip =
                            objDictionaryAction.getWord("NAME")+": "+file.getName();//+"\n"+
                            //objDictionaryAction.getWord("PATH")+": "+file.getPath();
                    Tooltip toolTip = new Tooltip(strTooltip);
                    Tooltip.install(imageView, toolTip);
                    //System.out.println(i+":"+j);
                    
                    imageView.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            try {
                                BufferedImage srcImage = ImageIO.read(new File(imageView.getId()));//System.getProperty("user.dir")+"/mla/dobby/red_tshirt.jpg")); //colored red_tshirt white-cloth green-fabric
                                int intLength = srcImage.getWidth();
                                int intHeight = srcImage.getHeight();
                                
                                // resize image by tilling
                                BufferedImage destImage = new BufferedImage(intLength, intHeight,BufferedImage.TYPE_INT_RGB);
                                /*
                                Graphics2D g = destImage.createGraphics();
                                g.drawImage(srcImage, 0, 0, (int)(intLength), (int)(intHeight), null);
                                g.dispose();
                                srcImage = null;
                                */            
                                for(int i = 0; i < intHeight; i++) {
                                    for(int j = 0; j < intLength; j++) {
                                        if(i>=bufferedImage.getHeight() && j<bufferedImage.getWidth()){
                                            destImage.setRGB(j, i, bufferedImage.getRGB(j, i%bufferedImage.getHeight()));
                                        }else if(i<bufferedImage.getHeight() && j>=bufferedImage.getWidth()){
                                            destImage.setRGB(j, i, bufferedImage.getRGB(j%bufferedImage.getWidth(), i));
                                        }else if(i>=bufferedImage.getHeight() && j>=bufferedImage.getWidth()){
                                            destImage.setRGB(j, i, bufferedImage.getRGB(j%bufferedImage.getWidth(), i%bufferedImage.getHeight()));
                                        }else{
                                            destImage.setRGB(j, i, bufferedImage.getRGB(j, i));
                                        }
                                    }
                                }
                                
                                BufferedImage mappingImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
                                int pix = srcImage.getRGB(0, 0);
                                int a = (pix >> 24) & 0xff;
                                int r = (pix >>16) & 0xff;
                                int g = (pix >> 8) & 0xff;
                                int b =  pix & 0xff;
                                int rgb =0;
                                
                                for(int x = 0; x < intHeight; x++) {
                                    for(int y = 0; y < intLength; y++) {
                                        int pixel = srcImage.getRGB(y, x);
                                        int alpha = (pixel >> 24) & 0xff;
                                        int red   = (pixel >>16) & 0xff;
                                        int green = (pixel >> 8) & 0xff;
                                        int blue  =  pixel & 0xff;
                                        
                                        int npixel = destImage.getRGB(y, x);
                                        int nalpha = (npixel >> 24) & 0xff;
                                        int nred   = (npixel >>16) & 0xff;
                                        int ngreen = (npixel >> 8) & 0xff;
                                        int nblue  =  npixel & 0xff;

                                        //if(red!=255 || green!=255 || blue!=255){
                                        if(pixel!=pix){
                                            // Convert RGB to HSB
                                            float[] hsb = java.awt.Color.RGBtoHSB(red, green, blue, null);
                                            float hue = hsb[0];
                                            float saturation = hsb[1];
                                            float brightness = hsb[2];
                                            hsb = null;

                                            hsb = java.awt.Color.RGBtoHSB(nred, ngreen, nblue, null);
                                            hue = hsb[0];
                                            saturation = hsb[1];
                                            // Convert HSB to RGB value
                                            rgb = java.awt.Color.HSBtoRGB(hue, saturation, brightness);
                                        }else{
                                            //rgb = pixel;
                                            rgb = -1; //white
                                        }
                                        mappingImage.setRGB(y, x, rgb);
                                        /*
                                        int pixels = mappingImage.getRGB(y,x);
                                        pixels = (pixels & 0xff) >>> 6;
                                        simulationImage.setRGB(y, x, pixels);
                                         */
                                    }
                                }
                                mappingIV.setImage(SwingFXUtils.toFXImage(mappingImage, null));
                            } catch (FileNotFoundException ex) {
                                Logger.getLogger(MappingEditView.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (IOException ex) {
                                Logger.getLogger(MappingEditView.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (Exception ex) {
                                Logger.getLogger(MappingEditView.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    });
                    //System.out.println(i+":"+j);
                    
                    editToolPane.getChildren().add(i,imageView);
                    //System.out.println(i+":"+j);
                    
                }
            }
        } catch (Exception ex){
            Logger.getLogger(MappingEditView.class.getName()).log(Level.SEVERE, null, ex);
            new Logging("SEVERE",MappingEditView.class.getName(),"MappingEditView()",ex);
        }        
    }
    
    public static Mat bufferedImageToMat(BufferedImage bufferImage) {
        Mat mat = new Mat(bufferImage.getHeight(), bufferImage.getWidth(), CvType.CV_8UC3);
        byte[] data = ((DataBufferByte) bufferImage.getRaster().getDataBuffer()).getData();
        mat.put(0, 0, data);
        return mat;
    }
    
    public static BufferedImage MatToBufferedImage(Mat mat) {
        BufferedImage bufferImage = null;
        if ( mat != null ) { 
            int cols = mat.cols();  
            int rows = mat.rows();  
            int elemSize = (int)mat.elemSize();  
            byte[] data = new byte[cols * rows * elemSize];          
            int type;  
            mat.get(0, 0, data);  
            switch (mat.channels()) {  
            case 1:  
                type = BufferedImage.TYPE_BYTE_GRAY;  
                break;  
            case 3:  
                type = BufferedImage.TYPE_3BYTE_BGR;  
                // bgr to rgb  
                byte b;  
                for(int i=0; i<data.length; i=i+3) {  
                    b = data[i];  
                    data[i] = data[i+2];  
                    data[i+2] = b;  
                }  
                break;  
            default:  
                return null;  
            }  

            bufferImage = new BufferedImage(cols, rows, type);
            bufferImage.getRaster().setDataElements(0, 0, cols, rows, data);
        } else { // mat was null
            bufferImage = null;
        }
        return bufferImage;
    }
}

