/*
 * Copyright (C) Digital India Corporation (Media Lab Asia)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.mla.simulator;

import com.mla.dictionary.DictionaryAction;
import com.mla.main.Configuration;
import com.mla.main.Logging;
import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.imageio.ImageIO;
/**
 * MappingView Class
 * <p>
 * This class is used for defining UI for artwork assignment in fabric editor.
 *
 * @author Amit Kumar Singh
 * @version %I%, %G%
 * @since   1.0
 * @date 07/01/2016
 * @Designing UI class for artwork assignment in fabric editor.
 * @see java.stage.*;
 * @link com.mla.fabric.FabricView
 */
public class MappingView {
 
    Simulator objSimulator;
    DictionaryAction objDictionaryAction;
    Configuration objConfiguration;
    
    BufferedImage bufferedImage;
    
    private Stage mappingStage;
    private BorderPane root;    
    private Scene scene;    
    private Label lblStatus;
    ProgressBar progressB;
    ProgressIndicator progressI;
    
    private ImageView userIV;
    private ImageView mapIV;
    private Graphics2D objGraphics2D;
    
    Button btnApply;
    Button btnCancel;
    
    Label lblDimension;
    ComboBox mapsCB;

    double startX, startY, lastX, lastY, oldX, oldY, width, height, angle;
    ArrayList<Float> cropVertx = null;
    float clipVerts[];    
    /**
     * MappingView
     * <p>
     * This constructor is used for UX initialization.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   constructor is used for UX initialization.
     * @see         javafx.stage.*;
     * @link        com.mla.fabric.FabricView
     * @throws      SQLException
     * @param       objConfigurationCall Object of Configuration Class
     */        
    public MappingView(Configuration objConfigurationCall, BufferedImage objBufferedImage) {   
        this.objConfiguration = objConfigurationCall;
        //this.bufferedImage = objBufferedImage;
        
        objDictionaryAction = new DictionaryAction(objConfiguration);
        cropVertx = new ArrayList<>();
        
        mappingStage = new Stage();
        mappingStage.initModality(Modality.APPLICATION_MODAL);//WINDOW_MODAL
        mappingStage.initStyle(StageStyle.UTILITY);
        VBox parent =new VBox();
        root = new BorderPane();
        //root.setId("popup");
        scene = new Scene(root, objConfiguration.WIDTH, objConfiguration.HEIGHT, Color.WHITE);
        scene.getStylesheets().add(MappingView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        
        HBox footContainer = new HBox();
        progressB = new ProgressBar(0);
        progressB.setVisible(false);
        progressI = new ProgressIndicator(0);
        progressI.setVisible(false);
        lblStatus = new Label(objDictionaryAction.getWord("WELCOMETOCADTOOL"));
        lblStatus.setId("message");
        footContainer.getChildren().addAll(lblStatus,progressB,progressI);
        footContainer.setId("footContainer");
        root.setBottom(footContainer);
        
        HBox bodyContainer = new HBox();
        bodyContainer.setPrefSize(objConfiguration.WIDTH,objConfiguration.HEIGHT);
        //bodyContainer.setPrefSize(root.getWidth(),root.getHeight());
        root.setCenter(bodyContainer);
        
        ScrollPane leftPane = new ScrollPane();
        leftPane.setId("subpopup");
        leftPane.setPrefSize(objConfiguration.WIDTH*2/3,objConfiguration.HEIGHT);
        bodyContainer.getChildren().add(leftPane);
        ScrollPane rightPane = new ScrollPane();
        //rightPane.setId("subpopup");
        rightPane.setPrefSize(objConfiguration.WIDTH*1/3,objConfiguration.HEIGHT);
        bodyContainer.getChildren().add(rightPane);
        
        mapIV = new ImageView(new Image("/media/user.png"));
        //mapIV.setImage(SwingFXUtils.toFXImage(bufferedImage, null));
        //mapIV.setPreserveRatio(true);
        leftPane.setContent(mapIV);
        
        VBox container = new VBox();
        
        Button btnBrowse = new Button(objDictionaryAction.getWord("BROWSE"));
        btnBrowse.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/browse.png"));
        container.getChildren().add(btnBrowse);
        
        Image image=new Image("/media/user.png");
        bufferedImage = SwingFXUtils.fromFXImage(image, null);
        objGraphics2D = bufferedImage.createGraphics();
        objGraphics2D.drawImage(bufferedImage, 0, 0, bufferedImage.getWidth(), bufferedImage.getHeight(), null);
        image=SwingFXUtils.toFXImage(bufferedImage, null);  
        
        userIV = new ImageView();
        userIV.setImage(image);
        container.getChildren().add(userIV);
            
        btnBrowse.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                try{
                    FileChooser fileChooser = new FileChooser();             
                    //Set extension filter
                    FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
                    FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
                    FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("BMP files (*.bmp)", "*.BMP");
                    FileChooser.ExtensionFilter extFilterTIF = new FileChooser.ExtensionFilter("TIFF files (*.tif)", "*.TIF");
                    fileChooser.getExtensionFilters().addAll(extFilterPNG, extFilterJPG, extFilterBMP, extFilterTIF);
                    //fileChooser.setInitialDirectory(new File(objFabric.getObjConfiguration().strRoot));
                    fileChooser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("SELECTTO")+" "+objDictionaryAction.getWord("DESIGN"));
                    //Show open file dialog
                    File file = fileChooser.showOpenDialog(null);
                    String filePath = file.getCanonicalPath();
                    bufferedImage = ImageIO.read(file);

                    //ArtworkAction objArtworkAction = new ArtworkAction(false);
                    //lblStatus.setText(objDictionaryAction.getWord("COLORINFO"));
                    //bufferedImage = objArtworkAction.rectifyImage(bufferedImage,objConfiguration.getIntColorLimit());

                    width = bufferedImage.getWidth();
                    height = bufferedImage.getHeight();
                    
                    objGraphics2D = bufferedImage.createGraphics();
                    objGraphics2D.drawImage(bufferedImage, 0, 0, bufferedImage.getWidth(), bufferedImage.getHeight(), null);
                    //objGraphics2D.setColor(java.awt.Color.WHITE);
                    //objGraphics2D.fillRect(0, 0, bufferedImage.getWidth(), bufferedImage.getHeight());
        

                    Image image=SwingFXUtils.toFXImage(bufferedImage, null);  
                    userIV.setImage(image);


                } catch (Exception ex) {
                    new Logging("SEVERE",MappingView.class.getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });

        addEventHandler();

        Button btnCrop = new Button(objDictionaryAction.getWord("CROP"));
        btnCrop.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/browse.png"));
        container.getChildren().add(btnCrop);
        
        rightPane.setContent(container);
        
        mappingStage.setScene(scene);
        mappingStage.getIcons().add(new Image("/media/icon.png"));
        mappingStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWMAPPING")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
        mappingStage.setIconified(false);
        mappingStage.setResizable(false);        
        mappingStage.setX(-5);
        mappingStage.setY(0);
        mappingStage.showAndWait();
    } 
    
    /**
     * addEventHandler
     * <p>
     * Function use for drawing menu bar for menu item,
     * and binding events for each menus with style. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ArtworkView
     */
    private void addEventHandler(){
        userIV.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                //System.out.println("Clicked");
                onMouseClickedListener(event);
            }
        });
        userIV.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>(){ 
            @Override
            public void handle(MouseEvent event) {
                //System.out.println("Presed");
                onMousePressedListener(event);
            }
        });
        userIV.addEventHandler(MouseEvent.MOUSE_DRAGGED, new EventHandler<MouseEvent>(){ 
            @Override
            public void handle(MouseEvent event) {
                //System.out.println("Dragged");
                onMouseDraggedListener(event);
            }
        });
        userIV.addEventHandler(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>(){ 
            @Override
            public void handle(MouseEvent event) {
                //System.out.println("Relesed");
                onMouseReleaseListener(event);
            }
        });
    }
    
    private void onMouseClickedListener(MouseEvent e){
        
    }
    private void onMousePressedListener(MouseEvent e){
        this.startX = e.getX();
        this.startY = e.getY();
        this.oldX = e.getX();
        this.oldY = e.getY();
        cropVertx.add((float)oldX);
        cropVertx.add((float)oldY);
    }
    private void freeDrawing()
    {
        //System.err.println(startX+" : "+startY+" :: "+lastX+" : "+lastY+" :: "+oldX+" : "+oldY+" :: "+width+" : "+height+" : "+angle);
        BasicStroke bs = new BasicStroke(1);
        objGraphics2D.setStroke(bs);
        objGraphics2D.setColor(java.awt.Color.RED);
        objGraphics2D.drawLine((int)oldX, (int)oldY, (int)lastX, (int)lastY);
        oldX = lastX;
        oldY = lastY;
        cropVertx.add((float)oldX);
        cropVertx.add((float)oldY);
        
        Image image=SwingFXUtils.toFXImage(bufferedImage, null);  
        userIV.setImage(image);
        
    }
    private void onMouseDraggedListener(MouseEvent e){
        this.lastX = e.getX();
        this.lastY = e.getY();
        freeDrawing();
    }
    private void onMouseReleaseListener(MouseEvent e){
        Image image=SwingFXUtils.toFXImage(bufferedImage, null);  
        userIV.setImage(image);
        drawcropImage();
        
    }
    
    private void drawcropImage(){
        try {
            clipVerts = new float[cropVertx.size()];
            for(int i = 0; i<cropVertx.size(); i++)
                clipVerts[i]=cropVertx.get(i);
            System.err.println(cropVertx.size()+"=length points="+clipVerts.length);
            BufferedImage cropImage  = Mapping.clip(bufferedImage,clipVerts);
            
            File pngFile = new File("test.png");
            ImageIO.write(cropImage, "png", pngFile);
            
            Image image=SwingFXUtils.toFXImage(cropImage, null);
            mapIV.setImage(image);
        } catch (IOException ex) {
            Logger.getLogger(MappingView.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
