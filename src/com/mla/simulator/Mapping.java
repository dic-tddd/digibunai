/*
 * Copyright (C) 2018 HP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.mla.simulator;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.image.BufferedImage;

/**
 *
 * @author HP
 */
public class Mapping {

    /**
   * Clips the input image to the specified shape
   * 
   * @param image
   *           the input image
   * @param clipVerts
   *           list of x, y pairs defining the clip shape, normalised
   *           to image dimensions (think texture coordinates)
   * @return The smallest image containing those pixels that fall
   *         inside the clip shape
   */
  public static BufferedImage clip( BufferedImage image, float[] clipVerts )
  {
    assert clipVerts.length >= 6;
    assert clipVerts.length % 2 == 0;

    int[] xp = new int[ clipVerts.length / 2 ];
    int[] yp = new int[ xp.length ];

    int minX = image.getWidth(), minY = image.getHeight(), maxX = 0, maxY = 0;

    for( int j = 0; j < xp.length; j++ )
    {
      xp[ j ] = Math.round( clipVerts[ 2 * j ] * image.getWidth() );
      yp[ j ] = Math.round( clipVerts[ 2 * j + 1 ] * image.getHeight() );

      minX = Math.min( minX, xp[ j ] );
      minY = Math.min( minY, yp[ j ] );
      maxX = Math.max( maxX, xp[ j ] );
      maxY = Math.max( maxY, yp[ j ] );
    }

    for( int i = 0; i < xp.length; i++ )
    {
      xp[ i ] -= minX;
      yp[ i ] -= minY;
    }

    Polygon clip = new Polygon( xp, yp, xp.length );
    BufferedImage out = new BufferedImage( maxX - minX, maxY - minY, image.getType() );
    Graphics g = out.getGraphics();

        g.setColor(Color.red);
        g.setClip( clip );

    g.drawImage( image, -minX, -minY, null );
    g.drawLine(minX, minY, maxX, maxY);
    g.dispose();

    return out;
  }

    
}
