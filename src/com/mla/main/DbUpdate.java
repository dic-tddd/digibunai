package com.mla.main;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

/**
 * Updates for database schema (before application startup)
 * @author Aatif Ahmad Khan
 * @since 0.9.7
 */
public class DbUpdate {
    
    public DbUpdate(){
        addColumnIfNotExists();
    }
    
    /**
     * Add given columns to database schema
     */
    private void addColumnIfNotExists(){
        try{
            String sDropProc = "DROP PROCEDURE IF EXISTS `addColumnIfNotExists`;";
            String sCreateProc="CREATE PROCEDURE `addColumnIfNotExists`()\n" +
                "BEGIN\n" +
                "  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN END;\n" +
                "  ALTER TABLE `mla_user_configuration` ADD COLUMN `COLORLIMIT` INT(5) DEFAULT 256 AFTER `PALETTEID`;\n" +
                "  ALTER TABLE `mla_design_library` ADD COLUMN `CATEGORY` varchar(30) NOT NULL DEFAULT 'other' AFTER `NAME`;\n" +
                "END";
            String sCallProc = "CALL `addColumnIfNotExists`();";
            String sDropProcEnd = "DROP PROCEDURE `addColumnIfNotExists`;";
            Connection conn = DbConnect.getConnection();
            Statement stmt = conn.createStatement();
            stmt.addBatch(sDropProc);
            stmt.addBatch(sCreateProc);
            stmt.addBatch(sCallProc);
            stmt.addBatch(sDropProcEnd);
            int returnCodes[]=stmt.executeBatch();
            new Logging("INFO",getClass().getName(), "Queries Return Codes: "+Arrays.toString(returnCodes), null);
        }
        catch(SQLException sqlEx){
            new Logging("SEVERE",getClass().getName(),"Issue in DbUpdate: ",sqlEx);
        }
    }
}
