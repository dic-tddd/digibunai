/*
 * Copyright (C) 2017 Digital India Corporation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mla.main;

import java.awt.HeadlessException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import static com.mla.main.DigiBunai.Constants.CONFIG;
import java.net.ConnectException;
import java.security.Security;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
/**
 *
 * @Designing database connection file
 * @author Amit Kumar Singh
 * 
 */
public class DbConnect {
    
 /**
 * getConnection()
 * <p>
 * This Function is used for creation of jdbc connection. 
 * 
 * @param       null
 * @return      Connection
 * @throws      SQLException
 * @author      Amit Kumar Singh
 * @version     %I%, %G%
 * @since       1.0
 * @see         java.sql.*;
 * @link        null
 */
    public static synchronized Connection getConnection() {
        Connection connection;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            //CREATE USER 'myuser'@'%' IDENTIFIED BY '4myuser';
            //GRANT ALL ON my_upload.* TO 'myuser'@'%' IDENTIFIED BY '4myuser';
            if(CONFIG.DBSSL.equalsIgnoreCase("enabled")){
                Security.insertProviderAt(new BouncyCastleProvider(), 1);
                System.setProperty("javax.net.ssl.trustStore",System.getProperty("user.dir")+"/mla/truststore"); 
                System.setProperty("javax.net.ssl.trustStorePassword","capassword");
                connection = DriverManager.getConnection("jdbc:mysql://"+CONFIG.DBHOST+":"+CONFIG.DBPORT+"/"+CONFIG.DBNAME+"?useSSL=true&requireSSL=true&verifyServerCertificate=true", CONFIG.DBUSER, CONFIG.DBPASSWORD);
                //System.err.println(CONFIG.DBHOST+"="+CONFIG.DBPORT +"="+ CONFIG.DBUSER+"="+CONFIG.DBPASSWORD);
            }else{
                //connection = DriverManager.getConnection("jdbc:mysql://192.168.1.196:3306/bunai?user=root&password=root");
                //connection = DriverManager.getConnection("jdbc:mysql://192.168.1.196:3306/bunai", "root","root");
                connection = DriverManager.getConnection("jdbc:mysql://"+CONFIG.DBHOST+":"+CONFIG.DBPORT+"/"+CONFIG.DBNAME, CONFIG.DBUSER, CONFIG.DBPASSWORD);
                //System.err.println(CONFIG.DBHOST+"="+CONFIG.DBPORT +"="+ CONFIG.DBUSER+"="+CONFIG.DBPASSWORD);
                /*
                InitialContext ctx = new InitialContext();
                //DataSource ds = (DataSource)ctx.lookup("java:comp/env/jdbc/MySQLDS");
                DataSource  ds = (DataSource)ctx.lookup("java:comp/env/jdbc/e-DhanwanthariDS");
                con = ds.getConnection(); 
                */
            }
            return connection;
        } catch (SQLException ex) {
             // handle any errors
           // System.out.println("SQLException: " + ex.getMessage());
            //System.out.println("SQLState: " + ex.getSQLState());
            //System.out.println("VendorError: " + ex.getErrorCode());
            new Logging("SEVERE",DbConnect.class.getName(),"Error Occured While Getting the Connection"+ex.toString(),ex);         
        } catch (Exception ex) {
            new Logging("SEVERE",DbConnect.class.getName(),"Error Occured While Getting the Connection"+ex.toString(),ex);
        } 
                
        return null;
    } 
    /**
 * getConnection()
 * <p>
 * This Function is used for creation of jdbc connection. 
 * 
 * @param       null
 * @return      Connection
 * @throws      SQLException
 * @author      Amit Kumar Singh
 * @version     %I%, %G%
 * @since       1.0
 * @see         java.sql.*;
 * @link        null
 */
    public static synchronized Connection getConnection(String dbHost, String dbPort, String dbName, String dbUsername, String dbPassword, boolean isSecure) {
        String result = "Connection Failed";
        Connection connection = null;
        try {            
            Class.forName("com.mysql.jdbc.Driver");
            if(isSecure){
                Security.insertProviderAt(new BouncyCastleProvider(), 1);
                System.setProperty("javax.net.ssl.trustStore",System.getProperty("user.dir")+"/mla/truststore"); 
                System.setProperty("javax.net.ssl.trustStorePassword","capassword");
                connection = DriverManager.getConnection("jdbc:mysql://"+dbHost+":"+dbPort+"/"+dbName+"?useSSL=true&requireSSL=true&verifyServerCertificate=true", dbUsername, dbPassword);
            } else{
                connection = DriverManager.getConnection("jdbc:mysql://"+dbHost+":"+dbPort+"/"+dbName, dbUsername, dbPassword);
            }
            //System.err.println(CONFIG.DBHOST+"="+CONFIG.DBPORT +"="+ CONFIG.DBUSER+"="+CONFIG.DBPASSWORD);
           result = "Connection Successful";
        } catch (SQLException ex) {
            connection = null;
             // handle any errors
            //System.out.println("SQLException: " + ex.getMessage());
            //System.out.println("SQLState: " + ex.getSQLState());
            //System.out.println("VendorError: " + ex.getErrorCode());
            new Logging("SEVERE",DbConnect.class.getName(),"Error Occured While Getting the Connection"+ex.toString(),ex);
        } catch (ClassNotFoundException ex) {
            connection = null;
            new Logging("SEVERE",DbConnect.class.getName(),"Error Occured While Getting the Connection"+ex.toString(),ex);
        }    
        return connection;
    } 
 /**
 * close()
 * <p>
 * This Function is used for closing of jdbc connection. 
 * 
 * @param       Connection connection
 * @return      Connection
 * @throws      SQLException
 * @author      Amit Kumar Singh
 * @version     %I%, %G%
 * @since       1.0
 * @see         java.sql.*;
 * @link        null
 */
     public static synchronized void close(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
                connection = null;
            } catch (SQLException ex) {
                new Logging("SEVERE",DbConnect.class.getName(),"Error Occured While Closing the Connection"+ex.toString(),ex);
            }
        }
    }
 /**
 * close()
 * <p>
 * This Function is used for closing of jdbc connection. 
 * 
 * @param       Statement statement, ResultSet resultset
 * @return      void
 * @throws      SQLException
 * @author      Amit Kumar Singh
 * @version     %I%, %G%
 * @since       1.0
 * @see         java.sql.*;
 * @link        null
 */
    public static synchronized void close(Statement statement, ResultSet resultset) {
        if (resultset != null) {
            try {
                resultset.close();
                resultset = null;
            } catch (SQLException ex) {
                new Logging("SEVERE",DbConnect.class.getName(),"Error Occured While Closing the ResultSet"+ex.toString(),ex);
            }
        }
        if (statement != null) {
            try {
                statement.close();
                statement = null;
            } catch (SQLException ex) {
                new Logging("SEVERE",DbConnect.class.getName(),"Error Occured While Closing the Statement"+ex.toString(),ex);
            }
        }
    }
    
    public static int restoreDBfromSQL(String restoreFilePath) {
        int processComplete = -1;
        try {
            /*NOTE: String s is the mysql file name including the .sql in its name*/
            /*NOTE: Getting path to the Jar file being executed*/
            /*NOTE: YourImplementingClass-> replace with the class executing the code*/
            
            /*NOTE: Used to create a cmd command*/
            /*NOTE: Do not create a single large string, this will cause buffer locking, use string array*/
            String[] executeCmd = new String[]{"mysql", CONFIG.DBNAME, "-u" + CONFIG.DBUSER, "-p" + CONFIG.DBPASSWORD, "-e", " source " + restoreFilePath};
            
            /*NOTE: processComplete=0 if correctly executed, will contain other values if not*/
            Process runtimeProcess = Runtime.getRuntime().exec(executeCmd);
            processComplete = runtimeProcess.waitFor();

            /*NOTE: processComplete=0 if correctly executed, will contain other values if not*/
            if (processComplete == 0) {
                System.err.println("Successfully restored from SQL : " + restoreFilePath);
            } else {
                System.err.println("Error at restoring");
            }
        } catch (IOException | InterruptedException | HeadlessException ex) {
            new Logging("SEVERE",DbConnect.class.getName(),"Error Occured While restore db from sql"+ex.getMessage(),ex);
        }
        return processComplete;
    }

    public static int backupDBtoSQL(String backupFilePath) {
        int processComplete = -1;
        try {
            /*NOTE: Getting path to the Jar file being executed*/
            /*NOTE: YourImplementingClass-> replace with the class executing the code*/
            System.out.println("Welcome to Backupdbtosql");
            System.err.println("Path: "+backupFilePath);                     
            /*NOTE: Used to create a cmd command*/
            String executeCmd = "mysqldump -q -u" + CONFIG.DBUSER + " -p" + CONFIG.DBPASSWORD + " --database " + CONFIG.DBNAME + " -r " + backupFilePath;
            System.err.println("Command: "+executeCmd);
            /*NOTE: Executing the command here*/
            Process runtimeProcess = Runtime.getRuntime().exec(executeCmd);
            processComplete = runtimeProcess.waitFor();
            System.err.println("Result Status: "+processComplete);
            /*NOTE: processComplete=0 if correctly executed, will contain other values if not*/
            if (processComplete == 0) {
                System.out.println("Backup Complete");
            } else {
                System.out.println("Backup Failure");
            }
        } catch (IOException | InterruptedException ex) {
            new Logging("SEVERE",DbConnect.class.getName(),"Error Occured While backup db to sql"+ex.getMessage(),ex);
        }
        return processComplete;
    }

}