/*
 * Copyright (C) Digital India Corporation (Media Lab Asia)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mla.main;

import static com.mla.main.DigiBunai.Constants.CONFIG;
import com.mla.secure.Security;
import com.mla.user.UserLoginView;
import com.mla.weave.WeaveAction;
import java.sql.SQLException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
/**
 *
 * @Designing GUI window function for instalation
 * @author Amit Kumar Singh
 * 
 */
public class DigiBunai extends Application {
    static Configuration objConfiguration = null;
    static Stage mainStage;
    private ProgressBar progressB;
    private ProgressIndicator progressI;
    
    public enum Constants {
        CONFIG;
        public String USERTYPE;
        public String USERNAME;
        public String PASSWORD;
        public String EMAIL;
        public String CENTER;
        public String LICENCE;
        public String LICENCTYPE;
        public String MACID;
        public String IP;
        public String PORT; 
        public String DBSYNCH;
        public String DBSSL;
        public String DBHOST;
        public String DBPORT;
        public String DBNAME;
        public String DBPREFIX;
        public String DBUSER;
        public String DBPASSWORD;
        public String MAILSERVER;
        public String MAILPORT;
        public String MAILUSER;
        public String MAILPASSWORD;
        public String MAILAUTH;
        public String MAILSTARTTLS;        
        static{
            Properties properties = new Properties();                
            try(InputStream resourceAsStream = new FileInputStream(System.getProperty("user.dir")+"/mla/configuration.properties")){
                //InputStream resourceAsStream =  DigiBunai.class.getClassLoader().getResourceAsStream("configuration.properties");
                if (resourceAsStream != null) {
                    properties.load(resourceAsStream);
                }
            } catch(IOException ex){
                new Logging("SEVERE",Email.class.getName(),"send(): Error reading parameters from configuration.properties",ex);
            }
            CONFIG.USERTYPE=properties.getProperty("USERTYPE", "Guest");
            CONFIG.USERNAME=properties.getProperty("USERNAME", "user");
            CONFIG.PASSWORD=properties.getProperty("PASSWORD", "openweave");
            CONFIG.EMAIL=properties.getProperty("EMAIL", "cadtool@medialabasia.in");
            CONFIG.CENTER=properties.getProperty("CENTER", "10001");
            CONFIG.LICENCE=properties.getProperty("LICENCE", "705A0F667CCF");
            CONFIG.LICENCTYPE=properties.getProperty("LICENCTYPE", "Prime");
            CONFIG.MACID=properties.getProperty("MACID", "70-5A-0F-66-7C-CF");
            CONFIG.IP=properties.getProperty("IP", "127.0.0.1");
            CONFIG.PORT=properties.getProperty("PORT", "8081");
            CONFIG.DBSYNCH=properties.getProperty("DBSYNCH", "disabled");
            CONFIG.DBSSL=properties.getProperty("DBSSL", "disabled");
            CONFIG.DBHOST=properties.getProperty("DBHOST", "localhost");
            CONFIG.DBPORT=properties.getProperty("DBPORT", "3306");
            CONFIG.DBNAME=properties.getProperty("DBNAME", "DigiBunai");
            CONFIG.DBPREFIX=properties.getProperty("DBPREFIX", "mla_");
            CONFIG.DBUSER=properties.getProperty("DBUSER", "openweave");
            CONFIG.DBPASSWORD=properties.getProperty("DBPASSWORD", "ml@sia");
            CONFIG.MAILSERVER=properties.getProperty("MAILSERVER", "smtp.gmail.com");
            CONFIG.MAILPORT=properties.getProperty("MAILPORT", "587");
            CONFIG.MAILUSER=properties.getProperty("MAILUSER", "emailtestmla@gmail.com");
            CONFIG.MAILPASSWORD=properties.getProperty("MAILPASSWORD", "passwordtestmla");
            CONFIG.MAILAUTH=properties.getProperty("MAILAUTH", "true");
            CONFIG.MAILSTARTTLS=properties.getProperty("MAILSTARTTLS", "true");  
            //System.err.println(CONFIG.DBHOST+"="+CONFIG.DBPORT +"="+ CONFIG.DBUSER+"="+CONFIG.DBPASSWORD);
        }
    }    
    private static void logBasicSystemInfo() {
        //System.err.println(Security.SecurePassword("Grapewater848!", "admin"));
        //System.exit(0);
        new Logging("INFO",DigiBunai.class.getName(),"Launching the application.",null);
        new Logging("CONFIG",DigiBunai.class.getName(),"Operating System: " + System.getProperty("os.name") + " " + System.getProperty("os.version"),null);
        new Logging("CONFIG",DigiBunai.class.getName(),"JRE: " + System.getProperty("java.version"),null);
        new Logging("INFO",DigiBunai.class.getName(),"Java Launched From: " + System.getProperty("java.home"),null);
        new Logging("CONFIG",DigiBunai.class.getName(),"Class Path: " + System.getProperty("java.class.path"),null);
        new Logging("CONFIG",DigiBunai.class.getName(),"Library Path: " + System.getProperty("java.library.path"),null);
        new Logging("CONFIG",DigiBunai.class.getName(),"Application Name: Bunai 1.0",null);
        new Logging("CONFIG",DigiBunai.class.getName(),"User Home Directory: " + System.getProperty("user.home"),null);
        new Logging("CONFIG",DigiBunai.class.getName(),"User Working Directory: " + System.getProperty("user.dir"),null);
        new Logging("INFO",DigiBunai.class.getName(),"Test INFO logging.",null);
        new Logging("FINE",DigiBunai.class.getName(),"Test FINE logging.",null);
        new Logging("FINEST",DigiBunai.class.getName(),"Test FINEST logging.",null);
        clearConsole();
       /* try{
            QRCode objQRCode = new QRCode();
            DbTest objDbTest = new DbTest();
            objDbTest.observTime();
        } catch (Exception ex) {
            new Logging("SEVERE",DbTest.class.getName(),ex.getMessage(),ex);
        }*/
        /*try {
            
            WeaveAction objWeaveAction = new WeaveAction();
            objWeaveAction.refrshWeaveIcon(objConfiguration);
            //objWeaveAction.dumpWeave(System.getProperty("user.dir"));
            System.err.println("I am here");
        } catch (SQLException ex) {
            Logger.getLogger(DigiBunai.class.getName()).log(Level.SEVERE, null, ex);
        } */
    }
    private static void clearConsole() {
        try {
            final String os = System.getProperty("os.name");
            if (os.contains("Windows")) {
                Runtime.getRuntime().exec("cls");
            } else {
                Runtime.getRuntime().exec("clear");
            }
        } catch (final Exception e) {
            //System.err.println("DigiBunai :(");
        }
    }
    private void logSystemMemoryInfo(){
        int mb = 1024*1024;
        //Getting the runtime reference from system
        Runtime runtime = Runtime.getRuntime();

        new Logging("INFO",DigiBunai.class.getName(),"##### Heap utilization statistics [MB] #####",null);
        new Logging("CONFIG",DigiBunai.class.getName(),"Used Memory:" + ((runtime.totalMemory() - runtime.freeMemory()) / mb),null);
        new Logging("CONFIG",DigiBunai.class.getName(),"Free Memory:" + (runtime.freeMemory() / mb),null);
        new Logging("INFO",DigiBunai.class.getName(),"Total Memory:" + (runtime.totalMemory() / mb),null);
        new Logging("INFO",DigiBunai.class.getName(),"Maximum Available Memory:" + (runtime.maxMemory() / mb),null);		
    }
    private void showUpdateInfo(){
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UNDECORATED);
        //dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.setIconified(false);
        dialogStage.setFullScreen(false);
        dialogStage.getIcons().add(new Image("/media/icon.png"));
        dialogStage.setTitle("Notification : Application Updates");
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 333, 111, Color.WHITE);
        scene.getStylesheets().add(DigiBunai.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        popup.add(new ImageView("/media/alert_warning.png"), 0, 0);
        Label lblAlert = new Label("New Update has been found ! \n Please run the updater to update application or press skip to proceed to application anyway.");
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(300);
        popup.add(lblAlert, 1, 0, 2, 1);
        Button btnYes = new Button("Skip");
        btnYes.setTooltip(new Tooltip("Skip to run the application anyway !"));
        btnYes.setPrefWidth(50);
        //btnYes.setMaxWidth(Double.MAX_VALUE);
        //btnYes.setAlignment(Pos.CENTER_LEFT);
        GridPane.setHalignment(btnYes, HPos.CENTER);
        btnYes.setId("btnYes");
        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
                UserLoginView objUserLoginView = new UserLoginView(objConfiguration);
            }
        });
        popup.add(btnYes, 1, 1);
        Button btnNo = new Button("Cancel");
        btnNo.setTooltip(new Tooltip("Cancel to close the application !"));
        btnNo.setPrefWidth(50);
        //btnNo.setMaxWidth(Double.MAX_VALUE);
        //btnNo.setAlignment(Pos.CENTER_RIGHT);
        GridPane.setHalignment(btnNo, HPos.RIGHT);
        btnNo.setId("btnNo");
        btnNo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
                System.exit(0);
            }
        });
        popup.add(btnNo, 2, 1);
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.showAndWait();
    }
    
    @Override
    public void start(final Stage primaryStage) {
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
            Date date = new Date();
            String currentDate = dateFormat.format(date);
            new Logging("INFO",DigiBunai.class.getName(),"Launching Application on "+currentDate,null);
            //launchDigiBunai();
            //EventQueue.invokeLater(new SplashScreenCloser());
            setUserAgentStylesheet(STYLESHEET_CASPIAN);
            //primaryStage.initModality(Modality.APPLICATION_MODAL);//WINDOW_MODAL
            //primaryStage.initStyle(StageStyle.UNDECORATED);
            primaryStage.initStyle(StageStyle.TRANSPARENT);
            primaryStage.setResizable(false);
            primaryStage.setIconified(false);
            primaryStage.setFullScreen(false);
            primaryStage.getIcons().add(new Image("/media/icon.png"));
            primaryStage.setTitle("DigiBunai");                
            //primaryStage.setX(-5);
            //primaryStage.setY(0);

            BorderPane root = new BorderPane();
            Scene scene = new Scene(root, 450, 250, Color.TRANSPARENT);
            scene.getStylesheets().add(DigiBunai.class.getResource("/media/login.css").toExternalForm());
            root.setId("splashpane");

            progressB = new ProgressBar(50);
            progressB.setVisible(true);
            progressI = new ProgressIndicator(0);
            progressI.setVisible(false);
            //root.setBottom(progressB);

            Thread system_message_thread=new Thread(new Runnable() {
                public void run() { 
                    try {
                        Thread.sleep(3000);
                    } catch(InterruptedException ex) {
                        new Logging("INFO",DigiBunai.class.getName(),"Closing the splash screen.",null);
                    }
                    Platform.runLater(new Runnable() {
                        public void run() {
                            new Logging("INFO",DigiBunai.class.getName(),"Loading data",null);
                            logBasicSystemInfo();
                            logSystemMemoryInfo();
                            objConfiguration = new Configuration();
                            objConfiguration.intializeConfiguration();
                            //new LaunchInstallationCloser().run();
                            primaryStage.close();
                            new Logging("INFO",DigiBunai.class.getName(),"Closing the splash screen.",null);
                            new Logging("INFO",DigiBunai.class.getName(),"Launching login screen",null);
                            File objFile = new File(System.getProperty("user.dir")+"/mla/configuration.properties");
                            if(!objFile.exists()){
                                InstallationView objInstallationView = new InstallationView(objConfiguration);
                            }else{
                                objFile = new File(System.getProperty("user.dir")+"/updates/DigiBunai.jar");
                                if(objFile.exists()){
                                    showUpdateInfo();
                                } else {
                                    new DbUpdate(); // updates db schema
                                    UserLoginView objUserLoginView = new UserLoginView(objConfiguration);
                                    //UserViewFull objUserViewFull = new UserViewFull(objConfiguration); 
                                }
                            }
                        }
                    });
                }
            });
            system_message_thread.start();

            primaryStage.setScene(scene);
            //primaryStage.setFocused(true);
            primaryStage.show();
            new Logging("INFO",DigiBunai.class.getName(),"Launch thread now exiting",null);
            new Logging("WARNING",DigiBunai.class.getName(),"UnsupportedOperationException",new UnsupportedOperationException("Not supported yet."));  
        } catch (Exception e) {
            // generic exception handling
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {   
        launch(args);    
    }
}