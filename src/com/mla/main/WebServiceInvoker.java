/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mla.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author Aatif Ahmad Khan
 */
public class WebServiceInvoker {
    
    //public final static String strGetMethod = "GET";
    //public final static String strPostMethod = "POST";
    //public final static String strHeadMethod = "HEAD";
    //public final static String strOptionsMethod = "OPTIONS";
    //public final static String strPutMethod = "PUT";
    //public final static String strDeleteMethod = "DELETE";
    //public final static String strTraceMethod = "TRACE";
    public String strResponse;
    
    public WebServiceInvoker(final String strUrl, final String jsonData, final String strMethod){
        try {
            URL url = new URL(strUrl);
            //System.out.println("Creating Connection to: "+strUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            
            // Set Request Method
            if(strMethod != null)
                con.setRequestMethod(strMethod);
            
            // Set Request Header Parameter "Content-Type"
            con.setRequestProperty("Content-Type", "application/json"); // "application/json; utf-8"
            
            // Set Response Format
            con.setRequestProperty("Accept", "application/json");
            
            // Enable writing request content to connection output stream
            con.setDoOutput(true);
            
            // Write data in Request Body
            if(jsonData != null){
                //System.out.println(jsonData);
                try(OutputStream os = con.getOutputStream()){
                    byte[] inputData = jsonData.getBytes("utf-8");
                    os.write(inputData, 0, inputData.length);
                }
            }
            
            // Read Response Content
            try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))){
                StringBuilder response = new StringBuilder();
                String strLine = null;
                while((strLine = br.readLine()) != null)
                    response.append(strLine.trim());
                strResponse = response.toString();
                //System.out.println(response.toString());
            }
        } catch (MalformedURLException ex) {
            new Logging("SEVERE",WebServiceInvoker.class.getName(),"MalformedURL: "+strUrl,ex);
            new MessageView("error", "Server Error", "Server Error. Please retry.");
        } catch (IOException ex) {
            new Logging("SEVERE",WebServiceInvoker.class.getName(),"Http Connection Issue (Activation Server)",ex);
            new MessageView("error", "Server Error", "Server Error. Please retry.");
        }
    }
}
