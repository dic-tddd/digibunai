/*
 * Copyright (C) Digital India Corporation (Media Lab Asia)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mla.main;
import com.mla.dictionary.DictionaryAction;
import static com.mla.main.DigiBunai.Constants.CONFIG;
import com.mla.user.User;
import com.mla.user.UserAction;
import com.mla.user.UserLoginView;
import com.mla.weave.WeaveAction;
import java.awt.HeadlessException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.AudioClip;
import static javafx.scene.media.AudioClip.INDEFINITE;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.util.encoders.Base64;
/**
 *
 * @Designing GUI window for dashboard
 * @author Amit Kumar Singh
 * 
 */
public class InstallationView extends Application {
    public static Stage windowStage;
    private BorderPane root;
    private Scene scene;
    private int stepCompleted = 0;
    static boolean isDBLoaded = false;
    //private AudioClip objAudioClip;
    
    Configuration objConfiguration = null;
    DictionaryAction objDictionaryAction = null;
    User objUser= null;
    int[] userGroups;
    
    private TextField txtDBHost;
    private TextField txtDBPort;
    private TextField txtDBName;
    private TextField txtDBUsername;
    private TextField txtDBPassword;
    private CheckBox txtDBSecure;  
    
    private ToggleGroup txtloginType;
    private CheckBox txtSubscription;
    private CheckBox txtRegistration;
    private Button buttonNext;
    private Button buttonPrevious;
    private Text lblStatus;
    
/**
* InstallationView(Stage)
* <p>
* This constructor is used for individual call of class. 
* 
* @param       Stage primaryStage
* @author      Amit Kumar Singh
* @version     %I%, %G%
* @since       1.0
* @see         javafx.stage.*;
* @link        FabricView
*/
     public InstallationView(final Stage primaryStage) {}    
/**
* InstallationView(Configuration)
* <p>
* This class is used for prompting about software information. 
* 
* @param       Configuration objConfigurationCall
* @author      Amit Kumar Singh
* @version     %I%, %G%
* @since       1.0
* @see         javafx.scene.control.*;
* @link        Configuration
*/
    public InstallationView(Configuration objConfigurationCall) {  
        objConfiguration = objConfigurationCall;
        objDictionaryAction = new DictionaryAction(objConfiguration);    
        
        windowStage = new Stage(); 
        root = new BorderPane();
        scene = new Scene(root, 600, 360, Color.WHITE);
        scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/dashboard.css").toExternalForm());
        //objAudioClip = new AudioClip(getClass().getResource("/media/please_wait.mp3").toString());
        //objAudioClip.setCycleCount(INDEFINITE);        
        //TextToSpeech objTextToSpeech=new TextToSpeech();
        //objTextToSpeech.dospeak(objDictionaryAction.getWord("ARTWORKEDITOR")+" :"+objDictionaryAction.getWord("TOOLTIPARTWORKEDITOR"));
        
        stepCompleted = 0;
        if(isPrerequistSET()){
            root.setTop(addHeader());
            root.setLeft(addStepPane());
            root.setCenter(addContentPane());
        } else {
           root.setCenter(new Label("MySQL Path is not set ! Please try again after setting mysql Path."));
        }
        windowStage.setScene(scene);        
        windowStage.getIcons().add(new Image("/media/icon.png"));
        windowStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWDASHBOARD")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
        windowStage.setIconified(false);
        windowStage.setFullScreen(false);
        windowStage.setResizable(false);
        //windowStage.setMaximized(false);
        //windowStage.setX(0);
        //windowStage.setY(0);
        windowStage.show();  
        windowStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                exitMenuAction();
                we.consume();
            }
        });
    } 
    
    public HBox addHeader() {
        HBox hbox = new HBox();
        hbox.setPadding(new Insets(15, 12, 15, 12));
        hbox.setSpacing(10);
        hbox.setStyle("-fx-background-color: #336699;");

        buttonPrevious = new Button("<- Back");
        buttonPrevious.setPrefSize(100, 20);
        buttonPrevious.setDisable(true);
        buttonNext = new Button("Next ->");
        buttonNext.setPrefSize(100, 20);
        buttonPrevious.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {                
                if(stepCompleted>0){                    
                    buttonNext.setDisable(false); 
                    buttonNext.setText("Next ->"); 
                    stepCompleted--; 
                    root.setLeft(addStepPane());
                    root.setCenter(addContentPane());
                }
                if(stepCompleted<=0)
                    buttonPrevious.setDisable(true);
            }
        });
        buttonNext.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) { 
                if(stepCompleted<3){
                    buttonPrevious.setDisable(false);
                    stepCompleted++; 
                    root.setLeft(addStepPane());
                    root.setCenter(addContentPane());
                } else{
                    //buttonNext.setDisable(true);  
                    new Logging("INFO",InstallationView.class.getName(),"Before Update Old Weave Image",null);
                    try{
                        WeaveAction objWeaveAction = new WeaveAction();
                        objWeaveAction.refrshWeaveIcon(objConfiguration);
                    } catch(SQLException sqlEx){
                        new Logging("SEVERE",InstallationView.class.getName(),"refrshWeaveIcon",sqlEx);
                    }
                    new Logging("INFO",InstallationView.class.getName(),"After Update Old Weave Image",null);
                    //finish button opertion to save data
                    saveProperties(); 
                    windowStage.close();
                    new DbUpdate(); // updates db schema
                    UserLoginView objUserLoginView = new UserLoginView(objConfiguration);                                
                }
                if(stepCompleted==3)
                    buttonNext.setText("Finish !"); 
            }
        });
        
        hbox.getChildren().addAll(buttonPrevious, buttonNext);        
        return hbox;
    }
    private void saveProperties(){
        Properties properties = new Properties();
	
        CONFIG.USERTYPE=(txtloginType.getSelectedToggle().getUserData().toString().equalsIgnoreCase("Anonymous"))?"Guest":"multiuser";
        CONFIG.DBSYNCH=(txtSubscription.isSelected())?"enabled":"disabled";
        CONFIG.DBSSL=(txtDBSecure.isSelected())?"enabled":"disabled";
        CONFIG.DBHOST=(!txtDBHost.getText().equalsIgnoreCase(""))?txtDBHost.getText():"localhost";
        CONFIG.DBPORT=(!txtDBPort.getText().equalsIgnoreCase(""))?txtDBPort.getText():"3306";
        CONFIG.DBNAME=(!txtDBName.getText().equalsIgnoreCase(""))?txtDBName.getText():"bunai";
        CONFIG.DBUSER="openweave";//(!txtDBUsername.getText().equalsIgnoreCase(""))?txtDBUsername.getText():"openweave";
        CONFIG.DBPASSWORD="ml@sia";//(!txtDBPassword.getText().equalsIgnoreCase(""))?txtDBPassword.getText():"ml@sia";
        
        // set the properties value
        properties.setProperty("USERTYPE", CONFIG.USERTYPE);
        properties.setProperty("USERNAME", CONFIG.USERNAME);
        properties.setProperty("PASSWORD", CONFIG.PASSWORD);
        properties.setProperty("EMAIL", CONFIG.EMAIL);
        properties.setProperty("CENTER", CONFIG.CENTER);
        properties.setProperty("LICENCE", CONFIG.LICENCE);
        properties.setProperty("LICENCTYPE", CONFIG.LICENCTYPE);
        properties.setProperty("MACID", CONFIG.MACID);
        properties.setProperty("IP", CONFIG.IP);
        properties.setProperty("PORT", CONFIG.PORT);
        properties.setProperty("DBSYNCH", CONFIG.DBSYNCH);
        properties.setProperty("DBSSL", CONFIG.DBSSL);
        properties.setProperty("DBHOST", CONFIG.DBHOST);
        properties.setProperty("DBPORT", CONFIG.DBPORT);
        properties.setProperty("DBNAME", CONFIG.DBNAME);
        properties.setProperty("DBPREFIX", CONFIG.DBPREFIX);
        properties.setProperty("DBUSER", CONFIG.DBUSER);
        properties.setProperty("DBPASSWORD", CONFIG.DBPASSWORD);
        properties.setProperty("MAILSERVER", CONFIG.MAILSERVER);
        properties.setProperty("MAILPORT", CONFIG.MAILPORT);
        properties.setProperty("MAILUSER", CONFIG.MAILUSER);
        properties.setProperty("MAILPASSWORD", CONFIG.MAILPASSWORD);
        properties.setProperty("MAILAUTH", CONFIG.MAILAUTH);
        properties.setProperty("MAILSTARTTLS", CONFIG.MAILSTARTTLS);
        try (OutputStream resourceAsStream = new FileOutputStream(System.getProperty("user.dir")+"/mla/configuration.properties")){
            // save properties to project root folder            
            if (resourceAsStream != null) {
                properties.store(resourceAsStream, null);
            }
	} catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),"send(): Error writing parameters from configuration.properties",ex);
        } 
    }
        
    public VBox addStepPane(){        
        VBox vbox = new VBox();
        vbox.setPadding(new Insets(10));
        vbox.setSpacing(8);
        
        Text title = new Text("Processes");
        title.setFont(Font.font("Arial", FontWeight.BOLD, 14));
        vbox.getChildren().add(title);

        Hyperlink options[] = new Hyperlink[] {
            new Hyperlink("System Configuration"),
            new Hyperlink("Database Configuration"),
            new Hyperlink("User & Subscription"),
            new Hyperlink("Complete Installation")};
        
        for (int i=0; i<options.length; i++) {
            VBox.setMargin(options[i], new Insets(0, 0, 0, 8));
            if(stepCompleted>i)
                options[i].setGraphic(new ImageView("/media/correct.png"));
            else if(stepCompleted<i)
                options[i].setGraphic(new ImageView("/media/incorrect.png"));
            else
                options[i].setGraphic(new ImageView("/media/neutral.png"));
            vbox.getChildren().add(options[i]);
        }
        return vbox;
    }
    
    public ScrollPane addContentPane() {        
        ScrollPane container = new ScrollPane();
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(0, 10, 0, 10));

        if(stepCompleted==0)
            loadSystemConfiguration(grid);
        else if(stepCompleted==1)
            loadDatabaseConfiguration(grid);
        else if(stepCompleted==2)
            loadUserSubscription(grid);
        else    
            loadCompleteInstallation(grid);
        
        container.setContent(grid);
        return container;           
    }
    private void loadSystemConfiguration(GridPane grid){
        Text osName = new Text("Operating System: ");
        osName.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        grid.add(osName, 0, 1); 
        TextField txtOsName = new TextField(System.getProperty("os.name"));
        txtOsName.setDisable(true);
        txtOsName.setEditable(false);
        grid.add(txtOsName, 1, 1);
        
        Text osVersion = new Text("OS Version: ");
        osVersion.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        grid.add(osVersion, 0, 2); 
        TextField txtOsVersion = new TextField(System.getProperty("os.version"));
        txtOsVersion.setDisable(true);
        txtOsVersion.setEditable(false);
        grid.add(txtOsVersion, 1, 2);

        Text javaVersion = new Text("Java Version: ");
        javaVersion.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        grid.add(javaVersion, 0, 3); 
        TextField txtJavaVersion = new TextField(System.getProperty("java.version"));
        txtJavaVersion.setDisable(true);
        txtJavaVersion.setEditable(false);
        grid.add(txtJavaVersion, 1, 3);

        Text macAddress = new Text("MAC Address: ");
        macAddress.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        grid.add(macAddress, 0, 4);
        TextField txtmacAddress = new TextField(getClientInfo("mac"));
        txtmacAddress.setDisable(true);
        txtmacAddress.setEditable(false);
        grid.add(txtmacAddress, 1, 4);

        Text ipAddress = new Text("IP Address: ");
        ipAddress.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        grid.add(ipAddress, 0, 5);
        TextField txtIPAddress = new TextField(getClientInfo("ip"));
        txtIPAddress.setDisable(true);
        txtIPAddress.setEditable(false);
        grid.add(txtIPAddress, 1, 5);

        Text network = new Text("Network Connection: ");
        network.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        grid.add(network, 0, 6); 
        TextField txtnetwork = new TextField(getClientInfo("connection"));
        txtnetwork.setDisable(true);
        txtnetwork.setEditable(false);
        grid.add(txtnetwork, 1, 6);
        
        getClientInfo("");
    }
    private void loadDatabaseConfiguration(GridPane grid){
        Text dbHost = new Text("DB Host Address: ");
        dbHost.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        grid.add(dbHost, 0, 1); 
        txtDBHost = new TextField("127.0.0.1");
        grid.add(txtDBHost, 1, 1);

        Text dbPort = new Text("DB Port: ");
        dbPort.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        grid.add(dbPort, 0, 2); 
        txtDBPort = new TextField("3306");
        txtDBPort.setDisable(true);
        //txtDBPort.setEditable(false);
        grid.add(txtDBPort, 1, 2);

        Text dbName = new Text("DB Name: ");
        dbName.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        grid.add(dbName, 0, 3); 
        txtDBName = new TextField("DigiBunai");
        grid.add(txtDBName, 1, 3);

        Text dbUsername = new Text("Username: ");
        dbUsername.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        grid.add(dbUsername, 0, 4); 
        txtDBUsername = new TextField("root");
        grid.add(txtDBUsername, 1, 4);
        
        Text dbPassword = new Text("Password: ");
        dbPassword.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        grid.add(dbPassword, 0, 5); 
        txtDBPassword = new TextField("root");
        grid.add(txtDBPassword, 1, 5); 
        
        Text dbSecure = new Text("DB SSL: ");
        dbSecure.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        //grid.add(dbSecure, 0, 6);        
        txtDBSecure = new CheckBox("Secure My Connection");        
        //grid.add(txtDBSecure, 1, 6);
        
        Button btnTest = new Button("Test Connection");
        grid.add(btnTest, 0, 7);
        lblStatus = new Text();
        grid.add(lblStatus, 1, 7);
        
        btnTest.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent ae) {   
                //objAudioClip.play();
                Connection connection = DbConnect.getConnection(txtDBHost.getText(),
                        txtDBPort.getText(),
                        "",
                        txtDBUsername.getText(),
                        txtDBPassword.getText(),
                        txtDBSecure.isSelected());
                if(connection!=null && !txtDBName.getText().trim().equalsIgnoreCase("")){
                     if(!isDBLoaded && !txtDBSecure.isSelected()){
                        //create database queries
                        if(loadDatabase(connection)){                            
                            buttonNext.setDisable(false);
                            lblStatus.setText("Connection Successful !\n Application data updation done");
                            databaseUser(connection);
                            //uploadData(connection);
                            //lblStatus.setText("Connection Successful !\n Update Failed, please contact support team");
                        } else{
                            lblStatus.setText("Connection Successful !\n Update Failed, please contact support team");
                        }
                    } else{
                         lblStatus.setText("Connection Successful !");
                         buttonNext.setDisable(false);
                    }
                } else {
                    lblStatus.setText("Connection Failed");
                    buttonNext.setDisable(true);  
                }
                //objAudioClip.stop();
            }
        });
        buttonNext.setDisable(true);        
    }
    private void loadUserSubscription(GridPane grid){
        Text loginType = new Text("User Type: ");
        loginType.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        grid.add(loginType, 0, 1); 
                
        txtloginType = new ToggleGroup();
        RadioButton anonymousRB = new RadioButton("Anonymous/Guest User Environment");
        anonymousRB.setToggleGroup(txtloginType);
        anonymousRB.setUserData("Anonymous");
        grid.add(anonymousRB, 1, 1);
        Label lblAnonymous = new Label("(Need no login & no data will be avilable for mobile users)");
        lblAnonymous.setFont(Font.font("Arial", FontWeight.THIN, 8));
        lblAnonymous.setWrapText(true);
        grid.add(lblAnonymous, 1, 2);
        RadioButton personalRB = new RadioButton("Secure/Personal User Environment");
        personalRB.setToggleGroup(txtloginType);
        personalRB.setUserData("Secure");
        grid.add(personalRB, 1, 3);
        Label lblPersonal = new Label("(A Multi-User environment : require login for every session)");
        lblPersonal.setFont(Font.font("Arial", FontWeight.THIN, 8));
        lblPersonal.setWrapText(true);
        grid.add(lblPersonal, 1, 4);
        txtloginType.selectToggle(personalRB);
        
        Text registration = new Text("Import Licence: ");
        registration.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        grid.add(registration, 0, 5); 
        txtRegistration = new CheckBox("Upload Portal Registrtation");        
        grid.add(txtRegistration, 1, 5); 
        Label lblRegistration = new Label("If You don't want to register again on application & want to continue\n with portal registration, then upload the licence file");
        lblRegistration.setFont(Font.font("Arial", FontWeight.THIN, 8));
        lblRegistration.setWrapText(true);
        grid.add(lblRegistration, 1, 6);
        
        Text importProfile = new Text("Upload File: ");
        importProfile.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        grid.add(importProfile, 0, 7); 
        final Button btnImportProfile = new Button("Browse");
        btnImportProfile.setDisable(true);
        grid.add(btnImportProfile, 1, 7); 
        Label lblImportProfile = new Label("Upload the licenec file you have downloaded from portal.\nIf You have not downloaded yet, you can download \n it from download section on the https://bunai.medialabasia.in");
        lblImportProfile.setFont(Font.font("Arial", FontWeight.THIN, 8));
        lblImportProfile.setWrapText(true);
        grid.add(lblImportProfile, 1, 8);
        
        Text subscription = new Text("Subscription: ");
        subscription.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        grid.add(subscription, 0, 9); 
        txtSubscription = new CheckBox("Subscribe for data synchronization");        
        grid.add(txtSubscription, 1, 9); 
        Label lblSubscription = new Label("If User subscribe for syncrhonization services then his/her data \nwill be avilable on each & every login to other devices");
        lblSubscription.setFont(Font.font("Arial", FontWeight.THIN, 8));
        lblSubscription.setWrapText(true);
        grid.add(lblSubscription, 1, 10);
        
        txtloginType.selectedToggleProperty().addListener(new ChangeListener<Toggle>(){
            public void changed(ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) {
                 if (txtloginType.getSelectedToggle() != null) {
                     //System.out.println(txtloginType.getSelectedToggle().getUserData().toString());
                     // Do something here with the userData of newly selected radioButton
                     if(txtloginType.getSelectedToggle().getUserData().toString().equalsIgnoreCase("Anonymous")){
                         txtSubscription.setDisable(true);
                         txtSubscription.setSelected(false);
                         txtRegistration.setDisable(true);
                         txtRegistration.setSelected(false);                        
                     }else{
                         txtSubscription.setDisable(false);
                         txtRegistration.setDisable(false);
                     }
                 }
             } 
        }); 
        txtRegistration.selectedProperty().addListener(new ChangeListener<Boolean>() {
           public void changed(ObservableValue<? extends Boolean> ov,Boolean old_val, Boolean new_val) {
                //System.out.println(txtRegistration.isSelected());
                // Do something here with the userData of newly selected radioButton
                if(txtRegistration.isSelected()){
                    btnImportProfile.setDisable(false);
                    buttonNext.setDisable(true);
                }else{
                    btnImportProfile.setDisable(true);
                    buttonNext.setDisable(false);
                }
            }
        });
        btnImportProfile.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                FileChooser objFileChooser = new FileChooser();
                objFileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("BUN(.bun)","*.bun"));
                File selectedFile = objFileChooser.showOpenDialog(windowStage);
                if(selectedFile == null){
                    buttonNext.setDisable(true);
                    return;
                }else{
                    String content = "";
                    try (InputStream in = new FileInputStream(selectedFile)) {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                        String line;
                        content = "";
                        while ((line = reader.readLine()) != null) {
                            content+="\n"+line;
                        }   reader.close();
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                    }
                    //decode the content
                    // Get bytes from string
                    byte[] byteArray = Base64.decode(content.getBytes());
                    // Print the decoded array
                    //System.out.println(Arrays.toString(byteArray));
                    // Print the decoded string 
                    String decodedString = new String(byteArray);
                    //System.out.println(content + " = " + decodedString);
                    
                    objUser = new User();
                    StringTokenizer section;
                    String query = "INSERT INTO `mla_users` (`userid`,`username`,`password`,`app_password`,`sys_password`,`name`,`email`,`organization`,`contact_number`,`geo_city`,`geo_lat`,`geo_lng`,`address`,`city`,`state`,`country`,`pincode`,`occupation`,`education`,`gender`,`dob`,`block`,`is_enable`,`is_active`,`is_synchronization`,`registerDate`,`lastvisitDate`,`updateDate`,`sendEmail`,`activation`,`params`,`lastResetTime`,`resetCount`,`otpKey`,`otep`,`requireReset`) VALUES (";
                    section=new StringTokenizer(decodedString,"@@@@@");
                    while(section.hasMoreTokens()) {
                        String sectionTemp=section.nextToken();
                        //System.err.println(sectionTemp);
                        if(sectionTemp.contains("=")){
                            String key=sectionTemp.substring(0,sectionTemp.indexOf("="));
                            String value=sectionTemp.substring(sectionTemp.indexOf("=")+1);
                            //System.err.println(key+"<=>"+value);
                            if(!key.equalsIgnoreCase("GROUP_CONCAT(t2.group_id)"))
                                //user table data
                                query+="`"+value+"`,";
                            else{
                                //user_group_map table data
                                //user_groups = sectionTemp.substring(sectionTemp.indexOf("=")+1);
                                StringTokenizer subSection = new StringTokenizer(value,",");  
                                userGroups = new int[subSection.countTokens()];
                                int i = 0;
                                while(subSection.hasMoreTokens()) {
                                    userGroups[i] = Integer.parseInt(subSection.nextToken());
                                }

                                //alternaet way
                                if(key.equalsIgnoreCase("userid"))
                                    objUser.setStrUserID(new IDGenerator().getIDGenerator("USER_LIBRARY", CONFIG.LICENCE));
                                else if(key.equalsIgnoreCase("username"))
                                    objUser.setStrUsername(value);
                                else if(key.equalsIgnoreCase("password"))
                                    objUser.setStrPassword(value);
                                else if(key.equalsIgnoreCase("app_password"))
                                    objUser.setStrAppPassword(value);
                                else if(key.equalsIgnoreCase("name"))
                                    objUser.setStrName(value);
                                else if(key.equalsIgnoreCase("email"))
                                    objUser.setStrEmailID(value);
                                else if(key.equalsIgnoreCase("contact_number"))
                                    objUser.setStrContactNumber(value);
                                else if(key.equalsIgnoreCase("address"))
                                    objUser.setStrAddress(value);
                                else if(key.equalsIgnoreCase("city"))
                                    objUser.setStrCityAddress(value);
                                else if(key.equalsIgnoreCase("state"))
                                    objUser.setStrStateAddress(value);
                                else if(key.equalsIgnoreCase("country"))
                                    objUser.setStrCountryAddress(value);
                                else if(key.equalsIgnoreCase("pincode"))
                                    objUser.setStrPincodeAddress(value);                      
                            }
                        }
                    }
                    query = query.substring(0, query.length() - 1);
                    query+=");";                    
                    //System.err.println("query"+userGroups.length+"="+query); 
                    /*                    
                    content = 'INSERT INTO `mla_users` (`id`,`userid`,`username`,`password`,`app_password`,`sys_password`,`name`,`email`,`organization`,`contact_number`,`geo_city`,`geo_lat`,`geo_lng`,`address`,`city`,`state`,`country`,`pincode`,`occupation`,`education`,`gender`,`dob`,`block`,`is_enable`,`is_active`,`is_synchronization`,`registerDate`,`lastvisitDate`,`updateDate`,`sendEmail`,`activation`,`params`,`lastResetTime`,`resetCount`,`otpKey`,`otep`,`requireReset`)
				VALUES ('.$this->users_table[0].',"'.$this->users_table[1].'","'.$this->users_table[2].'","'.$this->users_table[3].'","'.$this->users_table[4].'","'.$this->users_table[5].'","'.$this->users_table[6].'","'.$this->users_table[7].'","'.$this->users_table[8].'","'.$this->users_table[9].'","'.$this->users_table[10].'","'.$this->users_table[11].'","'.$this->users_table[12].'","'.$this->users_table[13].'","'.$this->users_table[14].'","'.$this->users_table[15].'","'.$this->users_table[16].'","'.$this->users_table[17].'","'.$this->users_table[18].'","'.$this->users_table[19].'","'.$this->users_table[20].'","'.$this->users_table[21].'","'.$this->users_table[22].'","'.$this->users_table[23].'","'.$this->users_table[24].'","'.$this->users_table[25].'","'.$this->users_table[26].'","'.$this->users_table[27].'","'.$this->users_table[28].'","'.$this->users_table[29].'","'.$this->users_table[30].'","'.$this->users_table[31].'","'.$this->users_table[32].'","'.$this->users_table[33].'","'.$this->users_table[34].'","'.$this->users_table[35].'","'.$this->users_table[36].'");';
                    //load data from user_group table
                    for($i = 0;  $i<sizeof($this->usergroup_table) ; $i++ ) { 
			content = $content.'INSERT INTO `mla_user_usergroup_map` (`user_id`,`group_id`) VALUES ('.$this->usergroup_table[0]["user_id"].','.$this->usergroup_table[1]["group_id"].');';
                    }  
                    */
                    
                    final Path path = Paths.get(System.getProperty("user.dir").replace("\\","/")+"/mla/user.sql");
                    try(final BufferedWriter writer = Files.newBufferedWriter(path,StandardCharsets.UTF_8, StandardOpenOption.CREATE);) {
                        writer.write(query);
                        writer.flush();
                    } catch (IOException ex) {
                        Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                    }
                    buttonNext.setDisable(false);
                }
                //System.err.println(selectedFile.getAbsolutePath());
            }
        });  
    }    
    private void loadCompleteInstallation(GridPane grid){
        //objAudioClip.play();
        buttonNext.setDisable(true);
        buttonPrevious.setDisable(true);
        // added to notify user for waiting till database queries are processing
        Stage waitStage = new Stage();
        waitStage.setTitle("Please Wait till Finish Button appears.");
        waitStage.setHeight(20);
        waitStage.setWidth(300);
        waitStage.initStyle(StageStyle.UTILITY);
        waitStage.show();
        //upload database queryies
        try {
            LoadData objLoadData = null;
            if(!txtDBSecure.isSelected()){
                objLoadData = new LoadData(txtDBName.getText(), txtDBUsername.getText(),txtDBPassword.getText());
                objLoadData.start();
            }
            Text osname = new Text("Thank You for chosing DigiBunai ! \n\n\n\n\n DigiBunai Team \n Digital India Corporation");
            osname.setFont(Font.font("Arial", FontWeight.BOLD, 20));
            grid.add(osname, 0, 1);
            lblStatus = new Text();
            grid.add(lblStatus, 0, 2);        
            if(objLoadData!=null){
                objLoadData.join();
            }
            if(isDBLoaded){ 
                if(txtRegistration.isSelected())
                    saveUserLogin();
                buttonNext.setDisable(false);
            }
        } catch (InterruptedException | SQLException/**/ ex) {
            lblStatus.setFill(Color.FIREBRICK);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            new Logging("SEVERE",getClass().getName(),"Error Occured While exiting installation"+ex.getMessage(),ex);
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        //objAudioClip.stop();
        waitStage.close();
    }
    private boolean isPrerequistSET(){
        String[] executeCmd = new String[]{"mysql", "--version"};
        //System.err.println("Command: "+executeCmd);
        /*NOTE: Executing the command here*/            
        int processComplete = -1;
        try{            
            /*NOTE: processComplete=0 if correctly executed, will contain other values if not*/
            Process runtimeProcess = Runtime.getRuntime().exec(executeCmd);
            /*
            InputStream inputStream = proc.getInputStream();    
            String output = IOUtils.toString(inputStream);
            System.err.println("Error= "+output);            
            */
            //processComplete = runtimeProcess.exitValue();
            processComplete = runtimeProcess.waitFor();
            //System.err.println("Result Status: "+processComplete);
            /*NOTE: processComplete=0 if correctly executed, will contain other values if not*/
            if (processComplete == 0)
                return true;
             else 
                return false;            
        }catch (IOException | InterruptedException ex){
            //System.err.println("Error:"+ex.getStackTrace());
            //Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);            
            return false;
        }  catch (Throwable t) {
            //t.printStackTrace();
            return false;
        }
    }
    public static String getClientInfo(String type){	
        InetAddress ip;
        String output = "";
	try {
            ip = InetAddress.getLocalHost();
            NetworkInterface network = NetworkInterface.getByInetAddress(ip);
            if(type.equalsIgnoreCase("connection")){
                String strUrl = "http://www.google.com";
                output = (isInternetReachable(strUrl))?"Connected":"Not Connected";
            } if(type.equalsIgnoreCase("ip")){
                output = ip.getHostAddress();
                //System.out.println("Current IP address : " + ip.getHostAddress());
                //System.out.println("Current IP Name : " + ip.getHostName());
            } else  if(type.equalsIgnoreCase("mac")){
                byte[] mac = network.getHardwareAddress();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < mac.length; i++) {
                    sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
                }
                output = sb.toString();
                //System.out.println("Current MAC address : "+sb.toString());
            } else {                
                //System.out.println("Current getDisplayName() : "+network.getDisplayName() );	
                //System.out.println("Current getName() : "+network.getName() );	
                //System.out.println("Current getMTU()  : "+network.getMTU() );	
                //System.out.println("Current isVirtual()  : "+network.isVirtual() );                
            }
	} catch (UnknownHostException | SocketException e) {
            output = "Unknown";
            //e.printStackTrace();
	} catch (IOException e){			
            output = "Unknown";
            //e.printStackTrace();
	}
        return output;
   }
   //checks for connection to the internet through dummy request
    public static boolean isInternetReachable(String strUrl) throws UnknownHostException, IOException {
        //make a URL to a known source
        URL url = new URL(strUrl);
        //open a connection to that source
        HttpURLConnection urlConnect = (HttpURLConnection)url.openConnection();
        //trying to retrieve data from the source. If there
        //is no connection, this line will fail
        Object objData = urlConnect.getContent();
        return true;
    } 
    private boolean loadDatabase(Connection connection){
        try(Statement stmt = connection.createStatement()) {
            //Statement stmt = connection.createStatement();
            String query1 = "SET GLOBAL max_connections = 5000;";
            String query2 = "SET GLOBAL max_allowed_packet = 16776192;";            
            String query3 = "DROP DATABASE IF EXISTS "+txtDBName.getText()+";"; 
            String query4 = "CREATE DATABASE "+txtDBName.getText()+";"; 
            String query5 = "USE "+txtDBName.getText()+";"; 
            //import data to database  using query
            //String query6 = "SOURCE "+System.getProperty("user.dir").replace("\\","/")+"/mla/bunai.sql;";
            //String query6 = "SOURCE '"+System.getProperty("user.dir")+"\\mla\\bunai.sql';";
            stmt.addBatch(query1);
            stmt.addBatch(query2);
            stmt.addBatch(query3);
            stmt.addBatch(query4);
            stmt.addBatch(query5);
            //stmt.addBatch(query6);
            stmt.executeBatch();
            return true;
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"Error Occured While setup db"+ex.getMessage(),ex);
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    private void databaseUser(Connection connection){
        try(Statement stmt = connection.createStatement()) {
            //Statement stmt = connection.createStatement();
            String query1 = "DROP USER 'openweave'@'localhost';"; 
            String query2 = "FLUSH PRIVILEGES;";
            String query3 = "CREATE USER 'openweave'@'localhost' IDENTIFIED BY 'ml@sia';";
            String query4 = "GRANT ALL ON *.* TO 'openweave'@'localhost';";
            stmt.addBatch(query1);
            stmt.addBatch(query2);
            stmt.addBatch(query3);
            stmt.addBatch(query4);
            stmt.executeBatch();
            //import data to database 
            /*
            mysql -h localhost -u openweave --password=ml@sia bunai < bunai.sql
            */ 
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"Error Occured While restore db users"+ex.getMessage(),ex);
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);            
        }
    }
    private void uploadData(Connection connection) {
        try {            
            ScriptRunner runner = new ScriptRunner(connection, false, true);
            runner.runScript(new BufferedReader(new FileReader(System.getProperty("user.dir").replace("\\","/")+"/mla/bunai.sql")));   
        } catch (IOException | HeadlessException | SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"Error Occured While restore db from sql"+ex.getMessage(),ex);
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void saveUserLogin() throws SQLException{         
        UserAction objUserAction = new UserAction();
        int intUID=objUserAction.addUser(objUser);
        if(intUID!=-1){     // Record Added in mla_users successfully
            new UserAction().addUserTnC(objUser.getStrUserID());        
            // adding mla_user_usergroup_map table with same userid
            for(int i=0; i<userGroups.length;i++)
                new UserAction().addUserGroupMap(intUID, userGroups[i]);
            // adding mla_user_preference table with same userid
            objConfiguration.intializeConfiguration();
            objUser.setUserAccess(objConfiguration.getObjUser().getUserAccess());
            objConfiguration.setObjUser(objUser);
            new UserAction().addUserPrefrence(objConfiguration);
            new UserAction().addUserAccess(objConfiguration);
            // updating mla_user_configuration table with same userid
            //File[] files = new File("/data/clothsettings/").listFiles();
            String[] files = new String[7];
            files[0]=new String("data/clothsettings/Blouse.properties");
            files[1]=new String("data/clothsettings/Body.properties");
            files[2]=new String("data/clothsettings/Border.properties");
            files[3]=new String("data/clothsettings/CrossBorder.properties");
            files[4]=new String("data/clothsettings/Konia.properties");
            files[5]=new String("data/clothsettings/Palu.properties");
            files[6]=new String("data/clothsettings/Skirt.properties");
            //If this pathname does not denote a directory, then listFiles() returns null. 
            int confNum=1;//CONF#
            for (String file : files) {
                if (file!=null) {
                    objConfiguration.intializeClothConfiguration(file);
                    objConfiguration.setStrConfName("CONF"+objUser.getStrUserID()+(confNum++));
                    new UserAction().addUserConfiguration(objConfiguration);
                }
            }                
        } else{
            lblStatus.setFill(Color.FIREBRICK);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }	
    }    
    /**
     * exitMenuAction
     * <p>
     * Function use for exit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void exitMenuAction(){
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.setIconified(false);
        dialogStage.setFullScreen(false);
        dialogStage.setTitle(objDictionaryAction.getWord("ALERT"));
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 300, 100, Color.WHITE);
        scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        popup.add(new ImageView("/media/alert_warning.png"), 0, 0);   
        Label lblAlert = new Label(objDictionaryAction.getWord("ALERTCLOSE"));
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(250);
        popup.add(lblAlert, 1, 0);
        Button btnYes = new Button(objDictionaryAction.getWord("YES"));
        btnYes.setPrefWidth(50);
        btnYes.setId("btnYes");
        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
                windowStage.close();
            }
        });
        popup.add(btnYes, 0, 1);
        Button btnNo = new Button(objDictionaryAction.getWord("NO"));
        btnNo.setPrefWidth(50);
        btnNo.setId("btnNo");
        btnNo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
            }
        });
        popup.add(btnNo, 1, 1);
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.showAndWait();
        System.gc();
    }
     
    @Override
    public void start(Stage stage) throws Exception {
        new InstallationView(stage);
        new Logging("WARNING",getClass().getName(),"UnsupportedOperationException",new UnsupportedOperationException("Not supported yet."));
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

class LoadData extends Thread{    
    String dbName, dbUsername, dbPassword;
    public LoadData(String dbName, String dbUsername, String dbPassword){
        this.dbName = dbName;
        this.dbUsername = dbUsername;
        this.dbPassword = dbPassword;
    }    
    public void run(){
        //import data to database  using cmd
        /*mysql -h localhost -u openweave --password=ml@sia bunai < bunai.sql */
        String[] executeCmd = new String[]{"mysql", this.dbName, "-u" + this.dbUsername, "-p" + this.dbPassword, "-e", " source " + System.getProperty("user.dir").replace("\\","/")+"/mla/bunai.sql"};
        if(this.dbPassword.trim().length()==0)executeCmd = new String[]{"mysql", this.dbName, "-u" + this.dbUsername, "-e", " source " + System.getProperty("user.dir").replace("\\","/")+"/mla/bunai.sql"};
        //System.err.println("Command: "+executeCmd);
        /*NOTE: Executing the command here*/            
        int processComplete = -1;
        try{
            /*NOTE: processComplete=0 if correctly executed, will contain other values if not*/
            Process runtimeProcess = Runtime.getRuntime().exec(executeCmd);
            processComplete = runtimeProcess.waitFor();
        }catch (IOException | InterruptedException ex){
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }catch(Throwable ex){
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        //System.err.println("Result Status: "+processComplete);
        /*NOTE: processComplete=0 if correctly executed, will contain other values if not*/
        if (processComplete == 0) {
            InstallationView.isDBLoaded = true;
            //System.out.println("Import Complete"+InstallationView.isDBLoaded);
        } else {
            InstallationView.isDBLoaded = false;
            //System.out.println("Import Failure"+InstallationView.isDBLoaded);
        }
    }
}