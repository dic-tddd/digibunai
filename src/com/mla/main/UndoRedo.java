/*
 * Copyright (C) 2017 Media Lab Asia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mla.main;

import com.mla.main.Logging;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @Designing undo redo function
 * @author Amit Kumar Singh
 * 
 */
public class UndoRedo {
    private List<String> commands;
    private List<Object> datas;
    private int top = 0;
    private String strTag;
    private Object objData;
    
    public UndoRedo(){
        commands = new ArrayList<>();
        datas = new ArrayList<>();
    }
    public void setStrTag(String objString){
        this.strTag= objString;
    }
    public String getStrTag(){
        return this.strTag;
    }
    
    public void setObjData(Object objObject){
        this.objData= objObject;
    }
    public Object getObjData(){
        return this.objData;
    }
    
    public void doCommand(String objString, Object objObject) {
        //System.out.println("Pre - Command: Top="+top+", Size="+commands.size());
        //print();
        List<String> newStringList = new ArrayList<>(top + 1);
        for(int k = 0; k < top; k++) {
            newStringList.add(commands.get(k));
        }
        newStringList.add(objString);
        commands = newStringList;
        
        List<Object> newObjectList = new ArrayList<>(top + 1);
        for(int k = 0; k < top; k++) {
            newObjectList.add(datas.get(k));
        }
        newObjectList.add(objObject);
        datas = newObjectList;
        top++;
        //clearTopItems();
        //System.out.println("Post - Command: Top="+top+", Size="+commands.size());
        //print();
        //System.out.println("Command= "+top);
    }
    
    private void clearTopItems(){
        for(int i=commands.size()-1; i>=top; i--){
            //System.out.println("Removing i="+i);
            datas.remove(i);
            commands.remove(i);
        }
    }
    
    public void clear() {
        commands.clear();
        datas.clear();
        top=0;
        //System.out.println("Clear= "+top);
    }

    public boolean canUndo() {
        return top > 1;
    }
    
    public boolean canRedo() {
        return top < commands.size();
    }

    public void undo() {
        //System.out.println("Pre Undo: Top="+top+", Size="+commands.size());
        //print();
        if(canUndo()) {
            //System.out.println("undo= "+top);
            top--;
            setStrTag(commands.get(top-1));
            setObjData(datas.get(top-1));
         } else {
            new Logging("SEVERE",UndoRedo.class.getName(),"Cannot undo"+top, new IllegalStateException("Cannot undo"+top));
         }
        //System.out.println(" Post Undo: Top="+top+", Size="+commands.size());
        //print();
    }

    public void redo() {
        //System.out.println("Pre Redo: Top="+top+", Size="+commands.size());
        //print();
        if(canRedo()) {
            //System.out.println("Redo= "+top);
            //System.err.println("Top: "+top+"::"+commands.get(top));
            setStrTag(commands.get(top));
            setObjData(datas.get(top));
            top++;
        } else {
            new Logging("SEVERE",UndoRedo.class.getName(),"Cannot redo"+top, new IllegalStateException("Cannot redo"+top)); 
        }
        //System.out.println("Post Redo: Top="+top+", Size="+commands.size());
        //print();
    }
    
    private void print(){
        for(int i=0; i< commands.size(); i++)
            System.out.println("Command "+i+": "+commands.get(i));
    }
}