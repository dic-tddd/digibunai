/*
 * Copyright (C) Digital India Corporation (Media Lab Asia)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mla.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

public class QRCode {

    public static final String charset = "UTF-8"; // or "ISO-8859-1"
    public static Map hintMap = null;
    
    public static void main(String[] args) {
        QRCode objQRCode = new QRCode();
    }
    public QRCode(){    
       try {    
            hintMap = new HashMap();
            hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
            
            String qrCodeData = "DigiBunai ! developed by Digital India Corporation";
            qrCodeData = "[WIF]\n" +
"Version=1.2\n" +
"Date=9/28/2015\n" +
"Developers=wif@handweaving.net\n" +
"Source Program=Handweaving.net Draft Library\n" +
"Source Version=3.0\n" +
"[CONTENTS]\n" +
"COLOR PALETTE=yes\n" +
"WEAVING=yes\n" +
"WARP=yes\n" +
"WEFT=yes\n" +
"TIEUP=yes\n" +
"COLOR TABLE=yes\n" +
"THREADING=yes\n" +
"WARP COLORS=yes\n" +
"TREADLING=yes\n" +
"WEFT COLORS=yes\n" +
"[WEAVING]\n" +
"Shafts=8\n" +
"Treadles=8\n" +
"Rising Shed=yes\n" +
"Profile=no\n" +
"[COLOR PALETTE]\n" +
"Entries=2\n" +
"Form=RGB\n" +
"Range=0,255\n" +
"[COLOR TABLE]\n" +
"1=255,204,102\n" +
"2=255,255,204\n" +
"[WARP]\n" +
"Threads=32\n" +
"Units=Centimeters\n" +
"Spacing=0.0185\n" +
"Thickness=0.0213\n" +
"[WEFT]\n" +
"Threads=32\n" +
"Units=Centimeters\n" +
"Spacing=0.0185\n" +
"Thickness=0.0213\n" +
"[TIEUP]\n" +
"1=1,2,3,4\n" +
"2=5,6,7,8\n" +
"3=1,2,3,4\n" +
"4=5,7,6,8\n" +
"5=1,2,3,4\n" +
"6=5,6,7,8\n" +
"7=1,2,3,4\n" +
"8=5,6,7,8\n" +
"[THREADING]\n" +
"1=1\n" +
"2=2\n" +
"3=3\n" +
"4=4\n" +
"5=5\n" +
"6=6\n" +
"7=7\n" +
"8=8\n" +
"9=1\n" +
"10=2\n" +
"11=3\n" +
"12=4\n" +
"13=5\n" +
"14=6\n" +
"15=7\n" +
"16=8\n" +
"17=1\n" +
"18=2\n" +
"19=3\n" +
"20=4\n" +
"21=5\n" +
"22=6\n" +
"23=7\n" +
"24=8\n" +
"25=1\n" +
"26=2\n" +
"27=3\n" +
"28=4\n" +
"29=5\n" +
"30=6\n" +
"31=7\n" +
"32=8\n" +
"[TREADLING]\n" +
"1=1\n" +
"2=1\n" +
"3=1\n" +
"4=1\n" +
"5=2\n" +
"6=2\n" +
"7=2\n" +
"8=2\n" +
"9=3\n" +
"10=3\n" +
"11=3\n" +
"12=3\n" +
"13=4\n" +
"14=4\n" +
"15=4\n" +
"16=4\n" +
"17=5\n" +
"18=5\n" +
"19=5\n" +
"20=5\n" +
"21=6\n" +
"22=6\n" +
"23=6\n" +
"24=6\n" +
"25=7\n" +
"26=7\n" +
"27=7\n" +
"28=7\n" +
"29=8\n" +
"30=8\n" +
"31=8\n" +
"32=8\n" +
"[WARP COLORS]\n" +
"1=2\n" +
"2=2\n" +
"3=2\n" +
"4=2\n" +
"5=2\n" +
"6=2\n" +
"7=2\n" +
"8=2\n" +
"9=2\n" +
"10=2\n" +
"11=2\n" +
"12=2\n" +
"13=2\n" +
"14=2\n" +
"15=2\n" +
"16=2\n" +
"17=2\n" +
"18=2\n" +
"19=2\n" +
"20=2\n" +
"21=2\n" +
"22=2\n" +
"23=2\n" +
"24=2\n" +
"25=2\n" +
"26=2\n" +
"27=2\n" +
"28=2\n" +
"29=2\n" +
"30=2\n" +
"31=2\n" +
"32=2\n" +
"[WEFT COLORS]\n" +
"1=1\n" +
"2=1\n" +
"3=1\n" +
"4=1\n" +
"5=1\n" +
"6=1\n" +
"7=1\n" +
"8=1\n" +
"9=1\n" +
"10=1\n" +
"11=1\n" +
"12=1\n" +
"13=1\n" +
"14=1\n" +
"15=1\n" +
"16=1\n" +
"17=1\n" +
"18=1\n" +
"19=1\n" +
"20=1\n" +
"21=1\n" +
"22=1\n" +
"23=1\n" +
"24=1\n" +
"25=1\n" +
"26=1\n" +
"27=1\n" +
"28=1\n" +
"29=1\n" +
"30=1\n" +
"31=1\n" +
"32=1";
            String filePath = System.getProperty("user.dir")+"/mla/qrcode/MyQRCode5.png";            
            createQRCode(qrCodeData, filePath, 200, 200);
            System.out.println("QR Code image created successfully!");            
            //add logo to qr code
            
            
            
            
            
            filePath = System.getProperty("user.dir")+"/mla/qrcode/MyQRCode1.png";
            //File file = new File(filePath);
            //String decodedText = decodeQRCode(file);
            String decodedText = readQRCode(filePath);
            if(decodedText == null) {
                System.out.println("There is No QR Code found in the image");
            } else {
                System.out.println("Data read from QR Code: " + decodedText);
            }
        } catch (WriterException | IOException | NotFoundException ex) {
            System.out.println("Could not decode QR Code, IOException :: " + ex.getMessage());
        }
    }

    public static void createQRCode(String qrCodeData, String filePath, int qrCodeheight, int qrCodewidth) throws WriterException, IOException {
        BitMatrix matrix = new MultiFormatWriter().encode(new String(qrCodeData.getBytes(charset), charset),BarcodeFormat.QR_CODE, qrCodewidth, qrCodeheight, hintMap);
        MatrixToImageWriter.writeToFile(matrix, filePath.substring(filePath.lastIndexOf('.') + 1), new File(filePath));
    }

    public static String readQRCode(String filePath) throws FileNotFoundException, IOException, NotFoundException {
        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(ImageIO.read(new FileInputStream(filePath)))));
        Result qrCodeResult = new MultiFormatReader().decode(binaryBitmap,hintMap);
        return qrCodeResult.getText();
    }
      /*  
    private static String decodeQRCode(File qrCodeimage) throws IOException, NotFoundException {
        BufferedImage bufferedImage = ImageIO.read(qrCodeimage);
        LuminanceSource source = new BufferedImageLuminanceSource(bufferedImage);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        Result result = new MultiFormatReader().decode(bitmap);
        return result.getText();        
    }*/
}