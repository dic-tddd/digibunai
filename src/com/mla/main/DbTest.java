/*
 * Copyright (C) Digital India Corporation ( Media Lab Asia )
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mla.main;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @Designing model function for weave
 * @author Amit Kumar Singh
 * 
 */
public class DbTest {
    
    Connection connection = null; //DbConnect.getConnection();
    static Runtime runtime = Runtime.getRuntime();
    static long totalMemory=0L;
    static long freeMemory=0L;
    /**
     * DbTest
     * <p>
     * This method is used for create database connection.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for creating database connection.
     * @see         java.sql.*;
     * @throws      SQLException
     */
    public DbTest() throws SQLException{
        //runtime = Runtime.getRuntime();
        connection = DbConnect.getConnection();
    }    
    /**
     * close
     * <p>
     * This method is used for destroying database connection.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for closing database connection.
     * @see         java.sql.*;
     * @throws      SQLException
     */
    public void close() throws SQLException{
        connection.close();
    }
    
    public ResultSet executeSql(String strQuery){
        Statement oStatement =null;
        ResultSet oResultSet= null; 
        
        try {
            totalMemory = runtime.freeMemory();
            oStatement = connection.createStatement();
            oResultSet = oStatement.executeQuery(strQuery); 
            freeMemory = runtime.freeMemory();
        } catch (Exception ex) {
            new Logging("SEVERE",DbTest.class.getName(),"doCount() : "+strQuery,ex);
        } finally {
            try {                
                if(oStatement!=null) {
                    oStatement.close();
                    oStatement=null;
                }
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",DbTest.class.getName(),"doCount() : Error while closing connection"+e,ex);
                }
            }
        }
        return oResultSet;
    }
    
    public void doSelect(String strTableName,String ...strOrderBy ) throws SQLException{
        ResultSet oResultSet=null;
        String strQuery=null;        
        //System.out.println("<<<<<< doCount >>>>>>");
        strQuery = "SELECT * from "+strTableName+" WHERE 1;";
        oResultSet = executeSql(strQuery);
        /*while(oResultSet.next()) {
            //oResultSet.getFetchSize();
        }          
        */
        //System.out.println("<<<<<< doCount >>>>>>"+count);        
    }    
    public int doCount(String strTableName, String ...strGroupBy) throws SQLException{ 
        ResultSet oResultSet=null;
        String strQuery=null;
        int count = 0;
        //System.out.println("<<<<<< doCount >>>>>>");
        strQuery = "SELECT COUNT(*) from "+strTableName+" WHERE 1;";
        oResultSet = executeSql(strQuery);
        /*while(oResultSet.next()) {
            count += oResultSet.getInt(1);
        }            
        */
        //System.out.println("<<<<<< doCount >>>>>>"+count);
        return count;
    }
    public void doUpdate(){
        
    }
    public void doMassInsert(){
        //INSERT INTO table2 SELECT * FROM table1 WHERE condition;
    }
    public void doTableReplica(String strTableName, String strNewTableName) throws SQLException{
        //CREATE TABLE new_table AS SELECT *  FROM   old_table;
        ResultSet oResultSet=null;
        String strQuery=null;
        int count = 0;
        //System.out.println("<<<<<< doTableReplica >>>>>>");
        strQuery = "CREATE TABLE "+strNewTableName+" AS SELECT *  FROM "+strTableName+";";
        oResultSet = executeSql(strQuery);        
        //System.out.println("<<<<<< doTableReplica >>>>>>"+count);
    }
    
    public void observTime(){
        int mb = 1024*1024;
        //Getting the runtime reference from system
        System.out.println("Maximum Available Memory:"+(runtime.maxMemory() / mb));   
        System.out.println("Total Available Memory:"+(runtime.totalMemory() / mb));   
        long diffTime = 0L;
        long diffMemory = 0L;
        long start = System.currentTimeMillis();
        long stop = System.currentTimeMillis();
        System.out.println("Observation starts at : "+start);
        System.out.println("Observation starts free memory : "+(runtime.freeMemory() / mb));
        try{
            start = System.currentTimeMillis();
            totalMemory = runtime.freeMemory();
            DbTest objDbTest = new DbTest();             
            freeMemory = runtime.freeMemory();
            stop = System.currentTimeMillis();
            diffTime = (stop - start);
            diffMemory = (totalMemory-freeMemory);
            System.out.println("Data Connection Time in ms : "+diffTime+" ("+start+" to "+stop+")");
            System.out.println("Data Connection Memmory in bytes : "+diffMemory+" ("+totalMemory+" to "+freeMemory+")");
            
            start = System.currentTimeMillis();
            
            totalMemory = runtime.freeMemory();
            objDbTest.close();            
            freeMemory = runtime.freeMemory();            
            stop = System.currentTimeMillis();
            diffTime = (System.currentTimeMillis() - start);
            diffMemory = (totalMemory-freeMemory);
            System.out.println("Data Connection closing Time in ms : "+diffTime+" ("+start+" to "+stop+")");
            System.out.println("Data Connection closing Memmory in bytes : "+diffMemory+" ("+totalMemory+" to "+freeMemory+")");
           
            start = System.currentTimeMillis();
            objDbTest = new DbTest();
            objDbTest.doCount("mla_weave_library");
            stop = System.currentTimeMillis();
            diffTime = (System.currentTimeMillis() - start);
            diffMemory = (totalMemory-freeMemory);
            System.out.println("Data count Time in ms : "+diffTime+" ("+start+" to "+stop+")");
            System.out.println("Data count Memmory in bytes : "+diffMemory+" ("+totalMemory+" to "+freeMemory+")");
           
            
                
            start = System.currentTimeMillis();
            objDbTest = new DbTest();
            objDbTest.doSelect("mla_weave_library");
            stop = System.currentTimeMillis();
            diffTime = (System.currentTimeMillis() - start);
            diffMemory = (totalMemory-freeMemory);
            System.out.println("Data fetch Time in ms : "+diffTime+" ("+start+" to "+stop+")");
            System.out.println("Data fetch Memmory in bytes : "+diffMemory+" ("+totalMemory+" to "+freeMemory+")");
           
        } catch (Exception ex) {
            new Logging("SEVERE",DbTest.class.getName(),ex.getMessage(),ex);
            Logger.getLogger(DbTest.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        stop = System.currentTimeMillis();
        System.out.println("Observation stops at : "+stop);
        System.out.println("Observation stops free memory : "+(runtime.freeMemory() / mb));        
    }
}
