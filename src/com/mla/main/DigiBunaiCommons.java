package com.mla.main;

import java.util.StringTokenizer;

/**
 * Common functions
 * @since v0.9.6
 * @author Aatif Ahmad Khan
 */
public class DigiBunaiCommons {
    
    /**
     * Used in numeric graph, this function replace three or more consecutive
     * numbers with hyphen in between.
     * @since v0.9.6
     * @author Aatif Ahmad Khan
     * @see FabricView.exportTxtAction(), FabricView.saveAsTxtGraph(), ArtworkView.exportText()
     * @param strLineData Line String with comma separated numbers (1,5,7,8,9)
     * @return Line String replacing continuous number with hyphens in between (1,4,7-9)
     */
    public static String replaceStrWithHyphen(String strLineData){
        String strReturn = "";
        StringTokenizer st = new StringTokenizer(strLineData, ",");
        int firstNum = Integer.parseInt(st.nextToken());
        int lastNum = firstNum;
        int currentNum = -1;
        while(st.hasMoreTokens()){
            currentNum = Integer.parseInt(st.nextToken());
            if(currentNum == (lastNum+1)){
                lastNum = currentNum;
            } else{
                if(lastNum == firstNum)
                    strReturn += (lastNum+",");
                else if(lastNum == (firstNum+1))
                    strReturn += (firstNum+","+lastNum+",");
                else
                    strReturn += (firstNum+"-"+lastNum+",");
                firstNum = lastNum = currentNum;
            }
        }
        // remaining numbers
        if(lastNum == firstNum)
            strReturn += (lastNum+",");
        else if(lastNum == (firstNum+1))
            strReturn += (firstNum+","+lastNum+",");
        else
            strReturn += (firstNum+"-"+lastNum+",");
        return strReturn.substring(0, strReturn.length()-1);
    }
    
}
