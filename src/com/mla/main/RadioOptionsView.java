
package com.mla.main;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

/**
 * Dialog for Radio Options (select any one)
 * @since v0.9.6
 * @author Aatif Ahmad Khan
 */
public class RadioOptionsView {
    
    public int optionRBValue = -1; // selected option index (-1 is no selection)
    
    public RadioOptionsView(String[] labels, String[] tooltips){
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.setTitle("Select an option:");
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setAlignment(Pos.CENTER);
        popup.setHgap(10);
        popup.setVgap(10);
        popup.setPadding(new Insets(10, 10, 10, 10));
        
        for(int i=0; i<labels.length; i++){
            final RadioButton optionRB = new RadioButton(labels[i]);
            optionRB.setTooltip(new Tooltip(tooltips[i]));
            optionRB.setSelected(false);
            optionRB.setUserData(i);
            popup.add(optionRB, 0, i);
            optionRB.selectedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                    if(t1){
                        optionRBValue = (int)optionRB.getUserData();
                        dialogStage.close();
                    }
                }
            });
        }
        
        dialogStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                optionRBValue = -1; // invalid
                dialogStage.close();
            }
        });
        
        Scene scene = new Scene(popup, 300, labels.length*50);
        scene.getStylesheets().add(RadioOptionsView.class.getClass().getResource("/media/template/default/style.css").toExternalForm());
        dialogStage.setScene(scene);
        dialogStage.showAndWait();
    }
}
