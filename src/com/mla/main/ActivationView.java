/*
 * Copyright (C) Digital India Corporation (Media Lab Asia)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mla.main;

import com.mla.dictionary.DictionaryAction;
import com.mla.user.UserAction;
import com.mla.user.UserLoginView;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Activation of application
 * @author Aatif Ahmad Khan
 */
public class ActivationView {
    
    public static Stage activationStage;
    BorderPane root;
    
    Configuration objConfiguration = null;
    DictionaryAction objDictionaryAction = null;
    GridPane bodyContainer;
    private Text lblStatus;
    private TextField txtEnterKey;
    private Button btnGenerateKey;
    private Button btnValidateKey;
    private Button btnResendKey;
    private String systemMacAddress;
    
    // URLs for Web Services
    private String strActivationServerUrl = "https://stagingdigibunai.digitalindiacorporation.in"; // can be configured /mla/activationserver.txt
    private final String strUpdateJsonFileDbUrl = "/license_key_api/userRegistration_api.php";
    private final String strCreateLicenseKeyUrl = "/license_key_api/createLicenseKey_api.php?macid="; // add mac address at end
    private final String strValidateLicenseKeyUrl = "/license_key_api/validateKey_api.php";
    
    public ActivationView(Configuration objConfigurationCall, boolean user_status, boolean login_flag){
        objConfiguration = objConfigurationCall;
        objDictionaryAction = new DictionaryAction(objConfiguration);

        activationStage = new Stage();
        //activationStage.initModality(Modality.APPLICATION_MODAL);//WINDOW_MODAL
        //activationStage.initStyle(StageStyle.UNDECORATED);
        activationStage.initStyle(StageStyle.TRANSPARENT);
        activationStage.setResizable(false);
        activationStage.setIconified(false);
        activationStage.setFullScreen(false);
    
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root);
        scene.getStylesheets().add(ActivationView.class.getResource("/media/login.css").toExternalForm());
        root.setId("borderpane"); 
        
        // load activation server url if configured at /mla/activationserver.txt
        InputStream resourceAsStream;
        try {
            resourceAsStream = new FileInputStream(System.getProperty("user.dir")+"/mla/activationserver.txt");
            if (resourceAsStream != null) {
                byte[] b = new byte[256];
                resourceAsStream.read(b);
                strActivationServerUrl=new String(b).trim();
            }
        } catch (IOException ex) {
            new Logging("SEVERE",ActivationView.class.getName(),"read activation server url",ex);
        }
        
        try {
            // check internet connectivity, if not connected: return
            boolean isInternetConnected=isWebsiteReachable("google.com", 80)||isWebsiteReachable("bing.com", 80);
            if(!isInternetConnected){
                new MessageView("error", objDictionaryAction.getWord("NOINTERNET"), objDictionaryAction.getWord("NOINTERNETACTIVATION"));
                new Logging("SEVERE",ActivationView.class.getName(),"No Internet Connection.",null);
                activationStage.close();
                return;
            }
            systemMacAddress = getMacAddress();
            if(login_flag)
                new UserAction().getCurrentLoginUser(objConfiguration);
            else
                new UserAction().getNewRegisteredUser(objConfiguration);
            
            // Invoke UpdateJsonFileDb Web Service with request data
            final String strJsonData = getJsonRequestUserData(user_status);
            WebServiceInvoker objWebServiceInvoker = new WebServiceInvoker(strActivationServerUrl+strUpdateJsonFileDbUrl, strJsonData, "POST");
        } catch (SQLException ex) {
            new Logging("SEVERE",ActivationView.class.getName(),"getNewRegisteredUser()",ex);
        }
    
        final ImageView closeIV = new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png");
        Tooltip.install(closeIV, new Tooltip(objDictionaryAction.getWord("TOOLTIPCLOSE")));    
        closeIV.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                closeIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
                closeIV.setScaleX(1.5);
                closeIV.setScaleY(1.5);
                closeIV.setScaleZ(1.5);
            }
        });
        closeIV.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                closeIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
                closeIV.setScaleX(1);
                closeIV.setScaleY(1);
                closeIV.setScaleZ(1);
            }
        });
        closeIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                activationStage.close();
            }
        });

        bodyContainer = new GridPane();   
        bodyContainer.setVgap(3);
        bodyContainer.setId("bodyContainer");
        bodyContainer.getColumnConstraints().add(new ColumnConstraints(150)); // column 1 is 100 wide
        bodyContainer.getColumnConstraints().add(new ColumnConstraints(180)); // column 2 is 200 wide
        bodyContainer.getColumnConstraints().add(new ColumnConstraints(150)); // column 1 is 100 wide
        bodyContainer.getColumnConstraints().add(new ColumnConstraints(180)); // column 2 is 200 wide
        bodyContainer.setAlignment(Pos.CENTER);
        bodyContainer.setMaxSize(350, 200);
        bodyContainer.relocate(10, 10);
        //bodyContainer.setLayoutX(30);
        //bodyContainer.setLayoutY(10);

        Text projectTitle = new Text(objDictionaryAction.getWord("TRADEMARK"));
        projectTitle.setFont(Font.font(objConfiguration.getStrBFont(), FontWeight.NORMAL, objConfiguration.getIntBFontSize()));
        projectTitle.setId("welcome-text");
        bodyContainer.add(projectTitle, 0, 0, 6, 1);

        Label versionTitle = new Label(objDictionaryAction.getWord("VERSIONINFO"));
        versionTitle.setId("version-text");
        bodyContainer.add(versionTitle, 0, 1, 6, 1);
        
        // Label for Name
        Label lblName = new Label(objDictionaryAction.getWord("NAME")+ ":");
        lblName.setId("name");
        bodyContainer.add(lblName, 0, 2);
        Label lblNameValue = new Label(objConfiguration.getObjUser().getStrName());
        bodyContainer.add(lblNameValue, 1, 2);
        
        // Label for UserName
        Label lblUsername = new Label(objDictionaryAction.getWord("USERNAME")+ ":");
        lblUsername.setId("username");
        bodyContainer.add(lblUsername, 2, 2);
        Label lblUsernameValue = new Label(objConfiguration.getObjUser().getStrUsername());
        bodyContainer.add(lblUsernameValue, 3, 2);
        
        // Label for Email
        Label lblEmail = new Label(objDictionaryAction.getWord("EMAILID")+ ":");
        lblEmail.setId("email");
        bodyContainer.add(lblEmail, 0, 3);
        Label lblEmailValue = new Label(objConfiguration.getObjUser().getStrEmail());
        bodyContainer.add(lblEmailValue, 1, 3);
        
        // Label for Mobile
        Label lblContact= new Label(objDictionaryAction.getWord("CONTACTNUMBER")+ ":");
        lblContact.setId("contact");
        bodyContainer.add(lblContact, 2, 3);
        Label lblContactlValue = new Label(objConfiguration.getObjUser().getStrContactNumber());
        bodyContainer.add(lblContactlValue, 3, 3);
        
        // Label for Gender
        Label lblGender= new Label(objDictionaryAction.getWord("GENDER")+ ":");
        lblGender.setId("gender");
        bodyContainer.add(lblGender, 0, 4);
        Label lblGenderValue = new Label(objConfiguration.getObjUser().getStrGender());
        bodyContainer.add(lblGenderValue, 1, 4);
        
        // Label for Country
        Label lblCountry= new Label(objDictionaryAction.getWord("COUNTRY")+ ":");
        lblCountry.setId("country");
        bodyContainer.add(lblCountry, 2, 4);
        Label lblCountryValue = new Label(objConfiguration.getObjUser().getStrCountryAddress());
        bodyContainer.add(lblCountryValue, 3, 4);
        
        // Label for State
        Label lblState= new Label(objDictionaryAction.getWord("STATE")+ ":");
        lblState.setId("state");
        bodyContainer.add(lblState, 0, 5);
        Label lblStateValue = new Label(objConfiguration.getObjUser().getStrStateAddress());
        bodyContainer.add(lblStateValue, 1, 5);
        
        // Label for City
        Label lblCity= new Label(objDictionaryAction.getWord("CITY")+ ":");
        lblCity.setId("city");
        bodyContainer.add(lblCity, 2, 5);
        Label lblCityValue = new Label(objConfiguration.getObjUser().getStrCityAddress());
        bodyContainer.add(lblCityValue, 3, 5);
          
        /* Label for Pincode
        Label lblPincode= new Label(objDictionaryAction.getWord("PINCODEADDRESS")+ ":");
        lblPincode.setId("pincode");
        bodyContainer.add(lblPincode, 0, 6);
        Label lblPincodeValue = new Label(objConfiguration.getObjUser().getStrPincodeAddress());
        bodyContainer.add(lblPincodeValue, 1, 6);*/
        
        // Label for MAC Address
        Label lblMacAddress= new Label("MAC"+" "+objDictionaryAction.getWord("ADDRESS")+ ":");
        lblMacAddress.setId("mac");
        bodyContainer.add(lblMacAddress, 0, 6);
        Label lblMacAddressValue = new Label(systemMacAddress);
        bodyContainer.add(lblMacAddressValue, 1, 6);
        
        btnGenerateKey=new Button("Generate Key");
        btnGenerateKey.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
        btnGenerateKey.setDefaultButton(true);
        btnGenerateKey.setFocusTraversable(true);
        
        btnGenerateKey.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                // Invoke CreateLicenseKey Web Service
                WebServiceInvoker objWebServiceInvoker = new WebServiceInvoker(strActivationServerUrl+strCreateLicenseKeyUrl+systemMacAddress, null, null);
                plotValidateKeyUI();
            }
        });
        
        bodyContainer.add(btnGenerateKey, 1, 7, 2, 1);
        
        root.setRight(closeIV); 
        root.setCenter(bodyContainer);
        
        activationStage.getIcons().add(new Image("/media/icon.png"));
        activationStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWLOGIN")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
        activationStage.setScene(scene);
        activationStage.show();
    }
    
    private boolean isWebsiteReachable(String website, int port){
        Socket socket=new Socket();
        InetSocketAddress socketAddress=new InetSocketAddress(website, port);
        try{
            socket.connect(socketAddress, 5000); // timeout in milliseconds
            return true;
        }
        catch(IOException ioEx){
            new Logging("SEVERE",ActivationView.class.getName(),"isWebsiteReachable():",ioEx);
            return false;
        }
        finally{
            try{
                socket.close();
            }catch(IOException ioEx){
                new Logging("SEVERE",ActivationView.class.getName(),"isWebsiteReachable(): Socket Close",ioEx);
            }
        }
    }
    
    String getMacAddress(){
        String sysMacAddress = "";
        try {
            InetAddress localhost = InetAddress.getLocalHost();
            NetworkInterface objNetworkInterface = NetworkInterface.getByInetAddress(localhost);
            byte[] macAddress = objNetworkInterface.getHardwareAddress();
            String[] strMacAddress = new String[macAddress.length];
            for(int i=0; i<macAddress.length; i++){
                strMacAddress[i] = String.format("%02X", macAddress[i]);
                if(i>0)
                    sysMacAddress += ":";
                sysMacAddress += strMacAddress[i];
            }
        } catch (SocketException | UnknownHostException ex) {
            new Logging("SEVERE",ActivationView.class.getName(),"getMacAddress()",ex);
        }
        return sysMacAddress;
    }
    
    String getJsonRequestUserData(boolean user_status){
         return "{\"macid\":\"" + systemMacAddress+ "\","
                 + "\"emailid\":\"" + objConfiguration.getObjUser().getStrEmail() + "\","
                 + "\"mobileno\":\"" + objConfiguration.getObjUser().getStrContactNumber() + "\","
                 + "\"licensekey\":\"\","
                 + "\"activationdatetime\":\"\","
                 + "\"activationstatus\":\"0\","
                 + "\"name\":\"" + objConfiguration.getObjUser().getStrName()+ "\","
                 //+ "\"username\":\"" + objConfiguration.getObjUser().getStrUsername()+ "\","
                 //+ "\"address\":\"" + objConfiguration.getObjUser().getStrAddress()+ "\","
                 + "\"country\":\"" + objConfiguration.getObjUser().getStrCountryAddress()+ "\","
                 + "\"state\":\"" + objConfiguration.getObjUser().getStrStateAddress()+ "\","
                 + "\"city\":\"" + objConfiguration.getObjUser().getStrCityAddress()+ "\","
                 //+ "\"pincode\":\"" + objConfiguration.getObjUser().getStrPincodeAddress()+ "\","
                 + "\"cluster\":\"NA\","
                 + "\"gender\":\"" + objConfiguration.getObjUser().getStrGender()+ "\","
                 + "\"user_status\":\""+(user_status ? "Registered" : "New")+"\"}";
    }
    
    void plotValidateKeyUI(){
        bodyContainer.getChildren().clear();
        
        lblStatus = new Text();
        bodyContainer.add(lblStatus, 0, 3, 2, 1);
        lblStatus.setId("actiontarget");
        lblStatus.setText("License Key sent to registered Email Address: "+objConfiguration.getObjUser().getStrEmail());
        
        Label lblEnterKey= new Label("Enter License Key"+ ":");
        lblEnterKey.setId("pincode");
        bodyContainer.add(lblEnterKey, 0, 0, 2, 1);
        txtEnterKey = new TextField();
        txtEnterKey.setPromptText("Enter License Key Here");
        bodyContainer.add(txtEnterKey, 0, 1, 2, 1);
        
        btnValidateKey=new Button("Validate Key");
        btnValidateKey.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
        btnValidateKey.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                // Invoke ValidateLicenseKey Web Service with request data
                WebServiceInvoker objWebServiceInvoker = new WebServiceInvoker(strActivationServerUrl+strValidateLicenseKeyUrl, getJsonRequestKeyData(txtEnterKey.getText().trim()), "POST");
                if(validateResponse(objWebServiceInvoker.strResponse)){
                    try {
                        if(new UserAction().updateApplicationActivation(txtEnterKey.getText().trim())){
                            lblStatus.setText("Activation Successful!");
                            activationStage.close();
                            new UserLoginView(objConfiguration);
                        }
                    } catch (SQLException ex) {
                        new Logging("SEVERE",ActivationView.class.getName(),"updateApplicationActivation()",ex);
                    }
                } else {
                    lblStatus.setText("Invalid License Key! Please enter valid key.");
                }
            }
        });
        bodyContainer.add(btnValidateKey, 0, 2, 1, 1);
        
        btnResendKey=new Button("Resend Key");
        btnResendKey.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
        btnResendKey.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                // Will be written later after Email Service active.
                // Just need to replicate CreateLicenseKey functionality (with duplicate checking)
                // i.e. check request for existing macid record, resend the email/otp
            }
        });
        btnResendKey.setDisable(true);
        bodyContainer.add(btnResendKey, 1, 2, 1, 1);
    }
    
    String getJsonRequestKeyData(String strUserEnteredKey){
        return "{\"macid\":\""+ systemMacAddress +"\","
                + "\"userlicensekey\":\"" + strUserEnteredKey + "\"}";
    }
    
    boolean validateResponse(String response){
        if(response==null)
            return false;
        if(response.contains("Matched Sucessfully"))//"{\"response\":\"Matched\"}"
            return true;
        return false; //{"response":"Not Matched"} or something else
    }
}
