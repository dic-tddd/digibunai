/*
 * Copyright (C) Digital India Corporation (Media Lab Asia)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mla.main;

import com.mla.artwork.Artwork;
import com.mla.artwork.ArtworkAction;
import com.mla.artwork.ArtworkView;
import com.mla.cloth.Cloth;
import com.mla.cloth.ClothView;
import com.mla.cloth.ClothAction;
import com.mla.dictionary.ConvertorAction;
import com.mla.dictionary.DictionaryAction;
import com.mla.dictionary.TextToSpeech;
import com.mla.fabric.Fabric;
import com.mla.fabric.FabricAction;
import com.mla.fabric.FabricView;
import com.mla.user.UserAction;
import com.mla.user.UserLoginView;
import com.mla.user.UserView;
import com.mla.utility.UtilityView;
import com.mla.weave.Weave;
import com.mla.weave.WeaveView;
import com.mla.weave.WeaveAction;
import com.sun.media.jai.codec.ByteArraySeekableStream;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.SeekableStream;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.AudioClip;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import javax.media.jai.PlanarImage;
/**
 *
 * @Designing GUI window for dashboard
 * @author Amit Kumar Singh
 * 
 */
public class WindowView extends Application {
    public static Stage windowStage;
    private BorderPane root;
    private Scene scene;
   
    private MenuItem editProfileMI;
    private MenuItem editPreferenceMI;
    private MenuItem editSettingMI;
    private MenuItem helpMI;
    private MenuItem logoutMI;
          
    private TitledPane clothTP;
    private TitledPane fabricTP;
    private TitledPane artworkTP;
    private TitledPane weaveTP;
    private TitledPane yarnTP;
    
    private VBox clothVB;
    private VBox fabricVB;
    private VBox artworkVB;
    private VBox weaveVB;
    private VBox yarnVB;
    private VBox userVB;
    private VBox utilityVB;
    /*
    private AudioClip clothAC;
    private AudioClip fabricAC;
    private AudioClip artworkAC;
    private AudioClip weaveAC;
    private AudioClip yarnAC;
    private AudioClip userAC;
    private AudioClip utilityAC;
    */
    Configuration objConfiguration = null;
    DictionaryAction objDictionaryAction = null;
    List lstData=null;
    
    private List<Label> messages = new ArrayList<>();        
    private int index = 0;       
    Stage chatBoxStage = new Stage();
    
/**
* WindowView(Stage)
* <p>
* This constructor is used for individual call of class. 
* 
* @param       Stage primaryStage
* @author      Amit Kumar Singh
* @version     %I%, %G%
* @since       1.0
* @see         javafx.stage.*;
* @link        FabricView
*/
     public WindowView(final Stage primaryStage) {}    
/**
* WindowView(Configuration)
* <p>
* This class is used for prompting about software information. 
* 
* @param       Configuration objConfigurationCall
* @author      Amit Kumar Singh
* @version     %I%, %G%
* @since       1.0
* @see         javafx.scene.control.*;
* @link        Configuration
*/
    public WindowView(Configuration objConfigurationCall) {  
        objConfiguration = objConfigurationCall;
        objDictionaryAction = new DictionaryAction(objConfiguration);    
        checkDisplayScaling();
        setGlobalWidthHeight();
        
        windowStage = new Stage(); 
        root = new BorderPane();
        scene = new Scene(root, objConfiguration.WIDTH, objConfiguration.HEIGHT, Color.WHITE);
        scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/dashboard.css").toExternalForm());

        resolutionControl();
        populateContainer();

        // Code Added for ShortCuts
        addAccelratorKey();
        
        windowStage.setScene(scene);        
        windowStage.getIcons().add(new Image("/media/icon.png"));
        windowStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWDASHBOARD")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
        //configurationStage.setIconified(true);
        windowStage.setFullScreen(false);
        //windowStage.setMaximized(false);
        windowStage.setResizable(true);
        windowStage.setX(-5);
        windowStage.setY(0);
        windowStage.show();  
        windowStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                exitMenuAction();
                we.consume();
            }
        });
    } 
/**
* resolutionControl(Stage)
* <p>
* This function is used for reassign width and height. 
*  
* @param       Stage primaryStage
* @author      Amit Kumar Singh
* @version     %I%, %G%
* @since       1.0
* @see         javafx.stage.*;
* @link        WindowView
*/
    private void resolutionControl(){
        //windowStage.resizableProperty().addListener(listener);
        scene.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                setGlobalWidthHeight();
                windowStage.setHeight(objConfiguration.HEIGHT);
                windowStage.setWidth(objConfiguration.WIDTH);
                populateContainer();
            }
        });
        scene.heightProperty().addListener(new ChangeListener<Number>() {
            @Override 
            public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneHeight, Number newSceneHeight) {
                setGlobalWidthHeight();
                windowStage.setHeight(objConfiguration.HEIGHT);
                windowStage.setWidth(objConfiguration.WIDTH);
                populateContainer();
            }
        });
        /*
        com.sun.javafx.css.StyleManager.errorsProperty().addListener((ListChangeListener<? super CssError>) c -> {
            while(c.next()) {
                for(CssError error : c.getAddedSubList()) {
                    // maybe you want to check for specific errors here
                    System.out.println(error.getMessage());
                }       
            }
        });
        */
    }
    private void setGlobalWidthHeight(){
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        objConfiguration.WIDTH = screenSize.getWidth();
        objConfiguration.HEIGHT = screenSize.getHeight();
        
        objConfiguration.strIconResolution = (objConfiguration.WIDTH<1280)?"hd_none":(objConfiguration.WIDTH<1920)?"hd":(objConfiguration.WIDTH<2560)?"hd_full":"hd_quad";
    
        GraphicsDevice objGraphicsDevice = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        double width = objGraphicsDevice.getDisplayMode().getWidth();
        double height = objGraphicsDevice.getDisplayMode().getHeight(); 
        
        int screenWidth = (int) Screen.getPrimary().getBounds().getWidth();
        int screenHeight = (int) Screen.getPrimary().getBounds().getHeight();
        
        //System.err.println(objConfiguration.WIDTH+":"+objConfiguration.HEIGHT+"::"+width+":"+height+"::"+screenWidth+":"+screenHeight);
        //System.err.println("resolution"+Toolkit.getDefaultToolkit().getScreenResolution());
        //System.err.println("resolution"+(width/objConfiguration.WIDTH));
    }
    private void checkDisplayScaling(){
        int dpi = Toolkit.getDefaultToolkit().getScreenResolution();
        int scale=(int)(100*(double)dpi/96.0);
        if(scale>=150)
            new MessageView("warning", "Change Scaling Settings", "Your display scaling is 150% or more. Application may not work properly.\nChange the Scale to 100% or 125%. \"Settings -> Display -> Change Scale\"");
    }
    private void populateContainer(){        
        HBox container = new HBox();
        container.setId("container");
    
        //Recent Pane
        Accordion recentPane = new Accordion (); 
        recentPane.setPrefSize(objConfiguration.WIDTH*0.25, objConfiguration.HEIGHT);
        recentPane.setId("recentPane");
        
        clothTP = new TitledPane();
        clothTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_library.png"));
        
        GridPane clothList = new GridPane();
        clothList.setVgap(objConfiguration.WIDTH*0.02);
        clothList.setHgap(objConfiguration.WIDTH*0.01);
        clothList.setAlignment(Pos.TOP_CENTER);
        clothList.getChildren().clear();        
        clothTP.setText(objDictionaryAction.getWord("RECENT")+" "+objDictionaryAction.getWord("CLOTH"));
        clothTP.setContent(clothList);
         
        Cloth objCloth = new Cloth();
        objCloth.setObjConfiguration(objConfiguration);
        objCloth.setStrCondition("");
        objCloth.setStrSearchBy("All");
        objCloth.setStrSearchAccess("All User Data");
        objCloth.setStrOrderBy("Date");
        objCloth.setStrDirection("Descending");
        objCloth.setStrLimit("0,4");  
            
        List lstClothDetails = new ArrayList();
        WindowViewData objClothRunnable = new WindowViewData();
        objClothRunnable.lstDataDetails = lstClothDetails;
        objClothRunnable.objItem = objCloth;
        Thread  objClothThread = new Thread(objClothRunnable);  
        objClothThread.start();
        //ClothAction objClothAction = new ClothAction();
        //lstClothDetails = objClothAction.lstImportCloth(objCloth);
        
        fabricTP = new TitledPane();
        fabricTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fabric_library.png"));
        GridPane fabricList = new GridPane();
        fabricList.setVgap(objConfiguration.WIDTH*0.02);
        fabricList.setHgap(objConfiguration.WIDTH*0.01);
        fabricList.setAlignment(Pos.TOP_CENTER);
        fabricList.getChildren().clear();
        fabricTP.setText(objDictionaryAction.getWord("RECENT")+" "+objDictionaryAction.getWord("FABRIC"));
        fabricTP.setContent(fabricList);
        
        Fabric objFabric = new Fabric();
        objFabric.setObjConfiguration(objConfiguration);
        objFabric.setStrCondition("");
        objFabric.setStrSearchBy(objConfiguration.getStrClothType());  
        objFabric.setStrSearchAccess("All User Data");
        objFabric.setStrOrderBy("Date");
        objFabric.setStrDirection("Ascending");
        objFabric.setStrLimit("0,6"); 
            
        List lstFabricDetails = new ArrayList();
        WindowViewData objFabricRunnable = new WindowViewData();
        objFabricRunnable.lstDataDetails = lstFabricDetails;
        objFabricRunnable.objItem = objFabric;
        Thread objFabricThread =new Thread(objFabricRunnable);  
        objFabricThread.start(); 
        //FabricAction objFabricAction = new FabricAction();
        //lstFabricDetails = objFabricAction.lstImportFabric(objFabric);
        
        artworkTP = new TitledPane();
        artworkTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/artwork_library.png"));
        GridPane artworkList = new GridPane();
        artworkList.setVgap(objConfiguration.WIDTH*0.02);
        artworkList.setHgap(objConfiguration.WIDTH*0.01);
        artworkList.setAlignment(Pos.TOP_CENTER);
        artworkList.getChildren().clear();
        artworkTP.setText(objDictionaryAction.getWord("RECENT")+" "+objDictionaryAction.getWord("ARTWORK"));
        artworkTP.setContent(artworkList);
        
        Artwork objArtwork = new Artwork();
        objArtwork.setObjConfiguration(objConfiguration);
        objArtwork.setStrCondition("");
        objArtwork.setStrSearchBy("");
        objArtwork.setStrSearchAccess("All User Data");
        objArtwork.setStrOrderBy("Date");
        objArtwork.setStrDirection("Descending");
        objArtwork.setStrLimit("0,6");   

        List lstArtworkDetails = new ArrayList();                 
        WindowViewData objArtworkRunnable = new WindowViewData();
        objArtworkRunnable.lstDataDetails = lstArtworkDetails;
        objArtworkRunnable.objItem = objArtwork;
        Thread objArtworkThread =new Thread(objArtworkRunnable);  
        objArtworkThread.start();
        //ArtworkAction objArtworkAction = new ArtworkAction();
        //lstArtworkDetails = objArtworkAction.lstImportArtwork(objArtwork);
        
        weaveTP = new TitledPane();
        weaveTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/weave_library.png"));
        GridPane weaveList = new GridPane();
        weaveList.setVgap(objConfiguration.WIDTH*0.02);
        weaveList.setHgap(objConfiguration.WIDTH*0.01);
        weaveList.setAlignment(Pos.TOP_CENTER);
        weaveList.getChildren().clear();
        weaveTP.setText(objDictionaryAction.getWord("RECENT")+" "+objDictionaryAction.getWord("WEAVE"));
        weaveTP.setContent(weaveList);
        List lstWeaveDetails = new ArrayList();
        
        Weave objWeave = new Weave();
        objWeave.setObjConfiguration(objConfiguration);
        objWeave.setStrCondition("");
        objWeave.setStrSearchBy("All");
        objWeave.setStrSearchAccess("All User Data");
        objWeave.setStrOrderBy("Date");
        objWeave.setStrDirection("Descending");
        objWeave.setStrLimit("0,6"); 
            
        WindowViewData objWeaveRunnable = new WindowViewData();
        objWeaveRunnable.lstDataDetails = lstWeaveDetails;
        objWeaveRunnable.objItem = objWeave;
        Thread objWeaveThread =new Thread(objWeaveRunnable);  
        objWeaveThread.start(); 
        //WeaveAction objWeaveAction = new WeaveAction();
        //lstWeaveDetails = objWeaveAction.lstImportWeave(objWeave);
        
        recentPane.getPanes().addAll(clothTP, fabricTP, artworkTP, weaveTP);
        recentPane.setExpandedPane(clothTP);
    
        //Main Pane
        VBox mainPane = new VBox();
        mainPane.setId("mainPane");
        mainPane.setPrefSize(objConfiguration.WIDTH*0.75, objConfiguration.HEIGHT);    
        
        HBox topContainer = new HBox();
        //userPane.setPrefWidth(d);
        topContainer.setAlignment(Pos.TOP_RIGHT);
        Image userImage = new Image("/media/user.png");
        Circle clip = new Circle(20);
        clip.setTranslateX(1);
        clip.setTranslateY(1);
        clip.setCenterX(10);
        clip.setCenterY(10);
        clip.setFill(new ImagePattern(userImage));
        final DropShadow dropShadow= new DropShadow();
        dropShadow.setOffsetY(0f);
        dropShadow.setOffsetX(0f);
        dropShadow.setColor(Color.RED);
        dropShadow.setWidth(50);
        dropShadow.setHeight(50);
        clip.setEffect(dropShadow);
        
        clip.hoverProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                final Timeline timeline = new Timeline(
                        new KeyFrame(Duration.seconds(0),new KeyValue(dropShadow.colorProperty(), Color.WHITE)), 
                        new KeyFrame(Duration.seconds(1),new KeyValue(dropShadow.colorProperty(), Color.BLUE)), 
                        new KeyFrame(Duration.seconds(3),new KeyValue(dropShadow.colorProperty(), Color.PINK)), 
                        new KeyFrame(Duration.seconds(5),new KeyValue(dropShadow.colorProperty(), Color.GREEN)), 
                        new KeyFrame(Duration.seconds(8), new KeyValue(dropShadow.colorProperty(), Color.BLACK))
                    );
                timeline.setCycleCount(Timeline.INDEFINITE); 
                //timeline.setCycleCount(6);
                timeline.setAutoReverse(true);
                timeline.play();
            }
        });
        
        topContainer.getChildren().add(clip);
        topContainer.getChildren().add(new Label(" "+objDictionaryAction.getWord("WELCOME")+" "));        
        topContainer.getChildren().add(new ImageView(new Image("/media/namasty.png")));
        if(objConfiguration.getObjUser()!=null && objConfiguration.getObjUser().getStrName()!=null)
            topContainer.getChildren().add(new Label(" "+objConfiguration.getObjUser().getStrName().trim()+", "));
        Label logoutLbl = new Label();//objDictionaryAction.getWord("LOGOUT"));
        logoutLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPUSER")));
        logoutLbl.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/logout.png"));
        logoutLbl.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                event.consume();
                logoutAction();
            }
        });
        if(!DigiBunai.Constants.CONFIG.USERTYPE.equalsIgnoreCase("Guest")) //no logout button for guest user
            topContainer.getChildren().add(logoutLbl);    
        
        /*
        HBox topContainer = new HBox();
        //topContainer.setAlignment(Pos.TOP_RIGHT);
        topContainer.setPrefWidth(objConfiguration.WIDTH*0.75);
        topContainer.setStyle("-fx-width:"+objConfiguration.WIDTH+";");

        HBox logoPane = new HBox();
        logoPane.setAlignment(Pos.TOP_LEFT);
        ImageView logoImage = new ImageView("/media/logo.png");
        logoImage.setFitHeight(objConfiguration.HEIGHT*0.1);
        logoPane.getChildren().add(logoImage);
        //logoPane.setLayoutX(objConfiguration.WIDTH*0.25);
        topContainer.getChildren().addAll(logoPane,userPane);
        */
        GridPane bodyContainer = new GridPane();
        bodyContainer.setAlignment(Pos.CENTER);
        
        // fabric editor
        fabricVB = new VBox(); 
        Label fabricLbl= new Label(objDictionaryAction.getWord("FABRICEDITOR"));
        fabricLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("FABRICEDITOR")+" (Shift+J)\n"+objDictionaryAction.getWord("TOOLTIPFABRICEDITOR")));
        fabricVB.getChildren().addAll(new ImageView(objConfiguration.getStrTemplate()+"/img/d_fabric_editor.png"), fabricLbl);
        fabricVB.setPrefWidth(objConfiguration.WIDTH*0.20);
        fabricVB.setPrefHeight(objConfiguration.HEIGHT*0.30);
        fabricVB.getStyleClass().addAll("VBox");
        fabricVB.setCursor(Cursor.CROSSHAIR);
        bodyContainer.add(fabricVB, 0, 0);
        // artwork editor
        artworkVB = new VBox(); 
        Label artworkLbl= new Label(objDictionaryAction.getWord("ARTWORKEDITOR"));
        artworkLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("ARTWORKEDITOR")+" (Shift+D)\n"+objDictionaryAction.getWord("TOOLTIPARTWORKEDITOR")));
        artworkVB.getChildren().addAll(new ImageView(objConfiguration.getStrTemplate()+"/img/d_artwork_editor.png"), artworkLbl);
        artworkVB.setPrefWidth(objConfiguration.WIDTH*0.20);
        artworkVB.setPrefHeight(objConfiguration.HEIGHT*0.30);
        artworkVB.getStyleClass().addAll("VBox");
        artworkVB.setCursor(Cursor.CROSSHAIR);    
        bodyContainer.add(artworkVB, 1, 0);
        // weave editor
        weaveVB = new VBox(); 
        Label weaveLbl= new Label(objDictionaryAction.getWord("WEAVEEDITOR"));
        weaveLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("WEAVEEDITOR")+" (Shift+W)\n"+objDictionaryAction.getWord("TOOLTIPWEAVEEDITOR")));
        weaveVB.getChildren().addAll(new ImageView(objConfiguration.getStrTemplate()+"/img/d_weave_editor.png"), weaveLbl);
        weaveVB.setPrefWidth(objConfiguration.WIDTH*0.20);
        weaveVB.setPrefHeight(objConfiguration.HEIGHT*0.30);
        weaveVB.getStyleClass().addAll("VBox");
        weaveVB.setCursor(Cursor.CROSSHAIR);
        //weaveVB.requestFocus();
        bodyContainer.add(weaveVB, 2, 0);  
        // Cloth editor item
        clothVB = new VBox(); 
        Label clothLbl= new Label(objDictionaryAction.getWord("CLOTHEDITOR"));
        clothLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("CLOTHEDITOR")+" (Shift+G)"+objDictionaryAction.getWord("TOOLTIPCLOTHEDITOR")));
        clothVB.getChildren().addAll(new ImageView(objConfiguration.getStrTemplate()+"/img/d_cloth_editor.png"), clothLbl);
        clothVB.setPrefWidth(objConfiguration.WIDTH*0.20);
        clothVB.setPrefHeight(objConfiguration.HEIGHT*0.30);
        clothVB.getStyleClass().addAll("VBox");
        clothVB.setCursor(Cursor.CROSSHAIR);
        bodyContainer.add(clothVB, 0, 1);
        /*
        // yarn editor item
        VBox yarnVB = new VBox(); 
        Label yarnLbl= new Label(objDictionaryAction.getWord("YARNEDITOR"));
        yarnLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("YARNEDITOR")+" (Shift+Y)\n"+objDictionaryAction.getWord("TOOLTIPYARNEDITOR")));
        yarnVB.getChildren().addAll(new ImageView(objConfiguration.getStrTemplate()+"/img/d_yarn_editor.png"), yarnLbl);
        yarnVB.setPrefWidth(objConfiguration.WIDTH*0.20);
        yarnVB.setPrefHeight(objConfiguration.HEIGHT*0.30);
        yarnVB.getStyleClass().addAll("VBox");
        yarnVB.setCursor(Cursor.CROSSHAIR);
        bodyContainer.add(yarnVB, 1, 1);
        */
        // user item
        userVB = new VBox(); 
        Label userLbl= new Label(objDictionaryAction.getWord("USER"));
        userLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("USER")+" (Shift+P)\n"+objDictionaryAction.getWord("TOOLTIPUSER")));
        userVB.getChildren().addAll(new ImageView(objConfiguration.getStrTemplate()+"/img/d_user.png"), userLbl);
        userVB.setPrefWidth(objConfiguration.WIDTH*0.20);
        userVB.setPrefHeight(objConfiguration.HEIGHT*0.30);
        userVB.getStyleClass().addAll("VBox");
        userVB.setCursor(Cursor.CROSSHAIR);
        bodyContainer.add(userVB, 1, 1);
        // utiltiy item
        utilityVB = new VBox(); 
        Label utilityLbl= new Label(objDictionaryAction.getWord("UTILITYEDITOR"));
        utilityLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("UTILITYEDITOR")+" (Shift+U)\n"+objDictionaryAction.getWord("TOOLTIPUTILITYEDITOR")));
        utilityVB.getChildren().addAll(new ImageView(objConfiguration.getStrTemplate()+"/img/d_utilities.png"), utilityLbl);
        utilityVB.setPrefWidth(objConfiguration.WIDTH*0.20);
        utilityVB.setPrefHeight(objConfiguration.HEIGHT*0.30);
        utilityVB.getStyleClass().addAll("VBox");
        utilityVB.setCursor(Cursor.CROSSHAIR);
        bodyContainer.add(utilityVB, 2, 1);
        
        //Add the action to Buttons.
        fabricVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                fabricVBAction();
            }
        });        
        final Timeline fabricTL = new Timeline();
        fabricTL.setCycleCount(Timeline.INDEFINITE); 
        //fabricTL.setCycleCount(6);
        fabricTL.setAutoReverse(true);
        //timeline.getKeyFrames().clear();
        fabricTL.getKeyFrames().addAll(
            new KeyFrame(Duration.seconds(0),    new KeyValue(fabricVB.scaleYProperty(),1.1)),
            new KeyFrame(Duration.seconds(0.55), new KeyValue(fabricVB.scaleYProperty(),1.0)),
            new KeyFrame(Duration.seconds(1),    new KeyValue(fabricVB.scaleYProperty(),1.1))                
        );            
        fabricVB.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                fabricTL.play();
            }
        });
        fabricVB.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                fabricVB.setRotate(0);
                fabricTL.stop();
            }
        });
                
        artworkVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                artworkVBAction();
            }
        });
        final Timeline artworkTL = new Timeline();
        artworkTL.setCycleCount(Timeline.INDEFINITE); 
        //artworkTL.setCycleCount(6);
        artworkTL.setAutoReverse(true);
        //timeline.getKeyFrames().clear();
        artworkTL.getKeyFrames().addAll(
            new KeyFrame(Duration.seconds(0),    new KeyValue(artworkVB.scaleYProperty(),1.1)),
            new KeyFrame(Duration.seconds(0.55), new KeyValue(artworkVB.scaleYProperty(),1.0)),
            new KeyFrame(Duration.seconds(1),    new KeyValue(artworkVB.scaleYProperty(),1.1))  
        );
        artworkVB.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                artworkTL.play();
            }
        });
        artworkVB.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                artworkVB.setRotate(0);
                artworkTL.stop();
            }
        });
        
        weaveVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                weaveVBAction();
            }
        });
        final Timeline weaveTL = new Timeline();
        weaveTL.setCycleCount(Timeline.INDEFINITE); 
        //weaveTL.setCycleCount(6);
        weaveTL.setAutoReverse(true);
        //timeline.getKeyFrames().clear();
        weaveTL.getKeyFrames().addAll(
            new KeyFrame(Duration.seconds(0),    new KeyValue(weaveVB.scaleYProperty(),1.1)),
            new KeyFrame(Duration.seconds(0.55), new KeyValue(weaveVB.scaleYProperty(),1.0)),
            new KeyFrame(Duration.seconds(1),    new KeyValue(weaveVB.scaleYProperty(),1.1))                
        );            
        weaveVB.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                weaveTL.play();
            }
        });
        weaveVB.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                weaveVB.setRotate(0);
                weaveTL.stop();
            }
        });
        
        clothVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                garmentVBAction();
            }
        });
        final Timeline clothTL = new Timeline();
        clothTL.setCycleCount(Timeline.INDEFINITE); 
        //clothTL.setCycleCount(6);
        clothTL.setAutoReverse(true);
        //timeline.getKeyFrames().clear();
        clothTL.getKeyFrames().addAll(
            new KeyFrame(Duration.seconds(0),    new KeyValue(clothVB.scaleYProperty(),1.1)),
            new KeyFrame(Duration.seconds(0.55), new KeyValue(clothVB.scaleYProperty(),1.0)),
            new KeyFrame(Duration.seconds(1),    new KeyValue(clothVB.scaleYProperty(),1.1))                
        );            
        clothVB.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                clothTL.play();
            }
        });
        clothVB.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                clothVB.setRotate(0);
                clothTL.stop();
            }
        });
        /*yarnVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                yarnVBAction();
            }
        });*/
        userVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                userVBAction();
            }
        });
        final Timeline userTL = new Timeline();
        userTL.setCycleCount(Timeline.INDEFINITE); 
        //userTL.setCycleCount(6);
        userTL.setAutoReverse(true);
        //timeline.getKeyFrames().clear();
        userTL.getKeyFrames().addAll(
            new KeyFrame(Duration.seconds(0),    new KeyValue(userVB.scaleYProperty(),1.1)),
            new KeyFrame(Duration.seconds(0.55), new KeyValue(userVB.scaleYProperty(),1.0)),
            new KeyFrame(Duration.seconds(1),    new KeyValue(userVB.scaleYProperty(),1.1))                
        );            
        userVB.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                userTL.play();
            }
        });
        userVB.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                userVB.setScaleY(1);
                userTL.stop();
            }
        });
        
        utilityVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                utilityVBAction();
            }
        });
        final Timeline utilityTL = new Timeline();
        utilityTL.setCycleCount(Timeline.INDEFINITE); 
        //utilityTL.setCycleCount(6);
        utilityTL.setAutoReverse(true);
        //timeline.getKeyFrames().clear();
        utilityTL.getKeyFrames().addAll(
            new KeyFrame(Duration.seconds(0),    new KeyValue(utilityVB.scaleYProperty(),1.1)),
            new KeyFrame(Duration.seconds(0.55), new KeyValue(utilityVB.scaleYProperty(),1.0)),
            new KeyFrame(Duration.seconds(1),    new KeyValue(utilityVB.scaleYProperty(),1.1))                
        );            
        utilityVB.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                utilityTL.play();
            }
        });
        utilityVB.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                utilityVB.setRotate(0);
                utilityTL.stop();
            }
        });
    
        HBox footContainer = new HBox();
        footContainer.setAlignment(Pos.BOTTOM_RIGHT);
        footContainer.getChildren().add(new Label(objDictionaryAction.getWord("COPYRIGHT")));
        //footContainer.setAlignment(Pos.CENTER);    
                    
        
        Button lblChatBox = new Button("Chat Box");
        lblChatBox.setGraphic(new ImageView("/media/text_menu.png"));
        lblChatBox.setAlignment(Pos.TOP_RIGHT);
        lblChatBox.setLayoutX(objConfiguration.WIDTH-50);
        lblChatBox.setLayoutY(objConfiguration.HEIGHT-555);
        lblChatBox.setPickOnBounds(false);
        lblChatBox.setStyle("-fx-background-color: rgba(254,127,0,0.3);");
        lblChatBox.setRotate(-90);
        lblChatBox.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {  
                if(chatBoxStage!=null){
                    if(chatBoxStage.isShowing()){
                        chatBoxStage.hide();
                        //chatBoxStage.close();
                        //chatBoxStage = null;
                        //System.gc();
                        //System.err.println("hide data");
                    }else{
                        chatBoxStage.showAndWait();
                        //System.err.println("show data");
                    }
                } else{
                    initChatBox();
                }
            }                        
        });
        initChatBox();
        StackPane toolPane=new StackPane();
        //addStackPane(hbox);         // Add stack to HBox in top region

        toolPane.getChildren().addAll(footContainer);//,lblChatBox);
        
        
        mainPane.getChildren().addAll(topContainer,bodyContainer, toolPane);
        //root.setLeft(recentPane);
        //root.setCenter(mainPane);
        container.getChildren().addAll(recentPane,mainPane);        
        root.setCenter(container);
        
        //ploting recent cloth data
        try{                
            objClothThread.join();
            lstClothDetails = objClothRunnable.lstDataDetails;
        } catch (InterruptedException ex){

        }
        if(lstClothDetails.size()==0){
            clothList.getChildren().add(new Text(objDictionaryAction.getWord("Cloth")+" - "+objDictionaryAction.getWord("NOVALUE")));
        }else{            
            SeekableStream stream = null;                        
            Rectangle preview;
            for (int i=0, j=lstClothDetails.size(); i<j && i<10; i++){
                lstData = (ArrayList)lstClothDetails.get(i);
                final Label summary = new Label();
                preview = new Rectangle(objConfiguration.WIDTH*0.09,objConfiguration.WIDTH*0.06);
                preview.setArcHeight(10);
                preview.setArcWidth(10);
                preview.setTranslateX(1);
                preview.setTranslateY(1);
                preview.setEffect(new InnerShadow(3, Color.BLUE));
                try {
                    byte[] bytClothThumbnil = (byte[])lstData.get(4);
                    stream = new ByteArraySeekableStream(bytClothThumbnil);
                    String[] names = ImageCodec.getDecoderNames(stream);
                    ImageDecoder dec = ImageCodec.createImageDecoder(names[0], stream, null);
                    RenderedImage im = dec.decodeAsRenderedImage();
                    BufferedImage bufferedImage = PlanarImage.wrapRenderedImage(im).getAsBufferedImage();
                    Image image=SwingFXUtils.toFXImage(bufferedImage, null);
                    String strAccess = lstData.get(6).toString();//new IDGenerator().getUserAcessValueData("CLOTH_LIBRARY",lstData.get(6).toString());
                    String strTooltip = 
                            objDictionaryAction.getWord("NAME")+": "+lstData.get(1).toString()+"\n"+
                            objDictionaryAction.getWord("TYPE")+": "+lstData.get(2).toString()+"\n"+
                            objDictionaryAction.getWord("DESCRIPTION")+": "+lstData.get(3)+"\n"+
                            objDictionaryAction.getWord("REGION")+": "+lstData.get(5)+"\n"+
                            objDictionaryAction.getWord("PERMISSION")+": "+strAccess+"\n"+
                            objDictionaryAction.getWord("BY")+": "+lstData.get(7).toString()+"\n"+
                            objDictionaryAction.getWord("DATE")+": "+lstData.get(8).toString();    
                    preview.setFill(new ImagePattern(image));
                    summary.setGraphic(preview);
                    summary.setTooltip(new Tooltip(strTooltip));
                    summary.setCursor(Cursor.HAND);
                    summary.setId(lstData.get(0).toString());
                    summary.setUserData(strAccess);
                    summary.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {              
                            if(summary.getUserData().toString().equalsIgnoreCase("Public")){
                                objConfiguration.setServicePasswordValid(true);
                            } else{
                                new MessageView(objConfiguration);
                            }
                            if(objConfiguration.getServicePasswordValid()){
                                objConfiguration.setServicePasswordValid(false);
                                objConfiguration.strWindowFlowContext = "Dashboard";
                                objConfiguration.setStrRecentCloth(summary.getId());
                                windowStage.close();
                                System.gc();
                                ClothView objClothView = new ClothView(objConfiguration);
                            }
                        }
                    });
                    } catch (IOException ex) {
                        new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                    } finally {
                        try {
                            stream.close();
                        } catch (IOException ex) {
                            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                        }
                    }
                clothList.add(summary,i%2,i/2);
            }
        }
        lstClothDetails = null; 
        //Ploting recent fabric data
        try{                
            objFabricThread.join();
            lstFabricDetails = objFabricRunnable.lstDataDetails;
        } catch (InterruptedException ex){

        }    
        if(lstFabricDetails.size()==0){
            fabricList.getChildren().add(new Text(objDictionaryAction.getWord("FABRIC")+" - "+objDictionaryAction.getWord("NOVALUE")));
        }else{         
            SeekableStream stream = null;           
            Rectangle preview;
            for (int i=0, j = lstFabricDetails.size(); i<j && i<15; i++){
                lstData = (ArrayList)lstFabricDetails.get(i);

                final Label summary = new Label();
                preview = new Rectangle(objConfiguration.WIDTH*0.06,objConfiguration.WIDTH*0.06);
                preview.setArcHeight(10);
                preview.setArcWidth(10);
                preview.setTranslateX(1);
                preview.setTranslateY(1);
                preview.setEffect(new DropShadow(3, Color.BLACK));
                try {
                    byte[] bytWeaveThumbnil = (byte[])lstData.get(26);
                    stream = new ByteArraySeekableStream(bytWeaveThumbnil);
                    String[] names = ImageCodec.getDecoderNames(stream);
                    ImageDecoder dec = ImageCodec.createImageDecoder(names[0], stream, null);
                    RenderedImage im = dec.decodeAsRenderedImage();
                    BufferedImage bufferedImage = PlanarImage.wrapRenderedImage(im).getAsBufferedImage();
                    Image image=SwingFXUtils.toFXImage(bufferedImage, null);
                    String strTooltip = 
                            objDictionaryAction.getWord("NAME")+": "+lstData.get(1).toString()+"\n"+
                            objDictionaryAction.getWord("CLOTHTYPE")+": "+lstData.get(2)+"\n"+
                            objDictionaryAction.getWord("FABRICTYPE")+": "+lstData.get(3)+"\n"+
                            objDictionaryAction.getWord("FABRICLENGTH")+": "+lstData.get(4)+"\n"+
                            objDictionaryAction.getWord("FABRICWIDTH")+": "+lstData.get(5)+"\n"+
                            objDictionaryAction.getWord("ARTWORKLENGTH")+": "+lstData.get(6)+"\n"+
                            objDictionaryAction.getWord("ARTWORKWIDTH")+": "+lstData.get(7)+"\n"+
                            objDictionaryAction.getWord("WEFT")+": "+lstData.get(10)+"\n"+
                            objDictionaryAction.getWord("WARP")+": "+lstData.get(11)+"\n"+
                            objDictionaryAction.getWord("SHAFT")+": "+lstData.get(14)+"\n"+
                            objDictionaryAction.getWord("HOOKS")+": "+lstData.get(15)+"\n"+
                            objDictionaryAction.getWord("HPI")+": "+lstData.get(16)+"\n"+
                            objDictionaryAction.getWord("REEDCOUNT")+": "+lstData.get(17)+"\n"+
                            objDictionaryAction.getWord("DENTS")+": "+lstData.get(18)+"\n"+
                            objDictionaryAction.getWord("TPD")+": "+lstData.get(19)+"\n"+                            
                            objDictionaryAction.getWord("EPI")+": "+lstData.get(20)+"\n"+
                            objDictionaryAction.getWord("PPI")+": "+lstData.get(21)+"\n"+
                            objDictionaryAction.getWord("PROTECTION")+": "+lstData.get(22)+"\n"+
                            objDictionaryAction.getWord("BINDING")+": "+lstData.get(23)+"\n"+                            
                            objDictionaryAction.getWord("PERMISSION")+": "+lstData.get(29)+"\n"+
                            objDictionaryAction.getWord("BY")+": "+lstData.get(31).toString()+"\n"+
                            objDictionaryAction.getWord("DATE")+": "+lstData.get(30).toString();

                    preview.setFill(new ImagePattern(image));
                    summary.setGraphic(preview);
                    summary.setCursor(Cursor.HAND);
                    summary.setTooltip(new Tooltip(strTooltip));
                    summary.setEllipsisString(lstData.get(2).toString());
                    summary.setId(lstData.get(0).toString());
                    summary.setUserData(lstData.get(29).toString());
                    summary.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {              
                            if(summary.getUserData().toString().equalsIgnoreCase("Public")){
                                objConfiguration.setServicePasswordValid(true);
                            } else{
                                new MessageView(objConfiguration);
                            }
                            if(objConfiguration.getServicePasswordValid()){
                                objConfiguration.setServicePasswordValid(false);
                                try {
                                    objConfiguration.setStrClothType(summary.getEllipsisString());
                                    UserAction objUserAction = new UserAction();
                                    objUserAction.getConfiguration(objConfiguration);
                                    //objConfiguration.clothRepeat();
                                    objConfiguration.strWindowFlowContext = "Dashboard";
                                    objConfiguration.setStrRecentFabric(summary.getId());
                                    windowStage.close();
                                    System.gc();
                                    FabricView objFabricView = new FabricView(objConfiguration);
                                } catch (SQLException ex) {
                                    new Logging("SEVERE",getClass().getName(),ex.toString()+"Load Fabric in Fabric Module",ex);
                                }
                            }
                        }
                    });                    
                } catch (IOException ex) {
                    new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                } finally {
                    try {
                        stream.close();
                    } catch (Exception ex) {
                        new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                    }
                }                
                fabricList.add(summary,i%3,i/3);
            }
        }
        lstFabricDetails = null;
        //ploting recent artwork data        
        try{                
            objArtworkThread.join();
            lstArtworkDetails = objArtworkRunnable.lstDataDetails;
        } catch (InterruptedException ex){

        }
        if(lstArtworkDetails.size()==0){
            artworkList.getChildren().add(new Text(objDictionaryAction.getWord("ARTWORK")+" - "+objDictionaryAction.getWord("NOVALUE")));
        }else{
            SeekableStream stream = null;
            Rectangle preview;
            for (int i=0, j = lstArtworkDetails.size(); i<j && i<15;i++){
                lstData = (ArrayList)lstArtworkDetails.get(i);
                final Label summary = new Label();
                preview = new Rectangle(objConfiguration.WIDTH*0.06,objConfiguration.WIDTH*0.06);
                preview.setArcHeight(10);
                preview.setArcWidth(10);
                preview.setTranslateX(1);
                preview.setTranslateY(1);
                preview.setEffect(new DropShadow(3, Color.BLACK));
                try {
                    byte[] bytArtworkThumbnil = (byte[])lstData.get(2);
                    stream = new ByteArraySeekableStream(bytArtworkThumbnil);
                    String[] names = ImageCodec.getDecoderNames(stream);
                    ImageDecoder dec = ImageCodec.createImageDecoder(names[0], stream, null);
                    RenderedImage im = dec.decodeAsRenderedImage();
                    BufferedImage bufferedImage = PlanarImage.wrapRenderedImage(im).getAsBufferedImage();
                    Image image=SwingFXUtils.toFXImage(bufferedImage, null);
                    String strAccess =lstData.get(6).toString();
                    String strTooltip = 
                            objDictionaryAction.getWord("NAME")+": "+lstData.get(1).toString()+"\n"+
                            objDictionaryAction.getWord("ARTWORKLENGTH")+": "+bufferedImage.getHeight()+"\n"+
                            objDictionaryAction.getWord("ARTWORKWIDTH")+": "+bufferedImage.getWidth()+"\n"+
                            objDictionaryAction.getWord("BACKGROUND")+": "+lstData.get(3).toString()+"\n"+
                            objDictionaryAction.getWord("PERMISSION")+": "+strAccess+"\n"+
                            objDictionaryAction.getWord("BY")+": "+lstData.get(5).toString()+"\n"+
                            objDictionaryAction.getWord("DATE")+": "+lstData.get(4).toString();

                    preview.setFill(new ImagePattern(image));
                    summary.setGraphic(preview);
                    summary.setCursor(Cursor.HAND);
                    summary.setTooltip(new Tooltip(strTooltip));
                    summary.setId(lstData.get(0).toString());
                    summary.setUserData(strAccess);
                    summary.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {              
                            if(summary.getUserData().toString().equalsIgnoreCase("Public")){
                                objConfiguration.setServicePasswordValid(true);
                            } else{
                                new MessageView(objConfiguration);
                            }
                            if(objConfiguration.getServicePasswordValid()){
                                objConfiguration.setServicePasswordValid(false);
                                objConfiguration.strWindowFlowContext = "Dashboard";
                                objConfiguration.setStrRecentArtwork(summary.getId());
                                windowStage.close();
                                System.gc();
                                ArtworkView objArtworkView = new ArtworkView(objConfiguration);
                            }
                        }
                    });
                } catch (IOException ex) {
                    new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                } finally {
                    try {
                        stream.close();
                    } catch (IOException ex) {
                        new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                    }
                }
                artworkList.add(summary,i%3,i/3);   
            }
        }
        lstArtworkDetails = null;
        //ploting recent weave data        
        try{                
            objWeaveThread.join();
            lstWeaveDetails = objWeaveRunnable.lstDataDetails;
        } catch (InterruptedException ex){

        }
        if(lstWeaveDetails.size()==0){
            weaveList.getChildren().add(new Text(objDictionaryAction.getWord("WEAVE")+" - "+objDictionaryAction.getWord("NOVALUE")));
        }else{            
            SeekableStream stream = null;                        
            Rectangle preview;
            for (int i=0, j=lstWeaveDetails.size(); i<j && i<15; i++){
                lstData = (ArrayList)lstWeaveDetails.get(i);
                final Label summary = new Label();
                preview = new Rectangle(objConfiguration.WIDTH*0.06,objConfiguration.WIDTH*0.06);
                preview.setArcHeight(10);
                preview.setArcWidth(10);
                preview.setTranslateX(1);
                preview.setTranslateY(1);
                preview.setEffect(new InnerShadow(3, Color.BLUE));
                try {
                    byte[] bytWeaveThumbnil = (byte[])lstData.get(2);
                    stream = new ByteArraySeekableStream(bytWeaveThumbnil);
                    String[] names = ImageCodec.getDecoderNames(stream);
                    ImageDecoder dec = ImageCodec.createImageDecoder(names[0], stream, null);
                    RenderedImage im = dec.decodeAsRenderedImage();
                    BufferedImage bufferedImage = PlanarImage.wrapRenderedImage(im).getAsBufferedImage();
                    Image image=SwingFXUtils.toFXImage(bufferedImage, null);
                    String strAccess = new IDGenerator().getUserAcessValueData("WEAVE_LIBRARY",lstData.get(15).toString());
                    String strTooltip = 
                            objDictionaryAction.getWord("NAME")+": "+lstData.get(1).toString()+"\n"+
                            objDictionaryAction.getWord("SHAFT")+": "+lstData.get(7).toString()+"\n"+
                            objDictionaryAction.getWord("TRADELES")+": "+lstData.get(8).toString()+"\n"+
                            objDictionaryAction.getWord("WEFTREPEAT")+": "+lstData.get(9).toString()+"\n"+
                            objDictionaryAction.getWord("WARPREPEAT")+": "+lstData.get(10).toString()+"\n"+
                            objDictionaryAction.getWord("WEFTFLOAT")+": "+lstData.get(11).toString()+"\n"+
                            objDictionaryAction.getWord("WARPFLOAT")+": "+lstData.get(12).toString()+"\n"+
                            objDictionaryAction.getWord("WEAVECATEGORY")+": "+lstData.get(4).toString()+"\n"+
                            objDictionaryAction.getWord("WEAVETYPE")+": "+lstData.get(3).toString()+"\n"+
                            objDictionaryAction.getWord("ISLIFTPLAN")+": "+lstData.get(5).toString()+"\n"+
                            objDictionaryAction.getWord("ISCOLOR")+": "+lstData.get(6).toString()+"\n"+
                            objDictionaryAction.getWord("PERMISSION")+": "+strAccess+"\n"+
                            objDictionaryAction.getWord("BY")+": "+lstData.get(14).toString()+"\n"+
                            objDictionaryAction.getWord("DATE")+": "+lstData.get(13).toString();    
                    preview.setFill(new ImagePattern(image));
                    summary.setGraphic(preview);
                    summary.setTooltip(new Tooltip(strTooltip));
                    summary.setCursor(Cursor.HAND);
                    summary.setId(lstData.get(0).toString());
                    summary.setUserData(strAccess);
                    summary.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {              
                            if(summary.getUserData().toString().equalsIgnoreCase("Public")){
                                objConfiguration.setServicePasswordValid(true);
                            } else{
                                new MessageView(objConfiguration);
                            }
                            if(objConfiguration.getServicePasswordValid()){
                                objConfiguration.setServicePasswordValid(false);
                                objConfiguration.strWindowFlowContext = "Dashboard";
                                objConfiguration.setStrRecentWeave(summary.getId());
                                windowStage.close();
                                System.gc();
                                WeaveView objWeaveView = new WeaveView(objConfiguration);
                            }
                        }
                    });
                    } catch (IOException ex) {
                        new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                    } finally {
                        try {
                            stream.close();
                        } catch (IOException ex) {
                            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                        }
                    }
                weaveList.add(summary,i%3,i/3);
            }
        }
        lstWeaveDetails = null;
        
        
    }
 
    
    /**
     * addAccelratorKey
     * <p>
     * Function use for adding shortcut key combinations. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void addAccelratorKey(){
        // variable names: kc + Alphabet + ALT|CONTROL|SHIFT // ACS alphabetical
        final KeyCodeCombination fabricKCC = new KeyCodeCombination(KeyCode.J, KeyCombination.SHIFT_DOWN);
        final KeyCodeCombination artworkKCC = new KeyCodeCombination(KeyCode.D, KeyCombination.SHIFT_DOWN);
        final KeyCodeCombination weaveKCC = new KeyCodeCombination(KeyCode.W, KeyCombination.SHIFT_DOWN);
        final KeyCodeCombination clothKCC = new KeyCodeCombination(KeyCode.G, KeyCombination.SHIFT_DOWN);
        final KeyCodeCombination userKCC = new KeyCodeCombination(KeyCode.P, KeyCombination.SHIFT_DOWN);
        final KeyCodeCombination utilityKCC = new KeyCodeCombination(KeyCode.U, KeyCombination.SHIFT_DOWN);
        final KeyCodeCombination logoutKCC = new KeyCodeCombination(KeyCode.L, KeyCombination.SHIFT_DOWN);
        scene.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                if(fabricKCC.match(t))
                    fabricVBAction();
                else if(artworkKCC.match(t))
                    artworkVBAction();
                else if(weaveKCC.match(t))
                    weaveVBAction();
                else if(clothKCC.match(t))
                    garmentVBAction();
                else if(userKCC.match(t))
                    userVBAction();
                else if(utilityKCC.match(t))
                    utilityVBAction();
                else if(logoutKCC.match(t))
                    logoutAction();                
            }
        });
    }
    /**
     * exitMenuAction
     * <p>
     * Function use for exit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void exitMenuAction(){
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.setIconified(false);
        dialogStage.setFullScreen(false);
        dialogStage.setTitle(objDictionaryAction.getWord("ALERT"));
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 300, 100, Color.WHITE);
        scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        popup.add(new ImageView("/media/alert_warning.png"), 0, 0);   
        Label lblAlert = new Label(objDictionaryAction.getWord("ALERTCLOSE"));
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(250);
        popup.add(lblAlert, 1, 0);
        Button btnYes = new Button(objDictionaryAction.getWord("YES"));
        btnYes.setPrefWidth(50);
        btnYes.setId("btnYes");
        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
                windowStage.close();
            }
        });
        popup.add(btnYes, 0, 1);
        Button btnNo = new Button(objDictionaryAction.getWord("NO"));
        btnNo.setPrefWidth(50);
        btnNo.setId("btnNo");
        btnNo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
            }
        });
        popup.add(btnNo, 1, 1);
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.showAndWait();
        System.gc();
    }
    
    private void logoutAction(){
        try {
            UserAction objUserAction= new UserAction();
            //objUserAction.trackUser(objConfiguration.getObjUser(), "LOGOUT", "Log-Out done", null);                    
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"Logout Tracking",ex);
        }
        windowStage.close();
        System.gc();
        UserLoginView objUserLoginView=new UserLoginView(objConfiguration); 
    }
    private void fabricVBAction(){
        objConfiguration.strWindowFlowContext = "Dashboard";
        System.gc();
        windowStage.close();
        FabricView objFabricView = new FabricView(objConfiguration);
    }    
    private void artworkVBAction(){
        objConfiguration.strWindowFlowContext = "Dashboard";
        System.gc();
        windowStage.close();
        ArtworkView objArtworkView= new ArtworkView(objConfiguration);
    }    
    private void weaveVBAction(){
        objConfiguration.strWindowFlowContext = "Dashboard";
        System.gc();
        windowStage.close();
        WeaveView objWeaveWindow= new WeaveView(objConfiguration);
    }  
    private void garmentVBAction(){
        objConfiguration.strWindowFlowContext = "Dashboard";
        System.gc();
        windowStage.close();
        ClothView objClothView = new ClothView(objConfiguration);
    }
    private void yarnVBAction(){
        System.gc();
        windowStage.close();
    }
    private void userVBAction(){
        System.gc();
        windowStage.close();
        UserView objUserView = new UserView(objConfiguration);
    }
    private void utilityVBAction(){
        System.gc();
        windowStage.close();
        UtilityView objUtilityView = new UtilityView(objConfiguration);
    }
    
    public void addStackPane(HBox hb) {
        
        
        StackPane stack = new StackPane();
        Rectangle helpIcon = new Rectangle(30.0, 25.0);
       /* helpIcon.setFill(new LinearGradient(0,0,0,1, true, MultipleGradientPaint.CycleMethod.NO_CYCLE,
            new Stop[]{
            new Stop(0,Color.web("#4977A3")),
            new Stop(0.5, Color.web("#B0C6DA")),
            new Stop(1,Color.web("#9CB6CF")),}));*/
        helpIcon.setStroke(Color.web("#D0E6FA"));
        helpIcon.setArcHeight(3.5);
        helpIcon.setArcWidth(3.5);

        Text helpText = new Text("?");
        helpText.setFont(Font.font("Verdana", FontWeight.BOLD, 18));
        helpText.setFill(Color.WHITE);
        helpText.setStroke(Color.web("#7080A0")); 

        stack.getChildren().addAll(helpIcon, helpText);
        stack.setAlignment(Pos.CENTER_RIGHT);     // Right-justify nodes in stack
        StackPane.setMargin(helpText, new Insets(0, 10, 0, 0)); // Center "?"

        hb.getChildren().add(stack);            // Add to HBox from Example 1-2
        HBox.setHgrow(stack, Priority.ALWAYS);    // Give stack any extra space
    }
    
    private void initChatBox(){
        chatBoxStage = new Stage();
        chatBoxStage.initOwner(windowStage);
        chatBoxStage.initStyle(StageStyle.UNDECORATED);
        //chatBoxStage.initStyle(StageStyle.UTILITY);
        //dialogStage.initModality(Modality.WINDOW_MODAL);
        //chatBoxStage.initModality(Modality.APPLICATION_MODAL);
        chatBoxStage.setResizable(false);
        chatBoxStage.setIconified(false);
        chatBoxStage.setFullScreen(false);
        chatBoxStage.setTitle(objDictionaryAction.getWord("ALERT"));
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 222, 444, Color.WHITE);
        scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        ScrollPane container = new ScrollPane();
        final VBox chatBox = new VBox(5); 
        container.setPrefSize(222, 333);
        container.setContent(chatBox);
        
        final GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        popup.add(container, 0, 0, 2, 1);
        Label lblAlert = new Label("Please add your message");
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(250);
        popup.add(lblAlert, 0, 1, 2, 1);
        final TextArea txtMessage = new TextArea();
        txtMessage.setPrefRowCount(10);
        txtMessage.setPrefColumnCount(100);
        txtMessage.setWrapText(true);
        txtMessage.setPrefWidth(150);
        GridPane.setHalignment(txtMessage, HPos.CENTER);
        popup.add(txtMessage, 0, 2, 1, 2);
        Button btnAdd = new Button(objDictionaryAction.getWord("ADD"));
        btnAdd.setPrefWidth(50);
        btnAdd.setId("btnYes");
        btnAdd.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                messages.add(new Label(txtMessage.getText()));
                messages.get(index).setStyle("-fx-background-color:purple; -fx-text-fill:white; -fx-pref-height:20px;  -fx-pref-width:200px; ");
                if(index%2==0){
                    messages.get(index).setAlignment(Pos.CENTER_LEFT);
                    //System.out.println(index%2);
                }else{
                    messages.get(index).setAlignment(Pos.CENTER_RIGHT);
                    //System.out.println(index%2);
                }
                chatBox.getChildren().add(messages.get(index));
                index++;
            }
        });
        popup.add(btnAdd, 1, 2, 1, 1);
        Button btnClear = new Button(objDictionaryAction.getWord("Clear"));
        btnClear.setPrefWidth(50);
        btnClear.setId("btnNo");
        btnClear.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                txtMessage.setText("");
            }
        });
        popup.add(btnClear, 1, 3, 1, 1);
        
        root.setCenter(popup);
        chatBoxStage.setScene(scene);
        chatBoxStage.setX(objConfiguration.WIDTH-222);
        chatBoxStage.setY(objConfiguration.HEIGHT-444);
        chatBoxStage.hide();
    }

    
    
    @Override
    public void start(Stage stage) throws Exception {
        new WindowView(stage);
        new Logging("WARNING",getClass().getName(),"UnsupportedOperationException",new UnsupportedOperationException("Not supported yet."));
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

class WindowViewData implements Runnable{
    Object objItem;
    List lstDataDetails;
    
    @Override
    public void run() {
        try{
            if(objItem instanceof Cloth){ 
                Cloth objCloth = (Cloth)objItem;
                ClothAction objClothAction = new ClothAction();
                lstDataDetails = objClothAction.lstImportCloth(objCloth);
                //System.err.println("I am "+objItem.getClass().getName()+" and my data size are "+lstDataDetails.size());
            } else if(objItem instanceof Fabric){   
                Fabric objFabric = (Fabric)objItem;
                FabricAction objFabricAction = new FabricAction();
                lstDataDetails = objFabricAction.lstImportFabric(objFabric);
                //System.err.println("I am "+objItem.getClass().getName()+" and my data size are "+lstDataDetails.size());
            } else if(objItem instanceof Artwork){  
                Artwork objArtwork = (Artwork)objItem;
                ArtworkAction objArtworkAction = new ArtworkAction();
                lstDataDetails = objArtworkAction.lstImportArtwork(objArtwork);
                //System.err.println("I am "+objArtwork.getClass().getName()+" and my data size are "+lstDataDetails.size());
            } else if(objItem instanceof Weave){   
                Weave objWeave = (Weave)objItem;
                WeaveAction objWeaveAction = new WeaveAction();
                lstDataDetails = objWeaveAction.lstImportWeave(objWeave);
                //System.err.println("I am "+objWeave.getClass().getName()+" and my data size are "+lstDataDetails.size());
            }            
        } catch (SQLException ex){
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }
}