/*
 * Copyright (C) 2018 Digital India Corporation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.mla.print;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.RenderedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
     
import java.util.ArrayList;
import java.util.Collection;
     
 
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.PrintRequestAttributeSet;

/**
 * 
 * @author Aatif Ahmad Khan
 */
public class MultiplePagePrint implements Printable{

    protected PrintView printViewInstance=null;
    protected ArrayList listPages = new ArrayList<String>();
      
    @SuppressWarnings("unchecked")
    public void print(PrintView printViewInstance, Collection listPages, String printerName, PrintRequestAttributeSet pras) 
    {
        this.printViewInstance=printViewInstance;
        this.listPages.clear();
        this.listPages.addAll(listPages);
        printData(printerName, pras);
    }
 
    protected void printData(String printerName, PrintRequestAttributeSet pras) 
    {
        String printer = printerName;           
             
        DocFlavor flavor = DocFlavor.SERVICE_FORMATTED.PRINTABLE;
        PrintService printService[] = PrintServiceLookup.lookupPrintServices(null, null);
        PrintService service = null;
 
             
        for (int i = 0; i < printService.length; i++)
        {
            String p_name = printService[i].getName();
            if (p_name.equals(printer))
            {
                service = printService[i];
            }
        }           
             
 
        try
        {
            DocPrintJob pj = service.createPrintJob();
            //PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
            //aset.add(new JobName(requestNumber, null));
            //aset.add(MediaSizeName.NA_LETTER);
            Doc doc = new SimpleDoc(this, flavor, null);
            pj.print(doc, pras);
        } 
        catch (PrintException pe) 
        {
            System.err.println(pe);
        }
    }
 
    public int print(Graphics g, PageFormat f, int pageIndex) 
    {
        if (pageIndex >= listPages.size()) 
        {
            return Printable.NO_SUCH_PAGE;
        }
        
        RenderedImage image = (RenderedImage) printViewInstance.generateActualPage(Integer.parseInt(listPages.get(pageIndex).toString()));//imageList.get(pageIndex);
 
        if (image != null) {
            Graphics2D g2 = (Graphics2D) g;
            g2.translate(f.getImageableX(), f.getImageableY());
 
            int imgWidth = (int)image.getWidth();
            int imgHeight = (int)image.getHeight();
            double xRatio = (double) f.getImageableWidth() / (double) imgWidth;
            double yRatio = (double) f.getImageableHeight() / (double) imgHeight;
 
            g2.scale(xRatio, yRatio);
 
            g2.drawRenderedImage(image, null);
                 
            return Printable.PAGE_EXISTS;
        } 
        else
        {
            return Printable.NO_SUCH_PAGE;
        }
    }
    
}
