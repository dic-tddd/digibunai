/*
 * Copyright (C) Digital India Corporation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.mla.print;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.print.DocFlavor;
import javax.print.MultiDoc;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.MediaPrintableArea;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.PrinterResolution;

/**
 * GUI-based Customization for printing features
 * @author Aatif Ahmad Khan
 */
public class PrintView {
    
    private final int DPI=300; // target DPI for high quality printing
    private final int DPI_JAVA=72; // Java assumes everything at 72 DPI
    private final double A4_WIDTH_INCHES=8.267;
    private final double A4_HEIGHT_INCHES=11.692;
    
    // Left Top (X, Y), printable width and heights (default: A4)
    // Note 1: Since java assumes 72DPI, imageable(X, Y) should be calculated using
    // page margins (inches) * Java DPI.
    // Note 2: Printable image pixels calculated using 300 DPI and then put in imageable area
    private double imageableX=0.5*DPI_JAVA;//150;//36;
    private double imageableY=0.5*DPI_JAVA;//150;//36;
    private double pageWidth=(A4_WIDTH_INCHES-1)*DPI+2*imageableX;//2482.8;//595.224;
    private double pageHeight=(A4_HEIGHT_INCHES-1)*DPI+2*imageableY;//3507.6;//841.824;
    private double imageableWidth=pageWidth-2*imageableX;//2182.8;//523.224;
    private double imageableHeight=pageHeight-2*imageableY;//3207.6;//769.824;
    
    // image printing should start here (excluding header, numbering)
    private double actualX;
    private double actualY;
    private double actualWidth;
    private double actualHeight;
    
    // default margins (inches)
    private final double defaultLeftMargin=0.5;
    private final double defaultRightMargin=0.5;
    private final double defaultTopMargin=0.5;
    private final double defaultBottomMargin=0.5;
    
    // user defined margins (inches)
    private double leftMarginInches=defaultLeftMargin;
    private double rightMarginInches=defaultRightMargin;
    private double topMarginInches=defaultTopMargin;
    private double bottomMarginInches=defaultBottomMargin;
    
    // showing page preview
    private int totalPreviewPages;
    private int currentPreviewPage; // also used in 'Print Current Page'
    private String strPreviewPage; // Page x of totalPages
    
    final String regExDouble="[0-9]{1,13}(\\.[0-9]*)?";
    final String regExInt="[0-9]+";
    //final String regExPages="([0-9]+-[0-9]+)+";
    
    private boolean isPortrait; // page orientation
    
    // aspect ratio of A4 page
    final private double pageLongerSide=pageHeight;//3507.6;//841.824;
    final private double pageSmallerSide=pageWidth;//2482.8;//595.224;
    // for showing small preview (Currently in ratio of A4 paper sides)
    final private int previewLongerSide=283;
    final private int previewSmallerSide=200;
    
    // space reserved for header
    final private int headerPixel=175;//*DPI/DPI_JAVA//42;
    // space reserved for horizontal and vertical numbering (if isNumbering is true)
    final private int numberingPixel=84;//*DPI/DPI_JAVA//20;
    
    // pixels for width of options pane in printing dialog
    final private int prefWidth=250;
    
    private int G0=12;
    private int G1=16;
    private int gridSize=20;
    private final int GRID_NUM=20;
    
    private boolean isGraphImage=false;
    private boolean isGridImage=false;
    private boolean isNumbering=false;
    private boolean isMinorNumbering=false;
    private boolean headerOff=false;
    
    private boolean isPage=true;
    private boolean isName=false;
    private boolean isOwner=false;
    private boolean isDate=false;
    private boolean isOrg=false;
    private boolean isLogo=true;
    
    final private double minMargin=0.2; // inches
    final private double maxMargin=2.5; // inches
    final private int minGraphGrid=5;
    final private int maxGraphGrid=50;
    
    final private int defaultG0=G0;//16;
    final private int defaultG1=G1;//12;
    final private int defaultGridSize=20;
    
    private int totalPages;
    
    private BufferedImage fullPage;
    private BufferedImage imageToPrint;
    private BufferedImage defaultImageWithDPI;
    private BufferedImage portionImage;
    
    private int numPageHor;
    private int numPageVer;
    
    private ImageView previewIV;
    private Label lblPreviewPage;
    
    final private int fontSize=30;//10;
    final private int smallFontSize=7;
    
    private ArrayList<String> lstPageIndexToPrint;
    
    Image logo;
    
    // optional texts
    private final String CAPTION_ITEM="Name: ";
    private final String CAPTION_USER="Created By: ";
    private final String CAPTION_DATE="Date: ";
    private final String CAPTION_ORG="Organization: ";
    
    private final String CAPTION_DIC="Digital India Corporation";
    
    // (e.g. Page 1 of 3)
    private final String CAPTION_PAGE="Page ";
    
    // date will be generated dynamically each time
    private String itemName;
    private String itemOwner;
    private String itemOrg;
    
    private int startPageNumber=1;
    private int endPageNumber=0;
    private boolean printCurrentPage=false;
    
    private boolean isSinglePage=false;
    private boolean isFitToPage=false;
    
    private int boxSize=defaultG1;
    private int autoBoxSize=defaultG0;
    
    private PrintRequestAttributeSet pras;
    private PrintService[] pss;
    private int printerIndex=0;
    
    private int fullPrintableImageWidth;
    private int fullPrintableImageHeight;
    
    PrintView instance;
    
    private int numFitWarp; // updated by getImageByPage for each page and
    private int numFitWeft; // utilized by generateActualPage() for numbering
    
    CheckBox chkSinglePageGraph;
    
    public PrintView(final BufferedImage imageToPrint, int printMode, String... param){ // input a BufferedImage imageToPrint
        instance=this;
        this.imageToPrint=imageToPrint;
        // initialize variables
        itemName="Image";
        itemOwner="User";
        itemOrg="DIC";
        
        if(param.length>0){
            // param specification Name, Username, GraphSize G0xG1
            if(param[0]!=null)
                itemName=param[0];
            if(param.length>1){
                if(param[1]!=null)
                    itemOwner=param[1];
                if(param.length>2){
                    // validate G0xG1 format
                    if(param[2]!=null&&param[2].contains("x")){
                        try{
                            G0=Integer.parseInt(param[2].substring(0, param[2].indexOf("x")));
                            G1=Integer.parseInt(param[2].substring(param[2].indexOf("x")+1));
                        }
                        catch(NumberFormatException nEx){
                            G0=12;
                            G1=16;
                        }
                    }
                }
            }
        }
        
        if(printMode==10)
            isGridImage=true;
        else if(printMode==11)
            isGraphImage=true;
        
        logo=new Image("/media/logo125.png");
        isPortrait=true;
        
        // calculating printable area
        calculateImageableArea(); // this will set imageableX,imageableY,imageableWidth,imageableHeight
        setPageSize(); // this will set pageWidth and pageHeight
        
        currentPreviewPage=1;
        setTotalPrintImageWidthHeight(imageToPrint);
        
        if(!isGraphImage&&!isGridImage)
            defaultImageWithDPI=getDefaultImageWithDPI(imageToPrint);
        
        setPageAndArea();
        // initialize lstPageIndexToPrint
        lstPageIndexToPrint=new ArrayList<>();
        calculateTotalPages(); // it sets totalPages
        lstPageIndexToPrint.clear();
        // add all pages
        for(int p=1; p<=totalPages; p++)
            lstPageIndexToPrint.add(String.valueOf(p));
        strPreviewPage=getStrPreviewPage();
        
        //-------------- PRINTER TAB -----------------//
        GridPane printerGP=new GridPane();
        printerGP.setPrefWidth(prefWidth);
        printerGP.setHgap(5);
        printerGP.setVgap(5);
        printerGP.setPadding(new Insets(5, 5, 5, 5));
        
        
        pras = new HashPrintRequestAttributeSet();
        pras.add(new Copies(1));
        pras.add(new PrinterResolution(600, 600, PrinterResolution.DPI));
        pss = PrintServiceLookup.lookupPrintServices(DocFlavor.INPUT_STREAM.PNG, pras);
        if (pss.length == 0)
          throw new RuntimeException("No printer services available.");
        
        final ComboBox printerList=new ComboBox();
        for(int c=0; c<pss.length; c++){
            printerList.getItems().add(pss[c].getName());
        }
        
        if(pss.length>0)
            printerList.setValue(printerList.getItems().get(0));
        
        printerList.valueProperty().addListener(new ChangeListener() {

            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                printerIndex=printerList.getItems().indexOf(t1);
            }
        });
        
        printerGP.add(printerList, 0, 0);
        
        //-------------- PAPER TAB -------------------//
        // this tab will be having Orientation and Margin Customization
        GridPane paperGP=new GridPane();
        paperGP.setPrefWidth(prefWidth);
        paperGP.setHgap(5);
        paperGP.setVgap(5);
        paperGP.setPadding(new Insets(5, 5, 5, 5));
        
        GridPane orientationGP=new GridPane();
        orientationGP.setVgap(5);
        Label lblOrientation=new Label("Orientation");
        orientationGP.add(lblOrientation, 0, 0);
        final ToggleGroup orientationTG = new ToggleGroup();
        final RadioButton portraitRB = new RadioButton("Portrait");
        portraitRB.setToggleGroup(orientationTG);
        portraitRB.setSelected(true);
        portraitRB.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                isPortrait=t1;
                updatePreviewLook();
            }
        });
        orientationGP.add(portraitRB, 0, 1);
        final RadioButton landscapeRB = new RadioButton("Landscape");
        landscapeRB.setToggleGroup(orientationTG);
        landscapeRB.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                isPortrait=!t1;
                updatePreviewLook();
            }
        });
        orientationGP.add(landscapeRB, 0, 2);
        
        GridPane marginGP=new GridPane();
        marginGP.setHgap(5);
        marginGP.setVgap(5);
        Label lblMargins=new Label("Page Margins (inches)");
        Label lblLeft=new Label("Left: ");
        Label lblRight=new Label("Right: ");
        Label lblTop=new Label("Top: ");
        Label lblBottom=new Label("Bottom: ");
        
        lblLeft.setMaxWidth(Integer.MAX_VALUE);
        lblRight.setMaxWidth(Integer.MAX_VALUE);
        lblTop.setMaxWidth(Integer.MAX_VALUE);
        lblBottom.setMaxWidth(Integer.MAX_VALUE);
        
        final TextField txtLeft=new TextField(String.valueOf(defaultLeftMargin));
        final TextField txtRight=new TextField(String.valueOf(defaultRightMargin));
        final TextField txtTop=new TextField(String.valueOf(defaultTopMargin));
        final TextField txtBottom=new TextField(String.valueOf(defaultBottomMargin));
        
        txtLeft.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!t1.matches(regExDouble))
                    txtLeft.setText(String.valueOf(defaultLeftMargin));
                else if(Double.parseDouble(t1)>=minMargin&&Double.parseDouble(t1)<=maxMargin){
                    leftMarginInches=Double.parseDouble(t1);
                    updatePreviewLook();
                }
            }
        });
        txtRight.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!t1.matches(regExDouble))
                    txtRight.setText(String.valueOf(defaultRightMargin));
                else if(Double.parseDouble(t1)>=minMargin&&Double.parseDouble(t1)<=maxMargin){
                    rightMarginInches=Double.parseDouble(t1);
                    updatePreviewLook();
                }
            }
        });
        txtTop.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!t1.matches(regExDouble))
                    txtTop.setText(String.valueOf(defaultTopMargin));
                else if(Double.parseDouble(t1)>=minMargin&&Double.parseDouble(t1)<=maxMargin){
                    topMarginInches=Double.parseDouble(t1);
                    updatePreviewLook();
                }
            }
        });
        txtBottom.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!t1.matches(regExDouble))
                    txtBottom.setText(String.valueOf(defaultBottomMargin));
                else if(Double.parseDouble(t1)>=minMargin&&Double.parseDouble(t1)<=maxMargin){
                    bottomMarginInches=Double.parseDouble(t1);
                    updatePreviewLook();
                }
            }
        });
        
        marginGP.add(lblMargins, 0, 0, 2, 1);
        marginGP.add(lblLeft, 0, 1);
        marginGP.add(txtLeft, 1, 1);
        marginGP.add(lblRight, 0, 2);
        marginGP.add(txtRight, 1, 2);
        marginGP.add(lblTop, 0, 3);
        marginGP.add(txtTop, 1, 3);
        marginGP.add(lblBottom, 0, 4);
        marginGP.add(txtBottom, 1, 4);
        
        paperGP.add(orientationGP, 0, 0);
        paperGP.add(marginGP, 0, 1);
        
        //----------------- PRINT TAB ----------------------//
        GridPane pagesGP=new GridPane();
        pagesGP.setPrefWidth(prefWidth);
        pagesGP.setVgap(5);
        pagesGP.setPadding(new Insets(5, 5, 5, 5));
        final TextField txtPageToPrint=new TextField(currentPreviewPage+"-"+totalPreviewPages);
        txtPageToPrint.setDisable(true);
        final ToggleGroup pagesTG = new ToggleGroup();
        final RadioButton allPagesRB = new RadioButton("All Pages");
        allPagesRB.setToggleGroup(pagesTG);
        allPagesRB.setSelected(true);
        allPagesRB.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                if(t1){
                    txtPageToPrint.setDisable(true);
                    printCurrentPage=false;
                    startPageNumber=1;
                    endPageNumber=totalPages;
                }
            }
        });
        pagesGP.add(allPagesRB, 0, 0);
        final RadioButton currentPageRB = new RadioButton("Current Page");
        currentPageRB.setToggleGroup(pagesTG);
        currentPageRB.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                if(t1){
                    txtPageToPrint.setDisable(true);
                    printCurrentPage=true;
                    startPageNumber=currentPreviewPage;
                    endPageNumber=currentPreviewPage;
                }
            }
        });
        pagesGP.add(currentPageRB, 0, 1);
        final RadioButton customPagesRB = new RadioButton("Page Range");
        customPagesRB.setToggleGroup(pagesTG);
        customPagesRB.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                printCurrentPage=false;
                if(t1){
                    txtPageToPrint.setDisable(false);
                }
            }
        });
        pagesGP.add(customPagesRB, 0, 2);
        txtPageToPrint.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!t1.contains("-")){
                    txtPageToPrint.setText("1-"+totalPages);
                    return;
                }
                int start=1;
                if(t1.indexOf("-")>0)
                    start=Integer.parseInt(t1.substring(0, t1.indexOf("-")));
                int end=totalPages;
                if(t1.indexOf("-")<t1.length()-1)
                    end=Integer.parseInt(t1.substring(t1.indexOf("-")+1));
                if(start>0&&end>0&&start<=totalPages&&end<=totalPages&&start<=end){
                    startPageNumber=start;
                    endPageNumber=end;
                }
            }
        });
        pagesGP.add(txtPageToPrint, 0, 3);
        
        chkSinglePageGraph=new CheckBox("Single Page");
        chkSinglePageGraph.setDisable(false);
        pagesGP.add(chkSinglePageGraph, 0, 4);
        
        //----------------- GRAPH TAB ----------------------//
        // This tab will be having graph related options
        
        GridPane graphGridGP=new GridPane();
        graphGridGP.setPrefWidth(prefWidth);
        graphGridGP.setVgap(5);
        graphGridGP.setPadding(new Insets(5, 5, 5, 5));
        final TextField txtG0=new TextField(String.valueOf(G0));
        final TextField txtG1=new TextField(String.valueOf(G1));
        final TextField txtBoxSize=new TextField(String.valueOf(G1));
        final TextField txtAutoBoxSize=new TextField(String.valueOf(G0));
        final TextField txtGridSize=new TextField(String.valueOf(gridSize));
        
        final CheckBox chkFitToPage=new CheckBox("Fit To Page"); // single page image made fit to page
        txtG0.setDisable(!isGraphImage);
        txtG1.setDisable(!isGraphImage);
        txtBoxSize.setDisable(!isGraphImage);
        txtAutoBoxSize.setDisable(true);
        txtGridSize.setDisable(!isGridImage);
        chkFitToPage.setDisable(totalPages!=1);
        
        final CheckBox chkInnerNum=new CheckBox("Minor Numbering");
        final CheckBox chkOuterNum=new CheckBox("Numbering");
        
        chkSinglePageGraph.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                isSinglePage=t1;
                chkOuterNum.setDisable(t1);
                chkInnerNum.setDisable(t1);
                if(t1){
                    chkOuterNum.setSelected(false);
                    chkInnerNum.setSelected(false);
                }
                updatePreviewLook();
            }
        });
        
        txtG0.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!t1.matches(regExInt))
                    txtG0.setText(String.valueOf(defaultG0));
                else if(Integer.parseInt(t1)>=minGraphGrid&&Integer.parseInt(t1)<=maxGraphGrid){
                    G0=Integer.parseInt(t1);
                    setTotalPrintImageWidthHeight(imageToPrint);
                    updatePreviewLook();
                }
            }
        });
        txtG1.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!t1.matches(regExInt))
                    txtG1.setText(String.valueOf(defaultG1));
                else if(Integer.parseInt(t1)>=minGraphGrid&&Integer.parseInt(t1)<=maxGraphGrid){
                    G1=Integer.parseInt(t1);
                    setTotalPrintImageWidthHeight(imageToPrint);
                    updatePreviewLook();
                }
            }
        });
        txtBoxSize.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!t1.matches(regExInt))
                    txtBoxSize.setText(String.valueOf(defaultG1));
                else if(Integer.parseInt(t1)>=minGraphGrid&&Integer.parseInt(t1)<=maxGraphGrid){
                    boxSize=Integer.parseInt(t1);
                    // check defaultG0, defaultG1 is set according to EPI/PPI
                    autoBoxSize=(int)(((double)defaultG0/(double)defaultG1)*(double)boxSize);
                    txtAutoBoxSize.setText(String.valueOf(autoBoxSize));
                    setTotalPrintImageWidthHeight(imageToPrint);
                    updatePreviewLook();
                }
            }
        });
        txtGridSize.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!t1.matches(regExInt))
                    txtGridSize.setText(String.valueOf(defaultGridSize));
                else if(Integer.parseInt(t1)>=minGraphGrid&&Integer.parseInt(t1)<=maxGraphGrid){
                    gridSize=Integer.parseInt(t1);
                    setTotalPrintImageWidthHeight(imageToPrint);
                    updatePreviewLook();
                }
            }
        });
        
        // content tab
        chkInnerNum.setDisable(!isGraphImage&&!isGridImage);
        chkOuterNum.setDisable(!isGraphImage&&!isGridImage);
        
        Label lblGraphGrid=new Label("Print Mode: ");
        graphGridGP.add(lblGraphGrid, 0, 0, 3, 1);
        final ToggleGroup graphGridTG = new ToggleGroup();
        final RadioButton defaultRB = new RadioButton("Default");
        defaultRB.setToggleGroup(graphGridTG);
        defaultRB.setSelected(!isGraphImage&&!isGridImage);
        
        graphGridGP.add(defaultRB, 0, 1, 3, 1);
        final RadioButton graphRB = new RadioButton("Graph");
        graphRB.setToggleGroup(graphGridTG);
        graphRB.setSelected(isGraphImage);
        
        graphGridGP.add(graphRB, 0, 2, 3, 1);
        final RadioButton gridRB = new RadioButton("Grid");
        gridRB.setToggleGroup(graphGridTG);
        gridRB.setSelected(isGridImage);
        
        graphGridGP.add(gridRB, 0, 3, 3, 1);
        
        defaultRB.setDisable(true);
        graphRB.setDisable(true);
        gridRB.setDisable(true);
        
        defaultRB.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                if(t1){
                    isGraphImage=false;
                    isGridImage=false;
                    if(totalPages==1)
                        chkFitToPage.setDisable(false);
                    chkInnerNum.setDisable(true);
                    chkOuterNum.setDisable(true);
                }
                setTotalPrintImageWidthHeight(imageToPrint);
                updatePreviewLook();
            }
        });
        graphRB.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                if(t1){
                    txtG0.setDisable(false);
                    txtG1.setDisable(false);
                    txtBoxSize.setDisable(false);
                    //chkSinglePageGraph.setDisable(false);
                    chkFitToPage.setDisable(true);
                    chkInnerNum.setDisable(false);
                    chkOuterNum.setDisable(false);
                }
                else{
                    txtG0.setDisable(true);
                    txtG1.setDisable(true);
                    txtBoxSize.setDisable(true);
                    //chkSinglePageGraph.setDisable(true);
                    chkInnerNum.setDisable(true);
                    chkOuterNum.setDisable(true);
                }
                isGraphImage=t1;
                setTotalPrintImageWidthHeight(imageToPrint);
                updatePreviewLook();
            }
        });
        gridRB.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                if(t1){
                    txtGridSize.setDisable(false);
                    //chkSinglePageGraph.setDisable(false);
                    chkFitToPage.setDisable(true);
                    chkInnerNum.setDisable(false);
                    chkOuterNum.setDisable(false);
                }
                else{
                    txtGridSize.setDisable(true);
                    //chkSinglePageGraph.setDisable(true);
                    chkInnerNum.setDisable(true);
                    chkOuterNum.setDisable(true);
                }
                isGridImage=t1;
                setTotalPrintImageWidthHeight(imageToPrint);
                updatePreviewLook();
            }
        });
        
        Label lblGraphSize=new Label("Graph Size: ");
        graphGridGP.add(lblGraphSize, 0, 4, 3, 1);
        
        graphGridGP.add(txtG0, 0, 5, 1, 1);
        Label lblGraphX=new Label("X");
        graphGridGP.add(lblGraphX, 1, 5, 1, 1);
        graphGridGP.add(txtG1, 2, 5, 1, 1);
        
        Label lblBoxSize=new Label("Box Size: ");
        graphGridGP.add(lblBoxSize, 0, 6, 3, 1);
        
        graphGridGP.add(txtBoxSize, 0, 7, 1, 1);
        Label lblBoxX=new Label("X");
        graphGridGP.add(lblBoxX, 1, 7, 1, 1);
        graphGridGP.add(txtAutoBoxSize, 2, 7, 1, 1);
        
        Label lblGridSize=new Label("Grid Size: ");
        graphGridGP.add(lblGridSize, 0, 8, 1, 1);
        graphGridGP.add(txtGridSize, 1, 8, 2, 1);
        
        //graphGridGP.add(chkFitToPage, 0, 9, 3, 1);
        
        //----------------- CONTENT TAB --------------------//
        
        GridPane optionsGP=new GridPane();
        optionsGP.setPrefWidth(prefWidth);
        optionsGP.setVgap(5);
        optionsGP.setPadding(new Insets(5, 5, 5, 5));
        
        Label lblOptions=new Label("Enable Options: ");
        optionsGP.add(lblOptions, 0, 0);
        
        final CheckBox chkPage=new CheckBox("Page Number");
        chkPage.setDisable(false);
        optionsGP.add(chkPage, 0, 1);
        
        final CheckBox chkName=new CheckBox("Name");
        optionsGP.add(chkName, 0, 2);
        
        final CheckBox chkOwner=new CheckBox("Created By");
        optionsGP.add(chkOwner, 0, 3);
        
        final CheckBox chkDate=new CheckBox("Date");
        optionsGP.add(chkDate, 0, 4);
        
        final CheckBox chkOrg=new CheckBox("Organization");
        optionsGP.add(chkOrg, 0, 5);
        
        optionsGP.add(chkOuterNum, 0, 6);
        optionsGP.add(chkInnerNum, 0, 7);
        
        chkPage.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                isPage=t1;
            }
        });
        chkName.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                isName=t1;
            }
        });
        chkOwner.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                isOwner=t1;
            }
        });
        chkDate.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                isDate=t1;
            }
        });
        chkOrg.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                isOrg=t1;
            }
        });
        
        chkInnerNum.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                if(!chkInnerNum.isDisabled()){
                    isMinorNumbering=t1;
                    if(t1){
                        chkOuterNum.setDisable(false);
                        chkOuterNum.setSelected(true);
                    }
                }
                else
                    isMinorNumbering=false;
            }
        });
        
        chkOuterNum.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                if(!chkOuterNum.isDisabled()){
                    isNumbering=t1;
                    if(!t1)
                        isMinorNumbering=false;
                }
                else{
                    isNumbering=false;
                    isMinorNumbering=false;
                }
                setTotalPrintImageWidthHeight(imageToPrint);
                updatePreviewLook();
            }
        });
        
        //----------------- PREVIEW TAB --------------------//
        // This tab will be having Preview image and move back forth options
        BorderPane previewBP=new BorderPane();
        previewBP.setPrefSize(350, 350);
        
        previewIV=new ImageView();
        
        previewIV.setImage(SwingFXUtils.toFXImage(getPreviewImageByPage(currentPreviewPage), null));
        previewIV.setFitWidth(previewSmallerSide);
        previewIV.setFitHeight(previewLongerSide);
        
        previewBP.setCenter(previewIV);
        
        HBox previewHB=new HBox();
        previewHB.setSpacing(5);
        Button btnPrev=new Button("<");
        lblPreviewPage=new Label(strPreviewPage);
        Button btnNext=new Button(">");
        
        btnPrev.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if((currentPreviewPage-1)>0){
                    previewIV.setImage(SwingFXUtils.toFXImage(getPreviewImageByPage(--currentPreviewPage), null));
                    lblPreviewPage.setText(getStrPreviewPage());
                }
            }
        });
        btnNext.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if((currentPreviewPage+1)<=totalPreviewPages){
                    previewIV.setImage(SwingFXUtils.toFXImage(getPreviewImageByPage(++currentPreviewPage), null));
                    lblPreviewPage.setText(getStrPreviewPage());
                }
            }
        });
        
        previewHB.getChildren().add(btnPrev);
        previewHB.getChildren().add(lblPreviewPage);
        previewHB.getChildren().add(btnNext);
        
        previewBP.setBottom(previewHB);
        previewHB.setAlignment(Pos.CENTER);
        
        HBox printCancelHB=new HBox();
        printCancelHB.setAlignment(Pos.CENTER);
        
        VBox printVB = new VBox();
        //printVB.setAlignment(Pos.CENTER);
        printVB.setCursor(Cursor.HAND);
        Label printLbl= new Label("Print");
        printVB.getChildren().addAll(new ImageView("/media/icon/blue/hd/print.png"), printLbl);
        printVB.setPrefWidth(80);
        
        final Stage dialogStage = new Stage();
        printVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                
                setPageAndArea();
                //generateActualPage(0);
                populateLstPageToPrint();
                
                if(isSinglePage){
                    lstPageIndexToPrint.clear();
                    lstPageIndexToPrint.add("1"); // single page
                }
                setPageSize(); // this will set pageWidth and pageHeight
                
                PrintService ps = pss[printerIndex];
                
                if(!isPortrait)
                    pras.add(javax.print.attribute.standard.OrientationRequested.LANDSCAPE);
                pras.add(new MediaPrintableArea((float)leftMarginInches, (float)topMarginInches, (float)(A4_WIDTH_INCHES-(leftMarginInches+rightMarginInches)), (float)(A4_HEIGHT_INCHES-(topMarginInches+bottomMarginInches)), MediaPrintableArea.INCH));
                pras.add(MediaSizeName.ISO_A4);
                
                System.out.println("Printing to " + ps);
                
                //ArrayList<BufferedImage> collection=new ArrayList<>();
                //for(String s: lstPageIndexToPrint){
                //    collection.add(fullPage[Integer.parseInt(s)-1]);
                //}
                MultiplePagePrint mp=new MultiplePagePrint();
                mp.print(instance, lstPageIndexToPrint, ps.getName(), pras); // collection
                dialogStage.close();
            }
        });
        
        VBox cancelVB = new VBox();
        cancelVB.setCursor(Cursor.HAND);
        Label cancelLbl= new Label("Cancel");
        cancelVB.getChildren().addAll(new ImageView("/media/icon/blue/hd/close.png"), cancelLbl);
        cancelVB.setPrefWidth(80);
        
        printCancelHB.getChildren().addAll(printVB, cancelVB);
        
        HBox parentHB=new HBox();
        ScrollPane allSP=new ScrollPane();
        allSP.setPrefWidth(prefWidth);
        allSP.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        Accordion optionsPane = new Accordion();
        optionsPane.setPrefWidth(prefWidth);
        //TitledPane printerTP=new TitledPane();
        TitledPane paperTP=new TitledPane();
        TitledPane pageTP=new TitledPane();
        TitledPane graphTP=new TitledPane();
        TitledPane contentTP=new TitledPane();
        //printerTP.setText("Printer");
        paperTP.setText("Layout");
        pageTP.setText("Pages");
        graphTP.setText("Graph");
        contentTP.setText("Options");
        
        VBox leftPaneVB=new VBox();
        leftPaneVB.getChildren().addAll(printCancelHB, printerGP, optionsPane);
        
        optionsPane.getPanes().addAll(paperTP, pageTP, graphTP, contentTP);
        optionsPane.setExpandedPane(paperTP);
        
        //printerTP.setContent(printerGP);
        paperTP.setContent(paperGP);
        pageTP.setContent(pagesGP);
        graphTP.setContent(graphGridGP);
        contentTP.setContent(optionsGP);
        
        allSP.setContent(leftPaneVB);
        
        parentHB.getChildren().addAll(allSP, previewBP);
        
        
        
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        
        cancelVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                dialogStage.close();
                System.gc();
            }
        });
        Scene scene = new Scene(parentHB);
        dialogStage.setScene(scene);
        dialogStage.setTitle("Print");
        dialogStage.showAndWait();
    }
    
    private void setTotalPrintImageWidthHeight(BufferedImage imageToPrint){
        int intHeight=(int)imageToPrint.getHeight();
        int intLength=(int)imageToPrint.getWidth();
        if(isGraphImage){
            fullPrintableImageWidth=(int)((double)intLength*(double)G1*(double)boxSize/(double)defaultG1);
            fullPrintableImageHeight=(int)((double)intHeight*(double)G0*(double)autoBoxSize/(double)defaultG0);
        }
        else if(isGridImage){
            fullPrintableImageWidth=(int)(intLength*gridSize);
            fullPrintableImageHeight=(int)(intHeight*gridSize);
        }
        else{
            fullPrintableImageWidth=(int)(intLength*DPI/DPI_JAVA);
            fullPrintableImageHeight=(int)(intHeight*DPI/DPI_JAVA);
        }
    }
    
    private BufferedImage getDefaultImageWithDPI(BufferedImage imageToPrint){
        if(!isGraphImage&&!isGridImage){
            // default option
            // since code is updated to adapt 300 DPI (earlier was 72 DPI_JAVA)
            // we need to scale this image by a factor of DPI/DPI_JAVA
            // to get it final image in actual size on paper
            defaultImageWithDPI=new BufferedImage((int)((double)imageToPrint.getWidth()*(double)DPI/(double)DPI_JAVA), (int)((double)imageToPrint.getHeight()*(double)DPI/(double)DPI_JAVA), BufferedImage.TYPE_INT_RGB);
            Graphics2D g=defaultImageWithDPI.createGraphics();
            g.drawImage(imageToPrint, 0, 0, defaultImageWithDPI.getWidth(), defaultImageWithDPI.getHeight(), null);
            g.dispose();
        }
        return defaultImageWithDPI;
    }
    
    /**
     * Assumes totalPages, numPageHor, numPageVer set.
     * @param pageNumber 1..totalPages
     * @return 
     */
    private BufferedImage getImageByPage(int pageNumber){
        // assuming pages 1..totalPages
        BufferedImage imageForPage=new BufferedImage((int)actualWidth, (int)actualHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g=imageForPage.createGraphics();
        // whiten whole image
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, imageForPage.getWidth(), imageForPage.getHeight());
        
        // if user has selected Single Page
        if(isSinglePage){
            // valid pageNumber is 1 only
            
            if(isGraphImage){
                if(imageToPrint.getWidth()<=(int)(actualWidth/4.0) && imageToPrint.getHeight()<=(int)(actualHeight/4.0)){
                    double imgW_H=(double)imageToPrint.getWidth()/(double)imageToPrint.getHeight(); // aspect ratio
                    if(imageToPrint.getWidth()>=(int)((double)imageToPrint.getHeight()*actualWidth/actualHeight)){
                        // W: actualWidth H: actualWidth/imgW_H
                        double boxSize=actualWidth/(double)imageToPrint.getWidth();
                        BufferedImage graphImage=new BufferedImage((int)actualWidth, (int)(actualWidth/imgW_H),BufferedImage.TYPE_INT_RGB);
                        Graphics2D g1 = graphImage.createGraphics();
                        g1.drawImage(imageToPrint, 0, 0, (int)(actualWidth), (int)(actualWidth/imgW_H), null);
                        g1.setColor(java.awt.Color.BLACK);

                        BasicStroke bs = new BasicStroke(2);
                        g1.setStroke(bs);
                        g1.drawLine(0, (int)(actualWidth/imgW_H), (int)actualWidth, (int)(actualWidth/imgW_H));
                        g1.drawLine((int)actualWidth, 0,  (int)actualWidth, (int)(actualWidth/imgW_H));
                        //For vertical lines
                        for(int j = 0; j < imageToPrint.getWidth(); j++) {
                            if((j%(int)boxSize)==0){
                                bs = new BasicStroke(2);
                                g1.setStroke(bs);
                            }else{
                                bs = new BasicStroke(1);
                                g1.setStroke(bs);
                            }
                            g1.drawLine((int)((double)j*boxSize), 0,  (int)((double)j*boxSize), (int)(actualWidth/imgW_H));
                        }
                        //For horizontal lines
                        for(int i = 0; i < imageToPrint.getHeight(); i++) {
                            if((i%(int)boxSize)==0){
                                bs = new BasicStroke(2);
                                g1.setStroke(bs);
                            }else{
                                bs = new BasicStroke(1);
                                g1.setStroke(bs);
                            }
                            g1.drawLine(0, (int)((double)i*boxSize), (int)(actualWidth), (int)((double)i*boxSize));
                        }
                        //
                        for(int x=0; x<graphImage.getWidth(); x++)
                            for(int y=0;y<graphImage.getHeight(); y++)
                                imageForPage.setRGB(x, y, graphImage.getRGB(x, y));
                    }
                    else{
                        // W: actualHeight*imgW_H H: actualHeight
                        double boxSize=actualHeight/(double)imageToPrint.getHeight();
                        BufferedImage graphImage=new BufferedImage((int)(actualHeight*imgW_H), (int)actualHeight, BufferedImage.TYPE_INT_RGB);
                        Graphics2D g1 = graphImage.createGraphics();
                        g1.drawImage(imageToPrint, 0, 0, (int)(actualHeight*imgW_H), (int)(actualHeight), null);
                        g1.setColor(java.awt.Color.BLACK);

                        BasicStroke bs = new BasicStroke(2);
                        g1.setStroke(bs);
                        g1.drawLine(0, (int)(actualHeight), (int)(actualHeight*imgW_H), (int)(actualHeight));
                        g1.drawLine((int)(actualHeight*imgW_H), 0,  (int)(actualHeight*imgW_H), (int)(actualHeight));
                        //For vertical lines
                        for(int j = 0; j < imageToPrint.getWidth(); j++) {
                            if((j%(int)boxSize)==0){
                                bs = new BasicStroke(2);
                                g1.setStroke(bs);
                            }else{
                                bs = new BasicStroke(1);
                                g1.setStroke(bs);
                            }
                            g1.drawLine((int)((double)j*boxSize), 0,  (int)((double)j*boxSize), (int)(actualHeight));
                        }
                        //For horizontal lines
                        for(int i = 0; i < imageToPrint.getHeight(); i++) {
                            if((i%(int)boxSize)==0){
                                bs = new BasicStroke(2);
                                g1.setStroke(bs);
                            }else{
                                bs = new BasicStroke(1);
                                g1.setStroke(bs);
                            }
                            g1.drawLine(0, (int)((double)i*boxSize), (int)(actualHeight*imgW_H), (int)((double)i*boxSize));
                        }
                        //
                        for(int x=0; x<graphImage.getWidth(); x++)
                            for(int y=0;y<graphImage.getHeight(); y++)
                                imageForPage.setRGB(x, y, graphImage.getRGB(x, y));
                    }
                }
                else
                    chkSinglePageGraph.setSelected(false);
                    //System.out.println("Too Big Image for Single Page.");
            }
            else if(isGridImage){
                if(imageToPrint.getWidth()<=(int)(actualWidth/4.0) && imageToPrint.getHeight()<=(int)(actualHeight/4.0)){
                    double imgW_H=(double)imageToPrint.getWidth()/(double)imageToPrint.getHeight(); // aspect ratio
                    if(imageToPrint.getWidth()>=(int)((double)imageToPrint.getHeight()*actualWidth/actualHeight)){
                        // W: actualWidth H: actualWidth/imgW_H
                        double boxSize=actualWidth/(double)imageToPrint.getWidth();
                        BufferedImage gridImage=new BufferedImage((int)actualWidth, (int)(actualWidth/imgW_H),BufferedImage.TYPE_INT_RGB);
                        Graphics2D g1 = gridImage.createGraphics();
                        g1.drawImage(imageToPrint, 0, 0, (int)(actualWidth), (int)(actualWidth/imgW_H), null);
                        g1.setColor(java.awt.Color.BLACK);

                        BasicStroke bs = new BasicStroke(2);
                        g1.setStroke(bs);
                        g1.drawLine(0, (int)(actualWidth/imgW_H), (int)actualWidth, (int)(actualWidth/imgW_H));
                        g1.drawLine((int)actualWidth, 0,  (int)actualWidth, (int)(actualWidth/imgW_H));
                        //For vertical lines
                        for(int j = 0; j < imageToPrint.getWidth(); j++) {
                            g1.drawLine((int)((double)j*boxSize), 0,  (int)((double)j*boxSize), (int)(actualWidth/imgW_H));
                        }
                        //For horizontal lines
                        for(int i = 0; i < imageToPrint.getHeight(); i++) {
                            g1.drawLine(0, (int)((double)i*boxSize), (int)(actualWidth), (int)((double)i*boxSize));
                        }
                        //
                        for(int x=0; x<gridImage.getWidth(); x++)
                            for(int y=0;y<gridImage.getHeight(); y++)
                                imageForPage.setRGB(x, y, gridImage.getRGB(x, y));
                    }
                    else{
                        // W: actualHeight*imgW_H H: actualHeight
                        double boxSize=actualHeight/(double)imageToPrint.getHeight();
                        BufferedImage gridImage=new BufferedImage((int)(actualHeight*imgW_H), (int)actualHeight, BufferedImage.TYPE_INT_RGB);
                        Graphics2D g1 = gridImage.createGraphics();
                        g1.drawImage(imageToPrint, 0, 0, (int)(actualHeight*imgW_H), (int)(actualHeight), null);
                        g1.setColor(java.awt.Color.BLACK);

                        BasicStroke bs = new BasicStroke(2);
                        g1.setStroke(bs);
                        g1.drawLine(0, (int)(actualHeight), (int)(actualHeight*imgW_H), (int)(actualHeight));
                        g1.drawLine((int)(actualHeight*imgW_H), 0,  (int)(actualHeight*imgW_H), (int)(actualHeight));
                        //For vertical lines
                        for(int j = 0; j < imageToPrint.getWidth(); j++) {
                            g1.drawLine((int)((double)j*boxSize), 0,  (int)((double)j*boxSize), (int)(actualHeight));
                        }
                        //For horizontal lines
                        for(int i = 0; i < imageToPrint.getHeight(); i++) {
                            g1.drawLine(0, (int)((double)i*boxSize), (int)(actualHeight*imgW_H), (int)((double)i*boxSize));
                        }
                        //
                        for(int x=0; x<gridImage.getWidth(); x++)
                            for(int y=0;y<gridImage.getHeight(); y++)
                                imageForPage.setRGB(x, y, gridImage.getRGB(x, y));
                    }
                }
                else
                    chkSinglePageGraph.setSelected(false);
                    //System.out.println("Too Big Image for Single Page.");
            }
            else{ // default image
                double imgW_H=(double)fullPrintableImageWidth/(double)fullPrintableImageHeight; // aspect ratio
                if(fullPrintableImageWidth<=(int)actualWidth&&fullPrintableImageHeight<=(int)actualHeight)
                    g.drawImage(defaultImageWithDPI, 0, 0, fullPrintableImageWidth, fullPrintableImageHeight, null);
                else if(fullPrintableImageWidth>(int)actualWidth&&fullPrintableImageHeight<=(int)actualHeight)
                    g.drawImage(defaultImageWithDPI, 0, 0, (int)actualWidth, (int)(actualWidth/imgW_H), null);
                else if(fullPrintableImageWidth<=(int)actualWidth&&fullPrintableImageHeight>(int)actualHeight)
                    g.drawImage(defaultImageWithDPI, 0, 0, (int)(actualHeight*imgW_H), (int)actualHeight, null);
                else{ // both imgW>pageW && imgH>pageH
                    if(fullPrintableImageWidth>fullPrintableImageHeight){
                        if((int)(actualWidth/imgW_H)<=(int)actualHeight)
                            g.drawImage(defaultImageWithDPI, 0, 0, (int)actualWidth, (int)(actualWidth/imgW_H), null);
                        else // this case is added to resolve issue when a part of singlePageImage was left beyond actualHeight due to different page W/H ratio
                            g.drawImage(defaultImageWithDPI, 0, 0, (int)(actualHeight*imgW_H), (int) actualHeight, null);
                    } else{
                        if((int)(actualHeight*imgW_H)<=(int)actualWidth)
                            g.drawImage(defaultImageWithDPI, 0, 0, (int)(actualHeight*imgW_H), (int) actualHeight, null);
                        else // this case is added to resolve issue when a part of singlePageImage was left beyond actualWidth due to different page W/H ratio
                            g.drawImage(defaultImageWithDPI, 0, 0, (int)actualWidth, (int)(actualWidth/imgW_H), null);
                    }
                }
            }
            return imageForPage;
        }
        
        if(isGraphImage){
            // get pixels in imageToPrint that fit in one page
            int numFitWarps=(int)(actualWidth/((double)G1*(double)boxSize/(double)defaultG1));
            int numFitWefts=(int)(actualHeight/((double)G0*(double)autoBoxSize/(double)defaultG0));
            numFitWarp=numFitWarps;
            numFitWeft=numFitWefts;
            int startWarp=((pageNumber-1)%numPageHor)*numFitWarps;
            int startWeft=((pageNumber-1)/numPageHor)*numFitWefts;
            if(pageNumber%numPageHor==0) // last page(s) horizontally
                numFitWarps=(this.imageToPrint.getWidth()%numFitWarps)!=0?(this.imageToPrint.getWidth()%numFitWarps):numFitWarps;
            if((pageNumber-1)/numPageHor==numPageVer-1) // last page(s) vertically
                numFitWefts=(this.imageToPrint.getHeight()%numFitWefts)!=0?(this.imageToPrint.getHeight()%numFitWefts):numFitWefts;
            // add selected pixels to portion image
            portionImage=new BufferedImage(numFitWarps, numFitWefts, BufferedImage.TYPE_INT_RGB);
            for(int x=startWarp; x<(startWarp+numFitWarps); x++)
                for(int y=startWeft; y<(startWeft+numFitWefts); y++)
                    portionImage.setRGB(x-startWarp, y-startWeft, this.imageToPrint.getRGB(x, y));
            BufferedImage tempBI=generateGraphImage(portionImage);
            for(int x=0; x<tempBI.getWidth(); x++)
                for(int y=0; y<tempBI.getHeight(); y++)
                    imageForPage.setRGB(x, y, tempBI.getRGB(x, y));
            tempBI=null;
            System.gc();
        }
        else if(isGridImage){
            // get pixels in imageToPrint that fit in one page
            int numFitWarps=(int)(actualWidth/(double)gridSize);
            int numFitWefts=(int)(actualHeight/(double)gridSize);
            numFitWarp=numFitWarps;
            numFitWeft=numFitWefts;
            int startWarp=((pageNumber-1)%numPageHor)*numFitWarps;
            int startWeft=((pageNumber-1)/numPageHor)*numFitWefts;
            if(pageNumber%numPageHor==0) // last page(s) horizontally
                numFitWarps=(this.imageToPrint.getWidth()%numFitWarps)!=0?(this.imageToPrint.getWidth()%numFitWarps):numFitWarps;
            if((pageNumber-1)/numPageHor==numPageVer-1) // last page(s) vertically
                numFitWefts=(this.imageToPrint.getHeight()%numFitWefts)!=0?(this.imageToPrint.getHeight()%numFitWefts):numFitWefts;
            // add selected pixels to portion image
            portionImage=new BufferedImage(numFitWarps, numFitWefts, BufferedImage.TYPE_INT_RGB);
            for(int x=startWarp; x<(startWarp+numFitWarps); x++)
                for(int y=startWeft; y<(startWeft+numFitWefts); y++)
                    portionImage.setRGB(x-startWarp, y-startWeft, this.imageToPrint.getRGB(x, y));
            BufferedImage tempBI=generateGridImage(portionImage);
            for(int x=0; x<tempBI.getWidth(); x++)
                for(int y=0; y<tempBI.getHeight(); y++)
                    imageForPage.setRGB(x, y, tempBI.getRGB(x, y));
            tempBI=null;
            System.gc();
        }
        else{ // default
            // get start X, Y , W, H in fullImage (imageToPrint in actual size by fixing DPI)
            int portionX=(int)(((pageNumber-1)%numPageHor)*(int)actualWidth);
            int portionY=(int)(((pageNumber-1)/numPageHor)*(int)actualHeight);
            int portionWidth=(int)actualWidth;
            if(pageNumber%numPageHor==0) // last page(s) horizontally
                portionWidth=(defaultImageWithDPI.getWidth()%(int)actualWidth)!=0?(defaultImageWithDPI.getWidth()%(int)actualWidth):(int)actualWidth;
            int portionHeight=(int)actualHeight;
            if((pageNumber-1)/numPageHor==numPageVer-1) // last page(s) vertically
                portionHeight=(defaultImageWithDPI.getHeight()%(int)actualHeight)!=0?(defaultImageWithDPI.getHeight()%(int)actualHeight):(int)actualHeight;
            // fill corresponding pixels from fullImage into imageForPage
            for(int x=portionX; x<(portionX+portionWidth); x++)
                for(int y=portionY; y<(portionY+portionHeight); y++)
                    imageForPage.setRGB(x-portionX, y-portionY, defaultImageWithDPI.getRGB(x, y));
        }
        return imageForPage;
    }
    
    private BufferedImage generateGraphImage(BufferedImage portionImage){
        int intHeight=(int)portionImage.getHeight();
        int intLength=(int)portionImage.getWidth();
        BufferedImage graphImage=new BufferedImage((int)((double)intLength*(double)G1*(double)boxSize/(double)defaultG1), (int)((double)intHeight*(double)G0*(double)autoBoxSize/(double)defaultG0),BufferedImage.TYPE_INT_RGB);
        Graphics2D g = graphImage.createGraphics();
        g.drawImage(portionImage, 0, 0, (int)((double)intLength*(double)G1*(double)boxSize/(double)defaultG1), (int)((double)intHeight*(double)G0*(double)autoBoxSize/(double)defaultG0), null);
        g.setColor(java.awt.Color.BLACK);
            
        BasicStroke bs = new BasicStroke(3);
        g.setStroke(bs);
        g.drawLine(0, (int)((double)intHeight*(double)G0*(double)autoBoxSize/(double)defaultG0), (int)((double)intLength*(double)G1*(double)boxSize/(double)defaultG1), (int)((double)intHeight*(double)G0*(double)autoBoxSize/(double)defaultG0));
        g.drawLine((int)((double)intLength*(double)G1*(double)boxSize/(double)defaultG1), 0,  (int)((double)intLength*(double)G1*(double)boxSize/(double)defaultG1), (int)((double)intHeight*(double)G0*(double)autoBoxSize/(double)defaultG0));
        //For vertical lines
        for(int j = 0; j < intLength; j++) {
            if((j%G0)==0){
                bs = new BasicStroke(3);
                g.setStroke(bs);
            }else{
                bs = new BasicStroke(1);
                g.setStroke(bs);
            }
            g.drawLine((int)((double)j*(double)G1*(double)boxSize/(double)defaultG1), 0,  (int)((double)j*(double)G1*(double)boxSize/(double)defaultG1), (int)((double)intHeight*(double)G0*(double)autoBoxSize/(double)defaultG0));
        }
        //For horizontal lines
        for(int i = 0; i < intHeight; i++) {
            if((i%G1)==0){
                bs = new BasicStroke(3);
                g.setStroke(bs);
            }else{
                bs = new BasicStroke(1);
                g.setStroke(bs);
            }
            g.drawLine(0, (int)((double)i*(double)G0*(double)autoBoxSize/(double)defaultG0), (int)((double)intLength*(double)G1*(double)boxSize/(double)defaultG1), (int)((double)i*(double)G0*(double)autoBoxSize/(double)defaultG0));
        }
        return graphImage;
    }
    
    private BufferedImage generateGridImage(BufferedImage portionImage){
        int intHeight=(int)portionImage.getHeight();
        int intLength=(int)portionImage.getWidth();
        BufferedImage gridImage=new BufferedImage((int)(intLength*gridSize), (int)(intHeight*gridSize),BufferedImage.TYPE_INT_RGB);
        Graphics2D g = gridImage.createGraphics();
        g.drawImage(portionImage, 0, 0, (int)(intLength*gridSize), (int)(intHeight*gridSize), null);
        g.setColor(java.awt.Color.BLACK);
        BasicStroke bs = new BasicStroke(2);
        g.setStroke(bs);
        g.drawLine(0, (int)(intHeight*gridSize), (int)(intLength*gridSize), (int)(intHeight*gridSize));
        g.drawLine((int)(intLength*gridSize), 0,  (int)(intLength*gridSize), (int)(intHeight*gridSize));
        //For vertical lines
        for(int j = 0; j < intLength; j++) {
            g.drawLine((int)(j*gridSize), 0,  (int)(j*gridSize), (int)(intHeight*gridSize));
        }
        //For horizontal lines
        for(int i = 0; i < intHeight; i++) {
            g.drawLine(0, (int)(i*gridSize), (int)(intLength*gridSize), (int)(i*gridSize));
        }
        return gridImage;
    }
    
    public BufferedImage generateActualPage(int pageNumber){
        fullPage=new BufferedImage((int)imageableWidth, (int)imageableHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g=fullPage.createGraphics();
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, fullPage.getWidth(), fullPage.getHeight());
        BufferedImage pageImg = getImageByPage(pageNumber);// optimization: reduces multiple calls to .getWidth() .getHeight()
        // draw header
        if(!headerOff)
            g.drawImage(getHeaderImage(pageNumber), 0, 0, fullPage.getWidth(), headerPixel, null);
        if(isNumbering){
            g.drawImage(pageImg, numberingPixel, headerPixel+numberingPixel, pageImg.getWidth(), pageImg.getHeight(), null);
            g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                         RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g.setFont(new Font("Arial", Font.PLAIN, fontSize));
            g.setColor(Color.BLACK);
            String tempNum;
            if(isGraphImage){
                // draw horizontal numbering
                for(int x=(((pageNumber-1)%numPageHor)*(int)actualWidth); x<getMinInt(((((pageNumber-1)%numPageHor)+1)*(int)actualWidth), fullPrintableImageWidth); x++){
                    if(x%((int)((double)(G0*G1)*(double)boxSize/(double)defaultG1))==0){
                        g.drawString("|", numberingPixel+(x%(int)actualWidth)-4, headerPixel+numberingPixel-1);
                        tempNum=String.valueOf((int)(((double)defaultG1/(double)boxSize)*(double)x/(double)G1));
                        g.drawString(tempNum, numberingPixel+(x%(int)actualWidth)-(g.getFontMetrics().stringWidth(tempNum)/2), headerPixel+numberingPixel-20);
                    }
                    else if(isMinorNumbering)
                        if(x%((int)(((double)(G0*G1)*(double)boxSize/(double)defaultG1)/2))==0){
                            g.drawString("|", numberingPixel+(x%(int)actualWidth)-4, headerPixel+numberingPixel-1);
                            tempNum=String.valueOf((int)(((double)defaultG1/(double)boxSize)*(double)x/(double)G1));
                            g.drawString(tempNum, numberingPixel+(x%(int)actualWidth)-(g.getFontMetrics().stringWidth(tempNum)/2), headerPixel+numberingPixel-20);
                        }
                }
                // draw vertical numbering
                for(int y=(((pageNumber-1)/numPageHor)*(int)actualHeight); y<getMinInt(((((pageNumber-1)/numPageHor)+1)*(int)actualHeight), fullPrintableImageHeight); y++){
                    if(y%((int)((double)(G0*G1)*(double)autoBoxSize/(double)defaultG0))==0){
                        g.drawString("_", numberingPixel-g.getFontMetrics().stringWidth("_"), headerPixel+numberingPixel+(y%(int)actualHeight)-4);
                        tempNum=String.valueOf((int)(((double)defaultG0/(double)autoBoxSize)*(double)y/(double)G0));
                        g.drawString(tempNum, 0, headerPixel+numberingPixel+(y%(int)actualHeight)+(g.getFontMetrics().getHeight()/2));
                    }
                    else if(isMinorNumbering)
                        if(y%((int)(((double)(G0*G1)*(double)autoBoxSize/(double)defaultG0))/2)==0){
                            g.drawString("_", numberingPixel-g.getFontMetrics().stringWidth("_"), headerPixel+numberingPixel+(y%(int)actualHeight)-4);
                            tempNum=String.valueOf((int)(((double)defaultG0/(double)autoBoxSize)*(double)y/(double)G0));
                            g.drawString(tempNum, 0, headerPixel+numberingPixel+(y%(int)actualHeight)+(g.getFontMetrics().getHeight()/2));
                        }
                }
            }
            else if(isGridImage){
                // draw horizontal numbering
                for(int x=(((pageNumber-1)%numPageHor)*(int)actualWidth); x<getMinInt(((((pageNumber-1)%numPageHor)+1)*(int)actualWidth), fullPrintableImageWidth); x++){
                    if(x%(gridSize*GRID_NUM)==0){
                        g.drawString("|", numberingPixel+(x%(int)actualWidth)-4, headerPixel+numberingPixel-1);
                        tempNum=String.valueOf(x/gridSize);
                        g.drawString(tempNum, numberingPixel+(x%(int)actualWidth)-(g.getFontMetrics().stringWidth(tempNum)/2), headerPixel+numberingPixel-20);
                    }
                    else if(isMinorNumbering)
                        if(x%((gridSize*GRID_NUM)/2)==0){
                            g.drawString("|", numberingPixel+(x%(int)actualWidth)-4, headerPixel+numberingPixel-1);
                            tempNum=String.valueOf(x/gridSize);
                            g.drawString(tempNum, numberingPixel+(x%(int)actualWidth)-(g.getFontMetrics().stringWidth(tempNum)/2), headerPixel+numberingPixel-20);
                        }
                }
                // draw vertical numbering
                for(int y=(((pageNumber-1)/numPageHor)*(int)actualHeight); y<getMinInt(((((pageNumber-1)/numPageHor)+1)*(int)actualHeight), fullPrintableImageHeight); y++){
                    if(y%(gridSize*GRID_NUM)==0){
                        g.drawString("_", numberingPixel-g.getFontMetrics().stringWidth("_"), headerPixel+numberingPixel+(y%(int)actualHeight)-4);
                        tempNum=String.valueOf(y/gridSize);
                        g.drawString(tempNum, 0, headerPixel+numberingPixel+(y%(int)actualHeight)+(g.getFontMetrics().getHeight()/2));
                    }
                    else if(isMinorNumbering)
                        if(y%((gridSize*GRID_NUM)/2)==0){
                            g.drawString("_", numberingPixel-g.getFontMetrics().stringWidth("_"), headerPixel+numberingPixel+(y%(int)actualHeight)-4);
                            tempNum=String.valueOf(y/gridSize);
                            g.drawString(tempNum, 0, headerPixel+numberingPixel+(y%(int)actualHeight)+(g.getFontMetrics().getHeight()/2));
                        }
                }
            }
        }
        else{
            if(headerOff)
                g.drawImage(pageImg, 0, 0, pageImg.getWidth(), pageImg.getHeight(), null);
            else
                g.drawImage(pageImg, 0, headerPixel, pageImg.getWidth(), pageImg.getHeight(), null);
        }
        return fullPage;
    }
    
    /**
     * Draws imagePage[page-1] to space where it should actually appear
     * Also draws Page margins with RED color
     * Considers:
     *  Page Orientation (final area of image to be returns)
     *  Actual startX, startY, width and height
     * @see divideIntoPages()
     * @param page Page Number (1...MaxPage)
     * @return Full Page-sized image
     */
    public BufferedImage getPreviewImageByPage(int pageNumber){
        BufferedImage prvBI=new BufferedImage((int)pageWidth, (int)pageHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g=prvBI.createGraphics();
        // whiten whole image
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, prvBI.getWidth(), prvBI.getHeight());
        // draw actual image
        if(isSinglePage)
            g.drawImage(getImageByPage(1), (int)actualX, (int)actualY, (int)actualWidth-1, (int)actualHeight-1, null);
        else
            g.drawImage(getImageByPage(pageNumber), (int)actualX, (int)actualY, (int)actualWidth-1, (int)actualHeight-1, null);
        BasicStroke bs = new BasicStroke(2);
        g.setStroke(bs);
        // draw page margins
        g.setColor(Color.RED);
        g.drawRect((int)imageableX, (int)imageableY, (int)imageableWidth-1, (int)imageableHeight-1);
        // draw page boundary
        g.setColor(Color.ORANGE);
        //g.drawRect(0, 0, prvBI.getWidth()-1, prvBI.getHeight()-1);
        g.drawLine(1, 1, 1, prvBI.getHeight()-2);
        g.drawLine(1, 1, prvBI.getWidth()-2,1);
        g.drawLine(1, prvBI.getHeight()-2, prvBI.getWidth()-2, prvBI.getHeight()-2);
        g.drawLine(prvBI.getWidth()-2, 1, prvBI.getWidth()-2, prvBI.getHeight()-2);
        return prvBI;
    }
    
    private void setPageAndArea(){
        setPageSize();
        calculateActualPrintArea();
    }
    
    /**
     * Resets orientation of preview image and shows 1st page preview
     */
    private void updatePreviewLook(){
        if(isPortrait){
            previewIV.setFitWidth(previewSmallerSide);
            previewIV.setFitHeight(previewLongerSide);
        }
        else{
            previewIV.setFitWidth(previewLongerSide);
            previewIV.setFitHeight(previewSmallerSide);
        }
        setPageAndArea();
        calculateTotalPages();
        currentPreviewPage=1;
        previewIV.setImage(SwingFXUtils.toFXImage(getPreviewImageByPage(currentPreviewPage), null));
        lblPreviewPage.setText(getStrPreviewPage());
    }
    
    /**
     * @return Page String (e.g. Page 1 of 8)
     */
    private String getStrPreviewPage(){
        return "Page "+currentPreviewPage+" of "+totalPreviewPages;
    }
    
    /**
     * Calculates and sets horizontal, vertical, total pages by dividing fullPrintableImage area
     * by actual Width/Height calculated
     * Updates totalPreviewPages==totalPages
     * @see calculateActualPrintArea()
     */
    private void calculateTotalPages(){
        if(isSinglePage){
            totalPages=totalPreviewPages=1;
            return;
        }
        numPageHor=(int)Math.ceil((double)fullPrintableImageWidth/(double)actualWidth);
        numPageVer=(int)Math.ceil((double)fullPrintableImageHeight/(double)actualHeight);
        // total pages
        totalPages=numPageHor*numPageVer;
        totalPreviewPages=totalPages;
    }
    
    private int getMinInt(int i, int j){
        return i<j?i:j;
    }
    
    /**
     * @param page Actual page number to print (>=1)
     * @return header image containing logo, page etc. [Optional]
     */
    private BufferedImage getHeaderImage(int page){
        BufferedImage headerImage=new BufferedImage((int)imageableWidth, headerPixel, BufferedImage.TYPE_INT_RGB);
        Graphics2D g=headerImage.createGraphics();
        g.setColor(Color.WHITE);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                             RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g.setFont(new Font("Arial", Font.PLAIN, fontSize));
        g.fillRect(0, 0, headerImage.getWidth(), headerImage.getHeight());
        if(isLogo)
            g.drawImage(SwingFXUtils.fromFXImage(logo, null), (int)imageableWidth-241, 0, null);
        g.setColor(Color.BLACK);
        if(isLogo)
            g.drawString(CAPTION_DIC, (int)imageableWidth-g.getFontMetrics().stringWidth(CAPTION_DIC), 170);
        if(isPage)
            g.drawString(CAPTION_PAGE+(page)+"/"+(isSinglePage?1:totalPages), (int)imageableWidth/2-80, 80);
        if(isName)
            g.drawString(CAPTION_ITEM+itemName, 0, 40);
        if(isOwner)
            g.drawString(CAPTION_USER+itemOwner, 0, 84);
        if(isDate)
            g.drawString(CAPTION_DATE+new Date(), 0, 127);
        if(isOrg)
            g.drawString(CAPTION_ORG+itemOrg, 0, 170);
        return headerImage;
    }
    
    /**
     * Adds page number(s) to be sent for printing
     */
    private void populateLstPageToPrint(){
        lstPageIndexToPrint.clear();
        if(endPageNumber==0)
            endPageNumber=totalPages;
        for(int p=startPageNumber; p<=endPageNumber; p++)
            lstPageIndexToPrint.add(String.valueOf(p));
    }
    
    // ------------------ Printing Area Abstraction ---------------- //
    
    /**
     * Calculate actual printing space available for images considering
     * orientation and numbering toggle and will also set actualX, actualY,
     * actualWidth, actualHeight
     */
    private void calculateActualPrintArea(){
        calculateImageableArea();
        // orientation abstracted
        // NOTE: set isNumbering considering isGraphImage (this function assumes its graph/grid only)
        if(isNumbering){ // horizontal and vertical numbering enabled
            actualX=imageableX+numberingPixel; // numberingPixel pixels for vertical numbering space
            actualY=imageableY+headerPixel+numberingPixel; // headerPixel header (logo, page num etc.) and numberingPixel horizontal numbering
            actualWidth=imageableWidth-numberingPixel;
            actualHeight=imageableHeight-(headerPixel+numberingPixel);
        } // no numbering, only header
        else{
            actualX=imageableX;
            actualY=imageableY+headerPixel;
            actualWidth=imageableWidth;
            actualHeight=imageableHeight-headerPixel;
        }
        
        if(headerOff){
            actualY-=headerPixel; // decrease start Y by headerPixel (header length)
            actualHeight+=headerPixel; // increase total printable length by headerHeight
        }
    }
    
    /**
     * This function will calculate imageable area considering margins and orientation
     * It will set imageableX, imageableY, imageableWidth and imageableHeight values
     * These set values can then be assigned to paper object
     */
    private void calculateImageableArea(){
        setPageMargin();
        if(isPortrait){ // portrait
            imageableX=DPI_JAVA*leftMarginInches;
            imageableY=DPI_JAVA*topMarginInches;
            imageableWidth=pageSmallerSide-DPI_JAVA*(leftMarginInches+rightMarginInches);
            imageableHeight=pageLongerSide-DPI_JAVA*(topMarginInches+bottomMarginInches);
        }
        else{ // landscape
            imageableX=DPI_JAVA*leftMarginInches;
            imageableY=DPI_JAVA*topMarginInches;
            imageableWidth=pageLongerSide-DPI_JAVA*(leftMarginInches+rightMarginInches);
            imageableHeight=pageSmallerSide-DPI_JAVA*(topMarginInches+bottomMarginInches);
        }
    }
    
    /**
     * This will update leftMarginInches, rightMarginInches, topMarginInches, bottomMarginInches
     */
    private void setPageMargin(){
        // may also validate values here
        if(!(leftMarginInches>=minMargin&&leftMarginInches<=maxMargin))
            leftMarginInches=defaultLeftMargin;
        if(!(rightMarginInches>=minMargin&&rightMarginInches<=maxMargin))
            rightMarginInches=defaultRightMargin;
        if(!(topMarginInches>=minMargin&&topMarginInches<=maxMargin))
            topMarginInches=defaultTopMargin;
        if(!(bottomMarginInches>=minMargin&&bottomMarginInches<=maxMargin))
            bottomMarginInches=defaultBottomMargin;
    }
    
    /**
     * This will update pageWidth and pageHeight considering orientation
     * pageSmallerSide and pageLongerSide should be containing values
     * as per selected page size
     */
    private void setPageSize(){
        if(isPortrait){
            pageWidth=pageSmallerSide;
            pageHeight=pageLongerSide;
        }
        else{
            pageWidth=pageLongerSide;
            pageHeight=pageSmallerSide;
        }
    }
    
    // ------------- DUMP CODE --------------------//
    /**
     * Initializes imagePage[]
     * Divides fullImage into totalPages number of distinct images each of size actual width
     * and actual height, so it can be used directly at getPreviewImageByPage()
     */
    /*private void divideIntoPages(){
        calculateTotalPages();
        // divide imageToPrint into multiple buffered images
        imagePage=new BufferedImage[totalPages];
        for(int a=0; a<totalPages; a++){
            imagePage[a]=new BufferedImage((int)actualWidth, (int)actualHeight, BufferedImage.TYPE_INT_RGB);
            Graphics2D g=imagePage[a].createGraphics();
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, imagePage[a].getWidth(), imagePage[a].getHeight());
            
            if(isSinglePage){
                double imgW_H=(double)fullImage.getWidth()/(double)fullImage.getHeight(); // aspect ratio
                if(fullImage.getWidth()<=(int)actualWidth&&fullImage.getHeight()<=(int)actualHeight)
                    g.drawImage(fullImage, 0, 0, fullImage.getWidth(), fullImage.getHeight(), null);
                else if(fullImage.getWidth()>(int)actualWidth&&fullImage.getHeight()<=(int)actualHeight)
                    g.drawImage(fullImage, 0, 0, (int)actualWidth, (int)(actualWidth/imgW_H), null);
                else if(fullImage.getWidth()<=(int)actualWidth&&fullImage.getHeight()>(int)actualHeight)
                    g.drawImage(fullImage, 0, 0, (int)(actualHeight*imgW_H), (int)actualHeight, null);
                else{ // both imgW>pageW && imgH>pageH
                    if(fullImage.getWidth()>fullImage.getHeight())
                        g.drawImage(fullImage, 0, 0, (int)actualWidth, (int)(actualWidth/imgW_H), null);
                    else
                        g.drawImage(fullImage, 0, 0, (int)(actualHeight*imgW_H), (int) actualHeight, null);
                }
                return;
            }
            
            for(int y=0; y<(int)actualHeight; y++){
                for(int x=0; x<(int)actualWidth; x++){
                    if((x+((a%numPageHor)*actualWidth))<fullImage.getWidth()&&(y+((a/numPageHor)*actualHeight))<fullImage.getHeight())
                        imagePage[a].setRGB(x, y, fullImage.getRGB(x+((a%numPageHor)*(int)actualWidth), y+((a/numPageHor)*(int)actualHeight)));
                }
            }
        }
    }
    
    public BufferedImage getImageByPage(BufferedImage imageToPrint, int pageNumber){
        int intHeight=(int)imageToPrint.getHeight();
        int intLength=(int)imageToPrint.getWidth();
        BufferedImage portionImage=new BufferedImage((int)actualWidth, (int)actualHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g=portionImage.createGraphics();
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, portionImage.getWidth(), portionImage.getHeight());
        
        // graph image
        // W: (int)((double)intLength*(double)G1*(double)boxSize/(double)defaultG1)
        // H: (int)((double)intHeight*(double)G0*(double)autoBoxSize/(double)defaultG0)
        // portion in a page of w: actualWidth and h: actualHeight
        int completeCellsWidthwise=0;
        int completeCellsHeightwise=0;
        
        // To generate big image from imageToPrint
        if(actualWidth>(int)((double)intLength*(double)G1*(double)boxSize/(double)defaultG1)){
            completeCellsWidthwise=(int)Math.floor((double)actualWidth/((double)G1*(double)boxSize/(double)defaultG1));
        }
        else{
            completeCellsWidthwise=intLength;
        }
        if(actualHeight>(int)((double)intHeight*(double)G0*(double)autoBoxSize/(double)defaultG0)){
            completeCellsHeightwise=(int)Math.floor((double)actualHeight/((double)G0*(double)autoBoxSize/(double)defaultG0));
        }
        else{
            completeCellsHeightwise=intHeight;
        }
        
        numPageHor=(int)Math.ceil((double)intLength/(double)completeCellsWidthwise);
        numPageVer=(int)Math.ceil((double)intHeight/(double)completeCellsHeightwise);
        
        return portionImage;
    }
    
    */
}
