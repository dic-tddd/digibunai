/*
 * Copyright (C) Digital India Corporation (Media Lab Asia)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.mla.user;

import com.mla.dictionary.DictionaryAction;
import com.mla.utility.AboutView;
import com.mla.main.Configuration;
import com.mla.utility.ContactView;
import com.mla.utility.HelpView;
import com.mla.main.Logging;
import com.mla.main.MessageView;
import com.mla.utility.TechnicalView;
import com.mla.main.WindowView;
import com.sun.javafx.scene.control.skin.ComboBoxListViewSkin;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
//import javafx.scene.control.DatePicker;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import javafx.util.StringConverter;
/**
 *
 * @Designing GUI window for user preferences
 * @author Amit Kumar Singh
 * 
 */
public class UserProfileView extends Application {
    private static Stage userStage;
    private BorderPane root;
    
    Configuration objConfiguration = null;
    DictionaryAction objDictionaryAction = null;
    
    private Label lblStatus;
    private ProgressBar progressB;
    private ProgressIndicator progressI;
    
    private ComboBox<Label> countryCB= new ComboBox<Label>();
    private ComboBox<Label> stateCB= new ComboBox<Label>();
    private ComboBox<Label> cityCB= new ComboBox<Label>();
        
    List lstCountry;
    List lstState;
    List lstCity;
    
 // indices of selected city, state & country in cityCB, stateCB & countryCB respectively
    int intcountry=0;
    int intstate=0;
    int intcity=0;
	
    public UserProfileView(final Stage primaryStage) {}
    
    public UserProfileView(Configuration objConfigurationCall) {
        objConfiguration = objConfigurationCall;
        objDictionaryAction = new DictionaryAction(objConfiguration);

        userStage = new Stage();
        root = new BorderPane();
        Scene scene = new Scene(root, objConfiguration.WIDTH, objConfiguration.HEIGHT, Color.WHITE);
        scene.getStylesheets().add(UserProfileView.class.getResource(objConfiguration.getStrTemplate()+"/setting.css").toExternalForm());

        HBox footContainer = new HBox();
        progressB = new ProgressBar(0);
        progressB.setVisible(false);
        progressI = new ProgressIndicator(0);
        progressI.setVisible(false);
        lblStatus = new Label(objDictionaryAction.getWord("WELCOMETOCADTOOL"));
        lblStatus.setId("message");
        footContainer.getChildren().addAll(lblStatus,progressB,progressI);
        footContainer.setId("footContainer");
        root.setBottom(footContainer);
        
        MenuBar menuBar = new MenuBar();
        menuBar.prefWidthProperty().bind(userStage.widthProperty());
       
        Menu homeMenu  = new Menu();
        HBox homeMenuHB = new HBox();
        homeMenuHB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/home.png"),new Label(objDictionaryAction.getWord("HOME")));
        homeMenu.setGraphic(homeMenuHB);
        homeMenu.setAccelerator(new KeyCodeCombination(KeyCode.M, KeyCombination.SHIFT_DOWN));
        homeMenuHB.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                homeMenuAction();  
                me.consume();
            }
        }); 
        Menu parentMenu  = new Menu();
        HBox parentMenuHB = new HBox();
        parentMenuHB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/settings.png"),new Label(objDictionaryAction.getWord("PARENT")));
        parentMenu.setGraphic(parentMenuHB);
        parentMenu.setAccelerator(new KeyCodeCombination(KeyCode.Z, KeyCombination.SHIFT_DOWN));
        parentMenuHB.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                parentMenuAction();                 
                me.consume();
            }
        }); 
        //Help Menu items
        Label supportMenuLabel = new Label(objDictionaryAction.getWord("SUPPORT"));
        supportMenuLabel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUPPORT")));
        Menu supportMenu = new Menu();
        supportMenu.setGraphic(supportMenuLabel);
        
        MenuItem helpMenuItem = new MenuItem(objDictionaryAction.getWord("HELP"));
        helpMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        helpMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.H, KeyCombination.SHIFT_DOWN));
        helpMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {       
                helpMenuAction();
            }
        });        
        MenuItem technicalMenuItem = new MenuItem(objDictionaryAction.getWord("TECHNICAL"));
        technicalMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/technical_info.png"));
        technicalMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.T, KeyCombination.SHIFT_DOWN));
        technicalMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {       
                technicalMenuAction();
            }
        });
        MenuItem aboutMenuItem = new MenuItem(objDictionaryAction.getWord("ABOUTUS"));
        aboutMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/about_software.png"));
        aboutMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.A, KeyCombination.SHIFT_DOWN));
        aboutMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                aboutMenuAction();
            }
        });
        MenuItem contactMenuItem = new MenuItem(objDictionaryAction.getWord("CONTACTUS"));
        contactMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/contact_us.png"));
        contactMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.SHIFT_DOWN));
        contactMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                contactMenuAction();
            }
        });
        MenuItem logoutMenuItem = new MenuItem(objDictionaryAction.getWord("LOGOUT"));
        logoutMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/logout.png"));
        logoutMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.L, KeyCombination.SHIFT_DOWN));
        logoutMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                logoutMenuAction();
            }
        });
        MenuItem exitMenuItem = new MenuItem(objDictionaryAction.getWord("EXIT"));
        exitMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/quit.png"));
        exitMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.ESCAPE));
        exitMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                exitMenuAction();
            }
        });       
        supportMenu.getItems().addAll(helpMenuItem, technicalMenuItem, aboutMenuItem, contactMenuItem, new SeparatorMenuItem(), logoutMenuItem);
        menuBar.getMenus().addAll(homeMenu, parentMenu, supportMenu);
        root.setTop(menuBar);

        ScrollPane mycon = new ScrollPane();
        GridPane container = new GridPane();
        container.setPrefSize(objConfiguration.WIDTH, objConfiguration.HEIGHT);
        container.setId("container");
        mycon.setContent(container);
        root.setCenter(mycon);

        Label caption = new Label(objDictionaryAction.getWord("USER")+" "+objDictionaryAction.getWord("DEMOGRAPHIC"));
        caption.setId("caption");
        GridPane.setConstraints(caption, 0, 0);
        GridPane.setColumnSpan(caption, 5);
        container.getChildren().add(caption);

        final Separator sepHor1 = new Separator();
        sepHor1.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHor1, 0, 1);
        GridPane.setColumnSpan(sepHor1, 5);
        container.getChildren().add(sepHor1);

        final Separator sepVert1 = new Separator();
        sepVert1.setOrientation(Orientation.VERTICAL);
        sepVert1.setValignment(VPos.CENTER);
        sepVert1.setPrefHeight(80);
        GridPane.setConstraints(sepVert1, 2, 2);
        GridPane.setRowSpan(sepVert1, 13);
        container.getChildren().add(sepVert1);

        final Separator sepHor2 = new Separator();
        sepHor2.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHor2, 0, 15);
        GridPane.setColumnSpan(sepHor2, 5);
        container.getChildren().add(sepHor2);
        
        Label name= new Label(objDictionaryAction.getWord("NAME")+" :");
        //name.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        container.add(name, 0, 2);
        final TextField nameTF = new TextField();
        nameTF.setPromptText(objDictionaryAction.getWord("PROMPTNAME"));
        nameTF.setText(objConfiguration.getObjUser().getStrName());
        nameTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPNAME")));
        container.add(nameTF, 1, 2);

        Label email= new Label(objDictionaryAction.getWord("EMAILID")+" :");
        //email.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        container.add(email, 0, 3);
        final TextField emailTF = new TextField();
        emailTF.setPromptText(objDictionaryAction.getWord("PROMPTEMAILID"));
        emailTF.setText(objConfiguration.getObjUser().getStrEmail());
        emailTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPEMAILID")));
        container.add(emailTF, 1, 3);

        Label contact= new Label(objDictionaryAction.getWord("CONTACTNUMBER")+" :");
        //contact.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        container.add(contact, 0, 4);
        final TextField contactTF = new TextField();
        contactTF.setPromptText(objDictionaryAction.getWord("PROMPTCONTACTNUMBER"));
        contactTF.setText(objConfiguration.getObjUser().getStrContactNumber());
        contactTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCONTACTNUMBER")));
        container.add(contactTF, 1, 4);

        Label address= new Label(objDictionaryAction.getWord("ADDRESS")+" :");
        //address.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        container.add(address, 0, 5);
        final TextField addressTF = new TextField();
        addressTF.setPromptText(objDictionaryAction.getWord("PROMPTADDRESS"));
        addressTF.setText(objConfiguration.getObjUser().getStrAddress());
        addressTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPADDRESS")));
        container.add(addressTF, 1, 5);

        Label countryAddress= new Label(objDictionaryAction.getWord("COUNTRY")+" :");
        //countryAddress.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        container.add(countryAddress, 0, 6);
        //countryCB = new ComboBox<Label>();
        loadCountry();
        countryCB.setCellFactory(new Callback<ListView<Label>, ListCell<Label>>() {
            @Override
            public ListCell<Label> call(ListView<Label> p) {
                return new ListCell<Label>() {
                    private final Label cell=new Label();
                    
                    @Override
                    protected void updateItem(Label item, boolean empty) {
                        super.updateItem(item, empty);
                        
                        if (item == null || empty) {
                            setText(null);
                        } else {
                            setText(item.getText());
                        }
                    }
                };
            }
        });
        countryCB.setConverter(new StringConverter<Label>(){
            @Override
            public String toString(Label object) {
                return object.getText();
            }

            @Override
            public Label fromString(String string) {
                return new Label();
            }
        });
        countryCB.valueProperty().addListener(new ChangeListener<Label>() {
            @Override
            public void changed(ObservableValue<? extends Label> ov, Label t, Label t1) {
                if(countryCB.isFocused()){
                    Label lblTemp = (Label)t1;
                    loadState(Integer.parseInt(lblTemp.getUserData().toString()));
                }
            }
        });
        container.add(countryCB, 1, 6);
        cmbKeyListener(countryCB);

        Label stateAddress= new Label(objDictionaryAction.getWord("STATE")+" :");
        //countryAddress.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        container.add(stateAddress, 0, 7);
        //stateCB = new ComboBox<Label>();
        loadState(0);
        stateCB.setCellFactory(new Callback<ListView<Label>, ListCell<Label>>() {
            @Override
            public ListCell<Label> call(ListView<Label> p) {
                return new ListCell<Label>() {
                    private final Label cell=new Label();
                    
                    @Override
                    protected void updateItem(Label item, boolean empty) {
                        super.updateItem(item, empty);
                        
                        if (item == null || empty) {
                            setText(null);
                        } else {
                            setText(item.getText());
                        }
                    }
                };
            }
        });
        stateCB.setConverter(new StringConverter<Label>(){
            @Override
            public String toString(Label object) {
                return object.getText();
            }

            @Override
            public Label fromString(String string) {
                return new Label();
            }
        });
        stateCB.valueProperty().addListener(new ChangeListener<Label>() {
            @Override
            public void changed(ObservableValue<? extends Label> ov, Label t, Label t1) {
                if(stateCB.isFocused()){
                    Label lblTemp = (Label)t1;
                    loadCity(Integer.parseInt(lblTemp.getUserData().toString()));
                }
            }
        });
        container.add(stateCB, 1, 7);
        cmbKeyListener(stateCB);

        Label cityAddress= new Label(objDictionaryAction.getWord("CITY")+" :");
        //countryAddress.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        container.add(cityAddress, 0, 8);
        //cityCB = new ComboBox<Label>();
        loadCity(0);
        cityCB.setCellFactory(new Callback<ListView<Label>, ListCell<Label>>() {
            @Override
            public ListCell<Label> call(ListView<Label> p) {
                return new ListCell<Label>() {
                    private final Label cell=new Label();
                    
                    @Override
                    protected void updateItem(Label item, boolean empty) {
                        super.updateItem(item, empty);
                        
                        if (item == null || empty) {
                            setText(null);
                        } else {
                            setText(item.getText());
                        }
                    }
                };
            }
        });
        cityCB.setConverter(new StringConverter<Label>(){
            @Override
            public String toString(Label object) {
                return object.getText();
            }

            @Override
            public Label fromString(String string) {
                return new Label();
            }
        });
        container.add(cityCB, 1, 8);
        cmbKeyListener(cityCB);
        
        // set user's country, state, city
        Label lCountry, lState, lCity;
        if(objConfiguration.getObjUser().getStrCountryAddress()!=null){
            loadCountry();
            for(Iterator it = countryCB.getItems().iterator(); it.hasNext();){
                lCountry = (Label)it.next();
                if(lCountry.getText().equalsIgnoreCase(objConfiguration.getObjUser().getStrCountryAddress())){
                    countryCB.setValue(lCountry);
                    loadState(Integer.parseInt(lCountry.getUserData().toString()));
                    break;
                }
            }
            if(objConfiguration.getObjUser().getStrStateAddress()!=null){
                for(Iterator it = stateCB.getItems().iterator(); it.hasNext();){
                    lState = (Label)it.next();
                    if(lState.getText().equalsIgnoreCase(objConfiguration.getObjUser().getStrStateAddress())){
                        stateCB.setValue(lState);
                        loadCity(Integer.parseInt(lState.getUserData().toString()));
                        break;
                    }
                }
                if(objConfiguration.getObjUser().getStrCityAddress()!=null){
                    for(Iterator it = cityCB.getItems().iterator(); it.hasNext();){
                        lCity = (Label)it.next();
                        if(lCity.getText().equalsIgnoreCase(objConfiguration.getObjUser().getStrCityAddress())){
                            cityCB.setValue(lCity);
                            break;
                        }
                    }
                }
            }
        }

        Label pincodeAddress= new Label(objDictionaryAction.getWord("PINCODEADDRESS")+" :");
        //address.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        container.add(pincodeAddress, 0, 9);
        final TextField pincodeTF = new TextField();
        pincodeTF.setPromptText(objDictionaryAction.getWord("PROMPTPINCODEADDRESS"));
        pincodeTF.setText(objConfiguration.getObjUser().getStrPincodeAddress());
        pincodeTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPPINCODEADDRESS")));
        container.add(pincodeTF, 1, 9);

        Label geoAddress= new Label(objDictionaryAction.getWord("NEARBY")+" :");
        //address.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        container.add(geoAddress, 0, 10);
        final TextField geoTF = new TextField();
        geoTF.setPromptText(objDictionaryAction.getWord("PROMPTNEARBY"));
        geoTF.setText(objConfiguration.getObjUser().getStrGeoCity());
        geoTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPNEARBY")));
        container.add(geoTF, 1, 10);

        Label dob= new Label(objDictionaryAction.getWord("DOB")+" (yyyy-mm-dd):");
        //dob.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        container.add(dob, 0, 11);
        //DatePicker checkInDatePicker = new DatePicker(); 
        final TextField dobTF = new TextField();
        dobTF.setPromptText(objDictionaryAction.getWord("PROMPTDOB")); 
        dobTF.setText(objConfiguration.getObjUser().getStrDOB());
        dobTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPDOB")));
        container.add(dobTF, 1, 11);

        Label gender= new Label(objDictionaryAction.getWord("GENDER")+" :");
        //gender.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        container.add(gender, 0, 12);
        final ComboBox genderCB = new ComboBox();
        genderCB.getItems().addAll("M","F","O");   
        genderCB.setValue(objConfiguration.getObjUser().getStrGender());        
        genderCB.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPGENDER")));
        container.add(genderCB, 1, 12);

        Label education= new Label(objDictionaryAction.getWord("EDUCATION")+" :");
        //education.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        container.add(education, 0, 13);
        final ComboBox educationCB = new ComboBox();
        educationCB.getItems().addAll("Illiterate","Matriculation","Intermediate","Bachelor","Master");   
        educationCB.setValue(objConfiguration.getObjUser().getStrEducation());        
        educationCB.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPEDUCATION")));
        container.add(educationCB, 1, 13);

        Label occupation= new Label(objDictionaryAction.getWord("OCCUPATION")+" :");
        //address.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        container.add(occupation, 0, 14);
        final TextField occupationTF = new TextField();
        occupationTF.setPromptText(objDictionaryAction.getWord("PROMPTOCCUPATION"));
        occupationTF.setText(objConfiguration.getObjUser().getStrOccupation());
        occupationTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPOCCUPATION")));
        container.add(occupationTF, 1, 14);

        /*Label organization= new Label(objDictionaryAction.getWord("ORGANIZATION")+" :");
        //organization.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        container.add(organization, 0, 15);
        final TextField organizationTF = new TextField();
        organizationTF.setPromptText(objDictionaryAction.getWord("PROMPTORGANIZATION")); 
        organizationTF.setText(objConfiguration.getObjUser().getStrOrganization());
        organizationTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPORGANIZATION")));
        container.add(organizationTF, 1, 15);
        */

        ImageView profilePic = new ImageView("/media/user.png");
        profilePic.setFitHeight(200);
        profilePic.setFitWidth(200);
        container.add(profilePic, 3, 2, 2, 13);

        //action == events
        Button B_apply = new Button(objDictionaryAction.getWord("APPLY"));
        B_apply.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
        B_apply.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPAPPLY")));
        B_apply.setDefaultButton(true);
        B_apply.setFocusTraversable(true);
        container.add(B_apply, 1, 16); 

        Button B_skip = new Button(objDictionaryAction.getWord("CANCEL"));
        B_skip.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        B_skip.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
        container.add(B_skip, 3, 16);  

        B_apply.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                new MessageView(objConfiguration);
                if(objConfiguration.getServicePasswordValid()){
                    objConfiguration.setServicePasswordValid(false);
                    objConfiguration.getObjUser().setStrName(nameTF.getText());
                    objConfiguration.getObjUser().setStrEmailID(emailTF.getText());
                    objConfiguration.getObjUser().setStrContactNumber(contactTF.getText());
                    objConfiguration.getObjUser().setStrAddress(addressTF.getText());
                    objConfiguration.getObjUser().setStrCityAddress(((Label)cityCB.getValue()).getUserData().toString());
                    objConfiguration.getObjUser().setStrStateAddress(((Label)stateCB.getValue()).getUserData().toString());
                    objConfiguration.getObjUser().setStrCountryAddress(((Label)countryCB.getValue()).getUserData().toString());
                    objConfiguration.getObjUser().setStrPincodeAddress(pincodeTF.getText());
                    objConfiguration.getObjUser().setStrGeoCity(geoTF.getText());
                    objConfiguration.getObjUser().setDblGeoLat(0.1);
                    objConfiguration.getObjUser().setDblGeoLng(0.1);
                    objConfiguration.getObjUser().setStrDOB(dobTF.getText());
                    objConfiguration.getObjUser().setStrGender(genderCB.getValue().toString());
                    objConfiguration.getObjUser().setStrEducation(educationCB.getValue().toString());
                    objConfiguration.getObjUser().setStrOccupation(occupationTF.getText());
                    try {
                        UserAction objUserAction = new UserAction();
                        objUserAction.updateUserProfile(objConfiguration);
                    } catch (Exception ex) {
                        new Logging("SEVERE",UserProfileView.class.getName(),"save configuration",ex);
                    }
                    System.gc();
                    lblStatus.setText(objDictionaryAction.getWord("DATASAVED"));
                }
            }
        });
        B_skip.setOnAction(new EventHandler<ActionEvent>() {
             @Override
            public void handle(ActionEvent e) {
                System.gc();
                parentMenuAction();
                e.consume();
            }
        });
        
        userStage.getIcons().add(new Image("/media/icon.png"));
        userStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWUSERSETTINGS")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
        //userSettingStage.setIconified(true);
        userStage.setResizable(false);
        userStage.setScene(scene);
        userStage.setX(0);
        userStage.setY(0);
        userStage.show();
        userStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                parentMenuAction();  
                we.consume();
            }
        });
        final KeyCodeCombination homeKCC = new KeyCodeCombination(KeyCode.M, KeyCombination.SHIFT_DOWN); // Home Menu]
        final KeyCodeCombination parentKCC = new KeyCodeCombination(KeyCode.Z, KeyCombination.SHIFT_DOWN); // Home Menu]
        scene.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                if(homeKCC.match(t)){
                    homeMenuAction();
                } else if(parentKCC.match(t)){
                    parentMenuAction();
                }
            }
        });
    }

    private void loadCountry(){
        try {
            countryCB.getItems().clear();
            Label lblTemp = new Label("Select");
            lblTemp.setUserData(0);
            countryCB.getItems().add(lblTemp);
            countryCB.setValue(lblTemp);
            
            UserAction objUserAction = new UserAction();
            lstCountry = objUserAction.getLstCountry();
            for(int i=0; i<lstCountry.size(); i++){
                List lstTemp = (ArrayList)lstCountry.get(i);
                lblTemp = new Label(lstTemp.get(1).toString());
                lblTemp.setUserData(lstTemp.get(0));
                countryCB.getItems().add(lblTemp);
            }
        } catch (SQLException ex) {
            new Logging("SEVERE",UserProfileView.class.getName(),"SQLException: Listing Country",ex);
        } catch (Exception ex) {
            new Logging("SEVERE",UserProfileView.class.getName(),"Exception: Listing Country",ex);
        }
    }
    
    private void loadState(int intCountry){
        try {
            stateCB.getItems().clear();
            Label lblTemp = new Label("Select");
            lblTemp.setUserData(0);
            stateCB.getItems().add(lblTemp);
            stateCB.setValue(lblTemp);
            
            UserAction objUserAction = new UserAction();
            lstState = objUserAction.getLstState(intCountry);
            for(int i=0; i<lstState.size(); i++){
                List lstTemp = (ArrayList)lstState.get(i);
                lblTemp = new Label(lstTemp.get(1).toString());
                lblTemp.setUserData(lstTemp.get(0).toString());
                stateCB.getItems().add(lblTemp);
            }
        } catch (SQLException ex) {
            new Logging("SEVERE",UserProfileView.class.getName(),"SQLException: Listing State",ex);
        } catch (Exception ex) {
            new Logging("SEVERE",UserProfileView.class.getName(),"Exception: Listing State",ex);
        }
    }
    
    private void loadCity(int intState){
        try {
            cityCB.getItems().clear();
            Label lblTemp = new Label("Select");
            lblTemp.setUserData(0);
            cityCB.getItems().add(lblTemp);
            cityCB.setValue(lblTemp);
            
            UserAction objUserAction = new UserAction();
            lstCity = objUserAction.getLstCity(intState);
            for(int i=0; i<lstCity.size(); i++){
                List lstTemp = (ArrayList)lstCity.get(i);
                lblTemp = new Label(lstTemp.get(1).toString());
                lblTemp.setUserData(lstTemp.get(0).toString());
                cityCB.getItems().add(lblTemp);
            }
        } catch (SQLException ex) {
            new Logging("SEVERE",UserProfileView.class.getName(),"SQLException: Listing city",ex);
        } catch (Exception ex) {
            new Logging("SEVERE",UserProfileView.class.getName(),"Exception: Listing city",ex);
        }
    }
    
    public void cmbKeyListener(final ComboBox c){
        final StringBuilder sb=new StringBuilder();
        c.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>(){
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.UP || event.getCode() == KeyCode.TAB) {
                    return;
                }
                else if (event.getCode() == KeyCode.BACK_SPACE && sb.length() > 0) {
                    sb.deleteCharAt(sb.length()-1);
                }
                else {
                    sb.append(event.getText());
                }
 
                if (sb.length() == 0) 
                    return;
                
                boolean found = false;
                ObservableList items = c.getItems();
                for (int i=0; i<items.size(); i++) {
                    if (event.getCode() != KeyCode.BACK_SPACE && c.getConverter().toString(items.get(i)).toLowerCase().startsWith(sb.toString().toLowerCase())) {
                        ListView lv = ((ComboBoxListViewSkin) c.getSkin()).getListView();
                        lv.getSelectionModel().clearAndSelect(i);           
                        lv.scrollTo(lv.getSelectionModel().getSelectedIndex());
                        found = true;
                        break;
                    }
                }
                if (!found && sb.length() > 0)
                    sb.deleteCharAt(sb.length() - 1);
            }
        });
    }
    /**
     * homeMenuAction
     * <p>
     * Function use for help menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ConvertorView
     */
    private void homeMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHOME"));
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.setIconified(false);
        dialogStage.setFullScreen(false);
        dialogStage.setTitle(objDictionaryAction.getWord("ALERT"));
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 300, 100, Color.WHITE);
        scene.getStylesheets().add(UserSettingView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        popup.add(new ImageView("/media/alert_warning.png"), 0, 0);   
        Label lblAlert = new Label(objDictionaryAction.getWord("ALERTCLOSE"));
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(250);
        popup.add(lblAlert, 1, 0);
        Button btnYes = new Button(objDictionaryAction.getWord("YES"));
        btnYes.setPrefWidth(50);
        btnYes.setId("btnYes");
        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();                
                userStage.close();
                System.gc();
                WindowView objWindoeView = new WindowView(objConfiguration);
            }
        });
        popup.add(btnYes, 0, 1);
        Button btnNo = new Button(objDictionaryAction.getWord("NO"));
        btnNo.setPrefWidth(50);
        btnNo.setId("btnNo");
        btnNo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
            }
        });
        popup.add(btnNo, 1, 1);
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.showAndWait(); 
    }
    /**
     * parentMenuAction
     * <p>
     * Function use for help menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ConvertorView
     */
    private void parentMenuAction(){
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.setIconified(false);
        dialogStage.setFullScreen(false);
        dialogStage.setTitle(objDictionaryAction.getWord("ALERT"));
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 300, 100, Color.WHITE);
        scene.getStylesheets().add(UserSettingView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        final GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        popup.add(new ImageView("/media/alert_warning.png"), 0, 0); 
        Label lblAlert = new Label(objDictionaryAction.getWord("ALERTCLOSE"));
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(250);
        popup.add(lblAlert, 1, 0);
        Button btnYes = new Button(objDictionaryAction.getWord("YES"));
        btnYes.setPrefWidth(50);
        btnYes.setId("btnYes");
        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                userStage.close();
                System.gc();
                UserView objUserView = new UserView(objConfiguration);
            }
        });
        popup.add(btnYes, 0, 1);
        Button btnNo = new Button(objDictionaryAction.getWord("NO"));
        btnNo.setPrefWidth(50);
        btnNo.setId("btnNo");
        btnNo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
            }
        });
        popup.add(btnNo, 1, 1);
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.showAndWait();
    }
    /**
     * helpMenuAction
     * <p>
     * Function use for help menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ConvertorView
     */
    private void helpMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHELP"));
        HelpView objHelpView = new HelpView(objConfiguration);
    }
    /**
     * technicalMenuAction
     * <p>
     * Function use for technical menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ConvertorView
     */
    private void technicalMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONTECHNICAL"));
        TechnicalView objTechnicalView = new TechnicalView(objConfiguration);
    }
    /**
     * aboutMenuAction
     * <p>
     * Function use for about menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ConvertorView
     */
    private void aboutMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONABOUTUS"));
        AboutView objAboutView = new AboutView(objConfiguration);
    }
    /**
     * contactMenuAction
     * <p>
     * Function use for contact menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ConvertorView
     */
    private void contactMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCONTACTUS"));
        ContactView objContactView = new ContactView(objConfiguration);
    }
    /**
     * logoutMenuAction
     * <p>
     * Function use for exit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void logoutMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHOME"));
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.setIconified(false);
        dialogStage.setFullScreen(false);
        dialogStage.setTitle(objDictionaryAction.getWord("ALERT"));
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 300, 100, Color.WHITE);
        scene.getStylesheets().add(UserView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        popup.add(new ImageView("/media/alert_warning.png"), 0, 0);   
        Label lblAlert = new Label(objDictionaryAction.getWord("ALERTCLOSE"));
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(250);
        popup.add(lblAlert, 1, 0);
        Button btnYes = new Button(objDictionaryAction.getWord("YES"));
        btnYes.setPrefWidth(50);
        btnYes.setId("btnYes");
        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                objConfiguration.setStrRecentWeave(null);
                dialogStage.close();
                userStage.close();
                System.gc();
                UserLoginView objUserLoginView = new UserLoginView(objConfiguration);
            }
        });
        popup.add(btnYes, 0, 1);
        Button btnNo = new Button(objDictionaryAction.getWord("NO"));
        btnNo.setPrefWidth(50);
        btnNo.setId("btnNo");
        btnNo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
            }
        });
        popup.add(btnNo, 1, 1);
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.showAndWait();
    }
    /**
     * exitMenuAction
     * <p>
     * Function use for exit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ConvertorView
     */
    private void exitMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONEXIT"));
        System.gc();
        userStage.close();
    }
    @Override
    public void start(Stage stage) throws Exception {
        new UserProfileView(stage);
        new Logging("WARNING",UserProfileView.class.getName(),"UnsupportedOperationException",new UnsupportedOperationException("Not supported yet."));
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}