/*
 * Copyright (C) Digital India Corporation (Media Lab Asia)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.mla.user;

import com.mla.dictionary.DictionaryAction;
import com.mla.utility.AboutView;
import com.mla.main.Configuration;
import com.mla.utility.ContactView;
import com.mla.utility.HelpView;
import com.mla.main.Logging;
import com.mla.main.MessageView;
import com.mla.utility.TechnicalView;
import com.mla.main.WindowView;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
/**
 *
 * @Designing GUI window for user preferences
 * @author Amit Kumar Singh
 * 
 */
public class UserResetPassword extends Application {
    private static Stage userStage;
    private BorderPane root;
    
    User objUser = null;
    Configuration objConfiguration = null;
    DictionaryAction objDictionaryAction = null;
    
    private Label lblStatus;
    private ProgressBar progressB;
    private ProgressIndicator progressI;
	
    public UserResetPassword(final Stage primaryStage) {}
    
    public UserResetPassword(Configuration objConfigurationCall) {
        objConfiguration = objConfigurationCall;
        objDictionaryAction = new DictionaryAction(objConfiguration);

        userStage = new Stage();
        root = new BorderPane();
        Scene scene = new Scene(root, objConfiguration.WIDTH, objConfiguration.HEIGHT, Color.WHITE);
        scene.getStylesheets().add(UserResetPassword.class.getResource(objConfiguration.getStrTemplate()+"/setting.css").toExternalForm());

        HBox footContainer = new HBox();
        progressB = new ProgressBar(0);
        progressB.setVisible(false);
        progressI = new ProgressIndicator(0);
        progressI.setVisible(false);
        lblStatus = new Label(objDictionaryAction.getWord("WELCOMETOCADTOOL"));
        lblStatus.setId("message");
        footContainer.getChildren().addAll(lblStatus,progressB,progressI);
        footContainer.setId("footContainer");
        root.setBottom(footContainer);
        
        MenuBar menuBar = new MenuBar();
        menuBar.prefWidthProperty().bind(userStage.widthProperty());
       
        Menu homeMenu  = new Menu();
        HBox homeMenuHB = new HBox();
        homeMenuHB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/home.png"),new Label(objDictionaryAction.getWord("HOME")));
        homeMenu.setGraphic(homeMenuHB);
        homeMenu.setAccelerator(new KeyCodeCombination(KeyCode.M, KeyCombination.SHIFT_DOWN));
        homeMenuHB.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                homeMenuAction();  
                me.consume();
            }
        }); 
        Menu parentMenu  = new Menu();
        HBox parentMenuHB = new HBox();
        parentMenuHB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/settings.png"),new Label(objDictionaryAction.getWord("PARENT")));
        parentMenu.setGraphic(parentMenuHB);
        parentMenu.setAccelerator(new KeyCodeCombination(KeyCode.Z, KeyCombination.SHIFT_DOWN));
        parentMenuHB.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                parentMenuAction();                 
                me.consume();
            }
        }); 
        //Help Menu items
        Label supportMenuLabel = new Label(objDictionaryAction.getWord("SUPPORT"));
        supportMenuLabel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUPPORT")));
        Menu supportMenu = new Menu();
        supportMenu.setGraphic(supportMenuLabel);
        
        MenuItem helpMenuItem = new MenuItem(objDictionaryAction.getWord("HELP"));
        helpMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        helpMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.H, KeyCombination.SHIFT_DOWN));
        helpMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {       
                helpMenuAction();
            }
        });        
        MenuItem technicalMenuItem = new MenuItem(objDictionaryAction.getWord("TECHNICAL"));
        technicalMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/technical_info.png"));
        technicalMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.T, KeyCombination.SHIFT_DOWN));
        technicalMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {       
                technicalMenuAction();
            }
        });
        MenuItem aboutMenuItem = new MenuItem(objDictionaryAction.getWord("ABOUTUS"));
        aboutMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/about_software.png"));
        aboutMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.A, KeyCombination.SHIFT_DOWN));
        aboutMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                aboutMenuAction();
            }
        });
        MenuItem contactMenuItem = new MenuItem(objDictionaryAction.getWord("CONTACTUS"));
        contactMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/contact_us.png"));
        contactMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.SHIFT_DOWN));
        contactMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                contactMenuAction();
            }
        });
        MenuItem logoutMenuItem = new MenuItem(objDictionaryAction.getWord("LOGOUT"));
        logoutMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/logout.png"));
        logoutMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.L, KeyCombination.SHIFT_DOWN));
        logoutMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                logoutMenuAction();
            }
        });
        MenuItem exitMenuItem = new MenuItem(objDictionaryAction.getWord("EXIT"));
        exitMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/quit.png"));
        exitMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.ESCAPE));
        exitMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                exitMenuAction();
            }
        });       
        supportMenu.getItems().addAll(helpMenuItem, technicalMenuItem, aboutMenuItem, contactMenuItem, new SeparatorMenuItem(), logoutMenuItem);
        menuBar.getMenus().addAll(homeMenu, parentMenu, supportMenu);
        root.setTop(menuBar);
        
        ScrollPane container = new ScrollPane();
        
        TabPane tabPane = new TabPane();

        Tab userPasswordTab = new Tab();
        Tab appPasswordTab = new Tab();
        
        userPasswordTab.setClosable(false);
        appPasswordTab.setClosable(false);
        
        userPasswordTab.setText(objDictionaryAction.getWord("USER")+" "+objDictionaryAction.getWord("CHANGEPASSWORD"));
        appPasswordTab.setText(objDictionaryAction.getWord("USER")+" "+objDictionaryAction.getWord("SERVICEPASSWORD"));
        
        userPasswordTab.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCHANGEPASSWORD")));
        appPasswordTab.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
        
        //userPasswordTab.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export.png"));
        //appPasswordTab.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/import.png"));
        
        GridPane userPasswordGP = new GridPane();
        GridPane appPasswordGP = new GridPane();
        
        userPasswordGP.setId("container");
        appPasswordGP.setId("container");
        
         //userPasswordGP.setPrefSize(objConfiguration.WIDTH, objConfiguration.HEIGHT);
        //appPasswordGP.setPrefSize(objConfiguration.WIDTH, objConfiguration.HEIGHT);
        
        //exportGP.setAlignment(Pos.TOP_CENTER);
        //appPasswordGP.setAlignment(Pos.TOP_CENTER);
        
        userPasswordTab.setContent(userPasswordGP);
        appPasswordTab.setContent(appPasswordGP);
        
        tabPane.getTabs().add(userPasswordTab);
        tabPane.getTabs().add(appPasswordTab);
        
        container.setContent(tabPane);
        
        //////---------user password Data-------//////
        Label userPassword = new Label(objDictionaryAction.getWord("USER")+" "+objDictionaryAction.getWord("CHANGEPASSWORD"));
        userPassword.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/profile.png"));
        userPassword.setId("caption");
        GridPane.setConstraints(userPassword, 0, 0);
        GridPane.setColumnSpan(userPassword, 4);
        userPasswordGP.getChildren().add(userPassword);

        Label oldUserPasswordLbl= new Label(objDictionaryAction.getWord("OLD")+" "+objDictionaryAction.getWord("PASSWORD")+" :");
        //oldUserPasswordLbl.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        userPasswordGP.add(oldUserPasswordLbl, 0, 1);
        final PasswordField oldUserPasswordTF = new PasswordField();
        oldUserPasswordTF.setPromptText(objDictionaryAction.getWord("PROMPTPASSWORD"));
        oldUserPasswordTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPPASSWORD")));
        userPasswordGP.add(oldUserPasswordTF, 1, 1);        
        oldUserPasswordTF.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                // validating password when password PasswordField loses focus
                if(!newValue){  
                    if(oldUserPasswordTF.getText()==""){
                        oldUserPasswordTF.setId("error");
                        lblStatus.setText(objDictionaryAction.getWord("INVALIDOLDPASSWORD"));
                    } else if(oldUserPasswordTF.getText().length()<6){ // smaller length password are weak but acceptable
                        oldUserPasswordTF.setId("error");
                        lblStatus.setText(objDictionaryAction.getWord("WEEKPASSWORD"));
                    } else {  // password ok
                        oldUserPasswordTF.setId("success");
                        lblStatus.setText(objDictionaryAction.getWord("STRONGPASSWORD"));
                    }
                }
            }
        });

        Label newUserPasswordLbl= new Label(objDictionaryAction.getWord("NEW")+" "+objDictionaryAction.getWord("PASSWORD")+" :");
        //newUserPasswordLbl.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        userPasswordGP.add(newUserPasswordLbl, 0, 2);
        final PasswordField newUserPasswordTF = new PasswordField();
        newUserPasswordTF.setPromptText(objDictionaryAction.getWord("PROMPTPASSWORD"));
        newUserPasswordTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPPASSWORD")));
        userPasswordGP.add(newUserPasswordTF, 1, 2);
        newUserPasswordTF.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                // validating password when password PasswordField loses focus
                if(!newValue){  
                    if(newUserPasswordTF.getText()==""){
                        newUserPasswordTF.setId("error");
                        lblStatus.setText(objDictionaryAction.getWord("INVALIDPASSWORD"));
                    } else if(!(oldUserPasswordTF.getText().equals(newUserPasswordTF.getText()))){
                        newUserPasswordTF.setId("error");
                        lblStatus.setText(objDictionaryAction.getWord("NOMATCHEDPASSWORD"));
                    } else if(newUserPasswordTF.getText().length()<6){ // smaller length password are weak but acceptable
                        newUserPasswordTF.setId("error");
                        lblStatus.setText(objDictionaryAction.getWord("WEEKPASSWORD"));
                    } else {  // password ok
                        newUserPasswordTF.setId("success");
                        lblStatus.setText(objDictionaryAction.getWord("STRONGPASSWORD"));
                    }
                }
            }
        });
        
        Label confUserPasswordLbl= new Label(objDictionaryAction.getWord("CONFIRMPASSWORD")+" :");
        //confUserPasswordLbl.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        userPasswordGP.add(confUserPasswordLbl, 0, 3);
        final PasswordField confUserPasswordTF = new PasswordField();
        confUserPasswordTF.setPromptText(objDictionaryAction.getWord("PROMPTPASSWORD"));
        confUserPasswordTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPPASSWORD")));
        userPasswordGP.add(confUserPasswordTF, 1, 3);
        confUserPasswordTF.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                // validate Confirm Password field when rePassword Password Field loses focus
                if(!newValue){  
                    if(confUserPasswordTF.getText()==""){
                        confUserPasswordTF.setId("error");
                        lblStatus.setText(objDictionaryAction.getWord("INVALIDPASSWORD"));
                    } else if(!(confUserPasswordTF.getText().equals(newUserPasswordTF.getText()))){
                        confUserPasswordTF.setId("error");
                        lblStatus.setText(objDictionaryAction.getWord("NOMATCHPASSWORD"));
                    } else {
                        confUserPasswordTF.setId("success");
                        lblStatus.setText(objDictionaryAction.getWord("STRONGPASSWORD"));
                    }
                }
            }
        });
        final Separator sepHor1 = new Separator();
        sepHor1.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHor1, 0, 4);
        GridPane.setColumnSpan(sepHor1, 4);
        userPasswordGP.getChildren().add(sepHor1);

        //action == events
        Button btnSubmitUser = new Button(objDictionaryAction.getWord("APPLY"));
        btnSubmitUser.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
        btnSubmitUser.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPAPPLY")));
        btnSubmitUser.setDefaultButton(true);
        btnSubmitUser.setFocusTraversable(true);
        userPasswordGP.add(btnSubmitUser, 0, 5); 

        Button btnCancelUser = new Button(objDictionaryAction.getWord("CANCEL"));
        btnCancelUser.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        btnCancelUser.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
        userPasswordGP.add(btnCancelUser, 1, 5);  

        btnSubmitUser.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                new MessageView(objConfiguration);
                if(objConfiguration.getServicePasswordValid()){
                    if(oldUserPasswordTF.getText().length()==0||newUserPasswordTF.getText().length()==0||confUserPasswordTF.getText().length()==0){
                        lblStatus.setText(objDictionaryAction.getWord("INVALIDDATA"));    // if either of the password fields is empty
                    } else{
                        if(!newUserPasswordTF.getText().equals(confUserPasswordTF.getText())){
                            // NEW password & CONFIRM NEW password do not match
                            lblStatus.setText(objDictionaryAction.getWord("NOMATCHPASSWORD"));
                        } else{
                            try{
                                UserAction userAction=new UserAction();
                                // temporarily, store getStrPassword() as we will be overwriting it with User Entered OLD (TO BE VALIDATED) password.
                                String backupStrPassword=objConfiguration.getObjUser().getStrPassword();
                                // overwriting getStrPassword() for validation
                                objConfiguration.getObjUser().setStrPassword(oldUserPasswordTF.getText());
                                // validating User Entered OLD password using "com.mla.user.UserAction.verifyLogin()".
                                boolean passwordMatch=userAction.verifyLogin(objConfiguration.getObjUser());
                                // setting back original getStrPassword() from backupStrPassword
                                objConfiguration.getObjUser().setStrPassword(backupStrPassword);
                                if(passwordMatch){  // OLD password validated successfully
                                    // NEW password being hashed and updated in "com.mla.user.UserAction.updatePassword()".
                                    objConfiguration.getObjUser().setStrPassword(newUserPasswordTF.getText());
                                    boolean changed=new UserAction().updatePassword(objConfiguration.getObjUser());
                                    if(changed){    // password modified successfully
                                        lblStatus.setText(objDictionaryAction.getWord("DATASAVED"));
                                    } else{       // password updation failed                                            
                                        lblStatus.setText(objDictionaryAction.getWord("DATAUNSAVED"));
                                    }
                                } else{           // OLD password not validated
                                    objConfiguration.getObjUser().setStrPassword(backupStrPassword);
                                    lblStatus.setText(objDictionaryAction.getWord("INVALIDOLDPASSWORD"));
                                }
                            } catch(Exception ex){
                                new Logging("SEVERE",UserProfileView.class.getName(),"ChangePassword -> verifyLogin() or updatePassword(): error validating/updating data from database",ex);
                            }
                        }
                    }
                }
            }
        });
        btnCancelUser.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                System.gc();
                parentMenuAction();
                e.consume();
            }
        });

        //////---------user password Data-------//////
        Label appPassword = new Label(objDictionaryAction.getWord("USER")+" "+objDictionaryAction.getWord("SERVICEPASSWORD"));
        appPassword.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/profile.png"));
        appPassword.setId("caption");
        GridPane.setConstraints(appPassword, 0, 0);
        GridPane.setColumnSpan(appPassword, 4);
        appPasswordGP.getChildren().add(appPassword);
        
        Label oldAppPasswordLbl= new Label(objDictionaryAction.getWord("OLD")+" "+objDictionaryAction.getWord("PASSWORD")+" :");
        //oldAppPasswordLbl.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        appPasswordGP.add(oldAppPasswordLbl, 0, 1);
        final PasswordField oldAppPasswordTF = new PasswordField();
        oldAppPasswordTF.setPromptText(objDictionaryAction.getWord("PROMPTPASSWORD"));
        oldAppPasswordTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPPASSWORD")));
        appPasswordGP.add(oldAppPasswordTF, 1, 1);
        oldAppPasswordTF.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                // validating password when password PasswordField loses focus
                if(!newValue){  
                    if(oldAppPasswordTF.getText()==""){
                        oldAppPasswordTF.setId("error");
                        lblStatus.setText(objDictionaryAction.getWord("INVALIDOLDPASSWORD"));
                    } else if(oldAppPasswordTF.getText().length()<6){ // smaller length password are weak but acceptable
                        oldAppPasswordTF.setId("error");
                        lblStatus.setText(objDictionaryAction.getWord("WEEKPASSWORD"));
                    } else {  // password ok
                        oldAppPasswordTF.setId("success");
                        lblStatus.setText(objDictionaryAction.getWord("STRONGPASSWORD"));
                    }
                }
            }
        });
        
        Label newAppPasswordLbl= new Label(objDictionaryAction.getWord("NEW")+" "+objDictionaryAction.getWord("PASSWORD")+" :");
        //newAppPasswordLbl.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        appPasswordGP.add(newAppPasswordLbl, 0, 2);
        final PasswordField newAppPasswordTF = new PasswordField();
        newAppPasswordTF.setPromptText(objDictionaryAction.getWord("PROMPTPASSWORD"));
        newAppPasswordTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPPASSWORD")));
        appPasswordGP.add(newAppPasswordTF, 1, 2);
        newUserPasswordTF.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                // validating password when password PasswordField loses focus
                if(!newValue){  
                    if(newAppPasswordTF.getText()==""){
                        newAppPasswordTF.setId("error");
                        lblStatus.setText(objDictionaryAction.getWord("INVALIDPASSWORD"));
                    } else if(!(oldAppPasswordTF.getText().equals(newAppPasswordTF.getText()))){
                        newAppPasswordTF.setId("error");
                        lblStatus.setText(objDictionaryAction.getWord("NOMATCHEDPASSWORD"));
                    } else if(newAppPasswordTF.getText().length()<6){ // smaller length password are weak but acceptable
                        newAppPasswordTF.setId("error");
                        lblStatus.setText(objDictionaryAction.getWord("WEEKPASSWORD"));
                    } else {  // password ok
                        newAppPasswordTF.setId("success");
                        lblStatus.setText(objDictionaryAction.getWord("STRONGPASSWORD"));
                    }
                }
            }
        });
        
        Label confAppPasswordLbl= new Label(objDictionaryAction.getWord("CONFIRMPASSWORD")+" :");
        //confAppPasswordLbl.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        appPasswordGP.add(confAppPasswordLbl, 0, 3);
        final PasswordField confAppPasswordTF = new PasswordField();
        confAppPasswordTF.setPromptText(objDictionaryAction.getWord("PROMPTPASSWORD"));
        confAppPasswordTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPPASSWORD")));
        appPasswordGP.add(confAppPasswordTF, 1, 3);
        confUserPasswordTF.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                // validate Confirm Password field when rePassword Password Field loses focus
                if(!newValue){  
                    if(confAppPasswordTF.getText()==""){
                        confAppPasswordTF.setId("error");
                        lblStatus.setText(objDictionaryAction.getWord("INVALIDPASSWORD"));
                    } else if(!(confAppPasswordTF.getText().equals(newAppPasswordTF.getText()))){
                        confAppPasswordTF.setId("error");
                        lblStatus.setText(objDictionaryAction.getWord("NOMATCHPASSWORD"));
                    } else {
                        confAppPasswordTF.setId("success");
                        lblStatus.setText(objDictionaryAction.getWord("STRONGPASSWORD"));
                    }
                }
            }
        });
        
        final Separator sepHor2 = new Separator();
        sepHor2.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHor2, 0, 4);
        GridPane.setColumnSpan(sepHor2, 4);
        appPasswordGP.getChildren().add(sepHor2);
        
        //action == events
        Button btnSubmitApp = new Button(objDictionaryAction.getWord("APPLY"));
        btnSubmitApp.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
        btnSubmitApp.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPAPPLY")));
        btnSubmitApp.setDefaultButton(true);
        btnSubmitApp.setFocusTraversable(true);
        appPasswordGP.add(btnSubmitApp, 0, 5); 

        Button btnCancelApp = new Button(objDictionaryAction.getWord("CANCEL"));
        btnCancelApp.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        btnCancelApp.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
        appPasswordGP.add(btnCancelApp, 1, 5);  

        btnSubmitApp.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                System.gc();
                lblStatus.setText(objDictionaryAction.getWord("ACTIONRESTRICT"));
            }
        });
        btnCancelApp.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                System.gc();
                parentMenuAction();
                e.consume();
            }
        });
        
        GridPane bodyContainer = new GridPane();
        bodyContainer.setPrefSize(objConfiguration.WIDTH, objConfiguration.HEIGHT);
        bodyContainer.setId("container");
        
        Label caption = new Label(objDictionaryAction.getWord("USER")+"-"+objDictionaryAction.getWord("SERVICE")+" "+objDictionaryAction.getWord("PASSWORD"));
        caption.setId("caption");
        bodyContainer.add(caption, 0, 0, 1, 1);
        
        bodyContainer.add(tabPane, 0, 1, 1, 1);
        /*
        final Separator sepHor = new Separator();
        sepHor.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHor, 0, 2);
        GridPane.setColumnSpan(sepHor, 1);
        bodyContainer.getChildren().add(sepHor);
        */
        root.setCenter(bodyContainer);
        
        userStage.getIcons().add(new Image("/media/icon.png"));
        userStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWUSERSETTINGS")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
        //userSettingStage.setIconified(true);
        userStage.setResizable(false);
        userStage.setScene(scene);
        userStage.setX(0);
        userStage.setY(0);
        userStage.show();
        userStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                parentMenuAction(); 
                we.consume();
            }
        });
        final KeyCodeCombination homeKCC = new KeyCodeCombination(KeyCode.M, KeyCombination.SHIFT_DOWN); // Home Menu]
        final KeyCodeCombination parentKCC = new KeyCodeCombination(KeyCode.Z, KeyCombination.SHIFT_DOWN); // Home Menu]
        scene.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                if(homeKCC.match(t)){
                    homeMenuAction();
                } else if(parentKCC.match(t)){
                    parentMenuAction();
                }
            }
        });
    }
    /**
     * homeMenuAction
     * <p>
     * Function use for help menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ConvertorView
     */
    private void homeMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHOME"));
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.setIconified(false);
        dialogStage.setFullScreen(false);
        dialogStage.setTitle(objDictionaryAction.getWord("ALERT"));
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 300, 100, Color.WHITE);
        scene.getStylesheets().add(UserSettingView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        popup.add(new ImageView("/media/alert_warning.png"), 0, 0);   
        Label lblAlert = new Label(objDictionaryAction.getWord("ALERTCLOSE"));
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(250);
        popup.add(lblAlert, 1, 0);
        Button btnYes = new Button(objDictionaryAction.getWord("YES"));
        btnYes.setPrefWidth(50);
        btnYes.setId("btnYes");
        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();                
                userStage.close();
                System.gc();
                WindowView objWindoeView = new WindowView(objConfiguration);
            }
        });
        popup.add(btnYes, 0, 1);
        Button btnNo = new Button(objDictionaryAction.getWord("NO"));
        btnNo.setPrefWidth(50);
        btnNo.setId("btnNo");
        btnNo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
            }
        });
        popup.add(btnNo, 1, 1);
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.showAndWait(); 
    }
    /**
     * parentMenuAction
     * <p>
     * Function use for help menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ConvertorView
     */
    private void parentMenuAction(){
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.setIconified(false);
        dialogStage.setFullScreen(false);
        dialogStage.setTitle(objDictionaryAction.getWord("ALERT"));
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 300, 100, Color.WHITE);
        scene.getStylesheets().add(UserSettingView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        final GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        popup.add(new ImageView("/media/alert_warning.png"), 0, 0); 
        Label lblAlert = new Label(objDictionaryAction.getWord("ALERTCLOSE"));
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(250);
        popup.add(lblAlert, 1, 0);
        Button btnYes = new Button(objDictionaryAction.getWord("YES"));
        btnYes.setPrefWidth(50);
        btnYes.setId("btnYes");
        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                userStage.close();
                System.gc();
                UserView objUserView = new UserView(objConfiguration);
            }
        });
        popup.add(btnYes, 0, 1);
        Button btnNo = new Button(objDictionaryAction.getWord("NO"));
        btnNo.setPrefWidth(50);
        btnNo.setId("btnNo");
        btnNo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
            }
        });
        popup.add(btnNo, 1, 1);
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.showAndWait();
    }
    /**
     * helpMenuAction
     * <p>
     * Function use for help menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ConvertorView
     */
    private void helpMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHELP"));
        HelpView objHelpView = new HelpView(objConfiguration);
    }
    /**
     * technicalMenuAction
     * <p>
     * Function use for technical menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ConvertorView
     */
    private void technicalMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONTECHNICAL"));
        TechnicalView objTechnicalView = new TechnicalView(objConfiguration);
    }
    /**
     * aboutMenuAction
     * <p>
     * Function use for about menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ConvertorView
     */
    private void aboutMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONABOUTUS"));
        AboutView objAboutView = new AboutView(objConfiguration);
    }
    /**
     * contactMenuAction
     * <p>
     * Function use for contact menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ConvertorView
     */
    private void contactMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCONTACTUS"));
        ContactView objContactView = new ContactView(objConfiguration);
    }
    /**
     * logoutMenuAction
     * <p>
     * Function use for exit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void logoutMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHOME"));
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.setIconified(false);
        dialogStage.setFullScreen(false);
        dialogStage.setTitle(objDictionaryAction.getWord("ALERT"));
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 300, 100, Color.WHITE);
        scene.getStylesheets().add(UserView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        popup.add(new ImageView("/media/alert_warning.png"), 0, 0);   
        Label lblAlert = new Label(objDictionaryAction.getWord("ALERTCLOSE"));
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(250);
        popup.add(lblAlert, 1, 0);
        Button btnYes = new Button(objDictionaryAction.getWord("YES"));
        btnYes.setPrefWidth(50);
        btnYes.setId("btnYes");
        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                objConfiguration.setStrRecentWeave(null);
                dialogStage.close();
                userStage.close();
                System.gc();
                UserLoginView objUserLoginView = new UserLoginView(objConfiguration);
            }
        });
        popup.add(btnYes, 0, 1);
        Button btnNo = new Button(objDictionaryAction.getWord("NO"));
        btnNo.setPrefWidth(50);
        btnNo.setId("btnNo");
        btnNo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
            }
        });
        popup.add(btnNo, 1, 1);
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.showAndWait();
    }
    /**
     * exitMenuAction
     * <p>
     * Function use for exit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        ConvertorView
     */
    private void exitMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONEXIT"));
        System.gc();
        userStage.close();
    }
    @Override
    public void start(Stage stage) throws Exception {
        new UserResetPassword(stage);
        new Logging("WARNING",UserResetPassword.class.getName(),"UnsupportedOperationException",new UnsupportedOperationException("Not supported yet."));
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}