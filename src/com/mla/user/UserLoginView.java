/*
 * Copyright (C) Digital India Corporation (Media Lab Asia)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mla.user;

import com.mla.dictionary.DictionaryAction;
import com.mla.main.ActivationView;
import com.mla.utility.AboutView;
import com.mla.main.Configuration;
import static com.mla.main.DigiBunai.Constants.CONFIG;
import com.mla.utility.ContactView;
import com.mla.utility.HelpView;
import com.mla.main.Logging;
import com.mla.utility.TechnicalView;
import com.mla.main.WindowView;
import java.sql.SQLException;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.Cursor;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
/**
 *
 * @Designing GUI Window for user
 * @author Amit Kumar Singh
 * 
 */
public class UserLoginView extends Application {

    private static Stage userStage;
    private TextField txtUsername;
    private PasswordField txtPassword;
    User objUser = null;
    Configuration objConfiguration = null;
    DictionaryAction objDictionaryAction = null;
    WindowView objWindowView = null;
    private Text lblStatus;    
    
  public UserLoginView(final Stage primaryStage) { }
  
  public UserLoginView(Configuration objConfigurationCall) {      
    objUser = new User();
    objConfiguration = objConfigurationCall;
    objDictionaryAction = new DictionaryAction(objConfiguration);
    
    userStage = new Stage();
    //userStage.initModality(Modality.APPLICATION_MODAL);//WINDOW_MODAL
    //userStage.initStyle(StageStyle.UNDECORATED);
    userStage.initStyle(StageStyle.TRANSPARENT);
    userStage.setResizable(false);
    userStage.setIconified(false);
    userStage.setFullScreen(false);
                
    BorderPane root = new BorderPane();
    Scene scene = new Scene(root, 450, 250, Color.TRANSPARENT);
    scene.getStylesheets().add(UserLoginView.class.getResource("/media/login.css").toExternalForm());
    root.setId("borderpane");    
    
    final ImageView closeIV = new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png");
    Tooltip.install(closeIV, new Tooltip(objDictionaryAction.getWord("TOOLTIPCLOSE")));    
    closeIV.setOnMouseEntered(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            closeIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            closeIV.setScaleX(1.5);
            closeIV.setScaleY(1.5);
            closeIV.setScaleZ(1.5);
        }
    });
    closeIV.setOnMouseExited(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            closeIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            closeIV.setScaleX(1);
            closeIV.setScaleY(1);
            closeIV.setScaleZ(1);
        }
    });
    closeIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            userStage.close();
        }
    });

    GridPane bodyConrainer = new GridPane();   
    bodyConrainer.setId("bodyConrainer");
    bodyConrainer.getColumnConstraints().add(new ColumnConstraints(130)); // column 1 is 100 wide
    bodyConrainer.getColumnConstraints().add(new ColumnConstraints(220)); // column 2 is 200 wide
    bodyConrainer.setAlignment(Pos.CENTER);
    bodyConrainer.setMaxSize(350, 200);
    bodyConrainer.relocate(10, 10);
    //bodyConrainer.setLayoutX(30);
    //bodyConrainer.setLayoutY(10);
        
    Text projectTitle = new Text(objDictionaryAction.getWord("TRADEMARK"));
    projectTitle.setFont(Font.font(objConfiguration.getStrBFont(), FontWeight.NORMAL, objConfiguration.getIntBFontSize()));
    projectTitle.setId("welcome-text");
    bodyConrainer.add(projectTitle, 0, 0, 2, 1);
        
    Label versionTitle = new Label(objDictionaryAction.getWord("VERSIONINFO"));
    versionTitle.setId("version-text");
    bodyConrainer.add(versionTitle, 0, 1, 2, 1);
    
    if(!CONFIG.USERTYPE.equalsIgnoreCase("Guest")){
        Label lblUsername = new Label(objDictionaryAction.getWord("USERNAME")+ ":");
        lblUsername.setId("username");
        lblUsername.getStyleClass().add("loginlabel");
        bodyConrainer.add(lblUsername, 0, 2);

        txtUsername = new TextField();
        txtUsername.setMinSize(180, 25);
        txtUsername.setMaxSize(180, 50);
        txtUsername.setPromptText(objDictionaryAction.getWord("PROMPTUSERNAME"));
        final Tooltip usernameTT = new Tooltip();
        usernameTT.setText(objDictionaryAction.getWord("TOOLTIPUSERNAME"));
        usernameTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        txtUsername.setTooltip(usernameTT);
        bodyConrainer.add(txtUsername, 1, 2);

        Label lblPassword = new Label(objDictionaryAction.getWord("PASSWORD")+":");
        lblPassword.setId("password");
        lblPassword.getStyleClass().add("loginlabel");
        bodyConrainer.add(lblPassword, 0, 3);

        txtPassword = new PasswordField();
        txtPassword.setMinSize(180, 25);
        txtPassword.setMaxSize(180, 50);
        txtPassword.setPromptText(objDictionaryAction.getWord("PROMPTPASSWORD"));
        final Tooltip passwordTT = new Tooltip();
        passwordTT.setText(objDictionaryAction.getWord("TOOLTIPPASSWORD"));
        passwordTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        txtPassword.setTooltip(passwordTT);
        bodyConrainer.add(txtPassword, 1, 3);

        Button btnLogin = new Button(objDictionaryAction.getWord("LOGIN"));
        btnLogin.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/login.png"));
        btnLogin.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPLOGIN")));
        btnLogin.setDefaultButton(true);
        btnLogin.setFocusTraversable(true);

        /**
         * Description: Adds "Register" button on Login Screen (UserLoginViewFull). When it is pressed User Registration stage pops up.
         * Date Added: 4 Feb 2017
         * Author: Aatif Ahmad Khan
         */
        Button btnRegister = new Button(objDictionaryAction.getWord("REGISTRATION"));
        btnRegister.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/profile.png"));
        btnRegister.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPREGISTRATION")));
        /**
         * Description:
         *  Adds "Forgot Password" button on Login Screen (UserLoginViewFull). When it is pressed Forgot Password Stage pops up.
         *  User is asked to enter "username" & "registered email".
         *  On clicking "Retrieve Password" button, these two fields are validated from database table "mla_users".
         *  After validation, an email containing Password Reset Steps is sent to registered email address using "send()" function in "com.mla.utility.Email" class.
         * Date Added: 8 Feb 2017
         * Author: Aatif Ahmad Khan
         */
        Button btnPassword = new Button(objDictionaryAction.getWord("FORGETPASSWORD"));
        btnPassword.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/forgot_password.png"));
        btnPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPFORGETPASSWORD")));

        btnLogin.setOnAction(new EventHandler<ActionEvent>() {
             @Override
            public void handle(ActionEvent e) {
                String a=txtUsername.getText();
                objUser.setStrUsername(a);
                String b=txtPassword.getText();
                //objUser.setStrPassword(Security.SecurePassword(b,a));
                objUser.setStrPassword(b);
                loginButtonAction();
            }
        });   
        btnRegister.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event) {
                userStage.close();
                System.gc();
                UserCheckRegistration objUserCheckRegistration = new UserCheckRegistration(objConfiguration);
            }
        });    
        btnPassword.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event) {
                userStage.close();
                System.gc();
                UserForgetPassword objUserForgetPassword = new UserForgetPassword(objConfiguration);
            }
        });
    
        HBox P_buttons = new HBox(10);
        P_buttons.setAlignment(Pos.BOTTOM_RIGHT);
        P_buttons.getChildren().addAll(btnLogin,btnRegister,btnPassword);
        bodyConrainer.add(P_buttons, 0, 4, 2, 1);
    }
    
    lblStatus = new Text();
    bodyConrainer.add(lblStatus, 0, 5, 2, 1);
    lblStatus.setId("actiontarget");
    
    final ImageView helpIV = new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png");
    Tooltip.install(helpIV, new Tooltip(objDictionaryAction.getWord("TOOLTIPHELP")));
    helpIV.setOnMouseEntered(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            helpIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
            helpIV.setScaleX(1.5);
            helpIV.setScaleY(1.5);
            helpIV.setScaleZ(1.5);
        }
    });
    helpIV.setOnMouseExited(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            helpIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
            helpIV.setScaleX(1);
            helpIV.setScaleY(1);
            helpIV.setScaleZ(1);
        }
    });
    helpIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            HelpView objHelpView = new HelpView(objConfiguration);
        }
    });
    final ImageView technicalIV = new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/technical_info.png");
    Tooltip.install(technicalIV, new Tooltip(objDictionaryAction.getWord("TOOLTIPTECHNICAL")));
    technicalIV.setOnMouseEntered(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            technicalIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/technical_info.png"));
            technicalIV.setScaleX(1.5);
            technicalIV.setScaleY(1.5);
            technicalIV.setScaleZ(1.5);
        }
    });
    technicalIV.setOnMouseExited(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            technicalIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/technical_info.png"));
            technicalIV.setScaleX(1);
            technicalIV.setScaleY(1);
            technicalIV.setScaleZ(1);
        }
    });
    technicalIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            TechnicalView objTechnicalView = new TechnicalView(objConfiguration);
        }
    });
    final ImageView aboutIV = new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/about_software.png");
    Tooltip.install(aboutIV, new Tooltip(objDictionaryAction.getWord("TOOLTIPABOUTUS")));
    aboutIV.setOnMouseEntered(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            aboutIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/about_software.png"));
            aboutIV.setScaleX(1.5);
            aboutIV.setScaleY(1.5);
            aboutIV.setScaleZ(1.5);
        }
    });
    aboutIV.setOnMouseExited(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            aboutIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/about_software.png"));
            aboutIV.setScaleX(1);
            aboutIV.setScaleY(1);
            aboutIV.setScaleZ(1);
        }
    });
    aboutIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            AboutView objAboutView = new AboutView(objConfiguration);
        }
    });
    final ImageView contactIV = new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/contact_us.png");
    Tooltip.install(contactIV, new Tooltip(objDictionaryAction.getWord("TOOLTIPCONTACTUS")));
    contactIV.setOnMouseEntered(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            contactIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/contact_us.png"));
            contactIV.setScaleX(1.5);
            contactIV.setScaleY(1.5);
            contactIV.setScaleZ(1.5);
        }
    });
    contactIV.setOnMouseExited(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            contactIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/contact_us.png"));
            contactIV.setScaleX(1);
            contactIV.setScaleY(1);
            contactIV.setScaleZ(1);
        }
    });
    contactIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            ContactView objContactView = new ContactView(objConfiguration);
        }
    });

    HBox topContainer = new HBox(10);
    topContainer.setAlignment(Pos.BOTTOM_CENTER);
    topContainer.getChildren().addAll(helpIV, technicalIV, aboutIV, contactIV);
    //bodyConrainer.add(P_links, 0, 7, 2, 1);
    
    root.setRight(closeIV); 
    root.setCenter(bodyConrainer);    
    root.setBottom(topContainer);
    
    userStage.getIcons().add(new Image("/media/icon.png"));
    userStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWLOGIN")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
    userStage.setScene(scene);
    userStage.show();
    //userStage.setX(-5);
    //userStage.setY(0);
            
    if(CONFIG.USERTYPE.equalsIgnoreCase("Guest")) {
        String a=CONFIG.USERNAME;
        objUser.setStrUsername(a);
        String b=CONFIG.PASSWORD;
        //objUser.setStrPassword(Security.SecurePassword(b,a));
        objUser.setStrPassword(b);
        loginButtonAction();
    }
 }
  
  private void loginButtonAction(){
      try {
        lblStatus.setFill(Color.FIREBRICK);
        lblStatus.setText(objDictionaryAction.getWord("ACTIONLOGIN"));
        if(objUser.getStrUsername()!="" && objUser.getStrPassword()!=""){
           UserAction objUserAction= new UserAction();
            if(objUserAction.verifyLogin(objUser)){
                objUserAction= new UserAction();
                if(!objUserAction.isUserLicenseIdLatest(objUser.getStrUserID())){  
                    updateUserLicenseTerms();
                    if(isTncOK){
                        userStage.close();
                        objWindowView = new WindowView(objConfiguration);
                    } else{
                        lblStatus.setText(objDictionaryAction.getWord("ACCEPTLICENSEAGREEMENT"));
                    }
                } else{
                    //objUser.setStrSeesionID(new IDGenerator().getIDGenerator("USER_TRACKING", objUser.getStrUserID()));
                    //objUserAction= new UserAction();
                    //objUserAction.trackUser(objUser, "LOGIN", "Log-In done", Security.getUserTrackDetails());
                    objConfiguration.setStrClothType("Body");
                    objUserAction= new UserAction();
                    objConfiguration.setObjUser(objUser);                         
                    objUserAction.getConfiguration(objConfiguration);
                    //objConfiguration.clothRepeat();
                    // added check for application activation
                    boolean isApplicationActivated = new UserAction().isApplicationActivated();
                    boolean isNewUserRegistered = new UserAction().isNewUserRegistered();
                    if(!isApplicationActivated && isNewUserRegistered && !(objUser.getStrUsername().equalsIgnoreCase("user") || objUser.getStrUsername().equalsIgnoreCase("admin"))){
                        // disallow login until activated (ALLOW for user "user" and "admin")
                        userStage.close();
                        new ActivationView(objConfiguration, false, true); // assuming user_status "New"
                        return;
                    }
                    System.gc();
                    userStage.close();
                    objWindowView = new WindowView(objConfiguration);
                }
                lblStatus.setText(objDictionaryAction.getWord("SUCCESS"));
            } else{
                lblStatus.setText(objDictionaryAction.getWord("INCORRELOGIN")+" , "+objDictionaryAction.getWord("TRYAGAIN"));
            } 
        } else{
            lblStatus.setText(objDictionaryAction.getWord("TRYAGAIN"));
        }
    } catch (SQLException ex) {
        new Logging("SEVERE",UserLoginView.class.getName(),"Error in login"+ex.toString(),ex);
        Logger.getLogger(UserLoginView.class.getName()).log(Level.SEVERE, null, ex);
        lblStatus.setText(objDictionaryAction.getWord("ERROR"));
    } catch (Exception ex) {
        new Logging("SEVERE",UserLoginView.class.getName(),ex.toString(),ex);
        Logger.getLogger(UserLoginView.class.getName()).log(Level.SEVERE, null, ex);
        lblStatus.setText(objDictionaryAction.getWord("ERROR"));
    }
  }
  
    private boolean isTncOK = false;
    private void updateUserLicenseTerms(){
        final Stage primaryStage=new Stage();
        primaryStage.initModality(Modality.APPLICATION_MODAL);//WINDOW_MODAL
        primaryStage.initStyle(StageStyle.UTILITY); 
        primaryStage.setTitle(objDictionaryAction.getWord("LICENSEAGREEMENTTITLE"));
        GridPane bodyConrainer=new GridPane();
        bodyConrainer.setVgap(10);
        bodyConrainer.setHgap(5);
        Label caption=new Label(objDictionaryAction.getWord("LICENSEAGREEMENTCAPTION"));
        bodyConrainer.add(caption, 0, 0, 2, 1);
        WebView license=new WebView();
        license.setPrefSize(600, 440);
        license.setCursor(Cursor.DEFAULT);
        //TextArea license=new TextArea();
        //license.setPrefColumnCount(50);
        //license.setPrefRowCount(15);
        //license.setWrapText(true);
        //license.setEditable(false);
        try{
            UserAction objUserAction=new UserAction();
            license.getEngine().loadContent(new Scanner(objUserAction.getLatestLicenseContent()).useDelimiter("\\Z").next());
            //license.getEngine().load("file:///"+objUserAction.getLatestLicenseContent().getAbsolutePath());
            //latestTnCId=objUserAction.getLatestLicenseId();
        } catch(SQLException ex){
            new Logging("ERROR", UserLoginView.class.getName(), "Error fetching database data"+ex.getMessage(), ex);
            Logger.getLogger(UserLoginView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            new Logging("ERROR", UserLoginView.class.getName(), "Error fetching database data"+ex.getMessage(), ex);
            Logger.getLogger(UserLoginView.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        bodyConrainer.add(license, 0, 1, 2, 1);
        final CheckBox chkAccept=new CheckBox(objDictionaryAction.getWord("AGREELICENSETERMS"));
        bodyConrainer.add(chkAccept, 0, 2);
        final Label message=new Label();
        bodyConrainer.add(message, 0, 4, 2, 1);
        
        Button btnAccept=new Button(objDictionaryAction.getWord("ACCEPT"));
        btnAccept.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event) {
                if(chkAccept.isSelected()){
                    try {
                        UserAction objUserAction = new UserAction();
                        objUserAction.addUserTnC(objUser.getStrUserID());
                        //objUser.setStrSeesionID(new IDGenerator().getIDGenerator("USER_TRACKING", objUser.getStrUserID()));
                        //objUserAction= new UserAction();
                        //objUserAction.trackUser(objUser, "LOGIN", "Log-In done", Security.getUserTrackDetails());
                        objConfiguration.setStrClothType("Body");
                        objUserAction= new UserAction();
                        objConfiguration.setObjUser(objUser);                         
                        objUserAction.getConfiguration(objConfiguration);
                        //objConfiguration.clothRepeat();
                        System.gc();
                        primaryStage.close();
                        isTncOK = true;
                        
                    } catch (SQLException ex) {
                        new Logging("SEVERE",UserLoginView.class.getName(),"Logout Tracking",ex);
                        Logger.getLogger(UserLoginView.class.getName()).log(Level.SEVERE, null, ex);
            
                    }
                }
                else{
                    message.setText(objDictionaryAction.getWord("ACCEPTLICENSEAGREEMENT"));
                }
            }
        });
        bodyConrainer.add(btnAccept, 0, 3);
        
        Button btnCancel=new Button(objDictionaryAction.getWord("SKIP"));
        btnCancel.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event) {
                /*try {
                    UserAction objUserAction= new UserAction();
                    objUserAction.trackUser(objConfiguration.getObjUser(), "LOGOUT", "Log-Out done", null);                    
                    objConfiguration.setObjUser(null);
                } catch (SQLException ex) {
                    new Logging("SEVERE",UserLoginView.class.getName(),"Logout Tracking",ex);
                }*/
                primaryStage.close();
                System.gc();
                isTncOK = false;
            }
        });
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                /*try {
                    UserAction objUserAction= new UserAction();
                    objUserAction.trackUser(objConfiguration.getObjUser(), "LOGOUT", "Log-Out done", null);                    
                    objConfiguration.setObjUser(null);
                } catch (SQLException ex) {
                    new Logging("SEVERE",UserLoginView.class.getName(),"Logout Tracking",ex);
                }*/
                primaryStage.close();
                System.gc();
                isTncOK = false;
            }
        });
        
        bodyConrainer.add(btnCancel, 1, 3);
        BorderPane root=new BorderPane();
        root.setCenter(bodyConrainer);
        Scene scene=new Scene(root, 500, 400, Color.WHITE);
        scene.getStylesheets().add(UserLoginView.class.getResource("/media/login.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.setIconified(false);
        primaryStage.setResizable(false);
        primaryStage.showAndWait();
    }
  
  @Override
   public void start(Stage stage) throws Exception {
        new UserLoginView(stage); 
        new Logging("WARNING",UserLoginView.class.getName(),"UnsupportedOperationException",new UnsupportedOperationException("Not supported yet."));
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}