/*
 * Copyright (C) Digital India Corporation (Media Lab Asia)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mla.user;

import com.mla.dictionary.DictionaryAction;
import com.mla.main.Configuration;
import com.mla.main.Logging;
import com.mla.main.MessageView;
import com.mla.main.WebServiceInvoker;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.sql.SQLException;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * Validates & fetch registration details if available on DigiBunai Web Portal
 * @author Aatif Ahmad Khan
 */
public class UserCheckRegistration {
    
    public static Stage checkRegistrationStage;
    BorderPane root;
    
    Configuration objConfiguration = null;
    DictionaryAction objDictionaryAction = null;
    GridPane bodyContainer;
    String userEmail;
    String response; // valid user details (if registered) or null
    
    // URL for Check Already Registered User - Web Service
    private String strActivationServerUrl = "https://stagingdigibunai.digitalindiacorporation.in"; // can be configured /mla/activationserver.txt
    private final String strCheckRegisteredUserUrl = "/license_key_api/userData_api.php";
        
    public UserCheckRegistration(Configuration objConfigurationCall){
        objConfiguration = objConfigurationCall;
        objDictionaryAction = new DictionaryAction(objConfiguration);

        checkRegistrationStage = new Stage();
        checkRegistrationStage.initStyle(StageStyle.TRANSPARENT);
        checkRegistrationStage.setResizable(false);
        checkRegistrationStage.setIconified(false);
        checkRegistrationStage.setFullScreen(false);
    
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root);
        scene.getStylesheets().add(UserCheckRegistration.class.getResource("/media/login.css").toExternalForm());
        root.setId("borderpane"); 
        
        try {
            if(!new UserAction().isApplicationActivated()){
                // check internet connectivity, if not connected: return
                boolean isInternetConnected=isWebsiteReachable("google.com", 80)||isWebsiteReachable("bing.com", 80);
                if(!isInternetConnected){
                    new MessageView("error", objDictionaryAction.getWord("NOINTERNET"), objDictionaryAction.getWord("NOINTERNETACTIVATION"));
                    new Logging("SEVERE",UserCheckRegistration.class.getName(),"No Internet Connection.",null);
                    checkRegistrationStage.close();
                    return;
                }
            }
        } catch (SQLException ex) {
            new Logging("SEVERE",UserCheckRegistration.class.getName(),"Check Application Activated",ex);
        }
        
        // load activation server url if configured at /mla/activationserver.txt
        InputStream resourceAsStream;
        try {
            resourceAsStream = new FileInputStream(System.getProperty("user.dir")+"/mla/activationserver.txt");
            if (resourceAsStream != null) {
                byte[] b = new byte[256];
                resourceAsStream.read(b);
                strActivationServerUrl=new String(b).trim();
            }
        } catch (IOException ex) {
            new Logging("SEVERE",UserCheckRegistration.class.getName(),"read activation server url",ex);
        }

        final ImageView closeIV = new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png");
        Tooltip.install(closeIV, new Tooltip(objDictionaryAction.getWord("TOOLTIPCLOSE")));    
        closeIV.setOnMouseEntered((MouseEvent me) -> {
            closeIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            closeIV.setScaleX(1.5);
            closeIV.setScaleY(1.5);
            closeIV.setScaleZ(1.5);
        });
        closeIV.setOnMouseExited((MouseEvent me) -> {
            closeIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            closeIV.setScaleX(1);
            closeIV.setScaleY(1);
            closeIV.setScaleZ(1);
        });
        closeIV.setOnMouseClicked((MouseEvent me) -> {
            checkRegistrationStage.close();
        });

        bodyContainer = new GridPane();   
        bodyContainer.setVgap(3);
        bodyContainer.setHgap(3);
        bodyContainer.setId("bodyContainer");
        bodyContainer.setAlignment(Pos.CENTER);
        bodyContainer.setMaxSize(350, 200);
        bodyContainer.relocate(10, 10);
        
        Label lblCaption = new Label(objDictionaryAction.getWord("PORTALREGISTERED"));
        bodyContainer.add(lblCaption, 0, 0, 2, 1);
        
        final ToggleGroup userTG = new ToggleGroup();
        RadioButton registeredUserRB = new RadioButton(objDictionaryAction.getWord("REGISTERED")+" "+objDictionaryAction.getWord("USER"));
        registeredUserRB.setToggleGroup(userTG);
        bodyContainer.add(registeredUserRB, 0, 1, 2, 1);
        RadioButton newUserRB = new RadioButton(objDictionaryAction.getWord("NEW")+" "+objDictionaryAction.getWord("USER"));
        newUserRB.setToggleGroup(userTG);
        bodyContainer.add(newUserRB, 0, 4, 2, 1);
        userTG.selectToggle(newUserRB);
        
        final TextField txtEmail = new TextField();
        txtEmail.setMinSize(180, 25);
        txtEmail.setMaxSize(180, 50);
        txtEmail.setPrefColumnCount(20);
        txtEmail.setDisable(!registeredUserRB.isSelected());
        txtEmail.setPromptText(objDictionaryAction.getWord("PROMPTEMAILID"));
        Tooltip emailTT = new Tooltip();
        emailTT.setText(objDictionaryAction.getWord("TOOLTIPEMAILID"));
        emailTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        txtEmail.setTooltip(emailTT);
        bodyContainer.add(txtEmail, 0, 2, 1, 1);
        
        Button btnContinueRegistered = new Button(objDictionaryAction.getWord("SUBMIT"));
        btnContinueRegistered.setDisable(!registeredUserRB.isSelected());
        bodyContainer.add(btnContinueRegistered, 1, 2, 1, 1);
        
        Separator hSeparator = new Separator(Orientation.HORIZONTAL);
        bodyContainer.add(hSeparator, 0, 3, 2, 1);
        
        Button btnContinueNew = new Button(objDictionaryAction.getWord("REGISTRATION"));
        btnContinueNew.setDisable(registeredUserRB.isSelected());
        bodyContainer.add(btnContinueNew, 0, 5, 1, 1);
        
        final Text lblStatus = new Text();
        bodyContainer.add(lblStatus, 0, 6, 2, 1);
        lblStatus.setId("actiontarget");
        
        userTG.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) -> {
            txtEmail.setDisable(!registeredUserRB.isSelected());
            btnContinueRegistered.setDisable(!registeredUserRB.isSelected());
            btnContinueNew.setDisable(registeredUserRB.isSelected());
        });
        
        btnContinueRegistered.setOnAction((ActionEvent event) -> {
            if(txtEmail.getText().trim().length()>0){
                userEmail = txtEmail.getText().trim();
                // Invoke CheckRegisteredUser Web Service with request data
                final String strJsonData = getJsonRequestUserData();
                WebServiceInvoker objWebServiceInvoker = new WebServiceInvoker(strActivationServerUrl+strCheckRegisteredUserUrl, strJsonData, "POST");
                if(populateUserDataFromJson(objWebServiceInvoker.strResponse)){
                    checkRegistrationStage.close();
                    UserRegistration objUserRegistration = new UserRegistration(objConfiguration);
                }
            }
            else {
                lblStatus.setFill(Color.FIREBRICK);
                lblStatus.setText(objDictionaryAction.getWord("INVALIDDATA"));
            }
        });
        
        btnContinueNew.setOnAction((ActionEvent event) -> {
            objConfiguration.getObjUser().setStrEmailID(null); // user_status "New"
            checkRegistrationStage.close();
            UserRegistration objUserRegistration = new UserRegistration(objConfiguration);
        });
        
        root.setRight(closeIV); 
        root.setCenter(bodyContainer);
        
        checkRegistrationStage.getIcons().add(new Image("/media/icon.png"));
        checkRegistrationStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWLOGIN")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
        checkRegistrationStage.setScene(scene);
        checkRegistrationStage.show();
    }
    
    private boolean isWebsiteReachable(String website, int port){
        Socket socket=new Socket();
        InetSocketAddress socketAddress=new InetSocketAddress(website, port);
        try{
            socket.connect(socketAddress, 5000); // timeout in milliseconds
            return true;
        }
        catch(IOException ioEx){
            new Logging("SEVERE",UserCheckRegistration.class.getName(),"isWebsiteReachable():",ioEx);
            return false;
        }
        finally{
            try{
                socket.close();
            }catch(IOException ioEx){
                new Logging("SEVERE",UserCheckRegistration.class.getName(),"isWebsiteReachable(): Socket Close",ioEx);
            }
        }
    }
    
    String getJsonRequestUserData(){
         return "{\"email\":\"" + userEmail + "\"}";
    }
    
    boolean populateUserDataFromJson(String jsonResponse){
        if(jsonResponse == null)
            return false;
        if(jsonResponse.contains("Email not Found")){
            new MessageView("error", "Email not Found", "Email not Found. Please enter registered email.");
            return false;
        }
        try{
            JSONParser parser=new JSONParser();
            JSONObject parsed = (JSONObject)parser.parse(jsonResponse);
            JSONObject userData = (JSONObject)parsed.get("data");
            objConfiguration.getObjUser().setStrName((String)userData.get("name"));
            objConfiguration.getObjUser().setStrGender((String)userData.get("gender"));
            objConfiguration.getObjUser().setStrContactNumber((String)userData.get("contact_number"));
            objConfiguration.getObjUser().setStrCountryAddress((String)userData.get("country"));
            objConfiguration.getObjUser().setStrStateAddress((String)userData.get("state"));
            objConfiguration.getObjUser().setStrCityAddress((String)userData.get("city"));
            objConfiguration.getObjUser().setStrEmailID(userEmail);
            objConfiguration.getObjUser().setStrUsername((String)userData.get("username"));
            return true;
        }
        catch(Exception exc){
            new Logging("SEVERE",UserCheckRegistration.class.getName(),"populateUserDataFromJson(): Parsing JSON response",exc);
            return false;
        }
    }
}
