/*
 * Copyright (C) Digital India Corporation (Media Lab Asia)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.mla.user;

import com.mla.dictionary.DictionaryAction;
import com.mla.main.ActivationView;
import com.mla.main.Configuration;
import static com.mla.main.DigiBunai.Constants.CONFIG;
import com.mla.main.IDGenerator;
import com.mla.main.Logging;
import com.mla.secure.Security;
import com.mla.utility.AboutView;
import com.mla.utility.ContactView;
import com.mla.utility.HelpView;
import com.mla.utility.TechnicalView;
import com.sun.javafx.scene.control.skin.ComboBoxListViewSkin;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import javafx.util.StringConverter;
import org.json.simple.*;
import org.json.simple.parser.JSONParser;
/**
 * Description:
 *  -Generates User Registration UI Stage.
 *  -Validation of all entered user details.
 *  -Inserts user details into database table "mla_users"
 *  -Inserts data in related tables "mla_user_usergroup_map", "mla_user_configuration", "mla_user_preference" & "mla_user_access"
 * Date Added: 3 Feb 2017
 * @author Aatif Ahmad Khan
 * @version 0.9.2
 * @author Amit Kumar Singh
 */
public class UserRegistration {
    
    public static Stage userStage;
    BorderPane root;
    
    User objUser;
    Configuration objConfiguration = null;
    DictionaryAction objDictionaryAction = null;
    private Text lblStatus;
    private Button btnRegister;
    // comboboxes for showing drop down lists of countries, states and cities
    private ComboBox<Label> countryCB= new ComboBox<>();
    private ComboBox<Label> stateCB= new ComboBox<>();
    private ComboBox<Label> cityCB= new ComboBox<>();
    
    // lists of contries, states and cities to be populated in respective comboboxes
    List lstCountry;
    List lstState;
    List lstCity;
    
    // number of results (different locations) returned by Google Maps Geo Location API call for a queried location
    int resultCount;
    // array of geo locations (formatted_addresses) returned by Google Maps Geo Location API call for a queried location
    String[] geoLocations;
    // array of latitudes and longitudes returned by Google Maps Geo Location API call for a queried location
    double[] lats, lngs;
    // indices of selected country & state in countryCB & stateCB respectively
    // USAGE: state list needs to be populated based on selected country, and city list based on state selected
    int intcountry=0, intstate=0;
    
    // whether user is registed on DigiBunai web portal or not ("Registered" or "New")
    boolean user_status;
        
    public UserRegistration(Configuration objConfigurationCall) {
        objUser = new User();
        objConfiguration = objConfigurationCall;
        objDictionaryAction = new DictionaryAction(objConfiguration);
        
        // if user details are fetched successfully using userdata API:
        // User.getStrEmail() should be populated
        user_status = objConfiguration.getObjUser().getStrEmail()!= null;

        userStage = new Stage();
        //userStage.initModality(Modality.APPLICATION_MODAL);//WINDOW_MODAL
        //userStage.initStyle(StageStyle.UNDECORATED);
        userStage.initStyle(StageStyle.TRANSPARENT);
        userStage.setResizable(false);
        userStage.setIconified(false);
        userStage.setFullScreen(false);
    
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root);
        scene.getStylesheets().add(UserLoginView.class.getResource("/media/login.css").toExternalForm());
        root.setId("borderpane"); 
    
        final ImageView closeIV = new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png");
        Tooltip.install(closeIV, new Tooltip(objDictionaryAction.getWord("TOOLTIPCLOSE")));    
        closeIV.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                closeIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
                closeIV.setScaleX(1.5);
                closeIV.setScaleY(1.5);
                closeIV.setScaleZ(1.5);
            }
        });
        closeIV.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                closeIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
                closeIV.setScaleX(1);
                closeIV.setScaleY(1);
                closeIV.setScaleZ(1);
            }
        });
        closeIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                userStage.close();
            }
        });

        GridPane bodyContainer = new GridPane();   
        bodyContainer.setId("bodyContainer");
        bodyContainer.getColumnConstraints().add(new ColumnConstraints(200)); // column 1 is 200 wide
        bodyContainer.getColumnConstraints().add(new ColumnConstraints(180)); // column 2 is 180 wide
        bodyContainer.getColumnConstraints().add(new ColumnConstraints(30)); // column 3 is 30 wide
        bodyContainer.getColumnConstraints().add(new ColumnConstraints(200)); // column 4 is 200 wide
        bodyContainer.getColumnConstraints().add(new ColumnConstraints(180)); // column 5 is 180 wide
        bodyContainer.getColumnConstraints().add(new ColumnConstraints(30)); // column 6 is 30 wide
        bodyContainer.setAlignment(Pos.CENTER);
        //bodyContainer.setMaxSize(350, 200);
        bodyContainer.relocate(10, 10);
        //bodyContainer.setLayoutX(30);
        //bodyContainer.setLayoutY(10);

        Text projectTitle = new Text(objDictionaryAction.getWord("TRADEMARK"));
        projectTitle.setFont(Font.font(objConfiguration.getStrBFont(), FontWeight.NORMAL, objConfiguration.getIntBFontSize()));
        projectTitle.setId("welcome-text");
        bodyContainer.add(projectTitle, 0, 0, 6, 1);

        Label versionTitle = new Label(objDictionaryAction.getWord("VERSIONINFO"));
        versionTitle.setId("version-text");
        bodyContainer.add(versionTitle, 0, 1, 6, 1);
        
        // Label for Name, its corresponding TextField, and validation Label
        Label lblName = new Label(objDictionaryAction.getWord("NAME")+ ":");
        lblName.setId("name");
        bodyContainer.add(lblName, 0, 2);
        final TextField txtName = new TextField();
        txtName.setMinSize(180, 25);
        txtName.setMaxSize(180, 50);
        txtName.setPrefColumnCount(30);
        txtName.setPromptText(objDictionaryAction.getWord("PROMPTNAME"));
        Tooltip nameTT = new Tooltip();
        nameTT.setText(objDictionaryAction.getWord("TOOLTIPNAME"));
        nameTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        txtName.setTooltip(nameTT);
        bodyContainer.add(txtName, 1, 2);
        final Label msgName= new Label("");
        msgName.setGraphic(new ImageView("/media/incorrect.png"));
        msgName.setTooltip(nameTT);
        msgName.setId("error");
        bodyContainer.add(msgName, 2, 2);
        txtName.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                // validate name when name TextField loses focus
                if(!newValue){ 
                    if(txtName.getText().trim().length()==0){ // name field empty
                        msgName.setId("error");
                        msgName.setGraphic(new ImageView("/media/incorrect.png"));
                        msgName.setTooltip(new Tooltip(objDictionaryAction.getWord("BALNKINPUT")));
                    } else if(!txtName.getText().toLowerCase().trim().matches("^[\\p{L} .'-]+$")){ // name field should have valid characters
                        msgName.setId("error");
                        msgName.setGraphic(new ImageView("/media/incorrect.png"));
                        msgName.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPNAME")));
                    } else {   // valid name
                        updateSuccessMarker(msgName);
                    }
                }
            }
        });
        // Label for UserName, its corresponding TextField, and validation Label
        Label lblUsername = new Label(objDictionaryAction.getWord("USERNAME")+ ":");
        lblUsername.setId("username");
        bodyContainer.add(lblUsername, 3, 2);
        final TextField txtUsername = new TextField();
        txtUsername.setMinSize(180, 25);
        txtUsername.setMaxSize(180, 50);
        txtUsername.setPrefColumnCount(20);
        txtUsername.setPromptText(objDictionaryAction.getWord("PROMPTUSERNAME"));
        Tooltip usernameTT = new Tooltip();
        usernameTT.setText(objDictionaryAction.getWord("TOOLTIPUSERNAME"));
        usernameTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        txtUsername.setTooltip(usernameTT);
        bodyContainer.add(txtUsername, 4, 2);
        final Label msgUsername= new Label("");
        msgUsername.setGraphic(new ImageView("/media/incorrect.png"));
        msgUsername.setTooltip(usernameTT);
        msgUsername.setId("error");
        bodyContainer.add(msgUsername, 5, 2);
        txtUsername.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                // validate unique username when username TextField loses focus
                if(!newValue){
                    if(txtUsername.getText().trim().equalsIgnoreCase("")){               // empty field
                        msgUsername.setId("error");
                        msgUsername.setGraphic(new ImageView("/media/incorrect.png"));
                        msgUsername.setTooltip(new Tooltip(objDictionaryAction.getWord("BALNKINPUT")));
                    } else {
                        try{ // query database table "mla_users" to validate unique username
                            if(new UserAction().isUsernameExisting(txtUsername.getText())){
                                updateSuccessMarker(msgUsername);// valid & unique username
                            } else{    // existing username found in database table "mla_users"
                                msgUsername.setId("error");
                                msgUsername.setGraphic(new ImageView("/media/incorrect.png"));
                                msgUsername.setTooltip(new Tooltip(objDictionaryAction.getWord("USEREXIST")));
                            }
                        } catch(Exception ex){
                            new Logging("SEVERE",UserRegistration.class.getName(),"Error in validating unique username from database",ex);
                        }
                    }
                }
            }
        });
        // Label for Password, its corresponding TextField, and validation Label
        Label lblPassword= new Label(objDictionaryAction.getWord("PASSWORD"));
        lblPassword.setId("password");
        bodyContainer.add(lblPassword, 0, 3);
        final PasswordField txtPassword = new PasswordField();
        txtPassword.setMinSize(180, 25);
        txtPassword.setMaxSize(180, 50);
        txtPassword.setPrefColumnCount(20);
        txtPassword.setPromptText(objDictionaryAction.getWord("PROMPTPASSWORD"));
        Tooltip passwordTT = new Tooltip();
        passwordTT.setText(objDictionaryAction.getWord("TOOLTIPPASSWORD"));
        passwordTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        txtPassword.setTooltip(passwordTT);
        bodyContainer.add(txtPassword, 1, 3);
        final Label msgPassword= new Label("");
        msgPassword.setGraphic(new ImageView("/media/incorrect.png"));
        msgPassword.setTooltip(passwordTT);
        msgPassword.setId("error");
        bodyContainer.add(msgPassword, 2, 3);
        txtPassword.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                // validating password when password PasswordField loses focus
                if(!newValue){  
                    if(txtPassword.getText().trim().equalsIgnoreCase("")){
                        msgPassword.setId("error");
                        msgPassword.setGraphic(new ImageView("/media/incorrect.png"));
                        msgPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("BALNKINPUT")));
                    } else if(txtPassword.getText().length()<6){ // smaller length password are weak but acceptable
                        msgPassword.setId("error");
                        msgPassword.setGraphic(new ImageView("/media/incorrect.png"));
                        msgPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("WEEKPASSWORD")));
                    } 
                    /* else if(!isValidPassword(txtName.getText(), txtUsername.getText(), txtPassword.getText())){ // smaller length password are weak but acceptable
                        msgPassword.setId("error");
                        msgPassword.setGraphic(new ImageView("/media/incorrect.png"));
                        msgPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("MEDIUMPASSWORD")));
                    } */
                    else {  // password ok
                        msgPassword.setId("success");
                        msgPassword.setGraphic(new ImageView("/media/correct.png"));
                        msgPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("STRONGPASSWORD")));
                    }
                }
            }
        }); 
        // Label for Confirm Password, its corresponding TextField, and validation Label
        Label lblRePassword= new Label(objDictionaryAction.getWord("CONFIRMPASSWORD"));
        lblRePassword.setId("password");
        bodyContainer.add(lblRePassword, 3, 3);
        final PasswordField txtRePassword = new PasswordField();
        txtRePassword.setMinSize(180, 25);
        txtRePassword.setMaxSize(180, 50);
        txtRePassword.setPrefColumnCount(20);
        txtRePassword.setText("");
        txtRePassword.setPromptText(objDictionaryAction.getWord("PROMPTCONFIRMPASSWORD"));
        txtRePassword.setTooltip(passwordTT);
        bodyContainer.add(txtRePassword, 4, 3);
        final Label msgRePassword= new Label("");
        msgRePassword.setGraphic(new ImageView("/media/incorrect.png"));
        msgRePassword.setTooltip(passwordTT);
        msgRePassword.setId("error");
        bodyContainer.add(msgRePassword, 5, 3);
        txtRePassword.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                // validate Confirm Password field when rePassword Password Field loses focus
                if(!newValue){  
                    if(txtRePassword.getText().trim().equalsIgnoreCase("")){
                        msgRePassword.setId("error");
                        msgRePassword.setGraphic(new ImageView("/media/incorrect.png"));
                        msgRePassword.setTooltip(new Tooltip(objDictionaryAction.getWord("BALNKINPUT")));
                    } else if(!(txtRePassword.getText().equals(txtPassword.getText()))){
                        msgRePassword.setId("error");
                        msgRePassword.setGraphic(new ImageView("/media/incorrect.png"));
                        msgRePassword.setTooltip(new Tooltip(objDictionaryAction.getWord("NOMATCHPASSWORD")));
                    } else {
                        msgRePassword.setId("success");
                        msgRePassword.setGraphic(new ImageView("/media/correct.png"));
                        msgRePassword.setTooltip(new Tooltip(objDictionaryAction.getWord("STRONGPASSWORD")));
                    }
                }
            }
        });
        /*// Label for Password, its corresponding TextField, and validation Label
        Label lblAppPassword= new Label(objDictionaryAction.getWord("SERVICEPASSWORD"));
        lblAppPassword.setId("password");
        bodyContainer.add(lblAppPassword, 0, 4);
        final PasswordField txtAppPassword = new PasswordField();
        txtAppPassword.setMinSize(180, 25);
        txtAppPassword.setMaxSize(180, 50);
        txtAppPassword.setPrefColumnCount(20);
        txtAppPassword.setText("");
        txtAppPassword.setPromptText(objDictionaryAction.getWord("PROMPTSERVICEPASSWORD"));
        txtAppPassword.setTooltip(passwordTT);
        bodyContainer.add(txtAppPassword, 1, 4);
        final Label msgAppPassword= new Label("");
        msgAppPassword.setGraphic(new ImageView("/media/incorrect.png"));
        msgAppPassword.setTooltip(passwordTT);
        msgAppPassword.setId("error");
        bodyContainer.add(msgAppPassword, 2, 4);
        txtAppPassword.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                // validating password when password PasswordField loses focus
                if(!newValue){  
                    if(txtAppPassword.getText().trim().equalsIgnoreCase("")){
                        msgAppPassword.setId("error");
                        msgAppPassword.setGraphic(new ImageView("/media/incorrect.png"));
                        msgAppPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("BALNKINPUT")));
                    } else if(txtAppPassword.getText().length()<6){ // smaller length password are weak but acceptable
                        msgAppPassword.setId("error");
                        msgAppPassword.setGraphic(new ImageView("/media/incorrect.png"));
                        msgAppPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("WEEKPASSWORD")));
                    } else if(!isValidPassword(txtName.getText(), txtUsername.getText(), txtAppPassword.getText())){ // smaller length password are weak but acceptable
                        msgAppPassword.setId("error");
                        msgAppPassword.setGraphic(new ImageView("/media/incorrect.png"));
                        msgAppPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("MEDIUMPASSWORD")));
                    } else {  // password ok
                        msgAppPassword.setId("success");
                        msgAppPassword.setGraphic(new ImageView("/media/correct.png"));
                        msgAppPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("STRONGPASSWORD")));
                    }
                }
            }
        }); 
        // Label for Confirm Password, its corresponding TextField, and validation Label
        Label lblReAppPassword= new Label(objDictionaryAction.getWord("CONFIRMPASSWORD"));
        lblReAppPassword.setId("password");
        bodyContainer.add(lblReAppPassword, 3, 4);
        final PasswordField txtReAppPassword = new PasswordField();
        txtReAppPassword.setMinSize(180, 25);
        txtReAppPassword.setMaxSize(180, 50);
        txtReAppPassword.setPrefColumnCount(20);
        txtReAppPassword.setText("");
        txtReAppPassword.setPromptText(objDictionaryAction.getWord("PROMPTCONFIRMPASSWORD"));
        txtReAppPassword.setTooltip(passwordTT);
        bodyContainer.add(txtReAppPassword, 4, 4);
        final Label msgReAppPassword= new Label("");
        msgReAppPassword.setGraphic(new ImageView("/media/incorrect.png"));
        msgReAppPassword.setTooltip(passwordTT);
        msgReAppPassword.setId("error");
        bodyContainer.add(msgReAppPassword, 5, 4);
        txtReAppPassword.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                // validate Confirm Password field when rePassword Password Field loses focus
                if(!newValue){  
                    if(txtReAppPassword.getText().trim().equalsIgnoreCase("")){
                        msgReAppPassword.setId("error");
                        msgReAppPassword.setGraphic(new ImageView("/media/incorrect.png"));
                        msgReAppPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("BALNKINPUT")));
                    } else if(!(txtReAppPassword.getText().equals(txtAppPassword.getText()))){
                        msgReAppPassword.setId("error");
                        msgReAppPassword.setGraphic(new ImageView("/media/incorrect.png"));
                        msgReAppPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("NOMATCHPASSWORD")));
                    } else {
                        msgReAppPassword.setId("success");
                        msgReAppPassword.setGraphic(new ImageView("/media/correct.png"));
                        msgReAppPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("STRONGPASSWORD")));
                    }
                }
            }
        });*/
        // Label for Email, its corresponding TextField, and validation Label
        Label lblEmail = new Label(objDictionaryAction.getWord("EMAILID")+ ":");
        lblEmail.setId("email");
        bodyContainer.add(lblEmail, 0, 5);
        final TextField txtEmail = new TextField();
        txtEmail.setMinSize(180, 25);
        txtEmail.setMaxSize(180, 50);
        txtEmail.setPrefColumnCount(20);
        txtEmail.setPromptText(objDictionaryAction.getWord("PROMPTEMAILID"));
        Tooltip emailTT = new Tooltip();
        emailTT.setText(objDictionaryAction.getWord("TOOLTIPEMAILID"));
        emailTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        txtEmail.setTooltip(emailTT);
        bodyContainer.add(txtEmail, 1, 5);
        final Label msgEmail= new Label("");
        msgEmail.setGraphic(new ImageView("/media/incorrect.png"));
        msgEmail.setTooltip(emailTT);
        msgEmail.setId("error");
        bodyContainer.add(msgEmail, 2, 5);
        txtEmail.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(!newValue){
                    if(txtEmail.getText().trim().equalsIgnoreCase("")){               // empty field
                        msgEmail.setId("error");
                        msgEmail.setGraphic(new ImageView("/media/incorrect.png"));
                        msgEmail.setTooltip(new Tooltip(objDictionaryAction.getWord("BALNKINPUT")));
                    } else if(!isValidEmailAddress(txtEmail.getText())){
                        msgEmail.setId("error");
                        msgEmail.setGraphic(new ImageView("/media/incorrect.png"));
                        msgEmail.setTooltip(new Tooltip(objDictionaryAction.getWord("INVALIDEMAILID")));
                    } else {
                        updateSuccessMarker(msgEmail);
                    }
                }
            }
        });
        // Label for Mobile, its corresponding TextField, and validation Label
        Label lblContact= new Label(objDictionaryAction.getWord("CONTACTNUMBER"));
        lblContact.setId("contact");
        bodyContainer.add(lblContact, 3, 5);
        final TextField txtContact = new TextField();
        txtContact.setMinSize(180, 25);
        txtContact.setMaxSize(180, 50);
        txtContact.setPrefColumnCount(20);
        txtContact.setPromptText(objDictionaryAction.getWord("PROMPTCONTACTNUMBER"));
        Tooltip contactTT = new Tooltip();
        contactTT.setText(objDictionaryAction.getWord("TOOLTIPCONTACTNUMBER"));
        contactTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        txtContact.setTooltip(contactTT);
        bodyContainer.add(txtContact, 4, 5);
        final Label msgContact= new Label("");
        msgContact.setGraphic(new ImageView("/media/incorrect.png"));
        msgContact.setTooltip(contactTT);
        msgContact.setId("error");
        bodyContainer.add(msgContact, 5, 5);
        txtContact.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(!newValue){
                    if(txtContact.getText().trim().equalsIgnoreCase("")){               // empty field
                        msgContact.setId("error");
                        msgContact.setGraphic(new ImageView("/media/incorrect.png"));
                        msgContact.setTooltip(new Tooltip(objDictionaryAction.getWord("BALNKINPUT")));
                    } else if(!isValidContactNumber(txtContact.getText())){
                        msgContact.setId("error");
                        msgContact.setGraphic(new ImageView("/media/incorrect.png"));
                        msgContact.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCONTACTNUMBER")));
                    } else {
                        updateSuccessMarker(msgContact);
                    }
                }
            }
        });
        // Label for Gender, its corresponding ComboBox, and validation Label
        Label gender= new Label(objDictionaryAction.getWord("GENDER")+" :");
        bodyContainer.add(gender, 0, 6);
        final ComboBox genderCB = new ComboBox();
        genderCB.getItems().addAll("Select", "M","F","O");   
        genderCB.setValue("Select");   
        genderCB.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPGENDER")));
        bodyContainer.add(genderCB, 1, 6);
        final Label msgGender= new Label("");
        msgGender.setGraphic(new ImageView("/media/incorrect.png"));
        msgGender.setId("error");
        bodyContainer.add(msgGender, 2, 6);
        genderCB.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(!newValue){
                    if(genderCB.getSelectionModel().getSelectedItem()=="Select"){               // empty field
                        msgGender.setId("error");
                        msgGender.setGraphic(new ImageView("/media/incorrect.png"));
                        msgGender.setTooltip(new Tooltip(objDictionaryAction.getWord("BALNKINPUT")));
                    } else {
                        updateSuccessMarker(msgGender);
                    }
                }
            }
        });
        /*// Label for Address, its corresponding TextField, and validation Label
        Label lblAddress= new Label(objDictionaryAction.getWord("ADDRESS"));
        lblAddress.setId("address");
        bodyContainer.add(lblAddress, 0, 6);
        final TextField txtAddress = new TextField();
        txtAddress.setMinSize(180, 25);
        txtAddress.setMaxSize(180, 50);
        txtAddress.setPrefColumnCount(20);
        txtAddress.setPromptText(objDictionaryAction.getWord("PROMPTADDRESS"));
        Tooltip addressTT = new Tooltip();
        addressTT.setText(objDictionaryAction.getWord("TOOLTIPADDRESS"));
        addressTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        txtAddress.setTooltip(addressTT);
        bodyContainer.add(txtAddress, 1, 6);
        final Label msgAddress= new Label("");
        msgAddress.setGraphic(new ImageView("/media/incorrect.png"));
        msgAddress.setTooltip(addressTT);
        msgAddress.setId("error");
        bodyContainer.add(msgAddress, 2, 6);
        txtAddress.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(!newValue){
                    if(txtAddress.getText().trim().equalsIgnoreCase("")){               // empty field
                        msgAddress.setId("error");
                        msgAddress.setGraphic(new ImageView("/media/incorrect.png"));
                        msgAddress.setTooltip(new Tooltip(objDictionaryAction.getWord("BALNKINPUT")));
                    } else {
                        msgAddress.setId("success");
                        msgAddress.setGraphic(new ImageView("/media/correct.png"));
                        msgAddress.setTooltip(new Tooltip(objDictionaryAction.getWord("OK")));
                    }
                }
            }
        });*/
        // Label for Country, its ChoiceBox, and its validation Label
        Label lblCountry= new Label(objDictionaryAction.getWord("COUNTRY"));
        lblCountry.setId("country");
        bodyContainer.add(lblCountry, 3, 6);
        countryCB.setMaxWidth(Double.MAX_VALUE);
        loadCountry(); // load list of countries
        countryCB.setCellFactory(new Callback<ListView<Label>, ListCell<Label>>() {
            @Override
            public ListCell<Label> call(ListView<Label> p) {
                return new ListCell<Label>() {
                    private final Label cell=new Label();                    
                    @Override
                    protected void updateItem(Label item, boolean empty) {
                        super.updateItem(item, empty);                        
                        if (item == null || empty) 
                            setText(null);
                        else 
                            setText(item.getText());
                    }
                };
            }
        });
        countryCB.setConverter(new StringConverter<Label>(){
            @Override
            public String toString(Label object) {
                return object.getText();
            }
            @Override
            public Label fromString(String string) {
                return new Label();
            }
        });
        cmbKeyListener(countryCB);
        bodyContainer.add(countryCB, 4, 6);
        final Label msgCountry= new Label("");
        msgCountry.setGraphic(new ImageView("/media/incorrect.png"));
        msgCountry.setTooltip(new Tooltip(objDictionaryAction.getWord("NOITEM")));
        msgCountry.setId("error");
        bodyContainer.add(msgCountry, 5, 6);
        countryCB.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(!newValue){
                    if(countryCB.getSelectionModel().getSelectedItem().getText()=="Select"){    // no country selected
                        msgCountry.setId("error");
                        msgCountry.setGraphic(new ImageView("/media/incorrect.png"));
                        msgCountry.setTooltip(new Tooltip(objDictionaryAction.getWord("NOITEM")));
                    } else {  // country is selected
                        intcountry=Integer.parseInt(countryCB.getSelectionModel().getSelectedItem().getUserData().toString());
                        loadState(intcountry);  // update state list for selected country
                        updateSuccessMarker(msgCountry);
                    }
                }
            }
        });
        // Label for State, its ChoiceBox, and its validation Label
        Label lblState= new Label(objDictionaryAction.getWord("STATE"));
        lblState.setId("state");
        bodyContainer.add(lblState, 0, 7);
        stateCB = new ComboBox<Label>();
        stateCB.setMaxWidth(Double.MAX_VALUE);
        loadState(intcountry); // when user selects a country then load states list based on country selected
        stateCB.setCellFactory(new Callback<ListView<Label>, ListCell<Label>>() {
            @Override
            public ListCell<Label> call(ListView<Label> p) {
                return new ListCell<Label>() {
                    private final Label cell=new Label();                    
                    @Override
                    protected void updateItem(Label item, boolean empty) {
                        super.updateItem(item, empty);                        
                        if (item == null || empty)
                            setText(null);
                        else
                            setText(item.getText());
                    }
                };
            }
        });
        stateCB.setConverter(new StringConverter<Label>(){
            @Override
            public String toString(Label object) {
                return object.getText();
            }
            @Override
            public Label fromString(String string) {
                return new Label();
            }
        });
        cmbKeyListener(stateCB);
        bodyContainer.add(stateCB, 1, 7);
        final Label msgState= new Label("");
        msgState.setGraphic(new ImageView("/media/incorrect.png"));
        msgState.setTooltip(new Tooltip(objDictionaryAction.getWord("NOITEM")));
        msgState.setId("error");
        bodyContainer.add(msgState, 2, 7);
        stateCB.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(!newValue){
                    if(stateCB.getSelectionModel().getSelectedItem().getText()=="Select"){  // no state selected
                        msgState.setId("error");
                        msgState.setGraphic(new ImageView("/media/incorrect.png"));
                        msgState.setTooltip(new Tooltip(objDictionaryAction.getWord("NOITEM")));
                    } else {
                        intstate=Integer.parseInt(stateCB.getSelectionModel().getSelectedItem().getUserData().toString());
                        loadCity(intstate); // update cities list based on selected state
                        updateSuccessMarker(msgState);
                    }
                }
            }
        });
        // Label for City, its ChoiceBox, and its validation Label
        Label lblCity= new Label(objDictionaryAction.getWord("CITY"));
        lblCity.setId("city");
        bodyContainer.add(lblCity, 3, 7);
        cityCB = new ComboBox<Label>();
        cityCB.setMaxWidth(Double.MAX_VALUE);
        loadCity(intstate); // when user selects a state then load cities list based on state selected
        cityCB.setCellFactory(new Callback<ListView<Label>, ListCell<Label>>() {
            @Override
            public ListCell<Label> call(ListView<Label> p) {
                return new ListCell<Label>() {
                    private final Label cell=new Label();                    
                    @Override
                    protected void updateItem(Label item, boolean empty) {
                        super.updateItem(item, empty);                        
                        if (item == null || empty) 
                            setText(null);
                        else 
                            setText(item.getText());
                    }
                };
            }
        });
        cityCB.setConverter(new StringConverter<Label>(){
            @Override
            public String toString(Label object) {
                return object.getText();
            }
            @Override
            public Label fromString(String string) {
                return new Label();
            }
        });
        cmbKeyListener(cityCB);
        bodyContainer.add(cityCB, 4, 7);
        final Label msgCity= new Label("");
        msgCity.setGraphic(new ImageView("/media/incorrect.png"));
        msgCity.setTooltip(new Tooltip(objDictionaryAction.getWord("NOITEM")));
        msgCity.setId("error");
        bodyContainer.add(msgCity, 5, 7);
        cityCB.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(!newValue){
                    if(cityCB.getSelectionModel().getSelectedItem().getText()=="Select"){   // no city is selected
                        msgCity.setId("error");
                        msgCity.setGraphic(new ImageView("/media/incorrect.png"));
                        msgCity.setTooltip(new Tooltip(objDictionaryAction.getWord("NOITEM")));
                    } else {
                        updateSuccessMarker(msgCity);
                    }
                }
            }
        });  
        /*// Label for Pincode, its TextField, and its validation Label
        Label lblPincode= new Label(objDictionaryAction.getWord("PINCODEADDRESS"));
        lblPincode.setId("pincode");
        bodyContainer.add(lblPincode, 0, 8);
        final TextField txtPincode = new TextField();
        txtPincode.setMinSize(180, 25);
        txtPincode.setMaxSize(180, 50);
        txtPincode.setPrefColumnCount(20);
        txtPincode.setPromptText(objDictionaryAction.getWord("PROMPTPINCODEADDRESS"));
        Tooltip pincodeTT = new Tooltip();
        pincodeTT.setText(objDictionaryAction.getWord("TOOLTIPPINCODEADDRESS"));
        pincodeTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        txtPincode.setTooltip(pincodeTT);
        bodyContainer.add(txtPincode, 1, 8);
        final Label msgPincode= new Label("");
        msgPincode.setGraphic(new ImageView("/media/incorrect.png"));
        msgPincode.setTooltip(pincodeTT);
        msgPincode.setId("error");
        bodyContainer.add(msgPincode, 2, 8);
        txtPincode.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(!newValue){
                    if(txtPincode.getText().trim().equalsIgnoreCase("")){               // empty field
                        msgPincode.setId("error");
                        msgPincode.setGraphic(new ImageView("/media/incorrect.png"));
                        msgPincode.setTooltip(new Tooltip(objDictionaryAction.getWord("BALNKINPUT")));
                    } else if(!isValidPincode(txtPincode.getText())){
                        msgPincode.setId("error");
                        msgPincode.setGraphic(new ImageView("/media/incorrect.png"));
                        msgPincode.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPPINCODEADDRESS")));
                    } else {
                        msgPincode.setId("success");
                        msgPincode.setGraphic(new ImageView("/media/correct.png"));
                        msgPincode.setTooltip(new Tooltip(objDictionaryAction.getWord("OK")));
                    }
                }
            }
        });*/
        // Label for GeoTag, its corresponding TextField, and validation Label
        /*Label lblGeotag= new Label("Nearby: ");
        grid.add(lblGeotag, 0, 9);
        final TextField txtGeotag = new TextField();
        txtGeotag.setPrefColumnCount(20);
        txtGeotag.setText("");
        final ComboBox<String> geotagCB=new ComboBox();
        grid.add(geotagCB, 1, 10);
        geotagCB.setVisible(false);
        geotagCB.setVisibleRowCount(3);*/
        /**
         * Google Maps Geo Location API is used for retrieving geo location of query location.
         * Http Request is fired, which returns a JSON result.
         * JSON result is parsed using library json-Simple, giving us JSON Object Array for all matching geo locations.
         * We then get our desired objects "formatted_address", "lat" & "lng".
         */
        /*txtGeotag.textProperty().addListener(new ChangeListener<String>(){
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if(newValue.isEmpty()){  // return when user clears geo Text Field (e.g. using backspace)
                    geotagCB.getItems().clear();
                    geotagCB.setVisible(false);
                    return;
                }
                if(geotagCB.isVisible() && newValue.equalsIgnoreCase(geotagCB.getValue()))
                    return;
                try {
                    newValue = newValue.replace(" ", "%20");    // replace all white spaces with "%20" to make a valid URL
                    URL url=new URL("http://maps.googleapis.com/maps/api/geocode/json?address="+newValue+"&sensor=true");
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setDoOutput(true);
                    conn.setInstanceFollowRedirects(false);
                    conn.setRequestMethod("GET");
                    conn.setRequestProperty("Content-Type", "application/json"); 
                    conn.setRequestProperty("charset", "utf-8");
            
                    if (conn.getResponseCode() != 200) {
                        throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
                    }
                    // reading response of HTTP Request into string "full"
                    BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
                    String output = "", full = "";
                    while ((output = br.readLine()) != null) {
                        full += output;
                    }
                    resultCount=getAddressFromJson(full);
                    if(resultCount>0){  // add result geo location(s) into geo ComboBox, so user can select one
                        geotagCB.getItems().clear();
                        geotagCB.setVisible(true);
                        geotagCB.getItems().add(0, "Select");
                        for(int a=0; a<resultCount;a++){
                            geotagCB.getItems().add(a+1, geoLocations[a]);
                        }
                        if(resultCount>0)
                            geotagCB.setValue(geotagCB.getItems().get(0));    // default selection is first geo location on list
                    } else{   // if no results are retrieved
                        geotagCB.getItems().clear();
                        geotagCB.setVisible(false);
                        geotagCB.getSelectionModel().clearSelection();
                    }
                    conn.disconnect();
                } catch (MalformedURLException muex) {
                    new Logging("SEVERE",UserRegistration.class.getName(),"URL Malfunction issue while Geo Locating",muex);
                } catch (IOException ioex) {
                    new Logging("SEVERE",UserRegistration.class.getName(),"IO Error while Geo Locating",ioex);
                } catch(Exception ex){
                    new Logging("SEVERE",UserRegistration.class.getName(),"Error while Geo Locating",ex);
                }
            }
        });
        geotagCB.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                // get Add, Lat, Lng from (index of) new value -> arr
                int indexInArr=geotagCB.getSelectionModel().getSelectedIndex();
                if(indexInArr<=0)
                    return;
                objUser.setStrGeoCity(geoLocations[indexInArr-1]);
                objUser.setDblGeoLat(lats[indexInArr-1]);
                objUser.setDblGeoLng(lngs[indexInArr-1]);
                txtGeotag.setText(geoLocations[indexInArr-1]);
            }
        });
        grid.add(txtGeotag, 1, 9);
        final Label errGeotag=new Label("");
        grid.add(errGeotag, 0, 10);
        geotagCB.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(!newValue){  // validating geo location when geo ComboBox loses focus
                    if(geotagCB.getSelectionModel().getSelectedIndex()>=0)
                        errGeotag.setId("success");
                        errGeotag.setText("OK");
                }
            }
        });*/   
        
        if(user_status){
            if(objConfiguration.getObjUser().getStrName()!=null){
                txtName.setText(objConfiguration.getObjUser().getStrName());
                updateSuccessMarker(msgName);
            }
            if(objConfiguration.getObjUser().getStrUsername()!=null){
                txtUsername.setText(objConfiguration.getObjUser().getStrUsername());
                try{ // query database table "mla_users" to validate unique username
                    if(new UserAction().isUsernameExisting(txtUsername.getText()))
                        updateSuccessMarker(msgUsername);// valid & unique username
                    else
                        msgUsername.setTooltip(new Tooltip(objDictionaryAction.getWord("USEREXIST")));
                } catch(Exception ex){
                    new Logging("SEVERE",UserRegistration.class.getName(),"Error in validating unique username from database",ex);
                }
            }
            if(objConfiguration.getObjUser().getStrEmail()!=null){
                txtEmail.setText(objConfiguration.getObjUser().getStrEmail());
                updateSuccessMarker(msgEmail);
            }
            if(objConfiguration.getObjUser().getStrContactNumber()!=null){
                txtContact.setText(objConfiguration.getObjUser().getStrContactNumber());
                updateSuccessMarker(msgContact);
            }
            if(objConfiguration.getObjUser().getStrGender()!=null){
                genderCB.setValue(objConfiguration.getObjUser().getStrGender());     
                updateSuccessMarker(msgGender);
            }
            Label lCountry, lState, lCity;
            if(objConfiguration.getObjUser().getStrCountryAddress()!=null){
                loadCountry();
                for(Iterator it = countryCB.getItems().iterator(); it.hasNext();){
                    lCountry = (Label)it.next();
                    if(lCountry.getText().equalsIgnoreCase(objConfiguration.getObjUser().getStrCountryAddress())){
                        countryCB.setValue(lCountry);
                        loadState(Integer.parseInt(lCountry.getUserData().toString()));
                        break;
                    }
                }
                updateSuccessMarker(msgCountry);
                if(objConfiguration.getObjUser().getStrStateAddress()!=null){
                    for(Iterator it = stateCB.getItems().iterator(); it.hasNext();){
                        lState = (Label)it.next();
                        if(lState.getText().equalsIgnoreCase(objConfiguration.getObjUser().getStrStateAddress())){
                            stateCB.setValue(lState);
                            loadCity(Integer.parseInt(lState.getUserData().toString()));
                            break;
                        }
                    }
                    updateSuccessMarker(msgState);
                    if(objConfiguration.getObjUser().getStrCityAddress()!=null){
                        for(Iterator it = cityCB.getItems().iterator(); it.hasNext();){
                            lCity = (Label)it.next();
                            if(lCity.getText().equalsIgnoreCase(objConfiguration.getObjUser().getStrCityAddress())){
                                cityCB.setValue(lCity);
                                break;
                            }
                        }
                        updateSuccessMarker(msgCity);
                    }
                }
            }
        }
        
        Hyperlink TnC = new Hyperlink(objDictionaryAction.getWord("DISCLAIMER"));
        TnC.setFont(Font.font(objConfiguration.getStrBFont(), FontWeight.EXTRA_BOLD, objConfiguration.getIntBFontSize()));
        final CheckBox chkAccept=new CheckBox(objDictionaryAction.getWord("IHAVEREAD"));
        chkAccept.setFont(Font.font(objConfiguration.getStrBFont(), FontWeight.BOLD, objConfiguration.getIntBFontSize()));
        HBox tncHB=new HBox();
        tncHB.setSpacing(3);
        tncHB.setAlignment(Pos.CENTER_RIGHT);
        tncHB.getChildren().add(chkAccept); 
        tncHB.getChildren().add(TnC);       
        bodyContainer.add(tncHB, 0, 10, 6, 1);
        TnC.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event) {
                new UserTnCView(objConfiguration);
            }
        });
        chkAccept.selectedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(newValue){
                    btnRegister.setDisable(false);
                    lblStatus.setFill(Color.DARKOLIVEGREEN);
                    lblStatus.setText(objDictionaryAction.getWord("THANKYOU"));
                }else{
                    btnRegister.setDisable(true);
                    lblStatus.setFill(Color.FIREBRICK);
                    lblStatus.setText(objDictionaryAction.getWord("ACCEPTLICENSEAGREEMENT"));
                }
            }
        });
        
        lblStatus = new Text();
        bodyContainer.add(lblStatus, 0, 11, 6, 1);
        lblStatus.setId("actiontarget");
        
        //Label lblNotePwd = new Label("NOTE: "+objDictionaryAction.getWord("TOOLTIPPASSWORD"));
        //bodyContainer.add(lblNotePwd, 0, 12, 6, 1);
        
        btnRegister=new Button(objDictionaryAction.getWord("SUBMIT"));
        btnRegister.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
        btnRegister.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUBMIT")));
        btnRegister.setDefaultButton(true);
        btnRegister.setFocusTraversable(true);
        btnRegister.setDisable(true);
        
        Button btnCancel=new Button(objDictionaryAction.getWord("BACK"));
        btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
          
        btnRegister.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event) {
                // if all entered details are valid, all corresponding <detail>OK labels will be having text "OK"
                // proceed only when all details are validated
                if(!chkAccept.isSelected()){
                    lblStatus.setFill(Color.FIREBRICK);
                    lblStatus.setText(objDictionaryAction.getWord("ACCEPTLICENSEAGREEMENT"));
                } else if(msgName.getId().equalsIgnoreCase("success")
                       && msgUsername.getId().equalsIgnoreCase("success")
                       && msgEmail.getId().equalsIgnoreCase("success")
                       && msgContact.getId().equalsIgnoreCase("success")
                       && msgPassword.getId().equalsIgnoreCase("success")
                       && msgRePassword.getId().equalsIgnoreCase("success")
                       //&& msgAppPassword.getId().equalsIgnoreCase("success")
                       //&& msgReAppPassword.getId().equalsIgnoreCase("success")
                       //&& msgAddress.getId().equalsIgnoreCase("success")
                       && msgGender.getId().equalsIgnoreCase("success")
                       && msgCountry.getId().equalsIgnoreCase("success")
                       && msgState.getId().equalsIgnoreCase("success")
                       && msgCity.getId().equalsIgnoreCase("success")
                       //&& msgPincode.getId().equalsIgnoreCase("success")
                       /*&& errGeotag.getId().equalsIgnoreCase("OK")*/){
                    try {
                        // generating userid with the mac (hardware) address of machine
                        //byte[] byteAddress=NetworkInterface.getByInetAddress(InetAddress.getLocalHost()).getHardwareAddress();
                        //String hwAddress=javax.xml.bind.DatatypeConverter.printHexBinary(byteAddress);
                        objUser.setStrUserID(new IDGenerator().getIDGenerator("USER_LIBRARY", CONFIG.LICENCE));
                        objUser.setStrUsername(txtUsername.getText());
                        objUser.setStrPassword(Security.hashPassword(txtPassword.getText()));
                        objUser.setStrAppPassword(Security.SecurePassword(txtPassword.getText(), txtUsername.getText()));
                        objUser.setStrName(txtName.getText());
                        objUser.setStrEmailID(txtEmail.getText());
                        objUser.setStrContactNumber(txtContact.getText());
                        objUser.setStrGender((String) genderCB.getValue());
                        // Add info for geotag--Already Set-----------------------------------------
                        //objConfiguration.getObjUser().setStrGeoCity("Lucknow");
                        //objConfiguration.getObjUser().setDblGeoLat(111);
                        //objConfiguration.getObjUser().setDblGeoLng(11);
                        //-----------
                        //objUser.setStrAddress(txtAddress.getText());
                        objUser.setStrCityAddress(cityCB.getConverter().toString(cityCB.getValue()));
                        objUser.setStrStateAddress(stateCB.getConverter().toString(stateCB.getValue()));
                        objUser.setStrCountryAddress(countryCB.getConverter().toString(countryCB.getValue()));
                        //objUser.setStrPincodeAddress(txtPincode.getText());
                        UserAction objUserAction = new UserAction();
                        new UserAction().addUserTnC(objUser.getStrUserID());
                        int intUID=objUserAction.addUser(objUser);
                        if(intUID!=-1){     // Record Added in mla_users successfully
                            // adding mla_user_usergroup_map table with same userid
                            new UserAction().addUserGroupMap(intUID, 2); //2 =register
                            new UserAction().addUserGroupMap(intUID, 10); //10 = desginer
                            // adding mla_user_preference table with same userid
                            objConfiguration.intializeConfiguration();
                            objUser.setUserAccess(objConfiguration.getObjUser().getUserAccess());
                            objConfiguration.setObjUser(objUser);
                            new UserAction().addUserPrefrence(objConfiguration);
                            new UserAction().addUserAccess(objConfiguration);
                            // updating mla_user_configuration table with same userid
                            //File[] files = new File("/data/clothsettings/").listFiles();
                            String[] files = new String[7];
                            files[0]=new String("data/clothsettings/Blouse.properties");
                            files[1]=new String("data/clothsettings/Body.properties");
                            files[2]=new String("data/clothsettings/Border.properties");
                            files[3]=new String("data/clothsettings/CrossBorder.properties");
                            files[4]=new String("data/clothsettings/Konia.properties");
                            files[5]=new String("data/clothsettings/Palu.properties");
                            files[6]=new String("data/clothsettings/Skirt.properties");
                            
                            //If this pathname does not denote a directory, then listFiles() returns null. 
                            int confNum=1;//CONF#
                            for (String file : files) {
                                if (file!=null) {
                                    objConfiguration.intializeClothConfiguration(file);
                                    objConfiguration.setStrConfName("CONF"+objUser.getStrUserID()+(confNum++));
                                    new UserAction().addUserConfiguration(objConfiguration);
                                }
                            }
                            userStage.close();
                            boolean isActivated = new UserAction().isApplicationActivated();
                            if(isActivated)
                                new UserLoginView(objConfiguration);
                            else
                                new ActivationView(objConfiguration, user_status, false);
                        } else{
                            lblStatus.setFill(Color.FIREBRICK);
                            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                        }
                    } catch (SQLException ex) {
                        lblStatus.setFill(Color.FIREBRICK);
                        lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                        new Logging("SEVERE",UserRegistration.class.getName(),"User Registration",ex);
                    }
                } else {
                    lblStatus.setFill(Color.FIREBRICK);
                    lblStatus.setText(objDictionaryAction.getWord("INVALIDDATA"));
                }
            }
        });
        btnCancel.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event) {
                userStage.close();
                System.gc();
                UserLoginView objUserLoginView = new UserLoginView(objConfiguration);
            }
        });
        
        HBox P_buttons = new HBox(10);
        P_buttons.setAlignment(Pos.BOTTOM_RIGHT);
        P_buttons.getChildren().addAll(btnRegister,btnCancel);
        bodyContainer.add(P_buttons, 0, 13, 6, 1);

        final ImageView helpIV = new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png");
        Tooltip.install(helpIV, new Tooltip(objDictionaryAction.getWord("TOOLTIPHELP")));
        helpIV.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                helpIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
                helpIV.setScaleX(1.5);
                helpIV.setScaleY(1.5);
                helpIV.setScaleZ(1.5);
            }
        });
        helpIV.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                helpIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
                helpIV.setScaleX(1);
                helpIV.setScaleY(1);
                helpIV.setScaleZ(1);
            }
        });
        helpIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                HelpView objHelpView = new HelpView(objConfiguration);
            }
        });
        final ImageView technicalIV = new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/technical_info.png");
        Tooltip.install(technicalIV, new Tooltip(objDictionaryAction.getWord("TOOLTIPTECHNICAL")));
        technicalIV.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                technicalIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/technical_info.png"));
                technicalIV.setScaleX(1.5);
                technicalIV.setScaleY(1.5);
                technicalIV.setScaleZ(1.5);
            }
        });
        technicalIV.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                technicalIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/technical_info.png"));
                technicalIV.setScaleX(1);
                technicalIV.setScaleY(1);
                technicalIV.setScaleZ(1);
            }
        });
        technicalIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                TechnicalView objTechnicalView = new TechnicalView(objConfiguration);
            }
        });
        final ImageView aboutIV = new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/about_software.png");
        Tooltip.install(aboutIV, new Tooltip(objDictionaryAction.getWord("TOOLTIPABOUTUS")));
        aboutIV.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                aboutIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/about_software.png"));
                aboutIV.setScaleX(1.5);
                aboutIV.setScaleY(1.5);
                aboutIV.setScaleZ(1.5);
            }
        });
        aboutIV.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                aboutIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/about_software.png"));
                aboutIV.setScaleX(1);
                aboutIV.setScaleY(1);
                aboutIV.setScaleZ(1);
            }
        });
        aboutIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                AboutView objAboutView = new AboutView(objConfiguration);
            }
        });
        final ImageView contactIV = new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/contact_us.png");
        Tooltip.install(contactIV, new Tooltip(objDictionaryAction.getWord("TOOLTIPCONTACTUS")));
        contactIV.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                contactIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/contact_us.png"));
                contactIV.setScaleX(1.5);
                contactIV.setScaleY(1.5);
                contactIV.setScaleZ(1.5);
            }
        });
        contactIV.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                contactIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/contact_us.png"));
                contactIV.setScaleX(1);
                contactIV.setScaleY(1);
                contactIV.setScaleZ(1);
            }
        });
        contactIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                ContactView objContactView = new ContactView(objConfiguration);
            }
        });

        HBox topContainer = new HBox(10);
        topContainer.setAlignment(Pos.BOTTOM_CENTER);
        topContainer.getChildren().addAll(helpIV, technicalIV, aboutIV, contactIV);
        //bodyContainer.add(P_links, 0, 13, 6, 1);

        root.setRight(closeIV); 
        root.setCenter(bodyContainer);    
        root.setBottom(topContainer);

        userStage.getIcons().add(new Image("/media/icon.png"));
        userStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWLOGIN")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
        userStage.setScene(scene);
        userStage.showAndWait();
        //userStage.setX(-5);
        //userStage.setY(0);
    }
    
    /**
     * Description: Loads list of countries in country ComboBox (countryCB)
     */
    private void loadCountry(){
        try {
            countryCB.getItems().clear();
            Label lblTemp = new Label("Select");
            lblTemp.setUserData(0);
            countryCB.getItems().add(lblTemp);
            countryCB.setValue(lblTemp);
            
            UserAction objUserAction = new UserAction();
            lstCountry = objUserAction.getLstCountry();
            for(int i=0; i<lstCountry.size(); i++){
                List lstTemp = (ArrayList)lstCountry.get(i);
                lblTemp = new Label(lstTemp.get(1).toString());
                lblTemp.setUserData(lstTemp.get(0));
                countryCB.getItems().add(lblTemp);
            }
            userStage.close();
        } catch (SQLException ex) {
            new Logging("SEVERE",UserRegistration.class.getName(),"SQLException: Listing Country",ex);
        } catch (Exception ex) {
            new Logging("SEVERE",UserRegistration.class.getName(),"Exception: Listing Country",ex);
        }
    }
    /**
     * Description: Loads list of states in state ComboBox (stateCB) based on selected country
     * @param intCountry (selected country)
     */
    private void loadState(int intCountry){
        try {
            stateCB.getItems().clear();
            Label lblTemp = new Label("Select");
            lblTemp.setUserData(0);
            stateCB.getItems().add(lblTemp);
            stateCB.setValue(lblTemp);
            
            UserAction objUserAction = new UserAction();
            lstState = objUserAction.getLstState(intCountry);
            for(int i=0; i<lstState.size(); i++){
                List lstTemp = (ArrayList)lstState.get(i);
                lblTemp = new Label(lstTemp.get(1).toString());
                lblTemp.setUserData(lstTemp.get(0).toString());
                stateCB.getItems().add(lblTemp);
            }
        } catch (SQLException ex) {
            new Logging("SEVERE",UserRegistration.class.getName(),"SQLException: Listing State",ex);
        } catch (Exception ex) {
            new Logging("SEVERE",UserRegistration.class.getName(),"Exception: Listing State",ex);
        }
    }
    
    /**
     * Description: Loads list of cities in city ComboBox (cityCB) based on selected state
     * @param intState (selected state)
     */
    private void loadCity(int intState){
        try {
            cityCB.getItems().clear();
            Label lblTemp = new Label("Select");
            lblTemp.setUserData(0);
            cityCB.getItems().add(lblTemp);
            cityCB.setValue(lblTemp);
            
            UserAction objUserAction = new UserAction();
            lstCity = objUserAction.getLstCity(intState);
            for(int i=0; i<lstCity.size(); i++){
                List lstTemp = (ArrayList)lstCity.get(i);
                lblTemp = new Label(lstTemp.get(1).toString());
                lblTemp.setUserData(lstTemp.get(0).toString());
                cityCB.getItems().add(lblTemp);
            }
        } catch (SQLException ex) {
            new Logging("SEVERE",UserRegistration.class.getName(),"SQLException: Listing city",ex);
        } catch (Exception ex) {
            new Logging("SEVERE",UserRegistration.class.getName(),"Exception: Listing city",ex);
        }
    }
    private void cmbKeyListener(final ComboBox c){
        final StringBuilder sb=new StringBuilder();
        c.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>(){
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.UP || event.getCode() == KeyCode.TAB) {
                    return;
                } else if (event.getCode() == KeyCode.BACK_SPACE && sb.length() > 0) {
                    sb.deleteCharAt(sb.length()-1);
                } else {
                    sb.append(event.getText());
                }
 
                if (sb.length() == 0) 
                    return;
                boolean found = false;
                ObservableList items = c.getItems();
                for (int i=0; i<items.size(); i++) {
                    if (event.getCode() != KeyCode.BACK_SPACE && c.getConverter().toString(items.get(i)).toLowerCase().startsWith(sb.toString().toLowerCase())) {
                        ListView lv = ((ComboBoxListViewSkin) c.getSkin()).getListView();
                        lv.getSelectionModel().clearAndSelect(i);           
                        lv.scrollTo(lv.getSelectionModel().getSelectedIndex());
                        found = true;
                        break;
                    }
                }
                if (!found && sb.length() > 0)
                    sb.deleteCharAt(sb.length() - 1);
            }
        });
    }
    /**
     * Description: Geo Location querying on Google Maps API returns array of json objects for all geo locations found for a query
     * We need to show all these possible geo locations for Auto Complete feature on Geo Location TextField
     * @param json string returned as response of geo location querying
     * @return number of different geo location
     */
    private int getAddressFromJson(String json){
        resultCount=0;
        try{
            JSONParser parser=new JSONParser();
            JSONObject parsed = (JSONObject)parser.parse(json);
            JSONArray arr=(JSONArray) parsed.get("results");
            geoLocations=new String[arr.size()];
            lats=new double[arr.size()];
            lngs=new double[arr.size()];
            for(int a=0; a<arr.size(); a++){
                JSONObject obj=(JSONObject) arr.get(a);
                geoLocations[a]= (String) obj.get("formatted_address");
                JSONObject geometry=(JSONObject) obj.get("geometry");
                JSONObject location=(JSONObject) geometry.get("location");
                lats[a]=(double) location.get("lat");
                lngs[a]=(double) location.get("lng");
            }
            resultCount=arr.size();
        }
        catch(Exception exc){}
        return resultCount;
    }
    /*public boolean isValidEmailAddress(String email){
        boolean valid=true;
        try{
            InternetAddress emailAdd=new InternetAddress(email);
            emailAdd.validate();
        } catch(AddressException ae){
            valid=false;
        }
        return valid;
    }*/
    private boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }
    private boolean isValidContactNumber(String contact) {
        if(!(contact.length()==10))
            return false;
        else 
            return contact.matches("\\d*");
    }
    private boolean isValidPincode(String conatct) {
        if(!(conatct.length()==6))
            return false;
        else 
            return conatct.matches("\\d*");
    }
    private boolean isValidPassword(String name, String username, String password){
        // check if part of name exists in password
        for(int i=0;(i+4)<name.length();i++)
          if(password.indexOf(name.substring(i,i+4))!=-1)
                return false;
        // check if part of username exists in password
        for(int i=0;(i+4)<username.length();i++)
          if(password.indexOf(username.substring(i,i+4))!=-1)
                return false;
        return true;
    }
    
    void updateSuccessMarker(Label msgX){
        msgX.setId("success");
        msgX.setGraphic(new ImageView("/media/correct.png"));
        msgX.setTooltip(new Tooltip(objDictionaryAction.getWord("OK")));
    }
    
}
