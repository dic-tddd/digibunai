/*
 * Copyright (C) Digital India Corporation (Media Lab Asia)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mla.user;

import com.mla.dictionary.DictionaryAction;
import com.mla.utility.AboutView;
import com.mla.main.Configuration;
import com.mla.utility.ContactView;
import com.mla.utility.HelpView;
import com.mla.main.Logging;
import com.mla.utility.TechnicalView;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.layout.ColumnConstraints;
import javafx.stage.StageStyle;
/**
 *
 * @Designing GUI Window for user
 * @author Amit Kumar Singh
 * 
 */
public class UserForgetPassword extends Application {

    private static Stage userStage;
    private TextField txtUsername;
    private TextField txtEmail;
    User objUser = null;
    Configuration objConfiguration = null;
    DictionaryAction objDictionaryAction = null;
    private Text lblStatus;    
    
  public UserForgetPassword(final Stage primaryStage) { }
  
  public UserForgetPassword(Configuration objConfigurationCall) {
    objUser = new User();
    objConfiguration = objConfigurationCall;
    objDictionaryAction = new DictionaryAction(objConfiguration);
        
    userStage = new Stage();
    //userStage.initModality(Modality.APPLICATION_MODAL);//WINDOW_MODAL
    //userStage.initStyle(StageStyle.UNDECORATED);
    userStage.initStyle(StageStyle.TRANSPARENT);
    userStage.setResizable(false);
    userStage.setIconified(false);
    userStage.setFullScreen(false);
                
    BorderPane root = new BorderPane();
    Scene scene = new Scene(root, 450, 250, Color.TRANSPARENT);
    scene.getStylesheets().add(UserForgetPassword.class.getResource("/media/login.css").toExternalForm());
    root.setId("borderpane");    
    
    final ImageView closeIV = new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png");
    Tooltip.install(closeIV, new Tooltip(objDictionaryAction.getWord("TOOLTIPCLOSE")));    
    closeIV.setOnMouseEntered(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            closeIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            closeIV.setScaleX(1.5);
            closeIV.setScaleY(1.5);
            closeIV.setScaleZ(1.5);
        }
    });
    closeIV.setOnMouseExited(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            closeIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            closeIV.setScaleX(1);
            closeIV.setScaleY(1);
            closeIV.setScaleZ(1);
        }
    });
    closeIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            userStage.close();
        }
    });

    final GridPane bodyConrainer = new GridPane();   
    bodyConrainer.setId("bodyConrainer");
    bodyConrainer.getColumnConstraints().add(new ColumnConstraints(130)); // column 1 is 100 wide
    bodyConrainer.getColumnConstraints().add(new ColumnConstraints(220)); // column 2 is 200 wide
    bodyConrainer.setAlignment(Pos.CENTER);
    bodyConrainer.setMaxSize(350, 200);
    bodyConrainer.relocate(10, 10);
    //bodyConrainer.setLayoutX(30);
    //bodyConrainer.setLayoutY(10);
        
    Text projectTitle = new Text(objDictionaryAction.getWord("TRADEMARK"));
    projectTitle.setFont(Font.font(objConfiguration.getStrBFont(), FontWeight.NORMAL, objConfiguration.getIntBFontSize()));
    projectTitle.setId("welcome-text");
    bodyConrainer.add(projectTitle, 0, 0, 2, 1);
        
    Label versionTitle = new Label(objDictionaryAction.getWord("VERSIONINFO"));
    versionTitle.setId("version-text");
    bodyConrainer.add(versionTitle, 0, 1, 2, 1);
    
    final Label lblUsername = new Label(objDictionaryAction.getWord("USERNAME")+ ":");
    lblUsername.setId("username");
    bodyConrainer.add(lblUsername, 0, 2);

    txtUsername = new TextField();
    txtUsername.setPromptText(objDictionaryAction.getWord("PROMPTUSERNAME"));
    final Tooltip usernameTT = new Tooltip();
    usernameTT.setText(objDictionaryAction.getWord("TOOLTIPUSERNAME"));
    usernameTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
    txtUsername.setTooltip(usernameTT);
    bodyConrainer.add(txtUsername, 1, 2);

    final Label lblEmail = new Label(objDictionaryAction.getWord("EMAILID")+":");
    lblEmail.setId("email");
    bodyConrainer.add(lblEmail, 0, 3);

    txtEmail = new TextField();
    txtEmail.setPromptText(objDictionaryAction.getWord("PROMPTEMAILID"));
    final Tooltip emailTT = new Tooltip();
    emailTT.setText(objDictionaryAction.getWord("TOOLTIPEMAILID"));
    emailTT.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
    txtEmail.setTooltip(emailTT);
    bodyConrainer.add(txtEmail, 1, 3);
    
    final Button btnSubmit = new Button(objDictionaryAction.getWord("SUBMIT"));
    btnSubmit.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
    btnSubmit.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUBMIT")));
    btnSubmit.setDefaultButton(true);
    btnSubmit.setFocusTraversable(true);
    
    final Button btnUpdate=new Button(objDictionaryAction.getWord("UPDATE"));
    btnUpdate.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
    btnUpdate.setTooltip(new Tooltip(objDictionaryAction.getWord("UPDATE")+" "+objDictionaryAction.getWord("PASSWORD")));
    btnUpdate.setVisible(false);
    
    btnUpdate.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent t) {
            if(txtUsername.getText().length()==0||txtEmail.getText().length()==0)
                lblStatus.setText(objDictionaryAction.getWord("INVALIDDATA"));    // if either of the password fields is empty
            else if(!txtUsername.getText().equals(txtEmail.getText()))
                lblStatus.setText(objDictionaryAction.getWord("NOMATCHPASSWORD")); // NEW password & CONFIRM NEW password does not match
            else if(txtUsername.getText().length()<6)
                lblStatus.setText(objDictionaryAction.getWord("WEEKPASSWORD"));
            else{
                try {
                    // NEW password being hashed and updated in "com.mla.user.UserAction.updatePassword()".
                    objConfiguration.getObjUser().setStrPassword(txtUsername.getText());
                    boolean changed=new UserAction().updatePassword(objConfiguration.getObjUser());
                    if(changed){    // password modified successfully
                        lblStatus.setText(objDictionaryAction.getWord("DATASAVED"));
                        userStage.close();
                        UserLoginView objUserLoginView = new UserLoginView(objConfiguration);
                    } else{       // password updation failed                                            
                        lblStatus.setText(objDictionaryAction.getWord("DATAUNSAVED"));
                    }
                } catch(Exception ex){
                    new Logging("SEVERE",UserForgetPassword.class.getName(),"updatePassword(): error updating data into database",ex);
                }
            }
        }
    });
    
    final Button btnCancel=new Button(objDictionaryAction.getWord("BACK"));
    btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
    btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));

    btnSubmit.setOnAction(new EventHandler<ActionEvent>() {
         @Override
        public void handle(ActionEvent e) {
             try {
                lblStatus.setFill(Color.FIREBRICK);
                if(txtUsername.getText().length()==0 || txtEmail.getText().length()==0){    // empty username or email field
                        lblStatus.setText(objDictionaryAction.getWord("BALNKINPUT"));
                } else{
                    UserAction objUserAction=new UserAction();
                    // match username & email from "mla_users" database table, returns true when validated else false
                    String userid=objUserAction.verifyUsernameEmail(txtUsername.getText(), txtEmail.getText());
                    // checking if email is sent successfully
                    if(userid!=null){
                        /*boolean sent=new com.mla.main.Email().send(txtEmail.getText(), "Password Reset Steps", "password");
                        if(sent){
                            lblStatus.setText(txtEmail.getText()+": "+objDictionaryAction.getWord("SENTNOTIFICATION"));
                        } else{
                            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                        }*/
                        objConfiguration.getObjUser().setStrUserID(userid);
                        lblUsername.setText(objDictionaryAction.getWord("NEW")+" "+objDictionaryAction.getWord("PASSWORD")+":");
                        txtUsername = new PasswordField();
                        txtUsername.setText("");
                        txtUsername.setPromptText(objDictionaryAction.getWord("PROMPTPASSWORD"));
                        bodyConrainer.add(txtUsername, 1, 2);
                        lblEmail.setText(objDictionaryAction.getWord("CONFIRMPASSWORD")+":");
                        txtEmail = new PasswordField();
                        txtEmail.setText("");
                        txtEmail.setPromptText(objDictionaryAction.getWord("PROMPTCONFIRMPASSWORD"));
                        bodyConrainer.add(txtEmail, 1, 3);
                        btnSubmit.setVisible(false);
                        btnUpdate.setVisible(true);
                        btnCancel.setText(objDictionaryAction.getWord("CANCEL"));
                    } else{
                        lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));    // if username & email are not matching
                    }
                }
            } catch (Exception ex) {
                new Logging("SEVERE",UserView.class.getName(),"ForgotPassword -> matchUsernameAndEmail(): error validating data from database",ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        }
    });
    
    btnCancel.setOnAction(new EventHandler<ActionEvent>(){
        @Override
        public void handle(ActionEvent event) {
            userStage.close();
            System.gc();
            UserLoginView objUserLoginView = new UserLoginView(objConfiguration);
        }
    });   
    
    
    HBox P_buttons = new HBox(10);
    P_buttons.setAlignment(Pos.BOTTOM_RIGHT);
    P_buttons.getChildren().addAll(btnSubmit, btnUpdate, btnCancel);
    bodyConrainer.add(P_buttons, 0, 4, 2, 1);

    lblStatus = new Text();
    bodyConrainer.add(lblStatus, 0, 5, 2, 1);
    lblStatus.setId("actiontarget");
    
    final ImageView helpIV = new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png");
    Tooltip.install(helpIV, new Tooltip(objDictionaryAction.getWord("TOOLTIPHELP")));
    helpIV.setOnMouseEntered(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            helpIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
            helpIV.setScaleX(1.5);
            helpIV.setScaleY(1.5);
            helpIV.setScaleZ(1.5);
        }
    });
    helpIV.setOnMouseExited(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            helpIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
            helpIV.setScaleX(1);
            helpIV.setScaleY(1);
            helpIV.setScaleZ(1);
        }
    });
    helpIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            HelpView objHelpView = new HelpView(objConfiguration);
        }
    });
    final ImageView technicalIV = new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/technical_info.png");
    Tooltip.install(technicalIV, new Tooltip(objDictionaryAction.getWord("TOOLTIPTECHNICAL")));
    technicalIV.setOnMouseEntered(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            technicalIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/technical_info.png"));
            technicalIV.setScaleX(1.5);
            technicalIV.setScaleY(1.5);
            technicalIV.setScaleZ(1.5);
        }
    });
    technicalIV.setOnMouseExited(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            technicalIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/technical_info.png"));
            technicalIV.setScaleX(1);
            technicalIV.setScaleY(1);
            technicalIV.setScaleZ(1);
        }
    });
    technicalIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            TechnicalView objTechnicalView = new TechnicalView(objConfiguration);
        }
    });
    final ImageView aboutIV = new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/about_software.png");
    Tooltip.install(aboutIV, new Tooltip(objDictionaryAction.getWord("TOOLTIPABOUTUS")));
    aboutIV.setOnMouseEntered(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            aboutIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/about_software.png"));
            aboutIV.setScaleX(1.5);
            aboutIV.setScaleY(1.5);
            aboutIV.setScaleZ(1.5);
        }
    });
    aboutIV.setOnMouseExited(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            aboutIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/about_software.png"));
            aboutIV.setScaleX(1);
            aboutIV.setScaleY(1);
            aboutIV.setScaleZ(1);
        }
    });
    aboutIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            AboutView objAboutView = new AboutView(objConfiguration);
        }
    });
    final ImageView contactIV = new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/contact_us.png");
    Tooltip.install(contactIV, new Tooltip(objDictionaryAction.getWord("TOOLTIPCONTACTUS")));
    contactIV.setOnMouseEntered(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            contactIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/contact_us.png"));
            contactIV.setScaleX(1.5);
            contactIV.setScaleY(1.5);
            contactIV.setScaleZ(1.5);
        }
    });
    contactIV.setOnMouseExited(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            contactIV.setImage(new Image(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/contact_us.png"));
            contactIV.setScaleX(1);
            contactIV.setScaleY(1);
            contactIV.setScaleZ(1);
        }
    });
    contactIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent me) {
            ContactView objContactView = new ContactView(objConfiguration);
        }
    });

    HBox topContainer = new HBox(10);
    topContainer.setAlignment(Pos.BOTTOM_CENTER);
    topContainer.getChildren().addAll(helpIV, technicalIV, aboutIV, contactIV);
    //bodyConrainer.add(P_links, 0, 7, 2, 1);
    
    root.setRight(closeIV); 
    root.setCenter(bodyConrainer);    
    root.setBottom(topContainer);
    
    userStage.getIcons().add(new Image("/media/icon.png"));
    userStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWUSERSETTINGS")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
    userStage.setScene(scene);
    userStage.showAndWait();
    //userStage.setX(-5);
    //userStage.setY(0);
  }
  
   @Override
   public void start(Stage stage) throws Exception {
        new UserForgetPassword(stage); 
        new Logging("WARNING",UserForgetPassword.class.getName(),"UnsupportedOperationException",new UnsupportedOperationException("Not supported yet."));
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}