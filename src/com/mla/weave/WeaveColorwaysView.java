/*
 * Copyright (C) 2018 Digital India Corporation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.mla.weave;

import com.mla.dictionary.DictionaryAction;
import com.mla.main.MessageView;
import com.mla.yarn.Yarn;
import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Pagination;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;

/**
 * Colourways (Blanket View) UI and Functions
 * @author Aatif Ahmad Khan
 * @since 0.9.3
 */
public class WeaveColorwaysView {
    
    Weave objWeave;
    DictionaryAction objDictionaryAction;
    
    final int INT_SOLID_WHITE=-1;
    
    public WeaveColorwaysView(Weave objWeave){
        this.objWeave=objWeave;
        this.objDictionaryAction = new DictionaryAction(this.objWeave.getObjConfiguration());
        colourwaysAction();
    }
    
    /**
    * Generates colourways (Blanket View) for given weave with assigned warp/weft
    * yarn colors.
    */
    private void colourwaysAction(){
        if(objWeave.getWarpYarn()==null||objWeave.getWeftYarn()==null)
            return;
        int warpCount=objWeave.getWarpYarn().length;
        int weftCount=objWeave.getWeftYarn().length;
        ArrayList<String> yarnColor=new ArrayList<>();
        for(int c=0; c<warpCount; c++){
            if(!yarnColor.contains("1"+objWeave.getWarpYarn()[c].getStrYarnColor()))
                yarnColor.add("1"+objWeave.getWarpYarn()[c].getStrYarnColor());
        }
        warpCount=yarnColor.size();
        for(int c=0; c<weftCount; c++){
            if(!yarnColor.contains("0"+objWeave.getWeftYarn()[c].getStrYarnColor()))
                yarnColor.add("0"+objWeave.getWeftYarn()[c].getStrYarnColor());
        }
        weftCount=yarnColor.size()-warpCount;
        final int intWeftCount=objWeave.getIntWeft();
        final int intWarpCount=objWeave.getIntWarp();
        final int totalColor=yarnColor.size();
        if(totalColor>7){
            new MessageView("error", "Reduce Colors", objDictionaryAction.getWord("MANYCOLOUR"));
            return;
        }
        final int[][] colorIndex=getColorIndex(yarnColor, warpCount);
        int totalComb=factorial(yarnColor.size());
        ArrayList<List<String>> list=new ArrayList<>();
        permute(yarnColor, 0, list);
        int count=0;
        final String[][] colorPermutation=new String[totalComb][yarnColor.size()];
        for(List<String> l:list){
            int sCount=0;
            for(String s: l){
                colorPermutation[count][sCount++]=s.substring(1);
            }
            count++;
        }

        // eliminate duplicate color combinations
        final String colorPermutationUpdated[][] = getColorPermutationNoDuplicates(colorPermutation, colorIndex);
        final int totalCombUpdated = colorPermutationUpdated.length;

        int height=(int)(objWeave.getObjConfiguration().HEIGHT/3);
        int width=(int)(objWeave.getObjConfiguration().WIDTH/3);
        BufferedImage bufferedImage = new BufferedImage(width*3, height*3,BufferedImage.TYPE_INT_RGB);
        final int COMB_PER_PAGE=9;
        if(totalCombUpdated<=0)
            return;
        
        final int totalPages=(int)Math.ceil((float)totalCombUpdated/(float)COMB_PER_PAGE);
        Pagination objPage=new Pagination(totalPages);
        objPage.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                if (pageIndex >= totalPages) {
                    return null;
                } else {
                    ImageView iv =plotShadedDesignPage(colorPermutationUpdated, colorIndex, COMB_PER_PAGE, pageIndex.intValue()*COMB_PER_PAGE, intWarpCount, intWeftCount, totalCombUpdated);
                    return iv;
                }
            }
        });

        final int pageDiv=(int)Math.sqrt(COMB_PER_PAGE);

        Graphics2D g=bufferedImage.createGraphics();
        BasicStroke bs = new BasicStroke(5);
        g.setStroke(bs);
        for(int z=1; z<=(pageDiv-1); z++){
            g.drawLine(0, z*((height*3)/pageDiv), bufferedImage.getWidth()-1, z*((height*3)/pageDiv));
            g.drawLine(z*((width*3)/pageDiv), 0, z*((width*3)/pageDiv), bufferedImage.getHeight()-1);
        }
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        BorderPane popup=new BorderPane();
        final ImageView weaveIV=new ImageView();

        int intLength = (int)(((objWeave.getObjConfiguration().getIntDPI()*bufferedImage.getWidth())/objWeave.getIntEPI()));
        int intHeight = (int)(((objWeave.getObjConfiguration().getIntDPI()*bufferedImage.getHeight())/objWeave.getIntPPI()));
        intHeight=(int)(intHeight*(objWeave.getObjConfiguration().WIDTH/intLength));
        intLength=(int)objWeave.getObjConfiguration().WIDTH;
        BufferedImage bufferedImageesize = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g1 = bufferedImageesize.createGraphics();
        g1.drawImage(bufferedImage, 0, 0, intLength, intHeight, null);
        g1.dispose();

        weaveIV.setImage(SwingFXUtils.toFXImage(bufferedImageesize, null));        
        weaveIV.setFitHeight(objWeave.getObjConfiguration().HEIGHT);
        weaveIV.setFitWidth(objWeave.getObjConfiguration().WIDTH);
        popup.setTop(objPage);
        popup.setId("popup");
        Scene popupScene = new Scene(popup);

        popupScene.getStylesheets().add(WeaveView.class.getResource(objWeave.getObjConfiguration().getStrTemplate()+"/style.css").toExternalForm());
        dialogStage.setScene(popupScene);
        dialogStage.setX(-5);
        dialogStage.setY(0);
        dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("ACTIONCOLOURWAYS"));
        dialogStage.showAndWait();

        bufferedImage = null;
        System.gc();
    }

    /**
     * Updates warp/weft yarn color of weave as per selected color combination and opens
     * the weave view in WeaveViewer
     * @param patternNum Index of selected pattern combination (1..N)
     * @param colorPermutation All color combinations
     * @param colorIndex  Index of colors with respect to original sequence
     */
    private void plotShadedDesignParam(int patternNum, String[][] colorPermutation, int[][] colorIndex){
        int intWarpCount=objWeave.getIntWarp();
        int intWeftCount=objWeave.getIntWeft();
        int height=(int)(objWeave.getObjConfiguration().HEIGHT/3);
        int width=(int)(objWeave.getObjConfiguration().WIDTH/3);
        byte[][] tilledMatrix = new byte[height][width];
        
        Yarn[] weftYarn = objWeave.getWeftYarn();
        Yarn[] warpYarn = objWeave.getWarpYarn();
        
        int a=patternNum; // starting number of combination
        for(int x=0; x<weftYarn.length; x++){
            for(int y=0; y<warpYarn.length; y++){
                if(x>=objWeave.getIntWeft() && y<objWeave.getIntWarp()){
                     tilledMatrix[x][y] = objWeave.getDesignMatrix()[x%objWeave.getIntWeft()][y];  
                }else if(x<objWeave.getIntWeft() && y>=objWeave.getIntWarp()){
                     tilledMatrix[x][y] = objWeave.getDesignMatrix()[x][y%objWeave.getIntWarp()];  
                }else if(x>=objWeave.getIntWeft() && y>=objWeave.getIntWarp()){
                     tilledMatrix[x][y] = objWeave.getDesignMatrix()[x%objWeave.getIntWeft()][y%objWeave.getIntWarp()];  
                }else{
                     tilledMatrix[x][y] = objWeave.getDesignMatrix()[x][y]; 
                }
                if(tilledMatrix[x][y]==1){
                    warpYarn[y%intWarpCount].setStrYarnColor(colorPermutation[a][colorIndex[x%intWeftCount][y%intWarpCount]]);
                } else if(tilledMatrix[x][y]==0){
                    weftYarn[x%intWeftCount].setStrYarnColor(colorPermutation[a][colorIndex[x%intWeftCount][y%intWarpCount]]);
                } else {
                    weftYarn[x%intWeftCount].setStrYarnColor(colorPermutation[a][colorIndex[x%intWeftCount][y%intWarpCount]]);
                }
            }
        }
        objWeave.setWarpYarn(warpYarn);
        objWeave.setWeftYarn(weftYarn);
        // open selected combination in WeaveViewer
        WeaveViewer objWeaveViewer=new WeaveViewer(objWeave);
    }
    
    /**
     * Plots pagination of colorways (Blanket View)
     * @param colorPermutation All color combinations
     * @param colorIndex Index of selected pattern combination (1..N)
     * @param combPerPage Number of items to display in a page
     * @param startNumPattern Starting index of combinations on a page
     * @param intWarpCount number of warp yarns in weave
     * @param intWeftCount number of weft yarns in weave
     * @param totalCombinations total combinations generated
     * @return ImageView container showing pagination control with color combinations
     */
    private ImageView plotShadedDesignPage(final String[][] colorPermutation, final int[][] colorIndex, int combPerPage, final int startNumPattern, int intWarpCount, int intWeftCount, final int totalCombinations){
        int height=(int)(objWeave.getObjConfiguration().HEIGHT/3);
        int width=(int)(objWeave.getObjConfiguration().WIDTH/3);
        byte[][] tilledMatrix = new byte[height][width];
        BufferedImage bufferedImage = new BufferedImage(width*3, height*3,BufferedImage.TYPE_INT_RGB);
        for(int a=0; a<bufferedImage.getWidth(); a++)
            for(int b=0; b<bufferedImage.getHeight(); b++)
                bufferedImage.setRGB(a, b, INT_SOLID_WHITE);
        int bands = 3;
        int rgb = 0;

        final int pageDiv=(int)Math.sqrt(combPerPage);
        final int patternW=(width*3)/pageDiv;
        final int patternH=(height*3)/pageDiv;


        for(int x = 0, p = 0; x < height; x++) {
            for(int y = 0, q = 0; y < width; y++) {
                if(x>=objWeave.getIntWeft() && y<objWeave.getIntWarp()){
                     tilledMatrix[x][y] = objWeave.getDesignMatrix()[x%objWeave.getIntWeft()][y];  
                }else if(x<objWeave.getIntWeft() && y>=objWeave.getIntWarp()){
                     tilledMatrix[x][y] = objWeave.getDesignMatrix()[x][y%objWeave.getIntWarp()];  
                }else if(x>=objWeave.getIntWeft() && y>=objWeave.getIntWarp()){
                     tilledMatrix[x][y] = objWeave.getDesignMatrix()[x%objWeave.getIntWeft()][y%objWeave.getIntWarp()];  
                }else{
                     tilledMatrix[x][y] = objWeave.getDesignMatrix()[x][y]; 
                }
            }
        }
        int a=startNumPattern; // starting number of combination
        int ch=a;
        for(int x = 0, p = 0; x < height; x++) {
            a=ch;
            for(int y = 0, q = 0; y < width; y++) {
                if(a>=totalCombinations)
                    break;
                for(int i = 0; i < bands; i++) {
                    for(int j = 0; j < bands; j++) {                        
                        if(tilledMatrix[x][y]==0){
                            if(i==0)
                                rgb=new java.awt.Color((float)javafx.scene.paint.Color.web(colorPermutation[a][colorIndex[x%intWeftCount][y%intWarpCount]]).getRed(),(float)javafx.scene.paint.Color.web(colorPermutation[a][colorIndex[x%intWeftCount][y%intWarpCount]]).getGreen(),(float)javafx.scene.paint.Color.web(colorPermutation[a][colorIndex[x%intWeftCount][y%intWarpCount]]).getBlue()).brighter().getRGB();
                            else if(i==2)
                                rgb=new java.awt.Color((float)javafx.scene.paint.Color.web(colorPermutation[a][colorIndex[x%intWeftCount][y%intWarpCount]]).getRed(),(float)javafx.scene.paint.Color.web(colorPermutation[a][colorIndex[x%intWeftCount][y%intWarpCount]]).getGreen(),(float)javafx.scene.paint.Color.web(colorPermutation[a][colorIndex[x%intWeftCount][y%intWarpCount]]).getBlue()).darker().getRGB();
                            else
                                rgb=new java.awt.Color((float)javafx.scene.paint.Color.web(colorPermutation[a][colorIndex[x%intWeftCount][y%intWarpCount]]).getRed(),(float)javafx.scene.paint.Color.web(colorPermutation[a][colorIndex[x%intWeftCount][y%intWarpCount]]).getGreen(),(float)javafx.scene.paint.Color.web(colorPermutation[a][colorIndex[x%intWeftCount][y%intWarpCount]]).getBlue()).getRGB();
                        } else if(tilledMatrix[x][y]==1){
                            if(j==0)
                                rgb=new java.awt.Color((float)javafx.scene.paint.Color.web(colorPermutation[a][colorIndex[x%intWeftCount][y%intWarpCount]]).getRed(),(float)javafx.scene.paint.Color.web(colorPermutation[a][colorIndex[x%intWeftCount][y%intWarpCount]]).getGreen(),(float)javafx.scene.paint.Color.web(colorPermutation[a][colorIndex[x%intWeftCount][y%intWarpCount]]).getBlue()).brighter().getRGB();
                            else if(j==2)
                                rgb=new java.awt.Color((float)javafx.scene.paint.Color.web(colorPermutation[a][colorIndex[x%intWeftCount][y%intWarpCount]]).getRed(),(float)javafx.scene.paint.Color.web(colorPermutation[a][colorIndex[x%intWeftCount][y%intWarpCount]]).getGreen(),(float)javafx.scene.paint.Color.web(colorPermutation[a][colorIndex[x%intWeftCount][y%intWarpCount]]).getBlue()).darker().getRGB();
                            else
                                rgb=new java.awt.Color((float)javafx.scene.paint.Color.web(colorPermutation[a][colorIndex[x%intWeftCount][y%intWarpCount]]).getRed(),(float)javafx.scene.paint.Color.web(colorPermutation[a][colorIndex[x%intWeftCount][y%intWarpCount]]).getGreen(),(float)javafx.scene.paint.Color.web(colorPermutation[a][colorIndex[x%intWeftCount][y%intWarpCount]]).getBlue()).getRGB();
                        } else {
                            if(i==0)
                                rgb = new java.awt.Color((float)javafx.scene.paint.Color.web("#FF0000").getRed(),(float)javafx.scene.paint.Color.web("#FF0000").getGreen(),(float)javafx.scene.paint.Color.web("#FF0000").getBlue()).brighter().getRGB();
                            else if(i==2)
                                rgb = new java.awt.Color((float)javafx.scene.paint.Color.web("#FF0000").getRed(),(float)javafx.scene.paint.Color.web("#FF0000").getGreen(),(float)javafx.scene.paint.Color.web("#FF0000").getBlue()).darker().getRGB();
                            else
                                rgb = new java.awt.Color((float)javafx.scene.paint.Color.web("#FF0000").getRed(),(float)javafx.scene.paint.Color.web("#FF0000").getGreen(),(float)javafx.scene.paint.Color.web("#FF0000").getBlue()).getRGB();
                        }
                        bufferedImage.setRGB(q+j, p+i, rgb);
                    }
                }
                q+=bands;
                if((y%(width/pageDiv))==0&&y!=0){
                    a++;
                }
            }
            p+=bands;
            if((x%(height/pageDiv))==0&&x!=0){
                ch+=pageDiv;
            }
        }

        Graphics2D g=bufferedImage.createGraphics();
        BasicStroke bs = new BasicStroke(5);
        g.setStroke(bs);
        for(int z=1; z<=(pageDiv-1); z++){
            g.drawLine(0, z*((height*3)/pageDiv), bufferedImage.getWidth()-1, z*((height*3)/pageDiv));
            g.drawLine(z*((width*3)/pageDiv), 0, z*((width*3)/pageDiv), bufferedImage.getHeight()-1);
        }
        
        final ImageView weaveIV=new ImageView();

        int intLength = (int)(((objWeave.getObjConfiguration().getIntDPI()*bufferedImage.getWidth())/objWeave.getIntEPI()));
        int intHeight = (int)(((objWeave.getObjConfiguration().getIntDPI()*bufferedImage.getHeight())/objWeave.getIntPPI()));
        intHeight=(int)(intHeight*(objWeave.getObjConfiguration().WIDTH/intLength));
        intLength=(int)objWeave.getObjConfiguration().WIDTH;
        BufferedImage bufferedImageesize = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g1 = bufferedImageesize.createGraphics();
        g1.drawImage(bufferedImage, 0, 0, intLength, intHeight, null);
        g1.dispose();

        weaveIV.setImage(SwingFXUtils.toFXImage(bufferedImageesize, null));        
        weaveIV.setFitHeight(objWeave.getObjConfiguration().HEIGHT - 150);
        weaveIV.setFitWidth(objWeave.getObjConfiguration().WIDTH);
        
        final double ratio=(double)(objWeave.getObjConfiguration().HEIGHT)/(double)(objWeave.getObjConfiguration().HEIGHT-150);
        weaveIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                double mouseX=t.getX();
                double mouseY=t.getY()*ratio;
                for(int i=1; i<=pageDiv; i++){
                    for(int j=1; j<=pageDiv; j++){
                        if((mouseX>=((i-1)*patternW))&&(mouseX<(i*patternW))&&(mouseY>=((j-1)*patternH))&&(mouseY<(j*patternH))){
                            if((startNumPattern+((j-1)*pageDiv)+(i-1))<totalCombinations)
                                plotShadedDesignParam((startNumPattern+((j-1)*pageDiv)+(i-1)), colorPermutation, colorIndex);
                        }
                    }
                }
            }
        });

        return weaveIV;
    }
    
    /**
     * Mapping function of warp/weft interlacement position to index of yarn color
     * present in given colors list
     * @param yarnColor List of all assigned colors (warp/weft colors treated differently)
     * @param warpColorCount Number of warp yarns in weave
     * @return mapping 2D array
     */
    private int[][] getColorIndex(ArrayList yarnColor, int warpColorCount){
        int[][] colorIndex=new int[objWeave.getIntWeft()][objWeave.getIntWarp()];
        for(int wf=0; wf<objWeave.getIntWeft(); wf++){
            for(int wp=0; wp<objWeave.getIntWarp(); wp++){
                if(objWeave.getDesignMatrix()[wf][wp]==0){ // weft up
                    for(int c=warpColorCount; c<yarnColor.size(); c++){
                        if(yarnColor.get(c).toString().substring(1).equalsIgnoreCase(objWeave.getWeftYarn()[wf].getStrYarnColor()))
                            colorIndex[wf][wp]=c;
                    }
                }
                else if(objWeave.getDesignMatrix()[wf][wp]==1){ // warp up
                    for(int c=0; c<warpColorCount; c++){
                        if(yarnColor.get(c).toString().substring(1).equalsIgnoreCase(objWeave.getWarpYarn()[wp].getStrYarnColor()))
                            colorIndex[wf][wp]=c;
                    }
                }
            }
        }
        return colorIndex;
    }

    /**
     * Eliminates duplicate combinations in colorways when same colors are assigned
     * both in warp and weft threads
     * @param colorPermutation original combinations (may contain duplicates)
     * @return combinations with duplicates eliminated
     */
    private String[][] getColorPermutationNoDuplicates(String[][] colorPermutation, int[][] colorIndex){
        int totalRows=colorPermutation.length;
        int totalColors=colorPermutation[0].length;
        String[] strFullRow = new String[totalRows];
        for(int row=0; row<totalRows; row++){
            strFullRow[row] = "";
            for(int col=0; col<totalColors; col++)
                strFullRow[row] += colorPermutation[row][col];
        }
        // now strFullRow[i] contains all concatenated coolor hexcodes for combination i

        // checking duplicates, and marking them
        byte[] marked=new byte[totalRows]; // all 0's
        int markedCount=0;
        for(int a=0; a<totalRows; a++){
            for(int b=0; b<totalRows; b++){
                // if same pattern, mark all occurences
                if(strFullRow[a].equals(strFullRow[b])){
                    if(marked[a]!=(byte)1 && a!=b){
                        marked[b]=(byte)1;
                        markedCount++;
                    }
                }
            }
        }
        
        // if pattern is different, check for circular repeat
        for(int a=0; a<totalRows; a++){
            for(int b=0; b<totalRows; b++){
                if(marked[a]!=(byte)1 && marked[b]!=(byte)1 && a!=b && isPatternCircularRepeat(a, b, colorPermutation, colorIndex)){
                    marked[b]=(byte)1;
                    markedCount++;
                }
            }
        }

        // eliminate duplicates (marked rows)
        String[][] colorPermutationUpdated=new String[totalRows-markedCount][totalColors];
        int rowCount=0;
        for(int row=0; row<totalRows; row++){
            if(marked[row]==(byte)1)
                continue;
            for(int col=0; col<totalColors; col++){
                colorPermutationUpdated[rowCount][col]=colorPermutation[row][col];
            }
            rowCount++;
        }
        return  colorPermutationUpdated;
    }
    
    private boolean is2DArrayCircularRepeat(String[][] arr1, String[][] arr2){
        if((arr1.length!=arr2.length)||(arr1[0].length!=arr2[0].length))
            return false;
        // get 2x2 repeat matrix of arr1
        String[][] repeatArr1=new String[2*arr1.length][2*arr1[0].length];
        for(int row=0; row<repeatArr1.length; row++)
            for(int col=0; col<repeatArr1[0].length; col++)
                repeatArr1[row][col]=arr1[row%arr1.length][col%arr1[0].length];
        // get a single String representation of repeatArr1
        String strRepeatArr1=getStr2DArray(repeatArr1);
        String strArr2=getStr2DArray(arr2);
        String toSearch1stRow=getStrArray(arr2[0]);
        boolean isArraySame=false;
        if(strRepeatArr1.indexOf(toSearch1stRow)!=-1){ // arr2 repeat may exist inside strRepeatArr1
            int startIndex=0;
            int index=strRepeatArr1.indexOf(toSearch1stRow);
            int lastIndex=strRepeatArr1.lastIndexOf(toSearch1stRow); // may be equal to firstIndex
            do{
                index=strRepeatArr1.indexOf(toSearch1stRow, startIndex);
                // get start row/col for array matching
                int startRow=(strRepeatArr1.indexOf(toSearch1stRow, startIndex)/7)/repeatArr1[0].length;
                int startCol=(strRepeatArr1.indexOf(toSearch1stRow, startIndex)/7)%repeatArr1[0].length; // /7 becoz 1 string in array #123456
                if((startRow+arr2.length<=repeatArr1.length)&&(startCol+arr2[0].length<=repeatArr1[0].length)){
                    isArraySame=true;
                    for(int row=0; row<arr2.length; row++)
                        for(int col=0; col<arr2[0].length; col++)
                            isArraySame&=repeatArr1[startRow+row][startCol+col].equals(arr2[row][col]);
                    if(isArraySame)
                        break;
                }
                startIndex=index+1;
            }while(index!=lastIndex);
        }
        return isArraySame;
    }
    
    private String getStr2DArray(String[][] strArray){
        StringBuffer sb=new StringBuffer();
        for(int row=0; row<strArray.length; row++)
            for(int col=0; col<strArray[0].length; col++)
                sb.append(strArray[row][col]);
        return sb.toString();
    }
    
    private String getStrArray(String[] strArray){
        StringBuffer sb=new StringBuffer();
        for(int col=0; col<strArray.length; col++)
            sb.append(strArray[col]);
        return sb.toString();
    }
    
    private boolean isPatternCircularRepeat(int a, int b, String[][] colorPermutation, int[][] colorIndex){
        // generate pattern with colors
        String[][] colorWeaveA = new String[colorIndex.length][colorIndex[0].length];
        String[][] colorWeaveB = new String[colorIndex.length][colorIndex[0].length];
        for(int rowA=0; rowA<colorWeaveA.length; rowA++)
            for(int colA=0; colA<colorWeaveA[0].length; colA++)
                colorWeaveA[rowA][colA]=colorPermutation[a][colorIndex[rowA][colA]];
        for(int rowB=0; rowB<colorWeaveB.length; rowB++)
            for(int colB=0; colB<colorWeaveB[0].length; colB++)
                colorWeaveB[rowB][colB]=colorPermutation[b][colorIndex[rowB][colB]];
        return is2DArrayCircularRepeat(colorWeaveA, colorWeaveB);
        /*
        // print patterns
        System.out.println("Weave A for #"+a);
        for(int row=0; row<colorWeaveA.length; row++){
            for(int col=0; col<colorWeaveA[0].length; col++){
                System.out.print(colorWeaveA[row][col]+" ");
            }
            System.out.println("");
        }
        System.out.println("\nWeave B for #"+b);
        for(int row=0; row<colorWeaveB.length; row++){
            for(int col=0; col<colorWeaveB[0].length; col++){
                System.out.print(colorWeaveB[row][col]+" ");
            }
            System.out.println("");
        }
        
        // compare pattern A with B for circular repeats
        // if each row (or column) of A is a circular repeat of B then mark 'b'
        
        // check all rows
        String[] fullRowA=new String[colorWeaveA.length];
        for(int row=0; row<colorWeaveA.length; row++){
            fullRowA[row] = "";
            for(int col=0; col<colorWeaveA[0].length; col++)
                fullRowA[row] += colorWeaveA[row][col];
        }
        String[] fullRowB=new String[colorWeaveB.length];
        for(int row=0; row<colorWeaveB.length; row++){
            fullRowB[row] = "";
            for(int col=0; col<colorWeaveB[0].length; col++)
                fullRowB[row] += colorWeaveB[row][col];
        }
        boolean isAllRowsCircular=true;
        for(int row=0; row<colorWeaveA.length; row++)
            isAllRowsCircular &= isStringCircularRepeat(fullRowA[row], fullRowB[row]);
        
        // check all columns
        String[] fullColA=new String[colorWeaveA[0].length];
        for(int col=0; col<colorWeaveA[0].length; col++){
            fullColA[col] = "";
            for(int row=0; row<colorWeaveA.length; row++)
                fullColA[col] += colorWeaveA[row][col];
        }
        String[] fullColB=new String[colorWeaveB[0].length];
        for(int col=0; col<colorWeaveB[0].length; col++){
            fullColB[col] = "";
            for(int row=0; row<colorWeaveB.length; row++)
                fullColB[col] += colorWeaveB[row][col];
        }
        boolean isAllColumnsCircular=true;
        for(int col=0; col<colorWeaveA[0].length; col++)
            isAllColumnsCircular &= isStringCircularRepeat(fullColA[col], fullColB[col]);
        
        if(isAllRowsCircular || isAllColumnsCircular)
            return true;
        else
            return false;
        */
    }
    
    /**
     * Checks whether str2 is a circular repeat of str1 (e.g. ABCD and DABC)
     * @param str1 First String
     * @param str2 Second String
     * @return whether str2 is a circular repeat of str1
     */
    private boolean isStringCircularRepeat(String str1, String str2){
        return (str1.length()==str2.length() && (str1+str1).indexOf(str2)!=-1);
    }
    
    /**
     * Computes and returns factorial of a given positive integer
     * @param n input positive integer
     * @return factorial of n
     */
    static int factorial(int n) {
        if (n == 0) {
            return 1;
        }
        int fact = 1;
        for (int i = 1; i <= n; i++) {
            fact *= i;
        }
        return fact;
    }

    /**
     * Generate all permutations of a given set of elements
     * @param arr Set of String elements in list form
     * @param k Index of pivot element
     * @param list List of all permutations of String elements in list form
     */
    private void permute(java.util.List<String> arr, int k, List<List<String>> list){
        for(int i = k; i < arr.size(); i++){
            java.util.Collections.swap(arr, i, k);
            permute(arr, k+1, list);
            java.util.Collections.swap(arr, k, i);
        }
        if (k == arr.size() -1){
            //System.out.println(java.util.Arrays.toString(arr.toArray()));
            List<String> l=new ArrayList(arr);
            list.add(l);
        }
    }
}
