package com.mla.weave;

import com.mla.dictionary.DictionaryAction;
import com.mla.main.Configuration;
import com.mla.main.UndoRedo;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * Edit Decompose View for Double Cloth Editing
 * @since v0.9.6
 * @author Aatif Ahmad Khan
 */
public class WeaveDecomposeView {
    
    Configuration objConfiguration;
    DictionaryAction objDictionaryAction;
    UndoRedo objUR;
    Weave objWeave;
    
    byte[][] objWeaveMatrix; // use this matrix for split view operations
    // generate combined matrix based on this
    
    byte[][] combinedWeaveMatrix;
    Label lblChange;
    
    // warpYarnPosition[i] = j; -> ith yarn position in visual grid contains the jth
    // yarn (actual) of original weave
    int[] warpYarnPosition; // map objWeaveMatrix X to actual warp yarn position
    int[] weftYarnPosition; // map objWeaveMatrix Y to actual weft yarn position
    // calculated from bottom i.e. weftYarnPosition[0] is for bottom most cell
    
    int[] reverseWarpYarnPosition; // maps actual yarn to current position on grid
    int[] reverseWeftYarnPosition;
    
    private Stage weaveStage;
    private BorderPane root;    
    private Scene scene;
    private ScrollPane container;
    
    private ScrollPane designSP;
    private ScrollPane warpColorSP;
    private ScrollPane weftColorSP;
    
    private GridPane bodyContainer;
    
    private ImageView designIV;
    private ImageView warpColorIV;
    private ImageView weftColorIV;
    
    private BufferedImage designImage;
    private BufferedImage warpColorImage;
    private BufferedImage weftColorImage;
    
    private Label lblWarpLayer;
    private Label lblWeftLayer;
    private TextField txtWarpLayer;
    private TextField txtWeftLayer;
    
    private byte[] bytWarpLayer; // 1 -> 1st Layer
    private byte[] bytWeftLayer; // 2 -> 2nd Layer (Double Cloth)
    // bytWeftLayer calculated from bottom i.e. bytWeftLayer[0] is for lowest cell
    
    final int MAX_GRID_ROW=100;
    final int MAX_GRID_COL=100;
    int activeGridRow=8;
    int activeGridCol=8;
    
    ArrayList<String> dragOverDots;
    ArrayList<Point> lstManualStitchPoints;
    
    int DIVIDER_GAP = 2;
    
    final int INT_SOLID_BLACK=-16777216;
    final int INT_SOLID_WHITE=-1;
    final int INT_SOLID_RED=-65536;
    final int INT_SOLID_GREY=-8355712;
    final int INT_TRANSPARENT=0;
    
    int dividePointX;
    int dividePointY;
    
    boolean manualStitchMode;
    String tmpWarpLayer;
    String tmpWeftLayer;
    
    public WeaveDecomposeView(Configuration objConfigurationCall, Weave objWeaveCall) {
        this.objConfiguration = objConfigurationCall;
        objDictionaryAction = new DictionaryAction(objConfiguration);
        objWeave = objWeaveCall;
        
        objWeaveMatrix = new byte[objWeaveCall.getIntWeft()][objWeaveCall.getIntWarp()];
        combinedWeaveMatrix = new byte[objWeaveCall.getIntWeft()][objWeaveCall.getIntWarp()];
        warpYarnPosition = new int[objWeaveCall.getIntWarp()];
        weftYarnPosition = new int[objWeaveCall.getIntWeft()];
        reverseWarpYarnPosition = new int[objWeaveCall.getIntWarp()];
        reverseWeftYarnPosition = new int[objWeaveCall.getIntWeft()];
        
        for(int wf=0; wf<objWeaveCall.getIntWeft(); wf++){
            weftYarnPosition[wf] = wf;
            reverseWeftYarnPosition[wf] = wf;
            for(int wp=0; wp<objWeaveCall.getIntWarp(); wp++){
                objWeaveMatrix[wf][wp] = objWeaveCall.getDesignMatrix()[wf][wp];
                combinedWeaveMatrix[wf][wp] = objWeaveCall.getDesignMatrix()[wf][wp];
                if(wf==0){
                    warpYarnPosition[wp] = wp; // will execute once for all warps
                    reverseWarpYarnPosition[wp] = wp;
                }
            }
        }
        lblChange = new Label("0");
        manualStitchMode = false;
        
        weaveStage = new Stage();
        root = new BorderPane();
        scene = new Scene(root, objConfiguration.WIDTH, objConfiguration.HEIGHT, Color.WHITE);
        scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm()); 
        
        lblWarpLayer = new Label(objDictionaryAction.getWord("WARPLAYER"));
        lblWeftLayer = new Label(objDictionaryAction.getWord("WEFTLAYER"));
        txtWarpLayer = new TextField();
        txtWeftLayer = new TextField();
        
        final Button btnSuggestStitching = new Button(objDictionaryAction.getWord("STITCHPOINTS"));
        final Button btnManualStitching = new Button(objDictionaryAction.getWord("STITCHONWEAVE"));
        btnSuggestStitching.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                suggestStitchingPoints();
            }
        });
        btnManualStitching.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if(!manualStitchMode){
                    manualStitchMode = true;
                    btnManualStitching.setStyle("-fx-border-width: 3;  -fx-border-color: black;");
                    suggestStitchingPoints();
                    // save existing layer information to load later
                    tmpWarpLayer = txtWarpLayer.getText();
                    tmpWeftLayer = txtWeftLayer.getText();
                    // update warp/weft layers to 1 for showing combined weave
                    txtWarpLayer.setText("1");
                    txtWeftLayer.setText("1");
                    txtWarpLayer.setDisable(true);
                    txtWeftLayer.setDisable(true);
                }
                else{
                    manualStitchMode = false;
                    btnManualStitching.setStyle("-fx-border-width: 0;  -fx-border-color: black;");
                    lstManualStitchPoints.clear();
                    // load warp weft layer information
                    txtWarpLayer.setText(tmpWarpLayer);
                    txtWeftLayer.setText(tmpWeftLayer);
                    txtWarpLayer.setDisable(false);
                    txtWeftLayer.setDisable(false);
                }
            }
        });
        
        // UI from WeaveView.java
        weftColorSP = new ScrollPane();
        weftColorSP.setPrefSize(10*2, objConfiguration.HEIGHT*0.75);
        weftColorSP.getStyleClass().add("subpopup");
        weftColorSP.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
        weftColorSP.setVbarPolicy(ScrollBarPolicy.NEVER);
        weftColorIV=new ImageView();
        weftColorIV.setImage(new Image("/media/GridV.png"));
        weftColorSP.setContent(weftColorIV);
        weftColorImage=SwingFXUtils.fromFXImage(weftColorIV.getImage(), null);
        weftColorSP.setVmin(0);
        weftColorSP.setVmax(weftColorIV.getImage().getHeight());
        
        warpColorSP = new ScrollPane();
        warpColorSP.setPrefSize(10*(MAX_GRID_COL+3), 10*2);
        warpColorSP.getStyleClass().add("subpopup");
        warpColorSP.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
        warpColorSP.setVbarPolicy(ScrollBarPolicy.ALWAYS);
        warpColorIV=new ImageView();
        warpColorIV.setImage(new Image("/media/GridH.png"));
        warpColorSP.setContent(warpColorIV);
        warpColorImage=SwingFXUtils.fromFXImage(warpColorIV.getImage(), null);
    
        designSP = new ScrollPane();
        designSP.setPrefSize(10*(MAX_GRID_COL+3), objConfiguration.HEIGHT*0.75);
        designSP.getStyleClass().add("subpopup");
        designSP.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
        designIV=new ImageView();
        designIV.setImage(new Image("/media/FullGrid.png"));
        designSP.setContent(designIV);
        designImage=SwingFXUtils.fromFXImage(designIV.getImage(), null);
        designIV.setPickOnBounds(true);
        designSP.setVmin(0);
        designSP.setVmax(designIV.getImage().getHeight());
        
        synchronizeScrollbars();
        
        bodyContainer=new GridPane();
        bodyContainer.setHgap(5);
        bodyContainer.setVgap(5);
        bodyContainer.setPadding(new Insets(10));
        
        bodyContainer.add(btnSuggestStitching,0,0,1,1);
        bodyContainer.add(btnManualStitching,0,1,1,1);
        bodyContainer.add(lblWarpLayer,1,0,1,1);
        bodyContainer.add(txtWarpLayer,2,0,1,1);
        bodyContainer.add(lblWeftLayer,1,1,1,1);
        bodyContainer.add(txtWeftLayer,2,1,1,1);
        
        bodyContainer.add(weftColorSP,0,2,1,1);
        bodyContainer.add(designSP,1,2,2,1);
        bodyContainer.add(warpColorSP,1,3,2,1);
        
        bodyContainer.setAlignment(Pos.CENTER);
        container = new ScrollPane();
        container.setMaxSize(objConfiguration.WIDTH, objConfiguration.HEIGHT);
        container.setContent(bodyContainer);
        root.setCenter(container);
        
        dragOverDots = new ArrayList<>();
        lstManualStitchPoints = new ArrayList<>();
        activeGridCol = dividePointX = objWeaveCall.getIntWarp();
        activeGridRow = dividePointY = objWeaveCall.getIntWeft(); // calculated from bottom
        initWarpLayer();
        initWeftLayer();
        
        refreshDesignPane();
        addDesignEventHandler();
        
        txtWarpLayer.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(validateTxtWarpLayer(t1)){
                    txtWarpLayer.setText(t1);
                    updateBytWarpLayer(t1);
                    calculateDividePointX();
                    updateWarpPosition();
                    updateObjWeaveMatrix();
                    refreshDesignPane();
                }
                else
                    txtWarpLayer.setText(t);
            }
        });
        
        txtWeftLayer.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(validateTxtWeftLayer(t1)){
                    txtWeftLayer.setText(t1);
                    updateBytWeftLayer(t1);
                    calculateDividePointY();
                    updateWeftPosition();
                    updateObjWeaveMatrix();
                    refreshDesignPane();
                }
                else
                    txtWeftLayer.setText(t);
            }
        });
        
        if(objWeave.getStrWarpLayer()!=null)
            txtWarpLayer.setText(objWeave.getStrWarpLayer());
        if(objWeave.getStrWeftLayer()!=null)
            txtWeftLayer.setText(objWeave.getStrWeftLayer());
        
        weaveStage.getIcons().add(new Image("/media/icon.png"));
        weaveStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("DOUBLECLOTH")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
        weaveStage.setX(-5);
        weaveStage.setY(0);
        weaveStage.setIconified(false);
        weaveStage.setResizable(false);
        weaveStage.setScene(scene);
        weaveStage.show();
        weaveStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                lblChange.setText("-1"); // indicates closing to caller (WeaveView.java)
                objWeave.setStrWarpLayer(txtWarpLayer.getText());
                objWeave.setStrWeftLayer(txtWeftLayer.getText());
                weaveStage.close();
                we.consume();
            }
        });
        weftColorSP.setVvalue(weftColorSP.getVmax());
        designSP.setVvalue(designSP.getVmax());
    }
    
    // Abstraction for Divider
    // Translation logic from grid point (visual) to objWeaveMatrix (visual)
    
    /**
     * Map grid point to objWeaveMatrix X value
     * @param dot Point on design grid
     * @return X value
     */
    private int getMatrixXPoint(Point dot){
        if(dot.getX()==dividePointX||dot.getX()==dividePointX+1||dot.getX()>=activeGridCol+DIVIDER_GAP)
            return -1; // in invalid area (either on divider part or outside active grid)
        if(dot.getX()>=dividePointX)
            return (int)dot.getX()-DIVIDER_GAP;
        else
            return (int)dot.getX();
    }
    
    /**
     * Map grid point to objWeaveMatrix Y value
     * @param dot Point on design grid
     * @return Y value
     */
    private int getMatrixYPoint(Point dot){
        if(dot.getY()==((MAX_GRID_ROW-1)-dividePointY)||dot.getY()==((MAX_GRID_ROW-1)-dividePointY-1)||dot.getY()<((MAX_GRID_ROW-1)-(activeGridRow-1))-DIVIDER_GAP)
            return -1; // in invalid area (either on divider part or outside active grid)
        if(((MAX_GRID_ROW-1)-dot.getY())<dividePointY)
            return (activeGridRow-1)-((MAX_GRID_ROW-1)-(int)dot.getY());
        else
            return (activeGridRow-1)-((MAX_GRID_ROW-1)-(int)dot.getY())+DIVIDER_GAP;
    }
    
    private void refreshDesignPane(){
        drawImageFromMatrix(objWeaveMatrix, designImage);
        plotWarpColor();
        plotWeftColor();
        if(manualStitchMode)
            plotManualStitchPoints();
    }
    
    private void addDesignEventHandler(){
        designIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                int mouseX=(int)t.getX();
                int mouseY=(int)t.getY();
                Point dot=getDotFromPixels(mouseX, mouseY);
                if(!isGridDotActive(dot)){
                    return;
                }
                if(t.getButton()==MouseButton.SECONDARY){
                    if(getDotColor(dot, designImage)==INT_SOLID_BLACK)
                        invertDotColor(dot, designImage);
                    //objWeaveMatrix[(objWeaveMatrix.length-1)-((MAX_GRID_ROW-1)-dot.y)][dot.x]=0;
                    objWeaveMatrix[getMatrixYPoint(dot)][getMatrixXPoint(dot)]=0;
                    refreshDesignImage();
                    return;
                }
                invertDotColor(dot, designImage);
                if(getDotColor(dot, designImage)==INT_SOLID_BLACK)
                    //objWeaveMatrix[(objWeaveMatrix.length-1)-((MAX_GRID_ROW-1)-dot.y)][dot.x]=1;
                    objWeaveMatrix[getMatrixYPoint(dot)][getMatrixXPoint(dot)]=1;
                else
                    //objWeaveMatrix[(objWeaveMatrix.length-1)-((MAX_GRID_ROW-1)-dot.y)][dot.x]=0;
                    objWeaveMatrix[getMatrixYPoint(dot)][getMatrixXPoint(dot)]=0;
                drawImageFromMatrix(objWeaveMatrix, designImage);
                refreshDesignImage();
                t.consume();
            }
        });
        
        designIV.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                int mouseX=(int)t.getX();
                int mouseY=(int)t.getY();
                Point dot=getDotFromPixels(mouseX, mouseY);
                String value=(int)dot.x+","+(int)dot.y;
                if(dragOverDots.contains(value))
                    return;
                if(isGridDotActive(dot)){
                    dragOverDots.add(value);
                    if(t.getButton()==MouseButton.SECONDARY){
                        if(getDotColor(dot, designImage)==INT_SOLID_BLACK)
                            invertDotColor(dot, designImage);
                        //objWeaveMatrix[(objWeaveMatrix.length-1)-((MAX_GRID_ROW-1)-dot.y)][dot.x]=0;
                        objWeaveMatrix[getMatrixYPoint(dot)][getMatrixXPoint(dot)]=0;
                    }
                    else if(t.getButton()==MouseButton.PRIMARY){
                        if(getDotColor(dot, designImage)==INT_SOLID_WHITE||getDotColor(dot, designImage)==INT_SOLID_RED) // red added for stitching suggest
                            invertDotColor(dot, designImage);
                        //objWeaveMatrix[(objWeaveMatrix.length-1)-((MAX_GRID_ROW-1)-dot.y)][dot.x]=1;
                        objWeaveMatrix[getMatrixYPoint(dot)][getMatrixXPoint(dot)]=1;
                    }
                    refreshDesignImage();
                }
                t.consume();
            }
        });
        designIV.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                dragOverDots.clear();
                int mouseX=(int)t.getX();
                int mouseY=(int)t.getY();
                Point dot=getDotFromPixels(mouseX, mouseY);
                if(isGridDotActive(dot)){
                    if(t.getButton()==MouseButton.PRIMARY){
                        if(getDotColor(dot, designImage)==INT_SOLID_BLACK)
                            invertDotColor(dot, designImage);
                        //objWeaveMatrix[(objWeaveMatrix.length-1)-((MAX_GRID_ROW-1)-dot.y)][dot.x]=0;
                        objWeaveMatrix[getMatrixYPoint(dot)][getMatrixXPoint(dot)]=0;
                        refreshDesignImage();
                    }
                }
                t.consume();
            }
        });
        designIV.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                /*int mouseX=(int)t.getX();
                int mouseY=(int)t.getY();
                Point dot=getDotFromPixels(mouseX, mouseY);
                if(isGridDotActive(dot)){
                    designSP.setTooltip(new Tooltip(objDictionaryAction.getWord("WARP")+": "+(dot.x+1)+", "+objDictionaryAction.getWord("WEFT")+": "+(MAX_GRID_ROW-dot.y)));
                } else{
                    designSP.setTooltip(null);//new Tooltip(objDictionaryAction.getWord("DESIGNPANE")));
                }*/
            }
        });
    }
    
    private void plotWarpColor(){
        clearImage(warpColorImage);
        Point dot=new Point();
        int intColor=-1;
        for(int i=0;i<objWeave.getIntWarp();i++) {
            intColor=getIntRgbFromColor(255
                    , Integer.parseInt(objWeave.getWarpYarn()[warpYarnPosition[i]].getStrYarnColor().substring(1, 3), 16)
                    , Integer.parseInt(objWeave.getWarpYarn()[warpYarnPosition[i]].getStrYarnColor().substring(3, 5), 16)
                    , Integer.parseInt(objWeave.getWarpYarn()[warpYarnPosition[i]].getStrYarnColor().substring(5, 7), 16));
            if(i>=dividePointX)
                dot.x=i+DIVIDER_GAP;
            else
                dot.x=i;
            dot.y=0;
            fillImageDotPixels(dot, intColor, warpColorImage);
        }
        refreshWarpColorImage();
    }
    
    private void plotWeftColor(){
        clearImage(weftColorImage);
        Point dot=new Point();
        int intColor=-1;
        for(int i=objWeave.getIntWeft()-1;i>=0;i--) {
            intColor=getIntRgbFromColor(255
                    , Integer.parseInt(objWeave.getWeftYarn()[(objWeave.getIntWeft()-1)-weftYarnPosition[(objWeave.getIntWeft()-1)-i]].getStrYarnColor().substring(1, 3), 16)
                    , Integer.parseInt(objWeave.getWeftYarn()[(objWeave.getIntWeft()-1)-weftYarnPosition[(objWeave.getIntWeft()-1)-i]].getStrYarnColor().substring(3, 5), 16)
                    , Integer.parseInt(objWeave.getWeftYarn()[(objWeave.getIntWeft()-1)-weftYarnPosition[(objWeave.getIntWeft()-1)-i]].getStrYarnColor().substring(5, 7), 16));
            dot.x=0;
            if(i>=(activeGridRow-dividePointY))
                dot.y=(MAX_GRID_ROW-1)-(activeGridRow-1)+i;
            else
                dot.y=(MAX_GRID_ROW-1)-(activeGridRow-1)+i-DIVIDER_GAP;
            fillImageDotPixels(dot, intColor, weftColorImage);
        }
        refreshWeftColorImage();
    }
    private int getIntRgbFromColor(int alpha, int red, int green, int blue){
        int rgb=alpha;
        rgb=(rgb << 8)+red;
        rgb=(rgb << 8)+green;
        rgb=(rgb << 8)+blue;
        return rgb;
    }
    
    private boolean isGridDotActive(Point p){
        //if(p.getX()>=0&&(p.getX()<=(activeGridCol-1))&&(p.getY()>=(MAX_GRID_ROW-activeGridRow))&&p.getY()<MAX_GRID_ROW)
        if(p.getX()>=0&&(p.getX()<=(activeGridCol-1)+DIVIDER_GAP)&&p.getX()!=dividePointX&&p.getX()!=(dividePointX+1)
                &&(p.getY()>=(MAX_GRID_ROW-activeGridRow)-DIVIDER_GAP)&&p.getY()<MAX_GRID_ROW&&(p.getY()!=MAX_GRID_ROW-1-dividePointY)&&(p.getY()!=MAX_GRID_ROW-1-dividePointY-1))
            return true;
        else
            return false;
    }
    
    private void drawImageFromMatrix(byte[][] matrix, BufferedImage image){
        if(matrix==null||matrix.length==0)
            return;
        clearImage(image);
        int row=matrix.length;
        int col=matrix[0].length;
        activeGridRow=row;
        activeGridCol=col;
        
        Point p=new Point();
        for(int r=0; r<row; r++){
            for(int c=0; c<col; c++){
                if(c>=dividePointX)
                    p.x=c+DIVIDER_GAP;
                else
                    p.x=c;
                if(r<(row-dividePointY))
                    p.y=((MAX_GRID_ROW-1)-((row-1)-r))-DIVIDER_GAP;
                else
                    p.y=(MAX_GRID_ROW-1)-((row-1)-r);
                if(matrix[r][c]==1)
                    fillImageDotPixels(p, INT_SOLID_BLACK, image);
                else if(matrix[r][c]==0)
                    fillImageDotPixels(p, INT_SOLID_WHITE, image);
            }
        }
        refreshDesignImage();
    }
    
    private void clearImage(BufferedImage image){
        int row=MAX_GRID_ROW;
        int col=MAX_GRID_COL;
        if(image.equals(warpColorImage))
            row=1;
        else if(image.equals(weftColorImage))
            col=1;
        
        for(int a=0; a<row; a++)
            for(int b=0; b<col; b++)
                fillImageDotPixels(new Point(b, a), INT_TRANSPARENT, image);
    }
    
    private void refreshDesignImage(){
        designIV.setImage(SwingFXUtils.toFXImage(designImage, null));
        updateWeave();
    }
    private void refreshWeftColorImage(){
        weftColorIV.setImage(SwingFXUtils.toFXImage(weftColorImage, null));
    }
    private void refreshWarpColorImage(){
        warpColorIV.setImage(SwingFXUtils.toFXImage(warpColorImage, null));
    }
    
    private void invertDotColor(Point p, BufferedImage image){
        if(p.x<0||p.y<0||p.x>=MAX_GRID_COL||p.y>=MAX_GRID_ROW)
            return;
        if(image.getRGB((int)(10*p.getX())+2, (int)(10*p.getY())+2)==INT_SOLID_BLACK)
            fillImageDotPixels(p, INT_SOLID_WHITE, image);
        else if(image.getRGB((int)(10*p.getX())+2, (int)(10*p.getY())+2)==INT_SOLID_WHITE)
            fillImageDotPixels(p, INT_SOLID_BLACK, image);
        // added for stitch point suggestion and manual stitch editing
        else if(image.getRGB((int)(10*p.getX())+2, (int)(10*p.getY())+2)==INT_SOLID_RED)
            fillImageDotPixels(p, INT_SOLID_BLACK, image);
    }    
    private int getDotColor(Point p, BufferedImage image){
        if(p.x<0||p.y<0||p.x>=MAX_GRID_COL||p.y>=MAX_GRID_ROW)
            return INT_SOLID_WHITE;
        return image.getRGB((int)(10*p.getX())+2, (int)(10*p.getY())+2);
    }    
    /**
     * Fills multiple pixels (contained in a dot point) inside an image
     * P.x: col, P.y:row of Dot Point
     * @author Aatif Ahmad Khan
     * @param p Dot point to be filled
     * @param image Image on which Dot point needs to be filled
     * @param color Fill color
     */
    private void fillImageDotPixels(Point p, int color, BufferedImage image){
        int limit=10;
        for(int a=1; a<limit-1; a++)
            for(int b=1; b<limit-1; b++)
                image.setRGB((int)(10*p.x)+b, (int)(10*p.y)+a, color);
    }
    
    private Point getDotFromPixels(int pixelX, int pixelY){
        return new Point((int)(pixelX/(10)), (int)(pixelY/(10)));
    }
    
    /**
     * Synchronizing movements of scrollbars
     */
    private void synchronizeScrollbars(){
        DoubleProperty hdPosition = new SimpleDoubleProperty();
        hdPosition.bind(designSP.hvalueProperty());
        hdPosition.addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                 warpColorSP.setHvalue((double) arg2);
            }
        }); 
        DoubleProperty hwarpPosition = new SimpleDoubleProperty();
        hwarpPosition.bind(warpColorSP.hvalueProperty());
        hwarpPosition.addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                 designSP.setHvalue((double) arg2);;
            }
        });
        DoubleProperty vdPosition = new SimpleDoubleProperty();
        vdPosition.bind(designSP.vvalueProperty());
        vdPosition.addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                 weftColorSP.setVvalue((double) arg2);
            }
        });
        DoubleProperty vweftPosition = new SimpleDoubleProperty();
        vweftPosition.bind(weftColorSP.vvalueProperty());
        vweftPosition.addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                 designSP.setVvalue((double) arg2);
            }
        });
    }
    
    // ----------- Warp/Weft Layer operations API ------------- //
    private void initWarpLayer(){
        bytWarpLayer = new byte[objWeave.getIntWarp()];
        txtWarpLayer.setText("1");
        updateBytWarpLayer(txtWarpLayer.getText());
    }
    
    private void initWeftLayer(){
        bytWeftLayer = new byte[objWeave.getIntWeft()];
        txtWeftLayer.setText("1");
        updateBytWeftLayer(txtWeftLayer.getText());
    }
    
    private boolean validateTxtWarpLayer(String test){
        return (test.length()<=objWeave.getIntWarp()&&test.matches("[1-2]+"));
    }
    
    private boolean validateTxtWeftLayer(String test){
        return (test.length()<=objWeave.getIntWeft()&&test.matches("[1-2]+"));
    }
    
    private void updateBytWarpLayer(String value){
        for(int i=0; i<objWeave.getIntWarp(); i++)
            bytWarpLayer[i] = Byte.parseByte(value.substring(i%value.length(), (i%value.length())+1));
    }
    
    private void updateBytWeftLayer(String value){
        for(int i=0; i<objWeave.getIntWeft(); i++)
            bytWeftLayer[i] = Byte.parseByte(value.substring(i%value.length(), (i%value.length())+1));
    }
    
    private void calculateDividePointX(){
        int x = objWeaveMatrix[0].length;
        for(int wp=objWeaveMatrix[0].length-1; wp>=0; wp--)
            if(bytWarpLayer[wp]==(byte)2)
                x--;
        dividePointX=x;
    }
    
    private void calculateDividePointY(){
        int y = objWeaveMatrix.length;
        for(int wf=objWeaveMatrix.length-1; wf>=0; wf--)
            if(bytWeftLayer[wf]==(byte)2)
                y--;
        dividePointY=y;
    }
    
    // Next, there should be a mapping between objWeaveMatrix to actual warp/weft
    // it should be based on bytWarpLayer and bytWeftLayer.
    
    private void updateWarpPosition(){
        int warpCountBeforeDivider=0, warpCountAfterDivider=0;
        for(int wp=0; wp<bytWarpLayer.length; wp++){
            if(bytWarpLayer[wp]==(byte)1){
                warpYarnPosition[warpCountBeforeDivider] = wp;
                reverseWarpYarnPosition[wp] = warpCountBeforeDivider;
                warpCountBeforeDivider++;
            } else if(bytWarpLayer[wp]==(byte)2){
                warpYarnPosition[dividePointX + warpCountAfterDivider] = wp;
                reverseWarpYarnPosition[wp] = dividePointX + warpCountAfterDivider;
                warpCountAfterDivider++;
            }
        }
    }
    
    private void updateWeftPosition(){
        int weftCountBeforeDivider=0, weftCountAfterDivider=0;
        for(int wf=0; wf<bytWeftLayer.length; wf++){
            if(bytWeftLayer[wf]==(byte)1){
                weftYarnPosition[weftCountBeforeDivider] = wf;
                reverseWeftYarnPosition[wf] = weftCountBeforeDivider;
                weftCountBeforeDivider++;
            } else if(bytWeftLayer[wf]==(byte)2){
                weftYarnPosition[dividePointY + weftCountAfterDivider] = wf;
                reverseWeftYarnPosition[wf] = dividePointY + weftCountAfterDivider;
                weftCountAfterDivider++;
            }
        }
    }
    
    private void updateObjWeaveMatrix(){
        byte[][] tempMatrix = new byte[objWeaveMatrix.length][objWeaveMatrix[0].length];
        for(int wf=0; wf<bytWeftLayer.length; wf++){
            for(int wp=0; wp<bytWarpLayer.length; wp++){
                tempMatrix[wf][wp]=objWeave.getDesignMatrix()[(objWeave.getIntWeft()-1)-weftYarnPosition[(objWeave.getIntWeft()-1)-wf]][warpYarnPosition[wp]];
            }
        }
        objWeaveMatrix = tempMatrix;
        tempMatrix = null;
    }
    
    private void getCombinedWeaveMatrix(){
        byte[][] tempMatrix = new byte[objWeaveMatrix.length][objWeaveMatrix[0].length];
        for(int wf=0; wf<bytWeftLayer.length; wf++){
            for(int wp=0; wp<bytWarpLayer.length; wp++){
                tempMatrix[wf][wp]=objWeaveMatrix[(objWeave.getIntWeft()-1)-reverseWeftYarnPosition[(objWeave.getIntWeft()-1)-wf]][reverseWarpYarnPosition[wp]];
            }
        }
        combinedWeaveMatrix = tempMatrix;
        tempMatrix = null;
    }
    
    private void updateWeave(){
        getCombinedWeaveMatrix();
        objWeave.setDesignMatrix(combinedWeaveMatrix);
        lblChange.setText(String.valueOf(Integer.parseInt(lblChange.getText())+1));
    }
    
    private void suggestStitchingPoints(){
        for(int row = dividePointY-1; row>=0; row--){
            for(int col = dividePointX; col<bytWarpLayer.length; col++){
                int xInCombinedMatrix = warpYarnPosition[col];
                int yInCombinedMatrix = (bytWarpLayer.length-1)-weftYarnPosition[row];
                if(combinedWeaveMatrix[yInCombinedMatrix][xInCombinedMatrix]==(byte)0){
                    // 3-sided closed (top left right) || (bottom left right)
                    if((combinedWeaveMatrix[(yInCombinedMatrix-1+bytWeftLayer.length)%bytWeftLayer.length][xInCombinedMatrix]==(byte)1
                            && combinedWeaveMatrix[yInCombinedMatrix][(xInCombinedMatrix-1+bytWarpLayer.length)%bytWarpLayer.length]==(byte)1
                            && combinedWeaveMatrix[yInCombinedMatrix][(xInCombinedMatrix+1)%bytWarpLayer.length]==(byte)1)
                            || (combinedWeaveMatrix[(yInCombinedMatrix+1)%bytWeftLayer.length][xInCombinedMatrix]==(byte)1
                            && combinedWeaveMatrix[yInCombinedMatrix][(xInCombinedMatrix-1+bytWarpLayer.length)%bytWarpLayer.length]==(byte)1
                            && combinedWeaveMatrix[yInCombinedMatrix][(xInCombinedMatrix+1)%bytWarpLayer.length]==(byte)1)){
                        Point suggestedPointOnGrid = new Point();
                        if(manualStitchMode){
                            suggestedPointOnGrid.x = xInCombinedMatrix;
                            suggestedPointOnGrid.y = (MAX_GRID_ROW-1)-(bytWeftLayer.length-1)+yInCombinedMatrix;
                            lstManualStitchPoints.add(suggestedPointOnGrid);
                        }
                        else{
                            suggestedPointOnGrid.x = col+DIVIDER_GAP;
                            suggestedPointOnGrid.y = (MAX_GRID_ROW-1)-row;
                        }
                        fillImageDotPixels(suggestedPointOnGrid, INT_SOLID_RED, designImage);
                    }
                }
            }
        }
        designIV.setImage(SwingFXUtils.toFXImage(designImage, null));
    }
    
    private void plotManualStitchPoints(){
        for(Point p: lstManualStitchPoints)
            fillImageDotPixels(p, INT_SOLID_RED, designImage);
        designIV.setImage(SwingFXUtils.toFXImage(designImage, null));
    }
}
