/*
 * Copyright (C) 2017 Media Lab Asia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mla.weave;

import com.mla.dictionary.DictionaryAction;
import com.mla.main.IDGenerator;
import com.mla.main.Logging;
import com.mla.main.MessageView;
import com.mla.print.PrintView;
import com.mla.secure.Security;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
/**
 *
 * @Designing GUI window for user preferences
 * @author Amit Kumar Singh
 * 
 */
public class WeaveViewer extends Application {
    private static Stage weaveStage;
    private BorderPane root;
    private ImageView weave;    
    private Label lblStatus;
    private ProgressBar progressB;
    private ProgressIndicator progressI;
    
    Weave objWeave = null;
    DictionaryAction objDictionaryAction = null;
    
    byte plotViewActionMode = 1; //1= composite, 2 = front, 3 = rear
 
    public WeaveViewer(final Stage primaryStage) {}
    
    public WeaveViewer(Weave objWeaveCall) {
        objWeave = objWeaveCall;
        objDictionaryAction = new DictionaryAction(objWeave.getObjConfiguration());

        weaveStage = new Stage(); 
        root = new BorderPane();
        Scene scene = new Scene(root, objWeave.getObjConfiguration().WIDTH, objWeave.getObjConfiguration().HEIGHT, Color.WHITE);
        scene.getStylesheets().add(WeaveViewer.class.getResource(objWeave.getObjConfiguration().getStrTemplate()+"/style.css").toExternalForm());

        HBox footContainer = new HBox();
        progressB = new ProgressBar(0);
        progressB.setVisible(false);
        progressI = new ProgressIndicator(0);
        progressI.setVisible(false);
        lblStatus = new Label(objDictionaryAction.getWord("WELCOMETOCADTOOL"));
        lblStatus.setId("message");
        footContainer.getChildren().addAll(lblStatus,progressB,progressI);
        footContainer.setId("footContainer");
        root.setBottom(footContainer);        
        
        HBox topContainer = new HBox(); 
        
        ToolBar toolBar = new ToolBar();
        //toolBar.setMinHeight(45);
        FlowPane flow = new FlowPane();  
        flow.setPrefWrapLength(objWeave.getObjConfiguration().WIDTH);
        flow.getStyleClass().addAll("flow");
        //Add the Buttons to the ToolBar.
        flow.setAlignment(Pos.TOP_LEFT);
        toolBar.getItems().addAll(flow);
        
        MenuButton menuSelected = new MenuButton();
        menuSelected.setGraphic(new ImageView("/media/text_menu.png"));
        menuSelected.setAlignment(Pos.CENTER_RIGHT);
        menuSelected.setPickOnBounds(false);
        menuSelected.setStyle("-fx-background-color: rgba(0,0,0,0);"); 
        
        AnchorPane toolAnchor = new AnchorPane();
        toolAnchor.getChildren().addAll(menuSelected);
        AnchorPane.setRightAnchor(menuSelected, 0.0);
        toolAnchor.setPickOnBounds(false);
        
        StackPane toolPane=new StackPane();
        toolPane.getChildren().addAll(toolBar,toolAnchor);
        
        topContainer.getChildren().add(toolPane); 
        topContainer.setId("topContainer");
        root.setTop(topContainer);
        
        // Front Texture View item;
        Button frontSideViewBtn = new Button();
        frontSideViewBtn.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/front_view.png"));
        frontSideViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("FRONTSIDEVIEW")+" (Ctrl+F1)\n"+objDictionaryAction.getWord("TOOLTIPFRONTSIDEVIEW")));
        frontSideViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        frontSideViewBtn.getStyleClass().add("toolbar-button");    
        // Rear Texture View item
        Button rearSideViewBtn = new Button();
        rearSideViewBtn.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/rear_view.png"));
        rearSideViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("REARSIDEVIEW")+" (Ctrl+F2)\n"+objDictionaryAction.getWord("TOOLTIPREARSIDEVIEW")));
        rearSideViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rearSideViewBtn.getStyleClass().add("toolbar-button");    
        // Front Visulization View item;
        Button frontVisualizationViewBtn = new Button();
        frontVisualizationViewBtn.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/front_visualization.png"));
        frontVisualizationViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("VISUALIZATIONFRONTVIEW")+" (Ctrl+F4)\n"+objDictionaryAction.getWord("TOOLTIPVISUALIZATIONFRONTVIEW")));
        frontVisualizationViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        frontVisualizationViewBtn.getStyleClass().add("toolbar-button");    
        // Rear Visaulization View item;
        Button rearVisualizationViewBtn = new Button();
        rearVisualizationViewBtn.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/rear_visualization.png"));
        rearVisualizationViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("VISUALIZATIONREARVIEW")+" (Ctrl+F5)\n"+objDictionaryAction.getWord("TOOLTIPVISUALIZATIONREARVIEW")));
        rearVisualizationViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rearVisualizationViewBtn.getStyleClass().add("toolbar-button");    
        // Switch Side View item;
        Button flipVisualizationViewBtn = new Button();
        flipVisualizationViewBtn.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/flip_visualization.png"));
        flipVisualizationViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("VISUALIZATIONFLIPVIEW")+" (Ctrl+F6)\n"+objDictionaryAction.getWord("TOOLTIPVISUALIZATIONFLIPVIEW")));
        flipVisualizationViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        flipVisualizationViewBtn.getStyleClass().add("toolbar-button");    
        // cross Section View item;
        Button frontCrossSectionViewBtn = new Button();
        frontCrossSectionViewBtn.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/front_cut.png"));
        frontCrossSectionViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CROSSSECTIONFRONTVIEW")+" (Ctrl+F7)\n"+objDictionaryAction.getWord("TOOLTIPCROSSSECTIONFRONTVIEW")));
        frontCrossSectionViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        frontCrossSectionViewBtn.getStyleClass().add("toolbar-button");    
        // cross Section View item;
        Button rearCrossSectionViewBtn = new Button();
        rearCrossSectionViewBtn.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/rear_cut.png"));
        rearCrossSectionViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CROSSSECTIONREARVIEW")+" (Ctrl+F8)\n"+objDictionaryAction.getWord("TOOLTIPCROSSSECTIONREARVIEW")));
        rearCrossSectionViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rearCrossSectionViewBtn.getStyleClass().add("toolbar-button");    
        // Grid View item
        Button gridViewBtn = new Button();
        gridViewBtn.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/grid_view.png"));
        gridViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("GRIDVIEW")+" (Ctrl+F10)\n"+objDictionaryAction.getWord("TOOLTIPGRIDVIEW")));
        gridViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        gridViewBtn.getStyleClass().add("toolbar-button");    
        // Graph View item
        Button graphViewBtn = new Button();
        graphViewBtn.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/graph_view.png"));
        graphViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("GRAPHVIEW")+" (Ctrl+F11)\n"+objDictionaryAction.getWord("TOOLTIPGRAPHVIEW")));
        graphViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        graphViewBtn.getStyleClass().add("toolbar-button");    
        // Tiled View
        Button tilledViewBtn = new Button();
        tilledViewBtn.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/tiled_view.png"));
        tilledViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("TILLEDVIEW")+" (Ctrl+F12)\n"+objDictionaryAction.getWord("TOOLTIPTILLEDVIEW")));
        tilledViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tilledViewBtn.getStyleClass().add("toolbar-button");
        // Simulation View menu
        Button simulationViewBtn = new Button();
        simulationViewBtn.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/simulation.png"));
        simulationViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SIMULATION")+" (Ctrl+Shift+F4)\n"+objDictionaryAction.getWord("TOOLTIPSIMULATION")));
        simulationViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        simulationViewBtn.getStyleClass().add("toolbar-button");    
        // mapping View menu
        Button mappingViewBtn = new Button();
        mappingViewBtn.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/profile.png"));
        mappingViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MAPPING")+" (Ctrl+Shift+F2)\n"+objDictionaryAction.getWord("TOOLTIPMAPPING")));
        mappingViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        mappingViewBtn.getStyleClass().add("toolbar-button");    
        // Ruler item
        Button yarnViewBtn = new Button();
        yarnViewBtn.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/yarn_pattern.png"));
        yarnViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("YARNVIEW")+" (Alt+Y)\n"+objDictionaryAction.getWord("TOOLTIPYARNVIEW")));
        yarnViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        yarnViewBtn.getStyleClass().add("toolbar-button");    
        // Zoom-In item
        Button zoomInViewBtn = new Button();
        zoomInViewBtn.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/zoom_in.png"));
        zoomInViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ZOOMINVIEW")+" (Ctrl+Plus)\n"+objDictionaryAction.getWord("TOOLTIPZOOMINVIEW")));
        zoomInViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        zoomInViewBtn.getStyleClass().add("toolbar-button");    
        // Normal item
        Button normalViewBtn = new Button();
        normalViewBtn.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/zoom_normal.png"));
        normalViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ZOOMNORMALVIEW")+" (Ctrl+Enter)\n"+objDictionaryAction.getWord("TOOLTIPZOOMNORMALVIEW")));
        normalViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        normalViewBtn.getStyleClass().add("toolbar-button");    
        // Zoom-Out item
        Button zoomOutViewBtn = new Button();
        zoomOutViewBtn.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/zoom_out.png"));
        zoomOutViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ZOOMOUTVIEW")+" (Ctrl+Minus)\n"+objDictionaryAction.getWord("TOOLTIPZOOMOUTVIEW")));
        zoomOutViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        zoomOutViewBtn.getStyleClass().add("toolbar-button");    
        // Save As file item
        Button saveAsFileBtn = new Button();        
        saveAsFileBtn.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/save_as.png"));
        saveAsFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SAVEASFILE")+" (Ctrl+Shift+S)\n"+objDictionaryAction.getWord("TOOLTIPSAVEASFILE")));
        saveAsFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        saveAsFileBtn.getStyleClass().addAll("toolbar-button");        
        // export file item
        Button exportFileBtn = new Button();        
        exportFileBtn.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/export.png"));
        exportFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("EXPORTFILE")+" (Ctrl+E)\n"+objDictionaryAction.getWord("TOOLTIPEXPORTFILE")));
        exportFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        exportFileBtn.getStyleClass().addAll("toolbar-button");
        // print File menu
        Button printFileBtn = new Button();        
        printFileBtn.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/print.png"));
        printFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("PRINTFILE")+" (Ctrl+P)\n"+objDictionaryAction.getWord("TOOLTIPPRINTFILE")));
        printFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        printFileBtn.getStyleClass().addAll("toolbar-button");
        // print File menu
        Button closeBtn = new Button();        
        closeBtn.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/close.png"));
        closeBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CLOSE")+" (Escape)\n"+objDictionaryAction.getWord("TOOLTIPCLOSE")));
        closeBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        closeBtn.getStyleClass().addAll("toolbar-button");
        
        MenuItem frontSideViewMI = new MenuItem(objDictionaryAction.getWord("FRONTSIDEVIEW"));
        MenuItem rearSideViewMI = new MenuItem(objDictionaryAction.getWord("REARSIDEVIEW"));
        MenuItem frontVisualizationViewMI = new MenuItem(objDictionaryAction.getWord("VISUALIZATIONFRONTVIEW"));
        MenuItem rearVisualizationViewMI = new MenuItem(objDictionaryAction.getWord("VISUALIZATIONREARVIEW"));
        MenuItem flipVisualizationViewMI = new MenuItem(objDictionaryAction.getWord("VISUALIZATIONFLIPVIEW"));
        MenuItem frontCrossSectionViewMI = new MenuItem(objDictionaryAction.getWord("CROSSSECTIONFRONTVIEW"));
        MenuItem rearCrossSectionViewMI = new MenuItem(objDictionaryAction.getWord("CROSSSECTIONREARVIEW"));
        MenuItem gridViewMI = new MenuItem(objDictionaryAction.getWord("GRIDVIEW"));
        MenuItem graphViewMI = new MenuItem(objDictionaryAction.getWord("GRAPHVIEW"));
        MenuItem tilledViewMI = new MenuItem(objDictionaryAction.getWord("TILLEDVIEW"));
        MenuItem simulationViewMI = new MenuItem(objDictionaryAction.getWord("SIMULATION"));
        MenuItem mappingViewMI = new MenuItem(objDictionaryAction.getWord("MAPPING"));
        MenuItem colourwaysViewMI = new MenuItem(objDictionaryAction.getWord("COLOURWAYS"));
        MenuItem zoomInViewMI = new MenuItem(objDictionaryAction.getWord("ZOOMINVIEW"));
        MenuItem normalViewMI = new MenuItem(objDictionaryAction.getWord("ZOOMNORMALVIEW"));
        MenuItem zoomOutViewMI = new MenuItem(objDictionaryAction.getWord("ZOOMOUTVIEW"));
        
        frontSideViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F1, KeyCombination.CONTROL_DOWN));
        rearSideViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F2, KeyCombination.CONTROL_DOWN));
        frontVisualizationViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F4, KeyCombination.CONTROL_DOWN));
        rearVisualizationViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F5, KeyCombination.CONTROL_DOWN));
        flipVisualizationViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F6, KeyCombination.CONTROL_DOWN));
        frontCrossSectionViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F7, KeyCombination.CONTROL_DOWN));
        rearCrossSectionViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F8, KeyCombination.CONTROL_DOWN));
        gridViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F10, KeyCombination.CONTROL_DOWN));
        graphViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F11, KeyCombination.CONTROL_DOWN)); 
        tilledViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F12, KeyCombination.CONTROL_DOWN));
        simulationViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F4, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        mappingViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F2, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        colourwaysViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F1, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        zoomInViewMI.setAccelerator(new KeyCodeCombination(KeyCode.PLUS, KeyCombination.CONTROL_DOWN));
        normalViewMI.setAccelerator(new KeyCodeCombination(KeyCode.ENTER, KeyCombination.CONTROL_DOWN));
        zoomOutViewMI.setAccelerator(new KeyCodeCombination(KeyCode.MINUS, KeyCombination.CONTROL_DOWN));
        
        frontSideViewMI.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/front_view.png"));
        rearSideViewMI.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/rear_view.png"));
        frontVisualizationViewMI.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/front_visualization.png"));
        rearVisualizationViewMI.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/rear_visualization.png"));
        flipVisualizationViewMI.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/flip_visualization.png"));
        frontCrossSectionViewMI.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/front_cut.png"));
        rearCrossSectionViewMI.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/rear_cut.png"));
        gridViewMI.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/grid_view.png"));
        graphViewMI.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/graph_view.png"));
        tilledViewMI.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/tiled_view.png"));
        simulationViewMI.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/simulation.png"));
        mappingViewMI.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/profile.png"));
        colourwaysViewMI.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/colourways.png"));
        zoomInViewMI.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/zoom_in.png"));
        normalViewMI.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/zoom_normal.png"));
        zoomOutViewMI.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/zoom_out.png"));
           
        MenuItem saveAsFileMI = new MenuItem(objDictionaryAction.getWord("SAVEASFILE"));
        saveAsFileMI.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        saveAsFileMI.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/save_as.png"));
        
        MenuItem exportFileMI = new MenuItem(objDictionaryAction.getWord("EXPORTFILE"));
        exportFileMI.setAccelerator(new KeyCodeCombination(KeyCode.E, KeyCombination.CONTROL_DOWN));
        exportFileMI.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/export.png"));
        
        MenuItem printFileMI = new MenuItem(objDictionaryAction.getWord("PRINTFILE"));
        printFileMI.setAccelerator(new KeyCodeCombination(KeyCode.P, KeyCombination.CONTROL_DOWN));
        printFileMI.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/print.png"));
        
        MenuItem closeMI = new MenuItem(objDictionaryAction.getWord("CLOSE"));
        closeMI.setAccelerator(new KeyCodeCombination(KeyCode.ESCAPE));
        closeMI.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/close.png"));
       
        flow.getChildren().addAll(frontSideViewBtn,rearSideViewBtn,frontVisualizationViewBtn,rearVisualizationViewBtn,zoomInViewBtn,normalViewBtn,zoomOutViewBtn,saveAsFileBtn,printFileBtn,exportFileBtn,closeBtn);
        menuSelected.getItems().addAll(frontSideViewMI,rearSideViewMI,frontVisualizationViewMI,rearVisualizationViewMI,zoomInViewMI,normalViewMI,zoomOutViewMI,saveAsFileMI,printFileMI,exportFileMI,closeMI);
    
        frontSideViewBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                plotViewActionMode = 1; 
                plotViewAction();
            }
        });
        frontSideViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                plotViewActionMode = 1; 
                plotViewAction();
            }
        });
        rearSideViewBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                plotViewActionMode = 2; 
                plotViewAction();
            }
        });
        rearSideViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                plotViewActionMode = 2; 
                plotViewAction();
            }
        });
        frontVisualizationViewBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                plotViewActionMode = 3; 
                plotViewAction();
            }
        });
        frontVisualizationViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                plotViewActionMode = 3; 
                plotViewAction();
            }
        });
        rearVisualizationViewBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                plotViewActionMode = 4; 
                plotViewAction();
            }
        });
        rearVisualizationViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                plotViewActionMode = 4; 
                plotViewAction();
            }
        });
        yarnViewBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                plotViewActionMode = 13; //1= composite, 2 = front, 3 = rear
                plotViewAction();
            }
        });
        normalViewBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                zoomNormalAction();
            }
        });
        normalViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                zoomNormalAction();
            }
        });
        zoomInViewBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                zoomInAction();
            }
        });
        zoomInViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                zoomInAction();
            }
        });
        zoomOutViewBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                zoomOutAction();
            }
        });
        zoomOutViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                zoomOutAction();
            }
        });
	saveAsFileBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                saveUpdateAction();
            }
        });
        saveAsFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                saveUpdateAction();
            }
        });
	printFileBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                printAction();
            }
        });
        printFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                printAction();
            }
        });
        exportFileBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                exportAction();
            }
        });
        exportFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                exportAction();
            }
        });
        closeBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                closeAction();
            }
        });
        closeMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
               closeAction();
            }
        });
        
        final KeyCodeCombination frontSideView = new KeyCodeCombination(KeyCode.F1, KeyCombination.CONTROL_DOWN); // front side
        final KeyCodeCombination rearSideView = new KeyCodeCombination(KeyCode.F2, KeyCombination.CONTROL_DOWN); // rear side
        final KeyCodeCombination frontVisualizationView = new KeyCodeCombination(KeyCode.F4, KeyCombination.CONTROL_DOWN); // front visualization
        final KeyCodeCombination rearVisualizationView = new KeyCodeCombination(KeyCode.F5, KeyCombination.CONTROL_DOWN); // rear visualization
        final KeyCodeCombination flipVisualizationView = new KeyCodeCombination(KeyCode.F6, KeyCombination.CONTROL_DOWN); // flip visualization
        final KeyCodeCombination frontCrossSectionView = new KeyCodeCombination(KeyCode.F7, KeyCombination.CONTROL_DOWN); // front cut
        final KeyCodeCombination rearCrossSectionView = new KeyCodeCombination(KeyCode.F8, KeyCombination.CONTROL_DOWN); // rear cut
        final KeyCodeCombination gridView = new KeyCodeCombination(KeyCode.F10, KeyCombination.CONTROL_DOWN); // grid
        final KeyCodeCombination graphView = new KeyCodeCombination(KeyCode.F11, KeyCombination.CONTROL_DOWN); // graph 
        final KeyCodeCombination tilledView = new KeyCodeCombination(KeyCode.F12, KeyCombination.CONTROL_DOWN); // tilled view
        final KeyCodeCombination simulationView = new KeyCodeCombination(KeyCode.F4, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // simulation
        final KeyCodeCombination mappingView = new KeyCodeCombination(KeyCode.F2, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // mapping
        final KeyCodeCombination colourwaysView = new KeyCodeCombination(KeyCode.F1, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // colorways
        final KeyCodeCombination zoomInView = new KeyCodeCombination(KeyCode.PLUS, KeyCombination.CONTROL_DOWN); // zoom in
        final KeyCodeCombination normalView = new KeyCodeCombination(KeyCode.ENTER, KeyCombination.CONTROL_DOWN); // zoom normal
        final KeyCodeCombination zoomOutView = new KeyCodeCombination(KeyCode.MINUS, KeyCombination.CONTROL_DOWN); // zoom out
        
        final KeyCombination saveAsFile = new KeyCodeCombination(KeyCode.S,KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN);
        final KeyCombination exportFile = new KeyCodeCombination(KeyCode.E,KeyCombination.CONTROL_DOWN);
        final KeyCombination printFile = new KeyCodeCombination(KeyCode.P,KeyCombination.CONTROL_DOWN);
        final KeyCombination close = new KeyCodeCombination(KeyCode.ESCAPE);
        
        scene.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler() {
            @Override
            public void handle(Event t) {
                if (frontSideView.match((KeyEvent) t)) {
                    plotViewActionMode = 1; 
                    plotViewAction();
                } else if (rearSideView.match((KeyEvent) t)) {
                    plotViewActionMode = 2; 
                    plotViewAction();
                } else if (frontVisualizationView.match((KeyEvent) t)) {
                    plotViewActionMode = 3; 
                    plotViewAction();
                } else if (rearVisualizationView.match((KeyEvent) t)) {
                    plotViewActionMode = 4; 
                    plotViewAction();
                } else if (zoomInView.match((KeyEvent) t)) {
                    zoomInAction();
                } else if (zoomOutView.match((KeyEvent) t)) {
                    zoomOutAction();
                } else if (normalView.match((KeyEvent) t)) {
                    zoomNormalAction();
                } else if (saveAsFile.match((KeyEvent) t)) {
                    saveUpdateAction();
                } else if (exportFile.match((KeyEvent) t)) {
                    exportAction();
                } else if (printFile.match((KeyEvent) t)) {
                    printAction();
                } else if (close.match((KeyEvent) t)) {
                    closeAction();
                }
            }
        });
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                KeyCode key = t.getCode();
                if (key == KeyCode.ESCAPE){
                    closeAction();
                } else if (key == KeyCode.DOWN){
                     zoomOutAction();
                } else if (key == KeyCode.UP){
                     zoomInAction();
                } else if (key == KeyCode.ENTER){
                     zoomNormalAction();
                }
            }
        });
        
        ScrollPane mycon = new ScrollPane();
        weave = new ImageView();
        //weaveIV.setPrefSize(objWeave.getObjConfiguration().WIDTH, objWeave.getObjConfiguration().HEIGHT);
        weave.setId("container");
        mycon.setContent(weave);
        root.setCenter(mycon);
        plotViewAction();
        
        weaveStage.getIcons().add(new Image("/media/icon.png"));
        weaveStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWSIMULATION")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
        //weaveStage.setIconified(true);
        weaveStage.setResizable(false);
        weaveStage.setScene(scene);
        weaveStage.setX(0);
        weaveStage.setY(0);
        weaveStage.show();
    }
    
    private void zoomOutAction(){
        if(weave.getScaleX()==(double)1){
            lblStatus.setText(objDictionaryAction.getWord("MAXZOOMOUT"));
            return;
        }
        weave.setScaleX(weave.getScaleX()/2);
        weave.setScaleY(weave.getScaleY()/2);
        weave.setScaleZ(weave.getScaleZ()/2);
        lblStatus.setText(objDictionaryAction.getWord("ACTIONZOOMOUTVIEW"));
    }
    
    private void zoomInAction(){
        if(weave.getScaleX()==(double)16){
            lblStatus.setText(objDictionaryAction.getWord("MAXZOOMIN"));
            return;
        }
        weave.setScaleX(weave.getScaleX()*2);
        weave.setScaleY(weave.getScaleY()*2);
        weave.setScaleZ(weave.getScaleZ()*2);
        lblStatus.setText(objDictionaryAction.getWord("ACTIONZOOMINVIEW"));
    }
    
    private void zoomNormalAction(){
        weave.setScaleX(1);
        weave.setScaleY(1);
        weave.setScaleZ(1);
        lblStatus.setText(objDictionaryAction.getWord("ACTIONZOOMNORMALVIEW"));
    }
    
    private void closeAction(){
        weaveStage.close();
        System.gc();
    }
    
    private BufferedImage getPrintImage(){
        int intHeight = (int)(objWeave.getObjConfiguration().HEIGHT/3);
        int intLength = (int)(objWeave.getObjConfiguration().WIDTH/3);
        BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
        BufferedImage srcImage=null, dstImage=null;
        try{
            WeaveAction objWeaveAction = new WeaveAction();
            if(plotViewActionMode==4) {
                bufferedImage = objWeaveAction.plotVisualizationRearView(objWeave, objWeave.getIntWarp(), objWeave.getIntWeft(), intLength, intHeight);
            } else if(plotViewActionMode==3) {
                bufferedImage = objWeaveAction.plotVisualizationFrontView(objWeave, objWeave.getIntWarp(), objWeave.getIntWeft(), intLength, intHeight);
            } else if(plotViewActionMode==2) {
                bufferedImage = objWeaveAction.plotRearSideView(objWeave, objWeave.getIntWarp(), objWeave.getIntWeft(), intLength*3, intHeight*3);
            } else {
                bufferedImage = objWeaveAction.plotFrontSideView(objWeave, objWeave.getIntWarp(), objWeave.getIntWeft(), intLength*3, intHeight*3);
            }

            srcImage = ImageIO.read(WeaveViewer.class.getResource("/media/zig_zag.png"));
            dstImage = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(), BufferedImage.TYPE_INT_RGB);
        }
        catch(IOException ioEx){
            new Logging("SEVERE",WeaveViewer.class.getName(),ioEx.toString(),ioEx);
        }
        catch(SQLException sqlEx){
            new Logging("SEVERE",WeaveViewer.class.getName(),sqlEx.toString(),sqlEx);
        }
        Graphics2D g = dstImage.createGraphics();
        g.drawImage(srcImage, 0, 0, bufferedImage.getWidth(), bufferedImage.getHeight(), null);
        g.dispose();

        srcImage = null;
        System.gc();
        for(int i = 0; i < bufferedImage.getHeight(); i++) {
            for(int j = 0; j < bufferedImage.getWidth(); j++) {
                if(dstImage.getRGB(j, i)>=-1)
                    dstImage.setRGB(j, i, dstImage.getRGB(j, i));
                else
                    dstImage.setRGB(j, i, bufferedImage.getRGB(j, i));
            }
        }
        intLength = (int)(((objWeave.getObjConfiguration().getIntDPI()*bufferedImage.getWidth())/objWeave.getIntEPI()));
        intHeight = (int)(((objWeave.getObjConfiguration().getIntDPI()*bufferedImage.getHeight())/objWeave.getIntPPI()));

        intHeight=(int)(intHeight*(objWeave.getObjConfiguration().WIDTH/intLength));
        intLength=(int)objWeave.getObjConfiguration().WIDTH;
        
        // this fixes plain weave distortion in the middle due to 1 pixel mismatch
        if(bufferedImage.getWidth()!=intLength)
            intLength=bufferedImage.getWidth();

        BufferedImage bufferedImageesize = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g1 = bufferedImageesize.createGraphics();
        g1.drawImage(dstImage, 0, 0, intLength, intHeight, null);
        g1.dispose();
        return bufferedImageesize;
    }
    
    private void printAction(){
        new PrintView(getPrintImage(), 0, objWeave.getStrWeaveName(), objWeave.getObjConfiguration().getObjUser().getStrName());
    }
    
    private void exportAction(){
        FileChooser fileChoser=new FileChooser();
        fileChoser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTPNG")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
        FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        fileChoser.getExtensionFilters().add(extFilterPNG);
        File file=fileChoser.showSaveDialog(weaveStage);
        if(file==null)
            return;
        else
            weaveStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+file.getAbsoluteFile().getName()+"]");   
        if (!file.getName().endsWith("png")) 
            file = new File(file.getPath() +".png");
        try{
            ImageIO.write(getPrintImage(), "png", file);
            lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+file.getCanonicalPath());
        } catch(IOException ioEx){
            new Logging("SEVERE",WeaveViewer.class.getName(),ioEx.toString(),ioEx);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    
    private void plotViewAction(){
        try {
            int intHeight = (int)(objWeave.getObjConfiguration().HEIGHT/3);
            int intLength = (int)(objWeave.getObjConfiguration().WIDTH/3);
            BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
    
            WeaveAction objWeaveAction = new WeaveAction();
            if(plotViewActionMode==14) {
                int factor = 6;
                java.awt.Color myColour = new java.awt.Color(255, 255, 255, 127);// 50% transparent
                bufferedImage = objWeaveAction.plotYarnView(objWeave, objWeave.getIntWarp(), objWeave.getIntWeft(), intLength, intHeight, factor, myColour);
            } else if(plotViewActionMode==4) {
                bufferedImage = objWeaveAction.plotVisualizationRearView(objWeave, objWeave.getIntWarp(), objWeave.getIntWeft(), intLength, intHeight);
            } else if(plotViewActionMode==3) {
                bufferedImage = objWeaveAction.plotVisualizationFrontView(objWeave, objWeave.getIntWarp(), objWeave.getIntWeft(), intLength, intHeight);
            } else if(plotViewActionMode==2) {
                bufferedImage = objWeaveAction.plotRearSideView(objWeave, objWeave.getIntWarp(), objWeave.getIntWeft(), intLength*3, intHeight*3);
            } else {
                bufferedImage = objWeaveAction.plotFrontSideView(objWeave, objWeave.getIntWarp(), objWeave.getIntWeft(), intLength*3, intHeight*3);
            }
            
            BufferedImage srcImage = ImageIO.read(WeaveViewer.class.getResource("/media/zig_zag.png"));
            BufferedImage dstImage = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(), BufferedImage.TYPE_INT_RGB);
            
            Graphics2D g = dstImage.createGraphics();
            g.drawImage(srcImage, 0, 0, bufferedImage.getWidth(), bufferedImage.getHeight(), null);
            g.dispose();
            
            srcImage = null;
            System.gc();
            for(int i = 0; i < bufferedImage.getHeight(); i++) {
                for(int j = 0; j < bufferedImage.getWidth(); j++) {
                    if(dstImage.getRGB(j, i)>=-1)
                        dstImage.setRGB(j, i, dstImage.getRGB(j, i));
                    else
                        dstImage.setRGB(j, i, bufferedImage.getRGB(j, i));
                }
            }
            //System.err.println("Len:"+intLength+"Ht:"+intHeight);
            intLength = (int)(((objWeave.getObjConfiguration().getIntDPI()*bufferedImage.getWidth())/objWeave.getIntEPI()));
            intHeight = (int)(((objWeave.getObjConfiguration().getIntDPI()*bufferedImage.getHeight())/objWeave.getIntPPI()));
            
            //if(intLength>objWeave.getObjConfiguration().WIDTH){
            intHeight=(int)(intHeight*(objWeave.getObjConfiguration().WIDTH/intLength));
            intLength=(int)objWeave.getObjConfiguration().WIDTH;
            // this fixes plain weave distortion in the middle due to 1 pixel mismatch
            if(bufferedImage.getWidth()!=intLength)
                intLength=bufferedImage.getWidth();
            //}
                        
            //System.err.println("Len:"+intLength+"Ht:"+intHeight);
            BufferedImage bufferedImageesize = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            Graphics2D g1 = bufferedImageesize.createGraphics();
            g1.drawImage(dstImage, 0, 0, intLength, intHeight, null);
            g1.dispose();
            
            weave.setImage(SwingFXUtils.toFXImage(bufferedImageesize, null));
        } catch (IOException ex) {
            new Logging("SEVERE",WeaveViewer.class.getName(),ex.toString(),ex);
        } catch (SQLException ex) {
            new Logging("SEVERE",WeaveViewer.class.getName(),ex.toString(),ex);
        }
    }
    public void saveUpdateAction(){
        final Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 350, 300, Color.WHITE);
        scene.getStylesheets().add(WeaveViewer.class.getResource(objWeave.getObjConfiguration().getStrTemplate()+"/style.css").toExternalForm());

        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setAlignment(Pos.CENTER);
        popup.setHgap(10);
        popup.setVgap(10);
        popup.setPadding(new Insets(25, 25, 25, 25));

        final TextField txtName = new TextField(objWeave.getStrWeaveID());
        final ComboBox cbType = new ComboBox();
        Label lblName = new Label(objDictionaryAction.getWord("NAME"));
        lblName.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPNAME")));
        popup.add(lblName, 0, 0);
        txtName.setPromptText(objDictionaryAction.getWord("TOOLTIPNAME"));
        txtName.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPNAME")));
        popup.add(txtName, 1, 0, 2, 1);
        Label lblType = new Label(objDictionaryAction.getWord("TYPE"));
        lblType.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPTYPE")));
        popup.add(lblType, 0, 1);
        cbType.getItems().addAll("plain","twill","satin","basket","sateen","other");
        cbType.setValue("other");
        cbType.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPTYPE")));
        popup.add(cbType, 1, 1, 2, 1);
        
        final ToggleGroup weaveTG = new ToggleGroup();
        RadioButton weavePublicRB = new RadioButton(objDictionaryAction.getWord("PUBLIC"));
        weavePublicRB.setToggleGroup(weaveTG);
        weavePublicRB.setUserData("Public");
        popup.add(weavePublicRB, 0, 2);
        RadioButton weavePrivateRB = new RadioButton(objDictionaryAction.getWord("PRIVATE"));
        weavePrivateRB.setToggleGroup(weaveTG);
        weavePrivateRB.setUserData("Private");
        popup.add(weavePrivateRB, 1, 2);
        RadioButton weaveProtectedRB = new RadioButton(objDictionaryAction.getWord("PROTECTED"));
        weaveProtectedRB.setToggleGroup(weaveTG);
        weaveProtectedRB.setUserData("Protected");
        popup.add(weaveProtectedRB, 2, 2);
        
        if(objWeave.getObjConfiguration().getObjUser().getUserAccess("WEAVE_LIBRARY").equalsIgnoreCase("Public"))
            weaveTG.selectToggle(weavePublicRB);
        else if(objWeave.getObjConfiguration().getObjUser().getUserAccess("WEAVE_LIBRARY").equalsIgnoreCase("Protected"))
            weaveTG.selectToggle(weaveProtectedRB);
        else
            weaveTG.selectToggle(weavePrivateRB);

        final PasswordField passPF= new PasswordField();
        final Label lblAlert = new Label();
        if(objWeave.getObjConfiguration().getBlnAuthenticateService()){
            Label lblPassword=new Label(objDictionaryAction.getWord("SERVICEPASSWORD"));
            lblPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
            popup.add(lblPassword, 0, 3);
            passPF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
            passPF.setPromptText(objDictionaryAction.getWord("PROMPTSERVICEPASSWORD"));
            popup.add(passPF, 1, 3, 2, 1);
            lblAlert.setStyle("-fx-wrap-text:true;");
            lblAlert.setPrefWidth(250);
            popup.add(lblAlert, 0, 5, 3, 1);
        }

        Button btnOK = new Button(objDictionaryAction.getWord("SAVE"));
        btnOK.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSAVE")));
        btnOK.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/save.png"));
        btnOK.setDefaultButton(true);
        btnOK.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                objWeave.setStrWeaveAccess(weaveTG.getSelectedToggle().getUserData().toString());
                if(objWeave.getObjConfiguration().getBlnAuthenticateService()){
                    if(passPF.getText().length()==0){
                        lblAlert.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                    } else{
                        if(Security.SecurePassword(passPF.getText(), objWeave.getObjConfiguration().getObjUser().getStrUsername()).equals(objWeave.getObjConfiguration().getObjUser().getStrAppPassword())){   
                            if(txtName.getText().trim().length()==0)
                                objWeave.setStrWeaveName("my weave");
                            else
                                objWeave.setStrWeaveName(txtName.getText());
                            objWeave.setStrWeaveCategory(cbType.getValue().toString());
                            saveAsAction();
                            dialogStage.close();
                        }
                        else{
                            lblAlert.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                        }
                    }
                } else{   // service password is disabled
                    if(txtName.getText().trim().length()==0)
                        objWeave.setStrWeaveName("my weave");
                    else
                        objWeave.setStrWeaveName(txtName.getText());
                    objWeave.setStrWeaveCategory(cbType.getValue().toString());
                    saveAsAction();
                    dialogStage.close();
                }
            }
        });
        popup.add(btnOK, 1, 4);
        Button btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));
        btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
        btnCancel.setGraphic(new ImageView(objWeave.getObjConfiguration().getStrColour()+"/"+objWeave.getObjConfiguration().strIconResolution+"/close.png"));
        btnCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                dialogStage.close();
        }
        });
        popup.add(btnCancel, 0, 4);
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("SAVE"));
        dialogStage.showAndWait();
    }
    
    public void saveAsAction(){
        try {
            objWeave.setBytIsLiftPlan((byte)0);
            objWeave.setStrWeaveType("wif");
            WeaveAction objWeaveAction = new WeaveAction();
            objWeave.setStrWeaveID(new IDGenerator().getIDGenerator("WEAVE_LIBRARY", objWeave.getObjConfiguration().getObjUser().getStrUserID()));
            objWeaveAction = new WeaveAction();
            if(objWeaveAction.isValidWeave(objWeave)){
                objWeaveAction.singleWeaveContent(objWeave);
                objWeaveAction.injectWeaveContent(objWeave);
                objWeaveAction.getWeaveImage(objWeave);
                //objWeaveAction.getSingleWeaveImage(objWeave);
                objWeaveAction.setWeave(objWeave);
                lblStatus.setText(objWeave.getStrWeaveName()+":"+objDictionaryAction.getWord("DATASAVED"));
            } else{
                lblStatus.setText(objWeave.getStrWeaveName()+":"+objDictionaryAction.getWord("INVALIDWEAVE"));
                new MessageView("error", objDictionaryAction.getWord("INVALIDWEAVE"), objDictionaryAction.getWord("BLANKWARPWEFT"));
            }
        } catch (Exception ex) {
            new Logging("SEVERE",WeaveView.class.getName(),ex.toString(),ex);
        }
    }

    @Override
    public void start(Stage stage) throws Exception {
        new WeaveViewer(stage);
        new Logging("WARNING",WeaveViewer.class.getName(),"UnsupportedOperationException",new UnsupportedOperationException("Not supported yet."));
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}