/*
 * Copyright (C) Digital India Corporation (Media Lab Asia)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mla.weave;

import com.mla.artwork.ArtworkAction;
import com.mla.artwork.ArtworkView;
import com.mla.cloth.ClothView;
import com.mla.dictionary.DictionaryAction;
import com.mla.fabric.FabricView;
import com.mla.main.Configuration;
import com.mla.main.EncryptZip;
import com.mla.main.IDGenerator;
import com.mla.main.Logging;
import com.mla.main.MessageView;
import com.mla.main.RadioOptionsView;
import com.mla.main.UndoRedo;
import com.mla.main.WindowView;
import com.mla.pattern.Pattern;
import com.mla.pattern.PatternAction;
import com.mla.pattern.PatternView;
import com.mla.print.PrintView;
import com.mla.secure.Security;
import com.mla.simulator.MappingEditView;
import com.mla.simulator.SimulatorEditView;
import com.mla.user.UserAction;
import com.mla.user.UserLoginView;
import com.mla.utility.AboutView;
import com.mla.utility.ContactView;
import com.mla.utility.HelpView;
import com.mla.utility.Palette;
import com.mla.utility.TechnicalView;
import com.mla.utility.UtilityAction;
import com.mla.yarn.Yarn;
import com.mla.yarn.YarnEditView;
import com.mla.yarn.YarnPaletteView;
import com.sun.media.jai.codec.ByteArraySeekableStream;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.SeekableStream;
import java.awt.BasicStroke;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.Separator;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.transform.Scale;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javax.imageio.ImageIO;
import javax.media.jai.PlanarImage;
/**
 *
 * @Designing GUI window for weave editor
 * @author Amit Kumar Singh
 * 
 */
public class WeaveView {
      
    Weave objWeave;
    WeaveAction objWeaveAction;
    Configuration objConfiguration;
    DictionaryAction objDictionaryAction;
    UndoRedo objUR;
    
    private static final String WIF_EXTENSION = ".wif";
    private static final String DRAFT_EXTENSION = ".wsml";
    static final int NUM_WEAVE_GRID = 30;
    
    private Stage weaveStage;
    private Stage weaveChildStage;
    private boolean isWeaveChildStageOn;
    private BorderPane root;    
    private Scene scene;
    private ScrollPane container;
    private ToolBar toolBar;
    private MenuButton menuSelected;
    private MenuBar menuBar;
    private GridPane bodyContainer;
    private Label lblStatus;
    private ProgressBar progressB;
    private ProgressIndicator progressI;
    
    private String selectedMenu = "FILE";
    private Button saveFileBtn;
    private Button selectEditBtn;
    private Button deleteWarpEditBtn = new Button();
    private Button deleteWeftEditBtn = new Button();
    private Button designEditBtn;
    
    private GridPane editThreadGP;
    private ScrollPane shaftSP;
    private ScrollPane paletteColorSP;
    private ScrollPane pegSP;
    private ScrollPane tieUpSP;
    private ScrollPane tradelesSP;
    private ScrollPane designSP;
    private ScrollPane dentingSP;
    private ScrollPane warpColorSP;
    private ScrollPane weftColorSP;
    
    private GridPane shaftGP;
    private GridPane paletteColorGP;
    private GridPane pegGP;
    private GridPane tieUpGP;
    private GridPane tradelesGP;
    private GridPane designGP;
    private GridPane dentingGP;
    private GridPane warpColorGP;
    private GridPane weftColorGP;
    
    private ImageView shaftIV;
    private ImageView pegIV;
    private ImageView tieUpIV;
    private ImageView tradelesIV;
    private ImageView designIV;
    private ImageView dentingIV;
    private ImageView warpColorIV;
    private ImageView weftColorIV;
    
    private BufferedImage shaftImage;
    private BufferedImage pegImage;
    private BufferedImage tieUpImage;
    private BufferedImage tradelesImage;
    private BufferedImage designImage;
    private BufferedImage dentingImage;
    private BufferedImage warpColorImage;
    private BufferedImage weftColorImage;
    
    private Menu homeMenu;
    private Menu fileMenu;
    private Menu editMenu;
    private Menu viewMenu;
    private Menu utilityMenu;
    private Menu supportMenu;
    private Menu runMenu;
    private Menu transformMenu;
    private Label fileMenuLabel;
    private Label editMenuLabel;
    private Label viewMenuLabel;
    private Label utilityMenuLabel;
    private Label runMenuLabel;
    private Label transformMenuLabel;
            
    private TextField txtWeft;
    private TextField txtWarp;
    private TextField txtShaft;
    private TextField txtTreadles;
    private TextField txtPaddle;
    private CheckBox liftplan;
    private FileChooser weaveFileName;  
    
    Yarn[] warpYarn;
    Yarn[] weftYarn;
    
    final int INT_SOLID_BLACK=-16777216;
    final int INT_SOLID_WHITE=-1;
    final int INT_SOLID_RED=-65536;
    final int INT_SOLID_GREY=-8355712;
    final int INT_TRANSPARENT=0;
    final int MAX_GRID_ROW=100;
    final int MAX_GRID_COL=100;
    int SELECTED_ROW=0;
    int SELECTED_COL=0;
    int activeGridRow=8;
    int activeGridCol=8;    
    int xindex=0;
    int yindex=0;
    int current_row=0;
    int current_col=0;
    int initial_row=0;
    int initial_col=0;
    int final_row=0;
    int final_col=0;
    int intWarpColor=0;
    int intWeftColor=26;
    int boxSize = 25;
    byte isDragBox=0;
    boolean isNew = true;
    boolean isWorkingMode = false;
    boolean isDesignMode = true;       
    boolean isRegionSelected=false;
    boolean isSelectionMode=false;
    boolean isPasteMode=false;
    double currentZoomFactor=1;
    
    private Slider hSlider;
    private Slider vSlider;
    private ScrollPane vSliderSP;
    private ScrollPane hSliderSP;
    ArrayList<String> dragOverDots;
    ArrayList<String> dentDragOverDots;
    ArrayList<String> designDragOverDots;
    String retainedWarpColor;
    String retainedWeftColor;
    Yarn retainedWarpYarn;
    Yarn retainedWeftYarn;
    byte marked[];
    // properties for binding horizontal & vertical synchronized movements of ScrollBars
    DoubleProperty hsPosition, hdPosition, hwarpPosition, hdentPosition, hSliderPosition;
    DoubleProperty vdPosition, vpPosition, vtiePosition, vtPosition, vweftPosition, vSliderPosition;
    
    public WeaveView(Configuration objConfigurationCall) {
        this.objConfiguration = objConfigurationCall;
        objDictionaryAction = new DictionaryAction(objConfiguration);
        objUR = new UndoRedo();        
        try{
            objWeaveAction = new WeaveAction(false);
        } catch (SQLException ex){
            //System.err.println(Security.SecurePassword("openweave", "admin"));
        }
        objWeave = new Weave();
        objWeave.setObjConfiguration(objConfiguration);
        objWeave.setIntEPI(objConfiguration.getIntEPI());
        objWeave.setIntPPI(objConfiguration.getIntPPI());
        //objWeave.setStrThreadPalettes(objConfiguration.strThreadPalettes);
        //objWeaveAction.populateColorPalette(objWeave);
        isWeaveChildStageOn=false;
        isSelectionMode=false;
        
        weaveStage = new Stage();
        root = new BorderPane();
        scene = new Scene(root, objConfiguration.WIDTH, objConfiguration.HEIGHT, Color.WHITE);
        scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm()); 
        resolutionControl();
        
        HBox footContainer = new HBox();
        progressB = new ProgressBar(0);
        progressB.setVisible(false);
        progressI = new ProgressIndicator(0);
        progressI.setVisible(false);
        lblStatus = new Label(objDictionaryAction.getWord("WELCOMETOCADTOOL"));
        lblStatus.setId("message");
        footContainer.getChildren().addAll(lblStatus,progressB,progressI);
        footContainer.setId("footContainer");
        root.setBottom(footContainer);
        
        VBox topContainer = new VBox();
        menuBar = new MenuBar();
        menuBar.prefWidthProperty().bind(weaveStage.widthProperty());
                
        toolBar = new ToolBar();
        toolBar.autosize();
        //toolBar.setMinHeight(45);
        menuSelected = new MenuButton();
        menuSelected.setGraphic(new ImageView("/media/text_menu.png"));
        menuSelected.setAlignment(Pos.CENTER_RIGHT);
        menuSelected.setPickOnBounds(false);
        menuSelected.setStyle("-fx-background-color: rgba(0,0,0,0);"); 
        
        AnchorPane toolAnchor = new AnchorPane();
        toolAnchor.getChildren().addAll(menuSelected);
        AnchorPane.setRightAnchor(menuSelected, 1.0);
        toolAnchor.setPickOnBounds(false);
        
        StackPane toolPane=new StackPane();
        toolPane.getChildren().addAll(toolBar,toolAnchor);
        
        topContainer.getChildren().add(menuBar);
        topContainer.getChildren().add(toolPane); 
        topContainer.setId("topContainer");
        root.setTop(topContainer);

        //menu bar and events
        homeMenu  = new Menu();
        HBox homeMenuHB = new HBox();
        homeMenuHB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/home.png"),new Label(objDictionaryAction.getWord("HOME")));
        homeMenu.setGraphic(homeMenuHB);
        homeMenu.setAccelerator(new KeyCodeCombination(KeyCode.M, KeyCombination.SHIFT_DOWN));
        homeMenuHB.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                homeMenuAction();  
                me.consume();
            }
        });       
        // File menu - new, save, save as, open, load recent.
        fileMenuLabel = new Label(objDictionaryAction.getWord("FILE"));
        fileMenu = new Menu();
        fileMenu.setGraphic(fileMenuLabel);
        fileMenu.setAccelerator(new KeyCodeCombination(KeyCode.F, KeyCombination.SHIFT_DOWN));
        fileMenuLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                fileMenuAction();            
            }
        });
        // Edit menu - Toolbar, Color, Composite View, Support Lines, Ruler, Zoom-in, zoom-out
        editMenuLabel = new Label(objDictionaryAction.getWord("EDIT"));
        editMenu = new Menu();
        editMenu.setGraphic(editMenuLabel);
        editMenu.setAccelerator(new KeyCodeCombination(KeyCode.E, KeyCombination.SHIFT_DOWN));
        editMenuLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                editMenuAction();
            }
        });  
        // View menu - Toolbar, Color, Composite View, Support Lines, Ruler, Zoom-in, zoom-out
        viewMenuLabel = new Label(objDictionaryAction.getWord("VIEW"));
        viewMenu = new Menu();
        viewMenu.setGraphic(viewMenuLabel);
        viewMenu.setAccelerator(new KeyCodeCombination(KeyCode.V, KeyCombination.SHIFT_DOWN));
        viewMenuLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                viewMenuAction();
            }
        });  
        // Utility menu - Weave, Calculation, Language
        utilityMenuLabel = new Label(objDictionaryAction.getWord("UTILITY"));
        utilityMenu = new Menu();
        utilityMenu.setGraphic(utilityMenuLabel);
        utilityMenu.setAccelerator(new KeyCodeCombination(KeyCode.U, KeyCombination.SHIFT_DOWN));
        utilityMenuLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                utilityMenuAction();
            }
        });
        //Help Menu items
        Label supportMenuLabel = new Label(objDictionaryAction.getWord("SUPPORT"));
        supportMenuLabel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUPPORT")));
        supportMenu = new Menu();
        supportMenu.setGraphic(supportMenuLabel);
        
        MenuItem helpMenuItem = new MenuItem(objDictionaryAction.getWord("HELP"));
        helpMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        helpMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.H, KeyCombination.SHIFT_DOWN));
        helpMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {       
                helpMenuAction();
            }
        });        
        MenuItem technicalMenuItem = new MenuItem(objDictionaryAction.getWord("TECHNICAL"));
        technicalMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/technical_info.png"));
        technicalMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.T, KeyCombination.SHIFT_DOWN));
        technicalMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {       
                technicalMenuAction();
            }
        });
        MenuItem aboutMenuItem = new MenuItem(objDictionaryAction.getWord("ABOUTUS"));
        aboutMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/about_software.png"));
        aboutMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.A, KeyCombination.SHIFT_DOWN));
        aboutMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                aboutMenuAction();
            }
        });
        MenuItem contactMenuItem = new MenuItem(objDictionaryAction.getWord("CONTACTUS"));
        contactMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/contact_us.png"));
        contactMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.SHIFT_DOWN));
        contactMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                contactMenuAction();
            }
        });
        MenuItem logoutMenuItem = new MenuItem(objDictionaryAction.getWord("LOGOUT"));
        logoutMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/logout.png"));
        logoutMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.L, KeyCombination.SHIFT_DOWN));
        logoutMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                logoutMenuAction();
            }
        });
        MenuItem exitMenuItem = new MenuItem(objDictionaryAction.getWord("EXIT"));
        exitMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/quit.png"));
        exitMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.ESCAPE));
        exitMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                exitMenuAction();
            }
        });     
        runMenuLabel = new Label();
        runMenuLabel.setGraphic(new ImageView("/media/run.png"));
        runMenuLabel.setTooltip(new Tooltip(objDictionaryAction.getWord("SIMULATION")));
        runMenu  = new Menu();
        runMenu.setGraphic(runMenuLabel);
        runMenu.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.SHIFT_DOWN));
        runMenuLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                runMenuAction();                
            }
        });
        // Transform Operation menu - Weave, Calculation, Language
        transformMenuLabel = new Label(objDictionaryAction.getWord("TRANSFORMOPERATIONEDIT"));
        transformMenu = new Menu();
        transformMenu.setGraphic(transformMenuLabel);
        transformMenu.hide();
        transformMenu.setVisible(false);
        transformMenu.setDisable(true);
        transformMenu.setAccelerator(new KeyCodeCombination(KeyCode.U, KeyCombination.SHIFT_DOWN));
        transformMenuLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                transformMenuAction();
            }
        });
        supportMenu.getItems().addAll(helpMenuItem, technicalMenuItem, aboutMenuItem, contactMenuItem, new SeparatorMenuItem(), logoutMenuItem); //exitMenuItem);        
        menuBar.getMenus().addAll(homeMenu, fileMenu, editMenu, viewMenu, utilityMenu, supportMenu, runMenu, transformMenu);
        
        marked=new byte[52];
    
        container = new ScrollPane();
        container.setMaxSize(objConfiguration.WIDTH, objConfiguration.HEIGHT);
               
        shaftSP = new ScrollPane(); 
        shaftSP.setPrefSize(objConfiguration.WIDTH*.60, objConfiguration.HEIGHT*0.15);
        shaftSP.getStyleClass().add("subpopup");
        //shaftSP.setId("subpopup");
        shaftSP.setTooltip(new Tooltip(objDictionaryAction.getWord("DRAFTPANE")));
        //shaftGP=new GridPane();
        //shaftGP.setAlignment(Pos.BOTTOM_LEFT);
        //shaftSP.setContent(shaftGP);
        shaftIV=new ImageView();
        shaftIV.setImage(new Image("/media/FullGrid.png"));
        shaftSP.setContent(shaftIV);
        shaftImage=SwingFXUtils.fromFXImage(shaftIV.getImage(), null);
        shaftSP.setVmin(0);
        shaftSP.setVmax(shaftIV.getImage().getHeight());
        addShaftEventHandler();
        
        dentingSP = new ScrollPane();
        dentingSP.setPrefSize(objConfiguration.WIDTH*.60, objConfiguration.HEIGHT*0.06);
        dentingSP.getStyleClass().add("subpopup");
        //dentingSP.setId("subpopup");
        dentingSP.setHbarPolicy(ScrollBarPolicy.ALWAYS);
        dentingSP.setVbarPolicy(ScrollBarPolicy.ALWAYS);
        dentingSP.setTooltip(new Tooltip(objDictionaryAction.getWord("DENTINGPANE")));
        //dentingGP=new GridPane();
        //dentingGP.setAlignment(Pos.CENTER_LEFT);
        //dentingSP.setContent(dentingGP);
        dentingIV=new ImageView();
        dentingIV.setImage(new Image("/media/Grid2.png"));
        dentingSP.setContent(dentingIV);
        dentingImage=SwingFXUtils.fromFXImage(dentingIV.getImage(), null);
        dentDragOverDots=new ArrayList<>();
        addDentingEventHandler();
        
        paletteColorSP = new ScrollPane();
        paletteColorSP.setPrefSize(objConfiguration.WIDTH*.30, objConfiguration.HEIGHT*0.21);
        paletteColorSP.getStyleClass().add("subpopup");
        //paletteColorSP.setId("subpopup");
        paletteColorGP=new GridPane();        
        paletteColorGP.setStyle("-fx-background-color:rgb(153, 171, 187); -fx-wrap-text:true;");        
        paletteColorSP.setContent(paletteColorGP);
        
        weftColorSP = new ScrollPane();
        weftColorSP.setPrefSize(objConfiguration.WIDTH*.02, objConfiguration.HEIGHT*0.42);
        weftColorSP.getStyleClass().add("subpopup");
        //weftColorSP.setId("subpopup");
        weftColorSP.setHbarPolicy(ScrollBarPolicy.ALWAYS);
        weftColorSP.setVbarPolicy(ScrollBarPolicy.NEVER);
        //weftColorSP.setTooltip(new Tooltip(objDictionaryAction.getWord("WEFTCOLORPANE")));
        //weftColorGP=new GridPane();
        //weftColorGP.setAlignment(Pos.BOTTOM_CENTER);
        //weftColorSP.setContent(weftColorGP);
        weftColorIV=new ImageView();
        weftColorIV.setImage(new Image("/media/GridV.png"));
        weftColorSP.setContent(weftColorIV);
        weftColorImage=SwingFXUtils.fromFXImage(weftColorIV.getImage(), null);
        weftColorSP.setVmin(0);
        weftColorSP.setVmax(weftColorIV.getImage().getHeight());
    
        designSP = new ScrollPane();
        designSP.setPrefSize(objConfiguration.WIDTH*.60, objConfiguration.HEIGHT*0.42);
        designSP.getStyleClass().add("subpopup");
        //designSP.setId("subpopup");
        //designSP.setTooltip(new Tooltip(objDictionaryAction.getWord("DESIGNPANE")));
        //designGP=new GridPane();  
        //designGP.setAlignment(Pos.BOTTOM_LEFT);
        //designSP.setContent(designGP);
        designIV=new ImageView();
        designIV.setImage(new Image("/media/FullGrid.png"));
        designSP.setContent(designIV);
        designImage=SwingFXUtils.fromFXImage(designIV.getImage(), null);
        
        // set initial 24x24 grid white rest as it is
        resetActiveDesignGrid(activeGridCol, activeGridRow);
        refreshDesignImage();
        designIV.setPickOnBounds(true);
        // all dragged over points will be stored in this arraylist in the form (x,y)
        dragOverDots=new ArrayList<>();
        designDragOverDots=new ArrayList<>();
        addDesignEventHandler();
        
        hSliderSP=new ScrollPane();
        hSliderSP.setPrefHeight(30);     
        hSliderSP.setMaxWidth(objConfiguration.WIDTH*0.60);
        hSliderSP.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        hSliderSP.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        hSlider=new Slider(0, MAX_GRID_COL, 1);
        hSlider.setShowTickMarks(true);
        hSlider.setMajorTickUnit(1);
        hSlider.setMinorTickCount(0);
        hSlider.setPrefWidth(10*MAX_GRID_COL);
        hSlider.setPrefHeight(30);
        hSlider.setShowTickLabels(true);
        hSlider.setValue(activeGridCol);
        hSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                if(t.intValue()==t1.intValue())
                    return;
                if(hSlider.isFocused()){
                    // do not allow drag to zero
                    if(t1.intValue()<1){
                        hSlider.setValue(1);
                        return;
                    }
                    if(t1.intValue()>MAX_GRID_COL){
                        hSlider.setValue(MAX_GRID_COL);
                        return;
                    }                    
                    updateActiveDesignGrid(t1.intValue(), activeGridRow);                    
                    System.gc();
                }
            }
        });
        //hSlider.setTooltip(new Tooltip(String.valueOf(hSlider.getValue())));
        hSliderSP.setContent(hSlider);
        
        vSliderSP=new ScrollPane();
        vSliderSP.setPrefWidth(30);
        vSliderSP.setMaxHeight(objConfiguration.HEIGHT*0.42);
        vSliderSP.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        vSliderSP.setHbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        vSlider=new Slider(0, MAX_GRID_ROW, 1);
        vSlider.setOrientation(Orientation.VERTICAL);
        vSlider.setShowTickMarks(true);
        vSlider.setMajorTickUnit(1);
        vSlider.setMinorTickCount(0);
        vSlider.setShowTickLabels(true);
        vSlider.setPrefWidth(30);
        vSlider.setPrefHeight(10*MAX_GRID_ROW);
        vSlider.setValue(activeGridRow);
        vSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                if(t.intValue()==t1.intValue())
                    return;
                if(vSlider.isFocused()){
                    // do not allow drag to zero
                    if(t1.intValue()<1){
                        vSlider.setValue(1);
                        return;
                    }
                    if(t1.intValue()>MAX_GRID_ROW){
                        vSlider.setValue(MAX_GRID_ROW);
                        return;
                    }
                    updateActiveDesignGrid(activeGridCol, t1.intValue());
                    System.gc();
                }
            }
        });
        //vSlider.setTooltip(new Tooltip(String.valueOf(vSlider.getValue())));
        vSliderSP.setContent(vSlider);
        vSliderSP.setVmax(designIV.getImage().getHeight());
        
        designSP.setVmin(0);
        designSP.setVmax(designIV.getImage().getHeight());
        
        tradelesSP = new ScrollPane();
        tradelesSP.setPrefSize(objConfiguration.WIDTH*.10, objConfiguration.HEIGHT*0.42);
        tradelesSP.getStyleClass().add("subpopup");
        //tradelesSP.setId("subpopup");
        tradelesSP.setTooltip(new Tooltip(objDictionaryAction.getWord("TRADLEPANE")));
        //tradelesGP=new GridPane();
        //tradelesGP.setAlignment(Pos.BOTTOM_LEFT);
        //tradelesSP.setContent(tradelesGP);
        tradelesIV=new ImageView();
        tradelesIV.setImage(new Image("/media/FullGrid.png"));
        tradelesSP.setContent(tradelesIV);
        tradelesImage=SwingFXUtils.fromFXImage(tradelesIV.getImage(), null);
        tradelesSP.setVmin(0);
        tradelesSP.setVmax(tradelesIV.getImage().getHeight());
        addTradeleEventHandler();
        
        tieUpSP = new ScrollPane();
        tieUpSP.setPrefSize(objConfiguration.WIDTH*.10, objConfiguration.HEIGHT*0.42);
        tieUpSP.getStyleClass().add("subpopup");
        //tieUpSP.setId("subpopup");
        tieUpSP.setTooltip(new Tooltip(objDictionaryAction.getWord("TIEUPPANE")));
        //tieUpGP=new GridPane();
        //tieUpGP.setAlignment(Pos.BOTTOM_LEFT);
        //tieUpSP.setContent(tieUpGP);
        tieUpIV=new ImageView();
        tieUpIV.setImage(new Image("/media/FullGrid.png"));
        tieUpSP.setContent(tieUpIV);
        tieUpImage=SwingFXUtils.fromFXImage(tieUpIV.getImage(), null);
        tieUpSP.setVmin(0);
        tieUpSP.setVmax(tieUpIV.getImage().getHeight());
        addTieUpEventHandler();
        
        pegSP = new ScrollPane();
        pegSP.setPrefSize(objConfiguration.WIDTH*.10, objConfiguration.HEIGHT*0.42);
        pegSP.getStyleClass().add("subpopup");
        //pegSP.setId("subpopup");
        pegSP.setTooltip(new Tooltip(objDictionaryAction.getWord("PEGPANE")));
        //pegGP=new GridPane();
        //pegGP.setAlignment(Pos.BOTTOM_LEFT);
        //pegSP.setContent(pegGP);
        pegIV=new ImageView();
        pegIV.setImage(new Image("/media/FullGrid.png"));
        pegSP.setContent(pegIV);
        pegImage=SwingFXUtils.fromFXImage(pegIV.getImage(), null);
        pegSP.setVmin(0);
        pegSP.setVmax(pegIV.getImage().getHeight());
        addPegEventHandler();
        
        warpColorSP = new ScrollPane();
        warpColorSP.setPrefSize(objConfiguration.WIDTH*.60, objConfiguration.HEIGHT*0.04);
        warpColorSP.getStyleClass().add("subpopup");
        //warpColorSP.setId("subpopup");
        warpColorSP.setHbarPolicy(ScrollBarPolicy.NEVER);
        warpColorSP.setVbarPolicy(ScrollBarPolicy.ALWAYS);
        //warpColorSP.setTooltip(new Tooltip(objDictionaryAction.getWord("WARPCOLORPANE")));
        //warpColorGP=new GridPane();
        //warpColorGP.setAlignment(Pos.CENTER_LEFT);
        //warpColorSP.setContent(warpColorGP);
        warpColorIV=new ImageView();
        warpColorIV.setImage(new Image("/media/GridH.png"));
        warpColorSP.setContent(warpColorIV);
        warpColorImage=SwingFXUtils.fromFXImage(warpColorIV.getImage(), null);
        
        warpColorIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                int mouseX=(int)t.getX();
                int mouseY=(int)t.getY();
                Point dot=getDotFromPixels(mouseX, mouseY);
                if(dot.x>=activeGridCol)
                    return;
                // To fix bug: Double click (for yarn props), original color lost
                // due to single click new color assignment
                if(t.getClickCount()>1){
                    int intColor=getIntRgbFromColor(255
                        , Integer.parseInt(retainedWarpColor.substring(1, 3), 16)
                        , Integer.parseInt(retainedWarpColor.substring(3, 5), 16)
                        , Integer.parseInt(retainedWarpColor.substring(5, 7), 16));
                    fillImageDotPixels(dot, intColor, warpColorImage);
                    objWeave.getWarpYarn()[dot.x]=retainedWarpYarn;
                    refreshImage(warpColorImage);
                    yarnPropertiesAction();
                }
                else{
                    retainedWarpColor=objWeave.getWarpYarn()[dot.x].getStrYarnColor();
                    retainedWarpYarn=objWeave.getWarpYarn()[dot.x];
                    int intColor=getIntRgbFromColor(255
                        , Integer.parseInt(objWeave.getObjConfiguration().getColourPalette()[intWarpColor].substring(0, 2), 16)
                        , Integer.parseInt(objWeave.getObjConfiguration().getColourPalette()[intWarpColor].substring(2, 4), 16)
                        , Integer.parseInt(objWeave.getObjConfiguration().getColourPalette()[intWarpColor].substring(4, 6), 16));
                    fillImageDotPixels(dot, intColor, warpColorImage);
                    objWeave.getWarpYarn()[dot.x]= objWeave.getObjConfiguration().getYarnPalette()[intWarpColor];
                    Weave objClonedWeave=cloneWeave();
                    objUR.doCommand("Warp Color Click", objClonedWeave);
                    refreshImage(warpColorImage);
                    refreshWarpPaletteMark();
                }
            }
        });
        warpColorIV.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                int mouseX=(int)t.getX();
                int mouseY=(int)t.getY();
                Point dot=getDotFromPixels(mouseX, mouseY);
                if(dot.x<activeGridCol){
                    warpColorSP.setTooltip(new Tooltip(objDictionaryAction.getWord("WARP")+": "+(dot.x+1)));
                } else{
                    warpColorSP.setTooltip(null);
                }
            }
        });
        weftColorIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                int mouseX=(int)t.getX();
                int mouseY=(int)t.getY();
                Point dot=getDotFromPixels(mouseX, mouseY);
                if(!isGridDotActive(dot))
                    return;
                if(t.getClickCount()>1){
                    int intColor=getIntRgbFromColor(255
                        , Integer.parseInt(retainedWeftColor.substring(1, 3), 16)
                        , Integer.parseInt(retainedWeftColor.substring(3, 5), 16)
                        , Integer.parseInt(retainedWeftColor.substring(5, 7), 16));
                    fillImageDotPixels(dot, intColor, weftColorImage);
                    objWeave.getWeftYarn()[((activeGridRow-1)-((MAX_GRID_ROW-1)-dot.y))]=retainedWeftYarn;
                    refreshImage(weftColorImage);
                    yarnPropertiesAction();
                }
                else{
                    retainedWeftColor=objWeave.getWeftYarn()[((activeGridRow-1)-((MAX_GRID_ROW-1)-dot.y))].getStrYarnColor();
                    retainedWeftYarn=objWeave.getWeftYarn()[((activeGridRow-1)-((MAX_GRID_ROW-1)-dot.y))];
                    int intColor=getIntRgbFromColor(255
                        , Integer.parseInt(objWeave.getObjConfiguration().getColourPalette()[intWeftColor].substring(0, 2), 16)
                        , Integer.parseInt(objWeave.getObjConfiguration().getColourPalette()[intWeftColor].substring(2, 4), 16)
                        , Integer.parseInt(objWeave.getObjConfiguration().getColourPalette()[intWeftColor].substring(4, 6), 16));
                    fillImageDotPixels(dot, intColor, weftColorImage);
                    objWeave.getWeftYarn()[((activeGridRow-1)-((MAX_GRID_ROW-1)-dot.y))]= objWeave.getObjConfiguration().getYarnPalette()[intWeftColor];
                    Weave objClonedWeave=cloneWeave();
                    objUR.doCommand("Weft Color Click", objClonedWeave);
                    refreshImage(weftColorImage);
                    refreshWeftPaletteMark();
                }
            }
        });
        weftColorIV.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                int mouseX=(int)t.getX();
                int mouseY=(int)t.getY();
                Point dot=getDotFromPixels(mouseX, mouseY);
                if(isGridDotActive(dot)){
                    weftColorSP.setTooltip(new Tooltip(objDictionaryAction.getWord("WEFT")+": "+(MAX_GRID_ROW-dot.y)));
                } else{
                    weftColorSP.setTooltip(null);
                }
            }
        });
        warpColorIV.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                int mouseX=(int)t.getX();
                int mouseY=(int)t.getY();
                Point dot=getDotFromPixels(mouseX, mouseY);
                if(dot.x>=activeGridCol||dot.x<0||dot.y!=0)
                    return;
                int intColor=getIntRgbFromColor(255
                    , Integer.parseInt(objWeave.getObjConfiguration().getColourPalette()[intWarpColor].substring(0, 2), 16)
                    , Integer.parseInt(objWeave.getObjConfiguration().getColourPalette()[intWarpColor].substring(2, 4), 16)
                    , Integer.parseInt(objWeave.getObjConfiguration().getColourPalette()[intWarpColor].substring(4, 6), 16));
                fillImageDotPixels(dot, intColor, warpColorImage);
                objWeave.getWarpYarn()[dot.x]= objWeave.getObjConfiguration().getYarnPalette()[intWarpColor];
                //Weave objClonedWeave=cloneWeave();
                //objUR.doCommand("Warp Color Drag", objClonedWeave);
                refreshImage(warpColorImage);
                refreshWarpPaletteMark();
            }
        });
        weftColorIV.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                int mouseX=(int)t.getX();
                int mouseY=(int)t.getY();
                Point dot=getDotFromPixels(mouseX, mouseY);
                //if(!isGridDotActive(dot))
                if(dot.y<(MAX_GRID_ROW-activeGridRow)||dot.y>(MAX_GRID_ROW-1)||dot.x!=0)
                    return;
                int intColor=getIntRgbFromColor(255
                    , Integer.parseInt(objWeave.getObjConfiguration().getColourPalette()[intWeftColor].substring(0, 2), 16)
                    , Integer.parseInt(objWeave.getObjConfiguration().getColourPalette()[intWeftColor].substring(2, 4), 16)
                    , Integer.parseInt(objWeave.getObjConfiguration().getColourPalette()[intWeftColor].substring(4, 6), 16));
                fillImageDotPixels(dot, intColor, weftColorImage);
                objWeave.getWeftYarn()[((activeGridRow-1)-((MAX_GRID_ROW-1)-dot.y))]= objWeave.getObjConfiguration().getYarnPalette()[intWeftColor];
                refreshImage(weftColorImage);
                refreshWeftPaletteMark();
            }
        });
        
        hsPosition = new SimpleDoubleProperty();
        hsPosition.bind(shaftSP.hvalueProperty());
        hsPosition.addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                 designSP.setHvalue((double) arg2);
                 warpColorSP.setHvalue((double) arg2);
                 dentingSP.setHvalue((double) arg2);
                 hSliderSP.setHvalue((double) arg2);
            }
        }); 
        hdPosition = new SimpleDoubleProperty();
        hdPosition.bind(designSP.hvalueProperty());
        hdPosition.addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                 shaftSP.setHvalue((double) arg2);
                 warpColorSP.setHvalue((double) arg2);
                 dentingSP.setHvalue((double) arg2);
                 hSliderSP.setHvalue((double) arg2);
            }
        }); 
        hwarpPosition = new SimpleDoubleProperty();
        hwarpPosition.bind(warpColorSP.hvalueProperty());
        hwarpPosition.addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                 shaftSP.setHvalue((double) arg2);
                 designSP.setHvalue((double) arg2);
                 dentingSP.setHvalue((double) arg2);
                 hSliderSP.setHvalue((double) arg2);
            }
        });
        hdentPosition = new SimpleDoubleProperty();
        hdentPosition.bind(dentingSP.hvalueProperty());
        hdentPosition.addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                 shaftSP.setHvalue((double) arg2);
                 warpColorSP.setHvalue((double) arg2);
                 designSP.setHvalue((double) arg2);
                 hSliderSP.setHvalue((double) arg2);
            }
        }); 
        hSliderPosition = new SimpleDoubleProperty();
        hSliderPosition.bind(hSliderSP.hvalueProperty());
        hSliderPosition.addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                 shaftSP.setHvalue((double) arg2);
                 warpColorSP.setHvalue((double) arg2);
                 designSP.setHvalue((double) arg2);
                 dentingSP.setHvalue((double) arg2);
            }
        });
        vdPosition = new SimpleDoubleProperty();
        vdPosition.bind(designSP.vvalueProperty());
        vdPosition.addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                 pegSP.setVvalue((double) arg2);
                 tieUpSP.setVvalue((double) arg2);
                 tradelesSP.setVvalue((double) arg2);
                 weftColorSP.setVvalue((double) arg2);
                 vSliderSP.setVvalue((double) arg2);
            }
        });
        vpPosition = new SimpleDoubleProperty();
        vpPosition.bind(pegSP.vvalueProperty());
        vpPosition.addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                 designSP.setVvalue((double) arg2);
                 tieUpSP.setVvalue((double) arg2);
                 tradelesSP.setVvalue((double) arg2);
                 weftColorSP.setVvalue((double) arg2);
                 vSliderSP.setVvalue((double) arg2);
            }
        });
        vtiePosition = new SimpleDoubleProperty();
        vtiePosition.bind(tieUpSP.vvalueProperty());
        vtiePosition.addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                 pegSP.setVvalue((double) arg2);
                 designSP.setVvalue((double) arg2);
                 tradelesSP.setVvalue((double) arg2);
                 weftColorSP.setVvalue((double) arg2);
                 vSliderSP.setVvalue((double) arg2);
            }
        });
        vtPosition = new SimpleDoubleProperty();
        vtPosition.bind(tradelesSP.vvalueProperty());
        vtPosition.addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                 pegSP.setVvalue((double) arg2);
                 tieUpSP.setVvalue((double) arg2);
                 designSP.setVvalue((double) arg2);
                 weftColorSP.setVvalue((double) arg2);
                 vSliderSP.setVvalue((double) arg2);
            }
        });
        vweftPosition = new SimpleDoubleProperty();
        vweftPosition.bind(weftColorSP.vvalueProperty());
        vweftPosition.addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                 pegSP.setVvalue((double) arg2);
                 tieUpSP.setVvalue((double) arg2);
                 designSP.setVvalue((double) arg2);
                 tradelesSP.setVvalue((double) arg2);
                 vSliderSP.setVvalue((double) arg2);
            }
        });
        vSliderPosition = new SimpleDoubleProperty();
        vSliderPosition.bind(vSliderSP.vvalueProperty());
        vSliderPosition.addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                 pegSP.setVvalue((double) arg2);
                 tieUpSP.setVvalue((double) arg2);
                 designSP.setVvalue((double) arg2);
                 tradelesSP.setVvalue((double) arg2);
                 weftColorSP.setVvalue((double) arg2);
            }
        });
        
        /*
        objWeave = new Weave();
        objWeave.setObjConfiguration(objConfiguration);
        objWeave.setIntEPI(objConfiguration.getIntEPI());
        objWeave.setIntPPI(objConfiguration.getIntPPI());
        */        
        bodyContainer=new GridPane();
        bodyContainer.setHgap(5);
        bodyContainer.setVgap(5);
        bodyContainer.setPadding(new Insets(10));
        
        ScrollPane dummyShaftSP = new ScrollPane();
        dummyShaftSP.setPrefSize(objConfiguration.WIDTH*.02, objConfiguration.HEIGHT*0.15);
        //dummyShaftSP.getStyleClass().add("dummypopup");
        //dummyShaftSP.setId("dummypopup");
        dummyShaftSP.setStyle("-fx-padding: 5; -fx-border-width: 2; -fx-border-color: #b1b9bf; -fx-background-color: #bbc3c9;");
        dummyShaftSP.setHbarPolicy(ScrollBarPolicy.NEVER);
        dummyShaftSP.setVbarPolicy(ScrollBarPolicy.NEVER);
        
        Label lblShaft=new Label();
        lblShaft.setText(objDictionaryAction.getWord("DRAFTPANE"));
        lblShaft.setId("message");
        lblShaft.setVisible(true);
        lblShaft.setRotate(270);
        lblShaft.setTranslateY(objConfiguration.HEIGHT*0.075);
        dummyShaftSP.setContent(lblShaft);
        
        ScrollPane dummyDentingSP = new ScrollPane();
        dummyDentingSP.setPrefSize(objConfiguration.WIDTH*.02, objConfiguration.HEIGHT*0.06);
        //dummyDentingSP.getStyleClass().add("dummypopup");
        //dummyDentingSP.setId("dummypopup");
        dummyDentingSP.setStyle("-fx-padding: 5; -fx-border-width: 2; -fx-border-color: #b1b9bf; -fx-background-color: #bbc3c9;");
        dummyDentingSP.setHbarPolicy(ScrollBarPolicy.NEVER);
        dummyDentingSP.setVbarPolicy(ScrollBarPolicy.NEVER);
        
        ScrollPane dummyPegSP = new ScrollPane();
        dummyPegSP.setPrefSize(objConfiguration.WIDTH*.10, objConfiguration.HEIGHT*0.04);
        dummyPegSP.getStyleClass().add("dummypopup");
        //dummyPegSP.setId("dummypopup");
        dummyPegSP.setHbarPolicy(ScrollBarPolicy.NEVER);
        dummyPegSP.setVbarPolicy(ScrollBarPolicy.NEVER);
        Label lblPeg=new Label();
        lblPeg.setText(objDictionaryAction.getWord("PEGPANE"));
        lblPeg.setId("message");
        lblPeg.setVisible(true);
        lblPeg.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                shufflePegPlan();
            }
        });
        dummyPegSP.setContent(lblPeg);
        
        ScrollPane dummyTieUpSP = new ScrollPane();
        dummyTieUpSP.setPrefSize(objConfiguration.WIDTH*.10, objConfiguration.HEIGHT*0.04);
        dummyTieUpSP.getStyleClass().add("dummypopup");
        //dummyTieUpSP.setId("dummypopup");
        dummyTieUpSP.setHbarPolicy(ScrollBarPolicy.NEVER);
        dummyTieUpSP.setVbarPolicy(ScrollBarPolicy.NEVER);
        Label lblTieup=new Label();
        lblTieup.setText(objDictionaryAction.getWord("TIEUPPANE"));
        lblTieup.setId("message");
        lblTieup.setVisible(true);
        dummyTieUpSP.setContent(lblTieup);
        
        ScrollPane dummyTradleSP = new ScrollPane();
        dummyTradleSP.setPrefSize(objConfiguration.WIDTH*.10, objConfiguration.HEIGHT*0.04);
        dummyTradleSP.getStyleClass().add("dummypopup");
        //dummyTradleSP.setId("dummypopup");
        dummyTradleSP.setHbarPolicy(ScrollBarPolicy.NEVER);
        dummyTradleSP.setVbarPolicy(ScrollBarPolicy.NEVER);
        Label lblTreadles=new Label();
        lblTreadles.setText(objDictionaryAction.getWord("TRADLEPANE"));
        lblTreadles.setId("message");
        lblTreadles.setVisible(true);
        dummyTradleSP.setContent(lblTreadles);
        
        //bodyContainer.add(paletteColorSP,0,0,1,5);
        
        bodyContainer.add(dummyShaftSP,1,0,2,1);
        bodyContainer.add(dummyDentingSP, 1,1,2,1);
        bodyContainer.add(weftColorSP, 1,2,1,1);
        
        bodyContainer.add(vSliderSP,2,2,1,1); // added
        bodyContainer.add(hSliderSP,3,3,1,1); // added
        
        bodyContainer.add(shaftSP,3,0,1,1);  /// col+1
        bodyContainer.add(dentingSP,3,1,1,1); /// col+1
        bodyContainer.add(designSP,3,2,1,1);  /// col+1
        bodyContainer.add(warpColorSP, 3,4,1,1); /// col+1 row+1
        
        bodyContainer.add(paletteColorSP,4,0,3,2); /// col+1
        bodyContainer.add(pegSP,4,2,1,1); /// col+1
        bodyContainer.add(tieUpSP,5,2,1,1); /// col+1
        bodyContainer.add(tradelesSP,6,2,1,1); /// col+1
        bodyContainer.add(dummyPegSP,4,3,1,2); /// col+1
        bodyContainer.add(dummyTieUpSP,5,3,1,2); /// col+1
        bodyContainer.add(dummyTradleSP,6,3,1,2);/// col+1
        
        //bodyContainer.setStyle("-fx-padding: 10; -fx-background-color: #5F5E5E;");Id("popup");
        bodyContainer.setAlignment(Pos.CENTER);
        container.setContent(bodyContainer);
        root.setCenter(container);
        
        isWorkingMode=true;
        selectedMenu = "FILE";
        menuHighlight();
        // Code Added for ShortCuts
        addAccelratorKey();
        
        weaveStage.getIcons().add(new Image("/media/icon.png"));
        weaveStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWWEAVEEDITOR")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
        weaveStage.setX(-5);
        weaveStage.setY(0);
        weaveStage.setIconified(false);
        weaveStage.setResizable(false);
        weaveStage.setScene(scene);
        weaveStage.show();   
        weaveStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                homeMenuAction();
                we.consume();
            }
        });
        initObjWeave(false);
        populateNplot();
        plotColorPalette();
        plotWarpColor();
        plotWeftColor();
        setScrollBarAtEnd();
        if(objConfiguration.getStrRecentWeave()!=null && objConfiguration.strWindowFlowContext.equalsIgnoreCase("Dashboard")){
            try {
                objWeave = new Weave();
                objWeave.setObjConfiguration(objConfiguration);
                objWeave.setIntEPI(objConfiguration.getIntEPI());
                objWeave.setIntPPI(objConfiguration.getIntPPI());
                objWeave.setStrWeaveID(objConfiguration.getStrRecentWeave());
                loadWeave();
                // added initial weave to Undo Redo Stack
                objUR.clear();
                Weave objClonedWeave=cloneWeave();
                objUR.doCommand("Initial Weave", objClonedWeave);
            } catch (Exception ex) {
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        } else{ // Initial Weave put in Undo Redo Stack
            objUR.clear();
            Weave objClonedWeave=cloneWeave();
            objUR.doCommand("Initial Weave", objClonedWeave);
        }
    }
/**
* resolutionControl(Stage)
* <p>
* This function is used for reassign width and height. 
*  
* @param       Stage primaryStage
* @author      Amit Kumar Singh
* @version     %I%, %G%
* @since       1.0
* @see         javafx.stage.*;
* @link        WindowView
*/
    private void resolutionControl(){
        //windowStage.resizableProperty().addListener(listener);
        scene.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                setGlobalWidthHeight();
                weaveStage.setHeight(objConfiguration.HEIGHT);
                weaveStage.setWidth(objConfiguration.WIDTH);
                //selectedMenu = "FILE";
                menuHighlight();
            }
        });
        scene.heightProperty().addListener(new ChangeListener<Number>() {
            @Override 
            public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneHeight, Number newSceneHeight) {
                setGlobalWidthHeight();
                weaveStage.setHeight(objConfiguration.HEIGHT);
                weaveStage.setWidth(objConfiguration.WIDTH);
                //selectedMenu = "FILE";
                menuHighlight();
            }
        });
    }
    private void setGlobalWidthHeight(){
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        objConfiguration.WIDTH = screenSize.getWidth();
        objConfiguration.HEIGHT = screenSize.getHeight();
        objConfiguration.strIconResolution = (objConfiguration.WIDTH<1280)?"hd_none":(objConfiguration.WIDTH<1920)?"hd":(objConfiguration.WIDTH<2560)?"hd_full":"hd_quad";
    }
/**
     * addAccelratorKey
     * <p>
     * Function use for adding shortcut key combinations. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void addAccelratorKey(){
        // shortcuts variable names: kc + Alphabet + ALT|CONTROL|SHIFT // ACS alphabetical
        final KeyCodeCombination homeMenu = new KeyCodeCombination(KeyCode.M, KeyCombination.SHIFT_DOWN); // Home Menu
        final KeyCodeCombination fileMenu = new KeyCodeCombination(KeyCode.F, KeyCombination.SHIFT_DOWN); // File Menu
        final KeyCodeCombination editMenu = new KeyCodeCombination(KeyCode.E, KeyCombination.SHIFT_DOWN); // Edit Menu
        final KeyCodeCombination viewMenu = new KeyCodeCombination(KeyCode.V, KeyCombination.SHIFT_DOWN); // View Menu
        final KeyCodeCombination utilityMenu = new KeyCodeCombination(KeyCode.U, KeyCombination.SHIFT_DOWN); // Utility Menu
        //utility menu items
        final KeyCodeCombination infoUtility = new KeyCodeCombination(KeyCode.I, KeyCombination.SHIFT_DOWN); // info Menu
        final KeyCodeCombination artworkUtility = new KeyCodeCombination(KeyCode.D, KeyCombination.SHIFT_DOWN); // artwork
        final KeyCodeCombination fabricUtility = new KeyCodeCombination(KeyCode.J, KeyCombination.SHIFT_DOWN); // fabric
        final KeyCodeCombination clothUtility = new KeyCodeCombination(KeyCode.G, KeyCombination.SHIFT_DOWN); // cloth
        //view menu items
        final KeyCodeCombination frontSideView = new KeyCodeCombination(KeyCode.F1, KeyCombination.CONTROL_DOWN); // front side
        final KeyCodeCombination rearSideView = new KeyCodeCombination(KeyCode.F2, KeyCombination.CONTROL_DOWN); // rear side
        final KeyCodeCombination frontVisualizationView = new KeyCodeCombination(KeyCode.F4, KeyCombination.CONTROL_DOWN); // front visualization
        final KeyCodeCombination rearVisualizationView = new KeyCodeCombination(KeyCode.F5, KeyCombination.CONTROL_DOWN); // rear visualization
        final KeyCodeCombination flipVisualizationView = new KeyCodeCombination(KeyCode.F6, KeyCombination.CONTROL_DOWN); // flip visualization
        final KeyCodeCombination frontCrossSectionView = new KeyCodeCombination(KeyCode.F7, KeyCombination.CONTROL_DOWN); // front cut
        final KeyCodeCombination rearCrossSectionView = new KeyCodeCombination(KeyCode.F8, KeyCombination.CONTROL_DOWN); // rear cut
        final KeyCodeCombination gridView = new KeyCodeCombination(KeyCode.F10, KeyCombination.CONTROL_DOWN); // grid
        final KeyCodeCombination graphView = new KeyCodeCombination(KeyCode.F11, KeyCombination.CONTROL_DOWN); // graph 
        final KeyCodeCombination tilledView = new KeyCodeCombination(KeyCode.F12, KeyCombination.CONTROL_DOWN); // tilled view
        final KeyCodeCombination simulationView = new KeyCodeCombination(KeyCode.F4, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // simulation
        final KeyCodeCombination mappingView = new KeyCodeCombination(KeyCode.F2, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // mapping
        final KeyCodeCombination colourwaysView = new KeyCodeCombination(KeyCode.F1, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // colorways
        final KeyCodeCombination zoomInView = new KeyCodeCombination(KeyCode.EQUALS, KeyCombination.CONTROL_DOWN); // zoom in
        final KeyCodeCombination normalView = new KeyCodeCombination(KeyCode.ENTER, KeyCombination.CONTROL_DOWN); // zoom normal
        final KeyCodeCombination zoomOutView = new KeyCodeCombination(KeyCode.MINUS, KeyCombination.CONTROL_DOWN); // zoom out
        //Edit menu items
        final KeyCodeCombination undoEdit = new KeyCodeCombination(KeyCode.Z, KeyCombination.CONTROL_DOWN); // undo
        final KeyCodeCombination redoEdit = new KeyCodeCombination(KeyCode.Y, KeyCombination.CONTROL_DOWN); // redo
        final KeyCodeCombination designEdit = new KeyCodeCombination(KeyCode.F10, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // design mode
        final KeyCodeCombination insertWarpEdit = new KeyCodeCombination(KeyCode.INSERT, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN);
        final KeyCodeCombination deleteWarpEdit = new KeyCodeCombination(KeyCode.DELETE, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN);
        final KeyCodeCombination insertWeftEdit = new KeyCodeCombination(KeyCode.INSERT, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN);
        final KeyCodeCombination deleteWeftEdit = new KeyCodeCombination(KeyCode.DELETE, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN);
        final KeyCodeCombination selectEdit = new KeyCodeCombination(KeyCode.A, KeyCombination.CONTROL_DOWN); // redo
        final KeyCodeCombination cropEdit = new KeyCodeCombination(KeyCode.W, KeyCombination.CONTROL_DOWN); // redo
        final KeyCodeCombination copyEdit = new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_DOWN); // redo
        final KeyCodeCombination cutEdit = new KeyCodeCombination(KeyCode.X, KeyCombination.CONTROL_DOWN); // redo
        final KeyCodeCombination pasteEdit = new KeyCodeCombination(KeyCode.V, KeyCombination.CONTROL_DOWN); // undo
        final KeyCodeCombination mirrorVerticalEdit = new KeyCodeCombination(KeyCode.V, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN);
        final KeyCodeCombination mirrorHorizontalEdit = new KeyCodeCombination(KeyCode.H, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN);
        final KeyCodeCombination jacquardConversionEdit = new KeyCodeCombination(KeyCode.J, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN);
        final KeyCodeCombination complexWeaveEdit = new KeyCodeCombination(KeyCode.J, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN);
        final KeyCodeCombination repeatSelectedEdit = new KeyCodeCombination(KeyCode.R, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN);
        final KeyCodeCombination yarnEdit = new KeyCodeCombination(KeyCode.Y, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // edit yarn
        final KeyCodeCombination threadPatternEdit = new KeyCodeCombination(KeyCode.T, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // thread pattern
        final KeyCodeCombination switchColorEdit = new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // switch color
        final KeyCodeCombination repeatEdit = new KeyCodeCombination(KeyCode.R, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN); // repeats
        final KeyCodeCombination sprayToolEdit = new KeyCodeCombination(KeyCode.Q, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN); // spray
        final KeyCodeCombination transformOperationEdit = new KeyCodeCombination(KeyCode.W, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // weaving pattern
        // Transform Menu Items
        final KeyCodeCombination vMirrorTransform = new KeyCodeCombination(KeyCode.V, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN); // vertical mirror
        final KeyCodeCombination hMirrorTransform = new KeyCodeCombination(KeyCode.H, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN); // horizontal mirror
        final KeyCodeCombination clockwiseRotateTransform = new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN); // clockwise rotation
        final KeyCodeCombination antiClockwiseRotateTransform = new KeyCodeCombination(KeyCode.A, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN); // anti-clockwise rotation
        final KeyCodeCombination moveRightTransform = new KeyCodeCombination(KeyCode.RIGHT, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // move right
        final KeyCodeCombination moveLeftTransform = new KeyCodeCombination(KeyCode.LEFT, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // move left
        final KeyCodeCombination moveUpTransform = new KeyCodeCombination(KeyCode.UP, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // move up
        final KeyCodeCombination moveDownTransform = new KeyCodeCombination(KeyCode.DOWN, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // move down
        final KeyCodeCombination move8RightTransform = new KeyCodeCombination(KeyCode.RIGHT, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN); // move 8 right
        final KeyCodeCombination move8LeftTransform = new KeyCodeCombination(KeyCode.LEFT, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN); // move 8 left
        final KeyCodeCombination move8UpTransform = new KeyCodeCombination(KeyCode.UP, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN); // move 8 up
        final KeyCodeCombination move8DownTransform = new KeyCodeCombination(KeyCode.DOWN, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN); // move 8 down
        final KeyCodeCombination tiltRightTransform = new KeyCodeCombination(KeyCode.RIGHT, KeyCombination.ALT_DOWN, KeyCombination.CONTROL_DOWN); // tilt right
        final KeyCodeCombination tiltLeftTransform = new KeyCodeCombination(KeyCode.LEFT, KeyCombination.ALT_DOWN, KeyCombination.CONTROL_DOWN); // tilt left
        final KeyCodeCombination tiltUpTransform = new KeyCodeCombination(KeyCode.UP, KeyCombination.ALT_DOWN, KeyCombination.CONTROL_DOWN); // tilt up
        final KeyCodeCombination tiltDownTransform = new KeyCodeCombination(KeyCode.DOWN, KeyCombination.ALT_DOWN, KeyCombination.CONTROL_DOWN); // tilt down
        final KeyCodeCombination invertTransform = new KeyCodeCombination(KeyCode.I, KeyCombination.ALT_DOWN, KeyCombination.CONTROL_DOWN); // inversion
        final KeyCodeCombination clearTransform = new KeyCodeCombination(KeyCode.BACK_SPACE, KeyCombination.CONTROL_DOWN); // clear
        final KeyCodeCombination closeTransform = new KeyCodeCombination(KeyCode.ESCAPE); // close
        //File menu items
        final KeyCodeCombination newFile = new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN); // new
        final KeyCodeCombination openFile = new KeyCodeCombination(KeyCode.O, KeyCombination.CONTROL_DOWN); // open
        final KeyCodeCombination loadFile = new KeyCodeCombination(KeyCode.L, KeyCombination.CONTROL_DOWN); // load recent
        final KeyCodeCombination importFile = new KeyCodeCombination(KeyCode.I, KeyCombination.CONTROL_DOWN); // import
        final KeyCodeCombination saveFile = new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN); // save
        final KeyCodeCombination saveAsFile = new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // save as
        final KeyCodeCombination exportFile = new KeyCodeCombination(KeyCode.E, KeyCombination.CONTROL_DOWN); // export
        final KeyCodeCombination saveXMLFile = new KeyCodeCombination(KeyCode.X, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN); // save xml
        final KeyCodeCombination printFile = new KeyCodeCombination(KeyCode.P, KeyCombination.CONTROL_DOWN); // print
        
        scene.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                if(homeMenu.match(t))
                    homeMenuAction();
                else if(fileMenu.match(t))
                    fileMenuAction();
                else if(editMenu.match(t))
                    editMenuAction();
                else if(viewMenu.match(t))
                    viewMenuAction();
                else if(utilityMenu.match(t))
                    utilityMenuAction();
                else if(infoUtility.match(t) && isWorkingMode)
                    populateProperties();
                else if(artworkUtility.match(t) && isWorkingMode)
                    artworkUtilityAction();
                else if(fabricUtility.match(t) && isWorkingMode)
                    fabricUtilityAction();
                else if(clothUtility.match(t) && isWorkingMode)
                    clothUtilityAction();
                else if(frontSideView.match(t) && isWorkingMode)
                    frontSideViewAction();
                else if(rearSideView.match(t) && isWorkingMode)
                    frontSideViewAction();
                    //rearSideViewAction();
                else if(frontVisualizationView.match(t) && isWorkingMode)
                    frontVisualizationViewAction();
                else if(rearVisualizationView.match(t) && isWorkingMode)
                    rearVisualizationViewAction();
                else if(gridView.match(t) && isWorkingMode)
                    gridViewAction();
                else if(graphView.match(t) && isWorkingMode)
                    graphViewAction();
                else if(tilledView.match(t) && isWorkingMode)
                    tilledViewAction();
                else if(simulationView.match(t) && isWorkingMode)
                    simulationViewAction();
                /*else if(mappingView.match(t) && isWorkingMode)
                    mappingViewAction();*/
                else if(colourwaysView.match(t) && isWorkingMode)
                    colourwaysAction();
                else if(zoomInView.match(t))
                    zoomInAction();
                else if(normalView.match(t))
                    ZoomNormalAction();
                else if(zoomOutView.match(t))
                    zoomOutAction();
                else if(undoEdit.match(t) && isWorkingMode)
                    undoAction();
                else if(redoEdit.match(t) && isWorkingMode)
                    redoAction();                
                else if(designEdit.match(t) && isWorkingMode)
                    editingModeSelection();
                else if(insertWarpEdit.match(t) && isWorkingMode)
                    insertWarpAction();
                else if(deleteWarpEdit.match(t) && isWorkingMode)
                    deleteWarpAction();
                else if(insertWeftEdit.match(t) && isWorkingMode)
                    insertWeftAction();
                else if(deleteWeftEdit.match(t) && isWorkingMode)
                    deleteWeftAction();
                else if(selectEdit.match(t) && isWorkingMode)
                    selectAction();
                else if(cropEdit.match(t) && isWorkingMode)
                    cropAction();
                else if(copyEdit.match(t) && isWorkingMode)
                    copyAction();
                else if(cutEdit.match(t) && isWorkingMode)
                    cutAction();
                else if(pasteEdit.match(t) && isWorkingMode)
                    pasteAction();
                else if(mirrorVerticalEdit.match(t) && isWorkingMode)
                    mirrorVerticalSelectedAction();
                else if(mirrorHorizontalEdit.match(t) && isWorkingMode)
                    mirrorHorizontalSelectedAction();
                else if(jacquardConversionEdit.match(t) && isWorkingMode)
                    jacquardConverstionAction();
                else if(complexWeaveEdit.match(t) && isWorkingMode)
                    complexWeaveAction();
                else if(repeatSelectedEdit.match(t) && isWorkingMode)
                    repeatSelectionInActiveArea();
                else if(yarnEdit.match(t) && isWorkingMode)
                    yarnPropertiesAction();
                else if(threadPatternEdit.match(t) && isWorkingMode)
                    threadSequenceAction();
                else if(switchColorEdit.match(t) && isWorkingMode)
                    switchColorAction();
                else if(repeatEdit.match(t) && isWorkingMode)
                    repeatOrientationAction();
                else if(sprayToolEdit.match(t) && isWorkingMode)
                    sprayToolAction();                
                else if(transformOperationEdit.match(t) && isWorkingMode)
                    transformOperationAction();
                else if(vMirrorTransform.match(t) && isWorkingMode)
                    mirrorVerticalAction();
                else if(hMirrorTransform.match(t) && isWorkingMode)
                    mirrorHorizontalAction();
                else if(clockwiseRotateTransform.match(t) && isWorkingMode)
                    rotateClockwiseAction();
                else if(antiClockwiseRotateTransform.match(t) && isWorkingMode)
                    rotateAntiClockwiseAction();
                else if(moveRightTransform.match(t) && isWorkingMode)
                    moveRightAction();
                else if(moveLeftTransform.match(t) && isWorkingMode)
                    moveLeftAction();
                else if(moveUpTransform.match(t) && isWorkingMode)
                    moveUpAction();
                else if(moveDownTransform.match(t) && isWorkingMode)
                    moveDownAction();
                else if(move8RightTransform.match(t) && isWorkingMode)
                    moveRight8Action();
                else if(move8LeftTransform.match(t) && isWorkingMode)
                    moveLeft8Action();
                else if(move8UpTransform.match(t) && isWorkingMode)
                    moveUp8Action();
                else if(move8DownTransform.match(t) && isWorkingMode)
                    moveDown8Action();
                else if(tiltRightTransform.match(t) && isWorkingMode)
                    tiltRightAction();
                else if(tiltLeftTransform.match(t) && isWorkingMode)
                    tiltLeftAction();
                else if(tiltUpTransform.match(t) && isWorkingMode)
                    tiltUpAction();
                else if(tiltDownTransform.match(t) && isWorkingMode)
                    tiltDownAction();
                else if(invertTransform.match(t) && isWorkingMode)
                    inversionAction();
                else if(clearTransform.match(t) && isWorkingMode)
                    clearAction();
                else if(closeTransform.match(t) && isWorkingMode)
                    closeAction();
                else if(newFile.match(t))
                    createMenuAction();
                else if(openFile.match(t))
                    openMenuAction();
                else if(loadFile.match(t))
                    loadRecentMenuAction();
                else if(importFile.match(t))
                    importMenuAction();
                else if(saveFile.match(t) && isWorkingMode && !isNew)
                    saveMenuAction();
                else if(saveAsFile.match(t) && isWorkingMode)
                    saveAsMenuAction();
                else if(exportFile.match(t) && isWorkingMode)
                    exportMenuAction();
                else if(saveXMLFile.match(t) && isWorkingMode)
                    exportHtmlAction();
                else if(printFile.match(t) && isWorkingMode)
                    printMenuAction();                
            }
        });
    }
    
    /**
     * addDentingEventHandler
     * <p>
     * Function use for drawing menu bar for menu item,
     * and binding events for each menus with style. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void addDentingEventHandler(){
        dentingIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                int mouseX=(int)t.getX();
                int mouseY=(int)t.getY();
                Point dot=getDotFromPixels(mouseX, mouseY);
                if(dot.y>=0&&dot.y<=1&&dot.x>=0&&dot.x<activeGridCol){
                    invertDotColor(dot, dentingImage);
                    if(getDotColor(dot, dentingImage)==INT_SOLID_BLACK){
                        objWeave.getDentMatrix()[dot.y][dot.x]=1;
                        // Added as only one dot in a col
                        dot.y=1-dot.y;
                        fillImageDotPixels(dot, INT_SOLID_WHITE, dentingImage);
                        objWeave.getDentMatrix()[dot.y][dot.x]=0;
                    }
                    else{
                        objWeave.getDentMatrix()[dot.y][dot.x]=0;
                        // Added as only one dot in a col
                        dot.y=1-dot.y;
                        fillImageDotPixels(dot, INT_SOLID_BLACK, dentingImage);
                        objWeave.getDentMatrix()[dot.y][dot.x]=1;
                    }
                    refreshDentingImage();
                    Weave objClonedWeave=cloneWeave();
                    objUR.doCommand("Denting Mouse Click", objClonedWeave);
                }
            }
        });
        dentingIV.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                int mouseX=(int)t.getX();
                int mouseY=(int)t.getY();
                Point dot=getDotFromPixels(mouseX, mouseY);
                if(dot.y>=0&&dot.y<=1&&dot.x>=0&&dot.x<activeGridCol){
                    if(dentDragOverDots.contains((dot.x+","+dot.y).toString()))
                        return;
                    dentDragOverDots.add(dot.x+","+dot.y);
                    invertDotColor(dot, dentingImage);
                    if(getDotColor(dot, dentingImage)==INT_SOLID_BLACK){
                        objWeave.getDentMatrix()[dot.y][dot.x]=1;
                        // Added as only one dot in a col
                        dot.y=1-dot.y;
                        fillImageDotPixels(dot, INT_SOLID_WHITE, dentingImage);
                        objWeave.getDentMatrix()[dot.y][dot.x]=0;
                    }
                    else{
                        objWeave.getDentMatrix()[dot.y][dot.x]=0;
                        // Added as only one dot in a col
                        dot.y=1-dot.y;
                        fillImageDotPixels(dot, INT_SOLID_BLACK, dentingImage);
                        objWeave.getDentMatrix()[dot.y][dot.x]=1;
                    }
                    refreshDentingImage();
                }
            }
        });
        dentingIV.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                dentDragOverDots.clear();
            }
        });
    }
    /**
     * addDesignEventHandler
     * <p>
     * Function use for drawing menu bar for menu item,
     * and binding events for each menus with style. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void addDesignEventHandler(){
        designIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                if(isSelectionMode&&isRegionSelected){
                    isRegionSelected=false;
                    return;
                }
                int mouseX=(int)t.getX();
                int mouseY=(int)t.getY();
                Point dot=getDotFromPixels(mouseX, mouseY);
                if(!isGridDotActive(dot)){
                    return;
                }
                current_row=getDesignMatrixRow(dot.y);
                current_col=dot.x;
                //int cX=dot.x;
                //int cY=dot.y;
                if(t.getButton()==MouseButton.SECONDARY){
                    // if selection on then paste
                    if(isSelectionMode){
                        pasteAction();
                        //objWeaveAction.paste(objWeave, getDesignMatrixRow(dot.y), dot.x);
                        drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
                        refreshDesignImage();
						//added by amit
                        populateNplot();
                        System.gc();
                        t.consume();
                        return;
                    }
                    else{
                        if(getDotColor(dot, designImage)==INT_SOLID_BLACK)
                            invertDotColor(dot, designImage);
                        objWeave.getDesignMatrix()[(objWeave.getDesignMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y)][dot.x]=0;
                        refreshDesignImage();
                        //added by amit
                        populateNplot();
                        System.gc();
                        t.consume();
                        Weave objClonedWeave=cloneWeave();
                        objUR.doCommand("Design Mouse Right Click", objClonedWeave);
                        return;
                    }
                    /*if(isSelectionMode&&(!dragOverDots.isEmpty())){
                    // click to (circular) paste, copied dots
                    System.out.println(dragOverDots.size());
                    int color=-1;
                    String s="";
                    for(int r=0; r<SELECTED_ROW; r++){
                        for(int c=0; c<SELECTED_COL; c++){
                            s=dragOverDots.get((r*SELECTED_COL)+c);
                            dot.x=Integer.parseInt(s.substring(0, s.indexOf(",")));
                            dot.y=Integer.parseInt(s.substring(s.indexOf(",")+1, s.length()));
                            color=getDotColor(dot, designImage);
                            System.out.println("Dot from: "+dot.x+", "+dot.y);
                            fillImageDotPixels(dot, color, designImage);
                            dot.x=(cX+c)%activeGridCol;
                            dot.y=(MAX_GRID_ROW-activeGridRow)+((activeGridRow-(MAX_GRID_ROW-cY)+r)%activeGridRow);
                            System.out.println("Dot to: "+dot.x+", "+dot.y);
                            fillImageDotPixels(dot, color, designImage);
                        }
                    }
                    dragOverDots.clear();
                    // also deselect the selected dots
                }*/
                }
                
                /* For Testing Matrix to Draw Image
                if(!isGridDotActive(dot)){
                    byte[][] designMatrix=new byte[7][];
                    designMatrix[0]=designMatrix[2]=designMatrix[4]=designMatrix[6]=new byte[]{0, 1, 1};
                    designMatrix[1]=designMatrix[3]=designMatrix[5]=new byte[]{1, 1, 0};
                    drawImageFromMatrix(designMatrix, designImage);
                }*/
                invertDotColor(dot, designImage);
                if(getDotColor(dot, designImage)==INT_SOLID_BLACK)
                    objWeave.getDesignMatrix()[(objWeave.getDesignMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y)][dot.x]=1;
                else
                    objWeave.getDesignMatrix()[(objWeave.getDesignMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y)][dot.x]=0;
                drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
                refreshDesignImage();
                populateNplot();
                System.gc();
                Weave objClonedWeave=cloneWeave();
                objUR.doCommand("Design Mouse Click", objClonedWeave);
                t.consume();
            }
        });
        // drag code
        /*designIV.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                int mouseX=(int)t.getX();
                int mouseY=(int)t.getY();
                Point dot=getDotFromPixels(mouseX, mouseY);
                if(!isGridDotActive(dot)){
                    return;
                }
                Weave objClonedWeave=cloneWeave();
                objUR.doCommand("Design Mouse Pressed", objClonedWeave);
                drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
                refreshDesignImage();
            }
        });*/
        designIV.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                int mouseX=(int)t.getX();
                int mouseY=(int)t.getY();
                Point dot=getDotFromPixels(mouseX, mouseY);
                //System.err.println("Mouse being dragged at: "+dot.x+", "+dot.y);
                String value=(int)dot.x+","+(int)dot.y;
                if(isSelectionMode){
                    if(isGridDotActive(dot)){
                        if(dragOverDots.isEmpty()){
                            dragOverDots.add(value); // this is initial point
                        }
                    }
                }
                else{
                    /*if(dragOverDots.contains(value))
                        return;
                    if(isGridDotActive(dot)){
                        dragOverDots.add(value);
                        if(getDotColor(dot, designImage)==INT_SOLID_BLACK)
                            objWeave.getDesignMatrix()[(objWeave.getDesignMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y)][dot.x]=0;
                        else
                            objWeave.getDesignMatrix()[(objWeave.getDesignMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y)][dot.x]=1;
                
                    }*/
                    if(designDragOverDots.contains(value))
                        return;
                    if(isGridDotActive(dot)){
                        designDragOverDots.add(value);
                        //invertDotColor(dot, designImage);
                        //if(getDotColor(dot, designImage)==INT_SOLID_BLACK)
                        //    objWeave.getDesignMatrix()[(objWeave.getDesignMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y)][dot.x]=1;
                        //else
                        //    objWeave.getDesignMatrix()[(objWeave.getDesignMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y)][dot.x]=0;
                        
                        // Mouse Button: Left On, Right Off
                        if(t.getButton()==MouseButton.SECONDARY){
                            if(getDotColor(dot, designImage)==INT_SOLID_BLACK)
                                invertDotColor(dot, designImage);
                            objWeave.getDesignMatrix()[(objWeave.getDesignMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y)][dot.x]=0;
                        }
                        else if(t.getButton()==MouseButton.PRIMARY){
                            if(getDotColor(dot, designImage)==INT_SOLID_WHITE)
                                invertDotColor(dot, designImage);
                            objWeave.getDesignMatrix()[(objWeave.getDesignMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y)][dot.x]=1;
                        }
                        refreshDesignImage();
                    }
                }
                t.consume();
            }
        });
        designIV.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                if(isSelectionMode){
                    int mouseX=(int)t.getX();
                    int mouseY=(int)t.getY();
                    Point dot=getDotFromPixels(mouseX, mouseY);
                    //System.err.println("Mouse Released at: "+dot.x+", "+dot.y);
                    if(dragOverDots.isEmpty())
                        return;
                    String s=dragOverDots.get(0);
                    if(isGridDotActive(dot)){
                        int[][] rectCorners=getRectCorner(Integer.parseInt(s.substring(0, s.indexOf(",")))
                                , Integer.parseInt(s.substring(s.indexOf(",")+1, s.length())), (int)dot.x, (int)dot.y);
                        //System.out.println(rectCorners[0][0]+":"+rectCorners[0][1]+"\n"
                        //+rectCorners[1][0]+":"+rectCorners[1][1]+"\n"
                        //+rectCorners[2][0]+":"+rectCorners[2][1]+"\n"
                        //+rectCorners[3][0]+":"+rectCorners[3][1]+"\n");
                        //System.out.println(getIntRgbFromColor(255, 255, 0, 0));
                        initial_row=getDesignMatrixRow(Integer.parseInt(s.substring(s.indexOf(",")+1, s.length())));
                        initial_col=Integer.parseInt(s.substring(0, s.indexOf(",")));
                        final_row=getDesignMatrixRow(dot.y);
                        final_col=dot.x;
                        //objWeaveAction.copy(objWeave, getDesignMatrixRow(Integer.parseInt(s.substring(s.indexOf(",")+1, s.length()))), Integer.parseInt(s.substring(0, s.indexOf(","))), getDesignMatrixRow(dot.y), dot.x);
                        dragOverDots.clear();
                        SELECTED_ROW=rectCorners[1][1]-rectCorners[0][1]+1;
                        SELECTED_COL=rectCorners[3][0]-rectCorners[0][0]+1;
                        //System.out.println("R "+SELECTED_ROW+" C "+SELECTED_COL);
                        for(int r=rectCorners[0][1]; r<=rectCorners[1][1]; r++){
                            for(int c=rectCorners[0][0]; c<=rectCorners[3][0]; c++){
                                dot.x=c;
                                dot.y=r;
                                fillDotBorder(dot, designImage);
                                //dragOverDots.add(c+","+r);
                            }
                        }
                        refreshDesignImage();
                    }
                } else{
                    /*for(String s: dragOverDots){
                        invertDotColor(new Point(Integer.parseInt(s.substring(0, s.indexOf(",")))
                                ,Integer.parseInt(s.substring(s.indexOf(",")+1, s.length()))), designImage);
                    }
                    refreshDesignImage();
                    dragOverDots.clear();*/
                    designDragOverDots.clear();
                    //populateNplot();
                    int mouseX=(int)t.getX();
                    int mouseY=(int)t.getY();
                    Point dot=getDotFromPixels(mouseX, mouseY);
                    if(isGridDotActive(dot)){
                        if(t.getButton()==MouseButton.PRIMARY){
                            if(getDotColor(dot, designImage)==INT_SOLID_BLACK)
                                invertDotColor(dot, designImage);
                            objWeave.getDesignMatrix()[(objWeave.getDesignMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y)][dot.x]=0;
                            refreshDesignImage();
                        }
                    }
                    //Weave objClonedWeave=cloneWeave();
                    //objUR.doCommand("Design Mouse Released", objClonedWeave);
                }
                t.consume();
                isRegionSelected=true;
            }
        });
        designIV.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                int mouseX=(int)t.getX();
                int mouseY=(int)t.getY();
                Point dot=getDotFromPixels(mouseX, mouseY);
                if(isGridDotActive(dot)){
                    designSP.setTooltip(new Tooltip(objDictionaryAction.getWord("WARP")+": "+(dot.x+1)+", "+objDictionaryAction.getWord("WEFT")+": "+(MAX_GRID_ROW-dot.y)));
                } else{
                    designSP.setTooltip(null);//new Tooltip(objDictionaryAction.getWord("DESIGNPANE")));
                }
            }
        });
    }
	/**
     * addShaftEventHandler
     * <p>
     * Function use for drawing menu bar for menu item,
     * and binding events for each menus with style. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void addShaftEventHandler(){
        shaftIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                int mouseX=(int)t.getX();
                int mouseY=(int)t.getY();
                Point dot=getDotFromPixels(mouseX, mouseY);
                int dotRow=dot.y;
                if(!isGridDotActive(dot))
                    return;
                checkDotInCol(dot);
                dot.y=dotRow;
                invertDotColor(dot, shaftImage);
                if(getDotColor(dot, shaftImage)==INT_SOLID_BLACK)
                    objWeave.getShaftMatrix()[(objWeave.getShaftMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y)][dot.x]=1;
                else
                    objWeave.getShaftMatrix()[(objWeave.getShaftMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y)][dot.x]=0;
                
                refreshShaftImage();
                if(isDesignMode){
                    objWeave.setDesignMatrix(new byte[objWeave.getIntWeft()][objWeave.getIntWarp()]);
                    objWeaveAction.populateDesign(objWeave);
                    //objWeaveAction.populateShaftDesign(objWeave, (objWeave.getShaftMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y),dot.x);
                    objWeaveAction.populateTreadles(objWeave);
                    objWeave.setIntWeaveTreadles(objWeave.getIntWarp());
                    initTreadles();
                    objWeaveAction.populateTieUp(objWeave);
                    objWeaveAction.populatePegPlan(objWeave);
                    objWeave.setIntWeaveShaft(objWeave.getIntWeft());
                    initPeg();
                    plotTradeles();
                    plotTieUp();
                    plotPeg();
                    populateDplot();
                } else{
                    //added by amit
                    /*System.out.println("Is lift plan="+objWeave.getBytIsLiftPlan());
                    for(int i=0; i<objWeave.getIntWeaveShaft();i++){
                        for(int j=0; j<objWeave.getIntWarp();j++)
                            System.err.print("\t"+objWeave.getShaftMatrix()[i][j]);
                        System.err.println("");
                    }
                    System.err.println("====================");
                    for(int i=0; i<objWeave.getIntWeft();i++){
                        for(int j=0; j<objWeave.getIntWeaveTreadles();j++)
                            System.err.print("\t"+objWeave.getTreadlesMatrix()[i][j]);
                        System.err.println("");
                    }
                    System.err.println("====================");
                    for(int i=0; i<objWeave.getIntWeaveShaft();i++){
                        for(int j=0; j<objWeave.getIntWeaveTreadles();j++)
                            System.err.print("\t"+objWeave.getTieupMatrix()[i][j]);
                        System.err.println("");
                    }
                    System.err.println("====================");
                    for(int i=0; i<objWeave.getIntWeft();i++){
                        for(int j=0; j<objWeave.getIntWarp();j++)
                            System.err.print("\t"+objWeave.getPegMatrix()[i][j]);
                        System.err.println("");
                    }
                    System.err.println("====================");
                    */
                    objWeave.setDesignMatrix(new byte[objWeave.getIntWeft()][objWeave.getIntWarp()]);
                    objWeaveAction.populateDesign(objWeave);
                    /*for(int i=0; i<objWeave.getIntWeft();i++){
                        for(int j=0; j<objWeave.getIntWarp();j++)
                            System.err.print("\t"+objWeave.getDesignMatrix()[i][j]);
                        System.err.println("");
                    }*/
                    //objWeaveAction.populateShaftDesign(objWeave, (objWeave.getShaftMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y),dot.x);
                    if(objWeave.getBytIsLiftPlan()==1){
                        //reverse design matrix
                        //objWeaveAction.mirrorVertical(objWeave);
                        drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);            
                        populateDplot();
                        objWeaveAction.populateTreadles(objWeave);
                        objWeave.setIntWeaveTreadles(objWeave.getIntWarp());
                        initTreadles();
                        objWeaveAction.populateTieUp(objWeave);
                        plotTradeles();
                        plotTieUp();                       
                    } else{ //populate Design from Shaft Tradeles tie-up
                        objWeaveAction.populatePegPlan(objWeave);
                        initPeg();
                        plotPeg();
                        populateDplot();
                    }                   
                }
                Weave objClonedWeave=cloneWeave();
                objUR.doCommand("Shaft Mouse Click", objClonedWeave);
            }
        });
        shaftIV.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                int mouseX=(int)t.getX();
                int mouseY=(int)t.getY();
                Point dot=getDotFromPixels(mouseX, mouseY);
                if(isGridDotActive(dot)&&(MAX_GRID_ROW-dot.y)<=objWeave.getIntWeaveShaft()){
                    shaftSP.setTooltip(new Tooltip(objDictionaryAction.getWord("WARP")+": "+(dot.x+1)+", "+objDictionaryAction.getWord("SHAFT")+": "+(MAX_GRID_ROW-dot.y)));
                } else{
                    shaftSP.setTooltip(null);
                }
            }
        });
    }
    /**
     * addTradeleEventHandler
     * <p>
     * Function use for drawing menu bar for menu item,
     * and binding events for each menus with style. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void addTradeleEventHandler(){
        tradelesIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                int mouseX=(int)t.getX();
                int mouseY=(int)t.getY();
                Point dot=getDotFromPixels(mouseX, mouseY);
                int dotCol=dot.x;
                if(!isGridDotActive(dot)){
                    return;
                }
                checkDotInRow(dot);
                dot.x=dotCol;
                objWeave.setBytIsLiftPlan((byte)0); //added by amit
				invertDotColor(dot, tradelesImage);
                if(getDotColor(dot, tradelesImage)==INT_SOLID_BLACK)
                    objWeave.getTreadlesMatrix()[(objWeave.getTreadlesMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y)][dot.x]=1;
                else
                    objWeave.getTreadlesMatrix()[(objWeave.getTreadlesMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y)][dot.x]=0;
                refreshTradelesImage();
                if(isDesignMode){
                    objWeave.setDesignMatrix(new byte[objWeave.getIntWeft()][objWeave.getIntWarp()]);
                    objWeaveAction.populateDesign(objWeave);
                    //objWeaveAction.populateTradelesDesign(objWeave, (objWeave.getTreadlesMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y),dot.x);
                    objWeaveAction.populateShaft(objWeave);
                    objWeave.setIntWeaveShaft(objWeave.getIntWeft());
                    initShaft();
                    objWeaveAction.populateTieUp(objWeave);
                    objWeaveAction.populatePegPlan(objWeave);
                    objWeave.setIntWeaveShaft(objWeave.getIntWeft());
                    initPeg();
                    populateDplot();
                    plotShaft();
                    plotPeg();
                    plotTieUp();
                } else{
                    objWeave.setDesignMatrix(new byte[objWeave.getIntWeft()][objWeave.getIntWarp()]);
                    objWeaveAction.populateDesign(objWeave);                    
                    //objWeaveAction.populateTradelesDesign(objWeave, (objWeave.getTreadlesMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y),dot.x);
                    //added by amit
                    populateDplot();
                    objWeaveAction.populatePegPlan(objWeave);                    
                    initPeg();
                    plotPeg();                    
                }
                Weave objClonedWeave=cloneWeave();
                objUR.doCommand("Treadle Mouse Click", objClonedWeave);
            }
        });
        tradelesIV.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                int mouseX=(int)t.getX();
                int mouseY=(int)t.getY();
                Point dot=getDotFromPixels(mouseX, mouseY);
                if(isGridDotActive(dot)&&(dot.x+1)<=objWeave.getIntWeaveTreadles()){
                    tradelesSP.setTooltip(new Tooltip(objDictionaryAction.getWord("TRADLE")+": "+(dot.x+1)+", "+objDictionaryAction.getWord("WEFT")+": "+(MAX_GRID_ROW-dot.y)));
                } else{
                    tradelesSP.setTooltip(null);
                }
            }
        });
    }
    /**
     * addTieUpEventHandler
     * <p>
     * Function use for drawing menu bar for menu item,
     * and binding events for each menus with style. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void addTieUpEventHandler(){
        tieUpIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                int mouseX=(int)t.getX();
                int mouseY=(int)t.getY();
                Point dot=getDotFromPixels(mouseX, mouseY);
                if(!isGridDotActive(dot)){
                    return;
                }
				objWeave.setBytIsLiftPlan((byte)0); //added by amit
                invertDotColor(dot, tieUpImage);
                if(getDotColor(dot, tieUpImage)==INT_SOLID_BLACK)
                    objWeave.getTieupMatrix()[(objWeave.getTieupMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y)][dot.x]=1;
                else
                    objWeave.getTieupMatrix()[(objWeave.getTieupMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y)][dot.x]=0;
                refreshTieUpImage();
                if(isDesignMode){
                    objWeaveAction.populateTieUpDesign(objWeave, (objWeave.getTieupMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y),dot.x);
                    objWeaveAction.populateShaft(objWeave);
                    objWeave.setIntWeaveShaft(objWeave.getIntWeft());
                    initShaft();
                    objWeaveAction.populateTreadles(objWeave);
                    objWeave.setIntWeaveTreadles(objWeave.getIntWarp());
                    initTreadles();
                    objWeaveAction.populatePegPlan(objWeave);
                    objWeave.setIntWeaveShaft(objWeave.getIntWeft());
                    initPeg();
                    populateDplot();
                    plotShaft();
                    plotPeg();
                    plotTradeles();
                } else{
                    objWeaveAction.populateTieUpDesign(objWeave, (objWeave.getTieupMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y),dot.x);
                    //added by amit
                    objWeaveAction.populatePegPlan(objWeave);
                    initPeg();
                    populateDplot();
                    plotPeg();
                }
                Weave objClonedWeave=cloneWeave();
                objUR.doCommand("Tieup Mouse Click", objClonedWeave);
            }
        });
        tieUpIV.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                int mouseX=(int)t.getX();
                int mouseY=(int)t.getY();
                Point dot=getDotFromPixels(mouseX, mouseY);
                if(isGridDotActive(dot)&&((dot.x+1)<=objWeave.getTieupMatrix().length)&&((MAX_GRID_ROW-dot.y)<=objWeave.getTieupMatrix()[0].length)){
                    tieUpSP.setTooltip(new Tooltip((dot.x+1)+", "+(MAX_GRID_ROW-dot.y)));
                } else{
                    tieUpSP.setTooltip(null);
                }
            }
        });
    }
    /**
     * addPegEventHandler
     * <p>
     * Function use for drawing menu bar for menu item,
     * and binding events for each menus with style. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void addPegEventHandler(){
        pegIV.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                int mouseX=(int)t.getX();
                int mouseY=(int)t.getY();
                Point dot=getDotFromPixels(mouseX, mouseY);
                if(!isGridDotActive(dot))
                    return;
				objWeave.setBytIsLiftPlan((byte)1); //added by amit
                invertDotColor(dot, pegImage);
                if(getDotColor(dot, pegImage)==INT_SOLID_BLACK)
                    objWeave.getPegMatrix()[(objWeave.getPegMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y)][dot.x]=1;
                else
                    objWeave.getPegMatrix()[(objWeave.getPegMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y)][dot.x]=0;
                refreshPegImage();
                if(isDesignMode){
                    objWeaveAction.populatePegDesign(objWeave, (objWeave.getPegMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y),dot.x);
                    objWeaveAction.populateShaft(objWeave);
                    objWeave.setIntWeaveShaft(objWeave.getIntWeft());
                    initShaft();

                    objWeaveAction.populateTreadles(objWeave);
                    objWeave.setIntWeaveTreadles(objWeave.getIntWarp());
                    initTreadles();
                    objWeaveAction.populateTieUp(objWeave);                    
                    populateDplot();
                    plotShaft();
                    plotTieUp();
                    plotTradeles();
                } else{
                    objWeaveAction.populatePegDesign(objWeave, (objWeave.getPegMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y),dot.x);
                    //added by amit
                    objWeaveAction.populateTreadles(objWeave);
                    objWeave.setIntWeaveTreadles(objWeave.getIntWarp());
                    initTreadles();
                    objWeaveAction.populateTieUp(objWeave);
                    populateDplot();
                    plotTieUp();
                    plotTradeles();
                }
                Weave objClonedWeave=cloneWeave();
                objUR.doCommand("Peg Mouse Click", objClonedWeave);
            }
        });
        pegIV.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                int mouseX=(int)t.getX();
                int mouseY=(int)t.getY();
                Point dot=getDotFromPixels(mouseX, mouseY);
                if(isGridDotActive(dot)&&((dot.x+1)<=objWeave.getIntWeaveShaft())){
                    pegSP.setTooltip(new Tooltip(objDictionaryAction.getWord("SHAFT")+": "+(dot.x+1)+", "+objDictionaryAction.getWord("WEFT")+": "+(MAX_GRID_ROW-dot.y)));
                } else{
                    pegSP.setTooltip(null);
                }
            }
        });
    }
    /**
     * homeMenuAction
     * <p>
     * Function use for exit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void homeMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHOME"));
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.setIconified(false);
        dialogStage.setFullScreen(false);
        dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("ALERT"));
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 300, 100, Color.WHITE);
        scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        final GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        popup.add(new ImageView("/media/alert_warning.png"), 0, 0);   
        Label lblAlert = new Label(objDictionaryAction.getWord("ALERTCLOSE"));
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(250);
        popup.add(lblAlert, 1, 0);
        Button btnYes = new Button(objDictionaryAction.getWord("YES"));
        btnYes.setPrefWidth(50);
        btnYes.setId("btnYes");
        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if(objWeave.getStrWeaveID()!=null && !isNew)
                    objConfiguration.setStrRecentWeave(objWeave.getStrWeaveID());
                dialogStage.close();
                weaveStage.close();
                System.gc();
                WindowView objWindowView = new WindowView(objConfiguration);
            }
        });
        popup.add(btnYes, 0, 1);
        Button btnNo = new Button(objDictionaryAction.getWord("NO"));
        btnNo.setPrefWidth(50);
        btnNo.setId("btnNo");
        btnNo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
            }
        });
        popup.add(btnNo, 1, 1);
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.showAndWait();
    }
    /**
     * fileMenuAction
     * <p>
     * Function use for file menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void fileMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONFILE"));
        selectedMenu = "FILE";
        menuHighlight();
    }
    /**
     * editMenuAction
     * <p>
     * Function use for edit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void editMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONEDIT"));
        selectedMenu = "EDIT";
        menuHighlight();
    }
    /**
     * viewMenuAction
     * <p>
     * Function use for view menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void viewMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONVIEW"));
        selectedMenu = "VIEW";
        menuHighlight();
    }    
    /**
     * utilityMenuAction
     * <p>
     * Function use for utility menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void utilityMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("TOOLTIPUTILITY"));
        selectedMenu = "UTILITY";
        menuHighlight();
    }
    /**
     * helpMenuAction
     * <p>
     * Function use for help menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void helpMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHELP"));
        HelpView objHelpView = new HelpView(objConfiguration);
    }
    /**
     * technicalMenuAction
     * <p>
     * Function use for technical menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void technicalMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONTECHNICAL"));
        TechnicalView objTechnicalView = new TechnicalView(objConfiguration);
    }
    /**
     * aboutMenuAction
     * <p>
     * Function use for about menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void aboutMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONABOUTUS"));
        AboutView objAboutView = new AboutView(objConfiguration);
    }
    /**
     * contactMenuAction
     * <p>
     * Function use for contact menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void contactMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCONTACTUS"));
        ContactView objContactView = new ContactView(objConfiguration);
    }
    /**
     * logoutMenuAction
     * <p>
     * Function use for exit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void logoutMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHOME"));
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.setIconified(false);
        dialogStage.setFullScreen(false);
        dialogStage.setTitle(objDictionaryAction.getWord("ALERT"));
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 300, 100, Color.WHITE);
        scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        popup.add(new ImageView("/media/alert_warning.png"), 0, 0);   
        Label lblAlert = new Label(objDictionaryAction.getWord("ALERTCLOSE"));
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(250);
        popup.add(lblAlert, 1, 0);
        Button btnYes = new Button(objDictionaryAction.getWord("YES"));
        btnYes.setPrefWidth(50);
        btnYes.setId("btnYes");
        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                objConfiguration.setStrRecentWeave(null);
                dialogStage.close();
                weaveStage.close();
                System.gc();
                UserLoginView objUserLoginView = new UserLoginView(objConfiguration);
            }
        });
        popup.add(btnYes, 0, 1);
        Button btnNo = new Button(objDictionaryAction.getWord("NO"));
        btnNo.setPrefWidth(50);
        btnNo.setId("btnNo");
        btnNo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
            }
        });
        popup.add(btnNo, 1, 1);
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.showAndWait();
    }
    /**
     * exitMenuAction
     * <p>
     * Function use for exit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void exitMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONEXIT"));
        System.gc();
        weaveStage.close();
    }
    private void runMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONREALTIMEVIEW"));
        if(isWorkingMode){
            WeaveViewer objWeaveViewer=new WeaveViewer(objWeave);
        }
     }
    /**
     * transformMenuAction
     * <p>
     * Function use for weave menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void transformMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONTRANSFORMOPERATIONEDIT"));
        selectedMenu = "TRANSFORMOPERATION";
        transformMenu.show();
        transformMenu.setVisible(true);
        fileMenu.setDisable(true);
        editMenu.setDisable(true);
        viewMenu.setDisable(true);
        utilityMenu.setDisable(true);
        runMenu.setDisable(true);
        transformMenu.setDisable(false);
        menuHighlight(); 
    }     
/**
 * menuHighlight
 * <p>
 * Function use for drawing menu bar for menu item,
 * and binding events for each menus with style. 
 * 
 * @param       String strMenu
 * @author      Amit Kumar Singh
 * @version     %I%, %G%
 * @since       1.0
 * @see         javafx.event.*;
 * @link        WeaveView
 */
    private void menuHighlight(){
        fileMenu.setStyle("-fx-background-color: linear-gradient(#000000,#5C594A);");
        editMenu.setStyle("-fx-background-color: linear-gradient(#000000,#5C594A);");
        viewMenu.setStyle("-fx-background-color: linear-gradient(#000000,#5C594A);");
        utilityMenu.setStyle("-fx-background-color: linear-gradient(#000000,#5C594A);");
        transformMenu.setStyle("-fx-background-color: linear-gradient(#000000,#5C594A);");
        fileMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-font-family: \"Segoe UI Semibold\"; -fx-text-fill: #FFFFFF;");
        editMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-font-family: \"Segoe UI Semibold\"; -fx-text-fill: #FFFFFF;");
        viewMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-font-family: \"Segoe UI Semibold\"; -fx-text-fill: #FFFFFF;");
        utilityMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-font-family: \"Segoe UI Semibold\"; -fx-text-fill: #FFFFFF;");
        transformMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-font-family: \"Segoe UI Semibold\"; -fx-text-fill: #FFFFFF;");
        toolBar.getItems().clear();
        menuSelected.getItems().clear();
        
        System.gc();
        if(selectedMenu == "TRANSFORMOPERATION"){
            transformMenu.setStyle("-fx-background-color: linear-gradient(#FFFFFF,#FFFEEE);");
            transformMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-text-fill: #000000;");
            populateTransformToolbar();
            populateTransformToolbarMenu();
        } else if(selectedMenu == "UTILITY"){
            utilityMenu.setStyle("-fx-background-color: linear-gradient(#FFFFFF,#FFFEEE);");
            utilityMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-text-fill: #000000;");
            populateUtilityToolbar();
            populateUtilityToolbarMenu();
        } else if(selectedMenu == "VIEW"){
            viewMenu.setStyle("-fx-background-color: linear-gradient(#FFFFFF,#FFFEEE);");
            viewMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-text-fill: #000000;");
            populateViewToolbar();
            populateViewToolbarMenu();
        } else if(selectedMenu == "EDIT"){
            editMenu.setStyle("-fx-background-color: linear-gradient(#FFFFFF,#FFFEEE);");
            editMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-text-fill: #000000;");
            populateEditToolbar();
            populateEditToolbarMenu();
        //linear-gradient(#f3e19f, #e8cf8d)
        } else {
            fileMenu.setStyle("-fx-background-color: linear-gradient(#FFFFFF,#FFFEEE);");
            fileMenuLabel.setStyle("-fx-wrap-text:true; -fx-text-alignment: center; -fx-alignment: CENTER; -fx-font-size: 15pt; -fx-text-fill: #000000;");
            populateFileToolbar();
            populateFileToolbarMenu();
        }
        System.gc();
    } 

    /**
    * populateFileToolbar
    * <p>
    * Function use for drawing tool bar for menu item File,
    * and binding events for each tools. 
    * 
    * @exception   (@throws SQLException)
    * @author      Amit Kumar Singh
    * @version     %I%, %G%
    * @since       1.0
    * @see         javafx.event.*;
    * @link        WeaveView
    */
    private void populateFileToolbarMenu(){
        //For drop down menus
        menuSelected.setTooltip(new Tooltip(objDictionaryAction.getWord("FILE")));
        
        MenuItem newFileMI = new MenuItem(objDictionaryAction.getWord("NEWFILE"));
        MenuItem openFileMI = new MenuItem(objDictionaryAction.getWord("OPENFILE"));
        MenuItem loadFileMI = new MenuItem(objDictionaryAction.getWord("LOADFILE"));
        MenuItem importFileMI = new MenuItem(objDictionaryAction.getWord("IMPORTFILE"));
        MenuItem saveFileMI = new MenuItem(objDictionaryAction.getWord("SAVEFILE"));
        MenuItem saveAsFileMI = new MenuItem(objDictionaryAction.getWord("SAVEASFILE"));
        MenuItem exportFileMI = new MenuItem(objDictionaryAction.getWord("EXPORTFILE"));
        MenuItem saveXMLFileMI = new MenuItem(objDictionaryAction.getWord("SAVEXMLFILE"));        
        MenuItem printFileMI = new MenuItem(objDictionaryAction.getWord("PRINTFILE"));
        
        newFileMI.setAccelerator(new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN));
        openFileMI.setAccelerator(new KeyCodeCombination(KeyCode.O, KeyCombination.CONTROL_DOWN));
        loadFileMI.setAccelerator(new KeyCodeCombination(KeyCode.L, KeyCombination.CONTROL_DOWN));
        importFileMI.setAccelerator(new KeyCodeCombination(KeyCode.I, KeyCombination.CONTROL_DOWN));
        saveFileMI.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN));
        saveAsFileMI.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        exportFileMI.setAccelerator(new KeyCodeCombination(KeyCode.E, KeyCombination.CONTROL_DOWN));
        saveXMLFileMI.setAccelerator(new KeyCodeCombination(KeyCode.X, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        printFileMI.setAccelerator(new KeyCodeCombination(KeyCode.P, KeyCombination.CONTROL_DOWN));
        
        newFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/new.png"));
        openFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/open.png"));
        loadFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/open_recent.png"));
        importFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/import.png"));
        saveFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
        saveAsFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save_as.png"));
        exportFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export.png"));
        saveXMLFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_html_as.png"));
        printFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/print.png"));
             
        if(!isWorkingMode){
            saveFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
            saveFileMI.setDisable(true);
            saveAsFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save_as.png"));
            saveAsFileMI.setDisable(true);
            exportFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export.png"));
            exportFileMI.setDisable(true);
            saveXMLFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_html_as.png"));
            saveXMLFileMI.setDisable(true);
            printFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/print.png"));
            printFileMI.setDisable(true);  
        }else{
            saveFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
            saveFileMI.setDisable(false);
            saveAsFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save_as.png"));
            saveAsFileMI.setDisable(false);
            exportFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export.png"));
            exportFileMI.setDisable(false);
            saveXMLFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_html_as.png"));
            saveXMLFileMI.setDisable(false);
            printFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/print.png"));
            printFileMI.setDisable(false);  
            if(isNew){
                //saveFileMI.setVisible(false);
                saveFileMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
                saveFileMI.setDisable(true);
            }
        }
        //Add the action to Buttons.
        newFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                createMenuAction();
            }
        });
        openFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                openMenuAction();
            }
        });
        loadFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                loadRecentMenuAction();
            }
        });
        importFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                importMenuAction();
            }
        });
        saveFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                saveMenuAction();
            }
        });
        saveAsFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                saveAsMenuAction();
            }
        });
        exportFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                exportMenuAction();
             }
        });
        saveXMLFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                exportHtmlAction();
            }
        });
        printFileMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                printMenuAction();
            }
        });
       
        menuSelected.getItems().addAll(newFileMI, openFileMI, loadFileMI, importFileMI, new SeparatorMenuItem(), saveFileMI, saveAsFileMI, new SeparatorMenuItem(), exportFileMI, saveXMLFileMI, printFileMI);
    }
    private void populateFileToolbar(){
        // New file item
        Button newFileBtn = new Button();
        newFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/new.png"));
        newFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("NEWFILE")+" (Ctrl+N)\n"+objDictionaryAction.getWord("TOOLTIPNEWFILE")));
        newFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        newFileBtn.getStyleClass().add("toolbar-button");    
        // Open file item
        Button openFileBtn = new Button();
        openFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/open.png"));
        openFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("OPENFILE")+" (Ctrl+O)\n"+objDictionaryAction.getWord("TOOLTIPOPENFILE")));
        openFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        openFileBtn.getStyleClass().add("toolbar-button");
        // load recent file item
        Button loadFileBtn = new Button();        
        loadFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/open_recent.png"));
        loadFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("LOADFILE")+" (Ctrl+L)\n"+objDictionaryAction.getWord("TOOLTIPLOADFILE")));
        loadFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        loadFileBtn.getStyleClass().addAll("toolbar-button");
        // import file item
        Button importFileBtn = new Button();        
        importFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/import.png"));
        importFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("IMPORTFILE")+" (Ctrl+I)\n"+objDictionaryAction.getWord("TOOLTIPIMPORTFILE")));
        importFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        importFileBtn.getStyleClass().addAll("toolbar-button");
        // Save file item
        saveFileBtn = new Button();        
        saveFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
        saveFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SAVEFILE")+" (Ctrl+S)\n"+objDictionaryAction.getWord("TOOLTIPSAVEFILE")));
        saveFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        saveFileBtn.getStyleClass().addAll("toolbar-button");
        // Save As file item
        Button saveAsFileBtn = new Button();        
        saveAsFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save_as.png"));
        saveAsFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SAVEASFILE")+" (Ctrl+Shift+S)\n"+objDictionaryAction.getWord("TOOLTIPSAVEASFILE")));
        saveAsFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        saveAsFileBtn.getStyleClass().addAll("toolbar-button");        
        // export file item
        Button exportFileBtn = new Button();        
        exportFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export.png"));
        exportFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("EXPORTFILE")+" (Ctrl+E)\n"+objDictionaryAction.getWord("TOOLTIPEXPORTFILE")));
        exportFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        exportFileBtn.getStyleClass().addAll("toolbar-button");
        // Save As file item
        Button saveXMLFileBtn = new Button();        
        saveXMLFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_html_as.png"));
        saveXMLFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SAVEXMLFILE")+" (Ctrl+Shift+X)\n"+objDictionaryAction.getWord("TOOLTIPSAVEXMLFILE")));
        saveXMLFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        saveXMLFileBtn.getStyleClass().addAll("toolbar-button");
        // print File menu
        Button printFileBtn = new Button();        
        printFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/print.png"));
        printFileBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("PRINTFILE")+" (Ctrl+P)\n"+objDictionaryAction.getWord("TOOLTIPPRINTFILE")));
        printFileBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        printFileBtn.getStyleClass().addAll("toolbar-button");
        //Working conditions check
        if(!isWorkingMode){
            saveFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
            saveFileBtn.setDisable(true);
            saveFileBtn.setCursor(Cursor.WAIT);
            saveAsFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save_as.png"));
            saveAsFileBtn.setDisable(true);
            saveAsFileBtn.setCursor(Cursor.WAIT);
            exportFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export.png"));
            exportFileBtn.setDisable(true);
            exportFileBtn.setCursor(Cursor.WAIT); 
            saveXMLFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_html_as.png"));
            saveXMLFileBtn.setDisable(true);
            saveXMLFileBtn.setCursor(Cursor.WAIT);
            printFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/print.png"));
            printFileBtn.setDisable(true);
            printFileBtn.setCursor(Cursor.WAIT);
        }else{
            saveFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
            saveFileBtn.setDisable(false);
            saveFileBtn.setCursor(Cursor.HAND); 
            saveAsFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save_as.png"));
            saveAsFileBtn.setDisable(false);
            saveAsFileBtn.setCursor(Cursor.HAND); 
            exportFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export.png"));
            exportFileBtn.setDisable(false);
            exportFileBtn.setCursor(Cursor.HAND);
            saveXMLFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_html_as.png"));
            saveXMLFileBtn.setDisable(false);
            saveXMLFileBtn.setCursor(Cursor.HAND); 
            printFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/print.png"));
            printFileBtn.setDisable(false);
            printFileBtn.setCursor(Cursor.HAND);
            if(isNew){
                //saveFileBtn.setVisible(false);
                saveFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
                saveFileBtn.setDisable(true);
                saveFileBtn.setCursor(Cursor.WAIT);
            }
        }
        //Add the action to Buttons.
        newFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                createMenuAction();
            }
        });
        openFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                openMenuAction();
            }
        });
        loadFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadRecentMenuAction();
            }
        });
        importFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                importMenuAction();
            }
        });
        saveFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                saveMenuAction();
            }
        });
        saveAsFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                saveAsMenuAction();
            }
        });
        exportFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                exportMenuAction();
             }
        });
        saveXMLFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                exportHtmlAction();
            }
        });  
        printFileBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                printMenuAction();
            }
        });  
        //Create some Buttons.        
        FlowPane flow = new FlowPane();        
        flow.setPrefWrapLength(objConfiguration.WIDTH*.95);
        flow.getStyleClass().addAll("flow");
        //Add the Buttons to the ToolBar.
        flow.getChildren().addAll(newFileBtn,openFileBtn,loadFileBtn,importFileBtn,saveFileBtn,saveAsFileBtn,exportFileBtn,saveXMLFileBtn,printFileBtn);
        flow.setAlignment(Pos.TOP_LEFT);
        toolBar.getItems().addAll(flow); 
    }    
    private void createMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONNEWFILE"));
        weaveDetails(false);
    }
    
    private void weaveDetails(final boolean isResize){
        final Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 333, 333, Color.WHITE);
        scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());

        GridPane popup = new GridPane();
        popup.setId("popup");
        popup.setAlignment(Pos.CENTER);
        popup.setHgap(10);
        popup.setVgap(10);
        popup.setPadding(new Insets(25, 25, 25, 25));
        
        Tab weaveTab=new Tab();
        Tab fabricTab=new Tab();
        
        weaveTab.setClosable(false);
        fabricTab.setClosable(false);
        
        weaveTab.setText(objDictionaryAction.getWord("WEAVE"));
        fabricTab.setText(objDictionaryAction.getWord("FABRIC"));
        
        weaveTab.setTooltip(new Tooltip(objDictionaryAction.getWord("WEAVE")));
        fabricTab.setTooltip(new Tooltip(objDictionaryAction.getWord("FABRIC")));
        
        GridPane weaveGP=new GridPane();
        weaveGP.setId("subpopup");
        weaveGP.setAlignment(Pos.CENTER);
        weaveGP.setHgap(10);
        weaveGP.setVgap(10);
        weaveGP.setPadding(new Insets(25, 25, 25, 25));
        
        GridPane fabricGP=new GridPane();
        fabricGP.setId("subpopup");
        fabricGP.setAlignment(Pos.CENTER);
        fabricGP.setHgap(10);
        fabricGP.setVgap(10);
        fabricGP.setPadding(new Insets(25, 25, 25, 25));
        
        //Weave Tab
        Label lblWarp = new Label(objDictionaryAction.getWord("WARP")+" (Max "+MAX_GRID_COL+")");
        lblWarp.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPWARP")));
        lblWarp.setStyle("-fx-font-size: 14px;");
        weaveGP.add(lblWarp, 0, 0);
        txtWarp = new TextField(){
            @Override public void replaceText(int start, int end, String text) {
                if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
                if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        txtWarp.setPromptText(objDictionaryAction.getWord("TOOLTIPWARP"));
        txtWarp.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPWARP")));
        if(isResize && objWeave!=null)
            txtWarp.setText(String.valueOf(objWeave.getIntWarp()));
        weaveGP.add(txtWarp, 1, 0);
        
        Label lblWeft = new Label(objDictionaryAction.getWord("WEFT")+" (Max "+MAX_GRID_ROW+")");
        lblWeft.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPWEFT")));
        lblWeft.setStyle("-fx-font-size: 14px;");
        weaveGP.add(lblWeft, 0, 1);
        txtWeft = new TextField(){
            @Override public void replaceText(int start, int end, String text) {
                if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
                if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        txtWeft.setPromptText(objDictionaryAction.getWord("TOOLTIPWEFT"));
        txtWeft.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPWEFT")));
        if(isResize && objWeave!=null)
            txtWeft.setText(String.valueOf(objWeave.getIntWeft()));
        weaveGP.add(txtWeft, 1, 1);
        
        Label lblShaft = new Label(objDictionaryAction.getWord("SHAFT")+" (Max "+MAX_GRID_COL+")");
        lblShaft.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSHAFT")));
        lblShaft.setStyle("-fx-font-size: 14px;");
        weaveGP.add(lblShaft, 0, 2);
        txtShaft = new TextField(){
            @Override public void replaceText(int start, int end, String text) {
                if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
                if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        txtShaft.setPromptText(objDictionaryAction.getWord("TOOLTIPSHAFT"));
        txtShaft.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSHAFT")));
        if(isResize && objWeave!=null)
            txtShaft.setText(String.valueOf(objWeave.getIntWeaveShaft()));
        weaveGP.add(txtShaft, 1, 2);
        
        Label lblTreadles = new Label(objDictionaryAction.getWord("TRADLE")+" (Max "+MAX_GRID_ROW+")");
        lblTreadles.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPTRADLE")));
        lblTreadles.setStyle("-fx-font-size: 14px;");
        weaveGP.add(lblTreadles, 0, 3);
        txtTreadles = new TextField(){
            @Override public void replaceText(int start, int end, String text) {
                if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
                if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        txtTreadles.setPromptText(objDictionaryAction.getWord("TOOLTIPTRADLE"));
        txtTreadles.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPTRADLE")));
        if(isResize && objWeave!=null)
            txtTreadles.setText(String.valueOf(objWeave.getIntWeaveTreadles()));
        weaveGP.add(txtTreadles, 1, 3);
        
        Label lblWeaveType = new Label(objDictionaryAction.getWord("TYPE"));
        lblWeaveType.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPTYPE")));
        lblWeaveType.setStyle("-fx-font-size: 14px;");
        weaveGP.add(lblWeaveType, 0, 4);
        final ComboBox weaveTypeCB = new ComboBox();
        weaveTypeCB.getItems().addAll("plain","twill","satin","basket","sateen","other");
        weaveTypeCB.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPTYPE")));
        if(isResize && objWeave!=null)
            weaveTypeCB.setValue(objWeave.getStrWeaveType());
        else
            weaveTypeCB.setValue("plain");
        weaveGP.add(weaveTypeCB, 1, 4);
        
        //Fabric Tab
        Label lblWarpDensity = new Label(objDictionaryAction.getWord("EPI"));
        lblWarpDensity.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPEPI")));
        lblWarpDensity.setStyle("-fx-font-size: 14px;");
        fabricGP.add(lblWarpDensity, 0, 0);
        final TextField txtWarpDensity = new TextField(){
            @Override public void replaceText(int start, int end, String text) {
                if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
                if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        txtWarpDensity.setPromptText(objDictionaryAction.getWord("TOOLTIPEPI"));
        txtWarpDensity.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPEPI")));
        if(isResize && objWeave!=null)
            txtWarpDensity.setText(String.valueOf(objWeave.getIntEPI()));
        else
            txtWarpDensity.setText(String.valueOf(objConfiguration.getIntEPI()));
        fabricGP.add(txtWarpDensity, 1, 0);
        
        Label lblWeftDensity = new Label(objDictionaryAction.getWord("PPI"));
        lblWeftDensity.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPPPI")));
        lblWeftDensity.setStyle("-fx-font-size: 14px;");
        fabricGP.add(lblWeftDensity, 0, 1);
        final TextField txtWeftDensity = new TextField(){
            @Override public void replaceText(int start, int end, String text) {
                if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
                if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        txtWeftDensity.setPromptText(objDictionaryAction.getWord("TOOLTIPPPI"));
        txtWeftDensity.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPPPI")));
        if(isResize && objWeave!=null)
            txtWeftDensity.setText(String.valueOf(objWeave.getIntPPI()));
        else
            txtWeftDensity.setText(String.valueOf(objConfiguration.getIntPPI()));
        fabricGP.add(txtWeftDensity, 1, 1);
        
        Label lblWarpSelvage = new Label(objDictionaryAction.getWord("WARP")+" "+objDictionaryAction.getWord("SELVAGE"));
        lblWarpSelvage.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSELVAGE")));
        lblWarpSelvage.setStyle("-fx-font-size: 14px;");
        //fabricGP.add(lblWarpSelvage, 0, 2);
        final TextField txtWarpSelvage = new TextField(){
            @Override public void replaceText(int start, int end, String text) {
                if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
                if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        txtWarpSelvage.setPromptText(objDictionaryAction.getWord("TOOLTIPSELVAGE"));
        txtWarpSelvage.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSELVAGE")));
        //if(isResize && objWeave!=null)
            txtWarpSelvage.setText("0"/*String.valueOf(objWeave.getObjConfiguration().getIntWarpSelvage())*/);
        txtWarpSelvage.setEditable(false);
        //fabricGP.add(txtWarpSelvage, 1, 2);
        
        Label lblWeftSelvage = new Label(objDictionaryAction.getWord("WEFT")+" "+objDictionaryAction.getWord("SELVAGE"));
        lblWeftSelvage.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSELVAGE")));
        lblWeftSelvage.setStyle("-fx-font-size: 14px;");
        //fabricGP.add(lblWeftSelvage, 0, 3);
        final TextField txtWeftSelvage = new TextField(){
            @Override public void replaceText(int start, int end, String text) {
                if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
                if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        txtWeftSelvage.setPromptText(objDictionaryAction.getWord("TOOLTIPSELVAGE"));
        txtWeftSelvage.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSELVAGE")));
        //if(isResize && objWeave!=null)
            txtWeftSelvage.setText("0"/*String.valueOf(objWeave.getObjConfiguration().getIntWeftSelvage())*/);
        txtWeftSelvage.setEditable(false);
        //fabricGP.add(txtWeftSelvage, 1, 3);
                
        Label lblWarpShrinkage = new Label(objDictionaryAction.getWord("WARP")+" "+objDictionaryAction.getWord("SHRINKAGE"));
        lblWarpShrinkage.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSHRINKAGE")));
        lblWarpShrinkage.setStyle("-fx-font-size: 14px;");
        //fabricGP.add(lblWarpShrinkage, 0, 4);
        final TextField txtWarpShrinkage = new TextField(){
            @Override public void replaceText(int start, int end, String text) {
                if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
                if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        txtWarpShrinkage.setPromptText(objDictionaryAction.getWord("TOOLTIPSHRINKAGE"));
        txtWarpShrinkage.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSHRINKAGE")));
        //if(isResize && objWeave!=null)
            txtWarpShrinkage.setText(String.valueOf(objWeave.getObjConfiguration().getIntWarpCrimp()));
        txtWarpShrinkage.setEditable(false);
        //fabricGP.add(txtWarpShrinkage, 1, 4);
        
        Label lblWeftShrinkage = new Label(objDictionaryAction.getWord("WEFT")+" "+objDictionaryAction.getWord("SHRINKAGE"));
        lblWeftShrinkage.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSHRINKAGE")));
        lblWeftShrinkage.setStyle("-fx-font-size: 14px;");
        //fabricGP.add(lblWeftShrinkage, 0, 5);
        final TextField txtWeftShrinkage = new TextField(){
            @Override public void replaceText(int start, int end, String text) {
                if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
                if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        txtWeftShrinkage.setPromptText(objDictionaryAction.getWord("TOOLTIPSHRINKAGE"));
        txtWeftShrinkage.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSHRINKAGE")));
        //if(isResize && objWeave!=null)
            txtWeftShrinkage.setText(String.valueOf(objWeave.getObjConfiguration().getIntWeftCrimp()));
        txtWeftShrinkage.setEditable(false);
        //fabricGP.add(txtWeftShrinkage, 1, 5);
                
        weaveTab.setContent(weaveGP);
        fabricTab.setContent(fabricGP);
        
        TabPane tabPane=new TabPane();
        tabPane.getTabs().addAll(weaveTab, fabricTab);
        
        popup.add(tabPane, 0, 0, 2, 1);
        
        Button btnSubmit = new Button(objDictionaryAction.getWord("SUBMIT"));
        btnSubmit.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUBMIT")));
        btnSubmit.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
        btnSubmit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                try {
                    if(txtWarp.getText().length()>0&&txtWeft.getText().length()>0&&txtShaft.getText().length()>0&&txtTreadles.getText().length()>0){
                        int intWeft=Integer.parseInt(txtWeft.getText());
                        int intWarp = Integer.parseInt(txtWarp.getText());
                        int intShaft =Integer.parseInt(txtShaft.getText());
                        int intTreadles =Integer.parseInt(txtTreadles.getText());
                        
                        int intEPI=(txtWarpDensity.getText().length()>0)?Integer.parseInt(txtWarpDensity.getText()):100;
                        int intPPI = (txtWeftDensity.getText().length()>0)?Integer.parseInt(txtWeftDensity.getText()):100;
                        //System.err.println(intEPI+"="+intPPI);
                        if(intWeft>0 && intWeft<=MAX_GRID_ROW && intWarp>0 && intWarp<=MAX_GRID_COL && intShaft>0 && intShaft<=intWarp && intTreadles>0 && intTreadles<=intWeft){
                            if(isResize) {                                
                                if((objWeave.getIntWarp()==intWarp && objWeave.getIntWeft()==intWeft)){
                                    objWeave.setIntEPI(intEPI);
                                    objWeave.setIntPPI(intPPI); 
                                    // add shaft and treadeles
                                    objWeave.setIntWeaveShaft(intShaft);
                                    initShaft();
                                    plotShaft();
                                    objWeave.setIntWeaveTreadles(intTreadles);
                                    initTreadles();
                                    plotTradeles();
                                    dialogStage.close();
                                    return;
                                }
                            } else {
                                objWeave = new Weave();
                                objWeaveAction = new WeaveAction();
                                objWeave.setObjConfiguration(objConfiguration);
                                objWeave.setStrWeaveID(new IDGenerator().getIDGenerator("WEAVE_LIBRARY", objConfiguration.getObjUser().getStrUserID()));
                                clearImage(designImage);
                                //objWeave.setStrThreadPalettes(objConfiguration.strThreadPalettes);
                                objWeave.setWarpYarn(new Yarn[intWarp]);
                                objWeave.setWeftYarn(new Yarn[intWeft]);
                                objUR.clear();                                                                 
                            }
                            objWeave.setStrWeaveType("other");
                            objWeave.setIntEPI(intEPI);
                            objWeave.setIntPPI(intPPI); 
                            objWeave.setIntWarp(intWarp);
                            objWeave.setIntWeft(intWeft);
                            objWeave.setIntWeaveShaft(intShaft);
                            objWeave.setIntWeaveTreadles(intTreadles);
                                
                            updateActiveDesignGrid(objWeave.getIntWarp(), objWeave.getIntWeft());
                            //refreshDesignImage();
                            //initObjWeave(true);
                            //disableWarpWeftDelete();
                            //Weave objClonedWeave=cloneWeave();
                            //objUR.doCommand("Redrawing Design Pane", objClonedWeave);
                            //populateNplot();
                            dialogStage.close();
                        } else{
                            lblStatus.setText(objDictionaryAction.getWord("INVALIDWEAVE"));
                        }
                    }
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        popup.add(btnSubmit, 0, 1, 1, 1);
        
        Button btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));
        btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
        btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        btnCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) { 
                dialogStage.close();
            }
        });
        popup.add(btnCancel, 1, 1, 1, 1); 
        
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WEAVINGDETAILS"));
        dialogStage.showAndWait();
    }
    private void openMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONOPENFILE"));
        try {
            Weave tempWeave = new Weave();
            tempWeave.setObjConfiguration(objConfiguration);
            tempWeave.setIntEPI(objConfiguration.getIntEPI());
            tempWeave.setIntPPI(objConfiguration.getIntPPI());
            WeaveImportView objWeaveImportView= new WeaveImportView(tempWeave);
            if(tempWeave.getStrWeaveID()!=null){
                objWeave=tempWeave;
                loadWeave();
                objUR.clear();
                Weave objClonedWeave=cloneWeave();
                objUR.doCommand("Load Weave", objClonedWeave);
            }
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void loadRecentMenuAction(){
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
            lblStatus.setText(objDictionaryAction.getWord("ACTIONLOADFILE"));
            try {
                if(objConfiguration.getStrRecentWeave()!=null){
                    objWeave = new Weave();
                    objWeave.setObjConfiguration(objConfiguration);
                    objWeave.setIntEPI(objConfiguration.getIntEPI());
                    objWeave.setIntPPI(objConfiguration.getIntPPI());
                    objWeave.setStrWeaveID(objConfiguration.getStrRecentWeave());
                    loadWeave();
                    objUR.clear();
                    Weave objClonedWeave=cloneWeave();
                    objUR.doCommand("Recent Weave", objClonedWeave);
                }else{
                    objWeave.setBytIsLiftPlan((byte)0);
                    objWeave.setStrCondition("");
                    objWeave.setStrOrderBy("Date");
                    objWeave.setStrSearchBy("All");
                    objWeave.setStrDirection("Descending");
                    objWeave.setStrSearchAccess("All User Data");
                    objWeaveAction = new WeaveAction();
                    List lstWeave = (ArrayList)(objWeaveAction.lstImportWeave(objWeave)).get(0);
                    if(lstWeave!=null){
                        objWeave = new Weave();
                        objWeave.setObjConfiguration(objConfiguration);
                        objWeave.setIntEPI(objConfiguration.getIntEPI());
                        objWeave.setIntPPI(objConfiguration.getIntPPI());
                        objWeave.setStrWeaveID(lstWeave.get(0).toString());
                        loadWeave();
                        objUR.clear();
                        Weave objClonedWeave=cloneWeave();
                        objUR.doCommand("Recent Weave", objClonedWeave);
                    }
                }
            } catch (Exception ex) {
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        }
    }
    private void importMenuAction(){
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
            lblStatus.setText(objDictionaryAction.getWord("ACTIONIMPORTFILE"));
            try {
                FileChooser fileChooser = new FileChooser();             
                //Set extension filter
                FileChooser.ExtensionFilter extFilterWIF = new FileChooser.ExtensionFilter("WIF files (*.wif)", "*.wif");
                FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
                FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("BMP files (*.bmp)", "*.BMP");
                fileChooser.getExtensionFilters().addAll(extFilterWIF, extFilterBMP); //extFilterPNG
                //fileChooser.setInitialDirectory(new File(objFabric.getObjConfiguration().strRoot));
                fileChooser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("SELECTTO")+" "+objDictionaryAction.getWord("WEAVE"));
                //Show open file dialog
                File file=fileChooser.showOpenDialog(weaveStage);
                if(file!=null){
                    //System.err.println(file.getName()+"="+file.getName().indexOf(".wif"));
                    if(file.getName().indexOf(".wif")>0){
                        InputStream in = new FileInputStream(file);
                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                        String line;
                        String content="";
                        while ((line = reader.readLine()) != null) {
                            content+="\n"+line;
                        }
                        reader.close();
                        in.close();
                        objWeaveAction = new WeaveAction();
                        if(objWeaveAction.isValidWeaveContent(content)){
                            objWeave = new Weave();
                            objWeave.setObjConfiguration(objConfiguration);
                            objWeave.setIntEPI(objConfiguration.getIntEPI());
                            objWeave.setIntPPI(objConfiguration.getIntPPI());
                            objWeave.setStrWeaveID(new IDGenerator().getIDGenerator("WEAVE_LIBRARY", objConfiguration.getObjUser().getStrUserID()));
                            objWeave.setStrWeaveFile(content);
                            //System.err.println(objWeave.getStrWeaveFile());
                            isNew = true;
                            initWeaveValue();
                            populateNplot();
                            objUR.clear();
                            Weave objClonedWeave=cloneWeave();
                            objUR.doCommand("Import Weave", objClonedWeave);
                        } else{
                            lblStatus.setText(objDictionaryAction.getWord("INVALIDWEAVE"));
                        }
                    }else{
                        BufferedImage bufferedImage = ImageIO.read(file);
                        int intWidth = bufferedImage.getWidth();
                        int intHeight = bufferedImage.getHeight();        
                        byte[][] imageMatrix = new byte[intHeight][intWidth];
                        //reduce to max 2 colors
                        ArtworkAction objArtworkAction = new ArtworkAction();
                        bufferedImage = objArtworkAction.rectifyImage(bufferedImage,2);
                        ArrayList colors = objArtworkAction.getImageColor(bufferedImage);
                        // create image matrix        
                        for(int y = 0; y < intHeight; y++) {
                            for(int x = 0; x < intWidth; x++) {
                                int pixel = bufferedImage.getRGB(x, y);     
                                int red   = (pixel & 0x00ff0000) >> 16;
                                int green = (pixel & 0x0000ff00) >> 8;
                                int blue  =  pixel & 0x000000ff;                    
                                java.awt.Color color = new java.awt.Color(red,green,blue);     
                                for(int z=0; z<colors.size(); z++){
                                    if(colors.get(z).equals(color)){
                                        imageMatrix[y][x] = (byte)z;                           
                                    }
                                }
                            }
                        }                        
                        /*
                        for(int i=0; i<intHeight;i++){
                            for(int j=0; j<intWidth;j++){
                                System.err.print("\t"+imageMatrix[i][j]);
                            }
                            System.err.println("");
                        }*/
                        objWeave = new Weave();
                        objWeave.setObjConfiguration(objConfiguration);
                        activeGridCol = intWidth; 
                        activeGridRow = intHeight;
                        initObjWeave(false);
                        objWeave.setIntEPI(objConfiguration.getIntEPI());
                        objWeave.setIntPPI(objConfiguration.getIntPPI());
                        objWeave.setStrWeaveID(new IDGenerator().getIDGenerator("WEAVE_LIBRARY", objConfiguration.getObjUser().getStrUserID()));
                        objWeave.setDesignMatrix(imageMatrix);
                        objWeave.setIntWeft(intHeight);
                        objWeave.setIntWarp(intWidth);
                       
                        objWeave.setDentMatrix(new byte[2][objWeave.getIntWarp()]);
                        for(int i=0; i<2; i++)
                            for(int j=0; j<objWeave.getIntWarp(); j++)
                                objWeave.getDentMatrix()[i][j]=(byte)((i+j)%2);
                                                
                        /*colors
                        for(int i=0; i<colors.size()/2; i++){
                            for(int j=0; j<26; j++)
                                objWeave.getWarpYarn()[i]=objWeave.getObjConfiguration().getYarnPalette()[j];
                        }
                        for(int i=0; i<colors.size()/2; i++){
                            for(int j=26; j<52; j++)
                                objWeave.getWeftYarn()[i]=objWeave.getObjConfiguration().getYarnPalette()[j];
                        }*/ 
                        populateDplot();                        
                        populateNplot();
                        objUR.clear();
                        Weave objClonedWeave=cloneWeave();
                        objUR.doCommand("Import Weave", objClonedWeave);
                        isNew = true;
                        isWorkingMode = true;
                        //System.exit(0);
                    }
                }else{
                    lblStatus.setText(objDictionaryAction.getWord("NOITEM"));
                }            
            } catch (FileNotFoundException ex) {
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            } catch (IOException ex) {
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            } catch (SQLException ex) {
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        }
    }
    private void saveMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONSAVEFILE"));
        isNew=false;
        saveUpdateAction();//saveAction();
        System.gc();
    }
    private void saveAsMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONSAVEASFILE"));
        objWeave.setStrWeaveAccess(objConfiguration.getObjUser().getUserAccess("WEAVE_LIBRARY"));
        isNew=true;
        saveUpdateAction();//saveAsAction();
        System.gc();
    }
    private void saveUpdateAction(){
        // if fabric access is Public, we need not show dialog for Save
        if(!isNew && objWeave.getStrWeaveAccess().equalsIgnoreCase("Public")){
            saveAction();
        } else{
            final Stage dialogStage = new Stage();
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            BorderPane root = new BorderPane();
            Scene scene = new Scene(root, 350, 300, Color.WHITE);
            scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());

            GridPane popup=new GridPane();
            popup.setId("popup");
            popup.setAlignment(Pos.CENTER);
            popup.setHgap(10);
            popup.setVgap(10);
            popup.setPadding(new Insets(25, 25, 25, 25));

            final TextField txtName = new TextField(objWeave.getStrWeaveID());
            final ComboBox cbType = new ComboBox();
            if(isNew){
                Label lblName = new Label(objDictionaryAction.getWord("NAME"));
                lblName.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPNAME")));
                popup.add(lblName, 0, 0);
                txtName.setPromptText(objDictionaryAction.getWord("TOOLTIPNAME"));
                txtName.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPNAME")));
                popup.add(txtName, 1, 0, 2, 1);
                Label lblType = new Label(objDictionaryAction.getWord("TYPE"));
                lblType.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPTYPE")));
                popup.add(lblType, 0, 1);
                cbType.getItems().addAll("plain","twill","satin","basket","sateen","other");
                cbType.setValue("other");
                cbType.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPTYPE")));
                popup.add(cbType, 1, 1, 2, 1);
            }

            final ToggleGroup weaveTG = new ToggleGroup();
            RadioButton weavePublicRB = new RadioButton(objDictionaryAction.getWord("PUBLIC"));
            weavePublicRB.setToggleGroup(weaveTG);
            weavePublicRB.setUserData("Public");
            popup.add(weavePublicRB, 0, 2);
            RadioButton weavePrivateRB = new RadioButton(objDictionaryAction.getWord("PRIVATE"));
            weavePrivateRB.setToggleGroup(weaveTG);
            weavePrivateRB.setUserData("Private");
            popup.add(weavePrivateRB, 1, 2);
            RadioButton weaveProtectedRB = new RadioButton(objDictionaryAction.getWord("PROTECTED"));
            weaveProtectedRB.setToggleGroup(weaveTG);
            weaveProtectedRB.setUserData("Protected");
            popup.add(weaveProtectedRB, 2, 2);
            if(objConfiguration.getObjUser().getUserAccess("WEAVE_LIBRARY").equalsIgnoreCase("Public"))
                weaveTG.selectToggle(weavePublicRB);
            else if(objConfiguration.getObjUser().getUserAccess("WEAVE_LIBRARY").equalsIgnoreCase("Protected"))
                weaveTG.selectToggle(weaveProtectedRB);
            else
                weaveTG.selectToggle(weavePrivateRB);
            if(!isNew && objWeave.getStrWeaveAccess()!=null){
                if(objWeave.getStrWeaveAccess().equalsIgnoreCase("Public"))
                    weaveTG.selectToggle(weavePublicRB);
                else if(objWeave.getStrWeaveAccess().equalsIgnoreCase("Protected"))
                    weaveTG.selectToggle(weaveProtectedRB);
                else
                    weaveTG.selectToggle(weavePrivateRB);
            }
            
            final PasswordField passPF= new PasswordField();
            final Label lblAlert = new Label();
            if(objConfiguration.getBlnAuthenticateService()){
                Label lblPassword=new Label(objDictionaryAction.getWord("SERVICEPASSWORD"));
                lblPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                popup.add(lblPassword, 0, 3);
                passPF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                passPF.setPromptText(objDictionaryAction.getWord("PROMPTSERVICEPASSWORD"));
                popup.add(passPF, 1, 3, 2, 1);                
            }
            lblAlert.setStyle("-fx-wrap-text:true;");
            lblAlert.setPrefWidth(250);
            popup.add(lblAlert, 0, 5, 3, 1);
                
            Button btnSave = new Button(objDictionaryAction.getWord("SAVE"));
            btnSave.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSAVE")));
            btnSave.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
            btnSave.setDefaultButton(true);
            btnSave.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    objWeave.setStrWeaveAccess(weaveTG.getSelectedToggle().getUserData().toString());
                    if(txtName.getText().trim().length()==0){
                        lblAlert.setText(objDictionaryAction.getWord("WRONGINPUT"));
                    } else{
                        if(objConfiguration.getBlnAuthenticateService()){
                            if(passPF.getText().length()==0){
                                lblAlert.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                            } else{
                                if(Security.SecurePassword(passPF.getText(), objConfiguration.getObjUser().getStrUsername()).equals(objConfiguration.getObjUser().getStrAppPassword())){   
                                    if(isNew){
                                        if(txtName.getText().trim().length()==0)
                                            objWeave.setStrWeaveName("my weave");
                                        else
                                            objWeave.setStrWeaveName(txtName.getText());
                                        objWeave.setStrWeaveCategory(cbType.getValue().toString());
                                        saveAsAction();
                                    } else {
                                        saveAction();
                                    }
                                    dialogStage.close();
                                }
                                else{
                                    lblAlert.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                                }
                            }
                        } else{   // service password is disabled
                            if(isNew){
                                if(txtName.getText().trim().length()==0)
                                    objWeave.setStrWeaveName("my weave");
                                else
                                    objWeave.setStrWeaveName(txtName.getText());
                                objWeave.setStrWeaveCategory(cbType.getValue().toString());
                                saveAsAction();
                            } else {
                                saveAction();
                            }
                            dialogStage.close();
                        }
                    }
                }
            });
            popup.add(btnSave, 1, 4);
            Button btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));
            btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
            btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            btnCancel.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {  
                    dialogStage.close();
                    lblStatus.setText(objDictionaryAction.getWord("TOOLTIPCANCEL"));
                }
            });
            popup.add(btnCancel, 0, 4);
            root.setCenter(popup);
            dialogStage.setScene(scene);
            dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("SAVE"));
            dialogStage.showAndWait();
        }
    }
    private void saveAction(){
        try {
            objWeave.setBytIsLiftPlan((byte)0);
            objWeaveAction = new WeaveAction();
            if(objWeaveAction.isValidWeave(objWeave)){
                //objWeaveAction.singleWeaveContent(objWeave);
                objWeaveAction.injectWeaveContent(objWeave);
                //System.err.println(objWeave.getStrWeaveFile());
                objWeaveAction.getWeaveImage(objWeave);
                //objWeaveAction.getSingleWeaveImage(objWeave);
                objWeaveAction.resetWeave(objWeave);
                
                /*
                if(objFabricAction.setFabric(objFabric)>0){
                objFabricAction = new FabricAction();
                objFabricAction.setFabricPallets(objFabric);
                //objFabricAction = new FabricAction();
                //objFabricAction.setFabricYarn(objFabric);
                //objFabricAction.clearFabricYarn(objFabric.getStrFabricID());
                String yarnId = null;
                YarnAction objYarnAction;
                for(int i = 0; i<objFabric.getWarpYarn().length; i++){
                    yarnId = new IDGenerator().getIDGenerator("YARN_LIBRARY", objConfiguration.getObjUser().getStrUserID());
                    objFabric.getWarpYarn()[i].setStrYarnId(yarnId);
                    objFabric.getWarpYarn()[i].setObjConfiguration(objConfiguration);
                    objYarnAction = new YarnAction();
                    objYarnAction.setYarn(objFabric.getWarpYarn()[i]);
                    objYarnAction = new YarnAction();
                    objYarnAction.setFabricYarn(objFabric.getStrFabricID(), objFabric.getWarpYarn()[i],i);
                }
                for(int i = 0; i<objFabric.getWeftYarn().length; i++){
                    yarnId = new IDGenerator().getIDGenerator("YARN_LIBRARY", objConfiguration.getObjUser().getStrUserID());
                    objFabric.getWeftYarn()[i].setStrYarnId(yarnId);
                    objFabric.getWeftYarn()[i].setObjConfiguration(objConfiguration);
                    objYarnAction = new YarnAction();
                    objYarnAction.setYarn(objFabric.getWeftYarn()[i]);
                    objYarnAction = new YarnAction();
                    objYarnAction.setFabricYarn(objFabric.getStrFabricID(), objFabric.getWeftYarn()[i],i);
                }
                if(objFabric.getWarpExtraYarn()!=null){
                    for(int i = 0; i<objFabric.getWarpExtraYarn().length; i++){
                        yarnId = new IDGenerator().getIDGenerator("YARN_LIBRARY", objConfiguration.getObjUser().getStrUserID());
                        objFabric.getWarpExtraYarn()[i].setStrYarnId(yarnId);
                        objFabric.getWarpExtraYarn()[i].setObjConfiguration(objConfiguration);
                        objYarnAction = new YarnAction();
                        objYarnAction.setYarn(objFabric.getWarpExtraYarn()[i]);
                    objYarnAction = new YarnAction();
                        objYarnAction.setFabricYarn(objFabric.getStrFabricID(), objFabric.getWarpExtraYarn()[i],i);
                    }
                }
                if(objFabric.getWeftExtraYarn()!=null){
                    for(int i = 0; i<objFabric.getWeftExtraYarn().length; i++){
                        yarnId = new IDGenerator().getIDGenerator("YARN_LIBRARY", objConfiguration.getObjUser().getStrUserID());
                        objFabric.getWeftExtraYarn()[i].setStrYarnId(yarnId);
                        objFabric.getWeftExtraYarn()[i].setObjConfiguration(objConfiguration);
                        objYarnAction = new YarnAction();
                        objYarnAction.setYarn(objFabric.getWeftExtraYarn()[i]);
                    objYarnAction = new YarnAction();
                        objYarnAction.setFabricYarn(objFabric.getStrFabricID(), objFabric.getWeftExtraYarn()[i],i);
                    }
                }
                */
                lblStatus.setText(objWeave.getStrWeaveName()+":"+objDictionaryAction.getWord("DATASAVED"));
            } else{
                lblStatus.setText(objDictionaryAction.getWord("INVALIDWEAVE"));
            }
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void saveAsAction(){
        try {
            objWeave.setBytIsLiftPlan((byte)0);
            objWeave.setStrWeaveType("wif");
            objWeaveAction = new WeaveAction();
            objWeave.setStrWeaveID(new IDGenerator().getIDGenerator("WEAVE_LIBRARY", objConfiguration.getObjUser().getStrUserID()));
            objWeaveAction = new WeaveAction();
            if(objWeaveAction.isValidWeave(objWeave)){
                //objWeaveAction.singleWeaveContent(objWeave);
                objWeaveAction.injectWeaveContent(objWeave);
                //System.err.println(objWeave.getStrWeaveFile());
                
                objWeaveAction.getWeaveImage(objWeave);
                //objWeaveAction.getSingleWeaveImage(objWeave);
                objWeaveAction.setWeave(objWeave);
                lblStatus.setText(objWeave.getStrWeaveName()+":"+objDictionaryAction.getWord("DATASAVED"));
                isNew=false;
                saveFileBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
                //saveFileVB.setVisible(false);
                saveFileBtn.setDisable(false);
            } else{
                new MessageView("error", objDictionaryAction.getWord("INVALIDWEAVE"), objDictionaryAction.getWord("BLANKWARPWEFT"));
                lblStatus.setText(objDictionaryAction.getWord("INVALIDWEAVE"));
            }
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void exportMenuAction(){
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
            lblStatus.setText(objDictionaryAction.getWord("ACTIONEXPORTFILE"));
            try{
                objWeaveAction=new WeaveAction();
                if(objWeaveAction.isValidWeave(objWeave)){
                    char ch='S'; // S,L
                    int Tcount=0;
                    for(int i=0;i<objWeave.getIntWeaveTreadles();i++)
                        for(int j=0;j<objWeave.getIntWeft();j++)
                            if(objWeave.getTreadlesMatrix()[j][i]==1)
                               Tcount++;
                    if(Tcount!=0) {       
                        try {
                            FileChooser artworkExport=new FileChooser();
                            artworkExport.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTPNG")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
                            FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("WIF files (*.wif)", "*.WIF");
                            artworkExport.getExtensionFilters().add(extFilterPNG);
                            File file=artworkExport.showSaveDialog(weaveStage);
                            if(file==null)
                                return;
                            else
                                weaveStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+file.getAbsoluteFile().getName()+"]");   
                            if (!file.getName().endsWith("png")) 
                                file = new File(file.getPath() + objWeave.getStrWeaveID()+".wif");
                                
                            objWeaveAction = new WeaveAction();
                            objWeaveAction.injectWeaveContent(objWeave);
                            //System.err.println(objWeave.getStrWeaveFile());
                            FileWriter writer = new FileWriter(file);
                            writer.write(objWeave.getStrWeaveFile());
                            writer.close();

                            if(objConfiguration.getBlnAuthenticateService()){
                                ArrayList<File> filesToZip=new ArrayList<>();
                                filesToZip.add(file);
                                String zipFilePath=file.getAbsolutePath()+".zip";
                                String passwordToZip = file.getName();
                                new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                                file.delete();
                            }

                            lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+file.getCanonicalPath());
                        } catch (IOException ex) {
                            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                        } catch (Exception ex) {
                            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                        } 
                    }else{
                        lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                    }
                } else{
                    lblStatus.setText(objDictionaryAction.getWord("INVALIDWEAVE"));
                }
            } catch(SQLException sqlEx){
                new Logging("SEVERE",getClass().getName(),sqlEx.toString(),sqlEx);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        }
    }
    private void exportHtmlAction(){
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
            lblStatus.setText(objDictionaryAction.getWord("ACTIONSAVEXMLFILE"));
            saveAsHtml();
            //lblStatus.setText(objDictionaryAction.getWord("DATASAVED"));
        }
    }
    private void saveAsHtml() {   
        try {
            FileChooser fileChoser=new FileChooser();
            fileChoser.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORTHTML")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
            FileChooser.ExtensionFilter extFilterHTML = new FileChooser.ExtensionFilter("HTML (*.html)", "*.html");
            fileChoser.getExtensionFilters().add(extFilterHTML);
            File ifile=fileChoser.showSaveDialog(weaveStage);
            if(ifile==null)
                return;
            else
                weaveStage.setTitle(objDictionaryAction.getWord("PROJECT")+": ["+ifile.getAbsoluteFile().getName()+"]");
            
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
            Date date = new Date();
            String currentDate = dateFormat.format(date);
            String path = ifile.getPath()+"Weave_"+currentDate;
            
            int blocksize = 10;
            //-------------- Preparing Design Image --------------------//
            //System.out.println(blocksize+":"+objWeave.getIntWarp()+":"+objWeave.getIntWeft());
            BufferedImage texture = new BufferedImage(objWeave.getIntWarp(), objWeave.getIntWeft(),BufferedImage.TYPE_INT_RGB);        
            for(int x = 0; x < objWeave.getIntWeft(); x++) {
                for(int y = 0; y < objWeave.getIntWarp(); y++) {
                    if(objWeave.getDesignMatrix()[x][y]==0)
                        texture.setRGB(y, x, java.awt.Color.WHITE.getRGB());
                    else
                        texture.setRGB(y, x, java.awt.Color.BLACK.getRGB());
                }
            }
            BufferedImage bufferedImageesize = new BufferedImage( objWeave.getIntWarp()*blocksize, objWeave.getIntWeft()*blocksize, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bufferedImageesize.createGraphics();
            BasicStroke bs = new BasicStroke(1);
            g.setStroke(bs);
            g.setColor(java.awt.Color.BLACK);
            g.drawImage(texture, 0, 0, objWeave.getIntWarp()*blocksize, objWeave.getIntWeft()*blocksize, null);
            // added for grid lines
            for(int r=0; r<objWeave.getIntWeft(); r++)
                g.drawLine(0, r*blocksize, bufferedImageesize.getWidth()-1, r*blocksize);
            g.drawLine(0, bufferedImageesize.getHeight()-1, bufferedImageesize.getWidth()-1, bufferedImageesize.getHeight()-1);
            for(int c=0; c<objWeave.getIntWarp(); c++)
                g.drawLine(c*blocksize, 0, c*blocksize, bufferedImageesize.getHeight()-1);
            g.drawLine(bufferedImageesize.getWidth()-1, 0, bufferedImageesize.getWidth()-1, bufferedImageesize.getHeight()-1);
            g.dispose();
            File designFile = new File(path+"_design.png");
            ImageIO.write(bufferedImageesize, "png", designFile);
            
            //-------------- Preparing Shaft (Drafting) Image ----------------//
            texture = new BufferedImage(objWeave.getIntWarp(), objWeave.getIntWeaveShaft(),BufferedImage.TYPE_INT_RGB);        
            for(int x = 0; x < objWeave.getIntWeaveShaft(); x++) {
                for(int y = 0; y < objWeave.getIntWarp(); y++) {
                    if(objWeave.getShaftMatrix()[x][y]==0)
                        texture.setRGB(y, x, java.awt.Color.WHITE.getRGB());
                    else
                        texture.setRGB(y, x, java.awt.Color.BLACK.getRGB());
                }
            }
            bufferedImageesize = new BufferedImage( objWeave.getIntWarp()*blocksize, objWeave.getIntWeaveShaft()*blocksize, BufferedImage.TYPE_INT_RGB);
            g = bufferedImageesize.createGraphics();
            g.setStroke(bs);
            g.setColor(java.awt.Color.BLACK);
            g.drawImage(texture, 0, 0, objWeave.getIntWarp()*blocksize, objWeave.getIntWeaveShaft()*blocksize, null);
            // added for grid lines
            for(int r=0; r<objWeave.getIntWeaveShaft(); r++)
                g.drawLine(0, r*blocksize, bufferedImageesize.getWidth()-1, r*blocksize);
            g.drawLine(0, bufferedImageesize.getHeight()-1, bufferedImageesize.getWidth()-1, bufferedImageesize.getHeight()-1);
            for(int c=0; c<objWeave.getIntWarp(); c++)
                g.drawLine(c*blocksize, 0, c*blocksize, bufferedImageesize.getHeight()-1);
            g.drawLine(bufferedImageesize.getWidth()-1, 0, bufferedImageesize.getWidth()-1, bufferedImageesize.getHeight()-1);
            g.dispose();
            File shaftFile = new File(path+"_drafting.png");
            ImageIO.write(bufferedImageesize, "png", shaftFile);
            
            //-------------- Preparing Treadling Image ------------------------//
            texture = new BufferedImage(objWeave.getIntWeaveTreadles(), objWeave.getIntWeft(),BufferedImage.TYPE_INT_RGB);        
            for(int x = 0; x < objWeave.getIntWeft(); x++) {
                for(int y = 0; y < objWeave.getIntWeaveTreadles(); y++) {
                    if(objWeave.getTreadlesMatrix()[x][y]==0)
                        texture.setRGB(y, x, java.awt.Color.WHITE.getRGB());
                    else
                        texture.setRGB(y, x, java.awt.Color.BLACK.getRGB());
                }
            }
            bufferedImageesize = new BufferedImage( objWeave.getIntWeaveTreadles()*blocksize, objWeave.getIntWeft()*blocksize, BufferedImage.TYPE_INT_RGB);
            g = bufferedImageesize.createGraphics();
            g.setStroke(bs);
            g.setColor(java.awt.Color.BLACK);
            g.drawImage(texture, 0, 0, objWeave.getIntWeaveTreadles()*blocksize, objWeave.getIntWeft()*blocksize, null);
            // added for grid lines
            for(int r=0; r<objWeave.getIntWeft(); r++)
                g.drawLine(0, r*blocksize, bufferedImageesize.getWidth()-1, r*blocksize);
            g.drawLine(0, bufferedImageesize.getHeight()-1, bufferedImageesize.getWidth()-1, bufferedImageesize.getHeight()-1);
            for(int c=0; c<objWeave.getIntWeaveTreadles(); c++)
                g.drawLine(c*blocksize, 0, c*blocksize, bufferedImageesize.getHeight()-1);
            g.drawLine(bufferedImageesize.getWidth()-1, 0, bufferedImageesize.getWidth()-1, bufferedImageesize.getHeight()-1);
            g.dispose();
            File treadleFile = new File(path+"_treadles.png");
            ImageIO.write(bufferedImageesize, "png", treadleFile);
            
            //-------------- Preparing Tie Up Image ------------------------//
            texture = new BufferedImage(objWeave.getIntWeaveTreadles(), objWeave.getIntWeaveShaft(),BufferedImage.TYPE_INT_RGB);        
            for(int x = 0; x < objWeave.getIntWeaveShaft(); x++) {
                for(int y = 0; y < objWeave.getIntWeaveTreadles(); y++) {
                    if(objWeave.getTieupMatrix()[x][y]==0)
                        texture.setRGB(y, x, java.awt.Color.WHITE.getRGB());
                    else
                        texture.setRGB(y, x, java.awt.Color.BLACK.getRGB());
                }
            }
            bufferedImageesize = new BufferedImage( objWeave.getIntWeaveTreadles()*blocksize, objWeave.getIntWeaveShaft()*blocksize, BufferedImage.TYPE_INT_RGB);
            g = bufferedImageesize.createGraphics();
            g.setStroke(bs);
            g.setColor(java.awt.Color.BLACK);
            
            g.drawImage(texture, 0, 0, objWeave.getIntWeaveTreadles()*blocksize, objWeave.getIntWeaveShaft()*blocksize, null);
            // added for grid lines
            for(int r=0; r<objWeave.getIntWeaveShaft(); r++)
                g.drawLine(0, r*blocksize, bufferedImageesize.getWidth()-1, r*blocksize);
            g.drawLine(0, bufferedImageesize.getHeight()-1, bufferedImageesize.getWidth()-1, bufferedImageesize.getHeight()-1);
            for(int c=0; c<objWeave.getIntWeaveTreadles(); c++)
                g.drawLine(c*blocksize, 0, c*blocksize, bufferedImageesize.getHeight()-1);
            g.drawLine(bufferedImageesize.getWidth()-1, 0, bufferedImageesize.getWidth()-1, bufferedImageesize.getHeight()-1);
            g.dispose();
            File tieupFile = new File(path+"_tieup.png");
            ImageIO.write(bufferedImageesize, "png", tieupFile);
            
            //-------------- Preparing Peg Plan Image ------------------------//
            texture = new BufferedImage(objWeave.getIntWeaveShaft(), objWeave.getIntWeft(),BufferedImage.TYPE_INT_RGB);        
            for(int x = 0; x < objWeave.getIntWeft(); x++) {
                for(int y = 0; y < objWeave.getIntWeaveShaft(); y++) {
                    if(objWeave.getPegMatrix()[x][y]==0)
                        texture.setRGB(y, x, java.awt.Color.WHITE.getRGB());
                    else
                        texture.setRGB(y, x, java.awt.Color.BLACK.getRGB());
                }
            }
            bufferedImageesize = new BufferedImage( objWeave.getIntWeaveShaft()*blocksize, objWeave.getIntWeft()*blocksize, BufferedImage.TYPE_INT_RGB);
            g = bufferedImageesize.createGraphics();
            g.setStroke(bs);
            g.setColor(java.awt.Color.BLACK);
            g.drawImage(texture, 0, 0, objWeave.getIntWeaveShaft()*blocksize, objWeave.getIntWeft()*blocksize, null);
            // added for grid lines
            for(int r=0; r<objWeave.getIntWeft(); r++)
                g.drawLine(0, r*blocksize, bufferedImageesize.getWidth()-1, r*blocksize);
            g.drawLine(0, bufferedImageesize.getHeight()-1, bufferedImageesize.getWidth()-1, bufferedImageesize.getHeight()-1);
            for(int c=0; c<objWeave.getIntWeaveShaft(); c++)
                g.drawLine(c*blocksize, 0, c*blocksize, bufferedImageesize.getHeight()-1);
            g.drawLine(bufferedImageesize.getWidth()-1, 0, bufferedImageesize.getWidth()-1, bufferedImageesize.getHeight()-1);
            g.dispose();
            
            File pegliftFile = new File(path+"_peg.png");
            ImageIO.write(bufferedImageesize, "png", pegliftFile);
            File htmlFile = new File(path+".html");
            
            String INITIAL_TEXT = " This is coumputer generated, So no signature required";
            String IMAGE_TEXT ="<table border=0 width=\"50%\">"
                                    +"<tr>"
                                    +"<td>"
                                    +"<div><img src=\"file:\\\\"+path+"_drafting.png" + 
             "\" title=\"Drafting\" width=\""+objWeave.getIntWarp()*blocksize+"\" height=\""+objWeave.getIntWeaveShaft()*blocksize+"\"><br>Drafting</div>"
                                    +"</td>"
                                    +"<td>"
                                    +"<div><img src=\"file:\\\\"+path+"_tieup.png" + 
             "\" title=\"Tie Up\" width=\""+objWeave.getIntWeaveTreadles()*blocksize+"\" height=\""+objWeave.getIntWeaveShaft()*blocksize+"\"><br>Tie Up</div>"
                                    +"</td>"
                                    +"</tr>"
                                    +"<tr>"
                                    +"<td>"
                                    +"<div><img src=\"file:\\\\"+path+"_design.png" + 
             "\" title=\"Design\" width=\""+objWeave.getIntWarp()*blocksize+"\" height=\""+objWeave.getIntWeft()*blocksize+"\"><br>Design</div>"
                                    +"</td>"
                                    +"<td>"+
                                    "<div><img src=\"file:\\\\"+path+"_peg.png" + 
             "\" title=\"Peg Plan\" width=\""+objWeave.getIntWeaveShaft()*blocksize+"\" height=\""+objWeave.getIntWeft()*blocksize+"\"><br>Peg Plan</div>"
                                    +"</td>"
                                    +"<td>"
                                    +"<div><img src=\"file:\\\\"+path+"_treadles.png" + 
             "\" title=\"Treadling Order\" width=\""+objWeave.getIntWeaveTreadles()*blocksize+"\" height=\""+objWeave.getIntWeft()*blocksize+"\"><br>Treadling Order</div>"
                                    +"</td>"
                                    +"</tr>"
                                    +"</table>";
            
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(htmlFile, true))) {
                bw.write("<table border=0 width=\"100%\">");
                    bw.newLine();
                    bw.write("<tr>");
                        bw.newLine();
                        bw.write("<td><h1>Digital India Corporation (Media Lab Asia)</h1></td>");
                        bw.newLine();
                        bw.write("<td><h6 align=right>");
                            bw.newLine();
                            bw.write(objDictionaryAction.getWord("PROJECT")+": Weave_"+objWeave.getStrWeaveID()+"_"+currentDate+"<br>");
                            bw.newLine();
                            bw.write("<a href=\"https://dic.gov.in/\">https://dic.gov.in/</a><br>");
                            bw.newLine();
                            bw.write("&copy; Digital India Corporation (Media Lab Asia); New Delhi, India<br><br>"+date);
                            bw.newLine();
                        bw.write("</h6></td>");
                    bw.newLine();
                    bw.write("</tr>");
                bw.newLine();
                bw.write("</table>");
                bw.newLine();
                bw.write("<table border=1 cellspacing=0>");
                bw.newLine();
                bw.write("<tr align=right><th>Weave Category</th><td>"+objWeave.getStrWeaveCategory()+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("TYPE")+"</th><td>"+objWeave.getStrWeaveType()+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("NAME")+"</th><td>"+objWeave.getStrWeaveName()+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("WARP")+"</th><td>"+objWeave.getIntWarp()+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("WEFT")+"</th><td>"+objWeave.getIntWeft()+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("SHAFT")+"</th><td>"+objWeave.getIntWeaveShaft()+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("TRADLE")+"</th><td>"+objWeave.getIntWeaveTreadles()+"</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("PPI")+"</th><td>"+objWeave.getIntPPI()+" / inch</td></tr>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("EPI")+"</th><td>"+objWeave.getIntEPI()+" / inch</td></tr>");
                bw.newLine();
                bw.newLine();
                bw.newLine();
                bw.write("</table>");                
            bw.newLine();
            bw.write("");
            
            ArrayList<String> lstEntry;
            objWeaveAction.generateYarnPattern(objWeave);
            if(objWeave.getWarpYarn().length>0){
                PatternAction objPatternAction = new PatternAction();
                bw.write("<table border=1 cellspacing=0>");
                bw.newLine();
                // Adding Warp Pattern
                bw.write("<caption>"+objDictionaryAction.getWord("WARP")+" "+objDictionaryAction.getWord("YARNPATTERN")+": \t"+objPatternAction.getPattern(objWeave.getObjConfiguration().getStrWarpPatternID()).getStrPattern()+"</caption>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("WARP")+"</th><th>"+objDictionaryAction.getWord("NAME")+"</th><th>"+objDictionaryAction.getWord("YARNTYPE")+"</th><th>"+objDictionaryAction.getWord("YARNREPEAT")+"</th><th>"+objDictionaryAction.getWord("YARNCOUNT")+"</th><th>"+objDictionaryAction.getWord("YARNUNIT")+"</th><th>"+objDictionaryAction.getWord("YARNPLY")+"</th><th>"+objDictionaryAction.getWord("YARNFACTOR")+"</th><th>"+objDictionaryAction.getWord("YARNDIAMETER")+"(mm)</th><th>"+objDictionaryAction.getWord("YARNWEIGHT")+"(kg)</th><th>"+objDictionaryAction.getWord("YARNPRICE")+"(kg)</th><th>"+objDictionaryAction.getWord("YARNTWIST")+"/ inch</th><th>"+objDictionaryAction.getWord("YARNSENCE")+"</th><th>"+objDictionaryAction.getWord("YARNHAIRNESS")+"</th><th>"+objDictionaryAction.getWord("YARNDISTRIBUTION")+"</th><th>"+objDictionaryAction.getWord("YARNCOLOR")+"</th></tr>");
                bw.newLine();
            
                lstEntry = new ArrayList();
                for(int i=0; i<objWeave.getWarpYarn().length; i++){
                    if(lstEntry.size()==0){
                        lstEntry.add(objWeave.getWarpYarn()[i].getStrYarnColor());
                        bw.newLine();
                        bw.write("<tr align=right><td>"+objWeave.getWarpYarn()[i].getStrYarnSymbol()+"</td><td>"+objWeave.getWarpYarn()[i].getStrYarnName()+"</td><td>"+objWeave.getWarpYarn()[i].getStrYarnType()+"</td><td>"+objWeave.getWarpYarn()[i].getIntYarnRepeat()+"</td><td>"+objWeave.getWarpYarn()[i].getIntYarnCount()+"</td><td>"+objWeave.getWarpYarn()[i].getStrYarnCountUnit()+"</td><td>"+objWeave.getWarpYarn()[i].getIntYarnPly()+"</td><td>"+objWeave.getWarpYarn()[i].getIntYarnDFactor()+"</td><td>"+String.format("%.3f",objWeave.getWarpYarn()[i].getDblYarnDiameter())+"</td><td>-</td><td>"+objWeave.getWarpYarn()[i].getDblYarnPrice()+"</td><td>"+objWeave.getWarpYarn()[i].getIntYarnTwist()+"</td><td>"+objWeave.getWarpYarn()[i].getStrYarnTModel()+"</td><td>"+objWeave.getWarpYarn()[i].getIntYarnHairness()+"</td><td>"+objWeave.getWarpYarn()[i].getIntYarnHProbability()+"</td><th bgcolor=\""+objWeave.getWarpYarn()[i].getStrYarnColor()+"\">"+objWeave.getWarpYarn()[i].getStrYarnColor()+"</th></tr>");
                    }
                    //check for redudancy
                    else {                
                        if(!lstEntry.contains(objWeave.getWarpYarn()[i].getStrYarnColor())){
                            lstEntry.add(objWeave.getWarpYarn()[i].getStrYarnColor());
                            bw.newLine();
                            bw.write("<tr align=right><td>"+objWeave.getWarpYarn()[i].getStrYarnSymbol()+"</td><td>"+objWeave.getWarpYarn()[i].getStrYarnName()+"</td><td>"+objWeave.getWarpYarn()[i].getStrYarnType()+"</td><td>"+objWeave.getWarpYarn()[i].getIntYarnRepeat()+"</td><td>"+objWeave.getWarpYarn()[i].getIntYarnCount()+"</td><td>"+objWeave.getWarpYarn()[i].getStrYarnCountUnit()+"</td><td>"+objWeave.getWarpYarn()[i].getIntYarnPly()+"</td><td>"+objWeave.getWarpYarn()[i].getIntYarnDFactor()+"</td><td>"+String.format("%.3f",objWeave.getWarpYarn()[i].getDblYarnDiameter())+"</td><td>-</td><td>"+objWeave.getWarpYarn()[i].getDblYarnPrice()+"</td><td>"+objWeave.getWarpYarn()[i].getIntYarnTwist()+"</td><td>"+objWeave.getWarpYarn()[i].getStrYarnTModel()+"</td><td>"+objWeave.getWarpYarn()[i].getIntYarnHairness()+"</td><td>"+objWeave.getWarpYarn()[i].getIntYarnHProbability()+"</td><th bgcolor=\""+objWeave.getWarpYarn()[i].getStrYarnColor()+"\">"+objWeave.getWarpYarn()[i].getStrYarnColor()+"</th></tr>");
                        }
                    }
                }
                bw.newLine();
                bw.write("</table>");
            }
            
            if(objWeave.getWeftYarn().length>0){
                PatternAction objPatternAction = new PatternAction();
                bw.newLine();
                bw.write("<table border=1 cellspacing=0>");
                bw.newLine();
                // Adding Weft Pattern
                bw.write("<caption>"+objDictionaryAction.getWord("WEFT")+" "+objDictionaryAction.getWord("YARNPATTERN")+": \t"+getReverseWeftPattern(objPatternAction.getPattern(objWeave.getObjConfiguration().getStrWeftPatternID()).getStrPattern())+"</caption>");
                bw.newLine();
                bw.write("<tr align=right><th>"+objDictionaryAction.getWord("WEFT")+"</th><th>"+objDictionaryAction.getWord("NAME")+"</th><th>"+objDictionaryAction.getWord("YARNTYPE")+"</th><th>"+objDictionaryAction.getWord("YARNREPEAT")+"</th><th>"+objDictionaryAction.getWord("YARNCOUNT")+"</th><th>"+objDictionaryAction.getWord("YARNUNIT")+"</th><th>"+objDictionaryAction.getWord("YARNPLY")+"</th><th>"+objDictionaryAction.getWord("YARNFACTOR")+"</th><th>"+objDictionaryAction.getWord("YARNDIAMETER")+"(mm)</th><th>"+objDictionaryAction.getWord("YARNWEIGHT")+"(kg)</th><th>"+objDictionaryAction.getWord("YARNPRICE")+"(kg)</th><th>"+objDictionaryAction.getWord("YARNTWIST")+"/ inch</th><th>"+objDictionaryAction.getWord("YARNSENCE")+"</th><th>"+objDictionaryAction.getWord("YARNHAIRNESS")+"</th><th>"+objDictionaryAction.getWord("YARNDISTRIBUTION")+"</th><th>"+objDictionaryAction.getWord("YARNCOLOR")+"</th></tr>");
                lstEntry = new ArrayList();
                for(int i=objWeave.getWeftYarn().length-1; i>=0; i--){
                    if(lstEntry.size()==0){
                        lstEntry.add(objWeave.getWeftYarn()[i].getStrYarnColor());
                        bw.newLine();
                        bw.write("<tr align=right><td>"+objWeave.getWeftYarn()[i].getStrYarnSymbol()+"</td><td>"+objWeave.getWeftYarn()[i].getStrYarnName()+"</td><td>"+objWeave.getWeftYarn()[i].getStrYarnType()+"</td><td>"+objWeave.getWeftYarn()[i].getIntYarnRepeat()+"</td><td>"+objWeave.getWeftYarn()[i].getIntYarnCount()+"</td><td>"+objWeave.getWeftYarn()[i].getStrYarnCountUnit()+"</td><td>"+objWeave.getWeftYarn()[i].getIntYarnPly()+"</td><td>"+objWeave.getWeftYarn()[i].getIntYarnDFactor()+"</td><td>"+String.format("%.3f",objWeave.getWeftYarn()[i].getDblYarnDiameter())+"</td><td>-</td><td>"+objWeave.getWeftYarn()[i].getDblYarnPrice()+"</td><td>"+objWeave.getWeftYarn()[i].getIntYarnTwist()+"</td><td>"+objWeave.getWeftYarn()[i].getStrYarnTModel()+"</td><td>"+objWeave.getWeftYarn()[i].getIntYarnHairness()+"</td><td>"+objWeave.getWeftYarn()[i].getIntYarnHProbability()+"</td><th bgcolor=\""+objWeave.getWeftYarn()[i].getStrYarnColor()+"\">"+objWeave.getWeftYarn()[i].getStrYarnColor()+"</th></tr>");
                    }
                    //check for redudancy
                    else {                
                        if(!lstEntry.contains(objWeave.getWeftYarn()[i].getStrYarnColor())){
                            lstEntry.add(objWeave.getWeftYarn()[i].getStrYarnColor());
                            bw.newLine();
                            bw.write("<tr align=right><td>"+objWeave.getWeftYarn()[i].getStrYarnSymbol()+"</td><td>"+objWeave.getWeftYarn()[i].getStrYarnName()+"</td><td>"+objWeave.getWeftYarn()[i].getStrYarnType()+"</td><td>"+objWeave.getWeftYarn()[i].getIntYarnRepeat()+"</td><td>"+objWeave.getWeftYarn()[i].getIntYarnCount()+"</td><td>"+objWeave.getWeftYarn()[i].getStrYarnCountUnit()+"</td><td>"+objWeave.getWeftYarn()[i].getIntYarnPly()+"</td><td>"+objWeave.getWeftYarn()[i].getIntYarnDFactor()+"</td><td>"+String.format("%.3f",objWeave.getWeftYarn()[i].getDblYarnDiameter())+"</td><td>-</td><td>"+objWeave.getWeftYarn()[i].getDblYarnPrice()+"</td><td>"+objWeave.getWeftYarn()[i].getIntYarnTwist()+"</td><td>"+objWeave.getWeftYarn()[i].getStrYarnTModel()+"</td><td>"+objWeave.getWeftYarn()[i].getIntYarnHairness()+"</td><td>"+objWeave.getWeftYarn()[i].getIntYarnHProbability()+"</td><th bgcolor=\""+objWeave.getWeftYarn()[i].getStrYarnColor()+"\">"+objWeave.getWeftYarn()[i].getStrYarnColor()+"</th></tr>");
                        }
                    }
                }
                bw.newLine();
                bw.write("</table>");
            }
            //bw.newLine();
            //bw.write("<br><b>"+objDictionaryAction.getWord("GRAPHVIEW")+"<b><br>");
            bw.newLine();
            bw.newLine();
            bw.write(IMAGE_TEXT);
            bw.newLine();
            bw.write("<br>"+INITIAL_TEXT+"<br>");
            bw.newLine();
            } catch (IOException ex) {
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            } catch (Exception ex) {
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
            
            if(objConfiguration.getBlnAuthenticateService()){
                ArrayList<File> filesToZip=new ArrayList<>();
                filesToZip.add(designFile);
                filesToZip.add(shaftFile);
                filesToZip.add(treadleFile);
                filesToZip.add(tieupFile);
                filesToZip.add(pegliftFile);
                filesToZip.add(htmlFile);
                String zipFilePath=path+".zip";
                String passwordToZip = ifile.getName()+"Weave_"+currentDate;
                new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                designFile.delete();
                shaftFile.delete();
                treadleFile.delete();
                tieupFile.delete();
                pegliftFile.delete();
                htmlFile.delete();
            }
            
            lblStatus.setText("HTML "+objDictionaryAction.getWord("EXPORTEDTO")+" "+path+".html");
        } catch (IOException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void printMenuAction(){
        new MessageView(objConfiguration);
        if(objConfiguration.getServicePasswordValid()){
            objConfiguration.setServicePasswordValid(false);
            lblStatus.setText(objDictionaryAction.getWord("ACTIONPRINTFILE"));
            printAction();
        }
    }    
    private void printAction(){
        BufferedImage texture = new BufferedImage(objWeave.getIntWarp(), objWeave.getIntWeft(), BufferedImage.TYPE_INT_RGB);        
        for(int x = 0; x < objWeave.getIntWeft(); x++) {
            for(int y = 0; y < objWeave.getIntWarp(); y++) {
                if(objWeave.getDesignMatrix()[x][y]==0)
                    texture.setRGB(y, x, java.awt.Color.WHITE.getRGB());
                else
                    texture.setRGB(y, x, java.awt.Color.BLACK.getRGB());
            }
        }
        try{
            int widthDesign=objWeave.getIntWeft()*boxSize;
            int widthCompositeView=objWeave.getIntWeft()*boxSize;
            if((objWeave.getIntWeft()*boxSize)<(10*5))
                widthDesign=(10*5);
            if((objWeave.getIntWeft()*boxSize)<(10*14))
                widthCompositeView=(10*14);
            // 10 is added for providing space between design and composite view
            int totalWidth=widthDesign+widthCompositeView+10;
            BufferedImage bufferedImageesize = new BufferedImage(totalWidth, (int)(objWeave.getIntWarp()*boxSize)+(10*2),BufferedImage.TYPE_INT_RGB);
            // set white background
            Graphics2D g = bufferedImageesize.createGraphics();
            g.setColor(java.awt.Color.WHITE);
            g.fillRect(0, 0, bufferedImageesize.getWidth(), bufferedImageesize.getHeight());

            BasicStroke bs = new BasicStroke(1);
            g.setStroke(bs);
            g.setColor(java.awt.Color.BLACK);
            g.drawImage(texture, 0, 0, (int)(objWeave.getIntWeft()*boxSize), (int)(objWeave.getIntWarp()*boxSize), null);

            texture = new BufferedImage( (int)(objWeave.getIntWeft()*boxSize), (int)(objWeave.getIntWarp()*boxSize), BufferedImage.TYPE_INT_RGB);
            WeaveAction objWeaveAction = new WeaveAction();
            texture = objWeaveAction.plotFrontSideView(objWeave, objWeave.getIntWarp(), objWeave.getIntWeft(),  (int)(objWeave.getIntWeft()*boxSize), (int)(objWeave.getIntWarp()*boxSize));
            g.drawImage(texture, widthDesign+10, 0, (int)(objWeave.getIntWeft()*boxSize), (int)(objWeave.getIntWarp()*boxSize), null);

            g.drawString(objDictionaryAction.getWord("DESIGN"), widthDesign+10, (int)(objWeave.getIntWarp()*boxSize)+10);
            // added for grid lines
            for(int r=0; r<objWeave.getIntWeft(); r++)
                g.drawLine(0, r*boxSize, (int)(objWeave.getIntWeft()*boxSize)-1, r*boxSize);
            g.drawLine(0, (int)(objWeave.getIntWarp()*boxSize)-1, (int)(objWeave.getIntWeft()*boxSize)-1, (int)(objWeave.getIntWarp()*boxSize)-1);
            for(int c=0; c<objWeave.getIntWarp(); c++)
                g.drawLine(c*boxSize, 0, c*boxSize, (int)(objWeave.getIntWarp()*boxSize)-1);
            g.drawString(objDictionaryAction.getWord("WEAVE"), 0, objWeave.getIntWeft()*boxSize+10);
            g.drawLine((int)(objWeave.getIntWeft()*boxSize)-1, 0, (int)(objWeave.getIntWeft()*boxSize)-1, (int)(objWeave.getIntWarp()*boxSize)-1);
            g.dispose();
            texture = null;
            new PrintView(bufferedImageesize, 0, objWeave.getStrWeaveName(), objWeave.getObjConfiguration().getObjUser().getStrName());
            lblStatus.setText("");
        } catch(Exception ex){
            new Logging("SEVERE",getClass().getName(),"printAction() : Print Error",ex);
        }
    }
    
/**
 * populateEditToolbar
 * <p>
 * Function use for editing tool bar for menu item Edit,
 * and binding events for each tools. 
 * 
 * @exception   (@throws SQLException)
 * @author      Amit Kumar Singh
 * @version     %I%, %G%
 * @since       1.0
 * @see         javafx.event.*;
 * @link        WeaveView
 */
    private void populateEditToolbarMenu(){
        //For drop down menus
        menuSelected.setTooltip(new Tooltip(objDictionaryAction.getWord("EDIT")));
                
        MenuItem undoEditMI = new MenuItem(objDictionaryAction.getWord("UNDO"));
        MenuItem redoEditMI = new MenuItem(objDictionaryAction.getWord("REDO"));
        MenuItem insertWarpEditMI = new MenuItem(objDictionaryAction.getWord("INSERTWARP"));
        MenuItem deleteWarpEditMI = new MenuItem(objDictionaryAction.getWord("DELETEWARP"));
        MenuItem insertWeftEditMI = new MenuItem(objDictionaryAction.getWord("INSERTWEFT"));
        MenuItem deleteWeftEditMI = new MenuItem(objDictionaryAction.getWord("DELETEWEFT"));
        MenuItem infoUtilityMI = new MenuItem(objDictionaryAction.getWord("RESIZESCALE"));
        MenuItem selectEditMI = new MenuItem(objDictionaryAction.getWord("SELECT"));
        MenuItem cropEditMI = new MenuItem(objDictionaryAction.getWord("CROP"));
        MenuItem copyEditMI = new MenuItem(objDictionaryAction.getWord("COPY"));
        MenuItem cutEditMI = new MenuItem(objDictionaryAction.getWord("CUT"));
        MenuItem pasteEditMI = new MenuItem(objDictionaryAction.getWord("PASTE"));
        MenuItem mirrorVerticalEditMI = new MenuItem(objDictionaryAction.getWord("VERTICALMIRROR"));
        MenuItem mirrorHorizontalEditMI = new MenuItem(objDictionaryAction.getWord("HORIZENTALMIRROR"));
        MenuItem jacquardConversionEditMI = new MenuItem(objDictionaryAction.getWord("JACQUARDCONVERSIONEDIT"));
        MenuItem complexWeaveEditMI = new MenuItem(objDictionaryAction.getWord("COMPLEXWEAVEEDIT"));
        MenuItem decomposeEditMI = new MenuItem(objDictionaryAction.getWord("DOUBLECLOTH"));
        MenuItem repeatSelectedEditMI = new MenuItem(objDictionaryAction.getWord("REPEATSELECTED"));
        MenuItem yarnEditMI = new MenuItem(objDictionaryAction.getWord("YARNEDIT"));
        MenuItem threadPatternEditMI = new MenuItem(objDictionaryAction.getWord("THREADPATTERNEDIT"));
        MenuItem switchColorEditMI = new MenuItem(objDictionaryAction.getWord("SWITCHCOLOREDIT"));
        MenuItem repeatEditMI = new MenuItem(objDictionaryAction.getWord("REPEATORIENTATION"));
        MenuItem sprayToolEditMI = new MenuItem(objDictionaryAction.getWord("SPRAYTOOL"));
        MenuItem transformOperationEditMI = new MenuItem(objDictionaryAction.getWord("TRANSFORMOPERATIONEDIT"));
        
        undoEditMI.setAccelerator(new KeyCodeCombination(KeyCode.Z, KeyCombination.CONTROL_DOWN));
        redoEditMI.setAccelerator(new KeyCodeCombination(KeyCode.Y, KeyCombination.CONTROL_DOWN));
        insertWarpEditMI.setAccelerator(new KeyCodeCombination(KeyCode.INSERT, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        deleteWarpEditMI.setAccelerator(new KeyCodeCombination(KeyCode.DELETE, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        insertWeftEditMI.setAccelerator(new KeyCodeCombination(KeyCode.INSERT, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        deleteWeftEditMI.setAccelerator(new KeyCodeCombination(KeyCode.DELETE, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        infoUtilityMI.setAccelerator(new KeyCodeCombination(KeyCode.I, KeyCombination.SHIFT_DOWN));
        selectEditMI.setAccelerator(new KeyCodeCombination(KeyCode.A, KeyCombination.CONTROL_DOWN));
        cropEditMI.setAccelerator(new KeyCodeCombination(KeyCode.W, KeyCombination.CONTROL_DOWN));
        copyEditMI.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_DOWN));
        cutEditMI.setAccelerator(new KeyCodeCombination(KeyCode.X, KeyCombination.CONTROL_DOWN));
        pasteEditMI.setAccelerator(new KeyCodeCombination(KeyCode.V, KeyCombination.CONTROL_DOWN));
        mirrorVerticalEditMI.setAccelerator(new KeyCodeCombination(KeyCode.V, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        mirrorHorizontalEditMI.setAccelerator(new KeyCodeCombination(KeyCode.H, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        jacquardConversionEditMI.setAccelerator(new KeyCodeCombination(KeyCode.J, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        complexWeaveEditMI.setAccelerator(new KeyCodeCombination(KeyCode.J, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        decomposeEditMI.setAccelerator(new KeyCodeCombination(KeyCode.D, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        repeatSelectedEditMI.setAccelerator(new KeyCodeCombination(KeyCode.R, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        yarnEditMI.setAccelerator(new KeyCodeCombination(KeyCode.Y, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        threadPatternEditMI.setAccelerator(new KeyCodeCombination(KeyCode.T, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        switchColorEditMI.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        repeatEditMI.setAccelerator(new KeyCodeCombination(KeyCode.R, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        sprayToolEditMI.setAccelerator(new KeyCodeCombination(KeyCode.Q, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        transformOperationEditMI.setAccelerator(new KeyCodeCombination(KeyCode.W, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
                
        undoEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/undo.png"));
        redoEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/redo.png"));
        insertWarpEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/insert_warp.png"));
        deleteWarpEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/delete_warp.png"));
        insertWeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/insert_weft.png"));
        deleteWeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/delete_weft.png"));
        infoUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move.png"));
        selectEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/select.png"));
        cropEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/crop.png"));        
        copyEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/copy.png"));        
        cutEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cut.png"));        
        pasteEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/paste.png"));
        mirrorVerticalEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
        mirrorHorizontalEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
        jacquardConversionEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/assign_artwork_weave.png"));
        complexWeaveEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/duplicate_layer.png"));
        decomposeEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/double_cloth.png"));
        repeatSelectedEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tiled_view.png"));
        yarnEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/yarn.png"));
        threadPatternEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/thread_pattern.png"));
        switchColorEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/switch_color.png"));
        repeatEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/repeat.png"));
        sprayToolEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/spray_tool.png"));
        transformOperationEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/edit.png"));
        
        Menu yarnMenu = new Menu(objDictionaryAction.getWord("ADD")+"/"+objDictionaryAction.getWord("DELETE")+" "+objDictionaryAction.getWord("YARN"));
        yarnMenu.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/yarn_pattern.png"));
        yarnMenu.getItems().addAll(insertWarpEditMI, deleteWarpEditMI, insertWeftEditMI, deleteWeftEditMI);
        
        Menu mirrorMenu = new Menu(objDictionaryAction.getWord("MIRROR"));
        mirrorMenu.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/symmetry.png"));
        mirrorMenu.getItems().addAll(mirrorVerticalEditMI,mirrorHorizontalEditMI);
        
        //Add menu enable disable condition
        if(!isWorkingMode){
            undoEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/undo.png"));
            undoEditMI.setDisable(true);
            redoEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/redo.png"));
            redoEditMI.setDisable(true);
            insertWarpEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/insert_warp.png"));
            insertWarpEditMI.setDisable(true);
            deleteWarpEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/delete_warp.png"));
            deleteWarpEditMI.setDisable(true);
            insertWeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/insert_weft.png"));
            insertWeftEditMI.setDisable(true);
            deleteWeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/delete_weft.png"));
            deleteWeftEditMI.setDisable(true);
            infoUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move.png"));
            infoUtilityMI.setDisable(true);
            selectEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/select.png"));
            selectEditMI.setDisable(true);
            cropEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/crop.png"));
            cropEditMI.setDisable(true);
            copyEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/copy.png"));
            copyEditMI.setDisable(true);
            cutEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/cut.png"));
            cutEditMI.setDisable(true);
            pasteEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/paste.png"));
            pasteEditMI.setDisable(true);
            mirrorVerticalEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
            mirrorVerticalEditMI.setDisable(true);
            mirrorHorizontalEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
            mirrorHorizontalEditMI.setDisable(true);
            jacquardConversionEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/assign_artwork_weave.png"));
            jacquardConversionEditMI.setDisable(true);
            complexWeaveEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/duplicate_layer.png"));
            complexWeaveEditMI.setDisable(true);
            decomposeEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/double_cloth.png"));
            decomposeEditMI.setDisable(true);
            repeatSelectedEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/tiled_view.png"));
            repeatSelectedEditMI.setDisable(true);
            yarnEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/yarn.png"));
            yarnEditMI.setDisable(true);
            threadPatternEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/thread_pattern.png"));
            threadPatternEditMI.setDisable(true);
            switchColorEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/switch_color.png"));
            switchColorEditMI.setDisable(true);
            //repeatEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/repeat.png"));
            //repeatEditMI.setDisable(true);
            sprayToolEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/spray_tool.png"));
            sprayToolEditMI.setDisable(true);
            transformOperationEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/edit.png"));
            transformOperationEditMI.setDisable(true);       
        }else{
            undoEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/undo.png"));
            undoEditMI.setDisable(false);
            redoEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/redo.png"));
            redoEditMI.setDisable(false);
            insertWarpEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/insert_warp.png"));
            insertWarpEditMI.setDisable(false);
            deleteWarpEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/delete_warp.png"));
            deleteWarpEditMI.setDisable(false);
            insertWeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/insert_weft.png"));
            insertWeftEditMI.setDisable(false);
            deleteWeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/delete_weft.png"));
            deleteWeftEditMI.setDisable(false);
            infoUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move.png"));
            infoUtilityMI.setDisable(false);
            selectEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/select.png"));
            selectEditMI.setDisable(false);
            cropEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/crop.png"));
            cropEditMI.setDisable(false);
            copyEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/copy.png"));
            copyEditMI.setDisable(false);
            cutEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cut.png"));
            cutEditMI.setDisable(false);
            pasteEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/paste.png"));
            pasteEditMI.setDisable(false);
            mirrorVerticalEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
            mirrorVerticalEditMI.setDisable(false);
            mirrorHorizontalEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
            mirrorHorizontalEditMI.setDisable(false);
            jacquardConversionEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/assign_artwork_weave.png"));
            jacquardConversionEditMI.setDisable(false); 
            complexWeaveEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/duplicate_layer.png"));
            complexWeaveEditMI.setDisable(false);
            decomposeEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/double_cloth.png"));
            decomposeEditMI.setDisable(false);
            repeatSelectedEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tiled_view.png"));
            repeatSelectedEditMI.setDisable(false);
            repeatEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/repeat.png"));
            repeatEditMI.setDisable(false);
            yarnEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/yarn.png"));
            yarnEditMI.setDisable(false);
            threadPatternEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/thread_pattern.png"));
            threadPatternEditMI.setDisable(false);
            switchColorEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/switch_color.png"));
            switchColorEditMI.setDisable(false);
            //repeatEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/repeat.png"));
            //repeatEditMI.setDisable(false);
            sprayToolEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/spray_tool.png"));
            sprayToolEditMI.setDisable(false);
            transformOperationEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/edit.png"));
            transformOperationEditMI.setDisable(false);
            if(deleteWeftEditMI!=null&&objWeave.getIntWeft()<3){
                deleteWeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/delete_weft.png"));
                deleteWeftEditMI.setDisable(true);
            }
            if(deleteWarpEditMI!=null&&objWeave.getIntWarp()<3){
                deleteWarpEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/delete_warp.png"));
                deleteWarpEditMI.setDisable(true);
            } 
        }
            
        //Add the action to Buttons.
        undoEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                undoAction();                
            }
        });       
        redoEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                redoAction();                
            }
        });
        insertWarpEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                insertWarpAction();
            }
        });  
        deleteWarpEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                deleteWarpAction();
            }
        });  
        insertWeftEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                insertWeftAction();
            }
        });  
        deleteWeftEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                deleteWeftAction();
            }
        });
        infoUtilityMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                populateProperties();
            }
        });
        selectEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                selectAction();
            }
        });
        cropEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                cropAction();
            }
        });
        copyEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                copyAction();
            }
        });
        cutEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                cutAction();
            }
        });
        pasteEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                pasteAction();
            }
        });
        mirrorVerticalEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                mirrorVerticalSelectedAction();
            }
        });  
        mirrorHorizontalEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                mirrorHorizontalSelectedAction();
            }
        });
        jacquardConversionEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                jacquardConverstionAction();
            }
        });
        complexWeaveEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                complexWeaveAction();
            }
        });
        decomposeEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                editDecomposeOperationAction();
            }
        });
        repeatSelectedEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                repeatSelectionInActiveArea();
            }
        });        
        yarnEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                yarnPropertiesAction();
            }
        }); 
        threadPatternEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {              
                threadSequenceAction();
            }
        });   
        switchColorEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {              
                switchColorAction();
            }
        });
        repeatEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                repeatOrientationAction();
            }
        });        
        sprayToolEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                sprayToolAction();
            }
        });  
        transformOperationEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                transformOperationAction();
            }
        });        
        menuSelected.getItems().addAll(undoEditMI, redoEditMI, yarnMenu, new SeparatorMenuItem(), infoUtilityMI, selectEditMI, cropEditMI, copyEditMI, cutEditMI, pasteEditMI, mirrorMenu, jacquardConversionEditMI, complexWeaveEditMI, decomposeEditMI, repeatSelectedEditMI, yarnEditMI, threadPatternEditMI, switchColorEditMI, repeatEditMI, sprayToolEditMI, transformOperationEditMI);
    }
    private void populateEditToolbar(){
        // undo edit item
        Button undoEditBtn = new Button(); 
        undoEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/undo.png"));
        undoEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("UNDO")+" (Ctrl+Z)\n"+objDictionaryAction.getWord("TOOLTIPUNDO")));
        undoEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        undoEditBtn.getStyleClass().addAll("toolbar-button");   
        // redo edit item 
        Button redoEditBtn = new Button(); 
        redoEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/redo.png"));
        redoEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("REDO")+" (Ctrl+Y)\n"+objDictionaryAction.getWord("TOOLTIPREDO")));
        redoEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        redoEditBtn.getStyleClass().addAll("toolbar-button");
        // Insert warp 
        Button insertWarpEditBtn = new Button(); 
        insertWarpEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/insert_warp.png"));
        insertWarpEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("INSERTWARP")+" (Ctrl+Shift+Insert)\n"+objDictionaryAction.getWord("TOOLTIPINSERTWARP")));
        insertWarpEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        insertWarpEditBtn.getStyleClass().addAll("toolbar-button"); 
        // Delete Warp 
        deleteWarpEditBtn = new Button(); 
        deleteWarpEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/delete_warp.png"));
        deleteWarpEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("DELETEWARP")+" (Ctrl+Shift+Delete)\n"+objDictionaryAction.getWord("TOOLTIPDELETEWARP")));
        deleteWarpEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        deleteWarpEditBtn.getStyleClass().addAll("toolbar-button"); 
        // Insert weft 
        Button insertWeftEditBtn = new Button(); 
        insertWeftEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/insert_weft.png"));
        insertWeftEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("INSERTWEFT")+" (Alt+Shift+Insert)\n"+objDictionaryAction.getWord("TOOLTIPINSERTWEFT")));
        insertWeftEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        insertWeftEditBtn.getStyleClass().addAll("toolbar-button");   
        // Delete Weft 
        deleteWeftEditBtn = new Button(); 
        deleteWeftEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/delete_weft.png"));
        deleteWeftEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("DELETEWEFT")+" (Alt+Shift+Delete)\n"+objDictionaryAction.getWord("TOOLTIPDELETEWEFT")));
        deleteWeftEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        deleteWeftEditBtn.getStyleClass().addAll("toolbar-button");
        // settings
        Button infoUtilityBtn = new Button();
        infoUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move.png"));
        infoUtilityBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("RESIZESCALE")+" (Shift+I)\n"+objDictionaryAction.getWord("TOOLTIPRESIZESCALE")));
        infoUtilityBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        infoUtilityBtn.getStyleClass().add("toolbar-button"); 
        // select edit item
        selectEditBtn = new Button(); 
        selectEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/select.png"));
        selectEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SELECT")+" (Ctrl+A)\n"+objDictionaryAction.getWord("TOOLTIPSELECT")));
        selectEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        selectEditBtn.getStyleClass().addAll("toolbar-button");
        // crop item
        Button cropEditBtn = new Button(); 
        cropEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/crop.png"));
        cropEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CROP")+" (Ctrl+W)\n"+objDictionaryAction.getWord("TOOLTIPCROP")));
        cropEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        cropEditBtn.getStyleClass().addAll("toolbar-button");    
        // Copy item
        Button copyEditBtn = new Button(); 
        copyEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/copy.png"));
        copyEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("COPY")+" (Ctrl+C)\n"+objDictionaryAction.getWord("TOOLTIPCOPY")));
        copyEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        copyEditBtn.getStyleClass().addAll("toolbar-button");    
        // cut item
        Button cutEditBtn = new Button(); 
        cutEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cut.png"));
        cutEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CUT")+" (Ctrl+X)\n"+objDictionaryAction.getWord("TOOLTIPCUT")));
        cutEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        cutEditBtn.getStyleClass().addAll("toolbar-button");    
        // Paste item
        Button pasteEditBtn = new Button(); 
        pasteEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/paste.png"));
        pasteEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("PASTE")+" (Ctrl+V)\n"+objDictionaryAction.getWord("TOOLTIPPASTE")));
        pasteEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        pasteEditBtn.getStyleClass().addAll("toolbar-button"); 
        // mirror edit item
        Button mirrorVerticalEditBtn = new Button(); 
        mirrorVerticalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
        mirrorVerticalEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("VERTICALMIRROR")+" (Alt+Shift+V)\n"+objDictionaryAction.getWord("TOOLTIPVERTICALMIRROR")));
        mirrorVerticalEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        mirrorVerticalEditBtn.getStyleClass().addAll("toolbar-button");   
        // mirror edit item
        Button mirrorHorizontalEditBtn = new Button(); 
        mirrorHorizontalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
        mirrorHorizontalEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("HORIZENTALMIRROR")+" (Alt+Shift+H)\n"+objDictionaryAction.getWord("TOOLTIPHORIZENTALMIRROR")));
        mirrorHorizontalEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        mirrorHorizontalEditBtn.getStyleClass().addAll("toolbar-button");   
        // Pattern edit item
        Button jacquardConversionEditBtn = new Button();
        jacquardConversionEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/assign_artwork_weave.png"));
        jacquardConversionEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("JACQUARDCONVERSIONEDIT")+" (Ctrl+Shift+J)\n"+objDictionaryAction.getWord("TOOLTIPJACQUARDCONVERSIONEDIT")));
        jacquardConversionEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        jacquardConversionEditBtn.getStyleClass().add("toolbar-button");    
        // Pattern edit item
        Button complexWeaveEditBtn = new Button();
        complexWeaveEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/duplicate_layer.png"));
        complexWeaveEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("COMPLEXWEAVEEDIT")+" (Alt+Shift+J)\n"+objDictionaryAction.getWord("TOOLTIPCOMPLEXWEAVEEDIT")));
        complexWeaveEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        complexWeaveEditBtn.getStyleClass().add("toolbar-button");
        // edit decompose item (double cloth)
        Button decomposeEditBtn = new Button();
        decomposeEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/double_cloth.png"));
        decomposeEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("DOUBLECLOTH")+" (Alt+Shift+D)\n"+objDictionaryAction.getWord("TOOLTIPDOUBLECLOTH")));
        decomposeEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        decomposeEditBtn.getStyleClass().add("toolbar-button");
        // graph edit item
        Button repeatSelectedEditBtn = new Button();
        repeatSelectedEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tiled_view.png"));
        repeatSelectedEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("REPEATSELECTED")+" (Ctrl+Alt+R)\n"+objDictionaryAction.getWord("TOOLTIPREPEATSELECTED")));
        repeatSelectedEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        repeatSelectedEditBtn.getStyleClass().add("toolbar-button");    
        // yarn Calculation item
        Button yarnEditBtn = new Button();
        yarnEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/yarn.png"));
        yarnEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("YARNEDIT")+" (Ctrl+Shift+Y)\n"+objDictionaryAction.getWord("TOOLTIPYARNEDIT")));
        yarnEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        yarnEditBtn.getStyleClass().add("toolbar-button");    
        // Pattern edit item
        Button threadPatternEditBtn = new Button();
        threadPatternEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/thread_pattern.png"));
        threadPatternEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("THREADPATTERNEDIT")+" (Ctrl+Shift+T)\n"+objDictionaryAction.getWord("TOOLTIPTHREADPATTERNEDIT")));
        threadPatternEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        threadPatternEditBtn.getStyleClass().add("toolbar-button");    
        // switch color edit item
        Button switchColorEditBtn = new Button();
        switchColorEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/switch_color.png"));
        switchColorEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SWITCHCOLOREDIT")+" (Ctrl+Shift+C)\n"+objDictionaryAction.getWord("TOOLTIPSWITCHCOLOREDIT")));
        switchColorEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        switchColorEditBtn.getStyleClass().add("toolbar-button");    
        // repaet
        Button repeatEditBtn = new Button(); 
        repeatEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/repeat.png"));
        repeatEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("REPEATORIENTATION")+" (Alt+Shift+R)\n"+objDictionaryAction.getWord("TOOLTIPREPEATORIENTATION")));
        repeatEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        repeatEditBtn.getStyleClass().addAll("toolbar-button"); 
        // spray tool edit item
        Button sprayToolEditBtn= new Button(); 
        sprayToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/spray_tool.png"));
        sprayToolEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SPRAYTOOL")+" (Alt+Shift+Q)\n"+objDictionaryAction.getWord("TOOLTIPSPRAYTOOL")));
        sprayToolEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        sprayToolEditBtn.getStyleClass().addAll("toolbar-button");                 
        // transform Operation edit item
        Button transformOperationEditBtn = new Button();
        transformOperationEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/edit.png"));
        transformOperationEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("TRANSFORMOPERATIONEDIT")+" (Ctrl+Shift+W)\n"+objDictionaryAction.getWord("TOOLTIPTRANSFORMOPERATIONEDIT")));
        transformOperationEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        transformOperationEditBtn.getStyleClass().add("toolbar-button");         
        //Add menu enable disable condition
        if(!isWorkingMode){
            undoEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/undo.png"));
            undoEditBtn.setDisable(true);
            undoEditBtn.setCursor(Cursor.WAIT);
            redoEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/redo.png"));
            redoEditBtn.setDisable(true);
            redoEditBtn.setCursor(Cursor.WAIT);
            insertWarpEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/insert_warp.png"));
            insertWarpEditBtn.setDisable(true);
            insertWarpEditBtn.setCursor(Cursor.WAIT);
            deleteWarpEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/delete_warp.png"));
            deleteWarpEditBtn.setDisable(true);
            deleteWarpEditBtn.setCursor(Cursor.WAIT);
            insertWeftEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/insert_weft.png"));
            insertWeftEditBtn.setDisable(true);
            insertWeftEditBtn.setCursor(Cursor.WAIT);
            deleteWeftEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/delete_weft.png"));
            deleteWeftEditBtn.setDisable(true);
            deleteWeftEditBtn.setCursor(Cursor.WAIT);
            infoUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move.png"));
            infoUtilityBtn.setDisable(true);
            infoUtilityBtn.setCursor(Cursor.WAIT);
            selectEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/select.png"));
            selectEditBtn.setDisable(true);
            selectEditBtn.setCursor(Cursor.WAIT);
            cropEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/crop.png"));
            cropEditBtn.setDisable(true);
            cropEditBtn.setCursor(Cursor.WAIT);
            copyEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/copy.png"));
            copyEditBtn.setDisable(true);
            copyEditBtn.setCursor(Cursor.WAIT);
            cutEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/cut.png"));
            cutEditBtn.setDisable(true);
            cutEditBtn.setCursor(Cursor.WAIT);
            pasteEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/paste.png"));
            pasteEditBtn.setDisable(true);
            pasteEditBtn.setCursor(Cursor.WAIT);
            mirrorVerticalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
            mirrorVerticalEditBtn.setDisable(true);
            mirrorVerticalEditBtn.setCursor(Cursor.WAIT);
            mirrorHorizontalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
            mirrorHorizontalEditBtn.setDisable(true);
            mirrorHorizontalEditBtn.setCursor(Cursor.WAIT);            
            jacquardConversionEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/assign_artwork_weave.png"));
            jacquardConversionEditBtn.setDisable(true);
            jacquardConversionEditBtn.setCursor(Cursor.WAIT);
            complexWeaveEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/duplicate_layer.png"));
            complexWeaveEditBtn.setDisable(true);
            complexWeaveEditBtn.setCursor(Cursor.WAIT);
            decomposeEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/double_cloth.png"));
            decomposeEditBtn.setDisable(true);
            decomposeEditBtn.setCursor(Cursor.WAIT);
            repeatSelectedEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/tiled_view.png"));
            repeatSelectedEditBtn.setDisable(true);
            repeatSelectedEditBtn.setCursor(Cursor.WAIT);
            yarnEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/yarn.png"));
            yarnEditBtn.setDisable(true);
            yarnEditBtn.setCursor(Cursor.WAIT);
            threadPatternEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/thread_pattern.png"));
            threadPatternEditBtn.setDisable(true);
            threadPatternEditBtn.setCursor(Cursor.WAIT);
            switchColorEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/switch_color.png"));
            switchColorEditBtn.setDisable(true);
            switchColorEditBtn.setCursor(Cursor.WAIT);
            //repeatEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/repeat.png"));
            //repeatEditBtn.setDisable(true);
            //repeatEditBtn.setCursor(Cursor.WAIT);
            sprayToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/spray_tool.png"));
            sprayToolEditBtn.setDisable(true);
            sprayToolEditBtn.setCursor(Cursor.WAIT);
            transformOperationEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/edit.png"));
            transformOperationEditBtn.setDisable(true);
            transformOperationEditBtn.setCursor(Cursor.WAIT);              
        }else{
            undoEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/undo.png"));
            undoEditBtn.setDisable(false);
            undoEditBtn.setCursor(Cursor.HAND);
            redoEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/redo.png"));
            redoEditBtn.setDisable(false);
            redoEditBtn.setCursor(Cursor.HAND);
            insertWarpEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/insert_warp.png"));
            insertWarpEditBtn.setDisable(false);
            insertWarpEditBtn.setCursor(Cursor.HAND);
            deleteWarpEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/delete_warp.png"));
            deleteWarpEditBtn.setDisable(false);
            deleteWarpEditBtn.setCursor(Cursor.HAND);
            insertWeftEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/insert_weft.png"));
            insertWeftEditBtn.setDisable(false);
            insertWeftEditBtn.setCursor(Cursor.HAND);
            deleteWeftEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/delete_weft.png"));
            deleteWeftEditBtn.setDisable(false);
            deleteWeftEditBtn.setCursor(Cursor.HAND);
            infoUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move.png"));
            infoUtilityBtn.setDisable(false);
            infoUtilityBtn.setCursor(Cursor.HAND);
            selectEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/select.png"));
            selectEditBtn.setDisable(false);
            selectEditBtn.setCursor(Cursor.HAND);
            cropEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/crop.png"));
            cropEditBtn.setDisable(false);
            cropEditBtn.setCursor(Cursor.HAND);
            copyEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/copy.png"));
            copyEditBtn.setDisable(false);
            copyEditBtn.setCursor(Cursor.HAND);
            cutEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cut.png"));
            cutEditBtn.setDisable(false);
            cutEditBtn.setCursor(Cursor.HAND);
            pasteEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/paste.png"));
            pasteEditBtn.setDisable(false);
            pasteEditBtn.setCursor(Cursor.HAND);
            mirrorVerticalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
            mirrorVerticalEditBtn.setDisable(false);
            mirrorVerticalEditBtn.setCursor(Cursor.HAND);
            mirrorHorizontalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
            mirrorHorizontalEditBtn.setDisable(false);
            mirrorHorizontalEditBtn.setCursor(Cursor.HAND);
            jacquardConversionEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/assign_artwork_weave.png"));
            jacquardConversionEditBtn.setDisable(false);
            jacquardConversionEditBtn.setCursor(Cursor.HAND);
            complexWeaveEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/duplicate_layer.png"));
            complexWeaveEditBtn.setDisable(false);
            complexWeaveEditBtn.setCursor(Cursor.HAND);
            decomposeEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/double_cloth.png"));
            decomposeEditBtn.setDisable(false);
            decomposeEditBtn.setCursor(Cursor.HAND);
            repeatSelectedEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tiled_view.png"));
            repeatSelectedEditBtn.setDisable(false);
            repeatSelectedEditBtn.setCursor(Cursor.HAND);
            yarnEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/yarn.png"));
            yarnEditBtn.setDisable(false);
            yarnEditBtn.setCursor(Cursor.HAND);
            threadPatternEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/thread_pattern.png"));
            threadPatternEditBtn.setDisable(false);
            threadPatternEditBtn.setCursor(Cursor.HAND);
            switchColorEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/switch_color.png"));
            switchColorEditBtn.setDisable(false);
            switchColorEditBtn.setCursor(Cursor.HAND);
            //repeatEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/repeat.png"));
            //repeatEditBtn.setDisable(false);
            //repeatEditBtn.setCursor(Cursor.HAND); 
            sprayToolEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/spray_tool.png"));
            sprayToolEditBtn.setDisable(false);
            sprayToolEditBtn.setCursor(Cursor.HAND); 
            transformOperationEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/edit.png"));
            transformOperationEditBtn.setDisable(false);
            transformOperationEditBtn.setCursor(Cursor.HAND);  
        }        
        if(isSelectionMode){
            scene.setCursor(Cursor.HAND);
            selectEditBtn.setStyle("-fx-border-width: 3;  -fx-border-color: black;");
        } else{
            scene.setCursor(Cursor.DEFAULT);
            selectEditBtn.setStyle("-fx-border-width: 0;  -fx-border-color: black;");
        }
       //Add the action to Buttons.
        undoEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                undoAction();                
            }
        });       
        redoEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                redoAction();                
            }
        });
        insertWarpEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                insertWarpAction();
            }
        });  
        deleteWarpEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                deleteWarpAction();
            }
        });  
        insertWeftEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                insertWeftAction();
            }
        });  
        deleteWeftEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                deleteWeftAction();
            }
        });
        infoUtilityBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                populateProperties();
            }
        });
        selectEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                selectAction();
            }
        });
        cropEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                cropAction();
            }
        });
        copyEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                copyAction();
            }
        });
        cutEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                cutAction();
            }
        });
        pasteEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                pasteAction();
            }
        }); 
        mirrorVerticalEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mirrorVerticalSelectedAction();
            }
        });  
        mirrorHorizontalEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mirrorHorizontalSelectedAction();
            }
        });         
        jacquardConversionEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                jacquardConverstionAction();
            }
        });
        complexWeaveEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                complexWeaveAction();
            }
        });
        decomposeEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                editDecomposeOperationAction();
            }
        });
        repeatSelectedEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                repeatSelectionInActiveArea();
            }
        });
        yarnEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                yarnPropertiesAction();
            }
        }); 
        threadPatternEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {                
                threadSequenceAction();
            }
        });   
        switchColorEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {                
                switchColorAction();
            }
        });
        repeatEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                repeatOrientationAction();
            }
        });
        sprayToolEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {                
                sprayToolAction();
            }
        });
        transformOperationEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                transformOperationAction();
            }
        });
        //Create some Buttons.        
        FlowPane flow = new FlowPane();        
        flow.setPrefWrapLength(objConfiguration.WIDTH*.95);
        flow.getStyleClass().addAll("flow");
        //Add the Buttons to the ToolBar.
        flow.getChildren().addAll(undoEditBtn,redoEditBtn,insertWarpEditBtn,deleteWarpEditBtn,insertWeftEditBtn,deleteWeftEditBtn,infoUtilityBtn,selectEditBtn,cropEditBtn,copyEditBtn,cutEditBtn,pasteEditBtn,mirrorVerticalEditBtn,mirrorHorizontalEditBtn,jacquardConversionEditBtn,complexWeaveEditBtn,decomposeEditBtn,repeatSelectedEditBtn,yarnEditBtn,threadPatternEditBtn,switchColorEditBtn,repeatEditBtn,sprayToolEditBtn,transformOperationEditBtn);
        flow.setAlignment(Pos.TOP_LEFT);
        toolBar.getItems().addAll(flow);
    }
    private void undoAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONUNDO"));
        try {
            if(objUR.canUndo()){
                objUR.undo();
                objWeave = (Weave)objUR.getObjData();
                Weave clonedWeave = cloneWeave();
                objWeave = clonedWeave;
                objWeave.setObjConfiguration(objConfiguration);
            
                populateNplot();
                populateDplot();
                disableWarpWeftDelete();                   
            
                lblStatus.setText(objUR.getStrTag()+" "+objDictionaryAction.getWord("UNDO")+":"+objDictionaryAction.getWord("SUCCESS"));
            }else{
                lblStatus.setText(objDictionaryAction.getWord("MAXUNDO"));
            }        
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Undo",ex);
            ex.printStackTrace();
        }
    }
    private void redoAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONREDO"));
        try {
            if(objUR.canRedo()){
                objUR.redo();
                objWeave = (Weave)objUR.getObjData();
                Weave clonedWeave = cloneWeave();
                objWeave = clonedWeave;
                objWeave.setObjConfiguration(objConfiguration);
            
                populateNplot();
                populateDplot();
                disableWarpWeftDelete();
                    
                lblStatus.setText(objUR.getStrTag()+" "+objDictionaryAction.getWord("REDO")+":"+objDictionaryAction.getWord("SUCCESS"));
            }else{
                lblStatus.setText(objDictionaryAction.getWord("MAXREDO"));
            }
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Operation Redo",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void editingModeSelection() {
        //System.err.println(objDictionaryAction.getWord("AUTOMATICEDIT")+"="+objDictionaryAction.getWord("EDITINGOPTIONS")+"="+objDictionaryAction.getWord("TOOLTIPEDITINGOPTIONS")));
        if(isDesignMode){
            designEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_lock.png"));
            isDesignMode = false;
        } else{
            designEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
            isDesignMode = true;
        }
    }
    private void disableWarpWeftDelete(){
        if(deleteWarpEditBtn!=null){
            if(objWeave.getIntWarp()<3){
                deleteWarpEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/delete_warp.png"));
                deleteWarpEditBtn.setDisable(true);
                deleteWarpEditBtn.setCursor(Cursor.WAIT);
                lblStatus.setText(objDictionaryAction.getWord("MAXDELETE"));
            } else {
                deleteWarpEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/delete_warp.png"));
                deleteWarpEditBtn.setDisable(false);
                deleteWarpEditBtn.setCursor(Cursor.HAND);
            }
        }
        if(deleteWeftEditBtn!=null){
            if(objWeave.getIntWeft()<3){
                deleteWeftEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/delete_weft.png"));
                deleteWeftEditBtn.setDisable(true);
                deleteWeftEditBtn.setCursor(Cursor.WAIT);
                lblStatus.setText(objDictionaryAction.getWord("MAXDELETE"));
            } else {
                deleteWeftEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/delete_weft.png"));
                deleteWeftEditBtn.setDisable(false);
                deleteWeftEditBtn.setCursor(Cursor.HAND);
            }
        }        
    }
    private void insertWarpAction(){
        try {
            if(activeGridCol==MAX_GRID_COL)
                return;
            lblStatus.setText(objDictionaryAction.getWord("ACTIONINSERTWARP"));
            objWeaveAction.insertWarp(objWeave, current_col,'W');
            drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
            populateNplot();
            populateDplot();
            disableWarpWeftDelete();
            Weave objClonedWeave=cloneWeave();
            objUR.doCommand("Insert Warp", objClonedWeave);
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"insert warp Right",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void deleteWarpAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONDELETEWARP"));
            if(objWeave.getIntWarp()!=current_col+1){
                objWeaveAction.deleteWarp(objWeave, current_col,'W');
                drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
                populateNplot();
                populateDplot();
                Weave objClonedWeave=cloneWeave();
                objUR.doCommand("Delete Warp", objClonedWeave);
            }else{
                lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
            }
            disableWarpWeftDelete();            
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"delete warp",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void insertWeftAction(){
        try {
            if(activeGridRow==MAX_GRID_ROW)
                return;
            lblStatus.setText(objDictionaryAction.getWord("ACTIONINSERTWEFT"));
            objWeaveAction.insertWeft(objWeave, current_row,'W');
            drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
            populateNplot();
            populateDplot();
            disableWarpWeftDelete(); 
            Weave objClonedWeave=cloneWeave();
            objUR.doCommand("Insert Weft", objClonedWeave);
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"insert weft",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void deleteWeftAction(){        
        try {
            if(objWeave.getIntWeft()!=current_row+1){
                lblStatus.setText(objDictionaryAction.getWord("ACTIONDELETEWEFT"));
                objWeaveAction.deleteWeft(objWeave, current_row,'W');
                drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
                populateNplot();
                populateDplot();
                Weave objClonedWeave=cloneWeave();
                objUR.doCommand("Delete Weft", objClonedWeave);
            }else{
                lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
            }
            disableWarpWeftDelete();             
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"delete weft",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void selectAction(){
        try {
             lblStatus.setText(objDictionaryAction.getWord("ACTIONSELECT"));
             isSelectionMode=!isSelectionMode;
             if(isSelectionMode){
                 scene.setCursor(Cursor.HAND);
                 if(selectEditBtn!=null)
                    selectEditBtn.setStyle("-fx-border-width: 3;  -fx-border-color: black;");
             }
             else{
                 scene.setCursor(Cursor.DEFAULT);
                 if(selectEditBtn!=null)
                    selectEditBtn.setStyle("-fx-border-width: 0;  -fx-border-color: black;");
                 initial_row = final_row;
                 initial_col = final_col;
             }
             /*if(isDragBox==0){
                 isDragBox=1;
                 scene.setCursor(Cursor.HAND);
             }else{
                 isDragBox=0;
                 scene.setCursor(Cursor.DEFAULT);
             }
             designGP.getChildren().clear();
             populateDplot();*/
         } catch (Exception ex) {
           new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
           lblStatus.setText(objDictionaryAction.getWord("ERROR"));
         }
    }
    private void cropAction(){
        try {
            if(initial_row==final_row && initial_col==final_col){
                lblStatus.setText(objDictionaryAction.getWord("NOREGION"));
                new MessageView("alert", "alert", objDictionaryAction.getWord("NOREGION"));                        
            } else{                   
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCOPY"));
                objWeaveAction.crop(objWeave,initial_row,initial_col,final_row,final_col);
                // as active design area of updated weave is changed
                activeGridCol = objWeave.getIntWarp();
                activeGridRow = objWeave.getIntWeft();            
                populateNplot();
                populateDplot();
                Weave objClonedWeave=cloneWeave();
                objUR.doCommand("Crop", objClonedWeave);
                //isSelectionMode=false;
                //scene.setCursor(Cursor.DEFAULT);
                //selectEditBtn.setStyle("-fx-border-width: 0;  -fx-border-color: black;");
            }
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Right",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void copyAction(){
        try {
            if(initial_row==final_row && initial_col==final_col){
                lblStatus.setText(objDictionaryAction.getWord("NOREGION"));
                new MessageView("alert", "alert", objDictionaryAction.getWord("NOREGION"));                        
            } else{                   
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCOPY"));
                objWeaveAction.copy(objWeave,initial_row,initial_col,final_row,final_col);
                //populateNplot();
                //populateDplot();
            }
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Right",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void cutAction(){
        try {
            if(initial_row==final_row && initial_col==final_col){
                lblStatus.setText(objDictionaryAction.getWord("NOREGION"));
                new MessageView("alert", "alert", objDictionaryAction.getWord("NOREGION"));                        
            } else{                   
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCOPY"));
                objWeaveAction.cut(objWeave,initial_row,initial_col,final_row,final_col);
                populateNplot();
                populateDplot();
                Weave objClonedWeave=cloneWeave();
                objUR.doCommand("cut Paste", objClonedWeave);
                //isSelectionMode=false;
                //scene.setCursor(Cursor.DEFAULT);
                //selectEditBtn.setStyle("-fx-border-width: 0;  -fx-border-color: black;");
            }
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Right",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void pasteAction(){
        try {
            if(objWeave.getClipMatrix()==null){
                lblStatus.setText(objDictionaryAction.getWord("NOREGION"));
                new MessageView("alert", "alert", objDictionaryAction.getWord("NOREGION"));
            } else{ 
                lblStatus.setText(objDictionaryAction.getWord("ACTIONPASTE"));
                objWeaveAction.paste(objWeave,current_row,current_col);
                populateNplot();
                populateDplot();
                Weave objClonedWeave=cloneWeave();
                objUR.doCommand("Copy Paste", objClonedWeave);
                //isSelectionMode=false;
                //scene.setCursor(Cursor.DEFAULT);
                //selectEditBtn.setStyle("-fx-border-width: 0;  -fx-border-color: black;");
            }
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Right",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void mirrorVerticalSelectedAction(){
        try {               
            if(initial_row==final_row && initial_col==final_col){
                lblStatus.setText(objDictionaryAction.getWord("NOREGION"));
                new MessageView("alert", "alert", objDictionaryAction.getWord("NOREGION"));
            } else{     
                lblStatus.setText(objDictionaryAction.getWord("ACTIONVERTICALMIRROR"));
                objWeaveAction.mirrorVertical(objWeave, initial_row, initial_col, final_row, final_col);
                drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
                populateNplot();
                populateDplot();
                Weave objClonedWeave=cloneWeave();
                objUR.doCommand("Mirror Vertical", objClonedWeave);
                //isSelectionMode=false;
                //scene.setCursor(Cursor.DEFAULT);
                //selectEditBtn.setStyle("-fx-border-width: 0;  -fx-border-color: black;");
           }
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Mirror Vertical",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void mirrorHorizontalSelectedAction(){        
        try {
            if(initial_row==final_row && initial_col==final_col){
                lblStatus.setText(objDictionaryAction.getWord("NOREGION"));
                new MessageView("alert", "alert", objDictionaryAction.getWord("NOREGION"));
            } else{      
                lblStatus.setText(objDictionaryAction.getWord("ACTIONHORIZENTALMIRROR"));
                objWeaveAction.mirrorHorizontal(objWeave, initial_row, initial_col, final_row, final_col);
                drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
                populateNplot();
                populateDplot();
                Weave objClonedWeave=cloneWeave();
                objUR.doCommand("Mirror Horizontal", objClonedWeave);
                //isSelectionMode=false;
                //scene.setCursor(Cursor.DEFAULT);
                //selectEditBtn.setStyle("-fx-border-width: 0;  -fx-border-color: black;");
           }
        } catch (Exception ex) {
          new Logging("SEVERE",getClass().getName(),"Mirror Vertical",ex);
          lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    /**
     * complexWeaveAction
     * <p>
     * This method is used for creating GUI of artwork resize pane.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for creating GUI of artwork resize pane.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void complexWeaveAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCOMPLEXWEAVEEDIT")); 
        if(isWeaveChildStageOn){
            if(weaveChildStage!=null)
                    weaveChildStage.close();
        }           
                
        weaveChildStage = new Stage();
        weaveChildStage.initOwner(weaveStage);
        weaveChildStage.initStyle(StageStyle.UTILITY);
        //artworkPopupStage.initModality(Modality.WINDOW_MODAL);
        
        Label lblWeave = new Label(objDictionaryAction.getWord("SELECTTO")+" "+objDictionaryAction.getWord("ADD")+" "+objDictionaryAction.getWord("WEAVE"));
        lblWeave.setAlignment(Pos.TOP_LEFT);
        lblWeave.setPrefHeight(lblWeave.getFont().getSize());
        
        ScrollPane weaveSP = new ScrollPane();
        weaveSP.setId("subpopup");        
        GridPane weaveGP = new GridPane();
        //weaveGP.setAlignment(Pos.CENTER);
        //weaveGP.setPadding(new Insets(25, 25, 25, 25));
        weaveGP.setHgap(10);
        weaveGP.setVgap(10);
        weaveSP.setContent(weaveGP);
        weaveSP.setPrefSize(500, 275);
        //weaveGP.setPrefSize(500, 275);
        final List lstWeaves = new ArrayList();
        populateWeaveGP(weaveGP, lstWeaves);
        
        Label weavingMode = new Label(objDictionaryAction.getWord("WEAVINGMODE")+" :");
        weavingMode.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPWEAVINGMODE")));
        final ComboBox weavingModeCB = new ComboBox();
        weavingModeCB.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPWEAVINGMODE")));
        weavingModeCB.getItems().addAll("Complex Weave: Warp Faced","Complex Weave: Weft Faced","Extra Warp");
        weavingModeCB.setValue("Extra Warp");//Complex Weave: Alternate Arrangement
                
        final CheckBox layerdClothCB = new CheckBox(objDictionaryAction.getWord("MULTILYEARD"));
        layerdClothCB.setSelected(false);        
        layerdClothCB.selectedProperty().addListener(new ChangeListener<Boolean>() {
           public void changed(ObservableValue<? extends Boolean> ov,
             Boolean old_val, Boolean new_val) {
             weavingModeCB.setDisable(layerdClothCB.isSelected());
          }
        });
        
        Button btnApply = new Button(objDictionaryAction.getWord("APPLY"));
        btnApply.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/update.png"));
        btnApply.setTooltip(new Tooltip(objDictionaryAction.getWord("ACTIONAPPLY")));
        //btnApply.setMaxWidth(Double.MAX_VALUE);
        btnApply.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                Weave objWeaveNew = new Weave();
                objWeaveNew.setObjConfiguration(objWeave.getObjConfiguration());
                boolean extraWarpMode = false; // alternate merge arrangement (horizontal expansion) with colors
                if(layerdClothCB.isSelected()){
                    //code of multi layer cloth
                    objWeaveAction.multiLayeredMerge(objWeaveNew,lstWeaves); 
                    //warpYarn = objWeave.getWarpYarn();
                    //weftYarn = objWeave.getWeftYarn();
                    //plotWarpColor();
                    //plotWeftColor();
                    //objWeaveAction.populateColorPalette(objWeave);
                    //loadColorPalettes();
                }else{
                    //System.err.println(layerdClothCB.getText());
                    if(weavingModeCB.getValue().toString().equalsIgnoreCase("Complex Weave: Warp Faced")){
                        //System.err.println("Complex Weave: Warp Faced");
                        objWeaveAction.warpFaceMerge(objWeaveNew,lstWeaves);
                    } else if(weavingModeCB.getValue().toString().equalsIgnoreCase("Complex Weave: Weft Faced")){
                        //System.err.println("Complex Weave: Weft Faced");
                        objWeaveAction.weftFaceMerge(objWeaveNew,lstWeaves);
                    } else{ //"Complex Weave: Alternate Arrangement"
                        //System.err.println("Complex Weave: Alternate Arrangement");
                        extraWarpMode = true;
                        objWeaveAction.alternateMerge(objWeaveNew,lstWeaves);                        
                    }
                    
                }
                if(objWeaveNew.getIntWarp()>=100 || objWeaveNew.getIntWeft()>=100){
                    weaveChildStage.setTitle(objDictionaryAction.getWord("MAXVALUE"));
                    lblStatus.setText(objDictionaryAction.getWord("MAXVALUE"));
                }else{
                    objWeave.setDesignMatrix(objWeaveNew.getDesignMatrix());
                    objWeave.setIntWarp(objWeaveNew.getIntWarp());
                    objWeave.setIntWeft(objWeaveNew.getIntWeft()); 
                    // as active design area of updated weave is changed
                    activeGridCol = objWeave.getIntWarp();
                    activeGridRow = objWeave.getIntWeft();
                    if(layerdClothCB.isSelected() || extraWarpMode){
                        objWeave.setWarpYarn(objWeaveNew.getWarpYarn());
                        objWeave.setWeftYarn(objWeaveNew.getWeftYarn());
                        objWeave.setObjConfiguration(objWeaveNew.getObjConfiguration());  
                        //objWeaveAction.generateYarnPattern(objWeave);
                    }
                    Weave objClonedWeave=cloneWeave();
                    objUR.doCommand("Complex Weave", objClonedWeave);
                    populateNplot();
                    populateDplot();
                    System.gc();
                    if(weaveChildStage!=null)
                        weaveChildStage.close();
                }
            }
        });
        
        Button btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));
        btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
        //btnCancel.setMaxWidth(Double.MAX_VALUE);
        btnCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                weaveChildStage.close();
            }
        });
        
        weaveChildStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                weaveChildStage.close();
            }
        });  
        
        GridPane popup = new GridPane();
        popup.setId("popup");
        popup.setAlignment(Pos.CENTER);
        popup.setHgap(10);
        popup.setVgap(10);        
        //popup.setPadding(new Insets(25, 25, 25, 25));
        popup.add(lblWeave, 0, 0, 4, 1);
        popup.add(weaveSP, 0, 1, 4, 1);
        popup.add(layerdClothCB, 0, 2, 2, 1);
        popup.add(weavingMode, 2, 2, 1, 1);
        popup.add(weavingModeCB, 3, 2, 1, 1);
        popup.add(btnApply, 0, 3, 2, 1);
        popup.add(btnCancel, 2, 3, 2, 1);
        
        Scene popupScene = new Scene(popup, 525, 375);
        popupScene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        weaveChildStage.setScene(popupScene);
        weaveChildStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("WINDOWWEAVEASSIGNMENT"));
        weaveChildStage.showAndWait();      
    }
    private void editDecomposeOperationAction(){
        WeaveDecomposeView objWeaveDecomposeView = new WeaveDecomposeView(objConfiguration, objWeave);
        objWeaveDecomposeView.lblChange.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!t1.equals(t)){
                    populateDplot();
                    if(t1.equals("-1")){ // WeaveDecomposeView stage closed
                        populateNplot(); // generate components so weave can be saved
                    }
                }
            }
        });
    }
    private void populateWeaveGP(final GridPane weaveGP, final List lstWeaves){
        int i = 0;
        //System.err.println("Size="+lstWeaves.size());
        weaveGP.getChildren().clear();
        for(; i<lstWeaves.size(); i++){
            final ImageView weaveIV  = new ImageView("/media/assign_weave.png");
            weaveIV.setFitHeight(111);              
            weaveIV.setFitWidth(111);
            weaveIV.getStyleClass().addAll("myBox");
            Weave objNewWeave = (Weave)lstWeaves.get(i);
            weaveIV.setUserData(objNewWeave.getStrWeaveID());
            weaveIV.setId(Integer.toString(i));
            if(objNewWeave.getStrWeaveID()!=null){
                try {
                    objNewWeave.setObjConfiguration(objWeave.getObjConfiguration());
                    WeaveAction objWeaveAction = new WeaveAction();
                    objWeaveAction.getWeave(objNewWeave);
                    SeekableStream stream = new ByteArraySeekableStream(objNewWeave.getBytWeaveThumbnil());
                    String[] names = ImageCodec.getDecoderNames(stream);
                    ImageDecoder dec = ImageCodec.createImageDecoder(names[0], stream, null);
                    RenderedImage im = dec.decodeAsRenderedImage();
                    BufferedImage weaveImage = PlanarImage.wrapRenderedImage(im).getAsBufferedImage();
                    Image pattern=SwingFXUtils.toFXImage(weaveImage, null);
                    weaveIV.setImage(pattern);
                    //objNewWeave = null;
                    weaveIV.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            try {
                                weaveIV.setCursor(Cursor.HAND);
                                Weave objNewWeave = new Weave();
                                objNewWeave.setObjConfiguration(objWeave.getObjConfiguration()); 
                                objNewWeave.setStrWeaveID(weaveIV.getUserData().toString());
                                if(event.isControlDown() || event.isShiftDown()){
                                    //WeaveImportView objWeaveImportView= new WeaveImportView(objNewWeave); 
                                    //lstWeaves.remove(objNewWeave);
                                    lstWeaves.remove(Integer.parseInt(weaveIV.getId()));
                                }else if(event.isAltDown()){
                                    try {
                                        WeaveAction objWeaveAction = new WeaveAction();
                                        objWeaveAction.getWeave(objNewWeave);
                                        objWeaveAction.extractWeaveContent(objNewWeave);
                                        objWeaveAction.generateYarnPattern(objNewWeave);
                                        objNewWeave.getObjConfiguration().setWarpYarn(objNewWeave.getWarpYarn());
                                        objNewWeave.getObjConfiguration().setWeftYarn(objNewWeave.getWeftYarn());
                                        PatternView objPatternView = new PatternView(objNewWeave.getObjConfiguration());            
                                        for(int i=0, j = objNewWeave.getObjConfiguration().getWarpYarn().length; i<objNewWeave.getIntWarp();i++) {
                                            objNewWeave.getWarpYarn()[i]=objNewWeave.getObjConfiguration().getWarpYarn()[i%j];
                                        }
                                        //objNewWeave.setWeftYarn(objNewWeave.getObjConfiguration().getWeftYarn());
                                        for(int i=0, j = objNewWeave.getObjConfiguration().getWeftYarn().length; i<objNewWeave.getIntWeft();i++) {
                                            objNewWeave.getWeftYarn()[i]=objNewWeave.getObjConfiguration().getWeftYarn()[i%j];
                                        }
                                        objWeaveAction.populateColorPalette(objNewWeave);
                                    } catch (Exception ex) {
                                        new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                                        lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                    }
                                } else{
                                    WeaveEditView objWeaveEditView = new WeaveEditView(objNewWeave);
                                    if(objNewWeave.getStrWeaveID()!=null){
                                        lstWeaves.set(Integer.parseInt(weaveIV.getId()),objNewWeave);
                                    } else{
                                        lblStatus.setText("Your last action to assign weave pattern was not completed");
                                    }
                                }
                                populateWeaveGP(weaveGP, lstWeaves);                                
                            } catch (Exception ex) {
                                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                            }
                        }
                    });
                    weaveGP.add(weaveIV, i%4, i/4);
                } catch (SQLException ex) {
                    new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }                
            }
        }
        final ImageView addWeaveIV  = new ImageView("/media/add_item.png");
        addWeaveIV.setFitHeight(111);              
        addWeaveIV.setFitWidth(111);
        addWeaveIV.getStyleClass().addAll("myBox");
        
        addWeaveIV.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    addWeaveIV.setCursor(Cursor.HAND);
                    Weave objNewWeave = new Weave();
                    objNewWeave.setObjConfiguration(objWeave.getObjConfiguration());                        
                    if(event.isControlDown() || event.isAltDown()){
                        WeaveEditView objWeaveEditView = new WeaveEditView(objNewWeave);                        
                    } else{
                        WeaveImportView objWeaveImportView= new WeaveImportView(objNewWeave);                        
                    }
                    if(objNewWeave.getStrWeaveID()!=null){
                        lstWeaves.add(objNewWeave);
                        populateWeaveGP(weaveGP, lstWeaves);
                    } else{
                        lblStatus.setText("Your last action to assign weave pattern was not completed");
                    }
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        weaveGP.add(addWeaveIV, i%4, i/4);
    }
    
    private  void jacquardConverstionAction(){
        try {
            if(initial_row==final_row && initial_col==final_col){
                lblStatus.setText(objDictionaryAction.getWord("NOREGION"));
                new MessageView("alert", "alert", objDictionaryAction.getWord("NOREGION"));                        
            } else{                   
                lblStatus.setText(objDictionaryAction.getWord("ACTIONTRANSFORMOPERATIONEDIT"));
                Weave tmpWeave=new Weave();
                tmpWeave.setObjConfiguration(objConfiguration);
                tmpWeave.setIntEPI(objConfiguration.getIntEPI());
                tmpWeave.setIntPPI(objConfiguration.getIntPPI());
                WeaveImportView objWeaveImportView=new WeaveImportView(tmpWeave);
                if(tmpWeave!=null && tmpWeave.getStrWeaveID()!=null && tmpWeave.getStrWeaveID()!=""){
                    setWeaveClipMatrix(tmpWeave,initial_row,initial_col,final_row,final_col);
                    pasteAction();
                    // cleanup
                    tmpWeave=null;
                    objWeaveImportView=null;
                    System.gc();
                } else{
                    lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
                }
            }
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Right",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void setWeaveClipMatrix(Weave tmpWeave, int rowinitial,int colinitial,int rowfinal,int colfinal){
        try {
            int rows = rowinitial-rowfinal;
            int cols = colinitial-colfinal;
            int temp=0;
            byte[][] designPaste;
            if(rows>0){
                if(cols>0){
                    temp=rowfinal;
                    rowfinal = rowinitial;
                    rowinitial = temp;
                    temp = colfinal;
                    colfinal = colinitial;
                    colinitial = temp;
                }else{
                    temp = rowfinal;
                    rowfinal = rowinitial;
                    rowinitial = temp;
                }
            }else{
                if(cols>0){
                    temp = colfinal;
                    colfinal = colinitial;
                    colinitial = temp;
                }
            }
            rows = rowfinal-rowinitial;
            cols = colfinal-colinitial;
            designPaste=new byte[rows+1][cols+1];
            
             // process values from design matrix
            byte[][] fillWeaveDesignMatrix=tmpWeave.getDesignMatrix();
            int numWeft=fillWeaveDesignMatrix.length;
            int numWarp=fillWeaveDesignMatrix[0].length;
            for(int wf=rows; wf>=0; wf--){
                for(int wp=0; wp<cols+1; wp++){
                    designPaste[wf][wp]=fillWeaveDesignMatrix[(numWeft-1)-((rows)-wf)%numWeft][wp%numWarp];
                }
            }
            objWeave.setClipMatrix(designPaste);
            designPaste=null;
            System.gc();
            // now paste
            //System.err.println(current_col+":"+current_row+"::"+colinitial+":"+rowinitial+"::"+initial_col+":"+initial_row);
            current_col=colinitial;
            current_row=rowinitial;           
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"fille weave function"+ex.toString(),ex);
        }
    }   
    private void repeatSelectionInActiveArea(){
        try {
            if(initial_row==final_row && initial_col==final_col){
                lblStatus.setText(objDictionaryAction.getWord("NOREGION"));
                new MessageView("alert", "alert", objDictionaryAction.getWord("NOREGION"));                        
            } else{                   
                lblStatus.setText(objDictionaryAction.getWord("ACTIONREPEATSELECTED"));
                //objWeaveAction.copy(objWeave,initial_row,initial_col,final_row,final_col);
                copyAction();
                // prepare full repeat matrix
                objWeave.setClipMatrix(objWeaveAction.repeatMatrix(objWeave.getClipMatrix(), activeGridCol, activeGridRow));
                current_col=0;
                current_row=0;
                pasteAction();
            }
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Right",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }    
    private  void setClipMatrixInActiveArea(byte[][] weaveDesignMatrix){
        int numWeft=weaveDesignMatrix.length;
        int numWarp=weaveDesignMatrix[0].length;
        byte[][] tempMatrix=new byte[activeGridRow][activeGridCol];
        for(int wf=activeGridRow-1; wf>=0; wf--){
            for(int wp=0; wp<activeGridCol; wp++){
                tempMatrix[wf][wp]=weaveDesignMatrix[(numWeft-1)-((activeGridRow-1)-wf)%numWeft][wp%numWarp];
            }
        }
        objWeave.setClipMatrix(tempMatrix);
        tempMatrix=null;
    }
    
    private void populateProperties(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONINFOSETTINGSUTILITY"));
        weaveDetails(true); 
    }
    private void threadSequenceAction(){
        objWeaveAction.generateYarnPattern(objWeave);
        lblStatus.setText(objDictionaryAction.getWord("ACTIONTHREADPATTERNEDIT"));
        ///objUR.doCommand("Edit Thread Sequence", objFabricCopy);            
        try {
            //objWeave.getObjConfiguration().setColourPalette(objWeave.getColourPalette());
            //objWeave.getObjConfiguration().setStrWarpPatternID(objWeave.getStrWarpPatternID());
            //objWeave.getObjConfiguration().setStrWeftPatternID(objWeave.getStrWeftPatternID());
            objWeave.getObjConfiguration().setWarpYarn(objWeave.getWarpYarn());
            objWeave.getObjConfiguration().setWeftYarn(objWeave.getWeftYarn());
            PatternView objPatternView = new PatternView(objWeave.getObjConfiguration());            
            //objWeave.setColourPalette(objWeave.getObjConfiguration().getColourPalette());
            //objWeave.setStrWarpPatternID(objWeave.getObjConfiguration().getStrWarpPatternID());
            //objWeave.setStrWeftPatternID(objWeave.getObjConfiguration().getStrWeftPatternID());
            
            //objWeave.setWarpYarn(objWeave.getObjConfiguration().getWarpYarn());                        
            for(int i=0, j = objWeave.getObjConfiguration().getWarpYarn().length; i<objWeave.getIntWarp();i++) {
                objWeave.getWarpYarn()[i]=objWeave.getObjConfiguration().getWarpYarn()[i%j];
            }
            //objWeave.setWeftYarn(objWeave.getObjConfiguration().getWeftYarn());
            for(int i=0, j = objWeave.getObjConfiguration().getWeftYarn().length; i<objWeave.getIntWeft();i++) {
                objWeave.getWeftYarn()[(objWeave.getIntWeft()-1)-i]=objWeave.getObjConfiguration().getWeftYarn()[i%j];
            }
            warpYarn = objWeave.getWarpYarn();
            weftYarn = objWeave.getWeftYarn();
            plotWarpColor();
            plotWeftColor();
            objWeaveAction.populateColorPalette(objWeave);
            loadColorPalettes();
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void switchColorAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONSWITCHCOLOREDIT"));
        weftYarn = new Yarn[objWeave.getWeftYarn().length];
        warpYarn = new Yarn[objWeave.getWarpYarn().length];
        //System.err.println(weftYarn.length+"="+warpYarn.length);
        // reverse weft yarn array (as its [0] element counted from bottom)
        Collections.reverse(Arrays.asList(objWeave.getWeftYarn()));
        //create each new yarn object to swap
        for(int i=0; i<warpYarn.length;i++){
            warpYarn[i] = new Yarn(objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getStrYarnId(),
                    "Warp",
                    objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getStrYarnName(),
                    objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getStrYarnColor(),
                    objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getIntYarnRepeat(),
                    objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getStrYarnSymbol().toUpperCase(),
                    objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getIntYarnCount(),
                    objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getStrYarnCountUnit(),
                    objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getIntYarnPly(),
                    objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getIntYarnDFactor(),
                    objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getDblYarnDiameter(),
                    objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getIntYarnTwist(),
                    objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getStrYarnTModel(),
                    objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getIntYarnHairness(),
                    objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getIntYarnHProbability(),
                    objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getDblYarnPrice(),
                    objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getStrYarnAccess(),
                    objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getStrYarnUser(),
                    objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getStrYarnDate()
            );          
            /*
            warpYarn[i].setStrYarnId(objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getStrYarnId());
            warpYarn[i].setStrYarnType("Warp");            
            warpYarn[i].setStrYarnName(objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getStrYarnName());
            warpYarn[i].setStrYarnColor(objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getStrYarnColor());
            warpYarn[i].setIntYarnRepeat(objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getIntYarnRepeat());
            warpYarn[i].setStrYarnSymbol(objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getStrYarnSymbol().toUpperCase());
            warpYarn[i].setIntYarnCount(objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getIntYarnCount());
            warpYarn[i].setStrYarnCountUnit(objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getStrYarnCountUnit());
            warpYarn[i].setIntYarnPly(objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getIntYarnPly());
            warpYarn[i].setIntYarnDFactor(objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getIntYarnDFactor());
            warpYarn[i].setDblYarnDiameter(objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getDblYarnDiameter());
            warpYarn[i].setIntYarnTwist(objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getIntYarnTwist());
            warpYarn[i].setStrYarnTModel(objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getStrYarnTModel());
            warpYarn[i].setIntYarnHairness(objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getIntYarnHairness());
            warpYarn[i].setIntYarnHProbability(objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getIntYarnHProbability());
            warpYarn[i].setDblYarnPrice(objWeave.getWeftYarn()[i%objWeave.getWeftYarn().length].getDblYarnPrice());
            */
        }
        for(int i=0; i<weftYarn.length;i++){
            // weftYarn[(weftYarn.length-1)-i] assign warp to weft yarns with weft[0] starting from bottom
            weftYarn[(weftYarn.length-1)-i] = new Yarn(objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getStrYarnId(),
                    "Weft",
                    objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getStrYarnName(),
                    objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getStrYarnColor(),
                    objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getIntYarnRepeat(),
                    objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getStrYarnSymbol().toLowerCase(),
                    objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getIntYarnCount(),
                    objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getStrYarnCountUnit(),
                    objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getIntYarnPly(),
                    objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getIntYarnDFactor(),
                    objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getDblYarnDiameter(),
                    objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getIntYarnTwist(),
                    objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getStrYarnTModel(),
                    objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getIntYarnHairness(),
                    objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getIntYarnHProbability(),
                    objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getDblYarnPrice(),
                    objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getStrYarnAccess(),
                    objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getStrYarnUser(),
                    objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getStrYarnDate()
            );  
            /*
            weftYarn[i].setStrYarnId(objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getStrYarnId());
            weftYarn[i].setStrYarnType("Weft");            
            weftYarn[i].setStrYarnName(objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getStrYarnName());
            weftYarn[i].setStrYarnColor(objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getStrYarnColor());
            weftYarn[i].setIntYarnRepeat(objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getIntYarnRepeat());
            weftYarn[i].setStrYarnSymbol(objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getStrYarnSymbol().toLowerCase());
            weftYarn[i].setIntYarnCount(objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getIntYarnCount());
            weftYarn[i].setStrYarnCountUnit(objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getStrYarnCountUnit());
            weftYarn[i].setIntYarnPly(objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getIntYarnPly());
            weftYarn[i].setIntYarnDFactor(objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getIntYarnDFactor());
            weftYarn[i].setDblYarnDiameter(objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getDblYarnDiameter());
            weftYarn[i].setIntYarnTwist(objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getIntYarnTwist());
            weftYarn[i].setStrYarnTModel(objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getStrYarnTModel());
            weftYarn[i].setIntYarnHairness(objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getIntYarnHairness());
            weftYarn[i].setIntYarnHProbability(objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getIntYarnHProbability());
            weftYarn[i].setDblYarnPrice(objWeave.getWarpYarn()[i%objWeave.getWarpYarn().length].getDblYarnPrice());
            */
        }
        objWeave.setWarpYarn(warpYarn);
        objWeave.setWeftYarn(weftYarn);
        //System.err.println(weftYarn.length+"="+warpYarn.length);
        plotWarpColor();
        plotWeftColor();
    }
    private void yarnPropertiesAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONYARNEDIT"));                
        objWeave.getObjConfiguration().setWarpYarn(objWeave.getWarpYarn());
        objWeave.getObjConfiguration().setWeftYarn(objWeave.getWeftYarn());
        objWeave.getObjConfiguration().setWarpExtraYarn(null);
        objWeave.getObjConfiguration().setWeftExtraYarn(null);
        objWeave.getObjConfiguration().setIntExtraWeft(0);
        objWeave.getObjConfiguration().setIntExtraWarp(0);        
        YarnEditView objYarnEditView = new YarnEditView(objWeave.getObjConfiguration());
        objWeave.setWarpYarn(objWeave.getObjConfiguration().getWarpYarn());
        objWeave.setWeftYarn(objWeave.getObjConfiguration().getWeftYarn());
        plotWarpColor();
        plotWeftColor();
        objWeaveAction.populateColorPalette(objWeave);
        loadColorPalettes();
    } 
    private void editYarnPalette(){
        YarnPaletteView objYarnPaletteView=new YarnPaletteView(objWeave.getObjConfiguration());        
        objWeaveAction.populateColorPalette(objWeave);
        loadColorPalettes();
        populateNplotWarpWeftYarn();
    }
    /**
     * repeatOrientationAction
     * <p>
     * This method is used for creating GUI of artwork resize pane.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for creating GUI of artwork resize pane.
     * @see         javafx.stage.*;
     * @link        com.mla.artwork.Artwork
     */
    private void repeatOrientationAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONREPEATORIENTATION")); 
        if(isWeaveChildStageOn){
            if(weaveChildStage!=null)
                    weaveChildStage.close();
        }           
                
        weaveChildStage = new Stage();
        weaveChildStage.initOwner(weaveStage);
        weaveChildStage.initStyle(StageStyle.UTILITY);
        //artworkPopupStage.initModality(Modality.WINDOW_MODAL);
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setAlignment(Pos.CENTER);
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        
        final CheckBox multipleRepeatCB = new CheckBox(objDictionaryAction.getWord("MREPEAT"));
        multipleRepeatCB.setSelected(objConfiguration.getBlnMRepeat());        
        //multipleRepeatCB.setDisable(!objConfiguration.getBlnMRepeat());
        popup.add(multipleRepeatCB, 0, 1, 2, 1);
        
        Separator sepHor1 = new Separator();
        sepHor1.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHor1, 0, 2);
        GridPane.setColumnSpan(sepHor1, 2);
        popup.getChildren().add(sepHor1);
        
        final CheckBox freezeRepeatCB = new CheckBox(objDictionaryAction.getWord("FREEZEDESIGN"));
        freezeRepeatCB.setSelected(objConfiguration.getBlnMRepeat());        
        //freezeRepeatCB.setDisable(!objConfiguration.getBlnMRepeat());
        popup.add(freezeRepeatCB, 0, 3, 2, 1);
        
        Label vertical= new Label(objDictionaryAction.getWord("VREPEAT")+" :");
        //vertical.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        vertical.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPVREPEAT")));
        popup.add(vertical, 0, 4);
        final TextField verticalTF = new TextField(Integer.toString(1)){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        verticalTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPVREPEAT")));
        verticalTF.setDisable(!objConfiguration.getBlnMRepeat());
        popup.add(verticalTF, 1, 4);
        
        Label horizental= new Label(objDictionaryAction.getWord("HREPEAT")+" :");
        //horizental.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        horizental.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPHREPEAT")));
        popup.add(horizental, 0, 5);
        final TextField horizentalTF = new TextField(Integer.toString(1)){
            @Override public void replaceText(int start, int end, String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
            if (text.matches("[0-9]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        horizentalTF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPHREPEAT")));
        horizentalTF.setDisable(!objConfiguration.getBlnMRepeat());
        popup.add(horizentalTF, 1, 5);
                
        showDesignRepeats();
        multipleRepeatCB.selectedProperty().addListener(new ChangeListener<Boolean>() {
           public void changed(ObservableValue<? extends Boolean> ov,
             Boolean old_val, Boolean new_val) {
             objConfiguration.setBlnMRepeat(multipleRepeatCB.isSelected());
             showDesignRepeats();
          }
        });
        
        freezeRepeatCB.selectedProperty().addListener(new ChangeListener<Boolean>() {
           public void changed(ObservableValue<? extends Boolean> ov,
             Boolean old_val, Boolean new_val) {
             verticalTF.setDisable(!freezeRepeatCB.isSelected());
             horizentalTF.setDisable(!freezeRepeatCB.isSelected());
          }
        });
        
        Button btnApply = new Button(objDictionaryAction.getWord("APPLY"));
        btnApply.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/update.png"));
        btnApply.setTooltip(new Tooltip(objDictionaryAction.getWord("ACTIONAPPLY")));
        //btnApply.setMaxWidth(Double.MAX_VALUE);
        btnApply.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                try {
                    objConfiguration.setBlnMRepeat(false);
                    showDesignRepeats();
                    if(freezeRepeatCB.isSelected()){
                        //objUR.doCommand("Repeats", bufferedImage);
                        //objURF.store(bufferedImage);                                        
                        int vRepeat = Integer.parseInt(verticalTF.getText())*objWeave.getIntWeft();
                        int hRepeat = Integer.parseInt(horizentalTF.getText())*objWeave.getIntWarp();
                        if(vRepeat<=100 && hRepeat<=100){
                            objWeave.setIntWeft(vRepeat);
                            objWeave.setIntWarp(hRepeat);
                            activeGridCol = objWeave.getIntWarp();
                            activeGridRow = objWeave.getIntWeft();
                            //updateActiveDesignGrid(objWeave.getIntWarp(), objWeave.getIntWeft());
                            //refreshDesignImage();
                            objWeaveAction = new WeaveAction();
                            objWeave.setDesignMatrix(objWeaveAction.repeatMatrix(objWeave.getDesignMatrix(), objWeave.getIntWarp(), objWeave.getIntWeft()));
                            populateDplot();
                            initObjWeave(true);
                            populateNplot();                            
                            lblStatus.setText(objDictionaryAction.getWord("SUCCESS"));
                            Weave objClonedWeave=cloneWeave();
                            objUR.doCommand("repeat action", objClonedWeave);
                        }else{
                            lblStatus.setText(objDictionaryAction.getWord("MAXVALUE"));
                        }
                    }
                    objConfiguration.setBlnMRepeat(multipleRepeatCB.isSelected());
                    showDesignRepeats();
                } catch (SQLException ex) {               
                    new Logging("SEVERE",getClass().getName(),"Operation apply",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
               } catch(OutOfMemoryError ex){
                    undoAction();
                }
                System.gc();
                if(weaveChildStage!=null)
                    weaveChildStage.close();
            }
        });
        popup.add(btnApply, 0, 9);

        Button btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));
        btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
        btnCancel.setMaxWidth(Double.MAX_VALUE);
        btnCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                weaveChildStage.close();
            }
        });
        popup.add(btnCancel, 1, 9);

        weaveChildStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                weaveChildStage.close();
            }
        });        
        Scene popupScene = new Scene(popup);
        popupScene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        weaveChildStage.setScene(popupScene);
        weaveChildStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("REPEATORIENTATION"));
        weaveChildStage.showAndWait();      
    }
    private void showDesignRepeats(){
        if(objConfiguration.getBlnMRepeat()){
            isWorkingMode=false;
            //selectedMenu = "FILE";
            menuHighlight();
            // disable sliders
            vSlider.setDisable(true);
            hSlider.setDisable(true);
            designIV.setMouseTransparent(true);
            tradelesIV.setMouseTransparent(true);
            tieUpIV.setMouseTransparent(true);
            shaftIV.setMouseTransparent(true);
            pegIV.setMouseTransparent(true);
            // show repeats
            if(objWeave!=null && objWeave.getDentMatrix()!=null && objWeave.getShaftMatrix()!=null &&
                                objWeave.getPegMatrix()!=null && objWeave.getTreadlesMatrix()!=null &&
                                objWeave.getTieupMatrix()!=null && objWeave.getDentMatrix()!=null){
                drawRepeatImageFromMatrix(objWeave.getDesignMatrix(), designImage);
                drawRepeatImageFromMatrix(objWeave.getShaftMatrix(), shaftImage);
                drawRepeatImageFromMatrix(objWeave.getPegMatrix(), pegImage);
                drawRepeatImageFromMatrix(objWeave.getTreadlesMatrix(), tradelesImage);
                drawRepeatImageFromMatrix(objWeave.getDentMatrix(), dentingImage);
            }
        } else{
            isWorkingMode=true;
            //selectedMenu = "FILE";
            menuHighlight();
            if(isWeaveChildStageOn)
                weaveChildStage.show();
            // enable sliders
            vSlider.setDisable(false);
            hSlider.setDisable(false);
            designIV.setMouseTransparent(false);
            tradelesIV.setMouseTransparent(false);
            tieUpIV.setMouseTransparent(false);
            shaftIV.setMouseTransparent(false);
            pegIV.setMouseTransparent(false);
            // hide repeats
            if(objWeave!=null && objWeave.getDentMatrix()!=null && objWeave.getShaftMatrix()!=null &&
                            objWeave.getPegMatrix()!=null && objWeave.getTreadlesMatrix()!=null &&
                            objWeave.getTieupMatrix()!=null && objWeave.getDentMatrix()!=null){
                drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
                populateNplot();
            }
        }
    }    
    private void sprayToolAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONSPRAYTOOL"));
        objWeaveAction.spray(objWeave,current_row,current_col);
        populateNplot();
        populateDplot();
        Weave objClonedWeave=cloneWeave();
        objUR.doCommand("spray action", objClonedWeave);
    }
    private void transformOperationAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONTRANSFORMOPERATIONEDIT"));
        transformMenuAction();
    }
 /**
 * populateTransformToolbar
 * <p>
 * Function use for editing tool bar for menu item Edit,
 * and binding events for each tools. 
 * 
 * @exception   (@throws SQLException)
 * @author      Amit Kumar Singh
 * @version     %I%, %G%
 * @since       1.0
 * @see         javafx.event.*;
 * @link        WeaveView
 */
    private void populateTransformToolbarMenu(){
        //For drop down menus
        menuSelected.setTooltip(new Tooltip(objDictionaryAction.getWord("EDIT")));
       
        MenuItem mirrorVerticalEditMI = new MenuItem(objDictionaryAction.getWord("VERTICALMIRROR"));
        MenuItem mirrorHorizontalEditMI = new MenuItem(objDictionaryAction.getWord("HORIZENTALMIRROR"));
        MenuItem rotateClockwiseEditMI = new MenuItem(objDictionaryAction.getWord("CLOCKROTATION"));
        MenuItem rotateAnticlockwiseEditMI = new MenuItem(objDictionaryAction.getWord("ANTICLOCKROTATION"));
        MenuItem moveRightEditMI = new MenuItem(objDictionaryAction.getWord("MOVERIGHT"));
        MenuItem moveLeftEditMI = new MenuItem(objDictionaryAction.getWord("MOVELEFT"));
        MenuItem moveUpEditMI = new MenuItem(objDictionaryAction.getWord("MOVEUP"));
        MenuItem moveDownEditMI = new MenuItem(objDictionaryAction.getWord("MOVEDOWN"));        
        MenuItem moveRight8EditMI = new MenuItem(objDictionaryAction.getWord("MOVERIGHT8"));
        MenuItem moveLeft8EditMI = new MenuItem(objDictionaryAction.getWord("MOVELEFT8"));
        MenuItem moveUp8EditMI = new MenuItem(objDictionaryAction.getWord("MOVEUP8"));
        MenuItem moveDown8EditMI = new MenuItem(objDictionaryAction.getWord("MOVEDOWN8"));        
        MenuItem tiltRightEditMI = new MenuItem(objDictionaryAction.getWord("TILTRIGHT"));
        MenuItem tiltLeftEditMI = new MenuItem(objDictionaryAction.getWord("TILTLEFT"));
        MenuItem tiltUpEditMI = new MenuItem(objDictionaryAction.getWord("TILTUP"));
        MenuItem tiltDownEditMI = new MenuItem(objDictionaryAction.getWord("TILTDOWN")); 
        MenuItem inversionEditMI = new MenuItem(objDictionaryAction.getWord("INVERSION"));
        MenuItem clearEditMI = new MenuItem(objDictionaryAction.getWord("CLEARWEAVE"));
        MenuItem closeEditMI = new MenuItem(objDictionaryAction.getWord("CLOSE"));
        
        mirrorVerticalEditMI.setAccelerator(new KeyCodeCombination(KeyCode.V, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        mirrorHorizontalEditMI.setAccelerator(new KeyCodeCombination(KeyCode.H, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        rotateClockwiseEditMI.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        rotateAnticlockwiseEditMI.setAccelerator(new KeyCodeCombination(KeyCode.A, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        moveUpEditMI.setAccelerator(new KeyCodeCombination(KeyCode.UP, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        moveDownEditMI.setAccelerator(new KeyCodeCombination(KeyCode.DOWN, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        moveLeftEditMI.setAccelerator(new KeyCodeCombination(KeyCode.LEFT, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        moveRightEditMI.setAccelerator(new KeyCodeCombination(KeyCode.RIGHT, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        moveUp8EditMI.setAccelerator(new KeyCodeCombination(KeyCode.UP, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        moveDown8EditMI.setAccelerator(new KeyCodeCombination(KeyCode.DOWN, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        moveLeft8EditMI.setAccelerator(new KeyCodeCombination(KeyCode.LEFT, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        moveRight8EditMI.setAccelerator(new KeyCodeCombination(KeyCode.RIGHT, KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN));
        tiltUpEditMI.setAccelerator(new KeyCodeCombination(KeyCode.UP, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        tiltDownEditMI.setAccelerator(new KeyCodeCombination(KeyCode.DOWN, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        tiltLeftEditMI.setAccelerator(new KeyCodeCombination(KeyCode.LEFT, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        tiltRightEditMI.setAccelerator(new KeyCodeCombination(KeyCode.RIGHT, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        inversionEditMI.setAccelerator(new KeyCodeCombination(KeyCode.I, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN));
        clearEditMI.setAccelerator(new KeyCodeCombination(KeyCode.BACK_SPACE, KeyCombination.CONTROL_DOWN));
        closeEditMI.setAccelerator(new KeyCodeCombination(KeyCode.ESCAPE));
        
        mirrorVerticalEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
        mirrorHorizontalEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
        rotateClockwiseEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_90.png"));
        rotateAnticlockwiseEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_anti_90.png"));
        moveRightEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right.png"));
        moveLeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left.png"));
        moveUpEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up.png"));
        moveDownEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down.png"));
        moveRight8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right_by_8.png"));
        moveLeft8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left_by_8.png"));
        moveUp8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up_by_8.png"));
        moveDown8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down_by_8.png"));        
        tiltRightEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_right.png"));
        tiltLeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_left.png"));
        tiltUpEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_up.png"));
        tiltDownEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_down.png"));
        inversionEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/inversion.png"));
        clearEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/clear.png"));
        closeEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
         
        Menu mirrorMenu = new Menu(objDictionaryAction.getWord("MIRROR"));
        Menu rotateMenu = new Menu(objDictionaryAction.getWord("ROTATE"));
        Menu moveMenu = new Menu(objDictionaryAction.getWord("MOVE"));
        Menu tiltMenu = new Menu(objDictionaryAction.getWord("TILT"));        
        mirrorMenu.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/symmetry.png"));
        rotateMenu.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/update.png"));
        moveMenu.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/skip.png"));
        tiltMenu.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/diamond.png"));
        mirrorMenu.getItems().addAll(mirrorVerticalEditMI,mirrorHorizontalEditMI);
        rotateMenu.getItems().addAll(rotateClockwiseEditMI,rotateAnticlockwiseEditMI);
        moveMenu.getItems().addAll(moveRightEditMI, moveLeftEditMI, moveUpEditMI, moveDownEditMI, moveRight8EditMI, moveLeft8EditMI, moveUp8EditMI, moveDown8EditMI);
        tiltMenu.getItems().addAll(tiltRightEditMI, tiltLeftEditMI, tiltUpEditMI, tiltDownEditMI);
    
        //Add menu enable disable condition
        if(!isWorkingMode){
            mirrorVerticalEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
            mirrorVerticalEditMI.setDisable(true);
            mirrorHorizontalEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
            mirrorHorizontalEditMI.setDisable(true);
            rotateClockwiseEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/rotate_90.png"));
            rotateClockwiseEditMI.setDisable(true);
            rotateAnticlockwiseEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/rotate_anti_90.png"));
            rotateAnticlockwiseEditMI.setDisable(true);
            moveRightEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/move_right.png"));
            moveRightEditMI.setDisable(true);
            moveLeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/move_left.png"));
            moveLeftEditMI.setDisable(true);
            moveUpEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/move_up.png"));
            moveUpEditMI.setDisable(true);
            moveDownEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/move_down.png"));
            moveDownEditMI.setDisable(true);
            moveRight8EditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/move_right_by_8.png"));
            moveRight8EditMI.setDisable(true);
            moveLeft8EditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/move_left_by_8.png"));
            moveLeft8EditMI.setDisable(true);
            moveUp8EditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/move_up_by_8.png"));
            moveUp8EditMI.setDisable(true);
            moveDown8EditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/move_down_by_8.png"));
            moveDown8EditMI.setDisable(true);
            tiltRightEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/tilt_right.png"));
            tiltRightEditMI.setDisable(true);
            tiltLeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/tilt_left.png"));
            tiltLeftEditMI.setDisable(true);
            tiltUpEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/tilt_up.png"));
            tiltUpEditMI.setDisable(true);
            tiltDownEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/tilt_down.png"));
            tiltDownEditMI.setDisable(true);
            inversionEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/inversion.png"));
            inversionEditMI.setDisable(true);
            clearEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/clear.png"));
            clearEditMI.setDisable(true); 
            closeEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            closeEditMI.setDisable(true);
        }else{
            mirrorVerticalEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
            mirrorVerticalEditMI.setDisable(false);
            mirrorHorizontalEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
            mirrorHorizontalEditMI.setDisable(false);
            rotateClockwiseEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_90.png"));
            rotateClockwiseEditMI.setDisable(false);
            rotateAnticlockwiseEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_anti_90.png"));
            rotateAnticlockwiseEditMI.setDisable(false);
            moveRightEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right.png"));
            moveRightEditMI.setDisable(false);
            moveLeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left.png"));
            moveLeftEditMI.setDisable(false);
            moveUpEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up.png"));
            moveUpEditMI.setDisable(false);
            moveDownEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down.png"));
            moveDownEditMI.setDisable(false);
            moveRight8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right_by_8.png"));
            moveRight8EditMI.setDisable(false);
            moveLeft8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left_by_8.png"));
            moveLeft8EditMI.setDisable(false);
            moveUp8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up_by_8.png"));
            moveUp8EditMI.setDisable(false);
            moveDown8EditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down_by_8.png"));
            moveDown8EditMI.setDisable(false);
            tiltRightEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_right.png"));
            tiltRightEditMI.setDisable(false);
            tiltLeftEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_left.png"));
            tiltLeftEditMI.setDisable(false);
            tiltUpEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_up.png"));
            tiltUpEditMI.setDisable(false);
            tiltDownEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_down.png"));
            tiltDownEditMI.setDisable(false);
            inversionEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/inversion.png"));
            inversionEditMI.setDisable(false);
            clearEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/clear.png"));
            clearEditMI.setDisable(false);
            closeEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            closeEditMI.setDisable(false);
        }        
        //Add the action to Buttons.        
        mirrorVerticalEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                mirrorVerticalAction();
            }
        });  
        mirrorHorizontalEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                mirrorHorizontalAction();
            }
        }); 
        rotateClockwiseEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                rotateClockwiseAction();
            }
        }); 
        rotateAnticlockwiseEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                rotateAntiClockwiseAction();
            }
        }); 
        moveRightEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveRightAction();
            }
        });
        moveLeftEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveLeftAction();
            }
        });
        moveUpEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveUpAction();
            }
        });
        moveDownEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveDownAction();
            }
        });
        moveRight8EditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveRight8Action();
            }
        });
        moveLeft8EditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveLeft8Action();
            }
        });
        moveUp8EditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveUp8Action();
            }
        });
        moveDown8EditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                moveDown8Action();
            }
        });
        tiltRightEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                tiltRightAction();
            }
        });
        tiltLeftEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                tiltLeftAction();
            }
        });
        tiltUpEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                tiltUpAction();
            }
        });
        tiltDownEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                tiltDownAction();
            }
        });        
        inversionEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                inversionAction();
            }
        });
        clearEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                clearAction();
            }
        }); 
        closeEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                closeAction();
            }
        });
        menuSelected.getItems().addAll(mirrorMenu, rotateMenu, moveMenu, tiltMenu, inversionEditMI, clearEditMI, new SeparatorMenuItem(), closeEditMI);
    }
    private void populateTransformToolbar(){
        // mirror edit item
        Button mirrorVerticalEditBtn = new Button(); 
        mirrorVerticalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
        mirrorVerticalEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("VERTICALMIRROR")+" (Ctrl+Alt+V)\n"+objDictionaryAction.getWord("TOOLTIPVERTICALMIRROR")));
        mirrorVerticalEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        mirrorVerticalEditBtn.getStyleClass().addAll("toolbar-button");   
        // mirror edit item
        Button mirrorHorizontalEditBtn = new Button(); 
        mirrorHorizontalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
        mirrorHorizontalEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("HORIZENTALMIRROR")+" (Ctrl+Alt+H)\n"+objDictionaryAction.getWord("TOOLTIPHORIZENTALMIRROR")));
        mirrorHorizontalEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        mirrorHorizontalEditBtn.getStyleClass().addAll("toolbar-button");   
        // clock Wise edit item
        Button rotateClockwiseEditBtn = new Button(); 
        rotateClockwiseEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_90.png"));
        rotateClockwiseEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CLOCKROTATION")+" (Ctrl+Alt+C)\n"+objDictionaryAction.getWord("TOOLTIPCLOCKROTATION")));
        rotateClockwiseEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rotateClockwiseEditBtn.getStyleClass().addAll("toolbar-button");   
        // Anti clock wise edit item
        Button rotateAnticlockwiseEditBtn = new Button(); 
        rotateAnticlockwiseEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_anti_90.png"));
        rotateAnticlockwiseEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ANTICLOCKROTATION")+" (Ctrl+Alt+A)\n"+objDictionaryAction.getWord("TOOLTIPANTICLOCKROTATION")));
        rotateAnticlockwiseEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rotateAnticlockwiseEditBtn.getStyleClass().addAll("toolbar-button");
        // move Right
        Button moveRightEditBtn = new Button(); 
        moveRightEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right.png"));
        moveRightEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MOVERIGHT")+" (Ctrl+Shift+Right)\n"+objDictionaryAction.getWord("TOOLTIPMOVERIGHT")));
        moveRightEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveRightEditBtn.getStyleClass().addAll("toolbar-button");    
        // move Left
        Button moveLeftEditBtn = new Button(); 
        moveLeftEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left.png"));
        moveLeftEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MOVELEFT")+" (Ctrl+Shift+Left)\n"+objDictionaryAction.getWord("TOOLTIPMOVELEFT")));
        moveLeftEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveLeftEditBtn.getStyleClass().addAll("toolbar-button");    
        // move Up
        Button moveUpEditBtn = new Button(); 
        moveUpEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up.png"));
        moveUpEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MOVEUP")+" (Ctrl+Shift+Up)\n"+objDictionaryAction.getWord("TOOLTIPMOVEUP")));
        moveUpEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveUpEditBtn.getStyleClass().addAll("toolbar-button");    
        // move Down
        Button moveDownEditBtn = new Button(); 
        moveDownEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down.png"));
        moveDownEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MOVEDOWN")+" (Ctrl+Shift+Down)\n"+objDictionaryAction.getWord("TOOLTIPMOVEDOWN")));
        moveDownEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveDownEditBtn.getStyleClass().addAll("toolbar-button");
        // move Right 8
        Button moveRight8EditBtn = new Button(); 
        moveRight8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right_by_8.png"));
        moveRight8EditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MOVERIGHT8")+" (Alt+Shift+Right)\n"+objDictionaryAction.getWord("TOOLTIPMOVERIGHT8")));
        moveRight8EditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveRight8EditBtn.getStyleClass().addAll("toolbar-button");    
        // move Left 8
        Button moveLeft8EditBtn = new Button(); 
        moveLeft8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left_by_8.png"));
        moveLeft8EditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MOVELEFT8")+" (Alt+Shift+Left)\n"+objDictionaryAction.getWord("TOOLTIPMOVELEFT8")));
        moveLeft8EditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveLeft8EditBtn.getStyleClass().addAll("toolbar-button");    
        // move Up 8
        Button moveUp8EditBtn = new Button(); 
        moveUp8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up_by_8.png"));
        moveUp8EditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MOVEUP8")+" (Alt+Shift+Up)\n"+objDictionaryAction.getWord("TOOLTIPMOVEUP8")));
        moveUp8EditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveUp8EditBtn.getStyleClass().addAll("toolbar-button");    
        // move Down 8
        Button moveDown8EditBtn = new Button(); 
        moveDown8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down_by_8.png"));
        moveDown8EditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MOVEDOWN8")+" (Alt+Shift+Down)\n"+objDictionaryAction.getWord("TOOLTIPMOVEDOWN8")));
        moveDown8EditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveDown8EditBtn.getStyleClass().addAll("toolbar-button");
        // Tilt Right
        Button tiltRightEditBtn = new Button(); 
        tiltRightEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_right.png"));
        tiltRightEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("TILTRIGHT")+" (Ctrl+Alt+Right)\n"+objDictionaryAction.getWord("TOOLTIPTILTRIGHT")));
        tiltRightEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tiltRightEditBtn.getStyleClass().addAll("toolbar-button");    
        // Tilt Left
        Button tiltLeftEditBtn = new Button(); 
        tiltLeftEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_left.png"));
        tiltLeftEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("TILTLEFT")+" (Ctrl+Alt+Left)\n"+objDictionaryAction.getWord("TOOLTIPTILTLEFT")));
        tiltLeftEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tiltLeftEditBtn.getStyleClass().addAll("toolbar-button");    
        // Tilt Up
        Button tiltUpEditBtn = new Button(); 
        tiltUpEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_up.png"));
        tiltUpEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("TILTUP")+" (Ctrl+Alt+Up)\n"+objDictionaryAction.getWord("TOOLTIPTILTUP")));
        tiltUpEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tiltUpEditBtn.getStyleClass().addAll("toolbar-button");    
        // Tilt Down
        Button tiltDownEditBtn = new Button(); 
        tiltDownEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_down.png"));
        tiltDownEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("TILTDOWN")+" (Ctrl+Alt+Down)\n"+objDictionaryAction.getWord("TOOLTIPTILTDOWN")));
        tiltDownEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tiltDownEditBtn.getStyleClass().addAll("toolbar-button");
        // Inversion
        Button inversionEditBtn = new Button(); 
        inversionEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/inversion.png"));
        inversionEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("INVERSION")+" (Ctrl+Alt+I)\n"+objDictionaryAction.getWord("TOOLTIPINVERSION")));
        inversionEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        inversionEditBtn.getStyleClass().addAll("toolbar-button"); 
        // Clear
        Button clearEditBtn = new Button(); 
        clearEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/clear.png"));
        clearEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CLEARWEAVE")+" (Ctrl+Backspace)\n"+objDictionaryAction.getWord("TOOLTIPCLEARWEAVE")));
        clearEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        clearEditBtn.getStyleClass().addAll("toolbar-button"); 
        // Clear
        Button closeEditBtn = new Button(); 
        closeEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        closeEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CLOSE")+" (Escape)\n"+objDictionaryAction.getWord("TOOLTIPCLOSE")));
        closeEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        closeEditBtn.getStyleClass().addAll("toolbar-button"); 
        //Add menu enable disable condition
        if(!isWorkingMode){
            mirrorVerticalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
            mirrorVerticalEditBtn.setDisable(true);
            mirrorVerticalEditBtn.setCursor(Cursor.WAIT);
            mirrorHorizontalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
            mirrorHorizontalEditBtn.setDisable(true);
            mirrorHorizontalEditBtn.setCursor(Cursor.WAIT);            
            rotateClockwiseEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/rotate_90.png"));
            rotateClockwiseEditBtn.setDisable(true);
            rotateClockwiseEditBtn.setCursor(Cursor.WAIT);
            rotateAnticlockwiseEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/rotate_anti_90.png"));
            rotateAnticlockwiseEditBtn.setDisable(true);
            rotateAnticlockwiseEditBtn.setCursor(Cursor.WAIT);
            moveRightEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/move_right.png"));
            moveRightEditBtn.setDisable(true);
            moveRightEditBtn.setCursor(Cursor.WAIT);
            moveLeftEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/move_left.png"));
            moveLeftEditBtn.setDisable(true);
            moveLeftEditBtn.setCursor(Cursor.WAIT);
            moveUpEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/move_up.png"));
            moveUpEditBtn.setDisable(true);
            moveUpEditBtn.setCursor(Cursor.WAIT);
            moveDownEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/move_down.png"));
            moveDownEditBtn.setDisable(true);
            moveDownEditBtn.setCursor(Cursor.WAIT);
            moveRight8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/move_right_by_8.png"));
            moveRight8EditBtn.setDisable(true);
            moveRight8EditBtn.setCursor(Cursor.WAIT);
            moveLeft8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/move_left_by_8.png"));
            moveLeft8EditBtn.setDisable(true);
            moveLeft8EditBtn.setCursor(Cursor.WAIT);
            moveUp8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/move_up_by_8.png"));
            moveUp8EditBtn.setDisable(true);
            moveUp8EditBtn.setCursor(Cursor.WAIT);
            moveDown8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/move_down_by_8.png"));
            moveDown8EditBtn.setDisable(true);
            moveDown8EditBtn.setCursor(Cursor.WAIT);
            tiltRightEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/tilt_right.png"));
            tiltRightEditBtn.setDisable(true);
            tiltRightEditBtn.setCursor(Cursor.WAIT);
            tiltLeftEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/tilt_left.png"));
            tiltLeftEditBtn.setDisable(true);
            tiltLeftEditBtn.setCursor(Cursor.WAIT);
            tiltUpEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/tilt_up.png"));
            tiltUpEditBtn.setDisable(true);
            tiltUpEditBtn.setCursor(Cursor.WAIT);
            tiltDownEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/tilt_down.png"));
            tiltDownEditBtn.setDisable(true);
            tiltDownEditBtn.setCursor(Cursor.WAIT);
            inversionEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/inversion.png"));
            inversionEditBtn.setDisable(true);
            inversionEditBtn.setCursor(Cursor.WAIT);
            clearEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/clear.png"));
            clearEditBtn.setDisable(true);
            clearEditBtn.setCursor(Cursor.WAIT);
            closeEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            closeEditBtn.setDisable(true);
            closeEditBtn.setCursor(Cursor.WAIT);
        }else{
            mirrorVerticalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
            mirrorVerticalEditBtn.setDisable(false);
            mirrorVerticalEditBtn.setCursor(Cursor.HAND);
            mirrorHorizontalEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
            mirrorHorizontalEditBtn.setDisable(false);
            mirrorHorizontalEditBtn.setCursor(Cursor.HAND);
            rotateClockwiseEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_90.png"));
            rotateClockwiseEditBtn.setDisable(false);
            rotateClockwiseEditBtn.setCursor(Cursor.HAND);
            rotateAnticlockwiseEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_anti_90.png"));
            rotateAnticlockwiseEditBtn.setDisable(false);
            rotateAnticlockwiseEditBtn.setCursor(Cursor.HAND);
            moveRightEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right.png"));
            moveRightEditBtn.setDisable(false);
            moveRightEditBtn.setCursor(Cursor.HAND);
            moveLeftEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left.png"));
            moveLeftEditBtn.setDisable(false);
            moveLeftEditBtn.setCursor(Cursor.HAND);
            moveUpEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up.png"));
            moveUpEditBtn.setDisable(false);
            moveUpEditBtn.setCursor(Cursor.HAND);
            moveDownEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down.png"));
            moveDownEditBtn.setDisable(false);
            moveDownEditBtn.setCursor(Cursor.HAND);
            moveRight8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right_by_8.png"));
            moveRight8EditBtn.setDisable(false);
            moveRight8EditBtn.setCursor(Cursor.HAND);
            moveLeft8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left_by_8.png"));
            moveLeft8EditBtn.setDisable(false);
            moveLeft8EditBtn.setCursor(Cursor.HAND);
            moveUp8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up_by_8.png"));
            moveUp8EditBtn.setDisable(false);
            moveUp8EditBtn.setCursor(Cursor.HAND);
            moveDown8EditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down_by_8.png"));
            moveDown8EditBtn.setDisable(false);
            moveDown8EditBtn.setCursor(Cursor.HAND);
            tiltRightEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_right.png"));
            tiltRightEditBtn.setDisable(false);
            tiltRightEditBtn.setCursor(Cursor.HAND);
            tiltLeftEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_left.png"));
            tiltLeftEditBtn.setDisable(false);
            tiltLeftEditBtn.setCursor(Cursor.HAND);
            tiltUpEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_up.png"));
            tiltUpEditBtn.setDisable(false);
            tiltUpEditBtn.setCursor(Cursor.HAND);
            tiltDownEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_down.png"));
            tiltDownEditBtn.setDisable(false);
            tiltDownEditBtn.setCursor(Cursor.HAND);
            inversionEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/inversion.png"));
            inversionEditBtn.setDisable(false);
            inversionEditBtn.setCursor(Cursor.HAND);
            clearEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/clear.png"));
            clearEditBtn.setDisable(false);
            clearEditBtn.setCursor(Cursor.HAND);
            closeEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            closeEditBtn.setDisable(false);
            closeEditBtn.setCursor(Cursor.HAND);         
        }        
        //Add the action to Buttons.
        mirrorVerticalEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mirrorVerticalAction();
            }
        });  
        mirrorHorizontalEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mirrorHorizontalAction();
            }
        }); 
        rotateClockwiseEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rotateClockwiseAction();
            }
        }); 
        rotateAnticlockwiseEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rotateAntiClockwiseAction();
            }
        }); 
        moveRightEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                moveRightAction();
            }
        });
        moveLeftEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                moveLeftAction();
            }
        });
        moveUpEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                moveUpAction();
            }
        });
        moveDownEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                moveDownAction();
            }
        });
        moveRight8EditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                moveRight8Action();
            }
        });
        moveLeft8EditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                moveLeft8Action();
            }
        });
        moveUp8EditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                moveUp8Action();
            }
        });
        moveDown8EditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                moveDown8Action();
            }
        });
        tiltRightEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                tiltRightAction();
            }
        });
        tiltLeftEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                tiltLeftAction();
            }
        });
        tiltUpEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                tiltUpAction();
            }
        });
        tiltDownEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                tiltDownAction();
            }
        });        
        inversionEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                inversionAction();
            }
        });
        clearEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                clearAction();
            }
        });
        closeEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                closeAction();
            }
        });
        //Create some Buttons.        
        FlowPane flow = new FlowPane();        
        flow.setPrefWrapLength(objConfiguration.WIDTH*.95);
        flow.getStyleClass().addAll("flow");
        //Add the Buttons to the ToolBar.
        flow.getChildren().addAll(mirrorVerticalEditBtn,mirrorHorizontalEditBtn,rotateClockwiseEditBtn,rotateAnticlockwiseEditBtn,moveRightEditBtn,moveLeftEditBtn,moveUpEditBtn,moveDownEditBtn,moveRight8EditBtn,moveLeft8EditBtn,moveUp8EditBtn,moveDown8EditBtn,tiltRightEditBtn,tiltLeftEditBtn,tiltUpEditBtn,tiltDownEditBtn,inversionEditBtn,clearEditBtn,closeEditBtn);
        flow.setAlignment(Pos.TOP_LEFT);
        toolBar.getItems().addAll(flow);
    }
    private void mirrorVerticalAction(){
        try {               
            lblStatus.setText(objDictionaryAction.getWord("ACTIONVERTICALMIRROR"));
            objWeaveAction.mirrorVertical(objWeave);
            drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
            populateNplot();
            populateDplot();
            Weave objClonedWeave=cloneWeave();
            objUR.doCommand("Mirror Vertical", objClonedWeave);
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Mirror Vertical",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void mirrorHorizontalAction(){        
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONHORIZENTALMIRROR"));
            objWeaveAction.mirrorHorizontal(objWeave);
            drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
            populateNplot();
            populateDplot();
            Weave objClonedWeave=cloneWeave();
            objUR.doCommand("Mirror Horizontal", objClonedWeave);
        } catch (Exception ex) {
          new Logging("SEVERE",getClass().getName(),"Mirror Horizontal",ex);
          lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void rotateClockwiseAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONCLOCKROTATION"));
            objWeaveAction.rotation(objWeave);
            drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
            populateNplot();
            populateDplot();
            Weave objClonedWeave=cloneWeave();
            objUR.doCommand("Rotate Clockwise", objClonedWeave);
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Rotate Clock Wise ",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void rotateAntiClockwiseAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONANTICLOCKROTATION"));
            objWeaveAction.rotationAnti(objWeave);
            drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
            populateNplot();
            populateDplot();Weave objClonedWeave=cloneWeave();
            objUR.doCommand("Rotate Anti-Clockwise", objClonedWeave);
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Anti Rotate Clock Wise ",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void inversionAction(){        
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONINVERSION"));
            objWeaveAction.inversion(objWeave);
            drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
            populateNplot();
            populateDplot();
            Weave objClonedWeave=cloneWeave();
            objUR.doCommand("Invert", objClonedWeave);
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Inversion ",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void clearAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONCLEARWEAVE"));
            objWeaveAction.clear(objWeave);
            drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
            populateNplot();
            populateDplot();                        
            Weave objClonedWeave=cloneWeave();
            objUR.doCommand("Clear", objClonedWeave);
         } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Clear ",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void moveRightAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVERIGHT"));
            objWeaveAction.moveRight(objWeave);
            drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
            populateNplot();
            populateDplot();
            Weave objClonedWeave=cloneWeave();
            objUR.doCommand("Move Right", objClonedWeave);
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Right",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void moveLeftAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVELEFT"));
            objWeaveAction.moveLeft(objWeave);
            drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
            populateNplot();
            populateDplot();
            Weave objClonedWeave=cloneWeave();
            objUR.doCommand("Move Left", objClonedWeave);
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Left",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void moveUpAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVEUP"));
            objWeaveAction.moveUp(objWeave);
            drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
            populateNplot();
            populateDplot();
            Weave objClonedWeave=cloneWeave();
            objUR.doCommand("Move Up", objClonedWeave);
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Up",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void moveDownAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVEDOWN"));
            objWeaveAction.moveDown(objWeave);
            drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
            populateNplot();
            populateDplot();
            Weave objClonedWeave=cloneWeave();
            objUR.doCommand("Move Down", objClonedWeave);
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Down",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void moveRight8Action(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVERIGHT8"));
            objWeaveAction.moveRight8(objWeave);
            drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
            populateNplot();
            populateDplot();
            Weave objClonedWeave=cloneWeave();
            objUR.doCommand("Move Right 8", objClonedWeave);
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Right",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void moveLeft8Action(){        
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVELEFT8"));
            objWeaveAction.moveLeft8(objWeave);
            drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
            populateNplot();
            populateDplot();
            Weave objClonedWeave=cloneWeave();
            objUR.doCommand("Move Left 8", objClonedWeave);
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Left",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void moveUp8Action(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVEUP8"));
            objWeaveAction.moveUp8(objWeave);
            drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
            populateNplot();
            populateDplot();
            Weave objClonedWeave=cloneWeave();
            objUR.doCommand("Move Up 8", objClonedWeave);
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Up",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void moveDown8Action(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONMOVEDOWN8"));
            objWeaveAction.moveDown8(objWeave);
            drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
            populateNplot();
            populateDplot();
            Weave objClonedWeave=cloneWeave();
            objUR.doCommand("Move Down 8", objClonedWeave);
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Move Down",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void tiltRightAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONTILTRIGHT"));
            objWeaveAction.tiltRight(objWeave);
            drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
            populateNplot();
            populateDplot();
            Weave objClonedWeave=cloneWeave();
            objUR.doCommand("Tilt Right", objClonedWeave);
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Tilt Right ",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void tiltLeftAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONTILTLEFT"));
            objWeaveAction.tiltLeft(objWeave);
            drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
            populateNplot();
            populateDplot();
            Weave objClonedWeave=cloneWeave();
            objUR.doCommand("Tilt Left", objClonedWeave);
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Tilt Left ",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void tiltUpAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONTILTUP"));
            objWeaveAction.tiltUp(objWeave);
            drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
            populateNplot();
            populateDplot();
            Weave objClonedWeave=cloneWeave();
            objUR.doCommand("Tilt Up", objClonedWeave);
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Tilt Up ",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void tiltDownAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONTILTDOWN"));
            objWeaveAction.tiltDown(objWeave);
            drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
            populateNplot();
            populateDplot();
            Weave objClonedWeave=cloneWeave();
            objUR.doCommand("Tilt Down", objClonedWeave);
        } catch (Exception ex) {
           new Logging("SEVERE",getClass().getName(),"Tilt Down ",ex);
           lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void closeAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
        selectedMenu = "EDIT";
        transformMenu.show();
        transformMenu.setVisible(false);
        fileMenu.setDisable(false);
        editMenu.setDisable(false);
        viewMenu.setDisable(false);
        utilityMenu.setDisable(false);
        runMenu.setDisable(false);
        transformMenu.setDisable(true);
        menuHighlight();
    }
    /**
    * populateViewToolbar
    * <p>
    * Function use for drawing tool bar for menu item View,
    * and binding events for each tools. 
    * 
    * @exception   (@throws SQLException)
    * @author      Amit Kumar Singh
    * @version     %I%, %G%
    * @since       1.0
    * @see         javafx.event.*;
    * @link        WeaveView
    */ 
    
    private void populateViewToolbarMenu(){
        //For drop down menus
        menuSelected.setTooltip(new Tooltip(objDictionaryAction.getWord("VIEW")));
        
        MenuItem frontSideViewMI = new MenuItem(objDictionaryAction.getWord("FRONTSIDEVIEW"));
        MenuItem rearSideViewMI = new MenuItem(objDictionaryAction.getWord("REARSIDEVIEW"));
        MenuItem frontVisualizationViewMI = new MenuItem(objDictionaryAction.getWord("VISUALIZATIONFRONTVIEW"));
        MenuItem rearVisualizationViewMI = new MenuItem(objDictionaryAction.getWord("VISUALIZATIONREARVIEW"));
        MenuItem flipVisualizationViewMI = new MenuItem(objDictionaryAction.getWord("VISUALIZATIONFLIPVIEW"));
        MenuItem frontCrossSectionViewMI = new MenuItem(objDictionaryAction.getWord("CROSSSECTIONFRONTVIEW"));
        MenuItem rearCrossSectionViewMI = new MenuItem(objDictionaryAction.getWord("CROSSSECTIONREARVIEW"));
        MenuItem gridViewMI = new MenuItem(objDictionaryAction.getWord("GRIDVIEW"));
        MenuItem graphViewMI = new MenuItem(objDictionaryAction.getWord("GRAPHVIEW"));
        MenuItem tilledViewMI = new MenuItem(objDictionaryAction.getWord("TILLEDVIEW"));
        MenuItem simulationViewMI = new MenuItem(objDictionaryAction.getWord("SIMULATION"));
        MenuItem mappingViewMI = new MenuItem(objDictionaryAction.getWord("MAPPING"));
        MenuItem colourwaysViewMI = new MenuItem(objDictionaryAction.getWord("COLOURWAYS"));
        MenuItem zoomInViewMI = new MenuItem(objDictionaryAction.getWord("ZOOMINVIEW"));
        MenuItem normalViewMI = new MenuItem(objDictionaryAction.getWord("ZOOMNORMALVIEW"));
        MenuItem zoomOutViewMI = new MenuItem(objDictionaryAction.getWord("ZOOMOUTVIEW"));
        
        frontSideViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F1, KeyCombination.CONTROL_DOWN));
        rearSideViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F2, KeyCombination.CONTROL_DOWN));
        frontVisualizationViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F4, KeyCombination.CONTROL_DOWN));
        rearVisualizationViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F5, KeyCombination.CONTROL_DOWN));
        flipVisualizationViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F6, KeyCombination.CONTROL_DOWN));
        frontCrossSectionViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F7, KeyCombination.CONTROL_DOWN));
        rearCrossSectionViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F8, KeyCombination.CONTROL_DOWN));
        gridViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F10, KeyCombination.CONTROL_DOWN));
        graphViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F11, KeyCombination.CONTROL_DOWN)); 
        tilledViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F12, KeyCombination.CONTROL_DOWN));
        simulationViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F4, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        mappingViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F2, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        colourwaysViewMI.setAccelerator(new KeyCodeCombination(KeyCode.F1, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        zoomInViewMI.setAccelerator(new KeyCodeCombination(KeyCode.PLUS, KeyCombination.CONTROL_DOWN));
        normalViewMI.setAccelerator(new KeyCodeCombination(KeyCode.ENTER, KeyCombination.CONTROL_DOWN));
        zoomOutViewMI.setAccelerator(new KeyCodeCombination(KeyCode.MINUS, KeyCombination.CONTROL_DOWN));
        
        frontSideViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_view.png"));
        rearSideViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_view.png"));
        frontVisualizationViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_visualization.png"));
        rearVisualizationViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_visualization.png"));
        flipVisualizationViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/flip_visualization.png"));
        frontCrossSectionViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_cut.png"));
        rearCrossSectionViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_cut.png"));
        gridViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/grid_view.png"));
        graphViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/graph_view.png"));
        tilledViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tiled_view.png"));
        simulationViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/simulation.png"));
        mappingViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/mapping_view.png"));
        colourwaysViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/colourways.png"));
        zoomInViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
        normalViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
        zoomOutViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
     
        //enable disable conditions
        if(!isWorkingMode){
            frontSideViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/front_view.png"));
            frontSideViewMI.setDisable(true);
            rearSideViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/rear_view.png"));
            rearSideViewMI.setDisable(true);
            frontVisualizationViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/front_visualization.png"));
            frontVisualizationViewMI.setDisable(true);
            rearVisualizationViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/rear_visualization.png"));
            rearVisualizationViewMI.setDisable(true);
            flipVisualizationViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/flip_visualization.png"));
            flipVisualizationViewMI.setDisable(true);
            frontCrossSectionViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/front_cut.png"));
            frontCrossSectionViewMI.setDisable(true);
            rearCrossSectionViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/rear_cut.png"));
            rearCrossSectionViewMI.setDisable(true);
            gridViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/grid_view.png"));
            gridViewMI.setDisable(true);
            graphViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/graph_view.png"));
            graphViewMI.setDisable(true);
            tilledViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/tiled_view.png"));
            tilledViewMI.setDisable(true);
            simulationViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/simulation.png"));
            simulationViewMI.setDisable(true);
            mappingViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/mapping_view.png"));
            mappingViewMI.setDisable(true);
            colourwaysViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/colourways.png"));
            colourwaysViewMI.setDisable(true);
            zoomInViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
            zoomInViewMI.setDisable(true);
            normalViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
            normalViewMI.setDisable(true);
            zoomOutViewMI.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
            zoomOutViewMI.setDisable(true);              
        }else{
            frontSideViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_view.png"));
            frontSideViewMI.setDisable(false);
            rearSideViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_view.png"));
            rearSideViewMI.setDisable(false);
            frontVisualizationViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_visualization.png"));
            frontVisualizationViewMI.setDisable(false);
            rearVisualizationViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_visualization.png"));
            rearVisualizationViewMI.setDisable(false);
            flipVisualizationViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/flip_visualization.png"));
            flipVisualizationViewMI.setDisable(false);          
            frontCrossSectionViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_cut.png"));
            frontCrossSectionViewMI.setDisable(false);
            rearCrossSectionViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_cut.png"));
            rearCrossSectionViewMI.setDisable(false);
            gridViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/grid_view.png"));
            gridViewMI.setDisable(false);
            graphViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/graph_view.png"));
            graphViewMI.setDisable(false);
            tilledViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tiled_view.png"));
            tilledViewMI.setDisable(false);
            simulationViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/simulation.png"));
            simulationViewMI.setDisable(false);
            mappingViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/mapping_view.png"));
            mappingViewMI.setDisable(false);
            colourwaysViewMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/colourways.png"));
            colourwaysViewMI.setDisable(false);
            zoomInViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
            zoomInViewMI.setDisable(false);
            normalViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
            normalViewMI.setDisable(false);
            zoomOutViewMI.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
            zoomOutViewMI.setDisable(false);
        }
        //Add the action to Buttons.
        frontSideViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                frontSideViewAction();
            }
        });
        rearSideViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                rearSideViewAction();
            }
        });
        frontVisualizationViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                frontVisualizationViewAction();
            }
        });
        rearVisualizationViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                rearVisualizationViewAction();
            }
        });
        flipVisualizationViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                flipVisualizationViewAction();
            }
        });
        frontCrossSectionViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                frontCutViewAction();
            }
        });
        rearCrossSectionViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                rearCutViewAction();
            }
        });
        gridViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                gridViewAction();
            }
        });
        graphViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                graphViewAction();
            }
        });
        tilledViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                tilledViewAction();
            }
        });
        simulationViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                simulationViewAction();
            }
        }); 
        mappingViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                mappingViewAction();
            }
        });
        colourwaysViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                colourwaysAction();
            }
        });
        zoomInViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                zoomInAction();
            }
        });
        normalViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                ZoomNormalAction();
            }
        });
        zoomOutViewMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                zoomOutAction();
            }
        });
        //mappingViewMI,simulationViewMI
        menuSelected.getItems().addAll(frontSideViewMI,rearSideViewMI,frontVisualizationViewMI,rearVisualizationViewMI,gridViewMI,tilledViewMI, mappingViewMI,colourwaysViewMI,new SeparatorMenuItem(), zoomInViewMI,normalViewMI,zoomOutViewMI);
    }    
    private void populateViewToolbar(){
        // Front Texture View item;
        Button frontSideViewBtn = new Button();
        frontSideViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_view.png"));
        frontSideViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("FRONTSIDEVIEW")+" (Ctrl+F1)\n"+objDictionaryAction.getWord("TOOLTIPFRONTSIDEVIEW")));
        frontSideViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        frontSideViewBtn.getStyleClass().add("toolbar-button");    
        // Rear Texture View item
        Button rearSideViewBtn = new Button();
        rearSideViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_view.png"));
        rearSideViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("REARSIDEVIEW")+" (Ctrl+F2)\n"+objDictionaryAction.getWord("TOOLTIPREARSIDEVIEW")));
        rearSideViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rearSideViewBtn.getStyleClass().add("toolbar-button");    
        // Front Visulization View item;
        Button frontVisualizationViewBtn = new Button();
        frontVisualizationViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_visualization.png"));
        frontVisualizationViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("VISUALIZATIONFRONTVIEW")+" (Ctrl+F4)\n"+objDictionaryAction.getWord("TOOLTIPVISUALIZATIONFRONTVIEW")));
        frontVisualizationViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        frontVisualizationViewBtn.getStyleClass().add("toolbar-button");    
        // Rear Visaulization View item;
        Button rearVisualizationViewBtn = new Button();
        rearVisualizationViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_visualization.png"));
        rearVisualizationViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("VISUALIZATIONREARVIEW")+" (Ctrl+F5)\n"+objDictionaryAction.getWord("TOOLTIPVISUALIZATIONREARVIEW")));
        rearVisualizationViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rearVisualizationViewBtn.getStyleClass().add("toolbar-button");    
        // Switch Side View item;
        Button flipVisualizationViewBtn = new Button();
        flipVisualizationViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/flip_visualization.png"));
        flipVisualizationViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("VISUALIZATIONFLIPVIEW")+" (Ctrl+F6)\n"+objDictionaryAction.getWord("TOOLTIPVISUALIZATIONFLIPVIEW")));
        flipVisualizationViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        flipVisualizationViewBtn.getStyleClass().add("toolbar-button");    
        // cross Section View item;
        Button frontCrossSectionViewBtn = new Button();
        frontCrossSectionViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_cut.png"));
        frontCrossSectionViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CROSSSECTIONFRONTVIEW")+" (Ctrl+F7)\n"+objDictionaryAction.getWord("TOOLTIPCROSSSECTIONFRONTVIEW")));
        frontCrossSectionViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        frontCrossSectionViewBtn.getStyleClass().add("toolbar-button");    
        // cross Section View item;
        Button rearCrossSectionViewBtn = new Button();
        rearCrossSectionViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_cut.png"));
        rearCrossSectionViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CROSSSECTIONREARVIEW")+" (Ctrl+F8)\n"+objDictionaryAction.getWord("TOOLTIPCROSSSECTIONREARVIEW")));
        rearCrossSectionViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rearCrossSectionViewBtn.getStyleClass().add("toolbar-button");    
        // Grid View item
        Button gridViewBtn = new Button();
        gridViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/grid_view.png"));
        gridViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("GRIDVIEW")+" (Ctrl+F10)\n"+objDictionaryAction.getWord("TOOLTIPGRIDVIEW")));
        gridViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        gridViewBtn.getStyleClass().add("toolbar-button");    
        // Graph View item
        Button graphViewBtn = new Button();
        graphViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/graph_view.png"));
        graphViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("GRAPHVIEW")+" (Ctrl+F11)\n"+objDictionaryAction.getWord("TOOLTIPGRAPHVIEW")));
        graphViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        graphViewBtn.getStyleClass().add("toolbar-button");    
        // Tiled View
        Button tilledViewBtn = new Button();
        tilledViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tiled_view.png"));
        tilledViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("TILLEDVIEW")+" (Ctrl+F12)\n"+objDictionaryAction.getWord("TOOLTIPTILLEDVIEW")));
        tilledViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tilledViewBtn.getStyleClass().add("toolbar-button");
        // Simulation View menu
        Button simulationViewBtn = new Button();
        simulationViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/simulation.png"));
        simulationViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("SIMULATION")+" (Ctrl+Shift+F4)\n"+objDictionaryAction.getWord("TOOLTIPSIMULATION")));
        simulationViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        simulationViewBtn.getStyleClass().add("toolbar-button");    
        // mapping View menu
        Button mappingViewBtn = new Button();
        mappingViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/mapping_view.png"));
        mappingViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("MAPPING")+" (Ctrl+Shift+F2)\n"+objDictionaryAction.getWord("TOOLTIPMAPPING")));
        mappingViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        mappingViewBtn.getStyleClass().add("toolbar-button");    
        // colour ways
        Button colourwaysViewBtn = new Button();
        colourwaysViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/colourways.png"));
        colourwaysViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("COLOURWAYS")+" (Ctrl+Shift+F1)\n"+objDictionaryAction.getWord("TOOLTIPCOLOURWAYS")));
        colourwaysViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        colourwaysViewBtn.getStyleClass().add("toolbar-button");
        // Zoom-In item
        Button zoomInViewBtn = new Button();
        zoomInViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
        zoomInViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ZOOMINVIEW")+" (Ctrl+Plus)\n"+objDictionaryAction.getWord("TOOLTIPZOOMINVIEW")));
        zoomInViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        zoomInViewBtn.getStyleClass().add("toolbar-button");    
        // Normal item
        Button normalViewBtn = new Button();
        normalViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
        normalViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ZOOMNORMALVIEW")+" (Ctrl+Enter)\n"+objDictionaryAction.getWord("TOOLTIPZOOMNORMALVIEW")));
        normalViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        normalViewBtn.getStyleClass().add("toolbar-button");    
        // Zoom-Out item
        Button zoomOutViewBtn = new Button();
        zoomOutViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
        zoomOutViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ZOOMOUTVIEW")+" (Ctrl+Minus)\n"+objDictionaryAction.getWord("TOOLTIPZOOMOUTVIEW")));
        zoomOutViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        zoomOutViewBtn.getStyleClass().add("toolbar-button");    
        // Denting
        Button dentingViewBtn=new Button();
        dentingViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/thread_pattern.png"));
        dentingViewBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("DENTINGPANE")));
        dentingViewBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        dentingViewBtn.getStyleClass().add("toolbar-button");
        //enable disable conditions
        if(!isWorkingMode){
            frontSideViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/front_view.png"));
            frontSideViewBtn.setDisable(true);
            frontSideViewBtn.setCursor(Cursor.WAIT);
            rearSideViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/rear_view.png"));
            rearSideViewBtn.setDisable(true);
            rearSideViewBtn.setCursor(Cursor.WAIT);
            frontVisualizationViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/front_visualization.png"));
            frontVisualizationViewBtn.setDisable(true);
            frontVisualizationViewBtn.setCursor(Cursor.WAIT);
            rearVisualizationViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/rear_visualization.png"));
            rearVisualizationViewBtn.setDisable(true);
            rearVisualizationViewBtn.setCursor(Cursor.WAIT);
            flipVisualizationViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/flip_visualization.png"));
            flipVisualizationViewBtn.setDisable(true);
            flipVisualizationViewBtn.setCursor(Cursor.WAIT);
            frontCrossSectionViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/front_cut.png"));
            frontCrossSectionViewBtn.setDisable(true);
            frontCrossSectionViewBtn.setCursor(Cursor.WAIT);
            rearCrossSectionViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/rear_cut.png"));
            rearCrossSectionViewBtn.setDisable(true);
            rearCrossSectionViewBtn.setCursor(Cursor.WAIT);
            gridViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/grid_view.png"));
            gridViewBtn.setDisable(true);
            gridViewBtn.setCursor(Cursor.WAIT);
            graphViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/graph_view.png"));
            graphViewBtn.setDisable(true);
            graphViewBtn.setCursor(Cursor.WAIT);
            tilledViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/tiled_view.png"));
            tilledViewBtn.setDisable(true);
            tilledViewBtn.setCursor(Cursor.WAIT);
            simulationViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/simulation.png"));
            simulationViewBtn.setDisable(true);
            simulationViewBtn.setCursor(Cursor.WAIT);
            mappingViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/mapping_view.png"));
            mappingViewBtn.setDisable(true);
            mappingViewBtn.setCursor(Cursor.WAIT);
            colourwaysViewBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/colourways.png"));
            colourwaysViewBtn.setDisable(true);
            colourwaysViewBtn.setCursor(Cursor.WAIT);
            zoomInViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
            zoomInViewBtn.setDisable(true);
            zoomInViewBtn.setCursor(Cursor.WAIT);
            normalViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
            normalViewBtn.setDisable(true);
            normalViewBtn.setCursor(Cursor.WAIT);
            zoomOutViewBtn.setGraphic( new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
            zoomOutViewBtn.setDisable(true);
            zoomOutViewBtn.setCursor(Cursor.WAIT);                
        }else{
            frontSideViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_view.png"));
            frontSideViewBtn.setDisable(false);
            frontSideViewBtn.setCursor(Cursor.HAND);
            rearSideViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_view.png"));
            rearSideViewBtn.setDisable(false);
            rearSideViewBtn.setCursor(Cursor.HAND);
            frontVisualizationViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_visualization.png"));
            frontVisualizationViewBtn.setDisable(false);
            frontVisualizationViewBtn.setCursor(Cursor.HAND);
            rearVisualizationViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_visualization.png"));
            rearVisualizationViewBtn.setDisable(false);
            rearVisualizationViewBtn.setCursor(Cursor.HAND);
            flipVisualizationViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/flip_visualization.png"));
            flipVisualizationViewBtn.setDisable(false);
            flipVisualizationViewBtn.setCursor(Cursor.HAND);            
            frontCrossSectionViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/front_cut.png"));
            frontCrossSectionViewBtn.setDisable(false);
            frontCrossSectionViewBtn.setCursor(Cursor.HAND);
            rearCrossSectionViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rear_cut.png"));
            rearCrossSectionViewBtn.setDisable(false);
            rearCrossSectionViewBtn.setCursor(Cursor.HAND);
            gridViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/grid_view.png"));
            gridViewBtn.setDisable(false);
            gridViewBtn.setCursor(Cursor.HAND); 
            graphViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/graph_view.png"));
            graphViewBtn.setDisable(false);
            graphViewBtn.setCursor(Cursor.HAND);
            tilledViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tiled_view.png"));
            tilledViewBtn.setDisable(false);
            tilledViewBtn.setCursor(Cursor.HAND); 
            simulationViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/simulation.png"));
            simulationViewBtn.setDisable(false);
            simulationViewBtn.setCursor(Cursor.HAND);
            mappingViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/mapping_view.png"));
            mappingViewBtn.setDisable(false);
            mappingViewBtn.setCursor(Cursor.HAND);
            colourwaysViewBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/colourways.png"));
            colourwaysViewBtn.setDisable(false);
            colourwaysViewBtn.setCursor(Cursor.HAND);
            zoomInViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
            zoomInViewBtn.setDisable(false);
            zoomInViewBtn.setCursor(Cursor.HAND); 
            normalViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
            normalViewBtn.setDisable(false);
            normalViewBtn.setCursor(Cursor.HAND); 
            zoomOutViewBtn.setGraphic( new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
            zoomOutViewBtn.setDisable(false);
            zoomOutViewBtn.setCursor(Cursor.HAND); 
        }
        //Add the action to Buttons.
        frontSideViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                frontSideViewAction();
            }
        });
        rearSideViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rearSideViewAction();
            }
        });
        frontVisualizationViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                frontVisualizationViewAction();
            }
        });
        rearVisualizationViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rearVisualizationViewAction();
            }
        });
        flipVisualizationViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                flipVisualizationViewAction();
            }
        });
        frontCrossSectionViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                frontCutViewAction();
            }
        });
        rearCrossSectionViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rearCutViewAction();
            }
        });
        gridViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                gridViewAction();
            }
        });
        graphViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                graphViewAction();
            }
        });
        tilledViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                tilledViewAction();
            }
        });
        simulationViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                simulationViewAction();
            }
        }); 
        mappingViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mappingViewAction();
            }
        });
        colourwaysViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                colourwaysAction();
            }
        });
        zoomInViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                zoomInAction();
            }
        });
        normalViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                ZoomNormalAction();
            }
        });
        zoomOutViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                zoomOutAction();
            }
        });
        dentingViewBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                dentingViewAction();
            }
        });
        //Create some Buttons.        
        FlowPane flow = new FlowPane();        
        flow.setPrefWrapLength(objConfiguration.WIDTH*.95);
        flow.getStyleClass().addAll("flow"); 
        //Add the Buttons to the ToolBar.
        //, dentingViewBtn,simulationViewBtn
        flow.getChildren().addAll(frontSideViewBtn,rearSideViewBtn,frontVisualizationViewBtn,rearVisualizationViewBtn,gridViewBtn,tilledViewBtn,mappingViewBtn,colourwaysViewBtn,zoomInViewBtn,normalViewBtn,zoomOutViewBtn);
        flow.setAlignment(Pos.TOP_LEFT);
        toolBar.getItems().addAll(flow);        
    }
    /**
     * frontSideViewAction
     * <p>
     * Function use for plotting front View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */    
    private void frontSideViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONFRONTSIDEVIEW"));
        try{    
            objWeaveAction = new WeaveAction();
            //objWeaveAction.populateDenting(objWeave);
            int intHeight = (int)(objConfiguration.HEIGHT/objWeave.getIntWeft())*objWeave.getIntWeft();
            int intLength = (int)(objConfiguration.WIDTH/objWeave.getIntWarp())*objWeave.getIntWarp();

            BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objWeaveAction = new WeaveAction();
            bufferedImage = objWeaveAction.plotFrontSideView(objWeave, objWeave.getIntWarp(), objWeave.getIntWeft(), intLength, intHeight);
            
            intLength = (int)(((objWeave.getObjConfiguration().getIntDPI()*objConfiguration.WIDTH)/objWeave.getIntEPI()));
            intHeight = (int)(((objWeave.getObjConfiguration().getIntDPI()*objConfiguration.HEIGHT)/objWeave.getIntPPI()));
            intHeight=(int)(intHeight*(objWeave.getObjConfiguration().WIDTH/intLength));
            intLength=(int)objWeave.getObjConfiguration().WIDTH;
            intHeight = (int)(intHeight/objWeave.getIntWeft())*objWeave.getIntWeft();
            intLength = (int)(intLength/objWeave.getIntWarp())*objWeave.getIntWarp();

            /*BufferedImage bufferedImageesize = new BufferedImage((int)(intLength), (int)(intHeight),BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bufferedImageesize.createGraphics();
            g.drawImage(bufferedImage, 0, 0, (int)(intLength), (int)(intHeight), null);
            g.dispose(); */
            
            plotViewAction(bufferedImage);
            //bufferedImageesize = null;
            bufferedImage = null;
            System.gc();
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"frontSideViewAction() : Error while viewing composite view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    /**
     * rearSideViewAction
     * <p>
     * Function use for plotting rear View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */    
    private void rearSideViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONREARSIDEVIEW"));
        try{    
            objWeaveAction = new WeaveAction();
            //objWeaveAction.populateDenting(objWeave);
            int intHeight = (int)(objConfiguration.HEIGHT/objWeave.getIntWeft())*objWeave.getIntWeft();
            int intLength = (int)(objConfiguration.WIDTH/objWeave.getIntWarp())*objWeave.getIntWarp();
          
            BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objWeaveAction = new WeaveAction();
            bufferedImage = objWeaveAction.plotRearSideView(objWeave, objWeave.getIntWarp(), objWeave.getIntWeft(), intLength, intHeight);
            
            intLength = (int)(((objWeave.getObjConfiguration().getIntDPI()*objConfiguration.WIDTH)/objWeave.getIntEPI()));
            intHeight = (int)(((objWeave.getObjConfiguration().getIntDPI()*objConfiguration.HEIGHT)/objWeave.getIntPPI()));
            intHeight=(int)(intHeight*(objWeave.getObjConfiguration().WIDTH/intLength));
            intLength=(int)objWeave.getObjConfiguration().WIDTH;
            intHeight = (int)(intHeight/objWeave.getIntWeft())*objWeave.getIntWeft();
            intLength = (int)(intLength/objWeave.getIntWarp())*objWeave.getIntWarp();
            
            /*BufferedImage bufferedImageesize = new BufferedImage((int)(intLength), (int)(intHeight),BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bufferedImageesize.createGraphics();
            g.drawImage(bufferedImage, 0, 0, (int)(intLength), (int)(intHeight), null);
            g.dispose(); */
            
            plotViewAction(bufferedImage);
            //bufferedImageesize = null;
            bufferedImage = null;
            System.gc();
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"frontSideViewAction() : Error while viewing composite view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    /**
     * frontVisualizationViewAction
     * <p>
     * Function use for plotting front visualization View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void frontVisualizationViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONVISUALIZATIONFRONTVIEW"));
        try{    
            int intHeight = (int)(objConfiguration.HEIGHT/3);
            int intLength = (int)(objConfiguration.WIDTH/3);
            
            BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objWeaveAction = new WeaveAction();
            bufferedImage = objWeaveAction.plotVisualizationFrontView(objWeave, objWeave.getIntWarp(), objWeave.getIntWeft(), intLength, intHeight);
            
            intLength = (int)(((objWeave.getObjConfiguration().getIntDPI()*objConfiguration.WIDTH)/objWeave.getIntEPI()));
            intHeight = (int)(((objWeave.getObjConfiguration().getIntDPI()*objConfiguration.HEIGHT)/objWeave.getIntPPI()));
            intHeight=(int)(intHeight*(objWeave.getObjConfiguration().WIDTH/intLength));
            intLength=(int)objWeave.getObjConfiguration().WIDTH;
            intHeight = (int)(intHeight/objWeave.getIntWeft())*objWeave.getIntWeft();
            intLength = (int)(intLength/objWeave.getIntWarp())*objWeave.getIntWarp();
            //intHeight/=3;
            //intLength/=3;
            
            /*BufferedImage bufferedImageesize = new BufferedImage((int)(intLength), (int)(intHeight),BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bufferedImageesize.createGraphics();
            g.drawImage(bufferedImage, 0, 0, (int)(intLength), (int)(intHeight), null);
            g.dispose(); */
            
            plotViewAction(bufferedImage);
            //bufferedImageesize = null;
            bufferedImage = null;
            System.gc();
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"frontVisualizationViewAction() : Error while viewing front view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    /**
     * rearVisualizationViewAction
     * <p>
     * Function use for plotting rear visualization View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void rearVisualizationViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONVISUALIZATIONREARVIEW"));
        try{
            int intHeight = (int)(objConfiguration.HEIGHT/3);
            int intLength = (int)(objConfiguration.WIDTH/3);
            
            BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objWeaveAction = new WeaveAction();
            bufferedImage = objWeaveAction.plotVisualizationRearView(objWeave, objWeave.getIntWarp(), objWeave.getIntWeft(), intLength, intHeight);
            
            intLength = (int)(((objWeave.getObjConfiguration().getIntDPI()*objConfiguration.WIDTH)/objWeave.getIntEPI()));
            intHeight = (int)(((objWeave.getObjConfiguration().getIntDPI()*objConfiguration.HEIGHT)/objWeave.getIntPPI()));
            intHeight=(int)(intHeight*(objWeave.getObjConfiguration().WIDTH/intLength));
            intLength=(int)objWeave.getObjConfiguration().WIDTH;
            intHeight = (int)(intHeight/objWeave.getIntWeft())*objWeave.getIntWeft();
            intLength = (int)(intLength/objWeave.getIntWarp())*objWeave.getIntWarp();            
            //intHeight/=3;
            //intLength/=3;
            
            /*BufferedImage bufferedImageesize = new BufferedImage((int)(intLength), (int)(intHeight),BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bufferedImageesize.createGraphics();
            g.drawImage(bufferedImage, 0, 0, (int)(intLength), (int)(intHeight), null);
            g.dispose(); */
            
            plotViewAction(bufferedImage);
            //bufferedImageesize = null;
            bufferedImage = null;
            System.gc();
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"rearVisualizationViewAction() : Error while viewing back view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    /**
     * flipVisualizationViewAction
     * <p>
     * Function use for plotting flip visualization View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void flipVisualizationViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONVISUALIZATIONFLIPVIEW"));
        
    }
    /**
     * frontCutViewAction
     * <p>
     * Function use for plotting front cut View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void frontCutViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCROSSSECTIONFRONTVIEW"));
        
    }
    /**
     * rearCutViewAction
     * <p>
     * Function use for plotting rear cut View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void rearCutViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCROSSSECTIONREARVIEW"));
        
    }
    /**
     * gridViewAction
     * <p>
     * Function use for plotting grid View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void gridViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONGRIDVIEW"));
        try{    
            lblStatus.setText(objDictionaryAction.getWord("ACTIONGRIDVIEW"));        
            int intHeight = (int)(objConfiguration.HEIGHT/boxSize);
            int intLength = (int)(objConfiguration.WIDTH/boxSize);

            BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objWeaveAction = new WeaveAction();
            bufferedImage = objWeaveAction.plotFrontSideView(objWeave, objWeave.getIntWarp(), objWeave.getIntWeft(), intLength, intHeight);

            BufferedImage bufferedImageesize = new BufferedImage((int)(intLength*boxSize), (int)(intHeight*boxSize),BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bufferedImageesize.createGraphics();
            g.drawImage(bufferedImage, 0, 0, (int)(intLength*boxSize), (int)(intHeight*boxSize), null);
            g.setColor(java.awt.Color.BLACK);
            BasicStroke bs = new BasicStroke(1);
            g.setStroke(bs);

            for(int i = 0; i < intHeight; i++) {
                for(int j = 0; j < intLength; j++) {
                    if((j%boxSize)==0){
                        bs = new BasicStroke(1);
                         g.setStroke(bs);
                    }else{
                        bs = new BasicStroke(1);
                        g.setStroke(bs);
                    }
                    g.drawLine((int)(j*boxSize), 0,  (int)(j*boxSize), (int)(intHeight*boxSize));
                }
                if((i%boxSize)==0){
                    bs = new BasicStroke(1);
                    g.setStroke(bs);
                }else{
                    bs = new BasicStroke(1);
                    g.setStroke(bs);
                }
                g.drawLine(0, (int)(i*boxSize), (int)(intLength*boxSize), (int)(i*boxSize));
            }
            plotViewAction(bufferedImageesize);
            bufferedImage = null;
            System.gc();
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"gridViewAction() : Error while viewing grid view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    /**
     * graphViewAction
     * <p>
     * Function use for plotting graph View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void graphViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONGRAPHVIEW"));
        try{    
            lblStatus.setText(objDictionaryAction.getWord("ACTIONGRAPHVIEW"));
            int intHeight = (int)(objConfiguration.HEIGHT/boxSize);
            int intLength = (int)(objConfiguration.WIDTH/boxSize);

            BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objWeaveAction = new WeaveAction();
            bufferedImage = objWeaveAction.plotFrontSideView(objWeave, objWeave.getIntWarp(), objWeave.getIntWeft(), intLength, intHeight);

            BufferedImage bufferedImageesize = new BufferedImage((int)(intLength*boxSize), (int)(intHeight*boxSize),BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bufferedImageesize.createGraphics();
            g.drawImage(bufferedImage, 0, 0, (int)(intLength*boxSize), (int)(intHeight*boxSize), null);
            g.setColor(java.awt.Color.BLACK);
            BasicStroke bs = new BasicStroke(2);
            g.setStroke(bs);

            for(int i = 0; i < intHeight; i++) {
                for(int j = 0; j < intLength; j++) {
                    if((j%boxSize)==0){
                        bs = new BasicStroke(2);
                         g.setStroke(bs);
                    }else{
                        bs = new BasicStroke(1);
                        g.setStroke(bs);
                    }
                    g.drawLine((int)(j*boxSize), 0,  (int)(j*boxSize), (int)(intHeight*boxSize));
                }
                if((i%boxSize)==0){
                    bs = new BasicStroke(2);
                    g.setStroke(bs);
                }else{
                    bs = new BasicStroke(1);
                    g.setStroke(bs);
                }
                g.drawLine(0, (int)(i*boxSize), (int)(intLength*boxSize), (int)(i*boxSize));
            }
            plotViewAction(bufferedImageesize);
            bufferedImage = null;
            System.gc();
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"graphViewAction() : Error while viewing graph view",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    /**
     * tilledViewAction
     * <p>
     * Function use for plotting tilled View,
     * 
     * @exception   (@throws SQLException)
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        WeaveView
     */
    private void tilledViewAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONTILLEDVIEW"));
        GridPane popup=new GridPane();
        int height=(int)(objConfiguration.HEIGHT/boxSize);
        int width=(int)(objConfiguration.WIDTH/boxSize);
        byte[][] tilledMatrix = new byte[height][width];
        for(int x = 0; x < height; x++) {
            for(int y = 0; y < width; y++) {
                if(x>=objWeave.getIntWeft() && y<objWeave.getIntWarp()){
                     tilledMatrix[x][y] = objWeave.getDesignMatrix()[x%objWeave.getIntWeft()][y];  
                }else if(x<objWeave.getIntWeft() && y>=objWeave.getIntWarp()){
                     tilledMatrix[x][y] = objWeave.getDesignMatrix()[x][y%objWeave.getIntWarp()];  
                }else if(x>=objWeave.getIntWeft() && y>=objWeave.getIntWarp()){
                     tilledMatrix[x][y] = objWeave.getDesignMatrix()[x%objWeave.getIntWeft()][y%objWeave.getIntWarp()];  
                }else{
                     tilledMatrix[x][y] = objWeave.getDesignMatrix()[x][y]; 
                }
                Label lblbox = new Label("");
                lblbox.setPrefSize(boxSize,boxSize);
                if(tilledMatrix[x][y]==1 )
                    lblbox.setStyle("-fx-background-color: #000000;");
                else
                    lblbox.setStyle("-fx-background-color: #ffffff; -fx-border-width: 1;  -fx-border-color: black;");               
                popup.add(lblbox,y,x);
            }
        }   
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        popup.setId("popup");
        Scene scene = new Scene(popup);
        scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        dialogStage.setScene(scene);
        dialogStage.setX(-5);
        dialogStage.setY(0);
        dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("ACTIONTILLEDVIEW"));
        dialogStage.showAndWait();    
        
        tilledMatrix=null;
        System.gc();
    }
    private void plotViewAction(final BufferedImage objBufferedImage){
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);        
        BorderPane popup = new BorderPane();
        Scene popupScene = new Scene(popup);
        final ImageView weaveIV=new ImageView();        
        
        //method local inner calss to show image as rster image
        class MyBufferImage{
            double scaleFactor = 1;
            public void loadImage(){
                BufferedImage bufferedImageResize = new BufferedImage((int)(objBufferedImage.getWidth()*scaleFactor), (int)(objBufferedImage.getHeight()*scaleFactor), BufferedImage.TYPE_INT_RGB);
                Graphics2D g = bufferedImageResize.createGraphics();
                g.drawImage(objBufferedImage, 0, 0, (int)(objBufferedImage.getWidth()*scaleFactor), (int)(objBufferedImage.getHeight()*scaleFactor), null);
                g.dispose();
                if(scaleFactor>=1)
                    weaveIV.setImage(SwingFXUtils.toFXImage(bufferedImageResize.getSubimage(0, 0, (int)(objBufferedImage.getWidth()), (int)(objBufferedImage.getHeight())), null));  
                else
                    weaveIV.setImage(SwingFXUtils.toFXImage(bufferedImageResize, null));  
                bufferedImageResize = null;                
            }            
        }
        final MyBufferImage objMyBufferImage = new MyBufferImage();
        objMyBufferImage.loadImage();
        /*
        BufferedImage bufferedImageResize = new BufferedImage((int)objConfiguration.WIDTH, (int)objConfiguration.HEIGHT-50, BufferedImage.TYPE_INT_RGB);
        for(int i=0;i<objConfiguration.WIDTH;i++)
            for(int j=0;j<objConfiguration.HEIGHT-50;j++)
                bufferedImageResize.setRGB(i, j, objBufferedImage.getRGB(i%objBufferedImage.getWidth(), j%objBufferedImage.getHeight()));
        weaveIV.setImage(SwingFXUtils.toFXImage(bufferedImageResize, null));  
        bufferedImageResize = null;
        */
        //weaveIV.setFitHeight(objConfiguration.HEIGHT-50);
        //weaveIV.setFitWidth(objConfiguration.WIDTH);
        popup.setCenter(weaveIV);
        popup.setId("popup");       
        
        //For zoom Out weaving pattern
        final KeyCombination zoomOutKC = new KeyCodeCombination(KeyCode.SUBTRACT,KeyCombination.ALT_DOWN);
        //For undo weaving pattern
        final KeyCombination zoomInKC = new KeyCodeCombination(KeyCode.ADD,KeyCombination.ALT_DOWN);
        //For redo weaving pattern
        final KeyCombination normalKC = new KeyCodeCombination(KeyCode.EQUALS,KeyCombination.ALT_DOWN);
        //For redo weaving pattern
        final KeyCombination closeKC = new KeyCodeCombination(KeyCode.ESCAPE,KeyCombination.ALT_DOWN);

        popupScene.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler() {
            @Override
            public void handle(Event t) {
                if (closeKC.match((KeyEvent) t)){
                    dialogStage.close();
                } else if (normalKC.match((KeyEvent) t)){
                    objMyBufferImage.scaleFactor = 1;
                    objMyBufferImage.loadImage();
                    /*weaveIV.setScaleX(1);
                    weaveIV.setScaleY(1);
                    weaveIV.setScaleZ(1);*/
                } else if (zoomInKC.match((KeyEvent) t)){
                    objMyBufferImage.scaleFactor += .5;
                    objMyBufferImage.loadImage();
                    /*weaveIV.setScaleX(weaveIV.getScaleX()*2);
                    weaveIV.setScaleY(weaveIV.getScaleY()*2);
                    weaveIV.setScaleZ(weaveIV.getScaleZ()*2);*/
                } else if (zoomOutKC.match((KeyEvent) t)){
                    objMyBufferImage.scaleFactor -= .5;
                    if((int)(objConfiguration.WIDTH*objMyBufferImage.scaleFactor)>0 && (int)((objConfiguration.HEIGHT-50)*objMyBufferImage.scaleFactor)>0)
                        objMyBufferImage.loadImage();
                    else
                       objMyBufferImage.scaleFactor += .5;
                    /*weaveIV.setScaleX(weaveIV.getScaleX()/2);
                    weaveIV.setScaleY(weaveIV.getScaleY()/2);
                    weaveIV.setScaleZ(weaveIV.getScaleZ()/2);*/
                }
            }
        });
        popupScene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                KeyCode key = t.getCode();
                if (key == KeyCode.ESCAPE){
                    dialogStage.close();
                } else if (key == KeyCode.ENTER){
                    objMyBufferImage.scaleFactor = 1;
                    objMyBufferImage.loadImage();
                    /*weaveIV.setScaleX(1);
                    weaveIV.setScaleY(1);
                    weaveIV.setScaleZ(1);*/
                } else if (key == KeyCode.UP){
                    objMyBufferImage.scaleFactor += .5;
                    objMyBufferImage.loadImage();
                    /*weaveIV.setScaleX(weaveIV.getScaleX()*2);
                    weaveIV.setScaleY(weaveIV.getScaleY()*2);
                    weaveIV.setScaleZ(weaveIV.getScaleZ()*2);*/
                } else if (key == KeyCode.DOWN){
                    objMyBufferImage.scaleFactor -= .5;
                    if((int)(objConfiguration.WIDTH*objMyBufferImage.scaleFactor)>0 && (int)((objConfiguration.HEIGHT-50)*objMyBufferImage.scaleFactor)>0)
                        objMyBufferImage.loadImage();
                    else
                       objMyBufferImage.scaleFactor += .5; 
                    /*weaveIV.setScaleX(weaveIV.getScaleX()/2);
                    weaveIV.setScaleY(weaveIV.getScaleY()/2);
                    weaveIV.setScaleZ(weaveIV.getScaleZ()/2);*/
                }
            }
        });
        popupScene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        dialogStage.setScene(popupScene);
        dialogStage.setX(-5);
        dialogStage.setY(0);
        dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("VIEW"));
        dialogStage.showAndWait();
    }
    private void simulationViewAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONSIMULATION"));
            int intHeight = (int)objWeave.getObjConfiguration().HEIGHT;
            int intLength = (int)objWeave.getObjConfiguration().WIDTH;
            BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objWeaveAction = new WeaveAction();
            bufferedImage = objWeaveAction.plotFrontSideView(objWeave, objWeave.getIntWarp(), objWeave.getIntWeft(), intLength, intHeight);
            SimulatorEditView objBaseSimulationView = new SimulatorEditView(objConfiguration, bufferedImage);                    
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void mappingViewAction(){
        try {
            lblStatus.setText(objDictionaryAction.getWord("ACTIONMAPPING"));
            int intHeight = objWeave.getIntWeft();//(int)objWeave.getObjConfiguration().HEIGHT;
            int intLength = objWeave.getIntWarp();//(int)objWeave.getObjConfiguration().WIDTH;
            BufferedImage bufferedImage = new BufferedImage(intLength, intHeight, BufferedImage.TYPE_INT_RGB);
            objWeaveAction = new WeaveAction();
            bufferedImage = objWeaveAction.plotFrontSideView(objWeave, objWeave.getIntWarp(), objWeave.getIntWeft(), intLength, intHeight);
            MappingEditView objMappingEditView = new MappingEditView(objConfiguration,bufferedImage);
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void ZoomNormalAction(){
        try {
            zoomImages(1.00);
            //setScrollBarAtEnd();
            /*boxSize=15;
            plotDesign();
            plotShaft();
            plotPeg();
            plotTieUp();
            plotTradeles();
            plotWarpColor();
            plotWeftColor();
            plotDenting();
            lblStatus.setText(objDictionaryAction.getWord("ACTIONZOOMNORMALVIEW"));*/
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Normal View",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void zoomInAction(){
        try {
            zoomImages(1.50);
            //javafx.event.Event.fireEvent(null, MouseEvent.impl_mouseEvent(currentZoomFactor, currentZoomFactor, currentZoomFactor, currentZoomFactor, MouseButton.NONE, xindex, isNew, isNew, isNew, isNew, isNew, isNew, isNew, isNew, isNew, MouseEvent.MOUSE_CLICKED));
            //setScrollBarAtEnd();
            /*if(boxSize<=60){
                boxSize*=2;
                plotDesign();
                plotShaft();
                plotPeg();
                plotTieUp();
                plotTradeles();
                plotWarpColor();
                plotWeftColor();
                plotDenting();
                lblStatus.setText(objDictionaryAction.getWord("ACTIONZOOMINVIEW"));
            }else{
                lblStatus.setText(objDictionaryAction.getWord("MAXZOOMIN"));
            }*/
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void zoomOutAction(){
        try {
            zoomImages(1.00);
            //setScrollBarAtEnd();
            /*if(boxSize>=4){
                boxSize/=2;
                plotDesign();
                plotShaft();
                plotPeg();
                plotTieUp();
                plotTradeles();
                plotWarpColor();
                plotWeftColor();
                plotDenting();
                lblStatus.setText(objDictionaryAction.getWord("ACTIONZOOMOUTVIEW"));
            }else{
                lblStatus.setText(objDictionaryAction.getWord("MAXZOOMOUT"));
            }*/
        } catch (Exception ex) {
           new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
           lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }    
    /**
    * populateUtilityToolbar
    * <p>
    * Function use for drawing tool bar for menu item Utility,
    * and binding events for each tools. 
    * 
    * @exception   (@throws SQLException)
    * @author      Amit Kumar Singh
    * @version     %I%, %G%
    * @since       1.0
    * @see         javafx.event.*;
    * @link        ClothView
    */  
    private void populateUtilityToolbarMenu(){  
        //For drop down menus
        menuSelected.setTooltip(new Tooltip(objDictionaryAction.getWord("UTILITY")));
        
        MenuItem designEditMI = new MenuItem(objDictionaryAction.getWord("DESIGNMODE"));
        MenuItem artworkUtilityMI = new MenuItem(objDictionaryAction.getWord("ARTWORKEDITOR"));
        MenuItem fabricUtilityMI = new MenuItem(objDictionaryAction.getWord("FABRICEDITOR"));
        MenuItem clothUtilityMI = new MenuItem(objDictionaryAction.getWord("CLOTHEDITOR"));
        
        designEditMI.setAccelerator(new KeyCodeCombination(KeyCode.F10, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        artworkUtilityMI.setAccelerator(new KeyCodeCombination(KeyCode.D, KeyCombination.SHIFT_DOWN));
        fabricUtilityMI.setAccelerator(new KeyCodeCombination(KeyCode.J, KeyCombination.SHIFT_DOWN));
        clothUtilityMI.setAccelerator(new KeyCodeCombination(KeyCode.G, KeyCombination.SHIFT_DOWN));
        
        designEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
        artworkUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/artwork_editor.png"));
        fabricUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fabric_editor.png"));
        clothUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_editor.png"));
        
        //add enable disable condition
        if(!isWorkingMode){
            designEditMI.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
            designEditMI.setDisable(true);
            artworkUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/artwork_editor.png"));
            artworkUtilityMI.setDisable(true);
            fabricUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fabric_editor.png"));
            fabricUtilityMI.setDisable(true);
            clothUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_editor.png"));
            clothUtilityMI.setDisable(true);
        }else{
            designEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
            designEditMI.setDisable(false);
            artworkUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/artwork_editor.png"));
            artworkUtilityMI.setDisable(false);
            fabricUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fabric_editor.png"));
            fabricUtilityMI.setDisable(false);
            clothUtilityMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_editor.png"));
            clothUtilityMI.setDisable(false); 
            if(isDesignMode)
                designEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
            else
                designEditMI.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_lock.png"));
         }
        //Add the action to Buttons.
        designEditMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                editingModeSelection();   
            }
        });
        artworkUtilityMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                artworkUtilityAction();
            }
        });
        fabricUtilityMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                fabricUtilityAction();
            }
        });
        clothUtilityMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                clothUtilityAction();
            }
        });
        
        menuSelected.getItems().addAll(designEditMI, new SeparatorMenuItem(), artworkUtilityMI, fabricUtilityMI, clothUtilityMI);
          
    }
    private void populateUtilityToolbar(){  
        /* // fabric editor File menu
        VBox toolkitVB = new VBox(); 
        Label toolkitLbl= new Label(objDictionaryAction.getWord("TOOLKIT"));
        toolkitLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPTOOLKIT")));
        toolkitVB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/technical_info.png"), toolkitLbl);
        toolkitVB.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        toolkitVB.setCursor(Cursor.HAND); 
        toolkitVB.getStyleClass().addAll("VBox");*/
        // design mode edit item
        designEditBtn = new Button(); 
        designEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
        designEditBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("DESIGNMODE")+" (Ctrl+Shift+F10)\n"+objDictionaryAction.getWord("TOOLTIPDESIGNMODE")));
        designEditBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        designEditBtn.getStyleClass().addAll("toolbar-button");   
        // artwork editor item
        Button artworkUtilityBtn = new Button();
        artworkUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/artwork_editor.png"));
        artworkUtilityBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("ARTWORKEDITOR")+" (Shift+D)\n"+objDictionaryAction.getWord("TOOLTIPARTWORKEDITOR")));
        artworkUtilityBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        artworkUtilityBtn.getStyleClass().add("toolbar-button");    
        // fabric editor File menu 
        Button fabricUtilityBtn = new Button();
        fabricUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fabric_editor.png"));
        fabricUtilityBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("FABRICEDITOR")+" (Shift+J)\n"+objDictionaryAction.getWord("TOOLTIPFABRICEDITOR")));
        fabricUtilityBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        fabricUtilityBtn.getStyleClass().add("toolbar-button");  
        // garment viewer File menu
        Button clothUtilityBtn = new Button();
        clothUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_editor.png"));
        clothUtilityBtn.setTooltip(new Tooltip(objDictionaryAction.getWord("CLOTHEDITOR")+" (Shift+G)\n"+objDictionaryAction.getWord("TOOLTIPCLOTHEDITOR")));
        clothUtilityBtn.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        clothUtilityBtn.getStyleClass().add("toolbar-button");  
        //add enable disable condition
        if(!isWorkingMode){
            designEditBtn.setGraphic(new ImageView(objConfiguration.getStrColourDimmed()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
            designEditBtn.setDisable(true);
            designEditBtn.setCursor(Cursor.WAIT);
            artworkUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/artwork_editor.png"));
            artworkUtilityBtn.setDisable(true);
            artworkUtilityBtn.setCursor(Cursor.WAIT);
            fabricUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fabric_editor.png"));
            fabricUtilityBtn.setDisable(true);
            fabricUtilityBtn.setCursor(Cursor.WAIT);
            clothUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_editor.png"));
            clothUtilityBtn.setDisable(true);
            clothUtilityBtn.setCursor(Cursor.WAIT);
        }else{
            designEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
            designEditBtn.setDisable(false);
            designEditBtn.setCursor(Cursor.HAND);
            artworkUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/artwork_editor.png"));
            artworkUtilityBtn.setDisable(false);
            artworkUtilityBtn.setCursor(Cursor.HAND);
            fabricUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fabric_editor.png"));
            fabricUtilityBtn.setDisable(false);
            fabricUtilityBtn.setCursor(Cursor.HAND); 
            clothUtilityBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_editor.png"));
            clothUtilityBtn.setDisable(false);  
            clothUtilityBtn.setCursor(Cursor.HAND);          
            disableWarpWeftDelete();
            if(isDesignMode)
                designEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_unlock.png"));
            else
                designEditBtn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/pattern_lock.png"));
         }
        //Add the action to Buttons.
        designEditBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                editingModeSelection();
            }
        });
        artworkUtilityBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                artworkUtilityAction();
            }
        });
        fabricUtilityBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                fabricUtilityAction();
            }
        });
        clothUtilityBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                clothUtilityAction();
            }
        });
        //Create some Buttons. 
        FlowPane flow = new FlowPane();        
        flow.setPrefWrapLength(objConfiguration.WIDTH*.95); // preferred width allows for two columns
        flow.getStyleClass().addAll("flow");                
        //Add the Buttons to the ToolBar.
        flow.getChildren().addAll(designEditBtn,artworkUtilityBtn,fabricUtilityBtn,clothUtilityBtn);
        flow.setAlignment(Pos.TOP_LEFT);
        toolBar.getItems().addAll(flow);
    }    
    private void artworkUtilityAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONARTWORKEDITOR"));
        if(objWeave.getStrWeaveID()!=null && objWeave.getStrWeaveType()!=null){
            objConfiguration.setStrRecentWeave(objWeave.getStrWeaveID());
            objConfiguration.strWindowFlowContext="WeaveEditor";
            weaveStage.close();
            ArtworkView objArtworkView = new ArtworkView(objConfiguration);
        } else{
            new MessageView("error", objDictionaryAction.getWord("NOVALUE"), objDictionaryAction.getWord("UNSAVEDDATA"));
            lblStatus.setText(objDictionaryAction.getWord("UNSAVEDDATA"));
        }  
    }
    private void fabricUtilityAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONFABRICEDITOR"));
        if(objWeave.getStrWeaveID()!=null && objWeave.getStrWeaveType()!=null && !isNew){
            final Stage dialogStageParent = new Stage();
            dialogStageParent.initStyle(StageStyle.UTILITY);
            //dialogStage.initModality(Modality.WINDOW_MODAL);
            GridPane popupParent=new GridPane();
            popupParent.setId("popup");
            popupParent.setHgap(10);
            popupParent.setVgap(10);

            Label clothType = new Label(objDictionaryAction.getWord("CLOTHTYPE")+" : ");
            clothType.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCLOTHTYPE")));
            popupParent.add(clothType, 0, 0);
            final ComboBox clothTypeCB = new ComboBox();
            clothTypeCB.getItems().addAll("Body","Pallu","Border","Cross Border","Blouse","Skirt","Konia");
            clothTypeCB.setValue(objConfiguration.getStrClothType());
            clothTypeCB.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCLOTHTYPE")));
            popupParent.add(clothTypeCB, 1, 0);        

            Button btnApply = new Button(objDictionaryAction.getWord("APPLY"));
            btnApply.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/update.png"));
            btnApply.setTooltip(new Tooltip(objDictionaryAction.getWord("ACTIONAPPLY")));
            btnApply.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    try {
                        objConfiguration.setStrClothType(clothTypeCB.getValue().toString());
                        UserAction objUserAction = new UserAction();
                        objUserAction.getConfiguration(objConfiguration);
                        //objConfiguration.clothRepeat();
                        
                        objConfiguration.setStrRecentWeave(objWeave.getStrWeaveID());
                         objConfiguration.strWindowFlowContext="WeaveEditor";
                         weaveStage.close();
                         FabricView objFabricView = new FabricView(objConfiguration);
                    } catch (SQLException ex) {
                        new Logging("SEVERE",getClass().getName(),"Cloth type change",ex);
                        lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                    } catch (Exception ex) {
                        new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                        lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                    }
                    System.gc();
                    dialogStageParent.close();
                }
            });
            popupParent.add(btnApply, 0, 1);

            Button btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));
            btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
            btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
            btnCancel.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {  
                    lblStatus.setText(objDictionaryAction.getWord("ACTIONCANCEL"));
                    dialogStageParent.close();
                }
            });
            popupParent.add(btnCancel, 1, 1);

            Scene scene = new Scene(popupParent);
            scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
            dialogStageParent.setScene(scene);
            dialogStageParent.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("FABRICSETTINGS"));
            dialogStageParent.showAndWait();
        } else{
            new MessageView("error", objDictionaryAction.getWord("NOVALUE"), objDictionaryAction.getWord("UNSAVEDDATA"));
            lblStatus.setText(objDictionaryAction.getWord("UNSAVEDDATA"));
        }
    }
    private void clothUtilityAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCLOTHEDITOR"));
        if(objWeave.getStrWeaveID()!=null && objWeave.getStrWeaveType()!=null){
            objConfiguration.setStrRecentWeave(objWeave.getStrWeaveID());
            objConfiguration.strWindowFlowContext="WeaveEditor";
            weaveStage.close();
            ClothView objClothView = new ClothView(objConfiguration);
        } else{
            new MessageView("error", objDictionaryAction.getWord("NOVALUE"), objDictionaryAction.getWord("UNSAVEDDATA"));
            lblStatus.setText(objDictionaryAction.getWord("UNSAVEDDATA"));
        }
    }
    private Weave cloneWeave(){
        Weave objClonedWeave = new Weave();
        objClonedWeave.setBytIsColor(objWeave.getBytIsColor());
        objClonedWeave.setBytIsLiftPlan(objWeave.getBytIsLiftPlan());
        
        if(objWeave.getBytWeaveThumbnil()!=null){
            byte[] bytWeaveThumbnil = new byte[objWeave.getBytWeaveThumbnil().length];
            for(int i=0; i<objWeave.getBytWeaveThumbnil().length; i++)
                bytWeaveThumbnil[i] = objWeave.getBytWeaveThumbnil()[i];
            objClonedWeave.setBytWeaveThumbnil(bytWeaveThumbnil);
        }
        
        if(objWeave.getClipMatrix()!=null){
            byte[][] clipMatrix = new byte[objWeave.getClipMatrix().length][objWeave.getClipMatrix()[0].length];
            for(int i=0; i<objWeave.getClipMatrix().length; i++)
                for(int j=0; j<objWeave.getClipMatrix()[0].length; j++)
                    clipMatrix[i][j]=objWeave.getClipMatrix()[i][j];
            objClonedWeave.setClipMatrix(clipMatrix);
            clipMatrix=null;
        }
        
        if(objWeave.getDentMatrix()!=null){
            byte[][] dentMatrix = new byte[objWeave.getDentMatrix().length][objWeave.getDentMatrix()[0].length];
            for(int i=0; i<objWeave.getDentMatrix().length; i++)
                for(int j=0; j<objWeave.getDentMatrix()[0].length; j++)
                    dentMatrix[i][j]=objWeave.getDentMatrix()[i][j];
            objClonedWeave.setDentMatrix(dentMatrix);
            dentMatrix=null;
        }
        
        if(objWeave.getDesignMatrix()!=null){
            byte[][] designMatrix = new byte[objWeave.getDesignMatrix().length][objWeave.getDesignMatrix()[0].length];
            for(int i=0; i<objWeave.getDesignMatrix().length; i++)
                for(int j=0; j<objWeave.getDesignMatrix()[0].length; j++)
                    designMatrix[i][j]=objWeave.getDesignMatrix()[i][j];
            objClonedWeave.setDesignMatrix(designMatrix);
            designMatrix=null;
        }
        
        objClonedWeave.setIntEPI(objWeave.getIntEPI());
        objClonedWeave.setIntPPI(objWeave.getIntPPI());
        objClonedWeave.setIntWarp(objWeave.getIntWarp());
        objClonedWeave.setIntWeaveFloatX(objWeave.getIntWeaveFloatX());
        objClonedWeave.setIntWeaveFloatY(objWeave.getIntWeaveFloatY());
        objClonedWeave.setIntWeaveRepeatX(objWeave.getIntWeaveRepeatX());
        objClonedWeave.setIntWeaveRepeatY(objWeave.getIntWeaveRepeatY());
        objClonedWeave.setIntWeaveShaft(objWeave.getIntWeaveShaft());
        objClonedWeave.setIntWeaveTreadles(objWeave.getIntWeaveTreadles());
        objClonedWeave.setIntWeft(objWeave.getIntWeft());
        
        if(objWeave.getLstPattern()!=null){
            ArrayList<Pattern> lstPattern = new ArrayList<Pattern>();
            for(int i=0; i<objWeave.getLstPattern().size(); i++)
                lstPattern.set(i, new Pattern(objWeave.getLstPattern().get(i).getStrPatternID(), objWeave.getLstPattern().get(i).getStrPattern(), objWeave.getLstPattern().get(i).getIntPatternType(), objWeave.getLstPattern().get(i).getIntPatternRepeat(), objWeave.getLstPattern().get(i).getIntPatternUsed(), objWeave.getLstPattern().get(i).getStrPatternAccess(), objWeave.getLstPattern().get(i).getStrPatternUser(), objWeave.getLstPattern().get(i).getStrPatternDate()));
            objClonedWeave.setLstPattern(lstPattern);
        }
        
        if(objWeave.getLstYarn()!=null){
            ArrayList<Yarn> lstYarn = new ArrayList<Yarn>();
            for(int i=0; i<objWeave.getLstYarn().size(); i++)
                lstYarn.set(i, new Yarn(objWeave.getLstYarn().get(i).getStrYarnId(), objWeave.getLstYarn().get(i).getStrYarnType(), objWeave.getLstYarn().get(i).getStrYarnName(), objWeave.getLstYarn().get(i).getStrYarnColor(), objWeave.getLstYarn().get(i).getIntYarnRepeat(), objWeave.getLstYarn().get(i).getStrYarnSymbol(), objWeave.getLstYarn().get(i).getIntYarnCount(), objWeave.getLstYarn().get(i).getStrYarnCountUnit(), objWeave.getLstYarn().get(i).getIntYarnPly(), objWeave.getLstYarn().get(i).getIntYarnDFactor(), objWeave.getLstYarn().get(i).getDblYarnDiameter(), objWeave.getLstYarn().get(i).getIntYarnTwist(), objWeave.getLstYarn().get(i).getStrYarnTModel(), objWeave.getLstYarn().get(i).getIntYarnHairness(), objWeave.getLstYarn().get(i).getIntYarnHProbability(), objWeave.getLstYarn().get(i).getDblYarnPrice(), objWeave.getLstYarn().get(i).getStrYarnAccess(), objWeave.getLstYarn().get(i).getStrYarnUser(), objWeave.getLstYarn().get(i).getStrYarnDate()));
            objClonedWeave.setLstYarn(lstYarn);
        }
        
        objClonedWeave.setObjConfiguration(objWeave.getObjConfiguration());
        
        if(objWeave.getPegMatrix()!=null){
            byte[][] pegMatrix = new byte[objWeave.getPegMatrix().length][objWeave.getPegMatrix()[0].length];
            for(int i=0; i<objWeave.getPegMatrix().length; i++)
                for(int j=0; j<objWeave.getPegMatrix()[0].length; j++)
                    pegMatrix[i][j]=objWeave.getPegMatrix()[i][j];
            objClonedWeave.setPegMatrix(pegMatrix);
            pegMatrix=null;
        }
        
        if(objWeave.getShaftMatrix()!=null){
            byte[][] shaftMatrix = new byte[objWeave.getShaftMatrix().length][objWeave.getShaftMatrix()[0].length];
            for(int i=0; i<objWeave.getShaftMatrix().length; i++)
                for(int j=0; j<objWeave.getShaftMatrix()[0].length; j++)
                    shaftMatrix[i][j]=objWeave.getShaftMatrix()[i][j];
            objClonedWeave.setShaftMatrix(shaftMatrix);
            shaftMatrix=null;
        }
        
        if(objWeave.getSingleMatrix()!=null){
            byte[][] singleMatrix = new byte[objWeave.getSingleMatrix().length][objWeave.getSingleMatrix()[0].length];
            for(int i=0; i<objWeave.getSingleMatrix().length; i++)
                for(int j=0; j<objWeave.getSingleMatrix()[0].length; j++)
                    singleMatrix[i][j]=objWeave.getSingleMatrix()[i][j];
            objClonedWeave.setSingleMatrix(singleMatrix);
            singleMatrix=null;
        }
        
        objClonedWeave.setStrWeaveAccess(objWeave.getStrWeaveAccess());
        objClonedWeave.setStrWeaveCategory(objWeave.getStrWeaveCategory());
        objClonedWeave.setStrWeaveDate(objWeave.getStrWeaveDate());
        objClonedWeave.setStrWeaveFile(objWeave.getStrWeaveFile());
        objClonedWeave.setStrWeaveID(objWeave.getStrWeaveID());
        objClonedWeave.setStrWeaveName(objWeave.getStrWeaveName());
        objClonedWeave.setStrWeaveType(objWeave.getStrWeaveType());
        
        if(objWeave.getTieupMatrix()!=null){
            byte[][] tieupMatrix = new byte[objWeave.getTieupMatrix().length][objWeave.getTieupMatrix()[0].length];
            for(int i=0; i<objWeave.getTieupMatrix().length; i++)
                for(int j=0; j<objWeave.getTieupMatrix()[0].length; j++)
                    tieupMatrix[i][j]=objWeave.getTieupMatrix()[i][j];
            objClonedWeave.setTieupMatrix(tieupMatrix);
            tieupMatrix=null;
        }
        
        if(objWeave.getTreadlesMatrix()!=null){
            byte[][] treadlesMatrix = new byte[objWeave.getTreadlesMatrix().length][objWeave.getTreadlesMatrix()[0].length];
            for(int i=0; i<objWeave.getTreadlesMatrix().length; i++)
                for(int j=0; j<objWeave.getTreadlesMatrix()[0].length; j++)
                    treadlesMatrix[i][j]=objWeave.getTreadlesMatrix()[i][j];
            objClonedWeave.setTreadlesMatrix(treadlesMatrix);
            treadlesMatrix=null;
        }
        
        if(objWeave.getWarpYarn()!=null){
            Yarn[] warpYarn = new Yarn[objWeave.getWarpYarn().length];
            for(int i=0; i<objWeave.getWarpYarn().length; i++)
                warpYarn[i] = new Yarn(objWeave.getWarpYarn()[i].getStrYarnId(), objWeave.getWarpYarn()[i].getStrYarnType(), objWeave.getWarpYarn()[i].getStrYarnName(), objWeave.getWarpYarn()[i].getStrYarnColor(), objWeave.getWarpYarn()[i].getIntYarnRepeat(), objWeave.getWarpYarn()[i].getStrYarnSymbol(), objWeave.getWarpYarn()[i].getIntYarnCount(), objWeave.getWarpYarn()[i].getStrYarnCountUnit(), objWeave.getWarpYarn()[i].getIntYarnPly(), objWeave.getWarpYarn()[i].getIntYarnDFactor(), objWeave.getWarpYarn()[i].getDblYarnDiameter(), objWeave.getWarpYarn()[i].getIntYarnTwist(), objWeave.getWarpYarn()[i].getStrYarnTModel(), objWeave.getWarpYarn()[i].getIntYarnHairness(), objWeave.getWarpYarn()[i].getIntYarnHProbability(), objWeave.getWarpYarn()[i].getDblYarnPrice(), objWeave.getWarpYarn()[i].getStrYarnAccess(), objWeave.getWarpYarn()[i].getStrYarnUser(), objWeave.getWarpYarn()[i].getStrYarnDate());
            objClonedWeave.setWarpYarn(warpYarn);
        }
        
        if(objWeave.getWeftYarn()!=null){
            Yarn[] weftYarn = new Yarn[objWeave.getWeftYarn().length];
            for(int i=0; i<objWeave.getWeftYarn().length; i++)
                weftYarn[i] = new Yarn(objWeave.getWeftYarn()[i].getStrYarnId(), objWeave.getWeftYarn()[i].getStrYarnType(), objWeave.getWeftYarn()[i].getStrYarnName(), objWeave.getWeftYarn()[i].getStrYarnColor(), objWeave.getWeftYarn()[i].getIntYarnRepeat(), objWeave.getWeftYarn()[i].getStrYarnSymbol(), objWeave.getWeftYarn()[i].getIntYarnCount(), objWeave.getWeftYarn()[i].getStrYarnCountUnit(), objWeave.getWeftYarn()[i].getIntYarnPly(), objWeave.getWeftYarn()[i].getIntYarnDFactor(), objWeave.getWeftYarn()[i].getDblYarnDiameter(), objWeave.getWeftYarn()[i].getIntYarnTwist(), objWeave.getWeftYarn()[i].getStrYarnTModel(), objWeave.getWeftYarn()[i].getIntYarnHairness(), objWeave.getWeftYarn()[i].getIntYarnHProbability(), objWeave.getWeftYarn()[i].getDblYarnPrice(), objWeave.getWeftYarn()[i].getStrYarnAccess(), objWeave.getWeftYarn()[i].getStrYarnUser(), objWeave.getWeftYarn()[i].getStrYarnDate());
            objClonedWeave.setWeftYarn(weftYarn);
        }
        
        return objClonedWeave;
    }
    private void initWeaveValue(){
        try {
            if(!objWeave.getStrWeaveFile().equals("") && objWeave.getStrWeaveFile()!=null){
                objWeaveAction = new WeaveAction();
                objWeaveAction.extractWeaveContent(objWeave);
                //objWeave.setStrThreadPalettes(objWeave.getObjConfiguration().strThreadPalettes);
                objWeave.setDentMatrix(new byte[2][objWeave.getIntWarp()]);
                for(int i=0; i<2; i++)
                    for(int j=0; j<objWeave.getIntWarp(); j++)
                        objWeave.getDentMatrix()[i][j]=(byte)((i+j)%2);
                objWeaveAction.populateYarnPalette(objWeave);
                isWorkingMode = true;
                plotDesign();
                plotShaft();
                plotPeg();
                plotTieUp();  
                plotTradeles();
                plotWarpColor();
                plotWeftColor();
                plotDenting();
                plotColorPalette();
            } else{
                lblStatus.setText(objDictionaryAction.getWord("INVALIDWEAVE"));
                isWorkingMode = false;
            }
            selectedMenu = "FILE";
            menuHighlight();
            System.gc();
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private void loadWeave(){
        try{
            objWeave.setBytIsLiftPlan((byte)0);    
            objWeaveAction = new WeaveAction();
            objWeaveAction.getWeave(objWeave);
            objWeaveAction = new WeaveAction();
            if(objWeaveAction.countWeaveUsage(objWeave.getStrWeaveID())>0 || new IDGenerator().getUserAcessValueData("ARTWORK_LIBRARY", objWeave.getStrWeaveAccess()).equalsIgnoreCase("Public")){
                isNew = true;
            }else{
                isNew = false;
            }
            System.gc();
            objConfiguration.setStrRecentWeave(objWeave.getStrWeaveID());
            initWeaveValue();
        } catch (Exception ex) {
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
        }
    }
    private void plotDesign(){
        if(objWeave.getDesignMatrix()!=null){
            drawImageFromMatrix(objWeave.getDesignMatrix(), designImage);
            refreshDesignImage();
        }
    }
    
    private void initShaft(){
        if(objWeave.getShaftMatrix().length>objWeave.getIntWeaveShaft())
            return;
        byte[][] matrix=new byte[objWeave.getIntWeaveShaft()][objWeave.getIntWarp()];
        int toAdd=objWeave.getIntWeaveShaft()-objWeave.getShaftMatrix().length;
        for(int j=0;j<objWeave.getShaftMatrix().length;j++)
            for(int k=0;k<objWeave.getIntWarp(); k++)
                matrix[toAdd+j][k]=objWeave.getShaftMatrix()[j][k];
        for(int j=0;j<toAdd;j++)
            for(int k=0;k<objWeave.getIntWarp(); k++)
                matrix[j][k]=0;
        objWeave.setShaftMatrix(matrix);
    }
    
    private void plotShaft(){
        if(objWeave.getShaftMatrix()!=null){
            byte[][] matrix=new byte[objWeave.getShaftMatrix().length][objWeave.getShaftMatrix()[0].length];
            //int start=0;
            //if(activeGridRow>objWeave.getShaftMatrix().length)
            //    start=activeGridRow-objWeave.getShaftMatrix().length;
            for(int r=0; r<objWeave.getShaftMatrix().length; r++){
                for(int c=0; c<objWeave.getShaftMatrix()[0].length; c++){
                    matrix[r/*+start*/][c]=objWeave.getShaftMatrix()[r][c];
                }
            }
            drawImageFromMatrix(matrix, shaftImage);
            refreshShaftImage();
        }
    }
    
    private void initPeg(){
        if(objWeave.getPegMatrix()[0].length>objWeave.getIntWeaveShaft())
            return;
        byte[][] matrix=new byte[objWeave.getIntWeft()][objWeave.getIntWeaveShaft()];
        int toAdd=objWeave.getIntWeaveShaft()-objWeave.getPegMatrix()[0].length;
        for(int j=0;j<objWeave.getIntWeft();j++)
            for(int k=0;k<objWeave.getPegMatrix()[0].length; k++)
                matrix[j][k]=objWeave.getPegMatrix()[j][k];
        for(int j=0;j<objWeave.getIntWeft();j++)
            for(int k=0;k<toAdd; k++)
                matrix[j][k+objWeave.getPegMatrix()[0].length]=0;
        objWeave.setPegMatrix(matrix);
    }

    private void plotPeg(){
        if(objWeave.getPegMatrix()!=null){
            byte[][] matrix=new byte[objWeave.getPegMatrix().length][objWeave.getPegMatrix()[0].length];
            //new byte[activeGridRow][activeGridCol];
            for(int r=0; r<objWeave.getPegMatrix().length; r++){
                for(int c=0; c<objWeave.getPegMatrix()[0].length; c++){
                    matrix[r][c]=objWeave.getPegMatrix()[r][c];
                }
            }
            drawImageFromMatrix(matrix, pegImage);
            refreshPegImage();
        }
    }

    private void plotTieUp(){        
        if(objWeave.getTieupMatrix()!=null){
            byte[][] matrix=new byte[objWeave.getTieupMatrix().length][objWeave.getTieupMatrix()[0].length];
            //new byte[activeGridRow][activeGridCol];
            //int start=0;
            //if(activeGridRow>objWeave.getTieupMatrix().length)
            //    start=activeGridRow-objWeave.getTieupMatrix().length;
            for(int r=0; r<objWeave.getTieupMatrix().length; r++){
                for(int c=0; c<objWeave.getTieupMatrix()[0].length; c++){
                    //System.err.print("\t"+objWeave.getTieupMatrix()[r][c]);
                    matrix[r/*+start*/][c]=objWeave.getTieupMatrix()[r][c];
                }
                //System.err.println("");
            }
            drawImageFromMatrix(matrix, tieUpImage);
            refreshTieUpImage();
        }
    }
    
    private void initTreadles(){
        if(objWeave.getTreadlesMatrix()[0].length>objWeave.getIntWeaveTreadles())
            return;
        byte[][] matrix=new byte[objWeave.getIntWeft()][objWeave.getIntWeaveTreadles()];
        int toAdd=objWeave.getIntWeaveTreadles()-objWeave.getTreadlesMatrix()[0].length;
        for(int j=0;j<objWeave.getIntWeft();j++)
            for(int k=0;k<objWeave.getTreadlesMatrix()[0].length; k++)
                matrix[j][k]=objWeave.getTreadlesMatrix()[j][k];
        for(int j=0;j<objWeave.getIntWeft();j++)
            for(int k=0;k<toAdd; k++)
                matrix[j][objWeave.getTreadlesMatrix()[0].length+k]=0;
        objWeave.setTreadlesMatrix(matrix);
    }

    private void plotTradeles(){
        if(objWeave.getTreadlesMatrix()!=null){
            byte[][] matrix=new byte[objWeave.getTreadlesMatrix().length][objWeave.getTreadlesMatrix()[0].length];
                    //new byte[activeGridRow][activeGridCol];
            for(int r=0; r<objWeave.getTreadlesMatrix().length; r++){
                for(int c=0; c<objWeave.getTreadlesMatrix()[0].length; c++){
                    matrix[r][c]=objWeave.getTreadlesMatrix()[r][c];
                }
            }
            drawImageFromMatrix(matrix, tradelesImage);
            refreshTradelesImage();
        }
    }

    private void plotDenting(){
        if(objWeave.getDentMatrix()!=null){
            byte[][] matrix=new byte[2][objWeave.getDentMatrix()[0].length];
            for(int r=0; r<objWeave.getDentMatrix().length; r++){
                for(int c=0; c<objWeave.getDentMatrix()[0].length; c++){
                    matrix[r][c]=objWeave.getDentMatrix()[r][c];
                }
            }
            drawImageFromMatrix(matrix, dentingImage);
            refreshDentingImage();
        }
    }
    
    private void plotWarpColor(){
        clearImage(warpColorImage);
        Point dot=new Point();
        int intColor=-1;
        for(int i=0;i<objWeave.getIntWarp();i++) {
            intColor=getIntRgbFromColor(255
                    , Integer.parseInt(objWeave.getWarpYarn()[i].getStrYarnColor().substring(1, 3), 16)
                    , Integer.parseInt(objWeave.getWarpYarn()[i].getStrYarnColor().substring(3, 5), 16)
                    , Integer.parseInt(objWeave.getWarpYarn()[i].getStrYarnColor().substring(5, 7), 16));
            dot.x=i;
            dot.y=0;
            //System.err.println(intColor+"Yarn"+objWeave.getWarpYarn()[i].getStrYarnColor());
            fillImageDotPixels(dot, intColor, warpColorImage);
        }
        refreshImage(warpColorImage);
    }
    
    private void plotWeftColor(){
        clearImage(weftColorImage);
        Point dot=new Point();
        int intColor=-1;
        for(int i=objWeave.getIntWeft()-1;i>=0;i--) {
            intColor=getIntRgbFromColor(255
                    , Integer.parseInt(objWeave.getWeftYarn()[i].getStrYarnColor().substring(1, 3), 16)
                    , Integer.parseInt(objWeave.getWeftYarn()[i].getStrYarnColor().substring(3, 5), 16)
                    , Integer.parseInt(objWeave.getWeftYarn()[i].getStrYarnColor().substring(5, 7), 16));
            dot.x=0;
            dot.y=(MAX_GRID_ROW-1)-(activeGridRow-1)+i;
            fillImageDotPixels(dot, intColor, weftColorImage);
        }
        refreshImage(weftColorImage);
    }
    private byte[][] getUpdatedDentMatrix(){
        byte[][] dent_temp= new byte[2][objWeave.getIntWarp()];
        int remaining=0;
        int to=(objWeave.getDentMatrix()[0].length<objWeave.getIntWarp()?objWeave.getDentMatrix()[0].length:objWeave.getIntWarp());
        if(to==objWeave.getDentMatrix()[0].length)
            remaining=objWeave.getIntWarp()-objWeave.getDentMatrix()[0].length;
        for(int j=0;j<to;j++){
            dent_temp[0][j]=objWeave.getDentMatrix()[0][j];
            dent_temp[1][j]=objWeave.getDentMatrix()[1][j];
        }
        for(int k=0; k<remaining; k++){
            dent_temp[0][k+objWeave.getDentMatrix()[0].length]=(byte)((k+objWeave.getDentMatrix()[0].length)%2);
            dent_temp[1][k+objWeave.getDentMatrix()[0].length]=(byte)((k+objWeave.getDentMatrix()[0].length+1)%2);
        }
        return dent_temp;
    }
    
    private Yarn[] getUpdatedWarpColor(){
        Yarn[] warpYarn=new Yarn[objWeave.getIntWarp()];
        int to=(objWeave.getWarpYarn().length<objWeave.getIntWarp()?objWeave.getWarpYarn().length:objWeave.getIntWarp());
        for(int j=0;j<to;j++)
            warpYarn[j]=objWeave.getWarpYarn()[j];
        int newYarnCount=(objWeave.getWarpYarn().length<objWeave.getIntWarp()?(objWeave.getIntWarp()-objWeave.getWarpYarn().length):0);
        for(int j=to;j<to+newYarnCount;j++)
            warpYarn[j]=new Yarn(null, "Warp", objConfiguration.getStrWarpName(), "#ffffff", objConfiguration.getIntWarpRepeat(), "A", objConfiguration.getIntWarpCount(), objConfiguration.getStrWarpUnit(), objConfiguration.getIntWarpPly(), objConfiguration.getIntWarpFactor(), objConfiguration.getDblWarpDiameter(), objConfiguration.getIntWarpTwist(), objConfiguration.getStrWarpSence(), objConfiguration.getIntWarpHairness(), objConfiguration.getIntWarpDistribution(), objConfiguration.getDblWarpPrice(), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);
        return warpYarn;
    }
    
    private Yarn[] getUpdatedWeftColor(){
        Yarn[] weftYarn=new Yarn[objWeave.getIntWeft()];
        int to=(objWeave.getWeftYarn().length<objWeave.getIntWeft()?objWeave.getWeftYarn().length:objWeave.getIntWeft());
        if(objWeave.getWeftYarn().length<objWeave.getIntWeft()){
            int addedYarnCount=objWeave.getIntWeft()-objWeave.getWeftYarn().length;
            for(int j=0;j<to;j++)
                weftYarn[addedYarnCount+j]=objWeave.getWeftYarn()[j];
            for(int j=0;j<addedYarnCount;j++)
                weftYarn[j]=new Yarn(null, "Weft", objConfiguration.getStrWeftName(), "#ffffff", objConfiguration.getIntWeftRepeat(), "a", objConfiguration.getIntWeftCount(), objConfiguration.getStrWeftUnit(), objConfiguration.getIntWeftPly(), objConfiguration.getIntWeftFactor(), objConfiguration.getDblWeftDiameter(), objConfiguration.getIntWeftTwist(), objConfiguration.getStrWeftSence(), objConfiguration.getIntWeftHairness(), objConfiguration.getIntWeftDistribution(), objConfiguration.getDblWeftPrice(), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);
        } else{
            int deletedYarnCount=objWeave.getWeftYarn().length-objWeave.getIntWeft();
            for(int j=0;j<to;j++)
                weftYarn[j]=objWeave.getWeftYarn()[deletedYarnCount+j];
        }
        
        return weftYarn;
    }   
    private void populateNplotWarpWeftYarn(){
        for(int i=0; i<objWeave.getWarpYarn().length; i++){
            for(int j=0; j<26; j++)
                if(objWeave.getWarpYarn()[i].getStrYarnSymbol().equals(objWeave.getObjConfiguration().getYarnPalette()[j].getStrYarnSymbol()))
                    objWeave.getWarpYarn()[i]=objWeave.getObjConfiguration().getYarnPalette()[j];
        }
        for(int i=0; i<objWeave.getWeftYarn().length; i++){
            for(int j=26; j<52; j++)
                if(objWeave.getWeftYarn()[i].getStrYarnSymbol().equals(objWeave.getObjConfiguration().getYarnPalette()[j].getStrYarnSymbol()))
                    objWeave.getWeftYarn()[i]=objWeave.getObjConfiguration().getYarnPalette()[j];
        }
        plotWarpColor();
        plotWeftColor();
    }    
   private void populateNplot(){
        try{
            objWeaveAction=new WeaveAction();
            objWeaveAction.populateShaft(objWeave);
            //objWeave.setIntWeaveShaft(objWeave.getIntWeft());
            //initShaft();
            objWeaveAction.populateTreadles(objWeave);
            //objWeave.setIntWeaveTreadles(objWeave.getIntWarp());
            //initTreadles();
            objWeaveAction.populatePegPlan(objWeave);
            //objWeave.setIntWeaveShaft(objWeave.getIntWeft());
            //initPeg();
            objWeaveAction.populateTieUp(objWeave);
            objWeave.setDentMatrix(getUpdatedDentMatrix());
            objWeave.setWarpYarn(getUpdatedWarpColor());
            objWeave.setWeftYarn(getUpdatedWeftColor());
            //drawImageFromMatrix(objWeave.getShaftMatrix(), shaftImage);
            //drawImageFromMatrix(objWeave.getPegMatrix(), pegImage);
            //drawImageFromMatrix(objWeave.getTieupMatrix(), tieUpImage);
            //drawImageFromMatrix(objWeave.getTreadlesMatrix(), tradelesImage);
            //drawImageFromMatrix(objWeave.getDentMatrix(), dentingImage);
            //refreshShaftImage();
            //refreshPegImage();
            //refreshTieUpImage();
            //refreshTradelesImage();
            //refreshDentingImage();
            plotShaft();
            plotPeg();
            plotTieUp();
            plotTradeles();
            plotDenting();
            plotWarpColor();
            plotWeftColor();
        }catch(Exception ex){
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
   }   
   private void populateDplot(){
        try{
            //objWeaveAction.populateWarpColor(objWeave);
            //objWeaveAction.populateWeftColor(objWeave);
            //objWeaveAction.populateDenting(objWeave);
            plotDesign();
            plotWarpColor();
            plotWeftColor();
            plotDenting();
        }catch(Exception ex){
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
   }
   private void plotColorPalette(){
        paletteColorGP.getChildren().clear();
        paletteColorGP.setVgap(5);
        paletteColorGP.setHgap(5);
        //paletteColorGP.autosize();
        //paletteColorGP.setId("leftPane");       
        
        paletteColorGP.add(new Label(objDictionaryAction.getWord("PALETTE")), 0, 0, 1, 1);
        
        final ComboBox paletteCB = new ComboBox();
        paletteCB.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPPALETTE")));
        try{
            Palette objPalette = new Palette();
            objPalette.setObjConfiguration(objConfiguration);
            objPalette.setStrPaletteType("Colour");
            UtilityAction objUtilityAction = new UtilityAction();
            String[][] paletteNames=objUtilityAction.getPaletteName(objPalette);
            if(paletteNames!=null){
                for(String[] s:paletteNames){
                    paletteCB.getItems().add(s[1]);
                }
                if(paletteCB.getItems().size()>0)
                    paletteCB.setValue(paletteCB.getItems().get(0));
            }
        } catch(SQLException sqlEx){
            new Logging("SEVERE",getClass().getName(),"loadPaletteNames() : ", sqlEx);
        }
        paletteColorGP.add(paletteCB, 1, 0, 1, 1);
                
        paletteCB.valueProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                try{
                    UtilityAction objUtilityAction=new UtilityAction();
                    String id=objUtilityAction.getPaletteIdFromName(t1.toString());
                    if(id!=null){
                        Palette objPalette = new Palette();
                        objPalette.setStrPaletteID(id);
                        objPalette.setObjConfiguration(objConfiguration);
                        objUtilityAction=new UtilityAction();
                        objUtilityAction.getPalette(objPalette);
                        if(objPalette.getStrThreadPalette()!=null){
                            objWeave.getObjConfiguration().setColourPalette(objPalette.getStrThreadPalette());
                        }
                    }
                    /*String[] tPalette=objColorPaletteAction.getPaletteFromName(paletteName);
                    if(tPalette!=null)
                        threadPaletes=tPalette;*/
                    objWeaveAction.populateYarnPalette(objWeave);
                    loadColorPalettes(); // it sets colorPalates[][] using threadPalettes
                    plotWarpColor();
                    plotWeftColor();                    
                } catch(SQLException sqlEx){
                    new Logging("SEVERE",getClass().getName(),"loadColorPalette() : ", sqlEx);
                }
            }
        });        
        
        Button btnSwap = new Button();//objDictionaryAction.getWord("SWAPCOLOR"));
        btnSwap.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSWAPCOLOR")));
        btnSwap.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/update.png"));
        btnSwap.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                Yarn temp;String type, symbol; 
                for(int i =0; i<26; i++){
                    type = objWeave.getObjConfiguration().getYarnPalette()[i].getStrYarnType();
                    symbol = objWeave.getObjConfiguration().getYarnPalette()[i].getStrYarnSymbol();
                    
                    objWeave.getObjConfiguration().getYarnPalette()[i].setStrYarnType(objWeave.getObjConfiguration().getYarnPalette()[i+26].getStrYarnType());
                    objWeave.getObjConfiguration().getYarnPalette()[i].setStrYarnSymbol(objWeave.getObjConfiguration().getYarnPalette()[i+26].getStrYarnSymbol());
                    
                    objWeave.getObjConfiguration().getYarnPalette()[i+26].setStrYarnType(type);
                    objWeave.getObjConfiguration().getYarnPalette()[i+26].setStrYarnSymbol(symbol);
                    
                    temp = objWeave.getObjConfiguration().getYarnPalette()[i];
                    objWeave.getObjConfiguration().getYarnPalette()[i]=objWeave.getObjConfiguration().getYarnPalette()[i+26];
                    objWeave.getObjConfiguration().getYarnPalette()[i+26]=temp;
                }
                for(int i=0; i<52; i++){
                    objWeave.getObjConfiguration().getColourPalette()[i]=objWeave.getObjConfiguration().getYarnPalette()[i].getStrYarnColor().substring(1); // removing #
                }
                objWeaveAction.populateColorPalette(objWeave);
                loadColorPalettes();
            }
        });
        paletteColorGP.add(btnSwap, 2, 0, 1, 1);
        
        Button btnWarpWeft = new Button();//objDictionaryAction.getWord("WARPTOWEFT"));
        btnWarpWeft.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPWARPTOWEFT")));
        btnWarpWeft.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right.png"));
        btnWarpWeft.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                String type, symbol;
                for(int i =0; i<26; i++){
                    Yarn objYarn = objConfiguration.getYarnPalette()[i+26];

                    objYarn.setStrYarnType(objConfiguration.getYarnPalette()[i].getStrYarnType());
                    objYarn.setStrYarnName(objConfiguration.getYarnPalette()[i+26].getStrYarnName());
                    objYarn.setStrYarnColor(objConfiguration.getYarnPalette()[i].getStrYarnColor());
                    objYarn.setIntYarnRepeat(objConfiguration.getYarnPalette()[i].getIntYarnRepeat());
                    objYarn.setStrYarnSymbol(objConfiguration.getYarnPalette()[i+26].getStrYarnSymbol());
                    objYarn.setIntYarnCount(objConfiguration.getYarnPalette()[i].getIntYarnCount());
                    objYarn.setStrYarnCountUnit(objConfiguration.getYarnPalette()[i].getStrYarnCountUnit());
                    objYarn.setIntYarnPly(objConfiguration.getYarnPalette()[i].getIntYarnPly());
                    objYarn.setIntYarnDFactor(objConfiguration.getYarnPalette()[i].getIntYarnDFactor());
                    objYarn.setDblYarnDiameter(objConfiguration.getYarnPalette()[i].getDblYarnDiameter());
                    objYarn.setIntYarnTwist(objConfiguration.getYarnPalette()[i].getIntYarnTwist());
                    objYarn.setStrYarnTModel(objConfiguration.getYarnPalette()[i].getStrYarnTModel());
                    objYarn.setIntYarnHairness(objConfiguration.getYarnPalette()[i].getIntYarnHairness());
                    objYarn.setIntYarnHProbability(objConfiguration.getYarnPalette()[i].getIntYarnHProbability());
                    objYarn.setDblYarnPrice(objConfiguration.getYarnPalette()[i].getDblYarnPrice());
                    objYarn.setStrYarnAccess(objConfiguration.getYarnPalette()[i].getStrYarnAccess());
                    objYarn.setStrYarnUser(objConfiguration.getYarnPalette()[i].getStrYarnUser());
                    objYarn.setStrYarnDate(objConfiguration.getYarnPalette()[i].getStrYarnDate());
                    
                    objConfiguration.getYarnPalette()[i+26]=objYarn;
                }
                for(int i=0; i<52; i++){
                    objWeave.getObjConfiguration().getColourPalette()[i]=objWeave.getObjConfiguration().getYarnPalette()[i].getStrYarnColor().substring(1); // removing #
                }
                objWeaveAction.populateColorPalette(objWeave);
                loadColorPalettes();
            }
        });
        paletteColorGP.add(btnWarpWeft, 3, 0, 1, 1);
        
        Button btnWeftWarp = new Button();//objDictionaryAction.getWord("WEFTTOWARP"));
        btnWeftWarp.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPWEFTTOWARP")));
        btnWeftWarp.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left.png"));
        btnWeftWarp.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {  
                String type, symbol;
                for(int i =0; i<26; i++){
                    Yarn objYarn = objConfiguration.getYarnPalette()[i];

                    objYarn.setStrYarnType(objConfiguration.getYarnPalette()[i+26].getStrYarnType());
                    objYarn.setStrYarnName(objConfiguration.getYarnPalette()[i].getStrYarnName());
                    objYarn.setStrYarnColor(objConfiguration.getYarnPalette()[i+26].getStrYarnColor());
                    objYarn.setIntYarnRepeat(objConfiguration.getYarnPalette()[i+26].getIntYarnRepeat());
                    objYarn.setStrYarnSymbol(objConfiguration.getYarnPalette()[i].getStrYarnSymbol());
                    objYarn.setIntYarnCount(objConfiguration.getYarnPalette()[i+26].getIntYarnCount());
                    objYarn.setStrYarnCountUnit(objConfiguration.getYarnPalette()[i+26].getStrYarnCountUnit());
                    objYarn.setIntYarnPly(objConfiguration.getYarnPalette()[i+26].getIntYarnPly());
                    objYarn.setIntYarnDFactor(objConfiguration.getYarnPalette()[i+26].getIntYarnDFactor());
                    objYarn.setDblYarnDiameter(objConfiguration.getYarnPalette()[i+26].getDblYarnDiameter());
                    objYarn.setIntYarnTwist(objConfiguration.getYarnPalette()[i+26].getIntYarnTwist());
                    objYarn.setStrYarnTModel(objConfiguration.getYarnPalette()[i+26].getStrYarnTModel());
                    objYarn.setIntYarnHairness(objConfiguration.getYarnPalette()[i+26].getIntYarnHairness());
                    objYarn.setIntYarnHProbability(objConfiguration.getYarnPalette()[i+26].getIntYarnHProbability());
                    objYarn.setDblYarnPrice(objConfiguration.getYarnPalette()[i+26].getDblYarnPrice());
                    objYarn.setStrYarnAccess(objConfiguration.getYarnPalette()[i+26].getStrYarnAccess());
                    objYarn.setStrYarnUser(objConfiguration.getYarnPalette()[i+26].getStrYarnUser());
                    objYarn.setStrYarnDate(objConfiguration.getYarnPalette()[i+26].getStrYarnDate());
                    
                    objConfiguration.getYarnPalette()[i]=objYarn;
                }
                for(int i=0; i<52; i++){
                    objWeave.getObjConfiguration().getColourPalette()[i]=objWeave.getObjConfiguration().getYarnPalette()[i].getStrYarnColor().substring(1); // removing #
                }
                objWeaveAction.populateColorPalette(objWeave);
                loadColorPalettes();
            }
        });
        paletteColorGP.add(btnWeftWarp, 4, 0, 1, 1);        
        
        paletteColorGP.add(new Label(objDictionaryAction.getWord("WARP")), 0, 1, 1, 1);
        paletteColorGP.add(new Label(objDictionaryAction.getWord("WEFT")), 0, 2, 1, 1);
        
        editThreadGP = new GridPane();
        editThreadGP.setAlignment(Pos.CENTER);
        editThreadGP.setHgap(2);
        editThreadGP.setVgap(2);
        editThreadGP.setCursor(Cursor.HAND);
        //editThreadGP.getChildren().clear();
        loadColorPalettes(); // it sets colorPalates[][] using threadPalettes
        paletteColorGP.add(editThreadGP, 1, 1, 4, 2);        
    }
   private void loadColorPalettes(){
        //objWeave.getObjConfiguration().strThreadPalettes = objWeave.getObjConfiguration().strThreadPalettes;
        String[][] colorPaletes = new String[52][2];
        colorPaletes[0][0]="A";
        colorPaletes[0][1]=objWeave.getObjConfiguration().getColourPalette()[0];
        colorPaletes[1][0]="B";
        colorPaletes[1][1]=objWeave.getObjConfiguration().getColourPalette()[1];
        colorPaletes[2][0]="C";
        colorPaletes[2][1]=objWeave.getObjConfiguration().getColourPalette()[2];
        colorPaletes[3][0]="D";
        colorPaletes[3][1]=objWeave.getObjConfiguration().getColourPalette()[3];
        colorPaletes[4][0]="E";
        colorPaletes[4][1]=objWeave.getObjConfiguration().getColourPalette()[4];
        colorPaletes[5][0]="F";
        colorPaletes[5][1]=objWeave.getObjConfiguration().getColourPalette()[5];
        colorPaletes[6][0]="G";
        colorPaletes[6][1]=objWeave.getObjConfiguration().getColourPalette()[6];
        colorPaletes[7][0]="H";
        colorPaletes[7][1]=objWeave.getObjConfiguration().getColourPalette()[7];
        colorPaletes[8][0]="I";
        colorPaletes[8][1]=objWeave.getObjConfiguration().getColourPalette()[8];
        colorPaletes[9][0]="J";
        colorPaletes[9][1]=objWeave.getObjConfiguration().getColourPalette()[9];
        colorPaletes[10][0]="K";
        colorPaletes[10][1]=objWeave.getObjConfiguration().getColourPalette()[10];
        colorPaletes[11][0]="L";
        colorPaletes[11][1]=objWeave.getObjConfiguration().getColourPalette()[11];
        colorPaletes[12][0]="M";
        colorPaletes[12][1]=objWeave.getObjConfiguration().getColourPalette()[12];
        colorPaletes[13][0]="N";
        colorPaletes[13][1]=objWeave.getObjConfiguration().getColourPalette()[13];
        colorPaletes[14][0]="O";
        colorPaletes[14][1]=objWeave.getObjConfiguration().getColourPalette()[14];
        colorPaletes[15][0]="P";
        colorPaletes[15][1]=objWeave.getObjConfiguration().getColourPalette()[15];
        colorPaletes[16][0]="Q";
        colorPaletes[16][1]=objWeave.getObjConfiguration().getColourPalette()[16];
        colorPaletes[17][0]="R";
        colorPaletes[17][1]=objWeave.getObjConfiguration().getColourPalette()[17];
        colorPaletes[18][0]="S";
        colorPaletes[18][1]=objWeave.getObjConfiguration().getColourPalette()[18];
        colorPaletes[19][0]="T";
        colorPaletes[19][1]=objWeave.getObjConfiguration().getColourPalette()[19];
        colorPaletes[20][0]="U";
        colorPaletes[20][1]=objWeave.getObjConfiguration().getColourPalette()[20];
        colorPaletes[21][0]="V";
        colorPaletes[21][1]=objWeave.getObjConfiguration().getColourPalette()[21];
        colorPaletes[22][0]="W";
        colorPaletes[22][1]=objWeave.getObjConfiguration().getColourPalette()[22];
        colorPaletes[23][0]="X";
        colorPaletes[23][1]=objWeave.getObjConfiguration().getColourPalette()[23];
        colorPaletes[24][0]="Y";
        colorPaletes[24][1]=objWeave.getObjConfiguration().getColourPalette()[24];
        colorPaletes[25][0]="Z";
        colorPaletes[25][1]=objWeave.getObjConfiguration().getColourPalette()[25];
        colorPaletes[26][0]="a";
        colorPaletes[26][1]=objWeave.getObjConfiguration().getColourPalette()[26];
        colorPaletes[27][0]="b";
        colorPaletes[27][1]=objWeave.getObjConfiguration().getColourPalette()[27];
        colorPaletes[28][0]="c";
        colorPaletes[28][1]=objWeave.getObjConfiguration().getColourPalette()[28];
        colorPaletes[29][0]="d";
        colorPaletes[29][1]=objWeave.getObjConfiguration().getColourPalette()[29];
        colorPaletes[30][0]="e";
        colorPaletes[30][1]=objWeave.getObjConfiguration().getColourPalette()[30];
        colorPaletes[31][0]="f";
        colorPaletes[31][1]=objWeave.getObjConfiguration().getColourPalette()[31];
        colorPaletes[32][0]="g";
        colorPaletes[32][1]=objWeave.getObjConfiguration().getColourPalette()[32];
        colorPaletes[33][0]="h";
        colorPaletes[33][1]=objWeave.getObjConfiguration().getColourPalette()[33];
        colorPaletes[34][0]="i";
        colorPaletes[34][1]=objWeave.getObjConfiguration().getColourPalette()[34];
        colorPaletes[35][0]="j";
        colorPaletes[35][1]=objWeave.getObjConfiguration().getColourPalette()[35];
        colorPaletes[36][0]="k";
        colorPaletes[36][1]=objWeave.getObjConfiguration().getColourPalette()[36];
        colorPaletes[37][0]="l";
        colorPaletes[37][1]=objWeave.getObjConfiguration().getColourPalette()[37];
        colorPaletes[38][0]="m";
        colorPaletes[38][1]=objWeave.getObjConfiguration().getColourPalette()[38];
        colorPaletes[39][0]="n";
        colorPaletes[39][1]=objWeave.getObjConfiguration().getColourPalette()[39];
        colorPaletes[40][0]="o";
        colorPaletes[40][1]=objWeave.getObjConfiguration().getColourPalette()[40];
        colorPaletes[41][0]="p";
        colorPaletes[41][1]=objWeave.getObjConfiguration().getColourPalette()[41];
        colorPaletes[42][0]="q";
        colorPaletes[42][1]=objWeave.getObjConfiguration().getColourPalette()[42];
        colorPaletes[43][0]="r";
        colorPaletes[43][1]=objWeave.getObjConfiguration().getColourPalette()[43];
        colorPaletes[44][0]="s";
        colorPaletes[44][1]=objWeave.getObjConfiguration().getColourPalette()[44];
        colorPaletes[45][0]="t";
        colorPaletes[45][1]=objWeave.getObjConfiguration().getColourPalette()[45];
        colorPaletes[46][0]="u";
        colorPaletes[46][1]=objWeave.getObjConfiguration().getColourPalette()[46];
        colorPaletes[47][0]="v";
        colorPaletes[47][1]=objWeave.getObjConfiguration().getColourPalette()[47];
        colorPaletes[48][0]="w";
        colorPaletes[48][1]=objWeave.getObjConfiguration().getColourPalette()[48];
        colorPaletes[49][0]="x";
        colorPaletes[49][1]=objWeave.getObjConfiguration().getColourPalette()[49];
        colorPaletes[50][0]="y";
        colorPaletes[50][1]=objWeave.getObjConfiguration().getColourPalette()[50];
        colorPaletes[51][0]="z";
        colorPaletes[51][1]=objWeave.getObjConfiguration().getColourPalette()[51];

        editThreadGP.getChildren().clear();
        
        Separator sepHor = new Separator();
        sepHor.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHor, 0, 2);
        GridPane.setColumnSpan(sepHor, 13);
        editThreadGP.getChildren().add(sepHor);
                
        for(int i=0; i<(int)objWeave.getObjConfiguration().getColourPalette().length; i++){
            final int markedIndex=i;
            final Label lblC= new Label(colorPaletes[i][0]);
            lblC.setUserData(i);
            lblC.setPrefSize(boxSize,boxSize);
            lblC.setStyle("-fx-background-color: #"+objWeave.getObjConfiguration().getColourPalette()[i]+"; -fx-font-size: 10; -fx-width:25px; -fx-height:25px;-fx-border-width: 1;  -fx-border-color: black;");
            lblC.setTooltip(new Tooltip("Hex Code: #"+objWeave.getObjConfiguration().getColourPalette()[i]+"\nRGB: "+
                                    Integer.valueOf( objWeave.getObjConfiguration().getColourPalette()[i].substring( 0, 2 ), 16 )+","+
                                    Integer.valueOf( objWeave.getObjConfiguration().getColourPalette()[i].substring( 2, 4 ), 16 )+","+
                                    Integer.valueOf( objWeave.getObjConfiguration().getColourPalette()[i].substring( 4, 6 ), 16 )));
            setLabelPropertyEvent(lblC);
            if(marked[markedIndex]==1)
                lblC.setText(lblC.getText().trim().substring(0,1)+"*");
            if((byte)((i/26)+1)==2){
                lblC.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        if (event.getClickCount() >1) {
                            editYarnPalette();
                        } else {
                            lblC.setText(lblC.getText().trim().substring(0,1)+"*");
                            String strWeftColor = lblC.getStyle().substring(lblC.getStyle().lastIndexOf("-fx-background-color:")+21,lblC.getStyle().indexOf(";")).trim();
                            intWeftColor = Integer.parseInt(lblC.getUserData().toString());
                            marked[markedIndex]=1;
                        }
                   }
               });
            }else if((byte)((i/26)+1)==1){
                lblC.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        if (event.getClickCount() >1) {
                            editYarnPalette();
                        } else {
                            lblC.setText(lblC.getText().trim().substring(0,1)+"*");
                            String strWarpColor = lblC.getStyle().substring(lblC.getStyle().lastIndexOf("-fx-background-color:")+21,lblC.getStyle().indexOf(";")).trim();
                            intWarpColor = Integer.parseInt(lblC.getUserData().toString());
                            marked[markedIndex]=1;
                       }
                    }
               });
            }
            int colp = (i>25)?(i/13)+1:(i/13);
            editThreadGP.add(lblC, (i%13), colp);
        }
   }
   private void setLabelPropertyEvent(final Label lblC){
        
        lblC.setOnDragDetected(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                /* drag was detected, start a drag-and-drop gesture*/
                /* allow any transfer mode */
                Dragboard db = lblC.startDragAndDrop(TransferMode.ANY);

                /* Put a string on a dragboard */
                ClipboardContent content = new ClipboardContent();
                content.putString(lblC.getStyle().substring(lblC.getStyle().lastIndexOf("-fx-background-color:")+21,lblC.getStyle().indexOf(";")).trim());
                db.setContent(content);
                xindex = (byte)Integer.parseInt(lblC.getUserData().toString());
                event.consume();                   
            }
        });
        lblC.setOnDragOver(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                /* data is dragged over the target */
                /* accept it only if it is not dragged from the same node 
                 * and if it has a string data */
                if (event.getGestureSource() != lblC && event.getDragboard().hasString()) {
                    /* allow for both copying and moving, whatever user chooses */
                    event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                }
                event.consume();
            }
        });
        lblC.setOnDragEntered(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
            /* the drag-and-drop gesture entered the target */
            /* show to the user that it is an actual gesture target */
                 if (event.getGestureSource() != lblC && event.getDragboard().hasString()) {
                     lblC.setTextFill(Color.GREEN);
                 }
                 event.consume();
            }
        });
        lblC.setOnDragExited(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                /* mouse moved away, remove the graphical cues */
                lblC.setTextFill(Color.BLACK);
                event.consume();
            }
        });
        lblC.setOnDragDropped(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                /* data dropped */
                /* if there is a string data on dragboard, read it and use it */
                Dragboard db = event.getDragboard();
                boolean success = false;
                if (db.hasString()) {                    
                   ClipboardContent content = new ClipboardContent();
                   content.putString(lblC.getStyle().substring(lblC.getStyle().lastIndexOf("-fx-background-color:")+21,lblC.getStyle().indexOf(";")).trim());                
                   lblC.setStyle("-fx-background-color: "+db.getString()+"; -fx-font-size: 10; -fx-width:25px; -fx-height:25px;");
                   yindex = (byte)Integer.parseInt(lblC.getUserData().toString());
                   
                   db.setContent(content);                   
                   success = true;                   
                }
                /* let the source know whether the string was successfully 
                 * transferred and used */
                event.setDropCompleted(success);
                event.consume();
             }
        });
        lblC.setOnDragDone(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                /* the drag and drop gesture ended */
                /* if the data was successfully moved, clear it */
                if (event.getTransferMode() == TransferMode.MOVE) {
                    Dragboard db = event.getDragboard();
                    // swapThreadPatterns(lblC.getStyle().substring(lblC.getStyle().lastIndexOf("-fx-background-color:")+21,lblC.getStyle().indexOf(";")).trim(),db.getString());              
                    lblC.setStyle("-fx-background-color: "+db.getString()+"; -fx-font-size: 10; -fx-width:25px; -fx-height:25px;");
                    //swapThreadPalette();
                    String temp=objWeave.getObjConfiguration().getColourPalette()[xindex];
                    objWeave.getObjConfiguration().getColourPalette()[xindex]=objWeave.getObjConfiguration().getColourPalette()[yindex];
                    objWeave.getObjConfiguration().getColourPalette()[yindex]=temp;
                    objWeaveAction.populateYarnPalette(objWeave);
                    populateNplotWarpWeftYarn();  
                }
                event.consume();
            }
        });
    }
    private void colourwaysAction(){
        Weave objWeaveClone=cloneWeave();
        WeaveColorwaysView objWeaveColorwaysView=new WeaveColorwaysView(objWeaveClone);
    }
    private void initObjWeave(boolean mode){
        //objWeave.setStrThreadPalettes(objWeave.getObjConfiguration().strThreadPalettes);
        objWeaveAction.populateYarnPalette(objWeave);
        if(!mode){
            objWeave.setIntWarp(activeGridCol);
            objWeave.setIntWeft(activeGridRow);
            objWeave.setIntWeaveShaft(activeGridRow);
            objWeave.setIntWeaveTreadles(activeGridCol);          
                        
            warpYarn = new Yarn[activeGridCol];
            for(int i=0; i<activeGridCol; i++)
                warpYarn[i] = new Yarn(null, "Warp", objConfiguration.getStrWarpName(), "#"+objConfiguration.getStrWarpColor(), objConfiguration.getIntWarpRepeat(), "A", objConfiguration.getIntWarpCount(), objConfiguration.getStrWarpUnit(), objConfiguration.getIntWarpPly(), objConfiguration.getIntWarpFactor(), objConfiguration.getDblWarpDiameter(), objConfiguration.getIntWarpTwist(), objConfiguration.getStrWarpSence(), objConfiguration.getIntWarpHairness(), objConfiguration.getIntWarpDistribution(), objConfiguration.getDblWarpPrice(), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);
            objWeave.setWarpYarn(warpYarn);
            weftYarn = new Yarn[activeGridRow];
            for(int i=0; i<activeGridRow; i++)
                weftYarn[i] = new Yarn(null, "Weft", objConfiguration.getStrWeftName(), "#"+objConfiguration.getStrWeftColor(), objConfiguration.getIntWeftRepeat(), "a", objConfiguration.getIntWeftCount(), objConfiguration.getStrWeftUnit(), objConfiguration.getIntWeftPly(), objConfiguration.getIntWeftFactor(), objConfiguration.getDblWeftDiameter(), objConfiguration.getIntWeftTwist(), objConfiguration.getStrWeftSence(), objConfiguration.getIntWeftHairness(), objConfiguration.getIntWeftDistribution(), objConfiguration.getDblWeftPrice(), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);
            objWeave.setWeftYarn(weftYarn);
        } else{            
            warpYarn = new Yarn[objWeave.getIntWarp()];
            for(int i=0; i<objWeave.getIntWarp(); i++){
                if(i<objWeave.getWarpYarn().length && objWeave.getWarpYarn()[i]!=null)
                    warpYarn[i]=objWeave.getWarpYarn()[i];
                else
                    warpYarn[i] = new Yarn(null, "Warp", objConfiguration.getStrWarpName(), "#"+objConfiguration.getStrWarpColor(), objConfiguration.getIntWarpRepeat(), "A", objConfiguration.getIntWarpCount(), objConfiguration.getStrWarpUnit(), objConfiguration.getIntWarpPly(), objConfiguration.getIntWarpFactor(), objConfiguration.getDblWarpDiameter(), objConfiguration.getIntWarpTwist(), objConfiguration.getStrWarpSence(), objConfiguration.getIntWarpHairness(), objConfiguration.getIntWarpDistribution(), objConfiguration.getDblWarpPrice(), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);
            }
            objWeave.setWarpYarn(warpYarn);
            weftYarn = new Yarn[objWeave.getIntWeft()];
            for(int i=0; i<objWeave.getIntWeft(); i++){
                if(i<objWeave.getWeftYarn().length&&objWeave.getWeftYarn()[i]!=null){
                    weftYarn[objWeave.getIntWeft()-1-i]=objWeave.getWeftYarn()[objWeave.getWeftYarn().length-1-i];
                }   
                else{
                    weftYarn[objWeave.getIntWeft()-1-i] = new Yarn(null, "Weft", objConfiguration.getStrWeftName(), "#"+objConfiguration.getStrWeftColor(), objConfiguration.getIntWeftRepeat(), "a", objConfiguration.getIntWeftCount(), objConfiguration.getStrWeftUnit(), objConfiguration.getIntWeftPly(), objConfiguration.getIntWeftFactor(), objConfiguration.getDblWeftDiameter(), objConfiguration.getIntWeftTwist(), objConfiguration.getStrWeftSence(), objConfiguration.getIntWeftHairness(), objConfiguration.getIntWeftDistribution(), objConfiguration.getDblWeftPrice(), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);
                }
            }   
            objWeave.setWeftYarn(weftYarn);
        }
        objWeave.setShaftMatrix(new byte[objWeave.getIntWeaveShaft()][objWeave.getIntWarp()]);
        objWeave.setTreadlesMatrix(new byte[objWeave.getIntWeft()][objWeave.getIntWeaveTreadles()]);
        objWeave.setTieupMatrix(new byte[objWeave.getIntWeaveShaft()][objWeave.getIntWeaveTreadles()]);
        objWeave.setPegMatrix(new byte[objWeave.getIntWeft()][objWeave.getIntWeaveShaft()]);
        objWeave.setDesignMatrix(new byte[objWeave.getIntWeft()][objWeave.getIntWarp()]);
        byte[][] dentMatrix=new byte[2][objWeave.getIntWarp()];
        for(int c=0; c<objWeave.getIntWarp(); c++){
            dentMatrix[0][c]=(byte)(c%2);
            dentMatrix[1][c]=(byte)((c+1)%2);
        }
        objWeave.setDentMatrix(dentMatrix);
        
        for(int i=0; i<objWeave.getWarpYarn().length; i++){
            for(int j=0; j<26; j++)
                if(objWeave.getWarpYarn()[i].getStrYarnSymbol().equals(objWeave.getObjConfiguration().getYarnPalette()[j].getStrYarnSymbol()))
                    objWeave.getWarpYarn()[i]=objWeave.getObjConfiguration().getYarnPalette()[j];
        }
        for(int i=0; i<objWeave.getWeftYarn().length; i++){
            for(int j=26; j<52; j++)
                if(objWeave.getWeftYarn()[i].getStrYarnSymbol().equals(objWeave.getObjConfiguration().getYarnPalette()[j].getStrYarnSymbol()))
                    objWeave.getWeftYarn()[i]=objWeave.getObjConfiguration().getYarnPalette()[j];
        }

        objWeaveAction.populateColorPalette(objWeave);
        objWeave.setDesignMatrix(getDotDesignMatrix());
        plotDesign();
        plotShaft();
        plotPeg();
        plotTieUp();
        plotTradeles();
        plotWarpColor();
        plotWeftColor();
        plotDenting();
        plotColorPalette();
        
        isNew = true;
        isWorkingMode = true;
        //menuHighlight(null);                
    }
    
    private void updateObjWeave(){
        objWeave.setDesignMatrix(getDotDesignMatrix());
    }    
    // design image    
    /**
     * Resets design grid by reading horizontally mirrored dot
     * @param col
     * @param row 
     */
    private void resetActiveDesignGrid(int col, int row){
        if(designImage==null)
            return;
        clearImage(designImage);
        for(int a=0; a<row; a++){
            for(int b=0; b<col; b++){
                fillImageDotPixels(new Point(b, (MAX_GRID_ROW-1)-a), INT_SOLID_WHITE, designImage);
            }
        }
    }    
    private void updateActiveDesignGrid(int newCol, int newRow){
        initial_row=initial_col=final_row=final_col=0; //to release selection on design pane resize
        if(newCol>activeGridCol){ // column(s) extended
            int addedCol=newCol-activeGridCol;
            for(int a=0; a<activeGridRow; a++){
                for(int b=0; b<addedCol; b++){
                    fillImageDotPixels(new Point(activeGridCol+b, (MAX_GRID_ROW-1)-a), INT_SOLID_WHITE, designImage);
                }
            }
            activeGridCol=newCol;
        }
        else if(newCol<activeGridCol){ // column(s) truncated
            int truncatedCol=activeGridCol-newCol;
            for(int a=0; a<activeGridRow; a++){
                for(int b=0; b<truncatedCol; b++){
                    fillImageDotPixels(new Point(activeGridCol-(b+1), (MAX_GRID_ROW-1)-a), INT_TRANSPARENT, designImage);
                }
            }
            activeGridCol=newCol;
        }
        
        if(newRow>activeGridRow){ // row(s) extended
            int addedRow=newRow-activeGridRow;
            for(int a=0; a<addedRow; a++){
                for(int b=0; b<activeGridCol; b++){
                    fillImageDotPixels(new Point(b, (MAX_GRID_ROW-1)-(activeGridRow+a)), INT_SOLID_WHITE, designImage);
                }
            }
            activeGridRow=newRow;
        }
        else if(newRow<activeGridRow){ // row(s) truncated
            int truncatedRow=activeGridRow-newRow;
            for(int a=0; a<truncatedRow; a++){
                for(int b=0; b<activeGridCol; b++){
                    fillImageDotPixels(new Point(b, (MAX_GRID_ROW-1)-(activeGridRow-a)+1), INT_TRANSPARENT, designImage);
                }
            }
            activeGridRow=newRow;
        }
        hSlider.setValue(activeGridCol);
        vSlider.setValue(activeGridRow);
        objWeave.setIntWarp(activeGridCol);
        objWeave.setIntWeft(activeGridRow);
        if(activeGridRow<=current_row)
            current_row=0;
        if(activeGridCol<=current_col)
            current_col=0;
        
        refreshDesignImage();
        //objWeave.setDesignMatrix(getDotDesignMatrix());
        initObjWeave(true);
        disableWarpWeftDelete();
        Weave objClonedWeave=cloneWeave();
        objUR.doCommand("Redrawing Design Pane", objClonedWeave);
        populateNplot();
        //plotWarpColor();
        System.gc();
    }    
    private void clearImage(BufferedImage image){
        int row=MAX_GRID_ROW;
        int col=MAX_GRID_COL;
        if(image.equals(dentingImage))
            row=2;
        else if(image.equals(warpColorImage))
            row=1;
        else if(image.equals(weftColorImage))
            col=1;
        
        for(int a=0; a<row; a++)
            for(int b=0; b<col; b++)
                fillImageDotPixels(new Point(b, a), INT_TRANSPARENT, image);
    }    
    private void refreshDesignImage(){
        designIV.setImage(SwingFXUtils.toFXImage(designImage, null));
    }    
    private void refreshShaftImage(){
        shaftIV.setImage(SwingFXUtils.toFXImage(shaftImage, null));
    }    
    private void refreshPegImage(){
        pegIV.setImage(SwingFXUtils.toFXImage(pegImage, null));
    }    
    private void refreshTieUpImage(){
        tieUpIV.setImage(SwingFXUtils.toFXImage(tieUpImage, null));
    }    
    private void refreshTradelesImage(){
        tradelesIV.setImage(SwingFXUtils.toFXImage(tradelesImage, null));
    }    
    private void refreshDentingImage(){
        dentingIV.setImage(SwingFXUtils.toFXImage(dentingImage, null));
    }    
    private void refreshWarpColorImage(){
        warpColorIV.setImage(SwingFXUtils.toFXImage(warpColorImage, null));
    }    
    private void refreshWeftColorImage(){
        weftColorIV.setImage(SwingFXUtils.toFXImage(weftColorImage, null));
    }    
    private boolean isGridDotActive(Point p){
        if(p.getX()>=0&&(p.getX()<=(activeGridCol-1))&&(p.getY()>=(MAX_GRID_ROW-activeGridRow))&&p.getY()<MAX_GRID_ROW)
            return true;
        else
            return false;
    }    
    private byte[][] getDotDesignMatrix(){
        byte[][] designMatrix=new byte[activeGridRow][activeGridCol];
        Point p=new Point();
        for(int r=0; r<activeGridRow; r++){
            for(int c=0; c<activeGridCol; c++){
                p.x=c;
                p.y=(MAX_GRID_ROW-1)-((activeGridRow-1)-r);
                if(getDotColor(p, designImage)==INT_SOLID_BLACK)
                    designMatrix[r][c]=1;
                else if(getDotColor(p, designImage)==INT_SOLID_WHITE)
                    designMatrix[r][c]=0;
            }
        }
        return designMatrix;
    }    
    private void drawRepeatImageFromMatrix(byte[][] matrix, BufferedImage image){
        if(matrix==null||matrix.length==0||image.equals(tieUpImage))
            return;
        clearImage(image);
        Point p=new Point();
        int row=matrix.length;
        int col=matrix[0].length;
        int maxCountRow=0;
        int maxCountCol=0;
        if(image.equals(designImage)){
            maxCountRow=MAX_GRID_ROW;
            maxCountCol=MAX_GRID_COL;
        }
        else if(image.equals(shaftImage)){
            maxCountRow=row;
            maxCountCol=MAX_GRID_COL;
        }
        else if(image.equals(pegImage)){
            maxCountRow=MAX_GRID_ROW;
            maxCountCol=col;
        }
        else if(image.equals(tradelesImage)){
            maxCountRow=MAX_GRID_ROW;
            maxCountCol=col;
        }
        else if(image.equals(dentingImage)){
            maxCountRow=2;
            maxCountCol=MAX_GRID_COL;
        }
        for(int r=maxCountRow-1; r>=0; r--){
            for(int c=0; c<=maxCountCol-1; c++){
                p.x=c;
                p.y=r;
                if(image.equals(shaftImage)){
                    if(r>(row-1))
                        return;
                    else
                        p.y=(MAX_GRID_ROW-1)-((row-1)-r);
                }
                //if(matrix[(activeGridRow-1)-(((MAX_GRID_ROW-1)-r)%activeGridRow)][c%activeGridCol]==1){
                if(image.equals(dentingImage)){
                    if(matrix[(row-1)-(((maxCountRow-1)-r)%row)][c%col]==1){
                        if(p.y>=0&&p.y<=1&&p.x>=0&&p.x<activeGridCol)
                            fillImageDotPixels(p, INT_SOLID_BLACK, image);
                        else
                            fillImageDotPixels(p, INT_SOLID_GREY, image);
                    }
                    else if(matrix[(row-1)-(((maxCountRow-1)-r)%row)][c%col]==0){
                        if(p.y>=0&&p.y<=1&&p.x>=0&&p.x<activeGridCol)
                            fillImageDotPixels(p, INT_SOLID_WHITE, image);
                        else
                            fillGreyedDotPixels(p, INT_SOLID_WHITE, image);
                    }
                }
                else{
                    if(matrix[(row-1)-(((maxCountRow-1)-r)%row)][c%col]==1){
                        if(isGridDotActive(p))
                            fillImageDotPixels(p, INT_SOLID_BLACK, image);
                        else
                            fillImageDotPixels(p, INT_SOLID_GREY, image);
                    }
                    else if(matrix[(row-1)-(((maxCountRow-1)-r)%row)][c%col]==0){
                        if(isGridDotActive(p))
                            fillImageDotPixels(p, INT_SOLID_WHITE, image);
                        else
                            fillGreyedDotPixels(p, INT_SOLID_WHITE, image);
                    }
                }
            }
        }
        refreshImage(image);
    }    
    private void refreshImage(BufferedImage image){
        if(image.equals(designImage))
            refreshDesignImage();
        else if(image.equals(shaftImage))
            refreshShaftImage();
        else if(image.equals(pegImage))
            refreshPegImage();
        else if(image.equals(tieUpImage))
            refreshTieUpImage();
        else if(image.equals(tradelesImage))
            refreshTradelesImage();
        else if(image.equals(warpColorImage))
            refreshWarpColorImage();
        else if(image.equals(weftColorImage))
            refreshWeftColorImage();
        else if(image.equals(dentingImage))
            refreshDentingImage();
    }    
    private void drawImageFromMatrix(byte[][] matrix, BufferedImage image){
        if(matrix==null||matrix.length==0)
            return;
        clearImage(image);
        int row=matrix.length;
        int col=matrix[0].length;
        if(image.equals(designImage)){
            activeGridRow=row;
            activeGridCol=col;
            if(activeGridRow<=current_row)
                current_row=0;
            if(activeGridCol<=current_col)
                current_col=0;
        }
        Point p=new Point();
        for(int r=0; r<row; r++){
            for(int c=0; c<col; c++){
                p.x=c;
                p.y=(MAX_GRID_ROW-1)-((row-1)-r);
                if(image.equals(dentingImage))
                    p.y=r;
                if(matrix[r][c]==1)
                    fillImageDotPixels(p, INT_SOLID_BLACK, image);
                else if(matrix[r][c]==0)
                    fillImageDotPixels(p, INT_SOLID_WHITE, image);
            }
        }
        if(image.equals(designImage)){
            hSlider.setValue(activeGridCol);
            vSlider.setValue(activeGridRow);
        }
        refreshImage(image);
    }    
    // pixel level operations
    private void fillDotBorder(Point p, BufferedImage image){
        int limit=(int)(10*currentZoomFactor);
        for(int b=1; b<limit-1; b++){
            image.setRGB((int)(10*p.x*currentZoomFactor)+b, (int)(10*p.y*currentZoomFactor)+1, INT_SOLID_RED);
            image.setRGB((int)(10*p.x*currentZoomFactor)+b, (int)(10*p.y*currentZoomFactor)+limit-2, INT_SOLID_RED);
            image.setRGB((int)(10*p.x*currentZoomFactor)+1, (int)(10*p.y*currentZoomFactor)+b, INT_SOLID_RED);
            image.setRGB((int)(10*p.x*currentZoomFactor)+limit-2, (int)(10*p.y*currentZoomFactor)+b, INT_SOLID_RED);
        }
    }    
    private void invertDotColor(Point p, BufferedImage image){
        if(p.x<0||p.y<0||p.x>=MAX_GRID_COL||p.y>=MAX_GRID_ROW)
            return;
        if(image.getRGB((int)(10*p.getX()*currentZoomFactor)+2, (int)(10*p.getY()*currentZoomFactor)+2)==INT_SOLID_BLACK)
            fillImageDotPixels(p, INT_SOLID_WHITE, image);
        else if(image.getRGB((int)(10*p.getX()*currentZoomFactor)+2, (int)(10*p.getY()*currentZoomFactor)+2)==INT_SOLID_WHITE)
            fillImageDotPixels(p, INT_SOLID_BLACK, image);
    }    
    private int getDotColor(Point p, BufferedImage image){
        if(p.x<0||p.y<0||p.x>=MAX_GRID_COL||p.y>=MAX_GRID_ROW)
            return INT_SOLID_WHITE;
        return image.getRGB((int)(10*p.getX()*currentZoomFactor)+2, (int)(10*p.getY()*currentZoomFactor)+2);
    }    
    /**
     * Fills multiple pixels (contained in a dot point) inside an image
     * P.x: col, P.y:row of Dot Point
     * @author Aatif Ahmad Khan
     * @param p Dot point to be filled
     * @param image Image on which Dot point needs to be filled
     * @param color Fill color
     */
    private void fillImageDotPixels(Point p, int color, BufferedImage image){
        int limit=(int)(10*currentZoomFactor);
        for(int a=1; a<limit-1; a++)
            for(int b=1; b<limit-1; b++)
                image.setRGB((int)(10*p.x*currentZoomFactor)+b, (int)(10*p.y*currentZoomFactor)+a, color);
    }
    
    private void fillGreyedDotPixels(Point p, int color, BufferedImage image){
        int limit=(int)(10*currentZoomFactor);
        for(int a=1; a<limit-1; a++)
            for(int b=1; b<limit-1; b++)
                image.setRGB((int)(10*p.x*currentZoomFactor)+b, (int)(10*p.y*currentZoomFactor)+a, color);
        for(int b=1; b<limit-1; b++){
            image.setRGB((int)(10*p.x*currentZoomFactor)+b, (int)(10*p.y*currentZoomFactor)+1, INT_TRANSPARENT);
            image.setRGB((int)(10*p.x*currentZoomFactor)+b, (int)(10*p.y*currentZoomFactor)+limit-2, INT_TRANSPARENT);
            image.setRGB((int)(10*p.x*currentZoomFactor)+1, (int)(10*p.y*currentZoomFactor)+b, INT_TRANSPARENT);
            image.setRGB((int)(10*p.x*currentZoomFactor)+limit-2, (int)(10*p.y*currentZoomFactor)+b, INT_TRANSPARENT);
        }
    }
    
    /**
     * Returns actual dot on grid
     * @param pixelX
     * @param pixelY
     * @return Dot on grid
     */
    private Point getDotFromPixels(int pixelX, int pixelY){
        return new Point((int)(pixelX/(10*currentZoomFactor)), (int)(pixelY/(10*currentZoomFactor)));
    }    
    // utilities    
    /**
     * Returns all rectangular corner points coordinates
     * @author Aatif Ahmad Khan
     * @param xIntial drag start
     * @param yInitial drag start
     * @param xFinal drag end
     * @param yFinal drag end
     * @return all four points
     * (x1, y1)--------------(x4, y4)
     *        |              |
     *        |              |
     * (x2, y2)--------------(x3, y3)
     */
    private static int[][] getRectCorner(int xIntial, int yIntial, int xFinal, int yFinal){
        return new int[][]{
            {(xFinal>xIntial?xIntial:xFinal), (yFinal>yIntial?yIntial:yFinal)},
            {(xFinal>xIntial?xIntial:xFinal), (yFinal>yIntial?yFinal:yIntial)},
            {(xFinal>xIntial?xFinal:xIntial), (yFinal>yIntial?yFinal:yIntial)},
            {(xFinal>xIntial?xFinal:xIntial), (yFinal>yIntial?yIntial:yFinal)}
        };
    }
    
    /**
     * Returns solid color alpha=0xFF
     * @param red red value (0-255)
     * @param green value
     * @param blue
     * @return 
     */
    private int getIntRgbFromColor(int alpha, int red, int green, int blue){
        int rgb=alpha;
        rgb=(rgb << 8)+red;
        rgb=(rgb << 8)+green;
        rgb=(rgb << 8)+blue;
        return rgb;
    }
    
    private void checkDotInRow(Point dot){
        int rowInMatrix=(objWeave.getTreadlesMatrix().length-1)-((MAX_GRID_ROW-1)-dot.y);
        for(int c=0; c<objWeave.getTreadlesMatrix()[rowInMatrix].length; c++){
            if(objWeave.getTreadlesMatrix()[rowInMatrix][c]==1){
                dot.x=c;
                invertDotColor(dot, tradelesImage);
                objWeave.getTreadlesMatrix()[rowInMatrix][c]=0;
                break;
            }
        }
    }
    
    private void checkDotInCol(Point dot){
        for(int r=0; r<objWeave.getShaftMatrix().length; r++){
            if(objWeave.getShaftMatrix()[r][dot.x]==1){
                dot.y=(MAX_GRID_ROW-1)-(objWeave.getShaftMatrix().length-1)+r;
                invertDotColor(dot, shaftImage);
                objWeave.getShaftMatrix()[r][dot.x]=0;
                break;
            }
        }
    }
    
    private int getDesignMatrixRow(int dotRow){
        return (activeGridRow-1)-((MAX_GRID_ROW-1)-dotRow);
    }
    
    private void zoomImages(double zoomFactor){
        /*setZoomableScrollPane(designSP, designIV, zoomFactor);
        setZoomableScrollPane(dentingSP, dentingIV, zoomFactor);
        setZoomableScrollPane(shaftSP, shaftIV, zoomFactor);
        setZoomableScrollPane(pegSP, pegIV, zoomFactor);
        setZoomableScrollPane(tieUpSP, tieUpIV, zoomFactor);
        setZoomableScrollPane(tradelesSP, tradelesIV, zoomFactor);
        setZoomableScrollPane(warpColorSP, warpColorIV, zoomFactor);
        setZoomableScrollPane(weftColorSP, weftColorIV, zoomFactor);
        setZoomableScrollPane(hSliderSP, hSlider, zoomFactor);
        setZoomableScrollPane(vSliderSP, vSlider, zoomFactor);
        */
        zoomFactor=zoomFactor/currentZoomFactor;
        designIV.setImage(SwingFXUtils.toFXImage(getZoomedImage(designImage, zoomFactor), null));
        designImage=SwingFXUtils.fromFXImage(designIV.getImage(), null);
        shaftIV.setImage(SwingFXUtils.toFXImage(getZoomedImage(shaftImage, zoomFactor), null));
        shaftImage=SwingFXUtils.fromFXImage(shaftIV.getImage(), null);
        dentingIV.setImage(SwingFXUtils.toFXImage(getZoomedImage(dentingImage, zoomFactor), null));
        dentingImage=SwingFXUtils.fromFXImage(dentingIV.getImage(), null);
        warpColorIV.setImage(SwingFXUtils.toFXImage(getZoomedImage(warpColorImage, zoomFactor), null));
        warpColorImage=SwingFXUtils.fromFXImage(warpColorIV.getImage(), null);
        weftColorIV.setImage(SwingFXUtils.toFXImage(getZoomedImage(weftColorImage, zoomFactor), null));
        weftColorImage=SwingFXUtils.fromFXImage(weftColorIV.getImage(), null);
        pegIV.setImage(SwingFXUtils.toFXImage(getZoomedImage(pegImage, zoomFactor), null));
        pegImage=SwingFXUtils.fromFXImage(pegIV.getImage(), null);
        tieUpIV.setImage(SwingFXUtils.toFXImage(getZoomedImage(tieUpImage, zoomFactor), null));
        tieUpImage=SwingFXUtils.fromFXImage(tieUpIV.getImage(), null);
        tradelesIV.setImage(SwingFXUtils.toFXImage(getZoomedImage(tradelesImage, zoomFactor), null));
        tradelesImage=SwingFXUtils.fromFXImage(tradelesIV.getImage(), null);
        hSlider.setPrefWidth(zoomFactor*hSlider.getPrefWidth());
        vSlider.setPrefHeight(zoomFactor*vSlider.getPrefHeight());
        //hSliderSP.setPrefWidth(zoomFactor*hSliderSP.getPrefWidth());
        //vSliderSP.setPrefHeight(zoomFactor*vSliderSP.getPrefHeight());
        designSP.setVmax(designImage.getHeight());
        shaftSP.setVmax(shaftImage.getHeight());
        weftColorSP.setVmax(weftColorImage.getHeight());
        pegSP.setVmax(pegImage.getHeight());
        tieUpSP.setVmax(tieUpImage.getHeight());
        tradelesSP.setVmax(tradelesImage.getHeight());
        vSliderSP.setVmax(vSlider.getPrefHeight());
        currentZoomFactor=zoomFactor*currentZoomFactor;
        vSliderSP.layout();
        setScrollBarAtEnd();
        plotDesign();
        populateNplot();
    }
    
    private BufferedImage getZoomedImage(BufferedImage image, double zoomFactor){
        BufferedImage zoomedImage=new BufferedImage((int)(zoomFactor*image.getWidth()), (int)(zoomFactor*image.getHeight()), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g=zoomedImage.createGraphics();
        g.drawImage(image, 0, 0, zoomedImage.getWidth(), zoomedImage.getHeight(), null);
        g.dispose();
        return zoomedImage;
    }
    
    private void setZoomableScrollPane(ScrollPane sp, Node node, double zoomFactor)
    {
        Group contentGroup = new Group();
        Group zoomGroup = new Group();
        contentGroup.getChildren().add(zoomGroup);
        zoomGroup.getChildren().add(node);
        sp.setContent(contentGroup);
        Scale scaleTransform = new Scale(zoomFactor, zoomFactor, 0, 0);
        zoomGroup.getTransforms().add(scaleTransform);
    }
    
    private void setScrollBarAtEnd(){
        designSP.setVvalue(designSP.getVmax());
        shaftSP.setVvalue(shaftSP.getVmax());
        weftColorSP.setVvalue(weftColorSP.getVmax());
        pegSP.setVvalue(pegSP.getVmax());
        tieUpSP.setVvalue(tieUpSP.getVmax());
        tradelesSP.setVvalue(tradelesSP.getVmax());
        vSliderSP.setVvalue(vSliderSP.getVmax());
    }
    
    public void dentingViewAction(){
        byte[][] dentMat= objWeave.getDentMatrix();
        
        // yarn diameter getYarnDiameter() ->> mm
        
        // this list will contain lists of yarns in dents
        ArrayList<ArrayList<String>> fullList=new ArrayList<>();
        // this list will contain yarns in a dent
        ArrayList<String> cList=new ArrayList<>();
        byte cDent=0;
        for(int w=0; w<objWeave.getIntWarp(); w++){
            if(dentMat[0][w]==(byte)1){
                if(cDent==(byte)0){
                    cList.add(String.valueOf(w));
                    cDent=0;
                }
                else if(cDent==(byte)1){
                    if(cList.size()>0)
                        fullList.add(cList);
                    cList=new ArrayList<>();
                    cList.add(String.valueOf(w));
                    cDent=0;
                }
            }
            else if(dentMat[1][w]==(byte)1){
                if(cDent==(byte)0){
                    if(cList.size()>0)
                        fullList.add(cList);
                    cList=new ArrayList<>();
                    cList.add(String.valueOf(w));
                    cDent=1;
                }
                else if(cDent==(byte)1){
                    cList.add(String.valueOf(w));
                    cDent=1;
                }
            }
        }
        // remain yarns list in last dent
        if(cList.size()>0)
            fullList.add(cList);
        // maxOfYarns will contain overall maximum no. of yarns in a single dent
        int maxOfYarns=1;
        // print info
        //System.err.println("FullList.Length: "+fullList.size());
        for(ArrayList<String> aList: fullList){
            if(aList.size()>maxOfYarns)
                maxOfYarns=aList.size();
            //System.err.println("\nSize of this list: "+aList.size());
            //for(String s: aList){
            //    System.err.print(" "+s);
            //}
        }
        
        double dentSizeMm=getDentSizeInMm(objWeave.getIntEPI()*2);
        render(fullList);
        //printYarnFactors(fullList);
        /*
        // draw image
        int size=15; // size of a dent shown in pixels
        BufferedImage fullBI=new BufferedImage(fullList.size()*size, (objWeave.getIntWeft())*size, BufferedImage.TYPE_INT_RGB);
        BufferedImage seqBI=null;
        Graphics2D g = fullBI.createGraphics();
        Graphics2D gSeq=null;
        int e=0;
        // each dent/weft shown in 15 px, each warp yarn 3 px
        for(int a=0; a<fullList.size(); a++){ // for each dent
            seqBI=new BufferedImage(size, (objWeave.getIntWeft())*size, BufferedImage.TYPE_INT_RGB);
            gSeq=seqBI.createGraphics();
            BufferedImage ssBI=new BufferedImage(fullList.get(a).size()*3, (objWeave.getIntWeft())*size, BufferedImage.TYPE_INT_RGB);
            Color co=Color.web("#FF5680");
            Color wfCo=Color.web("#FF5680");
            int rgb=-1;
            for(int b=0; b<fullList.get(a).size(); b++){ // for each thread in a dent
                co=Color.web(objWeave.getWarpYarn()[e].getStrYarnColor());
                for(int c=0; c<(objWeave.getIntWeft())*size; c+=size){
                    wfCo=Color.web(objWeave.getWeftYarn()[c/size].getStrYarnColor());
                    for(int d=0; d<size; d++){
                        if(objWeave.getDesignMatrix()[c/size][e]==0){
                            if(d<size/3){
                                rgb=new java.awt.Color((float)wfCo.getRed(),(float)wfCo.getGreen(),(float)wfCo.getBlue()).brighter().getRGB();
                                ssBI.setRGB(b*3, c+d, rgb);
                                rgb=new java.awt.Color((float)wfCo.getRed(),(float)wfCo.getGreen(),(float)wfCo.getBlue()).brighter().getRGB();
                                ssBI.setRGB((b*3)+1, c+d, rgb);
                                rgb=new java.awt.Color((float)wfCo.getRed(),(float)wfCo.getGreen(),(float)wfCo.getBlue()).brighter().getRGB();
                                ssBI.setRGB((b*3)+2, c+d, rgb);
                            }
                            else if(d>=(size/3)*2){
                                rgb=new java.awt.Color((float)wfCo.getRed(),(float)wfCo.getGreen(),(float)wfCo.getBlue()).darker().getRGB();
                                ssBI.setRGB(b*3, c+d, rgb);
                                rgb=new java.awt.Color((float)wfCo.getRed(),(float)wfCo.getGreen(),(float)wfCo.getBlue()).darker().getRGB();
                                ssBI.setRGB((b*3)+1, c+d, rgb);
                                rgb=new java.awt.Color((float)wfCo.getRed(),(float)wfCo.getGreen(),(float)wfCo.getBlue()).darker().getRGB();
                                ssBI.setRGB((b*3)+2, c+d, rgb);
                            }
                            else{
                                rgb=new java.awt.Color((float)wfCo.getRed(),(float)wfCo.getGreen(),(float)wfCo.getBlue()).getRGB();
                                ssBI.setRGB(b*3, c+d, rgb);
                                rgb=new java.awt.Color((float)wfCo.getRed(),(float)wfCo.getGreen(),(float)wfCo.getBlue()).getRGB();
                                ssBI.setRGB((b*3)+1, c+d, rgb);
                                rgb=new java.awt.Color((float)wfCo.getRed(),(float)wfCo.getGreen(),(float)wfCo.getBlue()).getRGB();
                                ssBI.setRGB((b*3)+2, c+d, rgb);
                            }
                        }
                        else{
                            rgb=new java.awt.Color((float)co.getRed(),(float)co.getGreen(),(float)co.getBlue()).brighter().getRGB();
                            ssBI.setRGB(b*3, c+d, rgb);
                            rgb=new java.awt.Color((float)co.getRed(),(float)co.getGreen(),(float)co.getBlue()).getRGB();
                            ssBI.setRGB((b*3)+1, c+d, rgb);
                            rgb=new java.awt.Color((float)co.getRed(),(float)co.getGreen(),(float)co.getBlue()).darker().getRGB();
                            ssBI.setRGB((b*3)+2, c+d, rgb);
                        }
                    }
                    
                }
                e++;
            }
            gSeq.drawImage(ssBI, 0, 0, seqBI.getWidth(), seqBI.getHeight(), null);
            g.drawImage(seqBI, a*seqBI.getWidth(), 0, seqBI.getWidth(), seqBI.getHeight(), null);
        }
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        ScrollPane popup=new ScrollPane();
        popup.setPrefHeight(500);
        popup.setPrefWidth(objConfiguration.WIDTH);
        final ImageView weaveIV=new ImageView();
        BufferedImage show=new BufferedImage((int)objConfiguration.WIDTH, (int)objConfiguration.HEIGHT, BufferedImage.TYPE_INT_RGB);
        for(int x=0; x<show.getWidth(); x++){
            for(int y=0; y<show.getHeight(); y++){
                show.setRGB(x, y, fullBI.getRGB(x%fullBI.getWidth(), y%fullBI.getHeight()));
            }
        }
        weaveIV.setImage(SwingFXUtils.toFXImage(show, null));
        popup.setContent(weaveIV);//setCenter(weaveIV);        
        popup.setId("popup");
        Scene popupScene = new Scene(popup);
        popupScene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        dialogStage.setScene(popupScene);
        dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("COMPOSITEVIEW"));
        dialogStage.showAndWait();*/
    }
    
    private double getDentSizeInMm(int reedCount){
        return inch2mm(2/(double)reedCount);
    }
    
    private double getYarnDiameterInMm(int countInNe){
        return inch2mm(1/(28*Math.sqrt(countInNe)));
    }
    
    private double inch2mm(double inches){
        return (inches*25.4);
    }
    
    private double mm2inch(double mm){
        return (mm/25.4);
    }
    
    private ArrayList<String> getYarnsInDent(ArrayList<ArrayList<String>> fullList, int dentIndex){
        return fullList.get(dentIndex);
    }
    
    /**
     * Given a yarn index, this function returns the dent index in which that yarn is present
     * @param fullList list of list of yarns (each sublist denotes yarns is a dent)
     * @param yarnIndex index of yarn
     * @return index of dent where yarn belongs or -1 (not found)
     */
    private int getDentIndexForYarn(ArrayList<ArrayList<String>> fullList, int yarnIndex){
        int yarnsTraversed=0;
        for(int l=0; l<fullList.size(); l++){
            yarnsTraversed+=fullList.get(l).size();
            if(yarnIndex<yarnsTraversed){
                return l;
            }
        }
        return -1;
    }
    
    private void printYarnFactors(ArrayList<ArrayList<String>> fullList){
        double dentSizeMm=getDentSizeInMm(objWeave.getIntEPI()*2);
        //System.out.println("Dent Size: "+dentSizeMm);
        double numDents=fullList.size();
        // for each dent
        for(ArrayList<String> lstSingleDentYarns: fullList){
            double totalDiaInDent=0.0; // in mm
            // for each warp
            for(String warpIndex: lstSingleDentYarns){
                //System.out.println("Diameter of warp "+warpIndex+": "+objWeave.getWarpYarn()[Integer.parseInt(warpIndex)].getDblYarnDiameter());
                totalDiaInDent+=objWeave.getWarpYarn()[Integer.parseInt(warpIndex)].getDblYarnDiameter();
            }
            //System.out.println("Total Dia in Dent: "+totalDiaInDent);
            
            // render in dent
            if(totalDiaInDent<dentSizeMm){
                // calculate space and render
                double totalSpace=dentSizeMm-totalDiaInDent;
                double singleSpace=totalSpace/(lstSingleDentYarns.size()+1);
                //System.out.println("Total Space: "+totalSpace+" Single Dent: "+singleSpace);
            }
            else{ // no space is available, overlap if totalDia >> dentSize
                // show ceil(dentSize/singleDia) 
                int numWarpsToShow=(int)Math.ceil(dentSizeMm/objWeave.getWarpYarn()[Integer.parseInt(lstSingleDentYarns.get(0))].getDblYarnDiameter());
                //System.out.println("Numbers of Warps to show: "+numWarpsToShow);
            }
        }
        
    }
    
    private void render(ArrayList<ArrayList<String>> fullList){
        int weftSlotSize=(int)(objWeave.getWeftYarn()[0].getDblYarnDiameter()*100)+5;//35; // 30px for weft and 5px for spacing
        int warpMul=100;
        double dentSizeMm=getDentSizeInMm(objWeave.getIntEPI()*2);
        // draw image
        int dentSizePx=(int)(dentSizeMm*warpMul); // size of a dent shown in pixels
        BufferedImage fullBI=new BufferedImage(fullList.size()*dentSizePx/*maxOfYarns*3*/, (objWeave.getIntWeft())*weftSlotSize, BufferedImage.TYPE_INT_RGB);
        BufferedImage seqBI=null; // image corresponding to one dent
        Graphics2D g = fullBI.createGraphics();
        Graphics2D gSeq=null;
        BasicStroke bs=new BasicStroke(1);
        int e=0;
        for(int a=0; a<fullList.size(); a++){ // for each dent
            double totalDiaInDent=0.0; // in mm
            // for each warp
            for(String warpIndex: fullList.get(a)){
                totalDiaInDent+=objWeave.getWarpYarn()[Integer.parseInt(warpIndex)].getDblYarnDiameter();
            }
            
            // render in dent
            // calculate space and render
            double totalSpace=dentSizeMm-totalDiaInDent;
            //System.err.println("Total Space in dent "+(a+1)+" is: "+totalSpace);
            double singleSpace=0;
            if(totalSpace>0)
                singleSpace=totalSpace/(fullList.get(a).size()+1);
            int singleSpacePx=(int)(singleSpace*warpMul);
            //System.err.println("Single Space Px in dent "+(a+1)+" is: "+singleSpacePx);
            
            seqBI=new BufferedImage(dentSizePx, (objWeave.getIntWeft())*weftSlotSize, BufferedImage.TYPE_INT_RGB);
            gSeq=seqBI.createGraphics();
            gSeq.setStroke(bs);
            Color co=Color.web("#FF5680");
            Color wfCo=Color.web("#FF5680");
            int startXPixelInDent=singleSpacePx;
            for(int b=0; b<fullList.get(a).size(); b++){ // for each thread in a dent
                co=Color.web(objWeave.getWarpYarn()[e].getStrYarnColor());
                double wpDia=objWeave.getWarpYarn()[Integer.parseInt(fullList.get(a).get(b))].getDblYarnDiameter();
                int wpDiaPx=(int)(wpDia*warpMul);
                for(int c=0; c<(objWeave.getIntWeft())*weftSlotSize; c+=weftSlotSize){
                    wfCo=Color.web(objWeave.getWeftYarn()[c/weftSlotSize].getStrYarnColor());
                    //weftSlotSize=(int)(objWeave.getWeftYarn()[c/weftSlotSize].getDblYarnDiameter()*100)+5;
                    // show weft threads on entire width of dent
                    if(b==0){
                        /*gSeq.setColor(new java.awt.Color((float) wfCo.getRed(),
                                             (float) wfCo.getGreen(),
                                             (float) wfCo.getBlue(),
                                             (float) wfCo.getOpacity()));
                        gSeq.fillRect(0, c+5, seqBI.getWidth(), weftSlotSize-5); // entire width*/
                        gradualWeftFill(gSeq, 0, c+5, seqBI.getWidth(), weftSlotSize-5, wfCo);
                    }
                    // right most (show weft threads)
                    if(b==fullList.get(a).size()-1){
                        /*gSeq.setColor(new java.awt.Color((float) wfCo.getRed(),
                                             (float) wfCo.getGreen(),
                                             (float) wfCo.getBlue(),
                                             (float) wfCo.getOpacity()));
                        gSeq.fillRect(dentSizePx-singleSpacePx-1, c+5, singleSpacePx, weftSlotSize-5);*/
                        gradualWeftFill(gSeq, dentSizePx-singleSpacePx-1, c+5, singleSpacePx, weftSlotSize-5, wfCo);
                    }
                    
                    // lowest warp thread
                    if(c==(objWeave.getIntWeft()-1)*weftSlotSize){
                        gradualFill(gSeq, startXPixelInDent, c+weftSlotSize,  wpDiaPx, 5, co);
                    }
                    
                    if(objWeave.getDesignMatrix()[c/weftSlotSize][e]==0){ // weft up
                        /*gSeq.setColor(new java.awt.Color((float) wfCo.getRed(),
                                             (float) wfCo.getGreen(),
                                             (float) wfCo.getBlue(),
                                             (float) wfCo.getOpacity()));
                        gSeq.fillRect(startXPixelInDent, c+5, wpDiaPx+singleSpacePx, weftSlotSize-5);*/
                        gradualWeftFill(gSeq, startXPixelInDent, c+5, wpDiaPx+singleSpacePx, weftSlotSize-5, wfCo);
                    }
                    else{ // warp up
                        gradualFill(gSeq, startXPixelInDent, c, wpDiaPx, weftSlotSize+5, co);
                    }
                }
                startXPixelInDent+=wpDiaPx;
                startXPixelInDent+=singleSpacePx;
                e++;
            }
            g.drawImage(seqBI, a*seqBI.getWidth(), 0, seqBI.getWidth(), seqBI.getHeight(), null);
        }
        
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        ScrollPane popup=new ScrollPane();
        popup.setPrefHeight(objConfiguration.HEIGHT);
        popup.setPrefWidth(objConfiguration.WIDTH);
        final ImageView weaveIV=new ImageView();
        BufferedImage show=new BufferedImage((int)objConfiguration.WIDTH, (int)objConfiguration.HEIGHT, BufferedImage.TYPE_INT_RGB);
        for(int x=0; x<show.getWidth(); x++){
            for(int y=0; y<show.getHeight(); y++){
                show.setRGB(x, y, fullBI.getRGB(x%fullBI.getWidth(), y%fullBI.getHeight()));
            }
        }
        weaveIV.setImage(SwingFXUtils.toFXImage(show, null));
        popup.setContent(weaveIV);//setCenter(weaveIV);        
        popup.setId("popup");
        Scene popupScene = new Scene(popup);
        
        weaveIV.setFitHeight(objConfiguration.HEIGHT);
        weaveIV.setFitWidth(objConfiguration.WIDTH);
        
        //For zoom Out weaving pattern
        final KeyCombination zoomOutKC = new KeyCodeCombination(KeyCode.SUBTRACT,KeyCombination.ALT_DOWN);
        //For undo weaving pattern
        final KeyCombination zoomInKC = new KeyCodeCombination(KeyCode.ADD,KeyCombination.ALT_DOWN);
        //For redo weaving pattern
        final KeyCombination normalKC = new KeyCodeCombination(KeyCode.EQUALS,KeyCombination.ALT_DOWN);
        //For redo weaving pattern
        final KeyCombination closeKC = new KeyCodeCombination(KeyCode.ESCAPE,KeyCombination.ALT_DOWN);

        popupScene.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler() {
            @Override
            public void handle(Event t) {
                if (closeKC.match((KeyEvent) t)){
                    dialogStage.close();
                } else if (normalKC.match((KeyEvent) t)){
                    weaveIV.setScaleX(1);
                    weaveIV.setScaleY(1);
                    weaveIV.setScaleZ(1);
                } else if (zoomInKC.match((KeyEvent) t)){
                    weaveIV.setScaleX(weaveIV.getScaleX()*2);
                    weaveIV.setScaleY(weaveIV.getScaleY()*2);
                    weaveIV.setScaleZ(weaveIV.getScaleZ()*2);
                } else if (zoomOutKC.match((KeyEvent) t)){
                    weaveIV.setScaleX(weaveIV.getScaleX()/2);
                    weaveIV.setScaleY(weaveIV.getScaleY()/2);
                    weaveIV.setScaleZ(weaveIV.getScaleZ()/2);
                }
            }
        });
        popupScene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                KeyCode key = t.getCode();
                if (key == KeyCode.ESCAPE){
                    dialogStage.close();
                } else if (key == KeyCode.ENTER){
                    weaveIV.setScaleX(1);
                    weaveIV.setScaleY(1);
                    weaveIV.setScaleZ(1);
                } else if (key == KeyCode.UP){
                    weaveIV.setScaleX(weaveIV.getScaleX()*2);
                    weaveIV.setScaleY(weaveIV.getScaleY()*2);
                    weaveIV.setScaleZ(weaveIV.getScaleZ()*2);
                } else if (key == KeyCode.DOWN){
                    weaveIV.setScaleX(weaveIV.getScaleX()/2);
                    weaveIV.setScaleY(weaveIV.getScaleY()/2);
                    weaveIV.setScaleZ(weaveIV.getScaleZ()/2);
                }
            }
        });
        dialogStage.setX(-5);
        dialogStage.setY(0);
        popupScene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        dialogStage.setScene(popupScene);
        dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("DENTINGPANE"));
        dialogStage.showAndWait();
    }
    
    private void gradualFill(Graphics2D g, int startX, int startY, int width, int height, Color co){
        // till X<changeX color will be constant with 90% brightness, after which it will gradually reduce
        int changeX=startX+width/4;
        Color javafxCol;
        // startX to changeX-1
        int numShades=changeX-startX;
        for(int a=0; a<numShades; a++){
            javafxCol=Color.hsb(co.getHue(), co.getSaturation(), 0.9f);
            g.setColor(new java.awt.Color((float)javafxCol.getRed(), (float)javafxCol.getGreen(), (float)javafxCol.getBlue()));
            g.drawLine(startX+a, startY, startX+a, startY+height-1);
        }
        // changeX to startX+width-1
        numShades=(startX+width-1)-changeX+1;
        for(int a=0; a<numShades; a++){
            javafxCol=Color.hsb(co.getHue(), co.getSaturation(), 0.9f-(float)a/(2*numShades));
            g.setColor(new java.awt.Color((float)javafxCol.getRed(), (float)javafxCol.getGreen(), (float)javafxCol.getBlue()));
            g.drawLine(changeX+a, startY, changeX+a, startY+height-1);
        }
    }    
    private void gradualWeftFill(Graphics2D g, int startX, int startY, int width, int height, Color wfCo){
        // till Y<changeY color will be constant with 90% brightness, after which it will gradually reduce
        int changeY=startY+height/4;
        Color javafxCol;
        // startY to changeY-1
        int numShades=changeY-startY;
        for(int a=0; a<numShades; a++){
            javafxCol=Color.hsb(wfCo.getHue(), wfCo.getSaturation(), 0.9f);
            g.setColor(new java.awt.Color((float)javafxCol.getRed(), (float)javafxCol.getGreen(), (float)javafxCol.getBlue()));
            g.drawLine(startX, startY+a, startX+width-1, startY+a);
        }
        // changeY to startY+height-1
        numShades=(startY+height-1)-changeY+1;
        for(int a=0; a<numShades; a++){
            javafxCol=Color.hsb(wfCo.getHue(), wfCo.getSaturation(), 0.9f-(float)a/(2*numShades));
            g.setColor(new java.awt.Color((float)javafxCol.getRed(), (float)javafxCol.getGreen(), (float)javafxCol.getBlue()));
            g.drawLine(startX, changeY+a, startX+width-1, changeY+a);
        }
    }
    private void refreshWarpPaletteMark(){
        boolean match=false;
        for(int wpPal=0; wpPal<26; wpPal++){
            if(marked[wpPal]!=1)
                continue;
            match=false; // color in palette is not assigned to any warp yarn
            for(int wp=0; wp<objWeave.getIntWarp(); wp++){
                if(objWeave.getWarpYarn()[wp].getStrYarnColor().equalsIgnoreCase("#"+objWeave.getObjConfiguration().getColourPalette()[wpPal])){
                    match=true; // color matching found
                }
            }
            if(!match){ // color not assigned
                for(Node n:editThreadGP.getChildren()){
                    if((GridPane.getColumnIndex(n)==wpPal/13)&&GridPane.getRowIndex(n)==(wpPal%13)+1){
                        ((Label)n).setText(((Label)n).getText().trim().substring(0,1));
                        marked[wpPal]=0;
                        //System.out.println("Palette "+wpPal+" was not assigned hence unmarked!");
                    }
                }
            }
        }
    }    
    private void refreshWeftPaletteMark(){
        boolean match=false;
        for(int wfPal=26; wfPal<52; wfPal++){
            if(marked[wfPal]!=1)
                continue;
            match=false; // color in palette is not assigned to any warp yarn
            for(int wf=0; wf<objWeave.getIntWeft(); wf++){
                if(objWeave.getWeftYarn()[wf].getStrYarnColor().equalsIgnoreCase("#"+objWeave.getObjConfiguration().getColourPalette()[wfPal])){
                    match=true; // color matching found
                }
            }
            if(!match){ // color not assigned
                for(Node n:editThreadGP.getChildren()){
                    if((GridPane.getColumnIndex(n)==(wfPal/13)+1)&&GridPane.getRowIndex(n)==(wfPal%13)+1){
                        ((Label)n).setText(((Label)n).getText().trim().substring(0,1));
                        marked[wfPal]=0;
                        //System.out.println("Palette "+wpPal+" was not assigned hence unmarked!");
                    }
                }
            }
        }
    }
    
    private void shufflePegPlan(){
        String[] shuffleModeCaptions = {"Odd Even Column wise", "Odd Odd Column wise", "Even Even Column wise"
                , "Odd Even Row wise", "Odd Odd Row wise", "Even Even Row wise"};
        String[] shuffleModeTooltips = {"Shuffle neighbouring odd numbered columns with even numbered columns in Peg Plan"
                , "Shuffle neighbouring odd numbered columns, leaving even numbered columns in Peg Plan"
                , "Shuffle neighbouring even numbered columns, leaving odd numbered columns in Peg Plan"
                , "Shuffle neighbouring odd numbered rows with even numbered rows in Peg Plan"
                , "Shuffle neighbouring odd numbered rows, leaving even numbered rows in Peg Plan"
                , "Shuffle neighbouring even numbered rows, leaving odd numbered rows in Peg Plan"};
        RadioOptionsView objRadioOptionsView = new RadioOptionsView(shuffleModeCaptions, shuffleModeTooltips);
        int shuffleMode = objRadioOptionsView.optionRBValue;
        if(objWeave.getPegMatrix()==null || shuffleMode==-1)
            return;
        byte[][] updatedMatrix = new byte[objWeave.getPegMatrix().length][objWeave.getPegMatrix()[0].length];
        int numRow = objWeave.getPegMatrix().length;
        int numCol = objWeave.getPegMatrix()[0].length;
        
        switch(shuffleMode){
            case 0:
                // odd even swapping columnwise
                numCol = numCol%2==0 ? numCol : numCol-1; // if odd, swap till last-1 columns
                for(int r=0; r<objWeave.getPegMatrix().length; r++){
                    for(int c=0; c<numCol; c++)
                        updatedMatrix[r][c] = objWeave.getPegMatrix()[r][c%2==0?c+1:c-1];
                    if(numCol==objWeave.getPegMatrix()[0].length-1)// update last column if odd no. of columns
                        updatedMatrix[r][numCol] = objWeave.getPegMatrix()[r][numCol];
                }
                break;
            case 1:
                // odd odd swapping columnwise
                for(int r=0; r<objWeave.getPegMatrix().length; r++){
                    if(numCol == 1){
                        updatedMatrix[r][0] = objWeave.getPegMatrix()[r][0];
                        continue;
                    }
                    for(int c=0; c<numCol-2; c++)
                        updatedMatrix[r][c] = objWeave.getPegMatrix()[r][c%2==0?((c/2)%2==0?c+2:c-2):c];
                    for(int c=numCol-2; c<numCol; c++)// update rest of the columns {c-2 and c-1}
                        updatedMatrix[r][c] = objWeave.getPegMatrix()[r][c%2==0?((c/2)%2==0?c:c-2):c];
                }
                break;
            case 2:
                // even even swapping columnwise
                for(int r=0; r<objWeave.getPegMatrix().length; r++){
                    if(numCol == 1){
                        updatedMatrix[r][0] = objWeave.getPegMatrix()[r][0];
                        continue;
                    }
                    for(int c=0; c<numCol-2; c++)
                        updatedMatrix[r][c] = objWeave.getPegMatrix()[r][c%2==1?((c/2)%2==0?c+2:c-2):c];
                    for(int c=numCol-2; c<numCol; c++)// update rest of the columns {c-2 and c-1}
                        updatedMatrix[r][c] = objWeave.getPegMatrix()[r][c%2==1?((c/2)%2==0?c:c-2):c];
                }
                break;
            case 3:
                // odd even swapping rowwise
                numRow = numRow%2==0 ? numRow : numRow-1; // if odd, swap till last-1 rows
                for(int r=0; r<numRow; r++){
                    for(int c=0; c<objWeave.getPegMatrix()[0].length; c++){
                        updatedMatrix[r][c] = objWeave.getPegMatrix()[r%2==0?r+1:r-1][c];
                    }
                }
                break;
            case 4:
                // odd odd swapping rowwise
                for(int c=0; c<objWeave.getPegMatrix()[0].length; c++){
                    if(numRow == 1){
                        updatedMatrix[0][c] = objWeave.getPegMatrix()[0][c];
                        continue;
                    }
                    for(int r=0; r<numRow-2; r++)
                        updatedMatrix[r][c] = objWeave.getPegMatrix()[r%2==0?((r/2)%2==0?r+2:r-2):r][c];
                    for(int r=numRow-2; r<numRow; r++)// update rest of the rows {r-2 and r-1}
                        updatedMatrix[r][c] = objWeave.getPegMatrix()[r%2==0?((r/2)%2==0?r:r-2):r][c];
                }
                break;
            case 5:
                // even even swapping rowwise
                for(int c=0; c<objWeave.getPegMatrix()[0].length; c++){
                    if(numRow == 1){
                        updatedMatrix[0][c] = objWeave.getPegMatrix()[0][c];
                        continue;
                    }
                    for(int r=0; r<numRow-2; r++)
                        updatedMatrix[r][c] = objWeave.getPegMatrix()[r%2==1?((r/2)%2==0?r+2:r-2):r][c];
                    for(int r=numRow-2; r<numRow; r++)// update rest of the rows {r-2 and r-1}
                        updatedMatrix[r][c] = objWeave.getPegMatrix()[r%2==1?((r/2)%2==0?r:r-2):r][c];
                }
                break;
        }
        objWeave.setPegMatrix(updatedMatrix);
        plotPeg();
        try {
            objWeave.setBytIsLiftPlan((byte)1);
            WeaveAction objWeaveAction = new WeaveAction(false);
            objWeaveAction.populateDesign(objWeave);
            populateDplot();
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"shufflePegPlan():  ",ex);
        }
        Weave objClonedWeave=cloneWeave();
        objUR.doCommand("Weave after shuffle", objClonedWeave);
    }
    
    /**
     * Reverse a pattern e.g. "12a3b5c" will become "5c3b12a"
     * @since v1.0 (Beta)
     * @author Aatif Ahmad Khan
     * @see saveAsHtml()
     * @param strWeftPattern Weft Pattern
     * @return Reversed Weft Pattern
     */
    private String getReverseWeftPattern(String strWeftPattern){
        String[] numParts = strWeftPattern.split("[a-z]{1}"); // separated numbers
        String[] alphaParts = strWeftPattern.split("[0-9]+"); // separated alphabets
        String strReverse = "";
        for(int i=numParts.length-1; i>=0; i--)
            strReverse += numParts[i] + alphaParts[i+1];
        return strReverse;
    }
/*===U=N=U=S=E=D===C=O=D=E===S=T=A=R=T===H=E=R=E===*/
/* 
    private void printActionOld(){
        try {  
            PrinterJob printjob = PrinterJob.getPrinterJob();
            printjob.setJobName(objDictionaryAction.getWord("PRINT"));
            if(printjob.printDialog()){
                printjob.setPrintable(new Printable() {
                    @Override
                    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
                        if (pageIndex != 0) {
                            return NO_SUCH_PAGE;
                        }
                        BufferedImage texture = new BufferedImage(objWeave.getIntWarp(), objWeave.getIntWeft(), BufferedImage.TYPE_INT_RGB);        
                        for(int x = 0; x < objWeave.getIntWeft(); x++) {
                            for(int y = 0; y < objWeave.getIntWarp(); y++) {
                                if(objWeave.getDesignMatrix()[x][y]==0)
                                    texture.setRGB(y, x, java.awt.Color.WHITE.getRGB());
                                else
                                    texture.setRGB(y, x, java.awt.Color.BLACK.getRGB());
                            }
                        }
                        try{
                            int widthDesign=objWeave.getIntWeft()*boxSize;
                            int widthCompositeView=objWeave.getIntWeft()*boxSize;
                            if((objWeave.getIntWeft()*boxSize)<(graphics.getFont().getSize()*5))
                                widthDesign=(graphics.getFont().getSize()*5);
                            if((objWeave.getIntWeft()*boxSize)<(graphics.getFont().getSize()*14))
                                widthCompositeView=(graphics.getFont().getSize()*14);
                            // 10 is added for providing space between design and composite view
                            int totalWidth=widthDesign+widthCompositeView+10;
                            BufferedImage bufferedImageesize = new BufferedImage(totalWidth, (int)(objWeave.getIntWarp()*boxSize)+(graphics.getFont().getSize()*2),BufferedImage.TYPE_INT_RGB);
                            // set white background
                            Graphics2D g = bufferedImageesize.createGraphics();
                            g.setColor(java.awt.Color.WHITE);
                            g.fillRect(0, 0, bufferedImageesize.getWidth(), bufferedImageesize.getHeight());
                            
                            BasicStroke bs = new BasicStroke(1);
                            g.setStroke(bs);
                            g.setColor(java.awt.Color.BLACK);
                            g.drawImage(texture, 0, 0, (int)(objWeave.getIntWeft()*boxSize), (int)(objWeave.getIntWarp()*boxSize), null);
                            
                            texture = new BufferedImage( (int)(objWeave.getIntWeft()*boxSize), (int)(objWeave.getIntWarp()*boxSize), BufferedImage.TYPE_INT_RGB);
                            WeaveAction objWeaveAction = new WeaveAction();
                            texture = objWeaveAction.plotFrontSideView(objWeave, objWeave.getIntWarp(), objWeave.getIntWeft(),  (int)(objWeave.getIntWeft()*boxSize), (int)(objWeave.getIntWarp()*boxSize));
                            g.drawImage(texture, widthDesign+10, 0, (int)(objWeave.getIntWeft()*boxSize), (int)(objWeave.getIntWarp()*boxSize), null);

                            g.drawString(objDictionaryAction.getWord("COMPOSITEVIEW"), widthDesign+10, (int)(objWeave.getIntWarp()*boxSize)+graphics.getFont().getSize());
                            // added for grid lines
                            for(int r=0; r<objWeave.getIntWeft(); r++)
                                g.drawLine(0, r*boxSize, (int)(objWeave.getIntWeft()*boxSize)-1, r*boxSize);
                            g.drawLine(0, (int)(objWeave.getIntWarp()*boxSize)-1, (int)(objWeave.getIntWeft()*boxSize)-1, (int)(objWeave.getIntWarp()*boxSize)-1);
                            for(int c=0; c<objWeave.getIntWarp(); c++)
                                g.drawLine(c*boxSize, 0, c*boxSize, (int)(objWeave.getIntWarp()*boxSize)-1);
                            g.drawString(objDictionaryAction.getWord("DESIGN"), 0, objWeave.getIntWeft()*boxSize+graphics.getFont().getSize());
                            g.drawLine((int)(objWeave.getIntWeft()*boxSize)-1, 0, (int)(objWeave.getIntWeft()*boxSize)-1, (int)(objWeave.getIntWarp()*boxSize)-1);
                            g.dispose();
                            
                            //ImageIO.write(bufferedImageesize, "png", new File("d:\\i.png"));
                            texture = null;
                            Graphics2D g2d=(Graphics2D)graphics;
                            g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
                            graphics.drawImage(bufferedImageesize, 0, 0, bufferedImageesize.getWidth(), bufferedImageesize.getHeight(), null);                        
                            bufferedImageesize = null;
                            System.gc();
                        }
                        catch(Exception ex){
                            new Logging("SEVERE",getClass().getName(),"printAction() : Print Error",ex);
                        }
                        return PAGE_EXISTS;                    
                    }
                });
                printjob.print();
            }
        } catch (PrinterException ex) {             
            new Logging("SEVERE",getClass().getName(),ex.toString(),ex);  
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
   private void populateToolKit(){
        if(weaveChildStage!=null){
            weaveChildStage.close();
            isWeaveChildStageOn=false;
            weaveChildStage = null;
            System.gc();
        }
        weaveChildStage = new Stage();
        weaveChildStage.initOwner(weaveStage);
        weaveChildStage.initStyle(StageStyle.UTILITY);
        //dialogStage.initModality(Modality.WINDOW_MODAL);
        
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(0);
        popup.setVgap(0);

        Button insertWeftTP = new Button();
        Button deleteWeftTP = new Button();
        Button insertWarpTP = new Button();
        Button deleteWarpTP = new Button();
        Button selectTP = new Button();
        Button copyTP = new Button();
        Button pasteTP = new Button();
        Button clearTP = new Button();
        Button mirrorVerticalTP = new Button();
        Button mirrorHorizontalTP = new Button();
        Button inversionTP = new Button();
        Button rotateTP = new Button();
        Button rotateAntiTP = new Button();
        Button moveRightTP = new Button();
        Button moveLeftTP = new Button();
        Button moveUpTP = new Button();
        Button moveDownTP = new Button();
        Button moveRight8TP = new Button();
        Button moveLeft8TP = new Button();
        Button moveUp8TP = new Button();
        Button moveDown8TP = new Button();
        Button tiltRightTP = new Button();
        Button tiltLeftTP = new Button();
        Button tiltUpTP = new Button();
        Button tiltDownTP = new Button();
        Button normalTP = new Button();
        Button zoomInTP = new Button();
        Button ZoomOutTP = new Button();
        
        insertWeftTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/insert_weft.png"));
        deleteWeftTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/delete_weft.png"));
        insertWarpTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/insert_warp.png"));
        deleteWarpTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/delete_warp.png"));
        selectTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/select.png"));
        copyTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/copy.png"));
        pasteTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/paste.png"));
        clearTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/clear.png"));
        mirrorVerticalTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/vertical_mirror.png"));
        mirrorHorizontalTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/horizontal_mirror.png"));
        rotateTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_90.png"));
        rotateAntiTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/rotate_anti_90.png"));
        moveRightTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right.png"));
        moveLeftTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left.png"));
        moveUpTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up.png"));
        moveDownTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down.png"));
        moveRight8TP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right_by_8.png"));
        moveLeft8TP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left_by_8.png"));
        moveUp8TP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_up_by_8.png"));
        moveDown8TP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_down_by_8.png"));
        tiltRightTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_right.png"));
        tiltLeftTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_left.png"));
        tiltUpTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_up.png"));
        tiltDownTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/tilt_down.png"));
        inversionTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/inversion.png"));
        normalTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_normal.png"));
        zoomInTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_in.png"));
        ZoomOutTP.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/zoom_out.png"));
        
        insertWeftTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPINSERTWEFT")));
        deleteWeftTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPDELETEWEFT")));
        insertWarpTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPINSERTWARP")));
        deleteWarpTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPDELETEWARP")));
        selectTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSELECT")));
        copyTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCOPY")));
        pasteTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPPASTE")));
        clearTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCLEARWEAVE")));
        mirrorVerticalTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPVERTICALMIRROR")));
        mirrorHorizontalTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPHORIZENTALMIRROR")));
        rotateTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCLOCKROTATION")));
        rotateAntiTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPANTICLOCKROTATION")));
        moveRightTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPMOVERIGHT")));
        moveLeftTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPMOVELEFT")));
        moveUpTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPMOVEUP")));
        moveDownTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPMOVEDOWN")));
        moveRight8TP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPMOVERIGHT8")));
        moveLeft8TP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPMOVELEFT8")));
        moveUp8TP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPMOVEUP8")));
        moveDown8TP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPMOVEDOWN8")));
        tiltRightTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPTILTRIGHT")));
        tiltLeftTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPTILTLEFT")));
        tiltUpTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPTILTUP")));
        tiltDownTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPTILTDOWN")));
        inversionTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPINVERSION")));
        normalTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPZOOMNORMALVIEW")));
        zoomInTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPZOOMINVIEW")));
        ZoomOutTP.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPZOOMOUTVIEW")));
        
        insertWeftTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        deleteWeftTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        insertWarpTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        deleteWarpTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        selectTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        copyTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        pasteTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        clearTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        mirrorVerticalTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        mirrorHorizontalTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rotateTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        rotateAntiTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveRightTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveLeftTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveUpTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveDownTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveRight8TP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveLeft8TP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveUp8TP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        moveDown8TP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tiltRightTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tiltLeftTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tiltUpTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        tiltDownTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);                    
        inversionTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        normalTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        zoomInTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        ZoomOutTP.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        
        popup.add(insertWeftTP, 0, 0);
        popup.add(deleteWeftTP, 1, 0);
        popup.add(insertWarpTP, 0, 1);
        popup.add(deleteWarpTP, 1, 1);
        popup.add(selectTP, 0, 2);
        popup.add(copyTP, 1, 2);
        popup.add(pasteTP, 0, 3);
        popup.add(clearTP, 1, 3);
        popup.add(mirrorVerticalTP, 0, 4);
        popup.add(mirrorHorizontalTP, 1, 4);
        popup.add(rotateTP, 0, 5);
        popup.add(rotateAntiTP, 1, 5);
        popup.add(moveRightTP, 0, 6);
        popup.add(moveLeftTP, 1, 6);
        popup.add(moveUpTP, 0, 7);
        popup.add(moveDownTP, 1, 7);
        popup.add(moveRight8TP, 0, 8);
        popup.add(moveLeft8TP, 1, 8);
        popup.add(moveUp8TP, 0, 9);
        popup.add(moveDown8TP, 1, 9);
        popup.add(tiltRightTP, 0, 10);
        popup.add(tiltLeftTP, 1, 10);
        popup.add(tiltUpTP, 0, 11);
        popup.add(tiltDownTP, 1, 11);
        popup.add(inversionTP, 0, 12);
        popup.add(normalTP, 1, 12);
        popup.add(zoomInTP, 0, 13);
        popup.add(ZoomOutTP, 1, 13);
        
        insertWeftTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {                       
                    insertWeftAction();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Insert Weft",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });  
        deleteWeftTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {                       
                    deleteWeftAction();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Delete Weft",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        insertWarpTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {                       
                    insertWarpAction();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Insert Warp",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });  
        deleteWarpTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {                       
                    deleteWarpAction();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Delete Warp",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });  
        selectTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {                       
                    selectAction();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Select Weave",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        copyTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {                       
                    copyAction();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Copy Weave",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        pasteTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {                       
                    pasteAction();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Paste Weave",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });  
        clearTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {                       
                    clearAction();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Clear Weave",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });  
        mirrorVerticalTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {                       
                    mirrorVerticalAction();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Mirror Vertical",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });  
        mirrorHorizontalTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    mirrorHorizontalAction();
                } catch (Exception ex) {
                  new Logging("SEVERE",getClass().getName(),"Mirror Vertical",ex);
                  lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        }); 
        rotateTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    rotateClockwiseAction();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Rotate Clock Wise ",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        }); 
        rotateAntiTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    rotateAntiClockwiseAction();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Anti Rotate Clock Wise ",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        moveRightTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    moveRightAction();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Move Right",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        moveLeftTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    moveLeftAction();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Move Left",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        moveUpTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    moveUpAction();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Move Up",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        moveDownTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    moveDownAction();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Move Down",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
         moveRight8TP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    moveRight8Action();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Move Right",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        moveLeft8TP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    moveLeft8Action();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Move Left",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        moveUp8TP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    moveUp8Action();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Move Up",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        moveDown8TP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    moveDown8Action();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Move Down",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        tiltRightTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    tiltRightAction();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Tilt Right ",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        tiltLeftTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    tiltLeftAction();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Tilt Left ",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        tiltUpTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    tiltUpAction();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Tilt Up ",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        tiltDownTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    tiltDownAction();
                } catch (Exception ex) {
                   new Logging("SEVERE",getClass().getName(),"Tilt Down ",ex);
                   lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });         
        inversionTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    inversionAction();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Inversion ",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        normalTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    ZoomNormalAction();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Zoom Normal ",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        zoomInTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    zoomInAction();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Zoom In ",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        ZoomOutTP.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    zoomOutAction();
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),"Zoom out ",ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        weaveChildStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                isWeaveChildStageOn=false;
                weaveChildStage.close();
            }
        });
        isWeaveChildStageOn=true;
        
        Scene scene = new Scene(popup);
        scene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        weaveChildStage.setScene(scene);
        weaveChildStage.setTitle(objDictionaryAction.getWord("PROJECT")+": "+objDictionaryAction.getWord("OPERATION"));
        weaveChildStage.showAndWait();        
    }
*/
    
    
    /*
    private void plotShaft(){
        try {
            shaftGP.getChildren().clear();
            System.gc();
            objWeaveAction = new WeaveAction(objWeave,false);
            for(int i=0;i<objWeave.getIntWeaveShaft();i++){
                for(int j=0;j<objWeave.getIntWarp();j++) {
                    final int p=i;
                    final int q=j;
                    final Label lblbox = new Label("");
                    lblbox.setPrefSize(boxSize,boxSize);
                    if(objWeave.getShaftMatrix()[i][j]==1 ){
                        lblbox.setStyle("-fx-background-color: #000000;-fx-border-width: 1;-fx-border-color: black;");
                        lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.BOLD, boxSize));                        
                    }else{
                        lblbox.setStyle("-fx-background-color: #ffffff; -fx-border-width: 1;  -fx-border-color: black;");
                        lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                    }
                    lblbox.setOnMouseClicked(new EventHandler<MouseEvent>(){
                        @Override
                        public void handle(MouseEvent arg0) {
                            if(objWeave.getShaftMatrix()[p][q]==1 ){
                             //   Label lblbox1=shaftGP.getChildren().get()
                                lblbox.setStyle("-fx-background-color: #ffffff; -fx-border-width: 1;  -fx-border-color: black;");
                                lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                                objWeave.getShaftMatrix()[p][q]=0;                                
                            } else {
                               *//* for(int r=0;r<objWeave.getIntWeaveShaft();r++){
                                    lblbox.setStyle("-fx-background-color: #ffffff; -fx-border-width: 1;  -fx-border-color: black;");
                                    lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                                    objWeave.getShaftMatrix()[r][q]=0;
                                    if(isDesignMode){
                                        objWeaveAction.populateShaftDesign(objWeave, r,q);
                                        objWeaveAction.populateTreadles(objWeave);
                                        objWeaveAction.populateTieUp(objWeave);
                                        objWeaveAction.populatePegPlan(objWeave);
                                    } else if(editMode.equalsIgnoreCase("all")){

                                    } else{
                                        objWeaveAction.populateShaftDesign(objWeave, r,q);
                                    }
                                }
                                lblbox.setStyle("-fx-background-color: #000000;-fx-border-width: 1;-fx-border-color: black;");
                                lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.BOLD, boxSize));
                                objWeave.getShaftMatrix()[p][q]=1;
                                */
                                /*boolean shaftPresent = false;
                                for(int r=0;r<objWeave.getIntWeaveShaft();r++)
                                    if(objWeave.getShaftMatrix()[r][q]==1)
                                        shaftPresent = true;
                                if(!shaftPresent){
                                    lblbox.setStyle("-fx-background-color: #000000;-fx-border-width: 1;-fx-border-color: black;");
                                    lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.BOLD, boxSize));
                                    objWeave.getShaftMatrix()[p][q]=1;
                                }*/
                                /*
                                for(int r=0;r<objWeave.getIntWeaveShaft();r++){
                                    System.err.println(p+":"+q+"="+shaftMatrix[p][q]);
                                    if(p!=r){
                                        lblbox.setStyle("-fx-background-color: #ffffff; -fx-border-width: 1;  -fx-border-color: black;");
                                        lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                                        shaftMatrix[r][q]=0;                                        
                                    }
                                    System.err.println(r+":"+q+"="+shaftMatrix[r][q]);
                                }
                                
                
                            }*/
                            /*if(isDesignMode){
                                objWeaveAction.populateShaftDesign(objWeave, p,q);
                                objWeaveAction.populateTreadles(objWeave);
                                objWeaveAction.populateTieUp(objWeave);
                                objWeaveAction.populatePegPlan(objWeave);
                                plotTradeles();
                                plotTieUp();
                                plotPeg();
                                populateDplot();
                            } else if(editMode.equalsIgnoreCase("all")){
                              
                            } else{
                                objWeaveAction.populateShaftDesign(objWeave, p,q);
                                populateDplot();
                            }
                            //plotShaft();
                        }
                    });
                    shaftGP.add(lblbox,j,i);
                }
            }
        } catch (Exception ex) {
            new Logging("SEVERE",getClass().getName(),"Ploting Shaft",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }                   
    }

   private void plotTieUp(){
        try {
            tieUpGP.getChildren().clear();
            System.gc();
            objWeaveAction = new WeaveAction(objWeave,false);
            for(int i=0;i<objWeave.getIntWeaveShaft();i++){
                for(int j=0;j<objWeave.getIntWeaveTreadles();j++) {
                    final int p=i;
                    final int q=j;
                    final Label lblbox = new Label("");
                    lblbox.setPrefSize(boxSize,boxSize);
                    if(objWeave.getTieupMatrix()[i][j]==1 ){
                        lblbox.setStyle("-fx-background-color: #000000;-fx-border-width: 1;-fx-border-color: black;");
                        lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                    }
                    else{
                        lblbox.setStyle("-fx-background-color: #ffffff; -fx-border-width: 1;  -fx-border-color: black;");
                        lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                    }
                    lblbox.setOnMouseClicked(new EventHandler<MouseEvent>(){
                        @Override
                        public void handle(MouseEvent arg0) {
                            if(objWeave.getTieupMatrix()[p][q]==1 ){
                                lblbox.setStyle("-fx-background-color: #ffffff; -fx-border-width: 1;  -fx-border-color: black;");
                                lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                                objWeave.getTieupMatrix()[p][q]=0;
                            } else {
                                lblbox.setStyle("-fx-background-color: #000000;-fx-border-width: 1;-fx-border-color: black;");
                                lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                                objWeave.getTieupMatrix()[p][q]=1;
                            }
                            if(isDesignMode){
                                objWeaveAction.populateTieUpDesign(objWeave, p,q);
                                objWeaveAction.populateShaft(objWeave);
                                objWeaveAction.populateTreadles(objWeave);
                                objWeaveAction.populatePegPlan(objWeave);
                                populateDplot();
                                plotShaft();
                                plotPeg();
                                plotTradeles();
                            } else if(editMode.equalsIgnoreCase("all")){
                              
                            } else{
                                objWeaveAction.populateTieUpDesign(objWeave, p,q);
                                populateDplot();
                            }
                        }
                    });
                    tieUpGP.add(lblbox,objWeave.getIntWeaveShaft()-i-1,objWeave.getIntWeaveTreadles()-j-1);
                }
            }
        } catch (SQLException ex) {                   
            new Logging("SEVERE",getClass().getName(),"Ploting Tieup",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
   
    private void plotTradeles() {
        try {
            tradelesGP.getChildren().clear();
            System.gc();
            objWeaveAction = new WeaveAction(objWeave,false);
            for(int i=0;i<objWeave.getIntWeft();i++){
                for(int j=0;j<objWeave.getIntWeaveTreadles();j++) {
                    final int p=i;
                    final int q=j;
                    final Label lblbox = new Label("");
                    lblbox.setPrefSize(boxSize,boxSize);
                    if(objWeave.getTreadlesMatrix()[i][j]==1 ){
                        lblbox.setStyle("-fx-background-color: #000000;-fx-border-width: 1;-fx-border-color: black;");
                        lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                    } else{
                        lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                        lblbox.setStyle("-fx-background-color: #ffffff; -fx-border-width: 1;  -fx-border-color: black;");
                    }
                    lblbox.setOnMouseClicked(new EventHandler<MouseEvent>(){
                        @Override
                        public void handle(MouseEvent arg0) {
                            if(objWeave.getTreadlesMatrix()[p][q]==1 ){
                                lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                                lblbox.setStyle("-fx-background-color: #ffffff; -fx-border-width: 1;  -fx-border-color: black;");
                                objWeave.getTreadlesMatrix()[p][q]=0;
                            } else {
                                boolean treadlePresent = false;
                                for(int r=0;r<objWeave.getIntWeaveTreadles();r++)
                                    if(objWeave.getTreadlesMatrix()[p][r]==1 )
                                        treadlePresent = true;
                                if(!treadlePresent){
                                    lblbox.setStyle("-fx-background-color: #000000;-fx-border-width: 1;-fx-border-color: black;");
                                    lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                                    objWeave.getTreadlesMatrix()[p][q]=1;
                                }
                            }
                            if(isDesignMode){
                                objWeaveAction.populateTradelesDesign(objWeave, p,q);
                                objWeaveAction.populateShaft(objWeave);
                                objWeaveAction.populateTieUp(objWeave);
                                objWeaveAction.populatePegPlan(objWeave);
                                populateDplot();
                                plotShaft();
                                plotPeg();
                                plotTieUp();
                            } else if(editMode.equalsIgnoreCase("all")){
                              
                            } else{
                                objWeaveAction.populateTradelesDesign(objWeave, p,q);
                                populateDplot();
                            }
                        }
                    });
                    tradelesGP.add(lblbox,j,i);
                }
            }    
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"Ploting Treadles",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    
    private void plotPeg() {
        try {
            pegGP.getChildren().clear();
            System.gc();
            objWeaveAction = new WeaveAction(objWeave,false);
            for(int i=0;i<objWeave.getIntWeft();i++){
                for(int j=0;j<objWeave.getIntWeaveShaft();j++) {
                    final int p=i;
                    final int q=j;
                    final Label lblbox = new Label("");
                    lblbox.setPrefSize(boxSize,boxSize);
                    if(objWeave.getPegMatrix()[i][j]==1 ){
                        lblbox.setStyle("-fx-background-color: #000000;-fx-border-width: 1;-fx-border-color: black;");
                        lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                    } else{
                        lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                        lblbox.setStyle("-fx-background-color: #ffffff; -fx-border-width: 1;  -fx-border-color: black;");
                    }
                    lblbox.setOnMouseClicked(new EventHandler<MouseEvent>(){
                        @Override
                        public void handle(MouseEvent arg0) {
                            if(objWeave.getPegMatrix()[p][q]==1 ){
                                lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                                lblbox.setStyle("-fx-background-color: #ffffff; -fx-border-width: 1;  -fx-border-color: black;");
                                objWeave.getPegMatrix()[p][q]=0;
                            } else {
                                lblbox.setStyle("-fx-background-color: #000000;-fx-border-width: 1;-fx-border-color: black;");
                                lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                                objWeave.getPegMatrix()[p][q]=1;
                            }
                            if(isDesignMode){
                                objWeaveAction.populatePegDesign(objWeave, p,q);
                                objWeaveAction.populateShaft(objWeave);
                                objWeaveAction.populateTieUp(objWeave);
                                objWeaveAction.populateTreadles(objWeave);
                                populateDplot();
                                plotShaft();
                                plotTieUp();
                                plotTradeles();
                            } else if(editMode.equalsIgnoreCase("all")){
                                
                            } else{
                                objWeaveAction.populatePegDesign(objWeave, p,q);
                                populateDplot();
                            }
                        }
                    });
                    pegGP.add(lblbox,j,i);
                }
            }    
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"Ploting Treadles",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }

   private void plotDesign() {
        try {
            designGP.getChildren().clear();
            System.gc();
            objWeaveAction = new WeaveAction(objWeave,false);
            for(int i=0;i<objWeave.getIntWeft();i++){
                for(int j=0;j<objWeave.getIntWarp();j++) {
                    final int p=i;
                    final int q=j;
                    final Label lblbox = new Label("");
                    lblbox.setPrefSize(boxSize,boxSize);
                    if(objWeave.getDesignMatrix()[i][j]==1 ){
                        lblbox.setStyle("-fx-background-color: #000000;-fx-border-width: 1;  -fx-border-color: black");
                        lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                    } else{
                        lblbox.setStyle("-fx-background-color: #ffffff; -fx-border-width: 1;  -fx-border-color: black;");
                        lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                    }
                    if(isDragBox==0)
                        selectionLineArea(lblbox,p,q);
                    else
                        selectionBoxArea(lblbox, p, q);
                    lblbox.setOnMouseClicked(new EventHandler<MouseEvent>(){
                        @Override
                        public void handle(MouseEvent arg0) {
                            try{
                                if(isDragBox==0)
                                    selectionLineArea(lblbox,p,q);
                                else
                                    selectionBoxArea(lblbox, p, q);
                                if(objWeave.getDesignMatrix()[p][q]==1){
                                    lblbox.setStyle("-fx-background-color: #ffffff; -fx-border-width: 1;  -fx-border-color: black;");
                                    lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                                    objWeave.getDesignMatrix()[p][q]=0;
                                } else {
                                    lblbox.setStyle("-fx-background-color: #000000;-fx-border-width: 1;  -fx-border-color: black");
                                    lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                                    objWeave.getDesignMatrix()[p][q]=1;
                                }
                                current_row=p;
                                current_col=q;
                                populateNplot();
                            } catch (Exception ex) {
                                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                                new Logging("SEVERE",getClass().getName(),"Ploting weave Design",ex);
                                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                            }
                        }
                    });
                    designGP.add(lblbox,j,i);
                }    
            }
        } catch (SQLException ex) {
            new Logging("SEVERE",getClass().getName(),"Ploting weave Design",ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
   
   private void plotDenting() {
        dentingGP.getChildren().clear();
        System.gc();
        for(int i=0;i<2;i++){
            for(int j=0;j<objWeave.getIntWarp();j++) {
                final int p=i;
                final int q=j;
                final Label lblbox = new Label("");
                lblbox.setPrefSize(boxSize,boxSize);
                if(objWeave.getDentMatrix()[i][j]==1 ){
                    lblbox.setStyle("-fx-background-color: #fedcba;-fx-border-width: 1;  -fx-border-color: black");
                    lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                } else{
                    lblbox.setStyle("-fx-background-color: #ffffff; -fx-border-width: 1;  -fx-border-color: black;");
                    lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                }
                lblbox.setOnMouseClicked(new EventHandler<MouseEvent>(){
                    @Override
                    public void handle(MouseEvent arg0) {
                        try{
                            if(objWeave.getDentMatrix()[p][q]==1){
                                lblbox.setStyle("-fx-background-color: #ffffff; -fx-border-width: 1;  -fx-border-color: black;");
                                lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                                objWeave.getDentMatrix()[p][q]=0;
                                objWeave.getDentMatrix()[(p+1)%2][q]=1;
                            } else {
                                lblbox.setStyle("-fx-background-color: #fedcba;-fx-border-width: 1;  -fx-border-color: black");
                                lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                                objWeave.getDentMatrix()[p][q]=1;
                                objWeave.getDentMatrix()[(p+1)%2][q]=0;
                            }
                            plotDenting();
                        } catch (Exception ex) {
                            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                            new Logging("SEVERE",getClass().getName(),"Ploting weave Design",ex);
                            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                        }
                    }
                });
                dentingGP.add(lblbox,j,i);
            }    
        }
    }

private void colourwaysAction(){
    ColorCombination cc=new ColorCombination();
    //System.out.println("warp"+objWeave.getWarpYarn()[0].getStrYarnColor()+"weft"+objWeave.getIntWeft());
    //objWeave.getObjConfiguration().setWarpYarn(objWeave.getWarpYarn());
    //objWeave.getObjConfiguration().setWeftYarn(objWeave.getWeftYarn());
    ArrayList<Integer> warpPattern=new ArrayList<>();
    ArrayList<Integer> weftPattern=new ArrayList<>();
    for(int wp=0; wp<objWeave.getWarpYarn().length; wp++){
        for(int i=0; i<26; i++){
            if(objWeave.getWarpYarn()[wp].equals(objWeave.getObjConfiguration().getYarnPalette()[i])){
                warpPattern.add(i);
                System.out.println("Added Wp: "+i);
            }
        }
    }
    for(int wf=0; wf<objWeave.getWeftYarn().length; wf++){
        for(int i=0; i<26; i++){
            if(objWeave.getWeftYarn()[wf].equals(objWeave.getObjConfiguration().getYarnPalette()[i+26])){
                weftPattern.add(i+26);
                System.out.println("Added Wf: "+(i+26));
            }
        }
    }
    cc.intWarpPattern=convertIntegers(warpPattern);
    
    cc.intWeftPattern=convertIntegers(weftPattern);
    System.out.println(cc.intWarpPattern.length+":::"+cc.intWeftPattern.length);
    //for(int i=0; i<2; i++){
    //    cc.strPalette[i]=objWeave.getWarpYarn()[i].getStrYarnColor();
    //    cc.strPalette[i+26]=objWeave.getWeftYarn()[i].getStrYarnColor();
    //}
    cc.strPalette=objWeave.getObjConfiguration().strThreadPalettes;//objConfiguration.strThreadPaletes;
    cc.niam();
    lblStatus.setText(objDictionaryAction.getWord("ACTIONTEXTUREVIEW"));
    int height=(int)(objConfiguration.HEIGHT/3);
    int width=(int)(objConfiguration.WIDTH/3);
    byte[][] tilledMatrix = new byte[height][width];
    BufferedImage bufferedImage = new BufferedImage(width*3, height*3,BufferedImage.TYPE_INT_RGB);
    int bands = 3;
    int rgb = 0;
    int combPerPage=16;
    int pageDiv=(int)Math.sqrt(combPerPage);

    int a=0; // starting number of combination
    int ch=a;
    for(int x = 0, p = 0; x < height; x++) {
        a=ch;
        for(int y = 0, q = 0; y < width; y++) {
            if(x>=objWeave.getIntWeft() && y<objWeave.getIntWarp()){
                 tilledMatrix[x][y] = objWeave.getDesignMatrix()[x%objWeave.getIntWeft()][y];  
            }else if(x<objWeave.getIntWeft() && y>=objWeave.getIntWarp()){
                 tilledMatrix[x][y] = objWeave.getDesignMatrix()[x][y%objWeave.getIntWarp()];  
            }else if(x>=objWeave.getIntWeft() && y>=objWeave.getIntWarp()){
                 tilledMatrix[x][y] = objWeave.getDesignMatrix()[x%objWeave.getIntWeft()][y%objWeave.getIntWarp()];  
            }else{
                 tilledMatrix[x][y] = objWeave.getDesignMatrix()[x][y]; 
            }
            for(int i = 0; i < bands; i++) {
                for(int j = 0; j < bands; j++) {                        
                    if(tilledMatrix[x][y]==0){
                        if(i==0)
                            rgb=new java.awt.Color((float)javafx.scene.paint.Color.web("#"+cc.paletteCombination[a][26+x%objWeave.getIntWeft()]).getRed(),(float)javafx.scene.paint.Color.web("#"+cc.paletteCombination[a][26+x%objWeave.getIntWeft()]).getGreen(),(float)javafx.scene.paint.Color.web("#"+cc.paletteCombination[a][26+x%objWeave.getIntWeft()]).getBlue()).brighter().getRGB();
                            //rgb = getIntRgbFromColor(255
                                //, Integer.parseInt(cc.paletteCombination[a][x%objWeave.getIntWeft()].substring(0, 2), 16)
                                //, Integer.parseInt(cc.paletteCombination[a][x%objWeave.getIntWeft()].substring(2, 4), 16)
                                //, Integer.parseInt(cc.paletteCombination[a][x%objWeave.getIntWeft()].substring(4, 6), 16));
                        else if(i==2)
                            rgb=new java.awt.Color((float)javafx.scene.paint.Color.web("#"+cc.paletteCombination[a][26+x%objWeave.getIntWeft()]).getRed(),(float)javafx.scene.paint.Color.web("#"+cc.paletteCombination[a][26+x%objWeave.getIntWeft()]).getGreen(),(float)javafx.scene.paint.Color.web("#"+cc.paletteCombination[a][26+x%objWeave.getIntWeft()]).getBlue()).darker().getRGB();
                            //rgb = getIntRgbFromColor(255
                                //, Integer.parseInt(cc.paletteCombination[a][x%objWeave.getIntWeft()].substring(0, 2), 16)
                                //, Integer.parseInt(cc.paletteCombination[a][x%objWeave.getIntWeft()].substring(2, 4), 16)
                                //, Integer.parseInt(cc.paletteCombination[a][x%objWeave.getIntWeft()].substring(4, 6), 16));
                        else
                            rgb=new java.awt.Color((float)javafx.scene.paint.Color.web("#"+cc.paletteCombination[a][26+x%objWeave.getIntWeft()]).getRed(),(float)javafx.scene.paint.Color.web("#"+cc.paletteCombination[a][26+x%objWeave.getIntWeft()]).getGreen(),(float)javafx.scene.paint.Color.web("#"+cc.paletteCombination[a][26+x%objWeave.getIntWeft()]).getBlue()).getRGB();
                            //rgb = getIntRgbFromColor(255
                                //, Integer.parseInt(cc.paletteCombination[a][x%objWeave.getIntWeft()].substring(0, 2), 16)
                                //, Integer.parseInt(cc.paletteCombination[a][x%objWeave.getIntWeft()].substring(2, 4), 16)
                                //, Integer.parseInt(cc.paletteCombination[a][x%objWeave.getIntWeft()].substring(4, 6), 16));
                    } else if(tilledMatrix[x][y]==1){
                        if(j==0)
                            rgb=new java.awt.Color((float)javafx.scene.paint.Color.web("#"+cc.paletteCombination[a][y%objWeave.getIntWarp()]).getRed(),(float)javafx.scene.paint.Color.web("#"+cc.paletteCombination[a][y%objWeave.getIntWarp()]).getGreen(),(float)javafx.scene.paint.Color.web("#"+cc.paletteCombination[a][y%objWeave.getIntWarp()]).getBlue()).brighter().getRGB();
                            //rgb = getIntRgbFromColor(255
                                //, Integer.parseInt(cc.paletteCombination[a][y%objWeave.getIntWarp()].substring(0, 2), 16)
                                //, Integer.parseInt(cc.paletteCombination[a][y%objWeave.getIntWarp()].substring(2, 4), 16)
                                //, Integer.parseInt(cc.paletteCombination[a][y%objWeave.getIntWarp()].substring(4, 6), 16));
                        else if(j==2)
                            rgb=new java.awt.Color((float)javafx.scene.paint.Color.web("#"+cc.paletteCombination[a][y%objWeave.getIntWarp()]).getRed(),(float)javafx.scene.paint.Color.web("#"+cc.paletteCombination[a][y%objWeave.getIntWarp()]).getGreen(),(float)javafx.scene.paint.Color.web("#"+cc.paletteCombination[a][y%objWeave.getIntWarp()]).getBlue()).darker().getRGB();
                            //rgb = getIntRgbFromColor(255
                                //, Integer.parseInt(cc.paletteCombination[a][y%objWeave.getIntWarp()].substring(0, 2), 16)
                                //, Integer.parseInt(cc.paletteCombination[a][y%objWeave.getIntWarp()].substring(2, 4), 16)
                                //, Integer.parseInt(cc.paletteCombination[a][y%objWeave.getIntWarp()].substring(4, 6), 16));
                        else
                            rgb=new java.awt.Color((float)javafx.scene.paint.Color.web("#"+cc.paletteCombination[a][y%objWeave.getIntWarp()]).getRed(),(float)javafx.scene.paint.Color.web("#"+cc.paletteCombination[a][y%objWeave.getIntWarp()]).getGreen(),(float)javafx.scene.paint.Color.web("#"+cc.paletteCombination[a][y%objWeave.getIntWarp()]).getBlue()).getRGB();
                            //rgb = getIntRgbFromColor(255
                                //, Integer.parseInt(cc.paletteCombination[a][y%objWeave.getIntWarp()].substring(0, 2), 16)
                                //, Integer.parseInt(cc.paletteCombination[a][y%objWeave.getIntWarp()].substring(2, 4), 16)
                                //, Integer.parseInt(cc.paletteCombination[a][y%objWeave.getIntWarp()].substring(4, 6), 16));
                    } else {
                        if(i==0)
                            rgb = new java.awt.Color((float)javafx.scene.paint.Color.web("#FF0000").getRed(),(float)javafx.scene.paint.Color.web("#FF0000").getGreen(),(float)javafx.scene.paint.Color.web("#FF0000").getBlue()).brighter().getRGB();
                        else if(i==2)
                            rgb = new java.awt.Color((float)javafx.scene.paint.Color.web("#FF0000").getRed(),(float)javafx.scene.paint.Color.web("#FF0000").getGreen(),(float)javafx.scene.paint.Color.web("#FF0000").getBlue()).darker().getRGB();
                        else
                            rgb = new java.awt.Color((float)javafx.scene.paint.Color.web("#FF0000").getRed(),(float)javafx.scene.paint.Color.web("#FF0000").getGreen(),(float)javafx.scene.paint.Color.web("#FF0000").getBlue()).getRGB();
                    }
                    bufferedImage.setRGB(q+j, p+i, rgb);
                }
            }
            q+=bands;
            if((y%(width/pageDiv))==0){
                a++;
            }
        }
        p+=bands;
        if((x%(height/pageDiv))==0){
            ch+=pageDiv;
        }
    }
    
    Graphics2D g=bufferedImage.createGraphics();
    BasicStroke bs = new BasicStroke(2);
    g.setStroke(bs);
    for(int z=1; z<=(pageDiv-1); z++){
        g.drawLine(0, z*((height*3)/pageDiv), bufferedImage.getWidth()-1, z*((height*3)/pageDiv));
        g.drawLine(z*((width*3)/pageDiv), 0, z*((width*3)/pageDiv), bufferedImage.getHeight()-1);
    }
    final Stage dialogStage = new Stage();
    dialogStage.initStyle(StageStyle.UTILITY);
    BorderPane popup=new BorderPane();
    final ImageView weaveIV=new ImageView();
    weaveIV.setImage(SwingFXUtils.toFXImage(bufferedImage, null));        
    weaveIV.setFitHeight(objConfiguration.HEIGHT);
    weaveIV.setFitWidth(objConfiguration.WIDTH);
    popup.setCenter(weaveIV);        
    popup.setId("popup");
    Scene popupScene = new Scene(popup);

    //For zoom Out weaving pattern
    final KeyCombination zoomOutKC = new KeyCodeCombination(KeyCode.SUBTRACT,KeyCombination.ALT_DOWN);
    //For undo weaving pattern
    final KeyCombination zoomInKC = new KeyCodeCombination(KeyCode.ADD,KeyCombination.ALT_DOWN);
    //For redo weaving pattern
    final KeyCombination normalKC = new KeyCodeCombination(KeyCode.EQUALS,KeyCombination.ALT_DOWN);
    //For redo weaving pattern
    final KeyCombination closeKC = new KeyCodeCombination(KeyCode.ESCAPE,KeyCombination.ALT_DOWN);

    popupScene.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler() {
        @Override
        public void handle(Event t) {
            if (closeKC.match((KeyEvent) t)){
                dialogStage.close();
            } else if (normalKC.match((KeyEvent) t)){
                weaveIV.setScaleX(1);
                weaveIV.setScaleY(1);
                weaveIV.setScaleZ(1);
            } else if (zoomInKC.match((KeyEvent) t)){
                weaveIV.setScaleX(weaveIV.getScaleX()*2);
                weaveIV.setScaleY(weaveIV.getScaleY()*2);
                weaveIV.setScaleZ(weaveIV.getScaleZ()*2);
            } else if (zoomOutKC.match((KeyEvent) t)){
                weaveIV.setScaleX(weaveIV.getScaleX()/2);
                weaveIV.setScaleY(weaveIV.getScaleY()/2);
                weaveIV.setScaleZ(weaveIV.getScaleZ()/2);
            }
        }
    });
    popupScene.setOnKeyPressed(new EventHandler<KeyEvent>() {
        @Override
        public void handle(KeyEvent t) {
            KeyCode key = t.getCode();
            if (key == KeyCode.ESCAPE){
                dialogStage.close();
            } else if (key == KeyCode.ENTER){
                weaveIV.setScaleX(1);
                weaveIV.setScaleY(1);
                weaveIV.setScaleZ(1);
            } else if (key == KeyCode.UP){
                weaveIV.setScaleX(weaveIV.getScaleX()*2);
                weaveIV.setScaleY(weaveIV.getScaleY()*2);
                weaveIV.setScaleZ(weaveIV.getScaleZ()*2);
            } else if (key == KeyCode.DOWN){
                weaveIV.setScaleX(weaveIV.getScaleX()/2);
                weaveIV.setScaleY(weaveIV.getScaleY()/2);
                weaveIV.setScaleZ(weaveIV.getScaleZ()/2);
            }
        }
    });

    popupScene.getStylesheets().add(getClass().getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
    dialogStage.setScene(popupScene);
    dialogStage.setX(-5);
    dialogStage.setY(0);
    dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("ACTIONTEXTUREVIEW"));
    dialogStage.showAndWait();

    tilledMatrix=null;
    bufferedImage = null;
    System.gc();
}*/    
/*
private void plotWarpColor() {
    warpColorGP.getChildren().clear();
    System.gc();
    for(int i=0;i<objWeave.getIntWarp();i++) {
        final int p=i;
        final Label lblbox = new Label("");
        lblbox.setPrefSize(boxSize,boxSize);
        lblbox.setStyle("-fx-background-color: "+objWeave.getWarpYarn()[i].getStrYarnColor()+";-fx-border-width: 1;  -fx-border-color: black");
        lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));

        lblbox.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount() >1) {
                    yarnPropertiesAction();
                } else {
                    lblbox.setStyle("-fx-background-color: #"+objWeave.getStrThreadPalettes()[intWarpColor]+";-fx-border-width: 1;  -fx-border-color: black");
                    lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                    objWeave.getWarpYarn()[p]= new Yarn(null, "Warp", objConfiguration.getStrWarpName(), "#"+objWeave.getStrThreadPalettes()[intWarpColor], objConfiguration.getIntWarpRepeat(), Character.toString((char)(65+intWarpColor)), objConfiguration.getIntWarpCount(), objConfiguration.getStrWarpUnit(), objConfiguration.getIntWarpPly(), objConfiguration.getIntWarpFactor(), objConfiguration.getDblWarpDiameter(), objConfiguration.getIntWarpTwist(), objConfiguration.getStrWarpSence(), objConfiguration.getIntWarpHairness(), objConfiguration.getIntWarpDistribution(), objConfiguration.getDblWarpPrice(), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);
                }
            }
        });
        lblbox.setOnDragDetected(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                // drag was detected, start a drag-and-drop gesture
                // allow any transfer mode 
                Dragboard db = lblbox.startDragAndDrop(TransferMode.ANY);
                // Put a string on a dragboard
                ClipboardContent content = new ClipboardContent();
                content.putString(objWeave.getStrThreadPalettes()[intWarpColor]);
                db.setContent(content);

                lblbox.setStyle("-fx-background-color: #"+objWeave.getStrThreadPalettes()[intWarpColor]+";-fx-border-width: 1;  -fx-border-color: black");
                lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                objWeave.getWarpYarn()[p]= new Yarn(null, "Warp", objConfiguration.getStrWarpName(), "#"+objWeave.getStrThreadPalettes()[intWarpColor], objConfiguration.getIntWarpRepeat(), Character.toString((char)(65+intWarpColor)), objConfiguration.getIntWarpCount(), objConfiguration.getStrWarpUnit(), objConfiguration.getIntWarpPly(), objConfiguration.getIntWarpFactor(), objConfiguration.getDblWarpDiameter(), objConfiguration.getIntWarpTwist(), objConfiguration.getStrWarpSence(), objConfiguration.getIntWarpHairness(), objConfiguration.getIntWarpDistribution(), objConfiguration.getDblWarpPrice(), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);

                event.consume();
            }
        });
        lblbox.setOnDragOver(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                // data is dragged over the target
                // accept it only if it is not dragged 
                if (event.getGestureSource() != lblbox && event.getDragboard().hasString()) {
                    lblbox.setStyle("-fx-background-color: #"+objWeave.getStrThreadPalettes()[intWarpColor]+";-fx-border-width: 1;  -fx-border-color: black");
                    lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                    objWeave.getWarpYarn()[p]= new Yarn(null, "Warp", objConfiguration.getStrWarpName(), "#"+objWeave.getStrThreadPalettes()[intWarpColor], objConfiguration.getIntWarpRepeat(), Character.toString((char)(65+intWarpColor)), objConfiguration.getIntWarpCount(), objConfiguration.getStrWarpUnit(), objConfiguration.getIntWarpPly(), objConfiguration.getIntWarpFactor(), objConfiguration.getDblWarpDiameter(), objConfiguration.getIntWarpTwist(), objConfiguration.getStrWarpSence(), objConfiguration.getIntWarpHairness(), objConfiguration.getIntWarpDistribution(), objConfiguration.getDblWarpPrice(), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);

                    event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                }
                event.consume();
            }
        });
        lblbox.setOnDragEntered(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                // the drag-and-drop gesture entered the target
                // show to the user that it is an actual gesture target
                if (event.getGestureSource() != lblbox && event.getDragboard().hasString()) {

                }
                event.consume();
            }
        });
        lblbox.setOnDragExited(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                // mouse moved away, remove the graphical cues
                event.consume();
            }
        });
        lblbox.setOnDragDropped(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                // data dropped
                // if there is a string data on dragboard, read it and use it
                Dragboard db = event.getDragboard();
                boolean success = false;
                // let the source know whether the string was successfully
                // transferred and used 
                lblbox.setStyle("-fx-background-color: #"+objWeave.getStrThreadPalettes()[intWarpColor]+";-fx-border-width: 1;  -fx-border-color: black");
                lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                objWeave.getWarpYarn()[p]= new Yarn(null, "Warp", objConfiguration.getStrWarpName(), "#"+objWeave.getStrThreadPalettes()[intWarpColor], objConfiguration.getIntWarpRepeat(), Character.toString((char)(65+intWarpColor)), objConfiguration.getIntWarpCount(), objConfiguration.getStrWarpUnit(), objConfiguration.getIntWarpPly(), objConfiguration.getIntWarpFactor(), objConfiguration.getDblWarpDiameter(), objConfiguration.getIntWarpTwist(), objConfiguration.getStrWarpSence(), objConfiguration.getIntWarpHairness(), objConfiguration.getIntWarpDistribution(), objConfiguration.getDblWarpPrice(), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);

                event.setDropCompleted(success);
                event.consume();
             }
        });
        lblbox.setOnDragDone(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                // the drag and drop gesture ended 
                // if the data was successfully moved, clear it 
                if (event.getTransferMode() == TransferMode.MOVE) {
                    Dragboard db = event.getDragboard();
                }
                event.consume();
            }
        });
        warpColorGP.add(lblbox,i,0);
    }    
}

private void plotWeftColor() {
    weftColorGP.getChildren().clear();
    System.gc();
    for(int i=0;i<objWeave.getIntWeft();i++) {
        final int p=i;
        final Label lblbox = new Label("");
        lblbox.setPrefSize(boxSize,boxSize);

        lblbox.setStyle("-fx-background-color: " + objWeave.getWeftYarn()[i].getStrYarnColor() + ";-fx-border-width: 1;  -fx-border-color: black");
        lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));

        lblbox.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount() >1) {
                    yarnPropertiesAction();
                } else{
                    lblbox.setStyle("-fx-background-color: #" + objWeave.getStrThreadPalettes()[intWeftColor] + ";-fx-border-width: 1;  -fx-border-color: black");
                    lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                    objWeave.getWeftYarn()[p]= new Yarn(null, "Weft", objConfiguration.getStrWeftName(), "#"+objWeave.getStrThreadPalettes()[intWeftColor], objConfiguration.getIntWeftRepeat(), Character.toString((char)(97+intWeftColor-26)), objConfiguration.getIntWeftCount(), objConfiguration.getStrWeftUnit(), objConfiguration.getIntWeftPly(), objConfiguration.getIntWeftFactor(), objConfiguration.getDblWeftDiameter(), objConfiguration.getIntWeftTwist(), objConfiguration.getStrWeftSence(), objConfiguration.getIntWeftHairness(), objConfiguration.getIntWeftDistribution(), objConfiguration.getDblWeftPrice(), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);
                }
            }
        });
        lblbox.setOnDragDetected(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                // drag was detected, start a drag-and-drop gesture
                // allow any transfer mode 
                Dragboard db = lblbox.startDragAndDrop(TransferMode.ANY);
                // Put a string on a dragboard 
                ClipboardContent content = new ClipboardContent();
                content.putString(objWeave.getStrThreadPalettes()[intWeftColor]);
                db.setContent(content);

                lblbox.setStyle("-fx-background-color: #"+objWeave.getStrThreadPalettes()[intWeftColor]+";-fx-border-width: 1;  -fx-border-color: black");
                lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                objWeave.getWeftYarn()[p]= new Yarn(null, "Weft", objConfiguration.getStrWeftName(), "#"+objWeave.getStrThreadPalettes()[intWeftColor], objConfiguration.getIntWeftRepeat(), Character.toString((char)(97+intWeftColor-26)), objConfiguration.getIntWeftCount(), objConfiguration.getStrWeftUnit(), objConfiguration.getIntWeftPly(), objConfiguration.getIntWeftFactor(), objConfiguration.getDblWeftDiameter(), objConfiguration.getIntWeftTwist(), objConfiguration.getStrWeftSence(), objConfiguration.getIntWeftHairness(), objConfiguration.getIntWeftDistribution(), objConfiguration.getDblWeftPrice(), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);
                event.consume();
            }
        });
        lblbox.setOnDragOver(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                // data is dragged over the target 
                // accept it only if it is not dragged 
                if (event.getGestureSource() != lblbox && event.getDragboard().hasString()) {
                    lblbox.setStyle("-fx-background-color: #"+objWeave.getStrThreadPalettes()[intWeftColor]+";-fx-border-width: 1;  -fx-border-color: black");
                    lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                    objWeave.getWeftYarn()[p]= new Yarn(null, "Weft", objConfiguration.getStrWeftName(), "#"+objWeave.getStrThreadPalettes()[intWeftColor], objConfiguration.getIntWeftRepeat(), Character.toString((char)(97+intWeftColor-26)), objConfiguration.getIntWeftCount(), objConfiguration.getStrWeftUnit(), objConfiguration.getIntWeftPly(), objConfiguration.getIntWeftFactor(), objConfiguration.getDblWeftDiameter(), objConfiguration.getIntWeftTwist(), objConfiguration.getStrWeftSence(), objConfiguration.getIntWeftHairness(), objConfiguration.getIntWeftDistribution(), objConfiguration.getDblWeftPrice(), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);
                    event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                }
                event.consume();
            }
        });
        lblbox.setOnDragEntered(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                // the drag-and-drop gesture entered the target 
                // show to the user that it is an actual gesture target
                if (event.getGestureSource() != lblbox && event.getDragboard().hasString()) {

                }
                event.consume();
            }
        });
        lblbox.setOnDragExited(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                // mouse moved away, remove the graphical cues
                event.consume();
            }
        });
        lblbox.setOnDragDropped(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                // data dropped
                // if there is a string data on dragboard, read it and use it 
                Dragboard db = event.getDragboard();
                boolean success = false;
                // let the source know whether the string was successfully
                // transferred and used 
                lblbox.setStyle("-fx-background-color: #"+objWeave.getStrThreadPalettes()[intWeftColor]+";-fx-border-width: 1;  -fx-border-color: black");
                lblbox.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                objWeave.getWeftYarn()[p]= new Yarn(null, "Weft", objConfiguration.getStrWeftName(), "#"+objWeave.getStrThreadPalettes()[intWeftColor], objConfiguration.getIntWeftRepeat(), Character.toString((char)(97+intWeftColor-26)), objConfiguration.getIntWeftCount(), objConfiguration.getStrWeftUnit(), objConfiguration.getIntWeftPly(), objConfiguration.getIntWeftFactor(), objConfiguration.getDblWeftDiameter(), objConfiguration.getIntWeftTwist(), objConfiguration.getStrWeftSence(), objConfiguration.getIntWeftHairness(), objConfiguration.getIntWeftDistribution(), objConfiguration.getDblWeftPrice(), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);
                event.setDropCompleted(success);
                event.consume();
             }
        });
        lblbox.setOnDragDone(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                // the drag and drop gesture ended 
                // if the data was successfully moved, clear it 
                if (event.getTransferMode() == TransferMode.MOVE) {
                    Dragboard db = event.getDragboard();
                }
                event.consume();
            }
        });
        weftColorGP.add(lblbox,0,i);
    }    
}    
   private void selectionBoxArea(final Label lblC,final int r,final int col){
         lblC.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
           }
        });
        lblC.setOnDragDetected(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                // drag was detected, start a drag-and-drop gesture
                // allow any transfer mode 
                Dragboard db = lblC.startDragAndDrop(TransferMode.ANY);
                // Put a string on a dragboard 
                ClipboardContent content = new ClipboardContent();
                content.putString(lblC.getStyle().substring(lblC.getStyle().lastIndexOf("-fx-background-color:")+21,lblC.getStyle().indexOf(";")).trim());
                db.setContent(content);
                for(int i=initial_row,k=0;i<=final_row;i++){
                    for(int j=initial_col;j<=final_col;j++){
                        int m=(i*objWeave.getIntWarp())+j;
                        designGP.getChildren().get(m).setStyle("-fx-border-color: black;-fx-border-width: 1;");     
                    }
                }     
                initial_row=r;
                initial_col=col;
                event.consume();
            }
        });
        lblC.setOnDragOver(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                // data is dragged over the target 
                // accept it only if it is not dragged 
                if (event.getGestureSource() != lblC && event.getDragboard().hasString()) {
                     event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                      }
                event.consume();
            }
        });
        lblC.setOnDragEntered(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
            // the drag-and-drop gesture entered the target 
            // show to the user that it is an actual gesture target 
                 if (event.getGestureSource() != lblC && event.getDragboard().hasString()) {
                 }
                     event.consume();
            }
        });
        lblC.setOnDragExited(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                // mouse moved away, remove the graphical cues 
                  event.consume();
            }
        });
        lblC.setOnDragDropped(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                try {
                    // data dropped 
                    // if there is a string data on dragboard, read it and use it 
                    Dragboard db = event.getDragboard();
                    boolean success = false;
                    // let the source know whether the string was successfully
                    // transferred and used 
                    final_row=r;
                    final_col=col;
                    int size_x=initial_row-final_row;
                    int size_y=final_col-initial_col;        
          
                    final_row=initial_row-size_x;
                    final_col=initial_col+size_y;
                     int m=0,p=0;
                     for(int i=initial_row,k=0;i<=final_row;i++){
                        for(int j=initial_col;j<=final_col;j++,m++){
                            m=(i*objWeave.getIntWarp())+j;
                            if(objWeave.getDesignMatrix()[i][j]==0)
                                designGP.getChildren().get(m).setStyle("-fx-background-color: #FFFFFF;-fx-border-width: 1;  -fx-border-color: red");
                            else
                                designGP.getChildren().get(m).setStyle("-fx-background-color: #000000;-fx-border-width: 1;  -fx-border-color: red");
                        }
                    }
                    event.setDropCompleted(success);
                    event.consume();
                } catch (Exception ex) {
                   new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                   lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
             }
        });
        lblC.setOnDragDone(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                // the drag and drop gesture ended 
                // if the data was successfully moved, clear it 
                if (event.getTransferMode() == TransferMode.MOVE) {
                    Dragboard db = event.getDragboard();
                }
                event.consume();
            }
        });
    }
   private void selectionLineArea(final Label lblC,final int row,final int col){
        lblC.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
           }
        });
        lblC.setOnDragDetected(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                byte [][]designPaste=new byte[objWeave.getIntWeft()][objWeave.getIntWarp()];
                for(int i=0;i<objWeave.getIntWeft();i++){
                    for(int j=0;j<objWeave.getIntWarp();j++){
                       designPaste[i][j]=objWeave.getDesignMatrix()[i][j];   
                    }
               }             
                // drag was detected, start a drag-and-drop gesture
                // allow any transfer mode 
                Dragboard db = lblC.startDragAndDrop(TransferMode.ANY);
                // Put a string on a dragboard 
                ClipboardContent content = new ClipboardContent();
                initial_row=row;
                initial_col=col;
                if(event.isSecondaryButtonDown()){
                    lblC.setStyle("-fx-background-color: #ffffff;-fx-border-width: 1;  -fx-border-color: black");
                    lblC.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                    objWeave.getDesignMatrix()[row][col]=0;
                    content.putString("isSecondaryButtonDown");
                }else{
                    lblC.setStyle("-fx-background-color: #000000;-fx-border-width: 1;  -fx-border-color: black");
                    lblC.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                    objWeave.getDesignMatrix()[row][col]=1;
                    content.putString("isPrimaryButtonDown");
                }
                db.setContent(content);
                event.consume();
            }
        });
        lblC.setOnDragOver(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                // data is dragged over the target 
                // accept it only if it is not dragged 
                if (event.getGestureSource() != lblC && event.getDragboard().hasString()) {
                    if(event.getDragboard().getString().equalsIgnoreCase("isSecondaryButtonDown")){
                        lblC.setStyle("-fx-background-color: #ffffff;-fx-border-width: 1;  -fx-border-color: black");
                        lblC.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                        objWeave.getDesignMatrix()[row][col]=0;
                    }else{
                        lblC.setStyle("-fx-background-color: #000000;-fx-border-width: 1;  -fx-border-color: black");
                        lblC.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                        objWeave.getDesignMatrix()[row][col]=1;
                    }
                    event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                }
                event.consume();
            }
        });
        lblC.setOnDragEntered(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
            // the drag-and-drop gesture entered the target 
            // show to the user that it is an actual gesture target 
                 if (event.getGestureSource() != lblC && event.getDragboard().hasString()) {
                 }
                     event.consume();
            }
        });
        lblC.setOnDragExited(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                // mouse moved away, remove the graphical cues 
                  event.consume();
            }
        });
        lblC.setOnDragDropped(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                try {
                    // data dropped 
                    // if there is a string data on dragboard, read it and use it 
                    Dragboard db = event.getDragboard();
                    boolean success = false;
                    // let the source know whether the string was successfully
                    // transferred and used 
                    if(event.getDragboard().getString().equalsIgnoreCase("isSecondaryButtonDown")){
                        lblC.setStyle("-fx-background-color: #ffffff;-fx-border-width: 1;  -fx-border-color: black");
                        lblC.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                        objWeave.getDesignMatrix()[row][col]=0;
                    }else{
                        lblC.setStyle("-fx-background-color: #000000;-fx-border-width: 1;  -fx-border-color: black");
                        lblC.setFont(javafx.scene.text.Font.font("Arial", FontWeight.NORMAL, boxSize));
                        objWeave.getDesignMatrix()[row][col]=1;
                    }
                    final_row=row;
                    final_col=col;
                    populateNplot();
                    
                    event.setDropCompleted(success);
                    event.consume();
                } catch (Exception ex) {
                   new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                   lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
             }
        });
        lblC.setOnDragDone(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                // the drag and drop gesture ended 
                // if the data was successfully moved, clear it 
                if (event.getTransferMode() == TransferMode.MOVE) {
                    Dragboard db = event.getDragboard();
                }
                event.consume();
            }
        });
    }
*/    
}