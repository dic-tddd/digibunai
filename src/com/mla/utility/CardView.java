package com.mla.utility;

import com.mla.dictionary.DictionaryAction;
import com.mla.main.Configuration;
import com.mla.main.Logging;
import com.mla.main.MessageView;
import static com.mla.utility.CardView.blockPixels;
import com.mla.weave.Weave;
import com.mla.weave.WeaveAction;
import com.mla.weave.WeaveEditView;
import com.mla.weave.WeaveImportView;
import com.sun.media.jai.codec.ByteArraySeekableStream;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.SeekableStream;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import javax.media.jai.PlanarImage;

/**
 * This class provides provision to generate BMP for e-jacquard support
 * @see FabricView.java, FabricAction.java, ArtworkView.java
 * @since 0.9.7
 * @author Aatif Ahmad Khan
 */
public class CardView {
    
    Stage cardViewStage;
    Configuration objConfiguration;
    DictionaryAction objDictionaryAction;
    ImageView bmpIV;
    BufferedImage bmpImage;
    BufferedImage inputSplitGraphImage;
    Weave objWeave; // selvedge weave
    Label lblStatus;
    
    int numLeftHooks = 0; // number of hooks allocated on left side of design split graph
    int numRightHooks = 0; // number of hooks allocated on right side of design split graph
    int startDesignHooks = 0; // starting hook number for design (split graph) area
    // int numDesignHooks = 0; // total design hooks == inputSplitGraphImage.getWidth()
    int startFingerHooks = 0; // starting number of Finger Selection Hooks
    int numFingerHooks = 0; // total Finger Selection Hooks allocated
    int startRegulatorHooks = 0; // starting number of Regulator/Cramming Hooks
    int numRegulatorHooks = 0; // total Regulator/Cramming Hooks allocated
    int startSelvedgeHooks = 0; // starting number of Selvedge Hooks
    int numSelvedgeHooks = 0; // total Selvedge Hooks allocated
    
    boolean invertDesignFlag;
    boolean manualEditMode;
    
    GridPane fingersGP; // UI for assigning colors to fingers
    int[] fingerColorMap; // int RGB size: 0-numFingerHooks-1
    int[] pickTypeMap; // 0: base weft, 1: 1st Extra Weft, 2: 2nd Extra Weft
    
    final static int MAX_EXTRA_HOOKS = 100;
    
    final static String regExInt="[0-9]+";
    
    static int blockPixels = 1; // number of pixels for denoting 1 colored box
    
    public CardView(BufferedImage inputImage, Configuration objConfigurationCall){
        objConfiguration = objConfigurationCall;
        objDictionaryAction  = new DictionaryAction(objConfiguration);
        inputSplitGraphImage = inputImage;
        blockPixels = (inputSplitGraphImage.getWidth()*inputSplitGraphImage.getHeight()<=360000)?5:1;
        bmpIV = new ImageView();
        pickTypeMap = new int[inputSplitGraphImage.getHeight()];
        initPickTypeMap();
        lblStatus = new Label();
        lblStatus.setPrefWidth(200);
        
        cardViewStage = new Stage();
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 800, 600, Color.TRANSPARENT);
        ScrollPane imageSP = new ScrollPane();
        imageSP.setContent(bmpIV);
        
        Button btnRefreshDesign = new Button(objDictionaryAction.getWord("REFRESHIMAGE"));
        btnRefreshDesign.setStyle("-fx-font-weight: bold");
        btnRefreshDesign.setPrefWidth(200);
        btnRefreshDesign.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                prepareImage();
            }
        });
        
        // right panel
        VBox container = new VBox(10);
        
        Accordion optionsPane = new Accordion();
        optionsPane.setPrefWidth(200);
        optionsPane.setPrefHeight(400);
        TitledPane extraHooksTP=new TitledPane();
        TitledPane fingerSelectionTP=new TitledPane();
        TitledPane regulatorTP=new TitledPane();
        TitledPane selvedgeTP=new TitledPane();
        extraHooksTP.setText(objDictionaryAction.getWord("EXTRAHOOKS"));
        fingerSelectionTP.setText(objDictionaryAction.getWord("FINGERSELECTION"));
        regulatorTP.setText(objDictionaryAction.getWord("REGULATOR"));
        selvedgeTP.setText(objDictionaryAction.getWord("SELVAGE"));
        
        optionsPane.getPanes().addAll(extraHooksTP, fingerSelectionTP, regulatorTP, selvedgeTP);
        optionsPane.setExpandedPane(extraHooksTP);
        
        // Extra Hooks Tab
        GridPane extraHooksGP = new GridPane();
        extraHooksGP.setHgap(5);
        extraHooksGP.setVgap(5);
        Label lblLeftHooks = new Label(objDictionaryAction.getWord("LEFT")+": ");
        Label lblRightHooks = new Label(objDictionaryAction.getWord("RIGHT")+": ");
        final TextField txtLeftHooks = new TextField(String.valueOf(numLeftHooks));
        final TextField txtRightHooks = new TextField(String.valueOf(numRightHooks));
        txtLeftHooks.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!t1.matches(regExInt)){
                    txtLeftHooks.setText(t);
                    lblStatus.setText(objDictionaryAction.getWord("ONLYINTEGERALLOWED"));
                    return;
                }
                int value = Integer.parseInt(t1);
                if(value>=0&&value<=MAX_EXTRA_HOOKS)
                    numLeftHooks=value;
                else{
                    txtLeftHooks.setText(t);
                    lblStatus.setText(objDictionaryAction.getWord("VALUEOUTOFRANGE")+MAX_EXTRA_HOOKS);
                }
            }
        });
        txtRightHooks.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!t1.matches(regExInt)){
                    txtRightHooks.setText(t);
                    lblStatus.setText(objDictionaryAction.getWord("ONLYINTEGERALLOWED"));
                    return;
                }
                int value = Integer.parseInt(t1);
                if(value>=0&&value<=MAX_EXTRA_HOOKS)
                    numRightHooks=value;
                else{
                    txtRightHooks.setText(t);
                    lblStatus.setText(objDictionaryAction.getWord("VALUEOUTOFRANGE")+MAX_EXTRA_HOOKS);
                }
            }
        });
        extraHooksGP.add(lblLeftHooks, 0, 0);
        extraHooksGP.add(txtLeftHooks, 1, 0);
        extraHooksGP.add(lblRightHooks, 0, 1);
        extraHooksGP.add(txtRightHooks, 1, 1);
        extraHooksTP.setContent(extraHooksGP);
        
        // Finger Selection Tab
        GridPane fingerSelectionGP = new GridPane();
        fingerSelectionGP.setHgap(5);
        fingerSelectionGP.setVgap(5);
        Label lblFingerStartHook = new Label(objDictionaryAction.getWord("START")+": ");
        Label lblFingerEndHook = new Label(objDictionaryAction.getWord("END")+": ");
        Label lblFingerCount = new Label(objDictionaryAction.getWord("COUNT")+": ");
        final TextField txtFingerStartHook = new TextField(String.valueOf(startFingerHooks));
        final TextField txtFingerEndHook = new TextField(String.valueOf(startFingerHooks+numFingerHooks));
        final TextField txtFingerCount = new TextField(String.valueOf(numFingerHooks));
        txtFingerStartHook.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!txtFingerStartHook.isFocused())
                    return;
                if(!t1.matches(regExInt)){
                    txtFingerStartHook.setText(t);
                    lblStatus.setText(objDictionaryAction.getWord("ONLYINTEGERALLOWED"));
                    return;
                }
                int value = Integer.parseInt(t1);
                if(value>=0&&value<=numLeftHooks){
                    startFingerHooks=value;
                    if(Integer.parseInt(txtFingerEndHook.getText())>startFingerHooks){
                        numFingerHooks = Integer.parseInt(txtFingerEndHook.getText())-startFingerHooks;
                        txtFingerCount.setText(String.valueOf(numFingerHooks));
                    }
                    else{
                        numFingerHooks = 0;
                        txtFingerCount.setText("0");
                    }
                }
                else{
                    txtFingerStartHook.setText(t);
                    lblStatus.setText(objDictionaryAction.getWord("VALUEEXCEEDSHOOKS"));
                }
            }
        });
        txtFingerEndHook.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!txtFingerEndHook.isFocused())
                    return;
                if(!t1.matches(regExInt)){
                    txtFingerEndHook.setText(t);
                    lblStatus.setText(objDictionaryAction.getWord("ONLYINTEGERALLOWED"));
                    return;
                }
                int value = Integer.parseInt(t1);
                if(value>=0&&value<=numLeftHooks){
                    if(startFingerHooks<=value){
                        numFingerHooks = value-startFingerHooks+1;
                        txtFingerCount.setText(String.valueOf(numFingerHooks));
                    }
                    else{
                        numFingerHooks = 0;
                        txtFingerCount.setText("0");
                    }
                }
                else{
                    txtFingerEndHook.setText(t);
                    lblStatus.setText(objDictionaryAction.getWord("VALUEEXCEEDSHOOKS"));
                }
            }
        });
        txtFingerCount.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!txtFingerCount.isFocused()){
                    plotFingerSelection();
                    return;
                }
                if(!t1.matches(regExInt)){
                    txtFingerCount.setText(t);
                    lblStatus.setText(objDictionaryAction.getWord("ONLYINTEGERALLOWED"));
                    return;
                }
                int value = Integer.parseInt(t1);
                if(value>=0&&(value+startFingerHooks-1)<=numLeftHooks){
                    numFingerHooks=value;
                    txtFingerCount.setText(String.valueOf(numFingerHooks));
                    txtFingerEndHook.setText(String.valueOf(startFingerHooks+numFingerHooks-1));
                }
                else{
                    txtFingerCount.setText(t);
                    lblStatus.setText(objDictionaryAction.getWord("VALUEEXCEEDSHOOKS"));
                }
                plotFingerSelection();
            }
        });
        
        fingerSelectionGP.add(lblFingerStartHook, 0, 0);
        fingerSelectionGP.add(txtFingerStartHook, 1, 0);
        fingerSelectionGP.add(lblFingerEndHook, 0, 1);
        fingerSelectionGP.add(txtFingerEndHook, 1, 1);
        fingerSelectionGP.add(lblFingerCount, 0, 2);
        fingerSelectionGP.add(txtFingerCount, 1, 2);
        fingersGP = new GridPane();
        fingerColorMap = new int[numFingerHooks];
        ScrollPane fingersSP = new ScrollPane();
        fingersSP.setPrefHeight(fingerSelectionTP.getPrefHeight());
        fingersSP.setContent(fingersGP);
        fingerSelectionGP.add(fingersSP, 0, 3, 2, 1);
        fingerSelectionTP.setContent(fingerSelectionGP);
        
        // Regulator Tab
        GridPane regulatorGP = new GridPane();
        regulatorGP.setHgap(5);
        regulatorGP.setVgap(5);
        Label lblRegulatorStartHook = new Label(objDictionaryAction.getWord("START")+": ");
        Label lblRegulatorEndHook = new Label(objDictionaryAction.getWord("END")+": ");
        Label lblRegulatorCount = new Label(objDictionaryAction.getWord("COUNT")+": ");
        final TextField txtRegulatorStartHook = new TextField(String.valueOf(startRegulatorHooks));
        final TextField txtRegulatorEndHook = new TextField(String.valueOf(startRegulatorHooks+numRegulatorHooks));
        final TextField txtRegulatorCount = new TextField(String.valueOf(numRegulatorHooks));
        txtRegulatorStartHook.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!txtRegulatorStartHook.isFocused())
                    return;
                if(!t1.matches(regExInt)){
                    txtRegulatorStartHook.setText(t);
                    lblStatus.setText(objDictionaryAction.getWord("ONLYINTEGERALLOWED"));
                    return;
                }
                int value = Integer.parseInt(t1);
                if(value>=0&&value<=numLeftHooks){
                    startRegulatorHooks=value;
                    if(Integer.parseInt(txtRegulatorEndHook.getText())>startRegulatorHooks){
                        numRegulatorHooks = Integer.parseInt(txtRegulatorEndHook.getText())-startRegulatorHooks;
                        txtRegulatorCount.setText(String.valueOf(numRegulatorHooks));
                    }
                    else{
                        numRegulatorHooks = 0;
                        txtRegulatorCount.setText("0");
                    }
                }
                else{
                    txtRegulatorStartHook.setText(t);
                    lblStatus.setText(objDictionaryAction.getWord("VALUEEXCEEDSHOOKS"));
                }
            }
        });
        txtRegulatorEndHook.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!txtRegulatorEndHook.isFocused())
                    return;
                if(!t1.matches(regExInt)){
                    txtRegulatorEndHook.setText(t);
                    lblStatus.setText(objDictionaryAction.getWord("ONLYINTEGERALLOWED"));
                    return;
                }
                int value = Integer.parseInt(t1);
                if(value>=0&&value<=numLeftHooks){
                    if(startRegulatorHooks<=value){
                        numRegulatorHooks = value-startRegulatorHooks+1;
                        txtRegulatorCount.setText(String.valueOf(numRegulatorHooks));
                    }
                    else{
                        numRegulatorHooks = 0;
                        txtRegulatorCount.setText("0");
                    }
                }
                else{
                    txtRegulatorEndHook.setText(t);
                    lblStatus.setText(objDictionaryAction.getWord("VALUEEXCEEDSHOOKS"));
                }
            }
        });
        txtRegulatorCount.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!txtRegulatorCount.isFocused())
                    return;
                if(!t1.matches(regExInt)){
                    txtRegulatorCount.setText(t);
                    lblStatus.setText(objDictionaryAction.getWord("ONLYINTEGERALLOWED"));
                    return;
                }
                int value = Integer.parseInt(t1);
                if(value>=0&&(value+startRegulatorHooks-1)<=numLeftHooks){
                    numRegulatorHooks=value;
                    txtRegulatorCount.setText(String.valueOf(numRegulatorHooks));
                    txtRegulatorEndHook.setText(String.valueOf(startRegulatorHooks+numRegulatorHooks-1));
                }
                else{
                    txtRegulatorCount.setText(t);
                    lblStatus.setText(objDictionaryAction.getWord("VALUEEXCEEDSHOOKS"));
                }
            }
        });
        regulatorGP.add(lblRegulatorStartHook, 0, 0);
        regulatorGP.add(txtRegulatorStartHook, 1, 0);
        regulatorGP.add(lblRegulatorEndHook, 0, 1);
        regulatorGP.add(txtRegulatorEndHook, 1, 1);
        regulatorGP.add(lblRegulatorCount, 0, 2);
        regulatorGP.add(txtRegulatorCount, 1, 2);
        regulatorTP.setContent(regulatorGP);
        
        // Selvedge Tab
        GridPane selvedgeGP = new GridPane();
        selvedgeGP.setHgap(5);
        selvedgeGP.setVgap(5);
        Label lblSelvedgeStartHook = new Label(objDictionaryAction.getWord("START")+": ");
        Label lblSelvedgeEndHook = new Label(objDictionaryAction.getWord("END")+": ");
        Label lblSelvedgeCount = new Label(objDictionaryAction.getWord("COUNT")+": ");
        final TextField txtSelvedgeStartHook = new TextField(String.valueOf(startSelvedgeHooks));
        final TextField txtSelvedgeEndHook = new TextField(String.valueOf(startSelvedgeHooks+numSelvedgeHooks));
        final TextField txtSelvedgeCount = new TextField(String.valueOf(numSelvedgeHooks));
        txtSelvedgeStartHook.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!txtSelvedgeStartHook.isFocused())
                    return;
                if(!t1.matches(regExInt)){
                    txtSelvedgeStartHook.setText(t);
                    lblStatus.setText(objDictionaryAction.getWord("ONLYINTEGERALLOWED"));
                    return;
                }
                int value = Integer.parseInt(t1);
                if(value>=0&&value<=numLeftHooks){
                    startSelvedgeHooks=value;
                    if(Integer.parseInt(txtSelvedgeEndHook.getText())>startSelvedgeHooks){
                        numSelvedgeHooks = Integer.parseInt(txtSelvedgeEndHook.getText())-startSelvedgeHooks;
                        txtSelvedgeCount.setText(String.valueOf(numSelvedgeHooks));
                    }
                    else{
                        numSelvedgeHooks = 0;
                        txtSelvedgeCount.setText("0");
                    }
                }
                else{
                    txtSelvedgeStartHook.setText(t);
                    lblStatus.setText(objDictionaryAction.getWord("VALUEEXCEEDSHOOKS"));
                }
            }
        });
        txtSelvedgeEndHook.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!txtSelvedgeEndHook.isFocused())
                    return;
                if(!t1.matches(regExInt)){
                    txtSelvedgeEndHook.setText(t);
                    lblStatus.setText(objDictionaryAction.getWord("ONLYINTEGERALLOWED"));
                    return;
                }
                int value = Integer.parseInt(t1);
                if(value>=0&&value<=numLeftHooks){
                    if(startSelvedgeHooks<=value){
                        numSelvedgeHooks = value-startSelvedgeHooks+1;
                        txtSelvedgeCount.setText(String.valueOf(numSelvedgeHooks));
                    }
                    else{
                        numSelvedgeHooks = 0;
                        txtSelvedgeCount.setText("0");
                    }
                }
                else{
                    txtSelvedgeEndHook.setText(t);
                    lblStatus.setText(objDictionaryAction.getWord("VALUEEXCEEDSHOOKS"));
                }
            }
        });
        txtSelvedgeCount.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(!txtSelvedgeCount.isFocused())
                    return;
                if(!t1.matches(regExInt)){
                    txtSelvedgeCount.setText(t);
                    lblStatus.setText(objDictionaryAction.getWord("ONLYINTEGERALLOWED"));
                    return;
                }
                int value = Integer.parseInt(t1);
                if(value>=0&&(value+startSelvedgeHooks-1)<=numLeftHooks){
                    numSelvedgeHooks=value;
                    txtSelvedgeCount.setText(String.valueOf(numSelvedgeHooks));
                    txtSelvedgeEndHook.setText(String.valueOf(startSelvedgeHooks+numSelvedgeHooks-1));
                }
                else{
                    txtSelvedgeCount.setText(t);
                    lblStatus.setText(objDictionaryAction.getWord("VALUEEXCEEDSHOOKS"));
                }
                prepareSelvedgeArea();
            }
        });
        selvedgeGP.add(lblSelvedgeStartHook, 0, 0);
        selvedgeGP.add(txtSelvedgeStartHook, 1, 0);
        selvedgeGP.add(lblSelvedgeEndHook, 0, 1);
        selvedgeGP.add(txtSelvedgeEndHook, 1, 1);
        selvedgeGP.add(lblSelvedgeCount, 0, 2);
        selvedgeGP.add(txtSelvedgeCount, 1, 2);
        final ImageView weaveIV = new ImageView(new Image("/media/assign_weave.png"));
        weaveIV.setFitHeight(50);
        weaveIV.setFitWidth(50);
        weaveIV.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                if(t.isControlDown()){
                    try {
                        objWeave = new Weave();
                        objWeave.setObjConfiguration(objConfiguration);
                        WeaveEditView obWeaveEditView = new WeaveEditView(objWeave);
                        if(objWeave!=null && objWeave.getStrWeaveID()!=null && objWeave.getStrWeaveID()!=""){
                            byte[] bytArtworkThumbnil=objWeave.getBytWeaveThumbnil();
                            SeekableStream stream = new ByteArraySeekableStream(bytArtworkThumbnil);
                            String[] names = ImageCodec.getDecoderNames(stream);
                            ImageDecoder dec = ImageCodec.createImageDecoder(names[0], stream, null);
                            RenderedImage im = dec.decodeAsRenderedImage();
                            weaveIV.setImage(SwingFXUtils.toFXImage(PlanarImage.wrapRenderedImage(im).getAsBufferedImage(), null));
                            bytArtworkThumbnil=null;
                            System.gc();
                        }else{
                            weaveIV.setImage(new Image("/media/assign_weave.png"));
                            lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
                        }
                    } catch (Exception ex) {
                        new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                        lblStatus.setText(objDictionaryAction.getWord("INVALIDWEAVE"));
                    }
                }                                    
            }
        });
        final Hyperlink weaveDefaultHL = new Hyperlink();
        weaveDefaultHL.setText(objDictionaryAction.getWord("CHANGEDEFAULT"));
        weaveDefaultHL.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    objWeave= new Weave();
                    objWeave.setObjConfiguration(objConfiguration);
                    WeaveImportView objWeaveImportView= new WeaveImportView(objWeave);

                    if(objWeave!=null && objWeave.getStrWeaveID()!=null && objWeave.getStrWeaveID()!=""){
                        byte[] bytArtworkThumbnil=objWeave.getBytWeaveThumbnil();
                        SeekableStream stream = new ByteArraySeekableStream(bytArtworkThumbnil);
                        String[] names = ImageCodec.getDecoderNames(stream);
                        ImageDecoder dec = ImageCodec.createImageDecoder(names[0], stream, null);
                        RenderedImage im = dec.decodeAsRenderedImage();
                        weaveIV.setImage(SwingFXUtils.toFXImage(PlanarImage.wrapRenderedImage(im).getAsBufferedImage(), null));
                        bytArtworkThumbnil=null;
                        System.gc();
                    }else{
                        weaveIV.setImage(new Image("/media/assign_weave.png"));
                        lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
                    }
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("INVALIDWEAVE"));
                }
            }
        });
        selvedgeGP.add(weaveIV, 0, 3);
        selvedgeGP.add(weaveDefaultHL, 1, 3);
        selvedgeTP.setContent(selvedgeGP);
        
        CheckBox chkManualEdit = new CheckBox(objDictionaryAction.getWord("MANUALEDITING"));
        chkManualEdit.setPrefWidth(200);
        chkManualEdit.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                manualEditMode = t1;
            }
        });
        
        bmpIV.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                if(manualEditMode){
                    if(t.getX()<numLeftHooks*blockPixels){ // before design area
                        // click inside finger selector area
                        if((t.getX()>=(startFingerHooks-1)*blockPixels)&&(t.getX()<(startFingerHooks-1+numFingerHooks)*blockPixels)){
                            if(t.getButton()==MouseButton.PRIMARY) // left click
                                bmpImage.setRGB((int)t.getX()/blockPixels, (int)t.getY()/blockPixels, fingerColorMap[((int)t.getX()/blockPixels)%numFingerHooks]);
                            else if(t.getButton()==MouseButton.SECONDARY) // right click
                                bmpImage.setRGB((int)t.getX()/blockPixels, (int)t.getY()/blockPixels, -1);
                            refreshImage();
                        }
                        // click inside regulator area
                        else if((t.getX()>=(startRegulatorHooks-1)*blockPixels)&&(t.getX()<(startRegulatorHooks-1+numRegulatorHooks)*blockPixels)){
                            if(t.getButton()==MouseButton.PRIMARY) // left click
                                bmpImage.setRGB((int)t.getX()/blockPixels, (int)t.getY()/blockPixels, -16777216);
                            else if(t.getButton()==MouseButton.SECONDARY) // right click
                                bmpImage.setRGB((int)t.getX()/blockPixels, (int)t.getY()/blockPixels, -1);
                            refreshImage();
                        }
                        // click inside selvedge area
                        else if((t.getX()>=(startSelvedgeHooks-1)*blockPixels)&&(t.getX()<(startSelvedgeHooks-1+numSelvedgeHooks)*blockPixels)){
                            if(t.getButton()==MouseButton.PRIMARY) // left click
                                bmpImage.setRGB((int)t.getX()/blockPixels, (int)t.getY()/blockPixels, -16777216);
                            else if(t.getButton()==MouseButton.SECONDARY) // right click
                                bmpImage.setRGB((int)t.getX()/blockPixels, (int)t.getY()/blockPixels, -1);
                            refreshImage();
                        }
                    }
                }
            }
        });
        
        final CheckBox chkInvertDesign = new CheckBox(objDictionaryAction.getWord("INVERTDESIGN"));
        chkInvertDesign.setPrefWidth(200);
        chkInvertDesign.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                invertDesignFlag = t1;
            }
        });
        
        final CheckBox chkColorExport = new CheckBox(objDictionaryAction.getWord("EXPORTINCOLOR"));
        chkColorExport.setPrefWidth(200);
        chkColorExport.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                chkInvertDesign.setDisable(t1);
            }
        });
        
        Button btnExportBMP = new Button(objDictionaryAction.getWord("EXPORT"));
        btnExportBMP.setStyle("-fx-font-weight: bold");
        btnExportBMP.setPrefWidth(200);
        btnExportBMP.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                String strProVersion = "This feature is available only in the Pro version.\n"
                + "For Pro version, please send a request on: satyavir@digitalindia.gov.in";
                new MessageView("notification", "Pro Version Required", strProVersion);
                /*if(!validateHookNumbers()){
                    lblStatus.setText(objDictionaryAction.getWord("ERRORHOOKALLOCATION"));
                    return;
                }
                prepareImage();
                if(!chkColorExport.isSelected()){
                    convertToGrayscale();
                    // now bmpImage is BINARY
                    if(invertDesignFlag){
                        // invert finger, regulator, selvedge and design hooks
                        invertPortionInBmpImage(startFingerHooks-1, numFingerHooks);
                        invertPortionInBmpImage(startRegulatorHooks-1, numRegulatorHooks);
                        invertPortionInBmpImage(startSelvedgeHooks-1, numSelvedgeHooks);
                        invertPortionInBmpImage(startDesignHooks, inputSplitGraphImage.getWidth());
                    }   
                }
                exportBMPImage();*/
            }
        });
        
        container.getChildren().addAll(btnRefreshDesign, optionsPane, chkManualEdit, chkInvertDesign, chkColorExport, btnExportBMP, lblStatus);
        root.setRight(container);
        root.setCenter(imageSP);
        prepareImage();
        
        cardViewStage.getIcons().add(new Image("/media/icon.png"));
        cardViewStage.setTitle(objDictionaryAction.getWord("CARDVIEW"));
        cardViewStage.setIconified(false);
        cardViewStage.setResizable(false);
        cardViewStage.setScene(scene);
        cardViewStage.showAndWait();
    }
    
    /**
     * Prepares image with split graph and updated number of extra hooks
     * with their values
     */
    private void prepareImage(){
        lblStatus.setText("");
        if(!validateHookNumbers()){
            lblStatus.setText(objDictionaryAction.getWord("ERRORHOOKALLOCATION"));
            return;
        }
        bmpImage = null;
        System.gc();
        bmpImage = new BufferedImage(inputSplitGraphImage.getWidth()+numLeftHooks+numRightHooks
                , inputSplitGraphImage.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = bmpImage.createGraphics();
        g2d.setBackground(java.awt.Color.WHITE);
        g2d.clearRect(0, 0, bmpImage.getWidth(), bmpImage.getHeight());
        g2d.dispose();
        prepareFingerSelectionArea();
        prepareRegulatorArea();
        prepareSelvedgeArea();
        prepareDesignArea();
        refreshImage();
    }
    
    /**
     * Copies design area pixels from inputSplitGraphImage into bmpImage at required startCol (0..bmpImage.WIDTH-1)
     * startCol + inputSplitGraphImage.WIDTH < bmpImage.WIDTH
     * copySplitGraphPixels()
     */
    private void prepareDesignArea(){
        for(int row=0; row<inputSplitGraphImage.getHeight(); row++)
            for(int col=0; col<inputSplitGraphImage.getWidth(); col++)
                bmpImage.setRGB(startDesignHooks+col, row, inputSplitGraphImage.getRGB(col, row));
    }
    
    /**
     * Insert color values from fingerColorMap[] for corresponding finger hooks
     * @see plotFingerSelection()
     */
    private void prepareFingerSelectionArea(){
        for(int row=0; row<inputSplitGraphImage.getHeight(); row++)
            for(int col=startFingerHooks-1; col<startFingerHooks-1+numFingerHooks; col++){
                if(pickTypeMap[row]==col-(startFingerHooks-1))
                    bmpImage.setRGB(col, row, fingerColorMap[col-(startFingerHooks-1)]);
            }
    }
    
    /**
     * Provision to choose finger selector colors for selected finger hooks
     */
    private void plotFingerSelection(){
        fingersGP.getChildren().clear();
        fingerColorMap = null;
        System.gc();
        fingerColorMap = new int[numFingerHooks];
        for(int fCount=0; fCount<numFingerHooks; fCount++)
            fingerColorMap[fCount]=-1; // SOLID WHITE
        for(int fCount=0; fCount<numFingerHooks; fCount++){
            final Label lblFingerColor = new Label(objDictionaryAction.getWord("FINGER")+" "+String.valueOf(fCount+1));
            final ColorPicker fingerCP = new ColorPicker();
            final int index = fCount;
            fingerCP.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    lblFingerColor.setStyle("-fx-background-color:#"+fingerCP.getValue().toString().substring(2, 8)+";"); //0xaabbccdd
                    fingerColorMap[index] = java.awt.Color.decode("#"+fingerCP.getValue().toString().substring(2, 8)).getRGB();
                }
            });
            fingersGP.add(lblFingerColor, 0, fCount);
            fingersGP.add(fingerCP, 1, fCount);
        }
    }
    
    /**
     * Insert black pixel for extra wefts in regulator area
     */
    private void prepareRegulatorArea(){
        for(int row=0; row<inputSplitGraphImage.getHeight(); row++)
            for(int col=startRegulatorHooks-1; col<startRegulatorHooks-1+numRegulatorHooks; col++){
                if(pickTypeMap[row]>0)// extra weft
                    bmpImage.setRGB(col, row, -16777216); // SOLID BLACK
            }
    }
    
    /**
     * Fills user-defined selvedge weave in allocated hooks
     */
    private void prepareSelvedgeArea(){
        if(objWeave!=null){
            try{
                WeaveAction objWeaveAction = new WeaveAction();
                objWeaveAction.extractWeaveContent(objWeave);
                int h= objWeave.getDesignMatrix().length;
                int w= objWeave.getDesignMatrix()[0].length;
                for(int row=inputSplitGraphImage.getHeight()-1; row>=0; row--)
                    for(int col=startSelvedgeHooks-1; col<startSelvedgeHooks-1+numSelvedgeHooks; col++){
                        if(objWeave.getDesignMatrix()[(h-1)-(((inputSplitGraphImage.getHeight()-1)-row)%h)][(col-(startSelvedgeHooks-1))%w]==1)
                            bmpImage.setRGB(col, row, -16777216); // SOLID BLACK
                        else
                            bmpImage.setRGB(col, row, -1); // SOLID WHITE
                    }
            }
            catch(Exception ex){
                ex.printStackTrace();
                lblStatus.setText(objDictionaryAction.getWord("ERRORSELVEDGEWEAVE"));
            }
        }
    }
    
    
    /**
     * Checks hooks allocation
     * @return whether hook allocation ok or there is some conflict
     */
    private boolean validateHookNumbers(){
        startDesignHooks = numLeftHooks;
        boolean inFingerSelectionArea = false, inRegulatorArea = false, inSelvedgeArea = false;
        for(int hookNumber=1; hookNumber<=numLeftHooks; hookNumber++){
            if(numFingerHooks>0)
                inFingerSelectionArea = (hookNumber>=startFingerHooks)&&(hookNumber<=startFingerHooks-1+numFingerHooks);
            if(numRegulatorHooks>0)
                inRegulatorArea = (hookNumber>=startRegulatorHooks)&&(hookNumber<=startRegulatorHooks-1+numRegulatorHooks);
            if(numSelvedgeHooks>0)
                inSelvedgeArea = (hookNumber>=startSelvedgeHooks)&&(hookNumber<=startSelvedgeHooks-1+numSelvedgeHooks);
            if((inFingerSelectionArea&&inRegulatorArea)
                    ||(inFingerSelectionArea&&inSelvedgeArea)
                    ||(inRegulatorArea&&inSelvedgeArea))
                return false;
        }
        return true;
    }
    
    /**
     * Refresh ImageView with updated image
     */
    private void refreshImage(){
        lblStatus.setText("");
        BufferedImage bmpToShow = new BufferedImage(bmpImage.getWidth()*blockPixels, bmpImage.getHeight()*blockPixels, BufferedImage.TYPE_INT_RGB);
        Graphics g = bmpToShow.getGraphics();
        g.drawImage(bmpImage, 0, 0, bmpToShow.getWidth(), bmpToShow.getHeight(), null);
        if(blockPixels!=1){
            VLines vlines = new VLines(g, bmpImage.getWidth(), bmpImage.getHeight());
            try{
                while(vlines.isAlive()){
                    Thread.sleep(100);
                }
            }
            catch(InterruptedException iEx){
                new Logging("SEVERE",getClass().getName(),"Drawing Lines: InterruptedException: Main thread interrupted",iEx);
            }
        }
        bmpIV.setImage(SwingFXUtils.toFXImage(bmpToShow, null));
    }
    
    /**
     * Initializes pickTypeMap[] with numbers 0 (base weft), 1(extra weft 1), 2 (extra weft 2)...
     */
    private void initPickTypeMap(){
        // from bottom most pick
        ArrayList<Integer> lstUnique= new ArrayList(); // store unique color in pick wefts except white
        int rgb=-1;
        for(int y=inputSplitGraphImage.getHeight()-1; y>=0; y--){
            for(int x=0; x<inputSplitGraphImage.getWidth(); x++){
                rgb = inputSplitGraphImage.getRGB(x, y);
                if(rgb!=-1){ //not SOLID WHITE
                    if(lstUnique.size()==0)
                        lstUnique.add(rgb);
                    else if(!lstUnique.contains(rgb))
                        lstUnique.add(rgb);
                    // assuming 1st pick is base weft i.e. rgb is SOLID WHITE & BLACK, lstUnique(0) should be -16777216 SOLID BLACK
                    pickTypeMap[y]=lstUnique.indexOf(rgb);
                }
            }
        }
        
    }
    
    /**
     * Inverts grayscale image (design portion) i.e. Black -> White and White -> Black
     * use convertToGrayscale() first, then invertDesignImage()
     * @see convertToGrayscale()
     */
    private void invertDesignImage(){
        for(int x=startDesignHooks; x<startDesignHooks+inputSplitGraphImage.getWidth(); x++)
            for(int y=0; y<bmpImage.getHeight(); y++){
                if(bmpImage.getRGB(x, y)==-1) // SOLID WHITE
                    bmpImage.setRGB(x, y, -16777216); // set to SOLID BLACK
                else if(bmpImage.getRGB(x, y)==-16777216) // SOLID BLACK
                    bmpImage.setRGB(x, y, -1); // set to SOLID WHITE
            }
    }
    
    /**
     * Inverts portion of BINARY bmpImage (B->W & W->B)
     * use convertToGrayscale() first, then invertPortionInBmpImage()
     * @see convertToGrayscale()
     * @param startXPos starting hook number in bmpImage
     * @param numXPos number of hooks of said portion in bmpImage
     */
    private void invertPortionInBmpImage(int startXPos, int numXPos){
        for(int x=startXPos; x<startXPos+numXPos; x++)
            for(int y=0; y<bmpImage.getHeight(); y++){
                if(bmpImage.getRGB(x, y)==java.awt.Color.WHITE.getRGB())
                    bmpImage.setRGB(x, y, java.awt.Color.BLACK.getRGB());
                else if(bmpImage.getRGB(x, y)==java.awt.Color.BLACK.getRGB())
                    bmpImage.setRGB(x, y, java.awt.Color.WHITE.getRGB());
            }
    }
    
    /**
     * Converts colored image to black/white. Also changes type to BINARY (1-bit depth) image
     */
    private void convertToGrayscale(){
        BufferedImage oneBitBmpImage = new BufferedImage(bmpImage.getWidth(), bmpImage.getHeight(), BufferedImage.TYPE_BYTE_BINARY);
        for(int x=0; x<bmpImage.getWidth(); x++)
            for(int y=0; y<bmpImage.getHeight(); y++)
                if(bmpImage.getRGB(x, y)!=-1) // not SOLID WHITE
                    //bmpImage.setRGB(x, y, -16777216);
                    oneBitBmpImage.setRGB(x, y, java.awt.Color.BLACK.getRGB());
                else
                    oneBitBmpImage.setRGB(x, y, java.awt.Color.WHITE.getRGB());
        bmpImage = oneBitBmpImage;
    }
    
    /**
     * Exports BMP image at given location
     */
    private void exportBMPImage(){
        FileChooser fileChoser=new FileChooser();
        fileChoser.setTitle(objDictionaryAction.getWord("EXPORTBMPTITLE"));
        FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("BMP (.bmp)", "*.bmp");
        fileChoser.getExtensionFilters().add(extFilterBMP);
        File file=fileChoser.showSaveDialog(cardViewStage);
        if(file==null)
            return;
        if (!file.getName().endsWith("bmp"))
            file = new File(file.getPath() +".bmp");
        try {
            ImageIO.write(bmpImage, "BMP", file);
            cardViewStage.close();
        } catch (IOException ex) {
            lblStatus.setText(objDictionaryAction.getWord("ERROREXPORTBMP"));
        }
    }
}

// drawing lines in separate thread
class VLines extends Thread
{
    Graphics objGraphics;
    int intWidth, intHeight;
    VLines(Graphics g, int intWidth, int intHeight)
    {
        super("my extending thread");
        this.objGraphics = g;
        this.intHeight = intHeight;
        this.intWidth = intWidth;
        start();
    }
    public void run()
    {
        objGraphics.setColor(java.awt.Color.BLACK);
        // horizontal lines
        for(int row=0; row<intHeight; row++)
            objGraphics.drawLine(0, row*blockPixels+blockPixels-1, intWidth*blockPixels-1, row*blockPixels+blockPixels-1);
        // vertical lines
        for(int col=0; col<intWidth; col++)
            objGraphics.drawLine(col*blockPixels+blockPixels-1, 0, col*blockPixels+blockPixels-1, intHeight*blockPixels-1);
    }
}
