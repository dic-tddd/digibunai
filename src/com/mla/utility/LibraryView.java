/*
 * Copyright (C) Digital India Corporation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.mla.utility;

import com.mla.artwork.Artwork;
import com.mla.artwork.ArtworkAction;
import com.mla.artwork.ArtworkView;
import com.mla.cloth.Cloth;
import com.mla.cloth.ClothAction;
import com.mla.cloth.ClothView;
import com.mla.colour.Colour;
import com.mla.colour.ColourAction;
import com.mla.dictionary.DictionaryAction;
import com.mla.fabric.Fabric;
import com.mla.fabric.FabricAction;
import com.mla.fabric.FabricView;
import com.mla.yarn.Yarn;
import com.mla.main.Configuration;
import com.mla.main.DbUtility;
import com.mla.main.EncryptZip;
import com.mla.main.IDGenerator;
import com.mla.main.Logging;
import com.mla.main.MessageView;
import com.mla.main.WindowView;
import com.mla.secure.Security;
import com.mla.user.UserAction;
import com.mla.user.UserLoginView;
import com.mla.weave.Weave;
import com.mla.weave.WeaveAction;
import com.mla.weave.WeaveView;
import com.mla.yarn.YarnAction;
import com.sun.media.jai.codec.ByteArraySeekableStream;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.SeekableStream;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import javax.media.jai.PlanarImage;
/**
 *
 * @Designing GUI window for fabric preferences
 * @author Amit Kumar Singh
 * 
 */
public class LibraryView extends Application {
    public static Stage libraryStage;
    BorderPane root;
    private Label lblStatus;
    ProgressBar progressB;
    ProgressIndicator progressI;
    
    private ClothAction objClothAction;
    private FabricAction objFabricAction;
    private ArtworkAction objArtworkAction;
    private WeaveAction objWeaveAction;
    private YarnAction objYarnAction;
    private Cloth objCloth;
    private Fabric objFabric;
    private Artwork objArtwork;
    private Weave objWeave;
    private Yarn objYarn;
    private Colour objColour;
    Configuration objConfiguration = null;
    DictionaryAction objDictionaryAction = null;
    
    GridPane GP_container;
    
    private Tab currentTab;
    private Tab dashboardTab;
    private Tab clothTab;
    private Tab fabricTab;
    private Tab artworkTab;
    private Tab weaveTab;
    private Tab yarnTab;
    private Tab colourTab;
    int currentPage;
    int perPage;
    int currentPageCloth, currentPageFabric, currentPageWeave, currentPageArtwork, currentPageColour, currentPageYarn;
    int perPageCloth, perPageFabric, perPageWeave, perPageArtwork, perPageColour, perPageYarn;
    
    public LibraryView(final Stage primaryStage) {}
    
    public LibraryView(Configuration objConfigurationCall) {
        objConfiguration = objConfigurationCall;
        objDictionaryAction = new DictionaryAction(objConfiguration);

        libraryStage = new Stage(); 
        root = new BorderPane();
        Scene scene = new Scene(root, objConfiguration.WIDTH, objConfiguration.HEIGHT, Color.WHITE);
        scene.getStylesheets().add(LibraryView.class.getResource(objConfiguration.getStrTemplate()+"/setting.css").toExternalForm());
        
        HBox footContainer = new HBox();
        progressB = new ProgressBar(0);
        progressB.setVisible(false);
        progressI = new ProgressIndicator(0);
        progressI.setVisible(false);
        lblStatus = new Label(objDictionaryAction.getWord("WELCOMETOCADTOOL"));
        lblStatus.setId("message");
        footContainer.getChildren().addAll(lblStatus,progressB,progressI);
        footContainer.setId("footContainer");
        root.setBottom(footContainer);
        
        currentPage=0;
        perPage=10;
        currentPageCloth=0;currentPageFabric=0;currentPageArtwork=0;currentPageWeave=0;currentPageYarn=0;currentPageColour=0;
        perPageCloth=10;perPageFabric=10;perPageArtwork=10;perPageWeave=10;perPageYarn=20;perPageColour=20;
        
        MenuBar menuBar = new MenuBar();
        menuBar.prefWidthProperty().bind(libraryStage.widthProperty());
        
        Menu homeMenu  = new Menu();
        HBox homeMenuHB = new HBox();
        homeMenuHB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/home.png"),new Label(objDictionaryAction.getWord("HOME")));
        homeMenu.setGraphic(homeMenuHB);
        homeMenu.setAccelerator(new KeyCodeCombination(KeyCode.M, KeyCombination.SHIFT_DOWN));
        homeMenuHB.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                homeMenuAction();  
                me.consume();
            }
        }); 
        Menu parentMenu  = new Menu();
        HBox parentMenuHB = new HBox();
        parentMenuHB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/settings.png"),new Label(objDictionaryAction.getWord("PARENT")));
        parentMenu.setGraphic(parentMenuHB);
        parentMenu.setAccelerator(new KeyCodeCombination(KeyCode.Z, KeyCombination.SHIFT_DOWN));
        parentMenuHB.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                parentMenuAction();                 
                me.consume();
            }
        }); 
        //Help Menu items
        Label supportMenuLabel = new Label(objDictionaryAction.getWord("SUPPORT"));
        supportMenuLabel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUPPORT")));
        Menu supportMenu = new Menu();
        supportMenu.setGraphic(supportMenuLabel);
        
        MenuItem helpMenuItem = new MenuItem(objDictionaryAction.getWord("HELP"));
        helpMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        helpMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.H, KeyCombination.SHIFT_DOWN));
        helpMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {       
                helpMenuAction();
            }
        });        
        MenuItem technicalMenuItem = new MenuItem(objDictionaryAction.getWord("TECHNICAL"));
        technicalMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/technical_info.png"));
        technicalMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.T, KeyCombination.SHIFT_DOWN));
        technicalMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {       
                technicalMenuAction();
            }
        });
        MenuItem aboutMenuItem = new MenuItem(objDictionaryAction.getWord("ABOUTUS"));
        aboutMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/about_software.png"));
        aboutMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.A, KeyCombination.SHIFT_DOWN));
        aboutMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                aboutMenuAction();
            }
        });
        MenuItem contactMenuItem = new MenuItem(objDictionaryAction.getWord("CONTACTUS"));
        contactMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/contact_us.png"));
        contactMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.SHIFT_DOWN));
        contactMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                contactMenuAction();
            }
        });
        MenuItem logoutMenuItem = new MenuItem(objDictionaryAction.getWord("LOGOUT"));
        logoutMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/logout.png"));
        logoutMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.L, KeyCombination.SHIFT_DOWN));
        logoutMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                logoutMenuAction();
            }
        });
        MenuItem exitMenuItem = new MenuItem(objDictionaryAction.getWord("EXIT"));
        exitMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/quit.png"));
        exitMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.ESCAPE));
        exitMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                exitMenuAction();
            }
        });       
        supportMenu.getItems().addAll(helpMenuItem, technicalMenuItem, aboutMenuItem, contactMenuItem, new SeparatorMenuItem(), logoutMenuItem);
        menuBar.getMenus().addAll(homeMenu, parentMenu, supportMenu);
        root.setTop(menuBar);
        
        TabPane tabPane = new TabPane();

        dashboardTab = new Tab();
        clothTab = new Tab();
        fabricTab = new Tab();
        artworkTab = new Tab();
        weaveTab = new Tab();
        yarnTab = new Tab();
        colourTab = new Tab();
        currentTab=clothTab;
        
        dashboardTab.setClosable(true);
        clothTab.setClosable(false);
        fabricTab.setClosable(false);
        artworkTab.setClosable(false);
        weaveTab.setClosable(false);
        yarnTab.setClosable(false);
        colourTab.setClosable(false);
        
        dashboardTab.setText(objDictionaryAction.getWord("DASHBOARD"));
        clothTab.setText(objDictionaryAction.getWord("CLOTHLIBRARY"));
        fabricTab.setText(objDictionaryAction.getWord("FABRICLIBRARY"));
        artworkTab.setText(objDictionaryAction.getWord("ARTWORKLIBRARY"));
        weaveTab.setText(objDictionaryAction.getWord("WEAVELIBRARY"));
        yarnTab.setText(objDictionaryAction.getWord("YARNLIBRARY"));
        colourTab.setText(objDictionaryAction.getWord("COLOURLIBRARY"));
        
        dashboardTab.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPDASHBOARD")));
        clothTab.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCLOTHLIBRARY")));
        fabricTab.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPFABRICLIBRARY")));
        artworkTab.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPARTWORKLIBRARY")));
        weaveTab.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPWEAVELIBRARY")));
        yarnTab.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPYARNLIBRARY")));
        colourTab.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCOLOURLIBRARY")));
        
        //dashboardTab.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_library.png"));
        //clothTab.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_library.png"));
        //fabricTab.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fabric_library.png"));
        //artworkTab.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/artwork_library.png"));
        //weaveTab.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/weave_library.png"));
        //yarnTab.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/yarn_library.png"));
        //colourTab.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/color_palette.png"));
        
        GridPane dashboardGP = new GridPane();
        GridPane clothGP = new GridPane();
        GridPane fabricGP = new GridPane();
        GridPane artworkGP = new GridPane();
        GridPane weaveGP = new GridPane();
        GridPane yarnGP = new GridPane();
        GridPane colourGP = new GridPane();
        
        dashboardGP.setId("container");
        clothGP.setId("container");
        fabricGP.setId("container");
        artworkGP.setId("container");
        weaveGP.setId("container");
        yarnGP.setId("container");
        colourGP.setId("container");
        
        //dashboardGP.setPrefSize(objConfiguration.WIDTH, objConfiguration.HEIGHT);
        //clothGP.setPrefSize(objConfiguration.WIDTH, objConfiguration.HEIGHT);
        //fabricGP.setPrefSize(objConfiguration.WIDTH, objConfiguration.HEIGHT);
        //artworkGP.setPrefSize(objConfiguration.WIDTH, objConfiguration.HEIGHT);
        //weaveGP.setPrefSize(objConfiguration.WIDTH, objConfiguration.HEIGHT);
        //yarnGP.setPrefSize(objConfiguration.WIDTH, objConfiguration.HEIGHT);
        //colourGP.setPrefSize(objConfiguration.WIDTH, objConfiguration.HEIGHT);
        
        //dashboardGP.setAlignment(Pos.TOP_CENTER);
        //clothGP.setAlignment(Pos.TOP_CENTER);
        //fabricGP.setAlignment(Pos.TOP_CENTER);
        //artworkGP.setAlignment(Pos.TOP_CENTER);
        //weaveGP.setAlignment(Pos.TOP_CENTER);
        //yarnGP.setAlignment(Pos.TOP_CENTER);
        //colourGP.setAlignment(Pos.TOP_CENTER);
        
        dashboardTab.setContent(dashboardGP);
        clothTab.setContent(clothGP);
        fabricTab.setContent(fabricGP);
        artworkTab.setContent(artworkGP);
        weaveTab.setContent(weaveGP);
        yarnTab.setContent(yarnGP);
        colourTab.setContent(colourGP);
        
        tabPane.getTabs().add(dashboardTab);
        tabPane.getTabs().add(clothTab);
        tabPane.getTabs().add(fabricTab);
        tabPane.getTabs().add(artworkTab);
        tabPane.getTabs().add(weaveTab);
        tabPane.getTabs().add(yarnTab);
        tabPane.getTabs().add(colourTab);
        
        final Button btnPrev=new Button("<");
        btnPrev.setDisable(true);
        final Button btnNext=new Button(">");
        btnNext.setDisable(false);
        
        tabPane.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {
            @Override
            public void changed(ObservableValue<? extends Tab> observable, Tab oldTab, Tab newTab) {
                currentTab=newTab;
                btnPrev.setDisable(true);
                btnNext.setDisable(false);
                if(newTab == colourTab) {
                    objColour.setStrLimit((currentPageColour*perPageColour)+","+perPageColour);
                    populateLibraryColour();
                }else if(newTab == yarnTab) {
                    objYarn.setStrLimit((currentPageYarn*perPageYarn)+","+perPageYarn);
                    populateLibraryYarn();
                }else if(newTab == weaveTab) {
                    objWeave.setStrLimit((currentPageWeave*perPageWeave)+","+perPageWeave);
                    populateLibraryWeave();
                }else if(newTab == artworkTab) {
                    objArtwork.setStrLimit((currentPageArtwork*perPageArtwork)+","+perPageArtwork);
                    populateLibraryArtwork();
                }else if(newTab == fabricTab) {
                    objFabric.setStrLimit((currentPageFabric*perPageFabric)+","+perPageFabric);
                    populateLibraryFabric();
                }else if(newTab == clothTab) {
                    objCloth.setStrLimit((currentPageCloth*perPageCloth)+","+perPageCloth);
                    populateLibraryCloth();
                } else{
                    populateLibraryDashboard();
                }
            }
        });
        
        
        //===== Cloth =====//
        Label cloth = new Label(objDictionaryAction.getWord("CLOTHLIBRARY"));
        cloth.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/cloth_library.png"));
        cloth.setId("caption");
        GridPane.setConstraints(cloth, 0, 0);
        GridPane.setColumnSpan(cloth, 8);
        clothGP.getChildren().add(cloth);
       
        objCloth = new Cloth();
        objCloth.setObjConfiguration(objConfiguration);
        objCloth.setStrCondition("");
        objCloth.setStrSearchBy("All");
        objCloth.setStrSearchAccess("All User Data");
        objCloth.setStrOrderBy("Name");
        objCloth.setStrDirection("Ascending");
        //objCloth.setStrLimit("");
        
        clothGP.add(new Text(objDictionaryAction.getWord("CLOTH")+" "+objDictionaryAction.getWord("NAME")),0,1);
        clothGP.add(new Text(objDictionaryAction.getWord("CLOTH")+" "+objDictionaryAction.getWord("SEARCHBY")),2,1);
        clothGP.add(new Text(objDictionaryAction.getWord("CLOTH")+" "+objDictionaryAction.getWord("SORTBY")),4,1);
        clothGP.add(new Text(objDictionaryAction.getWord("CLOTH")+" "+objDictionaryAction.getWord("SORTDIRCTION")),6,1);
        
        final TextField clothName = new TextField();
        clothName.setPromptText(objDictionaryAction.getWord("PROMPTNAME"));
        clothName.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent ke) {
                objCloth.setStrLimit(null);
                objCloth.setStrCondition(clothName.getText());
                populateLibraryCloth(); 
            }
        });
        ComboBox clothSearch = new ComboBox();
        clothSearch.getItems().addAll(
            "Saree",
            "Dress Material",
            "Stole",
            "Shawl",
            "Gaisar Brocade",
            "All"
        );
        clothSearch.setPromptText(objDictionaryAction.getWord("SEARCHBY"));
        clothSearch.setEditable(false); 
        clothSearch.setValue("All"); 
        clothSearch.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String oldValue, String newValue) {
                objCloth.setStrSearchBy(newValue);
                populateLibraryCloth();
            }    
        });
        ComboBox clothSort = new ComboBox();
        clothSort.getItems().addAll(
            "Name",
            "Date"
        );
        clothSort.setPromptText(objDictionaryAction.getWord("SORTBY"));
        clothSort.setEditable(false); 
        clothSort.setValue("Name"); 
        clothSort.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String oldValue, String newValue) {
                objCloth.setStrOrderBy(newValue);
                populateLibraryCloth();
            }    
        });
        ComboBox clothDirction = new ComboBox();
        clothDirction.getItems().addAll(
            "Ascending",
            "Descending"
        );
        clothDirction.setPromptText(objDictionaryAction.getWord("SORTDIRCTION"));
        clothDirction.setEditable(false); 
        clothDirction.setValue("Ascending"); 
        clothDirction.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String oldValue, String newValue) {
                objCloth.setStrDirection(newValue);
                populateLibraryCloth();
            }    
        });
        
        clothGP.add(clothName,1,1);
        clothGP.add(clothSearch,3,1);
        clothGP.add(clothSort,5,1);
        clothGP.add(clothDirction,7,1);
        
        Separator clothSH = new Separator();
        clothSH.setValignment(VPos.CENTER);
        GridPane.setConstraints(clothSH, 0, 2);
        GridPane.setColumnSpan(clothSH, 8);
        clothGP.getChildren().add(clothSH);
        
        //===== Fabric =====//
        Label fabric = new Label(objDictionaryAction.getWord("FABRICLIBRARY"));
        fabric.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fabric_library.png"));
        fabric.setId("caption");
        GridPane.setConstraints(fabric, 0, 0);
        GridPane.setColumnSpan(fabric, 8);
        fabricGP.getChildren().add(fabric);
       
        objFabric = new Fabric();
        objFabric.setObjConfiguration(objConfiguration);
        objFabric.setStrCondition("");
        objFabric.setStrSearchBy("");
        objFabric.setStrSearchAccess("All User Data");
        objFabric.setStrOrderBy("Name");
        objFabric.setStrDirection("Ascending");
        //objFabric.setStrLimit("");
        
        fabricGP.add(new Text(objDictionaryAction.getWord("FABRIC")+" "+objDictionaryAction.getWord("NAME")),0,1);
        fabricGP.add(new Text(objDictionaryAction.getWord("FABRIC")+" "+objDictionaryAction.getWord("SEARCHBY")),2,1);
        fabricGP.add(new Text(objDictionaryAction.getWord("FABRIC")+" "+objDictionaryAction.getWord("SORTBY")),4,1);
        fabricGP.add(new Text(objDictionaryAction.getWord("FABRIC")+" "+objDictionaryAction.getWord("SORTDIRCTION")),6,1);
        
        final TextField fabricName = new TextField();
        fabricName.setPromptText(objDictionaryAction.getWord("PROMPTNAME"));
        fabricName.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent ke) {
                objFabric.setStrLimit(null);
                objFabric.setStrCondition(fabricName.getText());
                populateLibraryFabric(); 
            }
        });
        ComboBox fabricSearch = new ComboBox();
        fabricSearch.getItems().addAll(
            "All Cloth Type",
            "Body",
            "Pallu",
            "Border",
            "Cross Border",
            "Blouse",
            "Skirt",
            "Konia"
        );
        fabricSearch.setPromptText(objDictionaryAction.getWord("SEARCHBY"));
        fabricSearch.setEditable(false); 
        fabricSearch.setValue("All Cloth Type"); 
        fabricSearch.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String oldValue, String newValue) {
                objFabric.setStrSearchBy(newValue);
                populateLibraryFabric();
            }    
        });
        ComboBox fabricSort = new ComboBox();
        fabricSort.getItems().addAll(
            "Name",
            "Date"
        );
        fabricSort.setPromptText(objDictionaryAction.getWord("SORTBY"));
        fabricSort.setEditable(false); 
        fabricSort.setValue("Name"); 
        fabricSort.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String oldValue, String newValue) {
                objFabric.setStrOrderBy(newValue);
                populateLibraryFabric();
            }    
        });
        ComboBox fabricDirction = new ComboBox();
        fabricDirction.getItems().addAll(
            "Ascending",
            "Descending"
        );
        fabricDirction.setPromptText(objDictionaryAction.getWord("SORTDIRCTION"));
        fabricDirction.setEditable(false); 
        fabricDirction.setValue("Ascending"); 
        fabricDirction.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String oldValue, String newValue) {
                objFabric.setStrDirection(newValue);
                populateLibraryFabric();
            }    
        });
        
        fabricGP.add(fabricName,1,1);
        fabricGP.add(fabricSearch,3,1);
        fabricGP.add(fabricSort,5,1);
        fabricGP.add(fabricDirction,7,1);
        
        Separator fabricSH = new Separator();
        fabricSH.setValignment(VPos.CENTER);
        GridPane.setConstraints(fabricSH, 0, 2);
        GridPane.setColumnSpan(fabricSH, 8);
        fabricGP.getChildren().add(fabricSH);
    
        //===== Artwork =====//
        Label artwork = new Label(objDictionaryAction.getWord("ARTWORKLIBRARY"));
        artwork.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/artwork_library.png"));
        artwork.setId("caption");
        GridPane.setConstraints(artwork, 0, 0);
        GridPane.setColumnSpan(artwork, 8);
        artworkGP.getChildren().add(artwork);
       
        objArtwork = new Artwork();
        objArtwork.setObjConfiguration(objConfiguration);
        objArtwork.setStrCondition("");
        objArtwork.setStrSearchBy("");
        objArtwork.setStrSearchAccess("All User Data");
        objArtwork.setStrOrderBy("Name");
        objArtwork.setStrDirection("Ascending");
        //objArtwork.setStrLimit("");
        
        artworkGP.add(new Text(objDictionaryAction.getWord("ARTWORK")+" "+objDictionaryAction.getWord("NAME")),0,1);
        artworkGP.add(new Text(objDictionaryAction.getWord("ARTWORK")+" "+objDictionaryAction.getWord("SEARCHBY")),2,1);
        artworkGP.add(new Text(objDictionaryAction.getWord("ARTWORK")+" "+objDictionaryAction.getWord("SORTBY")),4,1);
        artworkGP.add(new Text(objDictionaryAction.getWord("ARTWORK")+" "+objDictionaryAction.getWord("SORTDIRCTION")),6,1);
        
        final TextField artworkName = new TextField();
        artworkName.setPromptText(objDictionaryAction.getWord("PROMPTNAME"));
        artworkName.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent ke) {
                objArtwork.setStrLimit(null);
                objArtwork.setStrCondition(artworkName.getText());
                populateLibraryArtwork(); 
            }
        });
        ComboBox artworkSearch = new ComboBox();
        artworkSearch.getItems().addAll(
            "All Type"
        );
        artworkSearch.setPromptText(objDictionaryAction.getWord("SEARCHBY"));
        artworkSearch.setEditable(false); 
        artworkSearch.setValue("All Type"); 
        artworkSearch.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String oldValue, String newValue) {
                objArtwork.setStrSearchBy(newValue);
                populateLibraryArtwork();
            }    
        });
        ComboBox artworkSort = new ComboBox();
        artworkSort.getItems().addAll(
            "Name",
            "Date"
        );
        artworkSort.setPromptText(objDictionaryAction.getWord("SORTBY"));
        artworkSort.setEditable(false); 
        artworkSort.setValue("Name"); 
        artworkSort.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String oldValue, String newValue) {
                objArtwork.setStrOrderBy(newValue);
                populateLibraryArtwork();
            }    
        });
        ComboBox artworkDirction = new ComboBox();
        artworkDirction.getItems().addAll(
            "Ascending",
            "Descending"
        );
        artworkDirction.setPromptText(objDictionaryAction.getWord("SORTDIRCTION"));
        artworkDirction.setEditable(false); 
        artworkDirction.setValue("Ascending"); 
        artworkDirction.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String oldValue, String newValue) {
                objArtwork.setStrDirection(newValue);
                populateLibraryArtwork();
            }    
        });
        
        artworkGP.add(artworkName,1,1);
        artworkGP.add(artworkSearch,3,1);
        artworkGP.add(artworkSort,5,1);
        artworkGP.add(artworkDirction,7,1);
        
        Separator artworkSH = new Separator();
        artworkSH.setValignment(VPos.CENTER);
        GridPane.setConstraints(artworkSH, 0, 2);
        GridPane.setColumnSpan(artworkSH, 8);
        artworkGP.getChildren().add(artworkSH);
    
        //===== Weave =====//
        Label weave = new Label(objDictionaryAction.getWord("WEAVELIBRARY"));
        weave.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/weave_library.png"));
        weave.setId("caption");
        GridPane.setConstraints(weave, 0, 0);
        GridPane.setColumnSpan(weave, 8);
        weaveGP.getChildren().add(weave);
       
        objWeave = new Weave();
        objWeave.setObjConfiguration(objConfiguration);
        objWeave.setStrCondition("");
        objWeave.setStrSearchBy("All");
        objWeave.setStrSearchAccess("All User Data");
        objWeave.setStrOrderBy("Name");
        objWeave.setStrDirection("Ascending");        
        //objWeave.setStrLimit("");
        
        weaveGP.add(new Text(objDictionaryAction.getWord("WEAVE")+" "+objDictionaryAction.getWord("NAME")),0,1);
        weaveGP.add(new Text(objDictionaryAction.getWord("WEAVE")+" "+objDictionaryAction.getWord("SEARCHBY")),2,1);
        weaveGP.add(new Text(objDictionaryAction.getWord("WEAVE")+" "+objDictionaryAction.getWord("SORTBY")),4,1);
        weaveGP.add(new Text(objDictionaryAction.getWord("WEAVE")+" "+objDictionaryAction.getWord("SORTDIRCTION")),6,1);
        
        final TextField weaveName = new TextField();
        weaveName.setPromptText(objDictionaryAction.getWord("PROMPTNAME"));
        weaveName.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent ke) {
                objWeave.setStrLimit(null);
                objWeave.setStrCondition(weaveName.getText());
                populateLibraryWeave(); 
            }
        });
        ComboBox weaveSearch = new ComboBox();
        weaveSearch.getItems().addAll(
            "All",
            "Plain",
            "Twill",
            "Satin",
            "Basket",
            "Sateen"
        );
        weaveSearch.setPromptText(objDictionaryAction.getWord("SEARCHBY"));
        weaveSearch.setEditable(false); 
        weaveSearch.setValue("All"); 
        weaveSearch.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String oldValue, String newValue) {
                objWeave.setStrSearchBy(newValue);
                populateLibraryWeave();
            }    
        });
        ComboBox weaveSort = new ComboBox();
        weaveSort.getItems().addAll(
            "Name",
            "Date",
            "Shaft",
            "Treadles",
            "Float X",
            "Float Y"
        );
        weaveSort.setPromptText(objDictionaryAction.getWord("SORTBY"));
        weaveSort.setEditable(false); 
        weaveSort.setValue("Name"); 
        weaveSort.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String oldValue, String newValue) {
                objWeave.setStrOrderBy(newValue);
                populateLibraryWeave();
            }    
        });
        ComboBox weaveDirction = new ComboBox();
        weaveDirction.getItems().addAll(
            "Ascending",
            "Descending"
        );
        weaveDirction.setPromptText(objDictionaryAction.getWord("SORTDIRCTION"));
        weaveDirction.setEditable(false); 
        weaveDirction.setValue("Ascending"); 
        weaveDirction.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String oldValue, String newValue) {
                objWeave.setStrDirection(newValue);
                populateLibraryWeave();
            }    
        });
        
        weaveGP.add(weaveName,1,1);
        weaveGP.add(weaveSearch,3,1);
        weaveGP.add(weaveSort,5,1);
        weaveGP.add(weaveDirction,7,1);
        
        Separator weaveSH = new Separator();
        weaveSH.setValignment(VPos.CENTER);
        GridPane.setConstraints(weaveSH, 0, 2);
        GridPane.setColumnSpan(weaveSH, 8);
        weaveGP.getChildren().add(weaveSH);
    
        //===== Yarn =====//
        Label yarn = new Label(objDictionaryAction.getWord("YARNLIBRARY"));
        yarn.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/yarn_library.png"));
        yarn.setId("caption");
        GridPane.setConstraints(yarn, 0, 0);
        GridPane.setColumnSpan(yarn, 8);
        yarnGP.getChildren().add(yarn);
       
        objYarn = objYarn = new Yarn(null, "Warp", objConfiguration.getStrWarpName(), "#"+objConfiguration.getStrWarpColor(), objConfiguration.getIntWarpRepeat(), "A", objConfiguration.getIntWarpCount(), objConfiguration.getStrWarpUnit(), objConfiguration.getIntWarpPly(), objConfiguration.getIntWarpFactor(), objConfiguration.getDblWarpDiameter(), objConfiguration.getIntWarpTwist(), objConfiguration.getStrWarpSence(), objConfiguration.getIntWarpHairness(), objConfiguration.getIntWarpDistribution(), objFabric.getObjConfiguration().getDblWarpPrice(), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);
        objYarn.setObjConfiguration(objConfiguration);
        objYarn.setStrCondition("");
        objYarn.setStrSearchBy("All");
        objYarn.setStrOrderBy("Name");
        objYarn.setStrDirection("Ascending");        
        //objYarn.setStrLimit("");
        
        yarnGP.add(new Text(objDictionaryAction.getWord("YARN")+" "+objDictionaryAction.getWord("NAME")),0,1);
        yarnGP.add(new Text(objDictionaryAction.getWord("YARN")+" "+objDictionaryAction.getWord("SEARCHBY")),2,1);
        yarnGP.add(new Text(objDictionaryAction.getWord("YARN")+" "+objDictionaryAction.getWord("SORTBY")),4,1);
        yarnGP.add(new Text(objDictionaryAction.getWord("YARN")+" "+objDictionaryAction.getWord("SORTDIRCTION")),6,1);
        
        final TextField yarnName = new TextField();
        yarnName.setPromptText(objDictionaryAction.getWord("PROMPTNAME"));
        yarnName.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent ke) {
                objYarn.setStrLimit(null);
                objYarn.setStrCondition(yarnName.getText());
                populateLibraryYarn(); 
            }
        });
        ComboBox yarnSearch = new ComboBox();
        yarnSearch.getItems().addAll(
            "All",
            "Cotton",
            "Silk",
            "Wool",
            "Jute",
            "Linen",
            "Flex",
            "Hemp"
        );  
        yarnSearch.setPromptText(objDictionaryAction.getWord("SEARCHBY"));
        yarnSearch.setEditable(false); 
        yarnSearch.setValue("All"); 
        yarnSearch.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String oldValue, String newValue) {
                objYarn.setStrSearchBy(newValue);
                populateLibraryYarn();
            }    
        });
        ComboBox yarnSort = new ComboBox();
        yarnSort.getItems().addAll(
            "Name",
            "Date",
            "Type",
            "Ply",
            "Diameter",
            "Price"
        );
        yarnSort.setPromptText(objDictionaryAction.getWord("SORTBY"));
        yarnSort.setEditable(false); 
        yarnSort.setValue("Name"); 
        yarnSort.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String oldValue, String newValue) {
                objYarn.setStrOrderBy(newValue);
                populateLibraryYarn();
            }    
        });
        ComboBox yarnDirction = new ComboBox();
        yarnDirction.getItems().addAll(
            "Ascending",
            "Descending"
        );
        yarnDirction.setPromptText(objDictionaryAction.getWord("SORTDIRCTION"));
        yarnDirction.setEditable(false); 
        yarnDirction.setValue("Ascending"); 
        yarnDirction.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String oldValue, String newValue) {
                objYarn.setStrDirection(newValue);
                populateLibraryYarn();
            }    
        });
        
        yarnGP.add(yarnName,1,1);
        yarnGP.add(yarnSearch,3,1);
        yarnGP.add(yarnSort,5,1);
        yarnGP.add(yarnDirction,7,1);
        
        Separator yarnSH = new Separator();
        yarnSH.setValignment(VPos.CENTER);
        GridPane.setConstraints(yarnSH, 0, 2);
        GridPane.setColumnSpan(yarnSH, 8);
        yarnGP.getChildren().add(yarnSH);
    
        //===== Colour =====//
        Label colour = new Label(objDictionaryAction.getWord("COLOURLIBRARY"));
        colour.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/color_library.png"));
        colour.setId("caption");
        GridPane.setConstraints(colour, 0, 0);
        GridPane.setColumnSpan(colour, 8);
        colourGP.getChildren().add(colour);
       
        objColour = new Colour();
        objColour.setObjConfiguration(objConfiguration);
        objColour.setStrCondition("");
        objColour.setStrOrderBy("Type");
        objColour.setStrSearchBy("");
        objColour.setStrDirection("Ascending");
        //objColour.setStrLimit("");
        
        colourGP.add(new Text(objDictionaryAction.getWord("COLOUR")+" "+objDictionaryAction.getWord("NAME")),0,1);
        colourGP.add(new Text(objDictionaryAction.getWord("COLOUR")+" "+objDictionaryAction.getWord("SEARCHBY")),2,1);
        colourGP.add(new Text(objDictionaryAction.getWord("COLOUR")+" "+objDictionaryAction.getWord("SORTBY")),4,1);
        colourGP.add(new Text(objDictionaryAction.getWord("COLOUR")+" "+objDictionaryAction.getWord("SORTDIRCTION")),6,1);
        
        final TextField colourName = new TextField();
        colourName.setPromptText(objDictionaryAction.getWord("PROMPTNAME"));
        colourName.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent ke) {
                objColour.setStrLimit(null);
                objColour.setStrCondition(colourName.getText());
                populateLibraryColour(); 
            }
        });
        ComboBox colourSearch = new ComboBox();
        /*colourSearch.getItems().addAll(
            "All",
            "Web Color",
            "Pantone Color"
        );
        colourSearch.getItems().addAll("All",getColourType());
        */
        //added by amit
        colourSearch.getItems().add("All");
        for (String object: getColourType()) {
            //System.out.println(object);
            colourSearch.getItems().add(object);
        }
        //added by amit
        colourSearch.setPromptText(objDictionaryAction.getWord("SEARCHBY"));
        colourSearch.setEditable(false); 
        colourSearch.setValue("All"); 
        if(colourSearch.getItems().size()>0){
            colourSearch.setValue(colourSearch.getItems().get(0).toString());
        }
        colourSearch.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String oldValue, String newValue) {
                objColour.setStrSearchBy(newValue);
                populateLibraryColour();
            }    
        });
        ComboBox colourSort = new ComboBox();
        colourSort.getItems().addAll(
            "Name",
            "Date",
            "Type",
            "Code",
            "Hex Code",
            "Red",
            "Green",
            "Blue"
        );            
        colourSort.setPromptText(objDictionaryAction.getWord("SORTBY"));
        colourSort.setEditable(false); 
        colourSort.setValue("Name"); 
        colourSort.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String oldValue, String newValue) {
                objColour.setStrOrderBy(newValue);
                populateLibraryColour();
            }    
        });
        ComboBox colourDirction = new ComboBox();
        colourDirction.getItems().addAll(
            "Ascending",
            "Descending"
        );
        colourDirction.setPromptText(objDictionaryAction.getWord("SORTDIRCTION"));
        colourDirction.setEditable(false); 
        colourDirction.setValue("Ascending"); 
        colourDirction.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String oldValue, String newValue) {
                objColour.setStrDirection(newValue);
                populateLibraryColour();
            }    
        });
        
        colourGP.add(colourName,1,1);
        colourGP.add(colourSearch,3,1);
        colourGP.add(colourSort,5,1);
        colourGP.add(colourDirction,7,1);
        
        Separator colourSH = new Separator();
        colourSH.setValignment(VPos.CENTER);
        GridPane.setConstraints(colourSH, 0, 2);
        GridPane.setColumnSpan(colourSH, 8);
        colourGP.getChildren().add(colourSH);
        
        //--------------------------
        ScrollPane container = new ScrollPane();
        container.setPrefSize(objConfiguration.WIDTH/1.1,objConfiguration.HEIGHT/2);
        
        GP_container = new GridPane();         
        GP_container.setAlignment(Pos.TOP_CENTER);
        GP_container.setHgap(20);
        GP_container.setVgap(20);
        GP_container.setPadding(new Insets(1, 1, 1, 1));
        container.setContent(GP_container);
        /*
        clothGP.add(GP_container,0,3,8,1);
        fabricGP.add(GP_container,0,3,8,1);
        artworkGP.add(GP_container,0,3,8,1);
        weaveGP.add(GP_container,0,3,8,1);
        yarnGP.add(GP_container,0,3,8,1);
        colourGP.add(GP_container,0,3,8,1);
        */
        populateLibraryDashboard();
        
        GridPane bodyContainer = new GridPane();
        bodyContainer.setPrefSize(objConfiguration.WIDTH, objConfiguration.HEIGHT);
        bodyContainer.setId("container");
        
        Label caption = new Label(objDictionaryAction.getWord("LIBRARY")+" "+objDictionaryAction.getWord("UTILITY"));
        caption.setId("caption");
        
        bodyContainer.add(caption, 0, 0, 3, 1);
        
        bodyContainer.add(tabPane, 0, 1, 3, 1);
        bodyContainer.add(container, 0, 2, 3, 1);
        
        Button btnExportImport = new Button(objDictionaryAction.getWord("EXPORT")+" "+objDictionaryAction.getWord("IMPORT"));
        btnExportImport.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/application_integration.png"));
        
        bodyContainer.add(btnExportImport, 0, 3, 1, 1);
        bodyContainer.add(btnPrev, 1, 3, 1, 1);
        bodyContainer.add(btnNext, 2, 3, 1, 1);

        btnPrev.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                getTabPagingContext();
                if(currentPage!=0){
                    currentPage--;
                    populateTabPagingContext();
                    if(btnNext.isDisabled())
                        btnNext.setDisable(false);
                }
                else{
                    btnPrev.setDisable(true);
                    btnNext.setDisable(false);
                }
                setTabPagingContext();
            }
        });
        
        btnNext.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if(!btnNext.isDisabled()){
                    getTabPagingContext();
                    currentPage++;
                    populateTabPagingContext();
                    if(btnPrev.isDisabled())
                        btnPrev.setDisable(false);
                    if(GP_container.getChildren().size()<=2){
                        currentPage--;
                        populateTabPagingContext();
                        btnNext.setDisable(true);
                    }
                    setTabPagingContext();
                }
            }
        });
        
        btnExportImport.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                final Stage dialogStage = new Stage();
                dialogStage.initStyle(StageStyle.UTILITY);
                dialogStage.initModality(Modality.APPLICATION_MODAL);
                dialogStage.setResizable(false);
                dialogStage.setIconified(false);
                dialogStage.setFullScreen(false);
                dialogStage.setTitle(objDictionaryAction.getWord("ALERT"));
                BorderPane root = new BorderPane();
                Scene scene = new Scene(root, 300, 100, Color.WHITE);
                scene.getStylesheets().add(LibraryView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
                final GridPane popup=new GridPane();
                popup.setId("popup");
                popup.setHgap(5);
                popup.setVgap(5);
                popup.setPadding(new Insets(25, 25, 25, 25));
                popup.add(new ImageView("/media/alert_warning.png"), 0, 0); 
                Label lblAlert = new Label(objDictionaryAction.getWord("ALERTCLOSE"));
                lblAlert.setStyle("-fx-wrap-text:true;");
                lblAlert.setPrefWidth(250);
                popup.add(lblAlert, 1, 0);
                Button btnYes = new Button(objDictionaryAction.getWord("YES"));
                btnYes.setPrefWidth(50);
                btnYes.setId("btnYes");
                btnYes.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent t) {
                        dialogStage.close();
                        libraryStage.close();
                        System.gc();
                        ExportImportView objExportImportView = new ExportImportView(objConfiguration);
                    }
                });
                popup.add(btnYes, 0, 1);
                Button btnNo = new Button(objDictionaryAction.getWord("NO"));
                btnNo.setPrefWidth(50);
                btnNo.setId("btnNo");
                btnNo.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent t) {
                        dialogStage.close();
                        System.gc();
                    }
                });
                popup.add(btnNo, 1, 1);
                root.setCenter(popup);
                dialogStage.setScene(scene);
                dialogStage.showAndWait();  
                t.consume();
            }
        });
        /*
        final Separator sepHor = new Separator();
        sepHor.setValignment(VPos.CENTER);
        GridPane.setConstraints(sepHor, 0, 2);
        GridPane.setColumnSpan(sepHor, 1);
        bodyContainer.getChildren().add(sepHor);
        */
        root.setCenter(bodyContainer);
        
        libraryStage.getIcons().add(new Image("/media/icon.png"));
        libraryStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWLIBRARY")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
        //libraryStage.setIconified(true);
        libraryStage.setResizable(false);
        libraryStage.setScene(scene);
        libraryStage.setX(0);
        libraryStage.setY(0);
        libraryStage.show();
        libraryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                parentMenuAction();
                we.consume();
            }
        });
        final KeyCodeCombination homeKCC = new KeyCodeCombination(KeyCode.M, KeyCombination.SHIFT_DOWN); // Home Menu]
        final KeyCodeCombination parentKCC = new KeyCodeCombination(KeyCode.Z, KeyCombination.SHIFT_DOWN); // Home Menu]
        scene.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                if(homeKCC.match(t)){
                    homeMenuAction();
                } else if(parentKCC.match(t)){
                    parentMenuAction();
                }
            }
        });
    }
 
    /*Dashboard */
    private void populateLibraryDashboard(){
        try{
            GP_container.getChildren().clear();
            
            objClothAction = new ClothAction();
            Map<String,Number> clothMap = objClothAction.countClothAccess();
            //Preparing ObservbleList object         
            ObservableList<PieChart.Data> pieChartDataCloth = FXCollections.observableArrayList();
            for (Map.Entry<String,Number> entry : clothMap.entrySet())  {
                //System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
                pieChartDataCloth.add(new PieChart.Data(entry.getKey(), (int)entry.getValue()));
            }
            //Creating a Pie chart 
            PieChart pieChartCloth = new PieChart(pieChartDataCloth);
            //pieChart.setData(pieChartData);
            //Setting the title of the Pie chart 
            pieChartCloth.setTitle("Cloth Access");
            //setting the direction to arrange the data 
            pieChartCloth.setClockwise(true);
            //Setting the start angle of the pie chart 
            pieChartCloth.setStartAngle(180); 
            //Setting the length of the label line 
            pieChartCloth.setLabelLineLength(10);
            //Setting position of the labels of the pie chart
            pieChartCloth.setLegendSide(Side.TOP);
            //Setting the labels of the pie chart visible  
            pieChartCloth.setLabelsVisible(true);
            GP_container.add(pieChartCloth, 0, 0);
            
            objFabricAction = new FabricAction();
            Map<String,Number> fabricMap = objFabricAction.countFabricAccess();
            //Preparing ObservbleList object         
            ObservableList<PieChart.Data> pieChartDataFabric = FXCollections.observableArrayList();
            for (Map.Entry<String,Number> entry : fabricMap.entrySet())  {
                //System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
                pieChartDataFabric.add(new PieChart.Data(entry.getKey(), (int)entry.getValue()));
            }
            //Creating a Pie chart 
            PieChart pieChartFabric = new PieChart(pieChartDataFabric);
            pieChartFabric.setTitle("Fabric Access");
            pieChartFabric.setClockwise(false);  
            pieChartFabric.setStartAngle(180);            
            pieChartFabric.setLabelLineLength(10);
            pieChartFabric.setLegendSide(Side.TOP);
            pieChartFabric.setLabelsVisible(true);
            GP_container.add(pieChartFabric, 1, 0);
            
            objArtworkAction = new ArtworkAction();
            Map<String,Number> artworkMap = objArtworkAction.countArtworkAccess();
            //Preparing ObservbleList object         
            ObservableList<PieChart.Data> pieChartDataArtwork = FXCollections.observableArrayList();
            for (Map.Entry<String,Number> entry : artworkMap.entrySet())  {
                //System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
                pieChartDataArtwork.add(new PieChart.Data(entry.getKey(), (int)entry.getValue()));
            }
            //Creating a Pie chart 
            PieChart pieChartArtwork = new PieChart(pieChartDataArtwork);
            pieChartArtwork.setTitle("Artwork Access");
            pieChartArtwork.setLabelLineLength(10); 
            pieChartArtwork.setLegendSide(Side.LEFT);
            pieChartArtwork.setLabelsVisible(true);             
            GP_container.add(pieChartArtwork, 0, 1);
            
            objWeaveAction = new WeaveAction();
            Map<String,Number> weaveMap = objWeaveAction.countWeaveAccess();
            //Preparing ObservbleList object         
            ObservableList<PieChart.Data> pieChartDataWeave = FXCollections.observableArrayList();
            for (Map.Entry<String,Number> entry : weaveMap.entrySet())  {
                //System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
                pieChartDataWeave.add(new PieChart.Data(entry.getKey(), (int)entry.getValue()));
            }
            //Creating a Pie chart 
            PieChart pieChartWeave = new PieChart(pieChartDataWeave);
            pieChartWeave.setTitle("Weave Access");
            pieChartWeave.setLabelLineLength(10);
            pieChartWeave.setLegendSide(Side.RIGHT);
            pieChartWeave.setLabelsVisible(true);
            GP_container.add(pieChartWeave, 1, 1);
            
            objYarnAction = new YarnAction();
            Map<String,Number> yarnMap = objYarnAction.countYarnAccess();
            //Preparing ObservbleList object         
            ObservableList<PieChart.Data> pieChartDataYarn = FXCollections.observableArrayList();
            for (Map.Entry<String,Number> entry : yarnMap.entrySet())  {
                //System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
                pieChartDataYarn.add(new PieChart.Data(entry.getKey(), (int)entry.getValue()));
            }
            //Creating a Pie chart 
            PieChart pieChartYarn = new PieChart(pieChartDataYarn);
            pieChartYarn.setTitle("Yarn Access");
            pieChartYarn.setClockwise(false);
            pieChartYarn.setStartAngle(180); 
            pieChartYarn.setLabelLineLength(10); 
            pieChartYarn.setLabelsVisible(true);
            GP_container.add(pieChartYarn, 0, 2);
            
            ColourAction objColourAction = new ColourAction();
            Map<String,Number> colourMap = objColourAction.countColourAccess();
            //Preparing ObservbleList object         
            ObservableList<PieChart.Data> pieChartDataColour = FXCollections.observableArrayList();
            for (Map.Entry<String,Number> entry : colourMap.entrySet())  {
                //System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
                pieChartDataColour.add(new PieChart.Data(entry.getKey(), (int)entry.getValue()));
            }
            //Creating a Pie chart 
            PieChart pieChartColour = new PieChart(pieChartDataColour);
            pieChartColour.setTitle("Colour Access");
            pieChartColour.setClockwise(true);
            pieChartColour.setStartAngle(180); 
            pieChartColour.setLabelLineLength(10);
            pieChartColour.setLabelsVisible(true);
            GP_container.add(pieChartColour, 1, 2);
            
            System.err.println(clothMap);
            System.err.println(fabricMap);
            System.err.println(artworkMap);
            System.err.println(weaveMap);
            System.err.println(yarnMap);            
            System.err.println(colourMap);
            /*=============== B A R ========== C H A R T ===============*/
            
            //Defining the x axis 
            CategoryAxis xAxis = new CategoryAxis();
            xAxis.setLabel("Library"); 
            xAxis.setAnimated(false);
            //Defining the y axis 
            NumberAxis yAxis = new NumberAxis();
            yAxis.setLabel("Value");
            yAxis.setAnimated(false);
            //Creating the Bar chart 
            final BarChart<String,Number> barChart = new BarChart<String,Number>(xAxis,yAxis);
            barChart.setTitle("Library Population by Access Summary");
            
            //Prepare XYChart.Series objects by setting data 
            XYChart.Series<String, Number> publicSeries = new XYChart.Series<>(); 
            publicSeries.setName("Public");
            publicSeries.getData().add(new XYChart.Data<>("Cloth", clothMap.containsKey("Public")?clothMap.get("Public"):(Number)0));
            publicSeries.getData().add(new XYChart.Data<>("Fabric", fabricMap.containsKey("Public")?fabricMap.get("Public"):(Number)0));
            publicSeries.getData().add(new XYChart.Data<>("Artwork", artworkMap.containsKey("Public")?artworkMap.get("Public"):(Number)0));
            publicSeries.getData().add(new XYChart.Data<>("Weave", weaveMap.containsKey("Public")?weaveMap.get("Public"):(Number)0));
            publicSeries.getData().add(new XYChart.Data<>("Yarn", yarnMap.containsKey("Public")?yarnMap.get("Public"):(Number)0));
            publicSeries.getData().add(new XYChart.Data<>("Colour", colourMap.containsKey("Public")?colourMap.get("Public"):(Number)0));
            
            XYChart.Series<String, Number> protectedSeries = new XYChart.Series<>(); 
            protectedSeries.setName("Protected"); 
            protectedSeries.getData().add(new XYChart.Data<>("Cloth", clothMap.containsKey("Protected")?clothMap.get("Protected"):(Number)0));
            protectedSeries.getData().add(new XYChart.Data<>("Fabric", fabricMap.containsKey("Protected")?fabricMap.get("Protected"):(Number)0));
            protectedSeries.getData().add(new XYChart.Data<>("Artwork", artworkMap.containsKey("Protected")?artworkMap.get("Protected"):(Number)0));
            protectedSeries.getData().add(new XYChart.Data<>("Weave", weaveMap.containsKey("Protected")?weaveMap.get("Protected"):(Number)0));
            protectedSeries.getData().add(new XYChart.Data<>("Yarn", yarnMap.containsKey("Protected")?yarnMap.get("Protected"):(Number)0));
            protectedSeries.getData().add(new XYChart.Data<>("Colour", colourMap.containsKey("Protected")?colourMap.get("Protected"):(Number)0));
            
            XYChart.Series<String, Number> privateSeries = new XYChart.Series<>(); 
            privateSeries.setName("Private"); 
            privateSeries.getData().add(new XYChart.Data<>("Cloth", clothMap.containsKey("Private")?clothMap.get("Private"):(Number)0));
            privateSeries.getData().add(new XYChart.Data<>("Fabric", fabricMap.containsKey("Private")?fabricMap.get("Private"):(Number)0));
            privateSeries.getData().add(new XYChart.Data<>("Artwork", artworkMap.containsKey("Private")?artworkMap.get("Private"):(Number)0));
            privateSeries.getData().add(new XYChart.Data<>("Weave", weaveMap.containsKey("Private")?weaveMap.get("Private"):(Number)0));
            privateSeries.getData().add(new XYChart.Data<>("Yarn", yarnMap.containsKey("Private")?yarnMap.get("Private"):(Number)0));
            privateSeries.getData().add(new XYChart.Data<>("Colour", colourMap.containsKey("Private")?colourMap.get("Private"):(Number)0));                   
            
            //Setting the data to bar chart   
            //barChart.getData().addAll(publicSeries, protectedSeries, privateSeries);
            barChart.getData().addAll(publicSeries, protectedSeries, privateSeries); 
            
            /*//Animating Data in Charts
            Timeline tl = new Timeline();
            tl.getKeyFrames().add(new KeyFrame(Duration.millis(500), 
                new EventHandler<ActionEvent>() {
                    @Override public void handle(ActionEvent actionEvent) {
                        for (XYChart.Series<String, Number> series : barChart.getData()) {
                            for (XYChart.Data<String, Number> data : series.getData()) {
                                //if(!data.getXValue().equalsIgnoreCase("Colour"))
                                    data.setYValue(Math.random() * 1000);
                            }
                        }
                    }
                 }
            ));
            tl.setCycleCount(Animation.INDEFINITE);
            tl.setAutoReverse(true);
            tl.play();*/
            
            GP_container.add(barChart,2, 0, 1, 3);            
        } catch (SQLException ex) {
            Logger.getLogger(LibraryView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /*Cloth Library*/
    private void populateLibraryCloth(){
        try{
            GP_container.getChildren().clear();
            List lstCloth=null;
            List lstClothDeatails = new ArrayList();
            objClothAction = new ClothAction();
            lstClothDeatails = objClothAction.lstImportCloth(objCloth);
            if(lstClothDeatails.size()==0){
                GP_container.add(new Text(objDictionaryAction.getWord("CLOTH")+" - "+objDictionaryAction.getWord("NOVALUE")),0,0);
            }else{
                for (int i=0, j = lstClothDeatails.size(), k=0; i<j; i++, k+=3){
                    lstCloth = (ArrayList)lstClothDeatails.get(i);

                    byte[] bytArtworkThumbnil = (byte[])lstCloth.get(4);
                    SeekableStream stream = new ByteArraySeekableStream(bytArtworkThumbnil);
                    String[] names = ImageCodec.getDecoderNames(stream);
                    ImageDecoder dec = ImageCodec.createImageDecoder(names[0], stream, null);
                    RenderedImage im = dec.decodeAsRenderedImage();
                    BufferedImage bufferedImage = PlanarImage.wrapRenderedImage(im).getAsBufferedImage();
                    Image image=SwingFXUtils.toFXImage(bufferedImage, null);
                    ImageView ivCloth = new ImageView(image);
                    ivCloth.setFitHeight(222);
                    ivCloth.setFitWidth(222);
                    stream.close();
                    bufferedImage = null;
                    GP_container.add(ivCloth, k%6, i/2);
                    
                    String strTempAccess="Private";
                    if(objCloth.getObjConfiguration().getObjUser().getStrUserID().equals("ADMIN"))
                       strTempAccess = "Private";
                    else
                       strTempAccess = lstCloth.get(6).toString();
                    final String strAccess = strTempAccess;//lstCloth.get(6).toString();
                    int intUsage=0;
                    
                    String strTooltip = 
                            objDictionaryAction.getWord("NAME")+": "+lstCloth.get(1).toString()+"\n"+
                            objDictionaryAction.getWord("GARMENTTYPE")+": "+lstCloth.get(2)+"\n"+
                            objDictionaryAction.getWord("DESCRIPTION")+": "+lstCloth.get(3)+"\n"+
                            objDictionaryAction.getWord("REGION")+": "+lstCloth.get(5)+"\n"+
                            objDictionaryAction.getWord("PERMISSION")+": "+lstCloth.get(6)+"\n"+
                            objDictionaryAction.getWord("BY")+": "+lstCloth.get(7).toString()+"\n"+
                            objDictionaryAction.getWord("DATE")+": "+lstCloth.get(8).toString();
                    
                    final Label lblCloth = new Label(strTooltip);
                    lblCloth.setTooltip(new Tooltip(strTooltip));
                    lblCloth.setId(lstCloth.get(0).toString());
                    lblCloth.setUserData(lstCloth.get(2).toString());
                    GP_container.add(lblCloth, (k+1)%6, i/2);
                    
                    VBox action = new VBox();
                    action.setSpacing(3);
                    Button btnExport = new Button(objDictionaryAction.getWord("EXPORT"));
                    btnExport.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export.png"));
                    btnExport.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPEXPORT")));
                    btnExport.setMaxWidth(Double.MAX_VALUE);
                    btnExport.setDisable(true);
                    if(!strAccess.equalsIgnoreCase("Public")){
                        btnExport.setDisable(false);
                        btnExport.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent e) {
                                System.gc();
                                final Stage dialogStage = new Stage();
                                dialogStage.initStyle(StageStyle.UTILITY);
                                dialogStage.initModality(Modality.APPLICATION_MODAL);
                                dialogStage.setResizable(false);
                                dialogStage.setIconified(false);
                                dialogStage.setFullScreen(false);
                                dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+":"+objDictionaryAction.getWord("ALERT"));
                                BorderPane root = new BorderPane();
                                Scene scene = new Scene(root, 300, 200, Color.WHITE);
                                scene.getStylesheets().add(LibraryView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
                                final GridPane popup=new GridPane();
                                popup.setId("popup");
                                popup.setHgap(5);
                                popup.setVgap(5);
                                popup.setPadding(new Insets(5, 5, 5, 5));
                                Label lblPassword=new Label(objDictionaryAction.getWord("SERVICEPASSWORD"));
                                lblPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                popup.add(lblPassword, 0, 0, 2, 1);
                                final PasswordField passPF =new PasswordField();
                                passPF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                passPF.setPromptText(objDictionaryAction.getWord("PROMPTSERVICEPASSWORD"));
                                popup.add(passPF, 0, 1, 2, 1);
                                final Label lblAlert=new Label("");
                                lblAlert.setStyle("-fx-wrap-text:true;");
                                lblAlert.setPrefWidth(250);
                                popup.add(lblAlert, 0, 3, 2, 1);
                                Button btnYes= new Button(objDictionaryAction.getWord("SUBMIT"));
                                btnYes.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUBMIT")));
                                btnYes.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
                                btnYes.setDefaultButton(true);
                                btnYes.setOnAction(new EventHandler<ActionEvent>(){
                                    @Override
                                    public void handle(ActionEvent event) {
                                        if(Security.SecurePassword(passPF.getText(), objConfiguration.getObjUser().getStrUsername()).equals(objConfiguration.getObjUser().getStrAppPassword())){
                                            dialogStage.close();
                                            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
                                            Date date = new Date();
                                            String currentDate = dateFormat.format(date);
                                            String savePath = System.getProperty("user.dir");

                                            DirectoryChooser directoryChooser=new DirectoryChooser();
                                            libraryStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORT")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
                                            File selectedDirectory = directoryChooser.showDialog(libraryStage);
                                            if(selectedDirectory != null)
                                                savePath = selectedDirectory.getPath();
                                            savePath = savePath+"\\"+objDictionaryAction.getWord("PROJECT")+"_"+currentDate+"\\";
                                            File file = new File(savePath);
                                            if (!file.exists()) {
                                                if (!file.mkdir())
                                                    savePath = System.getProperty("user.dir");
                                            }
                                            savePath = savePath +currentDate+".sql";
                                            file =new File(savePath);
                                            try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
                                                String content = new DbUtility().exportEachCloth(objConfiguration,lblCloth.getId().toString());
                                                bw.write(content);
                                                // no need to close it.
                                                bw.close();
                                                if(objConfiguration.getBlnAuthenticateService()){
                                                    ArrayList<File> filesToZip=new ArrayList<>();
                                                    filesToZip.add(file);
                                                    String zipFilePath=file.getAbsolutePath().substring(0, file.getAbsolutePath().indexOf(".sql"))+".bun";
                                                    String passwordToZip = file.getName().substring(0, file.getName().indexOf(".sql"));
                                                    new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                                                    file.delete();
                                                }
                                                lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+savePath);
                                            } catch (IOException ex) {
                                                new Logging("SEVERE",LibraryView.class.getName(),"Export Data"+ex.getMessage(),ex);
                                                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                            } catch (SQLException ex) {
                                                new Logging("SEVERE",LibraryView.class.getName(),"Export Data"+ex.getMessage(),ex);
                                                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                            }
                                        }
                                        else if(passPF.getText().length()==0){
                                            lblAlert.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                                        }
                                        else{
                                            lblAlert.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                                        }
                                    }
                                });
                                popup.add(btnYes, 0, 2);
                                Button btnNo = new Button(objDictionaryAction.getWord("CANCEL"));
                                btnNo.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
                                btnNo.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
                                btnNo.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        dialogStage.close();
                                        System.gc();
                                    }
                                });
                                popup.add(btnNo, 1, 2);
                                root.setCenter(popup);
                                dialogStage.setScene(scene);
                                dialogStage.showAndWait();  
                                System.gc();
                            }
                        });
                    }
                    action.getChildren().add(btnExport);
                    Button btnDelete = new Button(objDictionaryAction.getWord("DELETE"));
                    btnDelete.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/delete.png"));
                    btnDelete.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPDELETE")));
                    btnDelete.setMaxWidth(Double.MAX_VALUE);
                    btnDelete.setDisable(true);
                    if(!strAccess.equalsIgnoreCase("Public") && intUsage==0){
                        btnDelete.setDisable(false);
                        btnDelete.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent e) {
                                System.gc();
                                final Stage dialogStage = new Stage();
                                dialogStage.initStyle(StageStyle.UTILITY);
                                dialogStage.initModality(Modality.APPLICATION_MODAL);
                                dialogStage.setResizable(false);
                                dialogStage.setIconified(false);
                                dialogStage.setFullScreen(false);
                                dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+":"+objDictionaryAction.getWord("ALERT"));
                                BorderPane root = new BorderPane();
                                Scene scene = new Scene(root, 300, 180, Color.WHITE);
                                scene.getStylesheets().add(LibraryView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
                                final GridPane popup=new GridPane();
                                popup.setId("popup");
                                popup.setHgap(5);
                                popup.setVgap(5);
                                popup.setPadding(new Insets(25, 25, 25, 25));
                                popup.add(new ImageView("/media/alert_warning.png"), 0, 0); 
                                final Label lblAlert = new Label(objDictionaryAction.getWord("ALERTDELETE"));
                                lblAlert.setStyle("-fx-wrap-text:true;");
                                lblAlert.setPrefWidth(250);
                                popup.add(lblAlert, 1, 0);
                                
                                Label lblPassword=new Label(objDictionaryAction.getWord("SERVICEPASSWORD"));
                                lblPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                popup.add(lblPassword, 0, 1);
                                final PasswordField passPF= new PasswordField();
                                passPF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                passPF.setPromptText(objDictionaryAction.getWord("PROMPTSERVICEPASSWORD"));
                                popup.add(passPF, 1, 1);
                                
                                Button btnYes = new Button(objDictionaryAction.getWord("SUBMIT"));
                                btnYes.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUBMIT")));
                                btnYes.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
                                btnYes.setDefaultButton(true);
                                btnYes.setWrapText(false);
                                btnYes.setMinWidth(100);
                                btnYes.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        if(passPF.getText().length()==0){
                                            lblAlert.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                                        }
                                        else{
                                            if(Security.SecurePassword(passPF.getText(), objConfiguration.getObjUser().getStrUsername()).equals(objConfiguration.getObjUser().getStrAppPassword())){
                                                try {
                                                    lblStatus.setText(lblCloth.getId()+" : "+objDictionaryAction.getWord("ACTIONDELETE"));
                                                    objClothAction = new ClothAction();
                                                    objClothAction.clearCloth(lblCloth.getId().toString());
                                                    objClothAction = new ClothAction();
                                                    objClothAction.clearClothFabric(lblCloth.getId().toString());
                                                    populateLibraryCloth();
                                                    lblStatus.setText(lblCloth.getId()+" : "+objDictionaryAction.getWord("DATADELETED"));
                                                } catch (SQLException ex) {
                                                    new Logging("SEVERE",LibraryView.class.getName(),"Delete Data"+ex.getMessage(),ex);
                                                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                                } catch (Exception ex) {
                                                    new Logging("SEVERE",LibraryView.class.getName(),"Delete Data"+ex.getMessage(),ex);
                                                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                                }
                                                dialogStage.close();
                                            }
                                            else{
                                                lblAlert.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                                            }
                                        }
                                        System.gc();
                                    }
                                });
                                popup.add(btnYes, 0, 2);
                                Button btnNo = new Button(objDictionaryAction.getWord("CANCEL"));
                                btnNo.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
                                btnNo.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
                                btnNo.setWrapText(false);
                                //btnNo.setMaxWidth(Double.MAX_VALUE);
                                btnNo.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        dialogStage.close();
                                        System.gc();
                                    }
                                });
                                popup.add(btnNo, 1, 2);
                                root.setCenter(popup);
                                dialogStage.setScene(scene);
                                dialogStage.showAndWait();  
                                e.consume();
                                System.gc();
                            }
                        });
                    }
                    action.getChildren().add(btnDelete);
                    Button btnExportDelete = new Button(objDictionaryAction.getWord("EXPORTDELETE"));
                    btnExportDelete.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_delete.png"));
                    btnExportDelete.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPEXPORTDELETE")));
                    btnExportDelete.setMaxWidth(Double.MAX_VALUE);
                    btnExportDelete.setDisable(true);
                    if(!strAccess.equalsIgnoreCase("Public") && intUsage==0){
                        btnExportDelete.setDisable(false);
                        btnExportDelete.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent e) {
                                System.gc();
                                final Stage dialogStage = new Stage();
                                dialogStage.initStyle(StageStyle.UTILITY);
                                dialogStage.initModality(Modality.APPLICATION_MODAL);
                                dialogStage.setResizable(false);
                                dialogStage.setIconified(false);
                                dialogStage.setFullScreen(false);
                                dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+":"+objDictionaryAction.getWord("ALERT"));
                                BorderPane root = new BorderPane();
                                Scene scene = new Scene(root, 300, 150, Color.WHITE);
                                scene.getStylesheets().add(LibraryView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
                                final GridPane popup=new GridPane();
                                popup.setId("popup");
                                popup.setHgap(5);
                                popup.setVgap(5);
                                popup.setPadding(new Insets(5, 5, 5, 5));
                                Label lblPassword=new Label(objDictionaryAction.getWord("SERVICEPASSWORD"));
                                lblPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                popup.add(lblPassword, 0, 0, 2, 1);
                                final PasswordField passPF =new PasswordField();
                                passPF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                passPF.setPromptText(objDictionaryAction.getWord("PROMPTSERVICEPASSWORD"));
                                popup.add(passPF, 0, 1, 2, 1);
                                final Label lblAlert=new Label("");
                                lblAlert.setStyle("-fx-wrap-text:true;");
                                lblAlert.setPrefWidth(250);
                                popup.add(lblAlert, 0, 3, 2, 1);
                                Button btnYes = new Button(objDictionaryAction.getWord("SUBMIT"));
                                btnYes.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUBMIT")));
                                btnYes.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
                                btnYes.setDefaultButton(true);
                                btnYes.setOnAction(new EventHandler<ActionEvent>(){
                                    @Override
                                    public void handle(ActionEvent event) {
                                        if(Security.SecurePassword(passPF.getText(), objConfiguration.getObjUser().getStrUsername()).equals(objConfiguration.getObjUser().getStrAppPassword())){
                                            dialogStage.close();
                                            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
                                            Date date = new Date();
                                            String currentDate = dateFormat.format(date);
                                            String savePath = System.getProperty("user.dir");

                                            DirectoryChooser directoryChooser=new DirectoryChooser();
                                            libraryStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORT")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
                                            File selectedDirectory = directoryChooser.showDialog(libraryStage);
                                            if(selectedDirectory != null)
                                                savePath = selectedDirectory.getPath();
                                            savePath = savePath+"\\"+objDictionaryAction.getWord("PROJECT")+"_"+currentDate+"\\";
                                            File file = new File(savePath);
                                            if (!file.exists()) {
                                                if (!file.mkdir())
                                                    savePath = System.getProperty("user.dir");
                                            }
                                            savePath = savePath +currentDate+".sql";
                                            file =new File(savePath);
                                            try (BufferedWriter bw = new BufferedWriter(new FileWriter(savePath))) {
                                                String content = new DbUtility().exportEachCloth(objConfiguration,lblCloth.getId().toString());
                                                bw.write(content);
                                                // no need to close it.
                                                //bw.close();
                                                if(objConfiguration.getBlnAuthenticateService()){
                                                    ArrayList<File> filesToZip=new ArrayList<>();
                                                    filesToZip.add(file);
                                                    String zipFilePath=file.getAbsolutePath().substring(0, file.getAbsolutePath().indexOf(".sql"))+".bun";
                                                    String passwordToZip = file.getName().substring(0, file.getName().indexOf(".sql"));
                                                    new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                                                    file.delete();
                                                }
                                                lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+savePath);
                                                objClothAction = new ClothAction();
                                                objClothAction.clearCloth(lblCloth.getId().toString());
                                                objClothAction = new ClothAction();
                                                objClothAction.clearClothFabric(lblCloth.getId().toString());
                                                populateLibraryCloth();
                                            } catch (IOException ex) {
                                                new Logging("SEVERE",LibraryView.class.getName(),"Export Data"+ex.getMessage(),ex);
                                                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                            } catch (SQLException ex) {
                                                new Logging("SEVERE",LibraryView.class.getName(),"Export Data"+ex.getMessage(),ex);
                                                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                            }
                                        }
                                        else if(passPF.getText().length()==0){
                                            lblAlert.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                                        }
                                        else{
                                            lblAlert.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                                        }
                                    }
                                });
                                popup.add(btnYes, 0, 2, 1, 1);
                                Button btnNo = new Button(objDictionaryAction.getWord("CANCEL"));
                                btnNo.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
                                btnNo.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
                                btnNo.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        dialogStage.close();
                                        System.gc();
                                    }
                                });
                                popup.add(btnNo, 1, 2, 1, 1);
                                root.setCenter(popup);
                                dialogStage.setScene(scene);
                                dialogStage.showAndWait();  
                                System.gc();
                            }
                        });
                    }
                    action.getChildren().add(btnExportDelete);
                    Button btnPermission = new Button(objDictionaryAction.getWord("CHANGEPERMISSION"));
                    btnPermission.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/settings.png"));
                    btnPermission.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCHANGEPERMISSION")));
                    btnPermission.setMaxWidth(Double.MAX_VALUE);
                    btnPermission.setDisable(true);
                    if(!strAccess.equalsIgnoreCase("Public")){
                        btnPermission.setDisable(false);
                        btnPermission.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent e) {
                                final Stage dialogStage = new Stage();
                                dialogStage.initStyle(StageStyle.UTILITY);
                                dialogStage.initModality(Modality.APPLICATION_MODAL);
                                dialogStage.setResizable(false);
                                dialogStage.setIconified(false);
                                dialogStage.setFullScreen(false);
                                dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+":"+objDictionaryAction.getWord("ALERT"));
                                BorderPane root = new BorderPane();
                                Scene scene = new Scene(root, 300, 180, Color.WHITE);
                                scene.getStylesheets().add(LibraryView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
                                final GridPane popup=new GridPane();
                                popup.setId("popup");
                                popup.setHgap(5);
                                popup.setVgap(5);
                                popup.setPadding(new Insets(25, 25, 25, 25));
                                
                                Label lblData = new Label(objDictionaryAction.getWord("SELECT")+" "+objDictionaryAction.getWord("PERMISSION"));
                                lblData.setStyle("-fx-wrap-text:true;");
                                lblData.setPrefWidth(250);
                                popup.add(lblData, 0, 0, 3, 1);
                                final ToggleGroup dataTG = new ToggleGroup();
                                RadioButton dataPublicRB = new RadioButton(objDictionaryAction.getWord("PUBLIC"));
                                dataPublicRB.setToggleGroup(dataTG);
                                dataPublicRB.setUserData("Public");
                                popup.add(dataPublicRB, 0, 1);
                                RadioButton dataPrivateRB = new RadioButton(objDictionaryAction.getWord("PRIVATE"));
                                dataPrivateRB.setToggleGroup(dataTG);
                                dataPrivateRB.setUserData("Private");
                                popup.add(dataPrivateRB, 1, 1);
                                RadioButton dataProtectedRB = new RadioButton(objDictionaryAction.getWord("PROTECTED"));
                                dataProtectedRB.setToggleGroup(dataTG);
                                dataProtectedRB.setUserData("Protected");
                                popup.add(dataProtectedRB, 2, 1);
                                if(strAccess.equalsIgnoreCase("Public"))
                                    dataTG.selectToggle(dataPublicRB);
                                else if(strAccess.equalsIgnoreCase("Private"))
                                    dataTG.selectToggle(dataPrivateRB);
                                else 
                                    dataTG.selectToggle(dataProtectedRB);
                                
                                // Added 20 Feb 2017 -----------------------------------------
                                final Label lblPassword=new Label(objDictionaryAction.getWord("SERVICEPASSWORD"));
                                lblPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                popup.add(lblPassword, 0, 2, 1, 1);
                                final PasswordField passPF=new PasswordField();
                                passPF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                passPF.setPromptText(objDictionaryAction.getWord("PROMPTSERVICEPASSWORD"));
                                popup.add(passPF, 1, 2, 2, 1);
                                // -----------------------------------------------------------
                                
                                final Label lblAlert = new Label(objDictionaryAction.getWord("ALERTWHAT"));
                                lblAlert.setStyle("-fx-wrap-text:true;");
                                lblAlert.setPrefWidth(250);
                                popup.add(lblAlert, 0, 4, 3, 1);
                                
                                Button btnYes = new Button(objDictionaryAction.getWord("SUBMIT"));
                                btnYes.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUBMIT")));
                                btnYes.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
                                btnYes.setDefaultButton(true);
                                btnYes.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        if(passPF.getText().length()==0){
                                            lblAlert.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                                        }
                                        else{
                                            if(Security.SecurePassword(passPF.getText(), objConfiguration.getObjUser().getStrUsername()).equals(objConfiguration.getObjUser().getStrAppPassword())){
                                                try {
                                                    String strAccessNew = dataTG.getSelectedToggle().getUserData().toString();
                                                    System.err.println(strAccessNew);
                                                    strAccessNew = new IDGenerator().setUserAcessValueData("Cloth_LIBRARY",strAccessNew);
                                                    objClothAction = new ClothAction();
                                                    objClothAction.resetClothPermission(lblCloth.getId(),strAccessNew);
                                                    dialogStage.close();                                            
                                                    populateLibraryFabric();
                                                } catch (SQLException ex) {
                                                    new Logging("SEVERE",LibraryView.class.getName(),"Change Permission Data"+ex.getMessage(),ex);
                                                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                                }
                                            }
                                            else{
                                                lblAlert.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                                            }
                                        }
                                        System.gc();
                                    }
                                });
                                popup.add(btnYes, 0, 3);
                                Button btnNo = new Button(objDictionaryAction.getWord("CANCEL"));
                                btnNo.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
                                btnNo.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
                                btnNo.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        dialogStage.close();
                                        System.gc();
                                    }
                                });
                                popup.add(btnNo, 2, 3);
                                root.setCenter(popup);
                                dialogStage.setScene(scene);
                                dialogStage.showAndWait();  
                                e.consume();
                                System.gc();
                            }
                        });
                    }
                    action.getChildren().add(btnPermission);
                    Button btnUpdate;
                    if(!strAccess.equalsIgnoreCase("Public") && intUsage==0){
                        btnUpdate = new Button(objDictionaryAction.getWord("UPDATE"));
                        btnUpdate.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/update.png"));
                        btnUpdate.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPUPDATE")));
                    }else{
                        btnUpdate = new Button(objDictionaryAction.getWord("COPY"));
                        btnUpdate.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/copy.png"));
                        btnUpdate.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCOPY")));
                    }
                    btnUpdate.setMaxWidth(Double.MAX_VALUE);
                    btnUpdate.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent e) {
                            if(strAccess.equalsIgnoreCase("Public")){
                                objConfiguration.setServicePasswordValid(true);
                            } else{
                                new MessageView(objConfiguration);
                            }
                            if(objConfiguration.getServicePasswordValid()){
                                objConfiguration.setServicePasswordValid(false);
                                try {
                                    objConfiguration.strWindowFlowContext = "Dashboard";
                                    objConfiguration.setStrRecentCloth(lblCloth.getId());
                                    libraryStage.close();
                                    System.gc();
                                    ClothView objClothView = new ClothView(objConfiguration);
                                    System.gc();
                                } catch (Exception ex) {
                                    new Logging("SEVERE",LibraryView.class.getName(),"Load Cloth in Editor"+ex.toString(),ex);
                                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                }
                            }
                        }
                    });
                    action.getChildren().add(btnUpdate);
                    GP_container.add(action, (k+2)%6, i/2);
                }
            }
        } catch (Exception ex) {
            new Logging("SEVERE",LibraryView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    /*Fabric Library*/
    private void populateLibraryFabric(){
        try{
            GP_container.getChildren().clear();
            List lstFabric=null;
            List lstFabricDeatails = new ArrayList();
            objFabricAction = new FabricAction();
            lstFabricDeatails = objFabricAction.lstImportFabric(objFabric);
            if(lstFabricDeatails.size()==0){
                GP_container.add(new Text(objDictionaryAction.getWord("FABRIC")+" - "+objDictionaryAction.getWord("NOVALUE")),0,0);
            }else{
                for (int i=0, j = lstFabricDeatails.size(), k=0; i<j; i++, k+=3){
                    lstFabric = (ArrayList)lstFabricDeatails.get(i);

                    byte[] bytArtworkThumbnil = (byte[])lstFabric.get(26);
                    SeekableStream stream = new ByteArraySeekableStream(bytArtworkThumbnil);
                    String[] names = ImageCodec.getDecoderNames(stream);
                    ImageDecoder dec = ImageCodec.createImageDecoder(names[0], stream, null);
                    RenderedImage im = dec.decodeAsRenderedImage();
                    BufferedImage bufferedImage = PlanarImage.wrapRenderedImage(im).getAsBufferedImage();
                    Image image=SwingFXUtils.toFXImage(bufferedImage, null);
                    ImageView ivFabric = new ImageView(image);
                    ivFabric.setFitHeight(222);
                    ivFabric.setFitWidth(222);
                    stream.close();
                    bufferedImage = null;
                    GP_container.add(ivFabric, k%6, i/2);
                    
                    String strTempAccess="Private";
                    if(objFabric.getObjConfiguration().getObjUser().getStrUserID().equals("ADMIN"))
                        strTempAccess = "Private";
                    else
                        strTempAccess = lstFabric.get(29).toString();
                    final String strAccess = strTempAccess;//lstFabric.get(29).toString();
                    int intUsage=0;
                    objFabricAction = new FabricAction();
                    intUsage = objFabricAction.countFabricUsage(lstFabric.get(0).toString());
                    
                    String strTooltip = 
                            objDictionaryAction.getWord("NAME")+": "+lstFabric.get(1).toString()+"\n"+
                            objDictionaryAction.getWord("CLOTHTYPE")+": "+lstFabric.get(2)+"\n"+
                            objDictionaryAction.getWord("FABRICTYPE")+": "+lstFabric.get(3)+"\n"+
                            objDictionaryAction.getWord("FABRICLENGTH")+": "+lstFabric.get(4)+"\n"+
                            objDictionaryAction.getWord("FABRICWIDTH")+": "+lstFabric.get(5)+"\n"+
                            objDictionaryAction.getWord("ARTWORKLENGTH")+": "+lstFabric.get(6)+"\n"+
                            objDictionaryAction.getWord("ARTWORKWIDTH")+": "+lstFabric.get(7)+"\n"+
                            objDictionaryAction.getWord("WEFT")+": "+lstFabric.get(10)+"\n"+
                            objDictionaryAction.getWord("WARP")+": "+lstFabric.get(11)+"\n"+
                            objDictionaryAction.getWord("SHAFT")+": "+lstFabric.get(14)+"\n"+
                            objDictionaryAction.getWord("HOOKS")+": "+lstFabric.get(15)+"\n"+
                            objDictionaryAction.getWord("HPI")+": "+lstFabric.get(16)+"\n"+
                            objDictionaryAction.getWord("REEDCOUNT")+": "+lstFabric.get(17)+"\n"+
                            objDictionaryAction.getWord("DENTS")+": "+lstFabric.get(18)+"\n"+
                            objDictionaryAction.getWord("TPD")+": "+lstFabric.get(19)+"\n"+                            
                            objDictionaryAction.getWord("EPI")+": "+lstFabric.get(20)+"\n"+
                            objDictionaryAction.getWord("PPI")+": "+lstFabric.get(21)+"\n"+
                            objDictionaryAction.getWord("PROTECTION")+": "+lstFabric.get(22)+"\n"+
                            objDictionaryAction.getWord("BINDING")+": "+lstFabric.get(23)+"\n"+                            
                            objDictionaryAction.getWord("PERMISSION")+": "+lstFabric.get(29)+"\n"+
                            objDictionaryAction.getWord("BY")+": "+lstFabric.get(31)+"\n"+
                            objDictionaryAction.getWord("DATE")+": "+lstFabric.get(30);
                    if(objFabric.getObjConfiguration().getObjUser().getStrUserID().equals("ADMIN"))
                        intUsage=0;
                    final Label lblFabric = new Label(strTooltip);
                    lblFabric.setTooltip(new Tooltip(strTooltip));
                    lblFabric.setId(lstFabric.get(0).toString());
                    lblFabric.setUserData(lstFabric.get(2).toString());
                    GP_container.add(lblFabric, (k+1)%6, i/2);
                    
                    VBox action = new VBox();
                    action.setSpacing(3);
                    Button btnExport = new Button(objDictionaryAction.getWord("EXPORT"));
                    btnExport.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export.png"));
                    btnExport.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPEXPORT")));
                    btnExport.setMaxWidth(Double.MAX_VALUE);
                    btnExport.setDisable(true);
                    if(!strAccess.equalsIgnoreCase("Public")){
                        btnExport.setDisable(false);
                        btnExport.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent e) {
                                System.gc();
                                final Stage dialogStage = new Stage();
                                dialogStage.initStyle(StageStyle.UTILITY);
                                dialogStage.initModality(Modality.APPLICATION_MODAL);
                                dialogStage.setResizable(false);
                                dialogStage.setIconified(false);
                                dialogStage.setFullScreen(false);
                                dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+":"+objDictionaryAction.getWord("ALERT"));
                                BorderPane root = new BorderPane();
                                Scene scene = new Scene(root, 300, 200, Color.WHITE);
                                scene.getStylesheets().add(LibraryView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
                                final GridPane popup=new GridPane();
                                popup.setId("popup");
                                popup.setHgap(5);
                                popup.setVgap(5);
                                popup.setPadding(new Insets(5, 5, 5, 5));
                                Label lblPassword=new Label(objDictionaryAction.getWord("SERVICEPASSWORD"));
                                lblPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                popup.add(lblPassword, 0, 0, 2, 1);
                                final PasswordField passPF =new PasswordField();
                                passPF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                passPF.setPromptText(objDictionaryAction.getWord("PROMPTSERVICEPASSWORD"));
                                popup.add(passPF, 0, 1, 2, 1);
                                final Label lblAlert=new Label("");
                                lblAlert.setStyle("-fx-wrap-text:true;");
                                lblAlert.setPrefWidth(250);
                                popup.add(lblAlert, 0, 3, 2, 1);
                                Button btnYes= new Button(objDictionaryAction.getWord("SUBMIT"));
                                btnYes.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUBMIT")));
                                btnYes.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
                                btnYes.setDefaultButton(true);
                                btnYes.setOnAction(new EventHandler<ActionEvent>(){
                                    @Override
                                    public void handle(ActionEvent event) {
                                        if(Security.SecurePassword(passPF.getText(), objConfiguration.getObjUser().getStrUsername()).equals(objConfiguration.getObjUser().getStrAppPassword())){
                                            dialogStage.close();
                                            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
                                            Date date = new Date();
                                            String currentDate = dateFormat.format(date);
                                            String savePath = System.getProperty("user.dir");

                                            DirectoryChooser directoryChooser=new DirectoryChooser();
                                            libraryStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORT")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
                                            File selectedDirectory = directoryChooser.showDialog(libraryStage);
                                            if(selectedDirectory != null)
                                                savePath = selectedDirectory.getPath();
                                            savePath = savePath+"\\"+objDictionaryAction.getWord("PROJECT")+"_"+currentDate+"\\";
                                            File file = new File(savePath);
                                            if (!file.exists()) {
                                                if (!file.mkdir())
                                                    savePath = System.getProperty("user.dir");
                                            }
                                            savePath = savePath +currentDate+".sql";
                                            file =new File(savePath);
                                            try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
											String content = new DbUtility().exportEachFabric(objConfiguration,lblFabric.getId().toString());
                                                bw.write(content);
                                                // no need to close it.
                                                bw.close();
                                                if(objConfiguration.getBlnAuthenticateService()){
                                                    ArrayList<File> filesToZip=new ArrayList<>();
                                                    filesToZip.add(file);
                                                    String zipFilePath=file.getAbsolutePath().substring(0, file.getAbsolutePath().indexOf(".sql"))+".bun";
                                                    String passwordToZip = file.getName().substring(0, file.getName().indexOf(".sql"));
                                                    new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                                                    file.delete();
                                                }
                                                lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+savePath);
                                            } catch (IOException ex) {
                                                new Logging("SEVERE",LibraryView.class.getName(),"Export Data"+ex.getMessage(),ex);
                                                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                            } catch (SQLException ex) {
                                                new Logging("SEVERE",LibraryView.class.getName(),"Export Data"+ex.getMessage(),ex);
                                                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                            }
                                        }
                                        else if(passPF.getText().length()==0){
                                            lblAlert.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                                        }
                                        else{
                                            lblAlert.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                                        }
                                    }
                                });
                                popup.add(btnYes, 0, 2);
                                Button btnNo = new Button(objDictionaryAction.getWord("CANCEL"));
                                btnNo.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
                                btnNo.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
                                btnNo.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        dialogStage.close();
                                        System.gc();
                                    }
                                });
                                popup.add(btnNo, 1, 2);
                                root.setCenter(popup);
                                dialogStage.setScene(scene);
                                dialogStage.showAndWait();  
                                System.gc();
                            }
                        });
                    }
                    action.getChildren().add(btnExport);
                    Button btnDelete = new Button(objDictionaryAction.getWord("DELETE"));
                    btnDelete.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/delete.png"));
                    btnDelete.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPDELETE")));
                    btnDelete.setMaxWidth(Double.MAX_VALUE);
                    btnDelete.setDisable(true);
                    if(!strAccess.equalsIgnoreCase("Public") && intUsage==0){
                        btnDelete.setDisable(false);
                        btnDelete.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent e) {
                                System.gc();
                                final Stage dialogStage = new Stage();
                                dialogStage.initStyle(StageStyle.UTILITY);
                                dialogStage.initModality(Modality.APPLICATION_MODAL);
                                dialogStage.setResizable(false);
                                dialogStage.setIconified(false);
                                dialogStage.setFullScreen(false);
                                dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+":"+objDictionaryAction.getWord("ALERT"));
                                BorderPane root = new BorderPane();
                                Scene scene = new Scene(root, 300, 180, Color.WHITE);
                                scene.getStylesheets().add(LibraryView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
                                final GridPane popup=new GridPane();
                                popup.setId("popup");
                                popup.setHgap(5);
                                popup.setVgap(5);
                                popup.setPadding(new Insets(25, 25, 25, 25));
                                popup.add(new ImageView("/media/alert_warning.png"), 0, 0); 
                                final Label lblAlert = new Label(objDictionaryAction.getWord("ALERTDELETE"));
                                lblAlert.setStyle("-fx-wrap-text:true;");
                                lblAlert.setPrefWidth(250);
                                popup.add(lblAlert, 1, 0);
                                
                                Label lblPassword=new Label(objDictionaryAction.getWord("SERVICEPASSWORD"));
                                lblPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                popup.add(lblPassword, 0, 1);
                                final PasswordField passPF= new PasswordField();
                                passPF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                passPF.setPromptText(objDictionaryAction.getWord("PROMPTSERVICEPASSWORD"));
                                popup.add(passPF, 1, 1);
                                
                                Button btnYes = new Button(objDictionaryAction.getWord("SUBMIT"));
                                btnYes.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUBMIT")));
                                btnYes.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
                                btnYes.setDefaultButton(true);
                                btnYes.setWrapText(false);
                                btnYes.setMinWidth(100);
                                btnYes.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        if(passPF.getText().length()==0){
                                            lblAlert.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                                        }
                                        else{
                                            if(Security.SecurePassword(passPF.getText(), objConfiguration.getObjUser().getStrUsername()).equals(objConfiguration.getObjUser().getStrAppPassword())){
                                                try {
                                                    lblStatus.setText(lblFabric.getId()+" : "+objDictionaryAction.getWord("ACTIONDELETE"));
                                                    objFabricAction = new FabricAction();
                                                    objFabricAction.clearFabric(lblFabric.getId().toString(),"Main");
                                                    objFabricAction = new FabricAction();
                                                    objFabricAction.clearFabricArtwork(lblFabric.getId().toString(),"Main");
                                                    YarnAction objYarnAction = new YarnAction();
                                                    objYarnAction.clearFabricYarn(lblFabric.getId().toString(),"Main");
                                                    objFabricAction = new FabricAction();
                                                    objFabricAction.clearFabricPallets(lblFabric.getId().toString(),"Main");
                                                    objFabricAction = new FabricAction();
                                                    objFabricAction.clearFabric(lblFabric.getId().toString(),"Main");
                                                    populateLibraryFabric();
                                                    lblStatus.setText(lblFabric.getId()+" : "+objDictionaryAction.getWord("DATADELETED"));
                                                } catch (SQLException ex) {
                                                    new Logging("SEVERE",LibraryView.class.getName(),"Export Data"+ex.getMessage(),ex);
                                                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                                } catch (Exception ex) {
                                                    new Logging("SEVERE",LibraryView.class.getName(),"Export Data"+ex.getMessage(),ex);
                                                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                                }
                                                dialogStage.close();
                                            }
                                            else{
                                                lblAlert.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                                            }
                                        }
                                        System.gc();
                                    }
                                });
                                popup.add(btnYes, 0, 3);
                                Button btnNo = new Button(objDictionaryAction.getWord("CANCEL"));
                                btnNo.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
                                btnNo.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
                                btnNo.setWrapText(false);
                                btnNo.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        dialogStage.close();
                                        System.gc();
                                    }
                                });
                                popup.add(btnNo, 1, 3);
                                root.setCenter(popup);
                                dialogStage.setScene(scene);
                                dialogStage.showAndWait();  
                                e.consume();
                                System.gc();
                            }
                        });                        
                    }
                    action.getChildren().add(btnDelete);
                    Button btnExportDelete = new Button(objDictionaryAction.getWord("EXPORTDELETE"));
                    btnExportDelete.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_delete.png"));
                    btnExportDelete.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPEXPORTDELETE")));
                    btnExportDelete.setMaxWidth(Double.MAX_VALUE);
                    btnExportDelete.setDisable(true);
                    if(!strAccess.equalsIgnoreCase("Public") && intUsage==0){
                        btnExportDelete.setDisable(false);
                        btnExportDelete.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent e) {
                                System.gc();
                                final Stage dialogStage = new Stage();
                                dialogStage.initStyle(StageStyle.UTILITY);
                                dialogStage.initModality(Modality.APPLICATION_MODAL);
                                dialogStage.setResizable(false);
                                dialogStage.setIconified(false);
                                dialogStage.setFullScreen(false);
                                dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+":"+objDictionaryAction.getWord("ALERT"));
                                BorderPane root = new BorderPane();
                                Scene scene = new Scene(root, 300, 150, Color.WHITE);
                                scene.getStylesheets().add(LibraryView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
                                final GridPane popup=new GridPane();
                                popup.setId("popup");
                                popup.setHgap(5);
                                popup.setVgap(5);
                                popup.setPadding(new Insets(5, 5, 5, 5));
                                Label lblPassword=new Label(objDictionaryAction.getWord("SERVICEPASSWORD"));
                                lblPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                popup.add(lblPassword, 0, 0, 2, 1);
                                final PasswordField passPF =new PasswordField();
                                passPF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                passPF.setPromptText(objDictionaryAction.getWord("PROMPTSERVICEPASSWORD"));
                                popup.add(passPF, 0, 1, 2, 1);
                                final Label lblAlert=new Label("");
                                lblAlert.setStyle("-fx-wrap-text:true;");
                                lblAlert.setPrefWidth(250);
                                popup.add(lblAlert, 0, 3, 2, 1);
                                Button btnYes = new Button(objDictionaryAction.getWord("SUBMIT"));
                                btnYes.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUBMIT")));
                                btnYes.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
                                btnYes.setDefaultButton(true);
                                btnYes.setOnAction(new EventHandler<ActionEvent>(){
                                    @Override
                                    public void handle(ActionEvent event) {
                                        if(Security.SecurePassword(passPF.getText(), objConfiguration.getObjUser().getStrUsername()).equals(objConfiguration.getObjUser().getStrAppPassword())){
                                            dialogStage.close();
                                            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
                                            Date date = new Date();
                                            String currentDate = dateFormat.format(date);
                                            String savePath = System.getProperty("user.dir");

                                            DirectoryChooser directoryChooser=new DirectoryChooser();
                                            libraryStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORT")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
                                            File selectedDirectory = directoryChooser.showDialog(libraryStage);
                                            if(selectedDirectory != null)
                                                savePath = selectedDirectory.getPath();
                                            savePath = savePath+"\\"+objDictionaryAction.getWord("PROJECT")+"_"+currentDate+"\\";
                                            File file = new File(savePath);
                                            if (!file.exists()) {
                                                if (!file.mkdir())
                                                    savePath = System.getProperty("user.dir");
                                            }
                                            savePath = savePath +currentDate+".sql";
                                            file =new File(savePath);
                                            try (BufferedWriter bw = new BufferedWriter(new FileWriter(savePath))) {
                                                String content = new DbUtility().exportEachFabric(objConfiguration,lblFabric.getId().toString());
                                                bw.write(content);
                                                // no need to close it.
                                                //bw.close();
                                                if(objConfiguration.getBlnAuthenticateService()){
                                                    ArrayList<File> filesToZip=new ArrayList<>();
                                                    filesToZip.add(file);
                                                    String zipFilePath=file.getAbsolutePath().substring(0, file.getAbsolutePath().indexOf(".sql"))+".bun";
                                                    String passwordToZip = file.getName().substring(0, file.getName().indexOf(".sql"));
                                                    new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                                                    file.delete();
                                                }
                                                lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+savePath);
                                                objFabricAction = new FabricAction();
                                                objFabricAction.clearFabric(lblFabric.getId().toString(),"Main");
                                                objFabricAction = new FabricAction();
                                                objFabricAction.clearFabricArtwork(lblFabric.getId().toString(),"Main");
                                                YarnAction objYarnAction = new YarnAction();
                                                objYarnAction.clearFabricYarn(lblFabric.getId().toString(),"Main");
                                                objFabricAction = new FabricAction();
                                                objFabricAction.clearFabricPallets(lblFabric.getId().toString(),"Main");
                                                objFabricAction = new FabricAction();
                                                objFabricAction.clearFabric(lblFabric.getId().toString(),"Main");
                                                populateLibraryFabric();
                                            } catch (IOException ex) {
                                                new Logging("SEVERE",LibraryView.class.getName(),"Export Data"+ex.getMessage(),ex);
                                                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                            } catch (SQLException ex) {
                                                new Logging("SEVERE",LibraryView.class.getName(),"Export Data"+ex.getMessage(),ex);
                                                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                            }
                                        }
                                        else if(passPF.getText().length()==0){
                                            lblAlert.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                                        }
                                        else{
                                            lblAlert.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                                        }
                                    }
                                });
                                popup.add(btnYes, 0, 2, 1, 1);
                                Button btnNo = new Button(objDictionaryAction.getWord("CANCEL"));
                                btnNo.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
                                btnNo.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
                                btnNo.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        dialogStage.close();
                                        System.gc();
                                    }
                                });
                                popup.add(btnNo, 1, 2, 1, 1);
                                root.setCenter(popup);
                                dialogStage.setScene(scene);
                                dialogStage.showAndWait();  
                                System.gc();
                            }
                        });
                    }
                    action.getChildren().add(btnExportDelete);
                    Button btnPermission = new Button(objDictionaryAction.getWord("CHANGEPERMISSION"));
                    btnPermission.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/settings.png"));
                    btnPermission.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCHANGEPERMISSION")));
                    btnPermission.setMaxWidth(Double.MAX_VALUE);
                    btnPermission.setDisable(true);
                    if(!strAccess.equalsIgnoreCase("Public")){
                        btnPermission.setDisable(false);
                        btnPermission.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent e) {
                                final Stage dialogStage = new Stage();
                                dialogStage.initStyle(StageStyle.UTILITY);
                                dialogStage.initModality(Modality.APPLICATION_MODAL);
                                dialogStage.setResizable(false);
                                dialogStage.setIconified(false);
                                dialogStage.setFullScreen(false);
                                dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+":"+objDictionaryAction.getWord("ALERT"));
                                BorderPane root = new BorderPane();
                                Scene scene = new Scene(root, 300, 180, Color.WHITE);
                                scene.getStylesheets().add(LibraryView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
                                final GridPane popup=new GridPane();
                                popup.setId("popup");
                                popup.setHgap(5);
                                popup.setVgap(5);
                                popup.setPadding(new Insets(25, 25, 25, 25));
                                
                                Label lblData = new Label(objDictionaryAction.getWord("SELECT")+" "+objDictionaryAction.getWord("PERMISSION"));
                                lblData.setStyle("-fx-wrap-text:true;");
                                lblData.setPrefWidth(250);
                                popup.add(lblData, 0, 0, 3, 1);
                                final ToggleGroup dataTG = new ToggleGroup();
                                RadioButton dataPublicRB = new RadioButton(objDictionaryAction.getWord("PUBLIC"));
                                dataPublicRB.setToggleGroup(dataTG);
                                dataPublicRB.setUserData("Public");
                                popup.add(dataPublicRB, 0, 1);
                                RadioButton dataPrivateRB = new RadioButton(objDictionaryAction.getWord("PRIVATE"));
                                dataPrivateRB.setToggleGroup(dataTG);
                                dataPrivateRB.setUserData("Private");
                                popup.add(dataPrivateRB, 1, 1);
                                RadioButton dataProtectedRB = new RadioButton(objDictionaryAction.getWord("PROTECTED"));
                                dataProtectedRB.setToggleGroup(dataTG);
                                dataProtectedRB.setUserData("Protected");
                                popup.add(dataProtectedRB, 2, 1);
                                if(strAccess.equalsIgnoreCase("Public"))
                                    dataTG.selectToggle(dataPublicRB);
                                else if(strAccess.equalsIgnoreCase("Private"))
                                    dataTG.selectToggle(dataPrivateRB);
                                else 
                                    dataTG.selectToggle(dataProtectedRB);
                                
                                // Added 20 Feb 2017 -----------------------------------------
                                final Label lblPassword=new Label(objDictionaryAction.getWord("SERVICEPASSWORD"));
                                lblPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                popup.add(lblPassword, 0, 2, 1, 1);
                                final PasswordField passPF=new PasswordField();
                                passPF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                passPF.setPromptText(objDictionaryAction.getWord("PROMPTSERVICEPASSWORD"));
                                popup.add(passPF, 1, 2, 2, 1);
                                // -----------------------------------------------------------
                                
                                final Label lblAlert = new Label(objDictionaryAction.getWord("ALERTWHAT"));
                                lblAlert.setStyle("-fx-wrap-text:true;");
                                lblAlert.setPrefWidth(250);
                                popup.add(lblAlert, 0, 4, 3, 1);
                                
                                Button btnYes = new Button(objDictionaryAction.getWord("SUBMIT"));
                                btnYes.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUBMIT")));
                                btnYes.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
                                btnYes.setDefaultButton(true);
                                btnYes.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        if(passPF.getText().length()==0){
                                            lblAlert.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                                        }
                                        else{
                                            if(Security.SecurePassword(passPF.getText(), objConfiguration.getObjUser().getStrUsername()).equals(objConfiguration.getObjUser().getStrAppPassword())){
                                                try {
                                                    String strAccessNew = dataTG.getSelectedToggle().getUserData().toString();
                                                    System.err.println(strAccessNew);
                                                    strAccessNew = new IDGenerator().setUserAcessValueData("FABRIC_LIBRARY",strAccessNew);
                                                    objFabricAction = new FabricAction();
                                                    objFabricAction.resetFabricPermission(lblFabric.getId(),strAccessNew);
                                                    dialogStage.close();                                            
                                                    populateLibraryFabric();
                                                } catch (SQLException ex) {
                                                    new Logging("SEVERE",LibraryView.class.getName(),"Change Permission Data"+ex.getMessage(),ex);
                                                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                                }
                                            }
                                            else{
                                                lblAlert.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                                            }
                                        }
                                        System.gc();
                                    }
                                });
                                popup.add(btnYes, 0, 3);
                                Button btnNo = new Button(objDictionaryAction.getWord("CANCEL"));
                                btnNo.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
                                btnNo.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
                                btnNo.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        dialogStage.close();
                                        System.gc();
                                    }
                                });
                                popup.add(btnNo, 2, 3);
                                root.setCenter(popup);
                                dialogStage.setScene(scene);
                                dialogStage.showAndWait();  
                                e.consume();
                                System.gc();
                            }
                        });
                    }
                    action.getChildren().add(btnPermission);
                    Button btnUpdate;
                    if(!strAccess.equalsIgnoreCase("Public") && intUsage==0){
                        btnUpdate = new Button(objDictionaryAction.getWord("UPDATE"));
                        btnUpdate.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/update.png"));
                        btnUpdate.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPUPDATE")));
                    }else{
                        btnUpdate = new Button(objDictionaryAction.getWord("COPY"));
                        btnUpdate.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/copy.png"));
                        btnUpdate.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCOPY")));
                    }
                    btnUpdate.setMaxWidth(Double.MAX_VALUE);
                    btnUpdate.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent e) {
                            if(strAccess.equalsIgnoreCase("Public")){
                                objConfiguration.setServicePasswordValid(true);
                            } else{
                                new MessageView(objConfiguration);
                            }
                            if(objConfiguration.getServicePasswordValid()){
                                objConfiguration.setServicePasswordValid(false);
                                try {
                                    objConfiguration.setStrClothType(lblFabric.getUserData().toString());
                                    UserAction objUserAction = new UserAction();
                                    objUserAction.getConfiguration(objConfiguration);
                                    //objConfiguration.clothRepeat();
                                    objConfiguration.strWindowFlowContext = "Dashboard";
                                    objConfiguration.setStrRecentFabric(lblFabric.getId());
                                    libraryStage.close();
                                    System.gc();
                                    FabricView objFabricView = new FabricView(objConfiguration);
                                    System.gc();
                                } catch (SQLException ex) {
                                    new Logging("SEVERE",LibraryView.class.getName(),"Load Fabric in Editor"+ex.toString(),ex);
                                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                }
                            }
                        }
                    });
                    action.getChildren().add(btnUpdate);
                    GP_container.add(action, (k+2)%6, i/2);
                }
            }
        } catch (Exception ex) {
            new Logging("SEVERE",LibraryView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    /*Artwork Library*/
    private void populateLibraryArtwork(){
        try{
            GP_container.getChildren().clear();
            List lstArtwork=null;
            List lstArtworkDeatails = new ArrayList();
            objArtworkAction = new ArtworkAction();
            lstArtworkDeatails = objArtworkAction.lstImportArtwork(objArtwork);
            if(lstArtworkDeatails.size()==0){
                GP_container.getChildren().add(new Text(objDictionaryAction.getWord("ARTWORK")+" - "+objDictionaryAction.getWord("NOVALUE")));
            }else{  
                for (int i=0, j = lstArtworkDeatails.size(), k=0; i<j;i++,k+=3){
                    lstArtwork = (ArrayList)lstArtworkDeatails.get(i);

                    byte[] bytArtworkThumbnil = (byte[])lstArtwork.get(2);
                    SeekableStream stream = new ByteArraySeekableStream(bytArtworkThumbnil);
                    String[] names = ImageCodec.getDecoderNames(stream);
                    ImageDecoder dec = ImageCodec.createImageDecoder(names[0], stream, null);
                    RenderedImage im = dec.decodeAsRenderedImage();
                    BufferedImage bufferedImage = PlanarImage.wrapRenderedImage(im).getAsBufferedImage();
                    Image image=SwingFXUtils.toFXImage(bufferedImage, null);
                    ImageView ivArtwork = new ImageView(image);
                    ivArtwork.setFitHeight(222);
                    ivArtwork.setFitWidth(222);
                    GP_container.add(ivArtwork, k%6, i/2);
                    stream.close();
                    
                    String strTempAccess="Private";
                    if(objArtwork.getObjConfiguration().getObjUser().getStrUserID().equals("ADMIN"))
                        strTempAccess = "Private";
                    else
                        strTempAccess = lstArtwork.get(6).toString();
                    final String strAccess = strTempAccess;//lstArtwork.get(6).toString();
                    int intUsage=0;
                    
                    objArtworkAction = new ArtworkAction();
                    intUsage = objArtworkAction.countArtworkUsage(lstArtwork.get(0).toString());
                                        
                    String strTooltip = 
                            objDictionaryAction.getWord("NAME")+": "+lstArtwork.get(1).toString()+"\n"+
                            objDictionaryAction.getWord("ARTWORKLENGTH")+": "+bufferedImage.getHeight()+"\n"+
                            objDictionaryAction.getWord("ARTWORKWIDTH")+": "+bufferedImage.getWidth()+"\n"+
                            objDictionaryAction.getWord("BACKGROUND")+": "+lstArtwork.get(3).toString()+"\n"+
                            objDictionaryAction.getWord("USED")+": "+intUsage+"\n"+
                            objDictionaryAction.getWord("PERMISSION")+": "+lstArtwork.get(6)+"\n"+
                            objDictionaryAction.getWord("BY")+": "+lstArtwork.get(5).toString()+"\n"+
                            objDictionaryAction.getWord("DATE")+": "+lstArtwork.get(4).toString();
                    if(objArtwork.getObjConfiguration().getObjUser().getStrUserID().equals("ADMIN"))
                        intUsage=0;
                    final Label lblArtwork = new Label(strTooltip);
                    lblArtwork.setId(lstArtwork.get(0).toString());
                    lblArtwork.setTooltip(new Tooltip(strTooltip));
                    bufferedImage = null;
                    GP_container.add(lblArtwork, (k+1)%6, i/2);
                    
                    VBox action = new VBox();
                    action.setSpacing(3);
                    Button btnExport = new Button(objDictionaryAction.getWord("EXPORT"));
                    btnExport.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export.png"));
                    btnExport.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPEXPORT")));
                    btnExport.setMaxWidth(Double.MAX_VALUE);
                    btnExport.setDisable(true);
                    if(!strAccess.equalsIgnoreCase("Public")){
                        btnExport.setDisable(false);
                        btnExport.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent e) {
                                System.gc();
                                final Stage dialogStage = new Stage();
                                dialogStage.initStyle(StageStyle.UTILITY);
                                dialogStage.initModality(Modality.APPLICATION_MODAL);
                                dialogStage.setResizable(false);
                                dialogStage.setIconified(false);
                                dialogStage.setFullScreen(false);
                                dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+":"+objDictionaryAction.getWord("ALERT"));
                                BorderPane root = new BorderPane();
                                Scene scene = new Scene(root, 300, 200, Color.WHITE);
                                scene.getStylesheets().add(LibraryView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
                                final GridPane popup=new GridPane();
                                popup.setId("popup");
                                popup.setHgap(5);
                                popup.setVgap(5);
                                popup.setPadding(new Insets(5, 5, 5, 5));
                                Label lblPassword=new Label(objDictionaryAction.getWord("SERVICEPASSWORD"));
                                lblPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                popup.add(lblPassword, 0, 0, 2, 1);
                                final PasswordField passPF =new PasswordField();
                                passPF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                passPF.setPromptText(objDictionaryAction.getWord("PROMPTSERVICEPASSWORD"));
                                popup.add(passPF, 0, 1, 2, 1);
                                final Label lblAlert=new Label("");
                                lblAlert.setStyle("-fx-wrap-text:true;");
                                lblAlert.setPrefWidth(250);
                                popup.add(lblAlert, 0, 3, 2, 1);
                                Button btnYes= new Button(objDictionaryAction.getWord("SUBMIT"));
                                btnYes.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUBMIT")));
                                btnYes.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
                                btnYes.setDefaultButton(true);
                                btnYes.setOnAction(new EventHandler<ActionEvent>(){
                                    @Override
                                    public void handle(ActionEvent event) {
                                        if(Security.SecurePassword(passPF.getText(), objConfiguration.getObjUser().getStrUsername()).equals(objConfiguration.getObjUser().getStrAppPassword())){
                                            dialogStage.close();
                                            System.gc();
                                            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
                                            Date date = new Date();
                                            String currentDate = dateFormat.format(date);
                                            String savePath = System.getProperty("user.dir");

                                            DirectoryChooser directoryChooser=new DirectoryChooser();
                                            libraryStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORT")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
                                            File selectedDirectory = directoryChooser.showDialog(libraryStage);
                                            if(selectedDirectory != null)
                                                savePath = selectedDirectory.getPath();
                                            savePath = savePath+"\\"+objDictionaryAction.getWord("PROJECT")+"_"+currentDate+"\\";
                                            File file = new File(savePath);
                                            if (!file.exists()) {
                                                if (!file.mkdir())
                                                    savePath = System.getProperty("user.dir");
                                            }
                                            savePath = savePath +currentDate+".sql";
                                            file =new File(savePath);
                                            try (BufferedWriter bw = new BufferedWriter(new FileWriter(savePath))) {
                                                String content = new DbUtility().exportEachUserDesign(objConfiguration,lblArtwork.getId().toString());
                                                bw.write(content);
                                                // no need to close it.
                                                bw.close();
                                                if(objConfiguration.getBlnAuthenticateService()){
                                                    ArrayList<File> filesToZip=new ArrayList<>();
                                                    filesToZip.add(file);
                                                    String zipFilePath=file.getAbsolutePath().substring(0, file.getAbsolutePath().indexOf(".sql"))+".bun";
                                                    String passwordToZip = file.getName().substring(0, file.getName().indexOf(".sql"));
                                                    new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                                                    file.delete();
                                                }
                                                lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+savePath);
                                            } catch (IOException ex) {
                                                new Logging("SEVERE",LibraryView.class.getName(),"Export Data"+ex.getMessage(),ex);
                                                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                            } catch (SQLException ex) {
                                                new Logging("SEVERE",LibraryView.class.getName(),"Export Data"+ex.getMessage(),ex);
                                                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                            }
                                            System.gc();
                                        }
                                        else if(passPF.getText().length()==0){
                                            lblAlert.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                                        }
                                        else{
                                            lblAlert.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                                        }
                                    }
                                });
                                popup.add(btnYes, 0, 2, 1, 1);
                                Button btnNo = new Button(objDictionaryAction.getWord("CANCEL"));
                                btnNo.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
                                btnNo.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
                                btnNo.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        dialogStage.close();
                                        System.gc();
                                    }
                                });
                                popup.add(btnNo, 1, 2, 1, 1);
                                root.setCenter(popup);
                                dialogStage.setScene(scene);
                                dialogStage.showAndWait();  
                                System.gc();
                            }
                        });
                    }
                    action.getChildren().add(btnExport);
                    Button btnDelete = new Button(objDictionaryAction.getWord("DELETE"));
                    btnDelete.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/delete.png"));
                    btnDelete.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPDELETE")));
                    btnDelete.setMaxWidth(Double.MAX_VALUE);
                    btnDelete.setDisable(true);
                    if(!strAccess.equalsIgnoreCase("Public") && intUsage==0){
                        btnDelete.setDisable(false);
                        btnDelete.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent e) {
                                System.gc();
                                final Stage dialogStage = new Stage();
                                dialogStage.initStyle(StageStyle.UTILITY);
                                dialogStage.initModality(Modality.APPLICATION_MODAL);
                                dialogStage.setResizable(false);
                                dialogStage.setIconified(false);
                                dialogStage.setFullScreen(false);
                                dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+":"+objDictionaryAction.getWord("ALERT"));
                                BorderPane root = new BorderPane();
                                Scene scene = new Scene(root, 300, 180, Color.WHITE);
                                scene.getStylesheets().add(LibraryView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
                                final GridPane popup=new GridPane();
                                popup.setId("popup");
                                popup.setHgap(5);
                                popup.setVgap(5);
                                popup.setPadding(new Insets(25, 25, 25, 25));
                                popup.add(new ImageView("/media/alert_warning.png"), 0, 0); 
                                final Label lblAlert = new Label(objDictionaryAction.getWord("ALERTDELETE"));
                                lblAlert.setStyle("-fx-wrap-text:true;");
                                lblAlert.setPrefWidth(250);
                                popup.add(lblAlert, 1, 0);
                                
                                Label lblPassword=new Label(objDictionaryAction.getWord("SERVICEPASSWORD"));
                                lblPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                popup.add(lblPassword, 0, 1);
                                final PasswordField passPF= new PasswordField();
                                passPF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                passPF.setPromptText(objDictionaryAction.getWord("PROMPTSERVICEPASSWORD"));
                                popup.add(passPF, 1, 1);
                                
                                Button btnYes = new Button(objDictionaryAction.getWord("SUBMIT"));
                                btnYes.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUBMIT")));
                                btnYes.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
                                btnYes.setDefaultButton(true);
                                btnYes.setWrapText(false);
                                btnYes.setMinWidth(100);
                                btnYes.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        if(passPF.getText().length()==0){
                                            lblAlert.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                                        }
                                        else{
                                            if(Security.SecurePassword(passPF.getText(), objConfiguration.getObjUser().getStrUsername()).equals(objConfiguration.getObjUser().getStrAppPassword())){
                                                try {
                                                    lblStatus.setText(lblArtwork.getId()+" : "+objDictionaryAction.getWord("ACTIONDELETE"));
                                                    objArtworkAction = new ArtworkAction();
                                                    objArtworkAction.clearArtwork(lblArtwork.getId().toString());
                                                    populateLibraryArtwork();
                                                    lblStatus.setText(lblArtwork.getId()+" : "+objDictionaryAction.getWord("DATADELETED"));                                         
                                                } catch (SQLException ex) {
                                                    new Logging("SEVERE",LibraryView.class.getName(),"Export Data"+ex.getMessage(),ex);
                                                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                                } catch (Exception ex) {
                                                    new Logging("SEVERE",LibraryView.class.getName(),"Export Data"+ex.getMessage(),ex);
                                                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                                }
                                                dialogStage.close();
                                            }
                                            else{
                                                lblAlert.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                                            }
                                        }
                                        System.gc();
                                    }
                                });
                                popup.add(btnYes, 0, 3);
                                Button btnNo = new Button(objDictionaryAction.getWord("CANCEL"));
                                btnNo.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
                                btnNo.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
                                btnNo.setWrapText(false);
                                btnNo.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        dialogStage.close();
                                        System.gc();
                                    }
                                });
                                popup.add(btnNo, 1, 3);
                                root.setCenter(popup);
                                dialogStage.setScene(scene);
                                dialogStage.showAndWait();  
                                e.consume();
                                System.gc();
                            }
                        });
                    }
                    action.getChildren().add(btnDelete);
                    Button btnExportDelete = new Button(objDictionaryAction.getWord("EXPORTDELETE"));
                    btnExportDelete.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_delete.png"));
                    btnExportDelete.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPEXPORTDELETE")));
                    btnExportDelete.setMaxWidth(Double.MAX_VALUE);
                    btnExportDelete.setDisable(true);
                    if(!strAccess.equalsIgnoreCase("Public") && intUsage==0){
                        btnExportDelete.setDisable(false);
                        btnExportDelete.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent e) {
                                System.gc();
                                final Stage dialogStage = new Stage();
                                dialogStage.initStyle(StageStyle.UTILITY);
                                dialogStage.initModality(Modality.APPLICATION_MODAL);
                                dialogStage.setResizable(false);
                                dialogStage.setIconified(false);
                                dialogStage.setFullScreen(false);
                                dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+":"+objDictionaryAction.getWord("ALERT"));
                                BorderPane root = new BorderPane();
                                Scene scene = new Scene(root, 300, 200, Color.WHITE);
                                scene.getStylesheets().add(LibraryView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
                                final GridPane popup=new GridPane();
                                popup.setId("popup");
                                popup.setHgap(5);
                                popup.setVgap(5);
                                popup.setPadding(new Insets(5, 5, 5, 5));
                                Label lblPassword=new Label(objDictionaryAction.getWord("SERVICEPASSWORD"));
                                lblPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                popup.add(lblPassword, 0, 0, 2, 1);
                                final PasswordField passPF =new PasswordField();
                                passPF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                passPF.setPromptText(objDictionaryAction.getWord("PROMPTSERVICEPASSWORD"));
                                popup.add(passPF, 0, 1, 2, 1);
                                final Label lblAlert=new Label("");
                                lblAlert.setStyle("-fx-wrap-text:true;");
                                lblAlert.setPrefWidth(250);
                                popup.add(lblAlert, 0, 3, 2, 1);
                                Button btnYes = new Button(objDictionaryAction.getWord("SUBMIT"));
                                btnYes.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUBMIT")));
                                btnYes.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
                                btnYes.setDefaultButton(true);
                                btnYes.setOnAction(new EventHandler<ActionEvent>(){
                                    @Override
                                    public void handle(ActionEvent event) {
                                        if(Security.SecurePassword(passPF.getText(), objConfiguration.getObjUser().getStrUsername()).equals(objConfiguration.getObjUser().getStrAppPassword())){
                                            dialogStage.close();
                                            System.gc();
                                            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
                                            Date date = new Date();
                                            String currentDate = dateFormat.format(date);
                                            String savePath = System.getProperty("user.dir");

                                            DirectoryChooser directoryChooser=new DirectoryChooser();
                                            libraryStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORT")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
                                            File selectedDirectory = directoryChooser.showDialog(libraryStage);
                                            if(selectedDirectory != null)
                                                savePath = selectedDirectory.getPath();
                                            savePath = savePath+"\\"+objDictionaryAction.getWord("PROJECT")+"_"+currentDate+"\\";
                                            File file = new File(savePath);
                                            if (!file.exists()) {
                                                if (!file.mkdir())
                                                    savePath = System.getProperty("user.dir");
                                            }
                                            savePath = savePath +currentDate+".sql";
                                            file =new File(savePath);
                                            try (BufferedWriter bw = new BufferedWriter(new FileWriter(savePath))) {
                                                String content = new DbUtility().exportEachUserDesign(objConfiguration,lblArtwork.getId().toString());
                                                bw.write(content);
                                                // no need to close it.
                                                //bw.close();
                                                if(objConfiguration.getBlnAuthenticateService()){
                                                    ArrayList<File> filesToZip=new ArrayList<>();
                                                    filesToZip.add(file);
                                                    String zipFilePath=file.getAbsolutePath().substring(0, file.getAbsolutePath().indexOf(".sql"))+".bun";
                                                    String passwordToZip = file.getName().substring(0, file.getName().indexOf(".sql"));
                                                    new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                                                    file.delete();
                                                }
                                                lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+savePath);
                                                objArtworkAction = new ArtworkAction();
                                                objArtworkAction.clearArtwork(lblArtwork.getId().toString());
                                                populateLibraryArtwork();
                                            } catch (IOException ex) {
                                                new Logging("SEVERE",LibraryView.class.getName(),"Export Data"+ex.getMessage(),ex);
                                                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                            } catch (SQLException ex) {
                                                new Logging("SEVERE",LibraryView.class.getName(),"Export Data"+ex.getMessage(),ex);
                                                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                            }
                                            System.gc();
                                        }
                                        else if(passPF.getText().length()==0){
                                            lblAlert.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                                        }
                                        else{
                                            lblAlert.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                                        }
                                    }
                                });
                                popup.add(btnYes, 0, 2, 1, 1);
                                Button btnNo = new Button(objDictionaryAction.getWord("CANCEL"));
                                btnNo.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
                                btnNo.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
                                btnNo.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        dialogStage.close();
                                        System.gc();
                                    }
                                });
                                popup.add(btnNo, 1, 2, 1, 1);
                                root.setCenter(popup);
                                dialogStage.setScene(scene);
                                dialogStage.showAndWait();  
                                System.gc();
                            }
                        });
                    }
                    action.getChildren().add(btnExportDelete);
                    Button btnPermission = new Button(objDictionaryAction.getWord("CHANGEPERMISSION"));
                    btnPermission.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/settings.png"));
                    btnPermission.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCHANGEPERMISSION")));
                    btnPermission.setMaxWidth(Double.MAX_VALUE);
                    btnPermission.setDisable(true);
                    if(!strAccess.equalsIgnoreCase("Public")){
                        btnPermission.setDisable(false);
                        btnPermission.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent e) {
                                final Stage dialogStage = new Stage();
                                dialogStage.initStyle(StageStyle.UTILITY);
                                dialogStage.initModality(Modality.APPLICATION_MODAL);
                                dialogStage.setResizable(false);
                                dialogStage.setIconified(false);
                                dialogStage.setFullScreen(false);
                                dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+":"+objDictionaryAction.getWord("ALERT"));
                                BorderPane root = new BorderPane();
                                Scene scene = new Scene(root, 300, 180, Color.WHITE);
                                scene.getStylesheets().add(LibraryView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
                                final GridPane popup=new GridPane();
                                popup.setId("popup");
                                popup.setHgap(5);
                                popup.setVgap(5);
                                popup.setPadding(new Insets(25, 25, 25, 25));
                                
                                Label lblData = new Label(objDictionaryAction.getWord("SELECT")+" "+objDictionaryAction.getWord("PERMISSION"));
                                lblData.setStyle("-fx-wrap-text:true;");
                                lblData.setPrefWidth(250);
                                popup.add(lblData, 0, 0, 3, 1);
                                final ToggleGroup dataTG = new ToggleGroup();
                                RadioButton dataPublicRB = new RadioButton(objDictionaryAction.getWord("PUBLIC"));
                                dataPublicRB.setToggleGroup(dataTG);
                                dataPublicRB.setUserData("Public");
                                popup.add(dataPublicRB, 0, 1);
                                RadioButton dataPrivateRB = new RadioButton(objDictionaryAction.getWord("PRIVATE"));
                                dataPrivateRB.setToggleGroup(dataTG);
                                dataPrivateRB.setUserData("Private");
                                popup.add(dataPrivateRB, 1, 1);
                                RadioButton dataProtectedRB = new RadioButton(objDictionaryAction.getWord("PROTECTED"));
                                dataProtectedRB.setToggleGroup(dataTG);
                                dataProtectedRB.setUserData("Protected");
                                popup.add(dataProtectedRB, 2, 1);
                                if(strAccess.equalsIgnoreCase("Public"))
                                    dataTG.selectToggle(dataPublicRB);
                                else if(strAccess.equalsIgnoreCase("Private"))
                                    dataTG.selectToggle(dataPrivateRB);
                                else 
                                    dataTG.selectToggle(dataProtectedRB);
                                
                                final Label lblPassword=new Label(objDictionaryAction.getWord("SERVICEPASSWORD"));
                                lblPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                popup.add(lblPassword, 0, 2, 1, 1);
                                final PasswordField passPF=new PasswordField();
                                passPF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                passPF.setPromptText(objDictionaryAction.getWord("PROMPTSERVICEPASSWORD"));
                                popup.add(passPF, 1, 2, 2, 1);
                                                                
                                final Label lblAlert = new Label(objDictionaryAction.getWord("ALERTWHAT"));
                                lblAlert.setStyle("-fx-wrap-text:true;");
                                lblAlert.setPrefWidth(250);
                                popup.add(lblAlert, 0, 4, 3, 1);
                                
                                Button btnYes = new Button(objDictionaryAction.getWord("SUBMIT"));
                                btnYes.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUBMIT")));
                                btnYes.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
                                btnYes.setDefaultButton(true);
                                btnYes.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        if(passPF.getText().length()==0){
                                            lblAlert.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                                        }
                                        else{
                                            if(Security.SecurePassword(passPF.getText(), objConfiguration.getObjUser().getStrUsername()).equals(objConfiguration.getObjUser().getStrAppPassword())){
                                                try {
                                                    String strAccessNew = dataTG.getSelectedToggle().getUserData().toString();
                                                    System.err.println(strAccessNew);
                                                    strAccessNew = new IDGenerator().setUserAcessValueData("ARTWORK_LIBRARY",strAccessNew);
                                                    objArtworkAction = new ArtworkAction();
                                                    objArtworkAction.resetArtworkPermission(lblArtwork.getId(),strAccessNew);
                                                    dialogStage.close();                                            
                                                    populateLibraryArtwork();
                                                } catch (SQLException ex) {
                                                    new Logging("SEVERE",LibraryView.class.getName(),"Change Permission Data"+ex.getMessage(),ex);
                                                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                                }
                                                System.gc();
                                            }
                                            else{
                                                lblAlert.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                                            }
                                        }
                                        System.gc();
                                    }
                                });
                                popup.add(btnYes, 0, 3);
                                Button btnNo = new Button(objDictionaryAction.getWord("CANCEL"));
                                btnNo.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
                                btnNo.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
                                btnNo.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        dialogStage.close();
                                        System.gc();
                                    }
                                });
                                popup.add(btnNo, 2, 3);
                                root.setCenter(popup);
                                dialogStage.setScene(scene);
                                dialogStage.showAndWait();  
                                e.consume();
                                System.gc();
                            }
                        });
                    }
                    action.getChildren().add(btnPermission);
                    Button btnUpdate;
                    if(!strAccess.equalsIgnoreCase("Public") && intUsage==0){
                        btnUpdate = new Button(objDictionaryAction.getWord("UPDATE"));
                        btnUpdate.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/update.png"));
                        btnUpdate.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPUPDATE")));
                    }else{
                        btnUpdate = new Button(objDictionaryAction.getWord("COPY"));
                        btnUpdate.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/copy.png"));
                        btnUpdate.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCOPY")));
                    }
                    btnUpdate.setMaxWidth(Double.MAX_VALUE);
                    btnUpdate.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent e) {
                            if(strAccess.equalsIgnoreCase("Public")){
                                objConfiguration.setServicePasswordValid(true);
                            } else{
                                new MessageView(objConfiguration);
                            }
                            if(objConfiguration.getServicePasswordValid()){
                                objConfiguration.setServicePasswordValid(false);
                                objConfiguration.strWindowFlowContext = "Dashboard";
                                objConfiguration.setStrRecentArtwork(lblArtwork.getId());
                                libraryStage.close();
                                System.gc();
                                ArtworkView objArtworkView = new ArtworkView(objConfiguration);
                                System.gc();
                            }
                        }
                    });
                    action.getChildren().add(btnUpdate);
                    
                    Button btnFabric = new Button(objDictionaryAction.getWord("FABRICEDITORUTILITY"));
                    btnFabric.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fabric_editor.png"));
                    btnFabric.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPFABRICEDITORUTILITY")));
                    btnFabric.setMaxWidth(Double.MAX_VALUE);
                    btnFabric.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent e) {
                                System.gc();
                                final Stage dialogStage = new Stage();
                                dialogStage.initStyle(StageStyle.UTILITY);
                                dialogStage.initModality(Modality.APPLICATION_MODAL);
                                dialogStage.setResizable(false);
                                dialogStage.setIconified(false);
                                dialogStage.setFullScreen(false);
                                dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+":"+objDictionaryAction.getWord("ALERT"));
                                BorderPane root = new BorderPane();
                                Scene scene = new Scene(root, 300, 200, Color.WHITE);
                                scene.getStylesheets().add(LibraryView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
                                final GridPane popup=new GridPane();
                                popup.setId("popup");
                                popup.setHgap(5);
                                popup.setVgap(5);
                                popup.setPadding(new Insets(5, 5, 5, 5));
                                Label lblPassword=new Label(objDictionaryAction.getWord("SERVICEPASSWORD"));
                                lblPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                popup.add(lblPassword, 0, 0, 2, 1);
                                final PasswordField passPF =new PasswordField();
                                passPF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                passPF.setPromptText(objDictionaryAction.getWord("PROMPTSERVICEPASSWORD"));
                                popup.add(passPF, 0, 1, 2, 1);
                                final Label lblAlert=new Label("");
                                lblAlert.setStyle("-fx-wrap-text:true;");
                                lblAlert.setPrefWidth(250);
                                popup.add(lblAlert, 0, 3, 2, 1);
                                Button btnYes= new Button(objDictionaryAction.getWord("SUBMIT"));
                                btnYes.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUBMIT")));
                                btnYes.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
                                btnYes.setDefaultButton(true);
                                btnYes.setOnAction(new EventHandler<ActionEvent>(){
                                    @Override
                                    public void handle(ActionEvent event) {
                                        if(Security.SecurePassword(passPF.getText(), objConfiguration.getObjUser().getStrUsername()).equals(objConfiguration.getObjUser().getStrAppPassword())){
                                            System.gc();
                                            objConfiguration.strWindowFlowContext = "ArtworkEditor";
                                            objConfiguration.setStrRecentArtwork(lblArtwork.getId().toString());
                                            libraryStage.close();
                                            FabricView objFabricView = new FabricView(objConfiguration);
                                            System.gc();
                                        }
                                        else if(passPF.getText().length()==0){
                                            lblAlert.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                                        }
                                        else{
                                            lblAlert.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                                        }
                                    }
                                });
                                popup.add(btnYes, 0, 2);
                                Button btnNo = new Button(objDictionaryAction.getWord("CANCEL"));
                                btnNo.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
                                btnNo.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
                                btnNo.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        dialogStage.close();
                                        System.gc();
                                    }
                                });
                                popup.add(btnNo, 1, 2);
                                root.setCenter(popup);
                                dialogStage.setScene(scene);
                                dialogStage.showAndWait();  
                                System.gc();
                            }
                        });
                    action.getChildren().add(btnFabric);
                    
                    GP_container.add(action, (k+2)%6, i/2);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(LibraryView.class.getName()).log(Level.SEVERE, null, ex);           
            
            new Logging("SEVERE",LibraryView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    /*Weave Library*/
    private void populateLibraryWeave(){
        try{
            GP_container.getChildren().clear();
            List lstWeave=null;
            List lstWeaveDeatails = new ArrayList();
            objWeaveAction = new WeaveAction(objWeave,true);
            lstWeaveDeatails = objWeaveAction.lstImportWeave(objWeave);
            if(lstWeaveDeatails.size()==0){
                GP_container.getChildren().add(new Text(objDictionaryAction.getWord("WEAVE")+" - "+objDictionaryAction.getWord("NOVALUE")));
            }else{            
                //System.err.println(lstWeaveDeatails.size());
                for (int i=0, k=0; i<lstWeaveDeatails.size();i++, k+=3){
                    lstWeave = (ArrayList)lstWeaveDeatails.get(i);

                    byte[] bytWeaveThumbnil = (byte[])lstWeave.get(2);
                    SeekableStream stream = new ByteArraySeekableStream(bytWeaveThumbnil);
                    String[] names = ImageCodec.getDecoderNames(stream);
                    ImageDecoder dec = ImageCodec.createImageDecoder(names[0], stream, null);
                    RenderedImage im = dec.decodeAsRenderedImage();
                    BufferedImage bufferedImage = PlanarImage.wrapRenderedImage(im).getAsBufferedImage();
                    Image image=SwingFXUtils.toFXImage(bufferedImage, null);
                    ImageView ivWeave = new ImageView(image);
                    ivWeave.setFitHeight(222);
                    ivWeave.setFitWidth(222);
                    stream.close();
                    bufferedImage = null;
                    GP_container.add(ivWeave, k%6, i/2);
                    
                    String strTempAccess="Private";
                    if(objWeave.getObjConfiguration().getObjUser().getStrUserID().equals("ADMIN"))
                        strTempAccess = "Private";
                    else
                        strTempAccess = new IDGenerator().getUserAcessValueData("WEAVE_LIBRARY",lstWeave.get(15).toString());
                    final String strAccess = strTempAccess;//new IDGenerator().getUserAcessValueData("WEAVE_LIBRARY",lstWeave.get(15).toString());
                    int intUsage=0;
                    
                    objWeaveAction = new WeaveAction();
                    intUsage = objWeaveAction.countWeaveUsage(lstWeave.get(0).toString());
                    
                    String strTooltip = 
                                objDictionaryAction.getWord("NAME")+": "+lstWeave.get(1).toString()+"\n"+
                                objDictionaryAction.getWord("SHAFT")+": "+lstWeave.get(7).toString()+"\n"+
                                objDictionaryAction.getWord("TRADELES")+": "+lstWeave.get(8).toString()+"\n"+
                                objDictionaryAction.getWord("WEFTREPEAT")+": "+lstWeave.get(9).toString()+"\n"+
                                objDictionaryAction.getWord("WARPREPEAT")+": "+lstWeave.get(10).toString()+"\n"+
                                objDictionaryAction.getWord("WEFTFLOAT")+": "+lstWeave.get(11).toString()+"\n"+
                                objDictionaryAction.getWord("WARPFLOAT")+": "+lstWeave.get(12).toString()+"\n"+
                                objDictionaryAction.getWord("WEAVECATEGORY")+": "+lstWeave.get(4).toString()+"\n"+
                                objDictionaryAction.getWord("WEAVETYPE")+": "+lstWeave.get(3).toString()+"\n"+
                                objDictionaryAction.getWord("ISLIFTPLAN")+": "+lstWeave.get(5).toString()+"\n"+
                                objDictionaryAction.getWord("ISCOLOR")+": "+lstWeave.get(6).toString()+"\n"+
                                objDictionaryAction.getWord("USED")+": "+intUsage+"\n"+
                                objDictionaryAction.getWord("PERMISSION")+": "+new IDGenerator().getUserAcessValueData("WEAVE_LIBRARY",lstWeave.get(15).toString())+"\n"+
                                objDictionaryAction.getWord("BY")+": "+lstWeave.get(14).toString()+"\n"+
                                objDictionaryAction.getWord("DATE")+": "+lstWeave.get(13).toString();
                    if(objWeave.getObjConfiguration().getObjUser().getStrUserID().equals("ADMIN"))
                        intUsage=0;
                    final Label lblWeave = new Label(strTooltip);
                    lblWeave.setId(lstWeave.get(0).toString());
                    lblWeave.setTooltip(new Tooltip(strTooltip));
                    GP_container.add(lblWeave, (k+1)%6, i/2);
                    
                    VBox action = new VBox();
                    action.setSpacing(3);
                    Button btnExport = new Button(objDictionaryAction.getWord("EXPORT"));
                    btnExport.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export.png"));
                    btnExport.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPEXPORT")));
                    btnExport.setMaxWidth(Double.MAX_VALUE);
                    btnExport.setDisable(true);
                    if(!strAccess.equalsIgnoreCase("Public")){
                        btnExport.setDisable(false);
                        btnExport.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent e) {
                                //
                                System.gc();
                                final Stage dialogStage = new Stage();
                                dialogStage.initStyle(StageStyle.UTILITY);
                                dialogStage.initModality(Modality.APPLICATION_MODAL);
                                dialogStage.setResizable(false);
                                dialogStage.setIconified(false);
                                dialogStage.setFullScreen(false);
                                dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+":"+objDictionaryAction.getWord("ALERT"));
                                BorderPane root = new BorderPane();
                                Scene scene = new Scene(root, 300, 200, Color.WHITE);
                                scene.getStylesheets().add(LibraryView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
                                final GridPane popup=new GridPane();
                                popup.setId("popup");
                                popup.setHgap(5);
                                popup.setVgap(5);
                                popup.setPadding(new Insets(5, 5, 5, 5));
                                Label lblPassword=new Label(objDictionaryAction.getWord("SERVICEPASSWORD"));
                                lblPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                popup.add(lblPassword, 0, 0, 2, 1);
                                final PasswordField passPF =new PasswordField();
                                passPF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                passPF.setPromptText(objDictionaryAction.getWord("PROMPTSERVICEPASSWORD"));
                                popup.add(passPF, 0, 1, 2, 1);
                                final Label lblAlert=new Label("");
                                lblAlert.setStyle("-fx-wrap-text:true;");
                                lblAlert.setPrefWidth(250);
                                popup.add(lblAlert, 0, 3, 2, 1);
                                Button btnYes= new Button(objDictionaryAction.getWord("SUBMIT"));
                                btnYes.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUBMIT")));
                                btnYes.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
                                btnYes.setDefaultButton(true);
                                btnYes.setOnAction(new EventHandler<ActionEvent>(){
                                    @Override
                                    public void handle(ActionEvent event) {
                                        if(Security.SecurePassword(passPF.getText(), objConfiguration.getObjUser().getStrUsername()).equals(objConfiguration.getObjUser().getStrAppPassword())){
                                            System.gc();
                                            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
                                            Date date = new Date();
                                            String currentDate = dateFormat.format(date);
                                            String savePath = System.getProperty("user.dir");

                                            DirectoryChooser directoryChooser=new DirectoryChooser();
                                            libraryStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORT")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
                                            File selectedDirectory = directoryChooser.showDialog(libraryStage);
                                            if(selectedDirectory != null)
                                                savePath = selectedDirectory.getPath();
                                            savePath = savePath+"\\"+objDictionaryAction.getWord("PROJECT")+"_"+currentDate+"\\";
                                            File file = new File(savePath);
                                            if (!file.exists()) {
                                                if (!file.mkdir())
                                                    savePath = System.getProperty("user.dir");
                                            }
                                            savePath = savePath+currentDate+".sql";
                                            file =new File(savePath);
                                            try (BufferedWriter bw = new BufferedWriter(new FileWriter(savePath))) {
                                                String content = new DbUtility().exportEachUserWeave(objConfiguration,lblWeave.getId().toString());
                                                bw.write(content);
                                                // no need to close it.
                                                bw.close();
                                                if(objConfiguration.getBlnAuthenticateService()){
                                                    ArrayList<File> filesToZip=new ArrayList<>();
                                                    filesToZip.add(file);
                                                    String zipFilePath=file.getAbsolutePath().substring(0, file.getAbsolutePath().indexOf(".sql"))+".bun";
                                                    String passwordToZip = file.getName().substring(0, file.getName().indexOf(".sql"));
                                                    new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                                                    file.delete();
                                                }
                                                lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+savePath);
                                            } catch (IOException ex) {
                                                new Logging("SEVERE",LibraryView.class.getName(),"Export Data"+ex.getMessage(),ex);
                                                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                            } catch (SQLException ex) {
                                                new Logging("SEVERE",LibraryView.class.getName(),"Export Data"+ex.getMessage(),ex);
                                                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                            }
                                            System.gc();
                                        }
                                        else if(passPF.getText().length()==0){
                                            lblAlert.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                                        }
                                        else{
                                            lblAlert.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                                        }
                                    }
                                });
                                popup.add(btnYes, 0, 2, 1, 1);
                                Button btnNo = new Button(objDictionaryAction.getWord("CANCEL"));
                                btnNo.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
                                btnNo.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
                                btnNo.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        dialogStage.close();
                                        System.gc();
                                    }
                                });
                                popup.add(btnNo, 1, 2, 1, 1);
                                root.setCenter(popup);
                                dialogStage.setScene(scene);
                                dialogStage.showAndWait();  
                                System.gc();
                            }
                        });
                    }
                    action.getChildren().add(btnExport);
                    Button btnDelete = new Button(objDictionaryAction.getWord("DELETE"));
                    btnDelete.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/delete.png"));
                    btnDelete.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPDELETE")));
                    btnDelete.setMaxWidth(Double.MAX_VALUE);
                    btnDelete.setDisable(true);
                    if(!strAccess.equalsIgnoreCase("Public") && intUsage==0){
                        btnDelete.setDisable(false);
                        btnDelete.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent e) {
                                System.gc();
                                final Stage dialogStage = new Stage();
                                dialogStage.initStyle(StageStyle.UTILITY);
                                dialogStage.initModality(Modality.APPLICATION_MODAL);
                                dialogStage.setResizable(false);
                                dialogStage.setIconified(false);
                                dialogStage.setFullScreen(false);
                                dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+":"+objDictionaryAction.getWord("ALERT"));
                                BorderPane root = new BorderPane();
                                Scene scene = new Scene(root, 300, 180, Color.WHITE);
                                scene.getStylesheets().add(LibraryView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
                                final GridPane popup=new GridPane();
                                popup.setId("popup");
                                popup.setHgap(5);
                                popup.setVgap(5);
                                popup.setPadding(new Insets(25, 25, 25, 25));
                                popup.add(new ImageView("/media/alert_warning.png"), 0, 0); 
                                final Label lblAlert = new Label(objDictionaryAction.getWord("ALERTDELETE"));
                                lblAlert.setStyle("-fx-wrap-text:true;");
                                lblAlert.setPrefWidth(250);
                                popup.add(lblAlert, 1, 0);
                                
                                Label lblPassword=new Label(objDictionaryAction.getWord("SERVICEPASSWORD"));
                                lblPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                popup.add(lblPassword, 0, 1);
                                final PasswordField passPF= new PasswordField();
                                passPF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                passPF.setPromptText(objDictionaryAction.getWord("PROMPTSERVICEPASSWORD"));
                                popup.add(passPF, 1, 1);
                                
                                Button btnYes = new Button(objDictionaryAction.getWord("SUBMIT"));
                                btnYes.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUBMIT")));
                                btnYes.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
                                btnYes.setDefaultButton(true);
                                btnYes.setWrapText(false);
                                btnYes.setMinWidth(100);
                                btnYes.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        if(passPF.getText().length()==0){
                                            lblAlert.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                                        }
                                        else{
                                            if(Security.SecurePassword(passPF.getText(), objConfiguration.getObjUser().getStrUsername()).equals(objConfiguration.getObjUser().getStrAppPassword())){
                                                try { 
                                                    lblStatus.setText(lblWeave.getId()+" : "+objDictionaryAction.getWord("ACTIONDELETE"));
                                                    objWeaveAction = new WeaveAction();
                                                    objWeaveAction.clearWeave(lblWeave.getId().toString());
                                                    populateLibraryWeave();
                                                    lblStatus.setText(lblWeave.getId()+" : "+objDictionaryAction.getWord("DATADELETED"));
                                                } catch (SQLException ex) {
                                                    new Logging("SEVERE",LibraryView.class.getName(),"Export Data"+ex.getMessage(),ex);
                                                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                                } catch (Exception ex) {
                                                    new Logging("SEVERE",LibraryView.class.getName(),"Export Data"+ex.getMessage(),ex);
                                                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                                }
                                                dialogStage.close();
                                                System.gc();
                                            }
                                            else{
                                                lblAlert.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                                            }
                                        }
                                        System.gc();
                                    }
                                });
                                popup.add(btnYes, 0, 3);
                                Button btnNo = new Button(objDictionaryAction.getWord("CANCEL"));
                                btnNo.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
                                btnNo.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
                                btnNo.setWrapText(false);
                                btnNo.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        dialogStage.close();
                                        System.gc();
                                    }
                                });
                                popup.add(btnNo, 1, 3);
                                root.setCenter(popup);
                                dialogStage.setScene(scene);
                                dialogStage.showAndWait();  
                                e.consume();
                                System.gc();
                            }
                        });
                    }
                    action.getChildren().add(btnDelete);
                    Button btnExportDelete = new Button(objDictionaryAction.getWord("EXPORTDELETE"));
                    btnExportDelete.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/export_delete.png"));
                    btnExportDelete.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPEXPORTDELETE")));
                    btnExportDelete.setMaxWidth(Double.MAX_VALUE);
                    btnExportDelete.setDisable(true);
                    if(!strAccess.equalsIgnoreCase("Public") && intUsage==0){
                        btnExportDelete.setDisable(false);
                        btnExportDelete.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent e) {
                                System.gc();
                                final Stage dialogStage = new Stage();
                                dialogStage.initStyle(StageStyle.UTILITY);
                                dialogStage.initModality(Modality.APPLICATION_MODAL);
                                dialogStage.setResizable(false);
                                dialogStage.setIconified(false);
                                dialogStage.setFullScreen(false);
                                dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+":"+objDictionaryAction.getWord("ALERT"));
                                BorderPane root = new BorderPane();
                                Scene scene = new Scene(root, 300, 200, Color.WHITE);
                                scene.getStylesheets().add(LibraryView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
                                final GridPane popup=new GridPane();
                                popup.setId("popup");
                                popup.setHgap(5);
                                popup.setVgap(5);
                                popup.setPadding(new Insets(5, 5, 5, 5));
                                Label lblPassword=new Label(objDictionaryAction.getWord("SERVICEPASSWORD"));
                                lblPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                popup.add(lblPassword, 0, 0, 2, 1);
                                final PasswordField passPF =new PasswordField();
                                passPF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                passPF.setPromptText(objDictionaryAction.getWord("PROMPTSERVICEPASSWORD"));
                                popup.add(passPF, 0, 1, 2, 1);
                                final Label lblAlert=new Label("");
                                lblAlert.setStyle("-fx-wrap-text:true;");
                                lblAlert.setPrefWidth(250);
                                popup.add(lblAlert, 0, 3, 2, 1);
                                Button btnYes = new Button(objDictionaryAction.getWord("SUBMIT"));
                                btnYes.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUBMIT")));
                                btnYes.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
                                btnYes.setDefaultButton(true);
                                btnYes.setOnAction(new EventHandler<ActionEvent>(){
                                    @Override
                                    public void handle(ActionEvent event) {
                                        if(Security.SecurePassword(passPF.getText(), objConfiguration.getObjUser().getStrUsername()).equals(objConfiguration.getObjUser().getStrAppPassword())){
                                            System.gc();
                                            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
                                            Date date = new Date();
                                            String currentDate = dateFormat.format(date);
                                            String savePath = System.getProperty("user.dir");

                                            DirectoryChooser directoryChooser=new DirectoryChooser();
                                            libraryStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWEXPORT")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
                                            File selectedDirectory = directoryChooser.showDialog(libraryStage);
                                            if(selectedDirectory != null)
                                                savePath = selectedDirectory.getPath();
                                            savePath = savePath+"\\"+objDictionaryAction.getWord("PROJECT")+"_"+currentDate+"\\";
                                            File file = new File(savePath);
                                            if (!file.exists()) {
                                                if (!file.mkdir())
                                                    savePath = System.getProperty("user.dir");
                                            }
                                            savePath = savePath +currentDate+".sql";
                                            file =new File(savePath);
                                            try (BufferedWriter bw = new BufferedWriter(new FileWriter(savePath))) {
                                                String content = new DbUtility().exportEachUserWeave(objConfiguration,lblWeave.getId().toString());
                                                bw.write(content);
                                                // no need to close it.
                                                //bw.close();
                                                if(objConfiguration.getBlnAuthenticateService()){
                                                    ArrayList<File> filesToZip=new ArrayList<>();
                                                    filesToZip.add(file);
                                                    String zipFilePath=file.getAbsolutePath().substring(0, file.getAbsolutePath().indexOf(".sql"))+".bun";
                                                    String passwordToZip = file.getName().substring(0, file.getName().indexOf(".sql"));
                                                    new EncryptZip(zipFilePath, filesToZip, passwordToZip);
                                                    file.delete();
                                                }
                                                lblStatus.setText(objDictionaryAction.getWord("EXPORTEDTO")+" "+savePath);
                                                objWeaveAction = new WeaveAction();
                                                objWeaveAction.clearWeave(lblWeave.getId().toString());
                                                populateLibraryWeave();
                                            } catch (IOException ex) {
                                                new Logging("SEVERE",LibraryView.class.getName(),"Export Data"+ex.getMessage(),ex);
                                                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                            } catch (SQLException ex) {
                                                new Logging("SEVERE",LibraryView.class.getName(),"Export Data"+ex.getMessage(),ex);
                                                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                            }
                                            System.gc();
                                        }
                                        else if(passPF.getText().length()==0){
                                            lblAlert.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                                        }
                                        else{
                                            lblAlert.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                                        }
                                    }
                                });
                                popup.add(btnYes, 0, 2, 1, 1);
                                Button btnNo = new Button(objDictionaryAction.getWord("CANCEL"));
                                btnNo.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
                                btnNo.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
                                btnNo.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        dialogStage.close();
                                        System.gc();
                                    }
                                });
                                popup.add(btnNo, 1, 2, 1, 1);
                                root.setCenter(popup);
                                dialogStage.setScene(scene);
                                dialogStage.showAndWait();  
                                System.gc();
                            }
                        });
                    }
                    action.getChildren().add(btnExportDelete);
                    Button btnPermission = new Button(objDictionaryAction.getWord("CHANGEPERMISSION"));
                    btnPermission.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/settings.png"));
                    btnPermission.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCHANGEPERMISSION")));
                    btnPermission.setMaxWidth(Double.MAX_VALUE);
                    btnPermission.setDisable(true);
                    if(!strAccess.equalsIgnoreCase("Public")){
                        btnPermission.setDisable(false);
                        btnPermission.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent e) {
                                final Stage dialogStage = new Stage();
                                dialogStage.initStyle(StageStyle.UTILITY);
                                dialogStage.initModality(Modality.APPLICATION_MODAL);
                                dialogStage.setResizable(false);
                                dialogStage.setIconified(false);
                                dialogStage.setFullScreen(false);
                                dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+":"+objDictionaryAction.getWord("ALERT"));
                                BorderPane root = new BorderPane();
                                Scene scene = new Scene(root, 300, 180, Color.WHITE);
                                scene.getStylesheets().add(LibraryView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
                                final GridPane popup=new GridPane();
                                popup.setId("popup");
                                popup.setHgap(5);
                                popup.setVgap(5);
                                popup.setPadding(new Insets(25, 25, 25, 25));
                                
                                Label lblData = new Label(objDictionaryAction.getWord("SELECT")+" "+objDictionaryAction.getWord("PERMISSION"));
                                lblData.setStyle("-fx-wrap-text:true;");
                                lblData.setPrefWidth(250);
                                popup.add(lblData, 0, 0, 3, 1);
                                final ToggleGroup dataTG = new ToggleGroup();
                                RadioButton dataPublicRB = new RadioButton(objDictionaryAction.getWord("PUBLIC"));
                                dataPublicRB.setToggleGroup(dataTG);
                                dataPublicRB.setUserData("Public");
                                popup.add(dataPublicRB, 0, 1);
                                RadioButton dataPrivateRB = new RadioButton(objDictionaryAction.getWord("PRIVATE"));
                                dataPrivateRB.setToggleGroup(dataTG);
                                dataPrivateRB.setUserData("Private");
                                popup.add(dataPrivateRB, 1, 1);
                                RadioButton dataProtectedRB = new RadioButton(objDictionaryAction.getWord("PROTECTED"));
                                dataProtectedRB.setToggleGroup(dataTG);
                                dataProtectedRB.setUserData("Protected");
                                popup.add(dataProtectedRB, 2, 1);
                                if(strAccess.equalsIgnoreCase("Public"))
                                    dataTG.selectToggle(dataPublicRB);
                                else if(strAccess.equalsIgnoreCase("Private"))
                                    dataTG.selectToggle(dataPrivateRB);
                                else 
                                    dataTG.selectToggle(dataProtectedRB);
                                
                                final Label lblPassword=new Label(objDictionaryAction.getWord("SERVICEPASSWORD"));
                                lblPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                popup.add(lblPassword, 0, 2, 1, 1);
                                final PasswordField passPF=new PasswordField();
                                passPF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                passPF.setPromptText(objDictionaryAction.getWord("PROMPTSERVICEPASSWORD"));
                                popup.add(passPF, 1, 2, 2, 1);
                                                                
                                final Label lblAlert = new Label(objDictionaryAction.getWord("ALERTWHAT"));
                                lblAlert.setStyle("-fx-wrap-text:true;");
                                lblAlert.setPrefWidth(250);
                                popup.add(lblAlert, 0, 4, 3, 1);
                                
                                Button btnYes = new Button(objDictionaryAction.getWord("SUBMIT"));
                                btnYes.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUBMIT")));
                                btnYes.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
                                btnYes.setDefaultButton(true);
                                btnYes.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        if(passPF.getText().length()==0){
                                            lblAlert.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                                        }
                                        else{
                                            if(Security.SecurePassword(passPF.getText(), objConfiguration.getObjUser().getStrUsername()).equals(objConfiguration.getObjUser().getStrAppPassword())){
                                                try {
                                                    String strAccessNew = dataTG.getSelectedToggle().getUserData().toString();
                                                    System.err.println(strAccessNew);
                                                    strAccessNew = new IDGenerator().setUserAcessValueData("WEAVE_LIBRARY",strAccessNew);
                                                    objWeaveAction = new WeaveAction();
                                                    objWeaveAction.resetWeavePermission(lblWeave.getId(),strAccessNew);
                                                    populateLibraryWeave();
                                                    dialogStage.close();                                            
                                                } catch (SQLException ex) {
                                                    new Logging("SEVERE",LibraryView.class.getName(),"Change Permission Data"+ex.getMessage(),ex);
                                                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                                                }
                                                System.gc();
                                            }
                                            else{
                                                lblAlert.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                                            }
                                        }
                                        System.gc();
                                    }
                                });
                                popup.add(btnYes, 0, 3);
                                Button btnNo = new Button(objDictionaryAction.getWord("CANCEL"));
                                btnNo.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
                                btnNo.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
                                btnNo.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        dialogStage.close();
                                        System.gc();
                                    }
                                });
                                popup.add(btnNo, 2, 3);
                                root.setCenter(popup);
                                dialogStage.setScene(scene);
                                dialogStage.showAndWait();  
                                e.consume();
                                System.gc();
                            }
                        });
                    }
                    action.getChildren().add(btnPermission);
                    Button btnUpdate;
                    if(!strAccess.equalsIgnoreCase("Public") && intUsage==0){
                        btnUpdate = new Button(objDictionaryAction.getWord("UPDATE"));
                        btnUpdate.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/update.png"));
                        btnUpdate.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPUPDATE")));
                    }else{
                        btnUpdate = new Button(objDictionaryAction.getWord("COPY"));
                        btnUpdate.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/copy.png"));
                        btnUpdate.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCOPY")));
                    }
                    btnUpdate.setMaxWidth(Double.MAX_VALUE);
                    btnUpdate.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent e) {
                            if(strAccess.equalsIgnoreCase("Public")){
                                objConfiguration.setServicePasswordValid(true);
                            } else{
                                new MessageView(objConfiguration);
                            }
                            if(objConfiguration.getServicePasswordValid()){
                                objConfiguration.setServicePasswordValid(false);
                                objConfiguration.strWindowFlowContext = "Dashboard";
                                objConfiguration.setStrRecentWeave(lblWeave.getId());
                                libraryStage.close();
                                System.gc();
                                WeaveView objWeaveView = new WeaveView(objConfiguration);
                                System.gc();
                            }
                        }
                    });
                    action.getChildren().add(btnUpdate);
                    Button btnFabric = new Button(objDictionaryAction.getWord("FABRICEDITORUTILITY"));
                    btnFabric.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/fabric_editor.png"));
                    btnFabric.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPFABRICEDITORUTILITY")));
                    btnFabric.setMaxWidth(Double.MAX_VALUE);
                    btnFabric.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent e) {
                                System.gc();
                                final Stage dialogStage = new Stage();
                                dialogStage.initStyle(StageStyle.UTILITY);
                                dialogStage.initModality(Modality.APPLICATION_MODAL);
                                dialogStage.setResizable(false);
                                dialogStage.setIconified(false);
                                dialogStage.setFullScreen(false);
                                dialogStage.setTitle(objDictionaryAction.getWord("PROJECT")+":"+objDictionaryAction.getWord("ALERT"));
                                BorderPane root = new BorderPane();
                                Scene scene = new Scene(root, 300, 200, Color.WHITE);
                                scene.getStylesheets().add(LibraryView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
                                final GridPane popup=new GridPane();
                                popup.setId("popup");
                                popup.setHgap(5);
                                popup.setVgap(5);
                                popup.setPadding(new Insets(5, 5, 5, 5));
                                Label lblPassword=new Label(objDictionaryAction.getWord("SERVICEPASSWORD"));
                                lblPassword.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                popup.add(lblPassword, 0, 0, 2, 1);
                                final PasswordField passPF =new PasswordField();
                                passPF.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSERVICEPASSWORD")));
                                passPF.setPromptText(objDictionaryAction.getWord("PROMPTSERVICEPASSWORD"));
                                popup.add(passPF, 0, 1, 2, 1);
                                final Label lblAlert=new Label("");
                                lblAlert.setStyle("-fx-wrap-text:true;");
                                lblAlert.setPrefWidth(250);
                                popup.add(lblAlert, 0, 3, 2, 1);
                                Button btnYes= new Button(objDictionaryAction.getWord("SUBMIT"));
                                btnYes.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUBMIT")));
                                btnYes.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/save.png"));
                                btnYes.setDefaultButton(true);
                                btnYes.setOnAction(new EventHandler<ActionEvent>(){
                                    @Override
                                    public void handle(ActionEvent event) {
                                        if(Security.SecurePassword(passPF.getText(), objConfiguration.getObjUser().getStrUsername()).equals(objConfiguration.getObjUser().getStrAppPassword())){
                                            System.gc();
                                            objConfiguration.strWindowFlowContext = "WeaveEditor";
                                            objConfiguration.setStrRecentWeave(lblWeave.getId().toString());
                                            libraryStage.close();
                                            FabricView objFabricView = new FabricView(objConfiguration);
                                            System.gc();
                                        }
                                        else if(passPF.getText().length()==0){
                                            lblAlert.setText(objDictionaryAction.getWord("NOSERVICEPASSWORD"));
                                        }
                                        else{
                                            lblAlert.setText(objDictionaryAction.getWord("INVALIDSERVICEPASSWORD"));
                                        }
                                    }
                                });
                                popup.add(btnYes, 0, 2);
                                Button btnNo = new Button(objDictionaryAction.getWord("CANCEL"));
                                btnNo.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
                                btnNo.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
                                btnNo.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        dialogStage.close();
                                        System.gc();
                                    }
                                });
                                popup.add(btnNo, 1, 2);
                                root.setCenter(popup);
                                dialogStage.setScene(scene);
                                dialogStage.showAndWait();  
                                System.gc();
                            }
                        });
                    action.getChildren().add(btnFabric);
                    
                    GP_container.add(action, (k+2)%6, i/2);
                }
            }
        } catch (Exception ex) {
            new Logging("SEVERE",LibraryView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    /*Cloth Library*/
    private void populateLibraryYarn(){
        try{
            GP_container.getChildren().clear();
            List lstYarn=null;
            List lstYarnDeatails = new ArrayList();
            objYarnAction = new YarnAction();
            lstYarnDeatails = objYarnAction.lstImportYarn(objYarn);
            if(lstYarnDeatails.size()==0){
                GP_container.getChildren().add(new Text(objDictionaryAction.getWord("YARN")+" - "+objDictionaryAction.getWord("NOVALUE")));
            }else{            
                for (int i=0, k=0; i<lstYarnDeatails.size();i++, k+=2){
                    lstYarn = (ArrayList)lstYarnDeatails.get(i);
                    
                    byte[] bytWeaveThumbnil = (byte[])lstYarn.get(14);
                    SeekableStream stream = new ByteArraySeekableStream(bytWeaveThumbnil);
                    String[] names = ImageCodec.getDecoderNames(stream);
                    ImageDecoder dec = ImageCodec.createImageDecoder(names[0], stream, null);
                    RenderedImage im = dec.decodeAsRenderedImage();
                    BufferedImage bufferedImage = PlanarImage.wrapRenderedImage(im).getAsBufferedImage();
                    Image image=SwingFXUtils.toFXImage(bufferedImage, null);
                    ImageView ivYarn = new ImageView(image);
                    ivYarn.setFitHeight(222);
                    ivYarn.setFitWidth(222);
                    stream.close();
                    bufferedImage = null;
                    GP_container.add(ivYarn, k%6, i/3);
                    
                    String strTempAccess="Private";
                    if(objYarn.getObjConfiguration().getObjUser().getStrUserID().equals("ADMIN"))
                        strTempAccess = "Private";
                    else
                        strTempAccess = new IDGenerator().getUserAcessValueData("YARN_LIBRARY",lstYarn.get(15).toString());
                    final String strAccess = strTempAccess;//new IDGenerator().getUserAcessValueData("YARN_LIBRARY",lstYarn.get(15).toString());
                    int intUsage=0;
                    
                    objYarnAction = new YarnAction();
                    intUsage = objYarnAction.countYarnUsage(lstYarn.get(0).toString());
                    
                    String strTooltip =
                                objDictionaryAction.getWord("YARNNAME")+": "+lstYarn.get(1).toString()+"\n"+
                                objDictionaryAction.getWord("YARNTYPE")+": "+lstYarn.get(2).toString()+"\n"+
                                objDictionaryAction.getWord("YARNCOLOR")+": "+lstYarn.get(3).toString()+"\n"+
                                objDictionaryAction.getWord("YARNCOUNT")+": "+lstYarn.get(4).toString()+"\n"+
                                objDictionaryAction.getWord("YARNUNIT")+": "+lstYarn.get(5).toString()+"\n"+
                                objDictionaryAction.getWord("YARNPLY")+": "+lstYarn.get(6).toString()+"\n"+
                                objDictionaryAction.getWord("YARNFACTOR")+": "+lstYarn.get(7).toString()+"\n"+
                                objDictionaryAction.getWord("YARNDIAMETER")+": "+lstYarn.get(8).toString()+"\n"+
                                objDictionaryAction.getWord("YARNSENCE")+": "+lstYarn.get(9).toString()+"\n"+
                                objDictionaryAction.getWord("YARNTWIST")+": "+lstYarn.get(10).toString()+"\n"+
                                objDictionaryAction.getWord("YARNHAIRNESS")+": "+lstYarn.get(11).toString()+"\n"+
                                objDictionaryAction.getWord("YARNDISTRIBUTION")+": "+lstYarn.get(12).toString()+"\n"+
                                objDictionaryAction.getWord("YARNPRICE")+": "+lstYarn.get(13).toString()+"\n"+
                                objDictionaryAction.getWord("USED")+": "+intUsage+"\n"+
                                objDictionaryAction.getWord("PERMISSION")+": "+new IDGenerator().getUserAcessValueData("YARN_LIBRARY",lstYarn.get(15).toString())+"\n"+
                                objDictionaryAction.getWord("BY")+": "+lstYarn.get(16).toString()+"\n"+
                                objDictionaryAction.getWord("DATE")+": "+lstYarn.get(17).toString();
                    if(objYarn.getObjConfiguration().getObjUser().getStrUserID().equals("ADMIN"))
                        intUsage=0;
                    final Label lblYarn = new Label(strTooltip);
                    lblYarn.setTooltip(new Tooltip(strTooltip));
                    lblYarn.setId(lstYarn.get(0).toString());
                    lblYarn.setUserData(lstYarn.get(0).toString());
                    GP_container.add(lblYarn, (k+1)%6, i/3);
                }
            }
        } catch (Exception ex) {
            new Logging("SEVERE",LibraryView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    /*Colour Library*/
    private void populateLibraryColour(){
        try{
            GP_container.getChildren().clear();
            List lstColour=null;
            List lstColourDeatails = new ArrayList();
            objFabricAction = new FabricAction();
            lstColourDeatails = objFabricAction.lstImportColor(objColour);
            if(lstColourDeatails.size()==0){
                GP_container.getChildren().add(new Text(objDictionaryAction.getWord("COLOUR")+" - "+objDictionaryAction.getWord("NOVALUE")));
            }else{            
                for (int i=0, k=0; i<lstColourDeatails.size();i++, k+=2){
                    lstColour = (ArrayList)lstColourDeatails.get(i);

                    Label ivColour = new Label();
                    ivColour.setStyle("-fx-background-color:rgb("+lstColour.get(3).toString()+","+lstColour.get(4).toString()+","+lstColour.get(5).toString()+");");
                    ivColour.setPrefSize(222,222);
                    GP_container.add(ivColour, k%6, i/3);

                    String strTooltip = 
                            objDictionaryAction.getWord("NAME")+": "+lstColour.get(1).toString()+"\n"+
                            objDictionaryAction.getWord("TYPE")+": "+lstColour.get(11).toString()+"\n"+
                            objDictionaryAction.getWord("RGB")+": "+lstColour.get(3).toString()+", "+lstColour.get(4).toString()+", "+lstColour.get(5).toString()+"\n"+
                            objDictionaryAction.getWord("HEX")+": "+lstColour.get(6).toString()+"\n"+
                            objDictionaryAction.getWord("CODE")+": "+lstColour.get(7).toString()+"\n"+
                            objDictionaryAction.getWord("BY")+": "+lstColour.get(9).toString()+"\n"+
                            objDictionaryAction.getWord("DATE")+": "+lstColour.get(10).toString();
                    Label lblColour = new Label(strTooltip);
                    lblColour.setTooltip(new Tooltip(strTooltip));
                    GP_container.add(lblColour, (k+1)%6, i/3);
                }
            }
        } catch (Exception ex) {
            new Logging("SEVERE",LibraryView.class.getName(),ex.toString(),ex);
            lblStatus.setText(objDictionaryAction.getWord("ERROR"));
        }
    }
    private ArrayList<String> getColourType(){
        ArrayList<String> types=new ArrayList<>();
        try{
            types=new ColourAction().getColourType();
        }
        catch(SQLException sqlEx){
            new Logging("SEVERE",PaletteView.class.getName(),"getColourType() : ", sqlEx);
        }
        return types;
    }
    
    public void getTabPagingContext(){
        if(currentTab == colourTab) {
            currentPage=currentPageColour;
            perPage=perPageColour;
        }else if(currentTab == yarnTab) {
            currentPage=currentPageYarn;
            perPage=perPageYarn;
        }else if(currentTab == weaveTab) {
            currentPage=currentPageWeave;
            perPage=perPageWeave;
        }else if(currentTab == artworkTab) {
            currentPage=currentPageArtwork;
            perPage=perPageArtwork;
        }else if(currentTab == fabricTab) {
            currentPage=currentPageFabric;
            perPage=perPageFabric;
        }else if(currentTab == clothTab) {
            currentPage=currentPageCloth;
            perPage=perPageCloth;
        }
    }
    
    public void setTabPagingContext(){
        if(currentTab == colourTab) {
            currentPageColour=currentPage;
            perPageColour=perPage;
        }else if(currentTab == yarnTab) {
            currentPageYarn=currentPage;
            perPageYarn=perPage;
        }else if(currentTab == weaveTab) {
            currentPageWeave=currentPage;
            perPageWeave=perPage;
        }else if(currentTab == artworkTab) {
            currentPageArtwork=currentPage;
            perPageArtwork=perPage;
        }else if(currentTab == fabricTab) {
            currentPageFabric=currentPage;
            perPageFabric=perPage;
        }else if(currentTab == clothTab) {
            currentPageCloth=currentPage;
            perPageCloth=perPage;
        }
    }
    
    public void populateTabPagingContext(){
        if(currentTab == colourTab) {
            objColour.setStrLimit((currentPage*perPage)+","+perPage);
            populateLibraryColour();
        }else if(currentTab == yarnTab) {
            objYarn.setStrLimit((currentPage*perPage)+","+perPage);
            populateLibraryYarn();
        }else if(currentTab == weaveTab) {
            objWeave.setStrLimit((currentPage*perPage)+","+perPage);
            populateLibraryWeave();
        }else if(currentTab == artworkTab) {
            objArtwork.setStrLimit((currentPage*perPage)+","+perPage);
            populateLibraryArtwork();
        }else if(currentTab == fabricTab) {
            objFabric.setStrLimit((currentPage*perPage)+","+perPage);
            populateLibraryFabric();
        }else if(currentTab == clothTab) {
            objCloth.setStrLimit((currentPage*perPage)+","+perPage);
            populateLibraryCloth();
        }
    }
 
    /**
     * homeMenuAction
     * <p>
     * Function use for help menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        UtilityView
     */
    private void homeMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHOME"));
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.setIconified(false);
        dialogStage.setFullScreen(false);
        dialogStage.setTitle(objDictionaryAction.getWord("ALERT"));
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 300, 100, Color.WHITE);
        scene.getStylesheets().add(UtilityView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        popup.add(new ImageView("/media/alert_warning.png"), 0, 0); 
        Label lblAlert = new Label(objDictionaryAction.getWord("ALERTCLOSE"));
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(250);
        popup.add(lblAlert, 1, 0);
        Button btnYes = new Button(objDictionaryAction.getWord("YES"));
        btnYes.setPrefWidth(50);
        btnYes.setId("btnYes");
        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();                
                libraryStage.close();
                System.gc();
                WindowView objWindoeView = new WindowView(objConfiguration);
            }
        });
        popup.add(btnYes, 0, 1);
        Button btnNo = new Button(objDictionaryAction.getWord("NO"));
        btnNo.setPrefWidth(50);
        btnNo.setId("btnNo");
        btnNo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
            }
        });
        popup.add(btnNo, 1, 1);
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.showAndWait(); 
    }
    /**
     * parentMenuAction
     * <p>
     * Function use for help menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        UtilityView
     */
    private void parentMenuAction(){
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.setIconified(false);
        dialogStage.setFullScreen(false);
        dialogStage.setTitle(objDictionaryAction.getWord("ALERT"));
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 300, 100, Color.WHITE);
        scene.getStylesheets().add(DeviceView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        final GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        popup.add(new ImageView("/media/alert_warning.png"), 0, 0); 
        Label lblAlert = new Label(objDictionaryAction.getWord("ALERTCLOSE"));
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(250);
        popup.add(lblAlert, 1, 0);
        Button btnYes = new Button(objDictionaryAction.getWord("YES"));
        btnYes.setPrefWidth(50);
        btnYes.setId("btnYes");
        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                libraryStage.close();
                System.gc();
                UtilityView objUtilityView = new UtilityView(objConfiguration);
            }
        });
        popup.add(btnYes, 0, 1);
        Button btnNo = new Button(objDictionaryAction.getWord("NO"));
        btnNo.setPrefWidth(50);
        btnNo.setId("btnNo");
        btnNo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
            }
        });
        popup.add(btnNo, 1, 1);
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.showAndWait();
    }
    /**
     * helpMenuAction
     * <p>
     * Function use for help menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        UtilityView
     */
    private void helpMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHELP"));
        HelpView objHelpView = new HelpView(objConfiguration);
    }
    /**
     * technicalMenuAction
     * <p>
     * Function use for technical menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        UtilityView
     */
    private void technicalMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONTECHNICAL"));
        TechnicalView objTechnicalView = new TechnicalView(objConfiguration);
    }
    /**
     * aboutMenuAction
     * <p>
     * Function use for about menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        UtilityView
     */
    private void aboutMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONABOUTUS"));
        AboutView objAboutView = new AboutView(objConfiguration);
    }
    /**
     * contactMenuAction
     * <p>
     * Function use for contact menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        UtilityView
     */
    private void contactMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCONTACTUS"));
        ContactView objContactView = new ContactView(objConfiguration);
    }
    /**
     * logoutMenuAction
     * <p>
     * Function use for exit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void logoutMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHOME"));
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.setIconified(false);
        dialogStage.setFullScreen(false);
        dialogStage.setTitle(objDictionaryAction.getWord("ALERT"));
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 300, 100, Color.WHITE);
        scene.getStylesheets().add(LibraryView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        popup.add(new ImageView("/media/alert_warning.png"), 0, 0); 
        Label lblAlert = new Label(objDictionaryAction.getWord("ALERTCLOSE"));
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(250);
        popup.add(lblAlert, 1, 0);
        Button btnYes = new Button(objDictionaryAction.getWord("YES"));
        btnYes.setPrefWidth(50);
        btnYes.setId("btnYes");
        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                objConfiguration.setStrRecentWeave(null);
                dialogStage.close();
                libraryStage.close();
                System.gc();
                UserLoginView objUserLoginView = new UserLoginView(objConfiguration);
            }
        });
        popup.add(btnYes, 0, 1);
        Button btnNo = new Button(objDictionaryAction.getWord("NO"));
        btnNo.setPrefWidth(50);
        btnNo.setId("btnNo");
        btnNo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
            }
        });
        popup.add(btnNo, 1, 1);
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.showAndWait();
    }
    /**
     * exitMenuAction
     * <p>
     * Function use for exit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        UtilityView
     */
    private void exitMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONEXIT"));
        System.gc();
        libraryStage.close();
    }
    @Override
    public void start(Stage stage) throws Exception {
        new LibraryView(stage);
        new Logging("WARNING",LibraryView.class.getName(),"UnsupportedOperationException",new UnsupportedOperationException("Not supported yet."));
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
        
