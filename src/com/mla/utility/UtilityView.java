/*
 * Copyright (C) 2017 Media Lab Asia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mla.utility;

import com.mla.dictionary.DictionaryAction;
import com.mla.dictionary.DictionaryView;
import com.mla.main.Configuration;
import com.mla.main.ConfigurationView;
import com.mla.main.Logging;
import com.mla.main.WindowView;
import com.mla.simulator.SimulatorView;
import com.mla.user.UserLoginView;
import java.awt.Dimension;
import java.awt.Toolkit;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
/**
 *
 * @Designing GUI window for dashboard
 * @author Amit Kumar Singh
 * 
 */
public class UtilityView extends Application {
    private static Stage utilityStage;
    private Scene scene;
    private BorderPane root;
    private Label lblStatus;
    private ProgressBar progressB;
    private ProgressIndicator progressI;

    Configuration objConfiguration = null;
    DictionaryAction objDictionaryAction = null;
    
 /**
 * UtilityView(Stage)
 * <p>
 * This constructor is used for individual call of class. 
 * 
 * @param       Stage primaryStage
 * @author      Amit Kumar Singh
 * @version     %I%, %G%
 * @since       1.0
 * @see         javafx.stage.*;
 * @link        UtilityView
 */
     public UtilityView(final Stage primaryStage) {}
    
 /**
 * UtilityView(Configuration)
 * <p>
 * This class is used for prompting about software information. 
 * 
 * @param       Configuration objConfigurationCall
 * @author      Amit Kumar Singh
 * @version     %I%, %G%
 * @since       1.0
 * @see         javafx.scene.control.*;
 * @link        Configuration
 */
    public UtilityView(Configuration objConfigurationCall) {
        
        objConfiguration = objConfigurationCall;
        objDictionaryAction = new DictionaryAction(objConfiguration);

        utilityStage = new Stage(); 
        root = new BorderPane();
        scene = new Scene(root, objConfiguration.WIDTH, objConfiguration.HEIGHT, Color.WHITE);
        scene.getStylesheets().add(UtilityView.class.getResource(objConfiguration.getStrTemplate()+"/childboard.css").toExternalForm());
    
        //windowStage.resizableProperty().addListener(listener);
        scene.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                setGlobalWidthHeight();
                utilityStage.setHeight(objConfiguration.HEIGHT);
                utilityStage.setWidth(objConfiguration.WIDTH);
                populateContainer();
            }
        });
        scene.heightProperty().addListener(new ChangeListener<Number>() {
            @Override 
            public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneHeight, Number newSceneHeight) {
                setGlobalWidthHeight();
                utilityStage.setHeight(objConfiguration.HEIGHT);
                utilityStage.setWidth(objConfiguration.WIDTH);
                populateContainer();
            }
        });
        
        populateContainer();
        
        addAccelratorKey();
        
        utilityStage.getIcons().add(new Image("/media/icon.png"));
        utilityStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWDASHBOARD")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
        //configurationStage.setIconified(true);
        utilityStage.setResizable(false);
        utilityStage.setScene(scene);
        utilityStage.setX(-5);
        utilityStage.setY(0);
        utilityStage.show();  
        utilityStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                homeMenuAction();
                we.consume();
            }
        });
    }
    private void setGlobalWidthHeight(){
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        objConfiguration.WIDTH = screenSize.getWidth();
        objConfiguration.HEIGHT = screenSize.getHeight();
        objConfiguration.strIconResolution = (objConfiguration.WIDTH<1280)?"hd_none":(objConfiguration.WIDTH<1920)?"hd":(objConfiguration.WIDTH<2560)?"hd_full":"hd_quad";
    }
    private void populateContainer(){   
        HBox footContainer = new HBox();
        progressB = new ProgressBar(0);
        progressB.setVisible(false);
        progressI = new ProgressIndicator(0);
        progressI.setVisible(false);
        lblStatus = new Label(objDictionaryAction.getWord("WELCOMETOCADTOOL"));
        lblStatus.setId("message");
        footContainer.getChildren().addAll(lblStatus,progressB,progressI);
        footContainer.setId("footContainer");
        root.setBottom(footContainer);
        
        MenuBar menuBar = new MenuBar();
        menuBar.prefWidthProperty().bind(utilityStage.widthProperty());
        
        //menu bar and events
        Menu homeMenu  = new Menu();
        HBox homeMenuHB = new HBox();
        homeMenuHB.getChildren().addAll(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/home.png"),new Label(objDictionaryAction.getWord("HOME")));
        homeMenu.setGraphic(homeMenuHB);
        homeMenu.setAccelerator(new KeyCodeCombination(KeyCode.M, KeyCombination.SHIFT_DOWN));
        homeMenuHB.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                homeMenuAction();  
                me.consume();
            }
        });       
        //Help Menu items
        Label supportMenuLabel = new Label(objDictionaryAction.getWord("SUPPORT"));
        supportMenuLabel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSUPPORT")));
        Menu supportMenu = new Menu();
        supportMenu.setGraphic(supportMenuLabel);
        
        MenuItem helpMenuItem = new MenuItem(objDictionaryAction.getWord("HELP"));
        helpMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/help.png"));
        helpMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.H, KeyCombination.SHIFT_DOWN));
        helpMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {       
                helpMenuAction();
            }
        });        
        MenuItem technicalMenuItem = new MenuItem(objDictionaryAction.getWord("TECHNICAL"));
        technicalMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/technical_info.png"));
        technicalMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.T, KeyCombination.SHIFT_DOWN));
        technicalMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {       
                technicalMenuAction();
            }
        });
        MenuItem aboutMenuItem = new MenuItem(objDictionaryAction.getWord("ABOUTUS"));
        aboutMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/about_software.png"));
        aboutMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.A, KeyCombination.SHIFT_DOWN));
        aboutMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                aboutMenuAction();
            }
        });
        MenuItem contactMenuItem = new MenuItem(objDictionaryAction.getWord("CONTACTUS"));
        contactMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/contact_us.png"));
        contactMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.SHIFT_DOWN));
        contactMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                contactMenuAction();
            }
        });
        MenuItem logoutMenuItem = new MenuItem(objDictionaryAction.getWord("LOGOUT"));
        logoutMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/logout.png"));
        logoutMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.L, KeyCombination.SHIFT_DOWN));
        logoutMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                logoutMenuAction();
            }
        });
        MenuItem exitMenuItem = new MenuItem(objDictionaryAction.getWord("EXIT"));
        exitMenuItem.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/quit.png"));
        exitMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.ESCAPE));
        exitMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                exitMenuAction();
            }
        });       
        supportMenu.getItems().addAll(helpMenuItem, technicalMenuItem, aboutMenuItem, contactMenuItem, new SeparatorMenuItem(), logoutMenuItem);
        menuBar.getMenus().addAll(homeMenu, supportMenu);
        root.setTop(menuBar);

        //Main Pane
        GridPane containerGP = new GridPane();
        containerGP.setId("mainPane");
        //containerGP.setPrefSize(objConfiguration.WIDTH*0.4, objConfiguration.HEIGHT*0.5);
        containerGP.setAlignment(Pos.CENTER);
        
        // fabric editor
        VBox convertorVB = new VBox(); 
        Label convertorLbl= new Label(objDictionaryAction.getWord("CONVERSION"));
        convertorLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("CONVERSION")+" (Shift+F1)\n"+objDictionaryAction.getWord("TOOLTIPCONVERSION")));
        convertorVB.getChildren().addAll(new ImageView(objConfiguration.getStrTemplate()+"/img/s_converter.png"), convertorLbl);
        convertorVB.setPrefWidth(objConfiguration.WIDTH*0.25);
        convertorVB.setPrefHeight(objConfiguration.HEIGHT*0.25);
        convertorVB.getStyleClass().addAll("VBox");
        convertorVB.setCursor(Cursor.CROSSHAIR);
        containerGP.add(convertorVB, 0, 0);
        // artwork editor
        VBox synchronizationVB = new VBox(); 
        Label synchronizationLbl= new Label(objDictionaryAction.getWord("EXPORT")+" "+objDictionaryAction.getWord("IMPORT"));
        synchronizationLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("EXPORT")+" (Shift+F2)\n"+objDictionaryAction.getWord("TOOLTIPARTWORKEDITOR")));
        synchronizationVB.getChildren().addAll(new ImageView(objConfiguration.getStrTemplate()+"/img/s_export_import.png"), synchronizationLbl);
        synchronizationVB.setPrefWidth(objConfiguration.WIDTH*0.25);
        synchronizationVB.setPrefHeight(objConfiguration.HEIGHT*0.25);
        synchronizationVB.getStyleClass().addAll("VBox");
        synchronizationVB.setCursor(Cursor.CROSSHAIR);
        containerGP.add(synchronizationVB, 1, 0);
        // Cloth editor item
        VBox libraryVB = new VBox(); 
        Label libraryLbl= new Label(objDictionaryAction.getWord("LIBRARY"));
        libraryLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("LIBRARY")+" (Shift+F3)\n"+objDictionaryAction.getWord("TOOLTIPLIBRARY")));
        libraryVB.getChildren().addAll(new ImageView(objConfiguration.getStrTemplate()+"/img/s_library.png"), libraryLbl);
        libraryVB.setPrefWidth(objConfiguration.WIDTH*0.25);
        libraryVB.setPrefHeight(objConfiguration.HEIGHT*0.25);
        libraryVB.getStyleClass().addAll("VBox");
        libraryVB.setCursor(Cursor.CROSSHAIR);
        containerGP.add(libraryVB, 2, 0);
        // weave editor
        VBox translatorVB = new VBox(); 
        Label translatorLbl= new Label(objDictionaryAction.getWord("TRASLATOR"));
        translatorLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("TRASLATOR")+" (Shift+F4)\n"+objDictionaryAction.getWord("TOOLTIPTRASLATOR")));
        translatorVB.getChildren().addAll(new ImageView(objConfiguration.getStrTemplate()+"/img/s_translator.png"), translatorLbl);
        translatorVB.setPrefWidth(objConfiguration.WIDTH*0.25);
        translatorVB.setPrefHeight(objConfiguration.HEIGHT*0.25);
        translatorVB.getStyleClass().addAll("VBox");
        translatorVB.setCursor(Cursor.CROSSHAIR);
        containerGP.add(translatorVB, 0, 1);
        // Save Texture file item
        VBox simulatorVB = new VBox(); 
        Label simulatorLbl= new Label(objDictionaryAction.getWord("SIMULATOR"));
        simulatorLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("SIMULATOR")+" (Shift+F5)\n"+objDictionaryAction.getWord("TOOLTIPSIMULATOR")));
        simulatorVB.getChildren().addAll(new ImageView(objConfiguration.getStrTemplate()+"/img/s_simulator.png"), simulatorLbl);
        simulatorVB.setPrefWidth(objConfiguration.WIDTH*0.25);
        simulatorVB.setPrefHeight(objConfiguration.HEIGHT*0.25);
        simulatorVB.getStyleClass().addAll("VBox");
        simulatorVB.setCursor(Cursor.CROSSHAIR);
        containerGP.add(simulatorVB, 1, 1);
        // Save As file item
        VBox deviceVB = new VBox(); 
        Label deviceLbl= new Label(objDictionaryAction.getWord("APPLICATIONINTEGRATION"));
        deviceLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("APPLICATIONINTEGRATION")+" (Shift+F6)\n"+objDictionaryAction.getWord("TOOLTIPAPPLICATIONINTEGRATION")));
        deviceVB.getChildren().addAll(new ImageView(objConfiguration.getStrTemplate()+"/img/s_intregration.png"), deviceLbl);
        deviceVB.setPrefWidth(objConfiguration.WIDTH*0.25);
        deviceVB.setPrefHeight(objConfiguration.HEIGHT*0.25);
        deviceVB.getStyleClass().addAll("VBox");
        deviceVB.setCursor(Cursor.CROSSHAIR);
        containerGP.add(deviceVB, 2, 1);
        // Save Texture file item
        VBox paletteVB = new VBox(); 
        Label paletteLbl= new Label(objDictionaryAction.getWord("PALETTE"));
        paletteLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("PALETTE")+" (Shift+F7)\n"+objDictionaryAction.getWord("TOOLTIPPALETTE")));
        paletteVB.getChildren().addAll(new ImageView(objConfiguration.getStrTemplate()+"/img/s_yarn_color_palette.png"), paletteLbl);
        paletteVB.setPrefWidth(objConfiguration.WIDTH*0.25);
        paletteVB.setPrefHeight(objConfiguration.HEIGHT*0.25);
        paletteVB.getStyleClass().addAll("VBox");
        paletteVB.setCursor(Cursor.CROSSHAIR);
        containerGP.add(paletteVB, 0, 2);        
        // Cloth editor item
        VBox systemVB = new VBox(); 
        Label systemLbl= new Label(objDictionaryAction.getWord("CONFIGURATION"));
        systemLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("CONFIGURATION")+" (Shift+F8)\n"+objDictionaryAction.getWord("TOOLTIPCONFIGURATION")));
        systemVB.getChildren().addAll(new ImageView(objConfiguration.getStrTemplate()+"/img/s_configuration.png"), systemLbl);
        systemVB.setPrefWidth(objConfiguration.WIDTH*0.25);
        systemVB.setPrefHeight(objConfiguration.HEIGHT*0.25);
        systemVB.getStyleClass().addAll("VBox");
        systemVB.setCursor(Cursor.CROSSHAIR);
        containerGP.add(systemVB, 1, 2);
        // Cloth editor item
        VBox galleryVB = new VBox(); 
        Label galleryLbl= new Label(objDictionaryAction.getWord("HELP"));
        galleryLbl.setTooltip(new Tooltip(objDictionaryAction.getWord("HELP")+" (Shift+F9)\n"+objDictionaryAction.getWord("TOOLTIPHELP")));
        galleryVB.getChildren().addAll(new ImageView(objConfiguration.getStrTemplate()+"/img/s_help_support.png"), galleryLbl);
        galleryVB.setPrefWidth(objConfiguration.WIDTH*0.25);
        galleryVB.setPrefHeight(objConfiguration.HEIGHT*0.25);
        galleryVB.getStyleClass().addAll("VBox");
        galleryVB.setCursor(Cursor.CROSSHAIR);
        containerGP.add(galleryVB, 2, 2);
 
        //Add the action to Buttons.
        convertorVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                convertorAction();
            }
        });
        synchronizationVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                synchronizationAction();
            }
        });
        libraryVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                libraryAction();
            }
        });
        translatorVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                translatorAction();
            }
        });
        simulatorVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                simulatorAction();
            }
        });
        deviceVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                deviceAction();
            }
        });        
        paletteVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                paletteAction();
            }
        });
        systemVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                systemAction();
            }
        });
        galleryVB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                galleryAction();
            }
        });
        
        //containerGP.setAlignment(Pos.CENTER);
        VBox container = new VBox();
        container.setPrefSize(objConfiguration.WIDTH, objConfiguration.HEIGHT);        
        container.setAlignment(Pos.CENTER);
        container.getChildren().add(containerGP);
        container.setId("container");
        
        root.setLeft(container);
        //root.setCenter(containerGP);
    }
    /**
     * addAccelratorKey
     * <p>
     * Function use for adding shortcut key combinations. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void addAccelratorKey(){
        // variable names: kc + Alphabet + ALT|CONTROL|SHIFT // ACS alphabetical
        final KeyCodeCombination convertorKCC = new KeyCodeCombination(KeyCode.F1, KeyCombination.SHIFT_DOWN);
        final KeyCodeCombination synchronizationKCC = new KeyCodeCombination(KeyCode.F2, KeyCombination.SHIFT_DOWN);
        final KeyCodeCombination libraryKCC = new KeyCodeCombination(KeyCode.F3, KeyCombination.SHIFT_DOWN);
        final KeyCodeCombination translatorKCC = new KeyCodeCombination(KeyCode.F4, KeyCombination.SHIFT_DOWN);
        final KeyCodeCombination simulatorKCC = new KeyCodeCombination(KeyCode.F5, KeyCombination.SHIFT_DOWN);
        final KeyCodeCombination deviceKCC = new KeyCodeCombination(KeyCode.F6, KeyCombination.SHIFT_DOWN);
        final KeyCodeCombination paletteKCC = new KeyCodeCombination(KeyCode.F7, KeyCombination.SHIFT_DOWN);
        final KeyCodeCombination systemKCC = new KeyCodeCombination(KeyCode.F8, KeyCombination.SHIFT_DOWN);
        final KeyCodeCombination galleryKCC = new KeyCodeCombination(KeyCode.F9, KeyCombination.SHIFT_DOWN);
        scene.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                if(convertorKCC.match(t))
                    convertorAction();
                else if(synchronizationKCC.match(t))
                    synchronizationAction();
                else if(libraryKCC.match(t))
                    libraryAction();
                else if(translatorKCC.match(t))
                    translatorAction();
                else if(simulatorKCC.match(t))
                    simulatorAction();
                else if(deviceKCC.match(t))
                    deviceAction();
                else if(paletteKCC.match(t))
                    paletteAction();
                else if(systemKCC.match(t))
                    systemAction();
                else if(galleryKCC.match(t))
                    galleryAction();         
            }
        });
    }
    /**
     * homeMenuAction
     * <p>
     * Function use for help menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        UtilityView
     */
    private void homeMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHOME"));
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.setIconified(false);
        dialogStage.setFullScreen(false);
        dialogStage.setTitle(objDictionaryAction.getWord("ALERT"));
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 300, 100, Color.WHITE);
        scene.getStylesheets().add(UtilityView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        popup.add(new ImageView("/media/alert_warning.png"), 0, 0);  
        Label lblAlert = new Label(objDictionaryAction.getWord("ALERTCLOSE"));
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(250);
        popup.add(lblAlert, 1, 0);
        Button btnYes = new Button(objDictionaryAction.getWord("YES"));
        btnYes.setPrefWidth(50);
        btnYes.setId("btnYes");
        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();                
                utilityStage.close();
                System.gc();
                WindowView objWindoeView = new WindowView(objConfiguration);
            }
        });
        popup.add(btnYes, 0, 1);
        Button btnNo = new Button(objDictionaryAction.getWord("NO"));
        btnNo.setPrefWidth(50);
        btnNo.setId("btnNo");
        btnNo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
            }
        });
        popup.add(btnNo, 1, 1);
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.showAndWait(); 
    }
    /**
     * helpMenuAction
     * <p>
     * Function use for help menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        UtilityView
     */
    private void helpMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHELP"));
        HelpView objHelpView = new HelpView(objConfiguration);
    }
    /**
     * technicalMenuAction
     * <p>
     * Function use for technical menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        UtilityView
     */
    private void technicalMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONTECHNICAL"));
        TechnicalView objTechnicalView = new TechnicalView(objConfiguration);
    }
    /**
     * aboutMenuAction
     * <p>
     * Function use for about menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        UtilityView
     */
    private void aboutMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONABOUTUS"));
        AboutView objAboutView = new AboutView(objConfiguration);
    }
    /**
     * contactMenuAction
     * <p>
     * Function use for contact menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        UtilityView
     */
    private void contactMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONCONTACTUS"));
        ContactView objContactView = new ContactView(objConfiguration);
    }
    /**
     * logoutMenuAction
     * <p>
     * Function use for exit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        FabricView
     */
    private void logoutMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONHOME"));
        final Stage dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.setIconified(false);
        dialogStage.setFullScreen(false);
        dialogStage.setTitle(objDictionaryAction.getWord("ALERT"));
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 300, 100, Color.WHITE);
        scene.getStylesheets().add(UtilityView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        GridPane popup=new GridPane();
        popup.setId("popup");
        popup.setHgap(5);
        popup.setVgap(5);
        popup.setPadding(new Insets(25, 25, 25, 25));
        popup.add(new ImageView("/media/alert_warning.png"), 0, 0);  
        Label lblAlert = new Label(objDictionaryAction.getWord("ALERTCLOSE"));
        lblAlert.setStyle("-fx-wrap-text:true;");
        lblAlert.setPrefWidth(250);
        popup.add(lblAlert, 1, 0);
        Button btnYes = new Button(objDictionaryAction.getWord("YES"));
        btnYes.setPrefWidth(50);
        btnYes.setId("btnYes");
        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                objConfiguration.setStrRecentWeave(null);
                dialogStage.close();
                utilityStage.close();
                System.gc();
                UserLoginView objUserLoginView = new UserLoginView(objConfiguration);
            }
        });
        popup.add(btnYes, 0, 1);
        Button btnNo = new Button(objDictionaryAction.getWord("NO"));
        btnNo.setPrefWidth(50);
        btnNo.setId("btnNo");
        btnNo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialogStage.close();
                System.gc();
            }
        });
        popup.add(btnNo, 1, 1);
        root.setCenter(popup);
        dialogStage.setScene(scene);
        dialogStage.showAndWait();
    }
    /**
     * exitMenuAction
     * <p>
     * Function use for exit menu item. 
     * 
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @see         javafx.event.*;
     * @link        UtilityView
     */
    private void exitMenuAction(){
        lblStatus.setText(objDictionaryAction.getWord("ACTIONEXIT"));
        System.gc();
        utilityStage.close();
    }
    private void convertorAction(){
        System.gc();
        utilityStage.close();
        ConvertorView objConvertorView = new ConvertorView(objConfiguration);
    }
    private void synchronizationAction(){
       System.gc();
        utilityStage.close();
        ExportImportView objExportImportView = new ExportImportView(objConfiguration);
    }
    private void libraryAction(){
        System.gc();
        utilityStage.close();
        LibraryView objLibraryView = new LibraryView(objConfiguration);
    }
    private void translatorAction(){
        System.gc();
        utilityStage.close();
        DictionaryView objDictionaryView = new DictionaryView(objConfiguration);
        //TranslatorView objTranslatorView = new TranslatorView(objConfiguration);
    }
     private void simulatorAction(){
        System.gc();
        utilityStage.close();
        SimulatorView objSimulatorView = new SimulatorView(objConfiguration);
    }
     private void deviceAction(){
        System.gc();
        utilityStage.close();
        DeviceView objDeviceView = new DeviceView(objConfiguration);
    }
    private void paletteAction(){
        System.gc();
        utilityStage.close();
        //ColorPaletteEditView objColorPaletteEditView = new ColorPaletteEditView(objConfiguration);
        //ColorPaletteView objColorPaletteView = new ColorPaletteView(objConfiguration);
        PaletteView objPaletteView = new PaletteView(objConfiguration);
    }
     private void systemAction(){
        System.gc();
        utilityStage.close();
        ConfigurationView objConfigurationView = new ConfigurationView(objConfiguration);
    }
     private void galleryAction(){
        System.gc();
        utilityStage.close();
        GalleryView objGalleryView = new GalleryView(objConfiguration);
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        new UtilityView(stage);
        new Logging("WARNING",UtilityView.class.getName(),"UnsupportedOperationException",new UnsupportedOperationException("Not supported yet."));
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}