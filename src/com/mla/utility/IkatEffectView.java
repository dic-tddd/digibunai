package com.mla.utility;

import com.mla.dictionary.DictionaryAction;
import com.mla.main.Configuration;
import com.mla.main.Logging;
import com.mla.pattern.PatternView;
import com.mla.weave.Weave;
import com.mla.weave.WeaveAction;
import com.mla.weave.WeaveEditView;
import com.mla.weave.WeaveImportView;
import com.mla.yarn.Yarn;
import com.sun.media.jai.codec.ByteArraySeekableStream;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.SeekableStream;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.media.jai.PlanarImage;

/**
 * Simulates Ikat Effect on input image
 * Intensity of effect is varied with horizontal and vertical offset values
 * @since 0.9.7
 * @author Aatif Ahmad Khan
 */
public class IkatEffectView {
    
    int numTieThreads  = 1; // Number of threads tied together
    int hOffset = 0; // horizontal offset value
    int vOffset = 0; // vertical offset value
    boolean vFlag = true; // flag for vertical or horizontal ikat effect
    BufferedImage originalImage; // all operations on original (input) image
    // following fields are made public as these are required to be monitored in ArtworkView.java
    public TextField txtTieThreads;
    public TextField txtVOffset;
    public TextField txtHOffset;
    public Weave objWeave; // weave pattern fill
    public int bgColorRGB = -1; // default WHITE
    public int[] lstWarpSequenceColor; // int RGB of color
    public int[] lstWeftSequenceColor; // int RGB of color
    public StringProperty spWeaveId;
    public StringProperty spThreadSequence;
    public Stage ikatEffectStage;
    
    /**
     * Displays Ikat Effect Viewer Stage with controls to apply the effect on
     * input image and display updated image
     * @param inputImage 
     */
    public IkatEffectView(BufferedImage inputImage, final Configuration objConfiguration){
        ikatEffectStage = new Stage();
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 300, 220, Color.WHITE);
        scene.getStylesheets().add(IkatEffectView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        
        originalImage = inputImage;
        
        bgColorRGB = originalImage.getRGB(0, 0);
        lstWarpSequenceColor = new int[]{-1};
        lstWeftSequenceColor = new int[]{bgColorRGB};
        spWeaveId = new SimpleStringProperty("");
        spThreadSequence = new SimpleStringProperty("");
        
        // UI
        GridPane containerGP = new GridPane();
        containerGP.setHgap(5);
        containerGP.setVgap(5);
        containerGP.setPadding(new Insets(10));
        containerGP.setAlignment(Pos.CENTER);
        Label lblCaption = new Label("Select Direction:");
        containerGP.add(lblCaption, 0, 0, 2, 1);
        ToggleGroup vhTG = new ToggleGroup();
        RadioButton verticalRB = new RadioButton("Vertical");
        verticalRB.setSelected(vFlag);
        RadioButton horizontalRB = new RadioButton("Horizontal");
        horizontalRB.setSelected(!vFlag);
        verticalRB.setToggleGroup(vhTG);
        horizontalRB.setToggleGroup(vhTG);
        containerGP.add(verticalRB, 0, 1);
        containerGP.add(horizontalRB, 1, 1);
        final Label lblTieThreads = new Label("Ribbon (Tie)");
        txtTieThreads = new TextField("1");
        containerGP.add(lblTieThreads, 0, 2);
        containerGP.add(txtTieThreads, 1, 2);
        final Label lblVOffset = new Label("Vertical Offset");
        final Label lblHOffset = new Label("Horizontal Offset");
        lblHOffset.setDisable(vFlag);
        txtVOffset = new TextField("0");
        txtHOffset = new TextField("0");
        txtHOffset.setDisable(vFlag);
        containerGP.add(lblVOffset, 0, 3);
        containerGP.add(txtVOffset, 1, 3);
        containerGP.add(lblHOffset, 0, 4);
        containerGP.add(txtHOffset, 1, 4);
        
        // selecting/deselecting Radio Buttons
        verticalRB.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                if(t1){
                    vFlag = true;
                    lblVOffset.setDisable(!vFlag);
                    txtVOffset.setDisable(!vFlag);
                    lblHOffset.setDisable(vFlag);
                    txtHOffset.setDisable(vFlag);
                    txtHOffset.setText("0");
                }
            }
        });
        horizontalRB.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                if(t1){
                    vFlag = false;
                    lblVOffset.setDisable(!vFlag);
                    txtVOffset.setDisable(!vFlag);
                    lblHOffset.setDisable(vFlag);
                    txtHOffset.setDisable(vFlag);
                    txtVOffset.setText("0");
                }
            }
        });
        
        // text change for Number of Tie Threads
        txtTieThreads.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(t1.matches("[0-9]+")){
                    numTieThreads = Integer.parseInt(t1);
                    numTieThreads = (numTieThreads<1)?1:numTieThreads;
                    numTieThreads = (numTieThreads>8)?8:numTieThreads;
                } else{
                    txtTieThreads.setText(t);
                }
            }
        });
        
        // text change for offset values
        txtVOffset.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(t1.matches("[0-9]+")){
                    vOffset = Integer.parseInt(t1);
                    //refreshImage();
                } else{
                    txtVOffset.setText(t);
                }
            }
        });
        txtHOffset.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(t1.matches("[0-9]+")){
                    hOffset = Integer.parseInt(t1);
                    //refreshImage();
                } else{
                    txtHOffset.setText(t);
                }
            }
        });
        
        // Weave Pattern Fill
        final ImageView weaveIV = new ImageView(new Image("/media/assign_weave.png"));
        weaveIV.setFitHeight(50);
        weaveIV.setFitWidth(50);
        weaveIV.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                if(t.isControlDown()){
                    try {
                        objWeave = new Weave();
                        objWeave.setObjConfiguration(objConfiguration);
                        WeaveEditView obWeaveEditView = new WeaveEditView(objWeave);
                        if(objWeave!=null && objWeave.getStrWeaveID()!=null && objWeave.getStrWeaveID()!=""){
                            spWeaveId.set(objWeave.getStrWeaveID());
                            byte[] bytArtworkThumbnil=objWeave.getBytWeaveThumbnil();
                            SeekableStream stream = new ByteArraySeekableStream(bytArtworkThumbnil);
                            String[] names = ImageCodec.getDecoderNames(stream);
                            ImageDecoder dec = ImageCodec.createImageDecoder(names[0], stream, null);
                            RenderedImage im = dec.decodeAsRenderedImage();
                            weaveIV.setImage(SwingFXUtils.toFXImage(PlanarImage.wrapRenderedImage(im).getAsBufferedImage(), null));
                            bytArtworkThumbnil=null;
                            System.gc();
                        }else{
                            spWeaveId.set("");
                            weaveIV.setImage(new Image("/media/assign_weave.png"));
                            //lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
                        }
                    } catch (Exception ex) {
                        new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                        //lblStatus.setText(objDictionaryAction.getWord("INVALIDWEAVE"));
                    }
                }                                    
            }
        });
        final Hyperlink weaveDefaultHL = new Hyperlink();
        weaveDefaultHL.setText(new DictionaryAction(objConfiguration).getWord("CHANGEDEFAULT"));
        weaveDefaultHL.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    objWeave=new Weave();
                    objWeave.setObjConfiguration(objConfiguration);
                    WeaveImportView objWeaveImportView= new WeaveImportView(objWeave);

                    if(objWeave!=null && objWeave.getStrWeaveID()!=null && objWeave.getStrWeaveID()!=""){
                        spWeaveId.set(objWeave.getStrWeaveID());
                        byte[] bytArtworkThumbnil=objWeave.getBytWeaveThumbnil();
                        SeekableStream stream = new ByteArraySeekableStream(bytArtworkThumbnil);
                        String[] names = ImageCodec.getDecoderNames(stream);
                        ImageDecoder dec = ImageCodec.createImageDecoder(names[0], stream, null);
                        RenderedImage im = dec.decodeAsRenderedImage();
                        weaveIV.setImage(SwingFXUtils.toFXImage(PlanarImage.wrapRenderedImage(im).getAsBufferedImage(), null));
                        bytArtworkThumbnil=null;
                        System.gc();
                    }else{
                        spWeaveId.set("");
                        weaveIV.setImage(new Image("/media/assign_weave.png"));
                        //lblStatus.setText(objDictionaryAction.getWord("WRONGINPUT"));
                    }
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                    //lblStatus.setText(objDictionaryAction.getWord("INVALIDWEAVE"));
                }
            }
        });
        containerGP.add(weaveIV, 0, 5);
        containerGP.add(weaveDefaultHL, 1, 5);
        
        Button btnThreadSequence = new Button("Thread Sequence");
        containerGP.add(btnThreadSequence, 0, 6, 2, 1);
        btnThreadSequence.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                try {
                    Yarn objYarn = null;
                    objYarn = new Yarn(null, "Warp", objConfiguration.getStrWarpName(), "#"+objConfiguration.getStrWarpColor(), objConfiguration.getIntWarpRepeat(), "A", 10, "English Cotton (NeC)", objConfiguration.getIntWarpPly(), objConfiguration.getIntWarpFactor(), 0.287, objConfiguration.getIntWarpTwist(), objConfiguration.getStrWarpSence(), objConfiguration.getIntWarpHairness(), objConfiguration.getIntWarpDistribution(), objConfiguration.getDblWarpPrice(), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);
                    objYarn.setObjConfiguration(objConfiguration);
                    objConfiguration.setWarpYarn(new Yarn[]{objYarn});
                    objYarn = new Yarn(null, "Weft", objConfiguration.getStrWeftName(), "#"+objConfiguration.getStrWeftColor(), objConfiguration.getIntWeftRepeat(), "a", 10, "English Cotton (NeC)", objConfiguration.getIntWeftPly(), objConfiguration.getIntWeftFactor(), 0.287, objConfiguration.getIntWeftTwist(), objConfiguration.getStrWeftSence(), objConfiguration.getIntWeftHairness(), objConfiguration.getIntWeftDistribution(), objConfiguration.getDblWeftPrice(), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);
                    objYarn.setObjConfiguration(objConfiguration);
                    objConfiguration.setWeftYarn(new Yarn[]{objYarn});
                    PatternView objPatternView = new PatternView(objConfiguration);
                    lstWarpSequenceColor=lstWeftSequenceColor=null;
                    lstWarpSequenceColor = new int[objConfiguration.getWarpYarn().length];
                    lstWeftSequenceColor = new int[objConfiguration.getWeftYarn().length];
                    // new yarns colors
                    for(int wp=0; wp<objConfiguration.getWarpYarn().length; wp++){
                        lstWarpSequenceColor[wp]=java.awt.Color.decode(objConfiguration.getWarpYarn()[wp].getStrYarnColor()).getRGB();
                    }
                    for(int wf=0; wf<objConfiguration.getWeftYarn().length; wf++){
                        lstWeftSequenceColor[wf]=java.awt.Color.decode(objConfiguration.getWeftYarn()[wf].getStrYarnColor()).getRGB();
                    }
                    spThreadSequence.set(Arrays.toString(lstWarpSequenceColor)+Arrays.toString(lstWeftSequenceColor));
                } catch (Exception ex) {
                    new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
                }
            }
        });
        
        root.setTop(containerGP);
        //root.setCenter(imageSP);
        ikatEffectStage.getIcons().add(new Image("/media/icon.png"));
        ikatEffectStage.setTitle("Ikat Effect Viewer");
        ikatEffectStage.setX(300);
        ikatEffectStage.setY(200);
        ikatEffectStage.initModality(Modality.APPLICATION_MODAL);
        ikatEffectStage.setIconified(false);
        ikatEffectStage.setResizable(false);
        ikatEffectStage.setScene(scene);
        ikatEffectStage.show();
    }
    
    /**
     * Applies Ikat Effect on input image (Vertical or Horizontal depending on vFlag)
     * @param image original input image
     * @return image with Ikat Effect
     */
    public BufferedImage getIkatEffectImage(BufferedImage image){
        BufferedImage ikatEffectImage = new BufferedImage((vFlag?image.getWidth()*numTieThreads:image.getWidth()), (vFlag?image.getHeight():image.getHeight()*numTieThreads), BufferedImage.TYPE_INT_RGB);
        BufferedImage enlargedImage = new BufferedImage((vFlag?image.getWidth()*numTieThreads:image.getWidth()), (vFlag?image.getHeight():image.getHeight()*numTieThreads), BufferedImage.TYPE_INT_RGB);
        Graphics2D g = enlargedImage.createGraphics();
        g.drawImage(image, 0, 0, enlargedImage.getWidth(), enlargedImage.getHeight(), null);
        if(vFlag){ // vertical ikat effect
            int up = (vOffset+1)/2;
            int down = vOffset/2;
            int vShiftPixels = 0;
            for(int col=0; col<enlargedImage.getWidth(); col++){
                vShiftPixels = ThreadLocalRandom.current().nextInt(-down, up+1);
                for(int row=0; row<enlargedImage.getHeight(); row++){
                    ikatEffectImage.setRGB(col, row, enlargedImage.getRGB(col, (row+vShiftPixels+enlargedImage.getHeight())%enlargedImage.getHeight()));
                }
            }
        } else{ // horizontal ikat effect
            int right = (hOffset+1)/2;
            int left  = hOffset/2;
            int hShiftPixels = 0;
            for(int row=0; row<enlargedImage.getHeight(); row++){
                hShiftPixels = ThreadLocalRandom.current().nextInt(-left, right+1);
                for(int col=0; col<enlargedImage.getWidth(); col++){
                    ikatEffectImage.setRGB(col, row, enlargedImage.getRGB((col+hShiftPixels+enlargedImage.getWidth())%enlargedImage.getWidth(), row));
                }
            }
        }
        enlargedImage = null;
        System.gc();
        // Fill Weave Pattern
        // Inputs Required:
        // 1. Background Color
        // 2. Warp Color Sequence possibly ArrayList<Color1, Color2>
        // 3. Weft Color Sequence possibly ArrayList<Color3, Color4>
        if(objWeave!=null){
            try{
                WeaveAction objWeaveAction = new WeaveAction();
                objWeaveAction.extractWeaveContent(objWeave);
                int hWeave= objWeave.getDesignMatrix().length;
                int wWeave= objWeave.getDesignMatrix()[0].length;
                for(int row=ikatEffectImage.getHeight()-1; row>=0; row--)
                    for(int col=0; col<ikatEffectImage.getWidth(); col++){
                        if(objWeave.getDesignMatrix()[(hWeave-1)-(((ikatEffectImage.getHeight()-1)-row)%hWeave)][col%wWeave]==(byte)0){
                            // warp up
                            ikatEffectImage.setRGB(col, row, lstWarpSequenceColor[col%lstWarpSequenceColor.length]);
                        } else {
                            // weft up, replace only background (not design portion)
                            if(ikatEffectImage.getRGB(col, row)==bgColorRGB)
                                ikatEffectImage.setRGB(col, row, lstWeftSequenceColor[((ikatEffectImage.getHeight()-1)-row)%lstWeftSequenceColor.length]);
                        }
                    }
            }
            catch(Exception ex){
                new Logging("SEVERE",getClass().getName(),ex.toString(),ex);
            }
        }
        return ikatEffectImage;
    }
}
