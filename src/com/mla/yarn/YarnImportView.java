/*
 * Copyright (C) Digital India Corporation (Media Lab Asia)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.mla.yarn;

import com.mla.dictionary.DictionaryAction;
import com.mla.fabric.FabricAction;
import com.mla.main.Logging;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Amit Kumar Singh
 */
public class YarnImportView {
    
    private FabricAction objFabricAction;
    private DictionaryAction objDictionaryAction;
    private Yarn objYarn;
    
    private Stage yarnStage;
    private BorderPane root;
    private Scene scene;
    GridPane container;
    private ImageView yarnImage;
    
    ArrayList<Yarn> lstYarn=new ArrayList<Yarn>();
    private TableView<Yarn> yarnTable;
    private ObservableList<Yarn> yarnData = null;
               
    public YarnImportView(final Stage primaryStage) {  }

    public YarnImportView(Yarn objYarnCall) {  
        this.objYarn = objYarnCall;
        yarnStage = new Stage();
        yarnStage.initModality(Modality.APPLICATION_MODAL);//WINDOW_MODAL
        yarnStage.initStyle(StageStyle.UTILITY);
        root = new BorderPane();
        scene = new Scene(root, objYarn.getObjConfiguration().WIDTH/2, objYarn.getObjConfiguration().HEIGHT/2, Color.WHITE);
        scene.getStylesheets().add(YarnImportView.class.getResource(objYarn.getObjConfiguration().getStrTemplate()+"/style.css").toExternalForm());
        
        yarnTable = new TableView<Yarn>();
        //yarnTable.setPrefSize(objFabric.getObjConfiguration().WIDTH/2,objFabric.getObjConfiguration().HEIGHT/4);
        try {
            YarnAction objYarnAction = new YarnAction();
            lstYarn = objYarnAction.getAllYarn(objYarn);
        } catch (SQLException ex) {
            new Logging("SEVERE",YarnImportView.class.getName(),ex.toString(),ex);
        } catch (Exception ex) {
            new Logging("SEVERE",YarnImportView.class.getName(),ex.toString(),ex);
        }  
        yarnData = FXCollections.observableArrayList(lstYarn);
        
        TableColumn selectCol = new TableColumn("Thread#");
        TableColumn typeCol = new TableColumn("Type");
        TableColumn nameCol = new TableColumn("Name");
        TableColumn colorCol = new TableColumn("Color");
        TableColumn repeatCol = new TableColumn("Repeat");
        TableColumn symbolCol = new TableColumn("Symbol");
        TableColumn countCol = new TableColumn("Count");
        TableColumn unitCol = new TableColumn("Count Unit");
        TableColumn plyCol = new TableColumn("Ply");
        TableColumn factorCol = new TableColumn("Dimensional Factor");
        TableColumn diameterCol = new TableColumn("Diameter");
        TableColumn twistCol = new TableColumn("Twist Count");
        TableColumn modelCol = new TableColumn("Twist Model");
        TableColumn hairnessCol = new TableColumn("Hairness");
        TableColumn probabilityCol = new TableColumn("Hair Percentage");
        TableColumn priceCol = new TableColumn("Price");
        selectCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("strYarnId"));
        typeCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("strYarnType"));
        nameCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("strYarnName"));
        colorCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("strYarnColor"));
        repeatCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("intYarnRepeat"));
        symbolCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("strYarnSymbol"));
        countCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("intYarnCount"));
        unitCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("strYarnCountUnit"));
        plyCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("intYarnPly"));
        factorCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("intYarnDFactor"));
        diameterCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("dblYarnDiameter"));        
        twistCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("intYarnTwist"));
        modelCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("strYarnTModel"));
        hairnessCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("intYarnHairness"));
        probabilityCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("intYarnHProbability"));
        priceCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("dblYarnPrice"));
        
        yarnTable.setItems(yarnData);
        yarnTable.setEditable(true);
        yarnTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        yarnTable.getColumns().addAll(selectCol, typeCol, nameCol, colorCol, repeatCol, symbolCol, countCol, unitCol, plyCol, factorCol, diameterCol, twistCol, modelCol, hairnessCol, probabilityCol, priceCol);
        yarnTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Yarn>() {
            @Override
            public void changed(ObservableValue<? extends Yarn> ov, Yarn t, Yarn t1) {
                try {
                    objYarn.setStrYarnId(t1.getStrYarnId());
                    objYarn.setStrYarnType(t1.getStrYarnType());
                    objYarn.setStrYarnName(t1.getStrYarnName());
                    objYarn.setStrYarnColor(t1.getStrYarnColor());
                    objYarn.setIntYarnRepeat(t1.getIntYarnRepeat());
                    objYarn.setStrYarnSymbol(t1.getStrYarnSymbol());
                    objYarn.setIntYarnCount(t1.getIntYarnCount());
                    objYarn.setStrYarnCountUnit(t1.getStrYarnCountUnit());
                    objYarn.setIntYarnPly(t1.getIntYarnPly());
                    objYarn.setIntYarnDFactor(t1.getIntYarnDFactor());
                    objYarn.setDblYarnDiameter(t1.getDblYarnDiameter());
                    objYarn.setIntYarnTwist(t1.getIntYarnTwist());
                    objYarn.setStrYarnTModel(t1.getStrYarnTModel());
                    objYarn.setIntYarnHairness(t1.getIntYarnHairness());
                    objYarn.setIntYarnHProbability(t1.getIntYarnHProbability());
                    objYarn.setDblYarnPrice(t1.getDblYarnPrice());
                    objYarn.setStrYarnAccess(t1.getStrYarnAccess());
                    objYarn.setStrYarnUser(t1.getStrYarnUser());
                    objYarn.setStrYarnDate(t1.getStrYarnDate());
                    
                    YarnAction objYarnAction = new YarnAction();
                    yarnImage.setImage(SwingFXUtils.toFXImage(objYarnAction.getYarnImage(objYarn), null));
                    System.gc();
                } catch (IOException ex) {
                    new Logging("SEVERE",YarnView.class.getName(),ex.toString(),ex);
                } catch (SQLException ex) {
                    new Logging("SEVERE",YarnView.class.getName(),ex.toString(),ex);
                }
            }
        });
        ScrollPane yarnSP = new ScrollPane();
        yarnSP.setPrefSize(objYarn.getObjConfiguration().WIDTH/2, objYarn.getObjConfiguration().HEIGHT/2);
        yarnSP.setContent(yarnTable);
        yarnSP.setId("popup");
        root.setCenter(yarnSP);
        
        yarnImage = new ImageView();
        yarnImage.setFitWidth(111);
        yarnImage.setFitHeight(objYarn.getObjConfiguration().HEIGHT/2);
        root.setRight(yarnImage);
        
        yarnStage.setScene(scene);
        yarnStage.getIcons().add(new Image("/media/icon.png"));
        yarnStage.setTitle("DigiBunai : Thread Pattern");
        yarnStage.showAndWait();
    }
}