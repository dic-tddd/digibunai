/*
 * Copyright (C) Digital India Corporation (Media Lab Asia)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mla.yarn;

import com.mla.colour.ColourSelector;
import com.mla.dictionary.DictionaryAction;
import com.mla.fabric.FabricAction;
import com.mla.main.Configuration;
import com.mla.main.Logging;
import com.mla.main.WindowView;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
/**
 *
 * @Designing GUI window for weave editor
 * @author Amit Kumar Singh
 * 
 */
public class YarnPaletteView {
   
    DictionaryAction objDictionaryAction;
    Configuration objConfiguration;
    
    private Stage yarnStage;
    private BorderPane root;    
    private Scene scene;
    private GridPane yarnHeader;
    private ImageView yarnImage;
    
    ComboBox colorCB;
    //ColorPicker color;
    ComboBox name;
    ComboBox countUnit;
    Label type;
    Label repeat;
    Label symbol;
    Label diameter;
    TextField count;
    TextField ply;
    TextField factor;    
    TextField txtTwistCount;
    TextField txtHairLength;
    TextField txtHairPercentage;
    TextField price;
    ToggleGroup twistScene;
    
    private Button btnPreview;
    private Button btnUpdate;
    private Button btnCancel;
    private Button btnClear;
    private Button btnSwitch;
    private Button btnWarpToWeft;
    private Button btnWeftToWarp;
    
    Label lblStatus;
    
    private BufferedImage bufferedImage;
    int xindex=0, yindex=0, yarnIndex=-1;
    
    public YarnPaletteView(final Stage primaryStage) {  }

    public YarnPaletteView(Configuration objConfigurationCall) {   
        this.objConfiguration = objConfigurationCall;
        objDictionaryAction = new DictionaryAction(objConfiguration);

        yarnStage = new Stage();
        yarnStage.initModality(Modality.APPLICATION_MODAL);//WINDOW_MODAL
        yarnStage.initStyle(StageStyle.UTILITY);
        root = new BorderPane();
        root.setId("popup");
        scene = new Scene(root, objConfiguration.WIDTH/1.5, objConfiguration.HEIGHT/2, Color.WHITE);
        scene.getStylesheets().add(YarnPaletteView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
        
        yarnHeader = new GridPane();
        Label warpCL = new Label(objDictionaryAction.getWord("WARP")+" "+objDictionaryAction.getWord("YARN")+": ");
        warpCL.setFont(Font.font("Arial", 16));
        yarnHeader.add(warpCL, 0, 0);
        Label weftCL = new Label(objDictionaryAction.getWord("WEFT")+" "+objDictionaryAction.getWord("YARN")+": ");
        weftCL.setFont(Font.font("Arial", 16));
        yarnHeader.add(weftCL, 0, 1);
        loadYarnPalette();
        
        GridPane yarnForm = new GridPane();
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNTYPE")), 0, 0);
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNNAME")), 0, 1);
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNCOUNT")), 0, 2);
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNUNIT")), 0, 3);
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNPLY")), 0, 4);
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNFACTOR")), 0, 5);
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNDIAMETER")), 0, 6); 
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNPRICE")), 0, 7);
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNCOLOR")), 2, 0);
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNREPEAT")), 2, 1);
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNSYMBOL")), 2, 2);
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNSENCE")), 2, 3);          
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNTWIST")), 2, 4);
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNHAIRNESS")), 2, 5);
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNDISTRIBUTION")), 2, 6);
        
        name = new ComboBox();
        name.getItems().addAll("Cotton","Silk","Wool","Jute","Linen","Flex","Hemp");  
        name.setValue("Cotton");
        yarnForm.add(name, 1, 1);
        type = new Label();
        yarnForm.add(type, 1, 0);
        count = new TextField("1"){
            @Override public void replaceText(int start, int end, String text) {
              if (text.matches("[0-9]*")) {
                super.replaceText(start, end, text);
              }
            }
            @Override public void replaceSelection(String text) {
              if (text.matches("[0-9]*")) {
                super.replaceSelection(text);
              }
            }
          };
        yarnForm.add(count, 1, 2);
        countUnit = new ComboBox();
        countUnit.getItems().addAll("Tex","dTex","K tex","Denier (Td)","New Metric (Nm)","Grains/Yard","Woollen (Aberdeen) (Ta)","Woollen (US Grain)","Asbestos (American) (NaA)","Asbestos (English) (NeA)","Cotton bump Yarn (NB)","Glass (UK & USA)","Linen (Set or Dry Spun) (NeL)","Spun Silk (Ns)","Woollen (American Cut) (Nac)","Woollen (American run) (Nar)","Woollen (Yarkshire) (Ny)","Woollen (Worsted) (New)","Linen, Hemp, Jute (Tj)","Micronaire (Mic)","Yards Per Pound (YPP)","English Worsted Count (NeK)","English Cotton (NeC)","New English (Ne)","Numero en puntos (Np)");
        countUnit.setValue("Tex");
        yarnForm.add(countUnit, 1, 3);
        ply = new TextField("1"){
            @Override public void replaceText(int start, int end, String text) {
              if (text.matches("[0-9]*")) {
                super.replaceText(start, end, text);
              }
            }
            @Override public void replaceSelection(String text) {
              if (text.matches("[0-9]*")) {
                super.replaceSelection(text);
              }
            }
          };
        yarnForm.add(ply, 1, 4);
        factor = new TextField("1"){
            @Override public void replaceText(int start, int end, String text) {
              if (text.matches("[0-9]*")) {
                super.replaceText(start, end, text);
              }
            }
            @Override public void replaceSelection(String text) {
              if (text.matches("[0-9]*")) {
                super.replaceSelection(text);
              }
            }
          };    
        yarnForm.add(factor, 1, 5);        
        diameter= new Label(); 
        yarnForm.add(diameter, 1, 6);
        price = new TextField("0");
        yarnForm.add(price, 1, 7);
        //color = new ColorPicker();
        //color.setValue(Color.web("#012345"));
        //color.setEditable(true);
        //yarnForm.add(color, 3, 0);
        colorCB=new ComboBox();
        colorCB.setStyle("-fx-background-color:#012345;");
        yarnForm.add(colorCB, 3, 0);
        repeat = new Label("1");
        yarnForm.add(repeat, 3, 1);
        symbol = new Label("");
        yarnForm.add(symbol, 3, 2);
        HBox tsHB = new HBox();
        twistScene = new ToggleGroup();
        RadioButton otsRB = new RadioButton("O");        
        otsRB.setToggleGroup(twistScene);
        otsRB.setUserData("O");
        otsRB.setSelected(true);
        RadioButton stsRB = new RadioButton("S");
        stsRB.setToggleGroup(twistScene);
        stsRB.setUserData("S");
        RadioButton ztsRB = new RadioButton("Z");
        ztsRB.setToggleGroup(twistScene);
        ztsRB.setUserData("Z");
        tsHB.getChildren().addAll(otsRB,stsRB,ztsRB);
        yarnForm.add(tsHB, 3, 3);
        txtTwistCount = new TextField("0"){
            @Override public void replaceText(int start, int end, String text) {
              if (text.matches("[0-9]*")) {
                super.replaceText(start, end, text);
              }
            }
            @Override public void replaceSelection(String text) {
              if (text.matches("[0-9]*")) {
                super.replaceSelection(text);
              }
            }
          };
        txtTwistCount.setEditable(false);
        yarnForm.add(txtTwistCount, 3, 4);
        txtHairLength = new TextField("0"){
            @Override public void replaceText(int start, int end, String text) {
              if (text.matches("[0-9]*")) {
                super.replaceText(start, end, text);
              }
            }
            @Override public void replaceSelection(String text) {
              if (text.matches("[0-9]*")) {
                super.replaceSelection(text);
              }
            }
          };
        yarnForm.add(txtHairLength, 3, 5);
        txtHairPercentage = new TextField("0"){
            @Override public void replaceText(int start, int end, String text) {
              if (text.matches("[0-9]*")) {
                super.replaceText(start, end, text);
              }
            }
            @Override public void replaceSelection(String text) {
              if (text.matches("[0-9]*")) {
                super.replaceSelection(text);
              }
            }
          };
        yarnForm.add(txtHairPercentage, 3, 6);
        
        /*color.setOnAction(new EventHandler() {
            @Override
            public void handle(Event t) {
                Color c = color.getValue();
            }
        });*/
        colorCB.setOnShown(new EventHandler() {
            @Override
            public void handle(Event t) {
                try {
                    ColourSelector objColourSelector=new ColourSelector(objConfiguration);
                    if(objColourSelector.colorCode!=null&&objColourSelector.colorCode.length()>0){
                        colorCB.setStyle("-fx-background-color:#"+objColourSelector.colorCode+";");
                    }
                    colorCB.hide();
                    t.consume();
                } catch (Exception ex) {
                    Logger.getLogger(YarnView.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        colorCB.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPYARNCOLOR")));
        countUnit.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                try {
                    FabricAction objFabricAction = new FabricAction();
                    double newCount = objFabricAction.convertUnit(t, t1, Double.parseDouble(count.getText()));
                    count.setText(Double.toString(newCount));
                    newCount = objFabricAction.convertUnit(t1, "Tex", newCount);
                    double newDiameter = objFabricAction.calculateDiameter(newCount,Double.parseDouble(ply.getText()),Double.parseDouble(factor.getText()));
                    diameter.setText(Double.toString(newDiameter));
                } catch (SQLException ex) {
                    new Logging("SEVERE",YarnPaletteView.class.getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                } catch (UnsupportedOperationException ex) {
                    new Logging("SEVERE",YarnPaletteView.class.getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        count.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                try {
                    FabricAction objFabricAction = new FabricAction();
                    double newCount = objFabricAction.convertUnit(countUnit.getValue().toString(), "Tex", Double.parseDouble(count.getText()));
                    double newDiameter = objFabricAction.calculateDiameter(newCount,Double.parseDouble(ply.getText()),Double.parseDouble(factor.getText()));
                    diameter.setText(Double.toString(newDiameter));                   
                } catch (SQLException ex) {
                    new Logging("SEVERE",YarnPaletteView.class.getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                } catch (UnsupportedOperationException ex) {
                    new Logging("SEVERE",YarnPaletteView.class.getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        ply.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                try {
                    FabricAction objFabricAction = new FabricAction();
                    double newCount = objFabricAction.convertUnit(countUnit.getValue().toString(), "Tex", Double.parseDouble(count.getText()));
                    double newDiameter = objFabricAction.calculateDiameter(newCount,Double.parseDouble(ply.getText()),Double.parseDouble(factor.getText()));
                    diameter.setText(Double.toString(newDiameter));
                } catch (SQLException ex) {
                    new Logging("SEVERE",YarnPaletteView.class.getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                } catch (UnsupportedOperationException ex) {
                    new Logging("SEVERE",YarnPaletteView.class.getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        factor.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                try {
                    FabricAction objFabricAction = new FabricAction();
                    double newCount = objFabricAction.convertUnit(countUnit.getValue().toString(), "Tex", Double.parseDouble(count.getText()));
                    double newDiameter = objFabricAction.calculateDiameter(newCount,Double.parseDouble(ply.getText()),Double.parseDouble(factor.getText()));
                    diameter.setText(Double.toString(newDiameter));
                } catch (SQLException ex) {
                    new Logging("SEVERE",YarnPaletteView.class.getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                } catch (UnsupportedOperationException ex) {
                    new Logging("SEVERE",YarnPaletteView.class.getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        twistScene.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            public void changed(ObservableValue<? extends Toggle> ov,Toggle old_toggle, Toggle new_toggle) {
              if (twistScene.getSelectedToggle() != null) {
                  char chrFabricTwistScene = twistScene.getSelectedToggle().getUserData().toString().trim().charAt(0);
                  if(chrFabricTwistScene=='S' || chrFabricTwistScene=='Z'){
                      txtTwistCount.setEditable(true);                  
                  }else{
                      txtTwistCount.setText("0");
                      txtTwistCount.setEditable(false);    
                  }
              }
            }
        });
        
        btnSwitch = new Button(objDictionaryAction.getWord("SWAPCOLOR"));
        btnWarpToWeft = new Button(objDictionaryAction.getWord("WARPTOWEFT"));
        btnWeftToWarp = new Button(objDictionaryAction.getWord("WEFTTOWARP"));
        btnClear = new Button(objDictionaryAction.getWord("CLEAR"));
        btnPreview = new Button(objDictionaryAction.getWord("PREVIEW"));
        btnUpdate = new Button(objDictionaryAction.getWord("UPDATE"));
        btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));

        //btnSwitch.setMaxWidth(Double.MAX_VALUE);
        //btnWarpToWeft.setMaxWidth(Double.MAX_VALUE);
        //btnWeftToWarp.setMaxWidth(Double.MAX_VALUE);
        //btnClear.setMaxWidth(Double.MAX_VALUE);
        //btnPreview.setMaxWidth(Double.MAX_VALUE);
        //btnUpdate.setMaxWidth(Double.MAX_VALUE);
        //btnClose.setMaxWidth(Double.MAX_VALUE);        

        btnSwitch.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/update.png"));
        btnWarpToWeft.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_right.png"));
        btnWeftToWarp.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/move_left.png"));
        btnClear.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/clear.png"));
        btnPreview.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/preview.png"));
        btnUpdate.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/update.png"));
        btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        
        btnSwitch.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPSWAPCOLOR")));
        btnWarpToWeft.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPWARPTOWEFT")));
        btnWeftToWarp.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPWEFTTOWARP")));
        btnClear.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCLEAR")));
        btnPreview.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPPREVIEW")));
        btnUpdate.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPUPDATE")));
        btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
        
        btnSwitch.setDisable(false);
        btnWarpToWeft.setDisable(false);
        btnWeftToWarp.setDisable(false);
        btnClear.setDisable(false);
        btnPreview.setDisable(true);
        btnUpdate.setDisable(true);
        btnCancel.setDefaultButton(true);
        
        btnSwitch.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {                
                yarnAction("Switch");
            }
        });
        btnWarpToWeft.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {                
                yarnAction("WarpWeft");
            }
        });
        btnWeftToWarp.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {                
                yarnAction("WeftWarp");
            }
        });
        btnClear.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {                
                yarnAction("Clear");
            }
        });
        btnPreview.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {                
                yarnAction("Preview");
            }
        });
        btnUpdate.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {                
                yarnAction("Update");
            }
        });
        btnCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                yarnAction("Cancel");
            }
        });      
        
        HBox P_buttons = new HBox(20);
        P_buttons.setAlignment(Pos.BOTTOM_RIGHT);
        P_buttons.getChildren().addAll(btnSwitch,btnWarpToWeft,btnWeftToWarp,btnClear);
        yarnHeader.add(P_buttons, 1, 2, 26, 1);

        yarnForm.add(btnPreview, 1, 8);
        yarnForm.add(btnUpdate, 2, 8);
        yarnForm.add(btnCancel, 3, 8);
        
        //yarnHeader.setPrefSize(objConfiguration.WIDTH/1.5, objConfiguration.HEIGHT/2);
        //yarnForm.setPrefSize(objConfiguration.WIDTH/1.5, objConfiguration.HEIGHT/2);
        
        yarnHeader.setId("subpopup");
        yarnForm.setId("subpopup");
        
        yarnImage = new ImageView();
        yarnImage.setFitWidth(123);
        yarnImage.setFitHeight(objConfiguration.HEIGHT/2);
        
        lblStatus = new Label(objDictionaryAction.getWord("SELECTYARNUPDATE"));
        lblStatus.setStyle("-fx-text-fill: #FF0000;");
        
        root.setTop(yarnHeader);
        root.setRight(yarnImage);        
        root.setCenter(yarnForm);
        //root.setBottom(lblStatus);
        /*
        VBox container=new VBox();
        container.getChildren().addAll(yarnHeader,yarnForm);
        container.setAlignment(Pos.CENTER);
        //container.setGridLinesVisible(true);
        container.autosize();       
        */
        yarnStage = new Stage(); 
        yarnStage.setScene(scene);
        yarnStage.initModality(Modality.APPLICATION_MODAL);//WINDOW_MODAL
        yarnStage.initStyle(StageStyle.UTILITY); 
        yarnStage.getIcons().add(new Image("/media/icon.png"));
        yarnStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWPATTERNEDITOR")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
        yarnStage.setIconified(false);
        yarnStage.setResizable(false);
        yarnStage.showAndWait();
    }
    
    public void yarnAction(String actionName) {        
        if (actionName.equalsIgnoreCase("Switch")) {
            try {
                Yarn temp;String type, symbol; 
                for(int i =0; i<26; i++){
                    type = objConfiguration.getYarnPalette()[i].getStrYarnType();
                    symbol = objConfiguration.getYarnPalette()[i].getStrYarnSymbol();
                    
                    objConfiguration.getYarnPalette()[i].setStrYarnType(objConfiguration.getYarnPalette()[i+26].getStrYarnType());
                    objConfiguration.getYarnPalette()[i].setStrYarnSymbol(objConfiguration.getYarnPalette()[i+26].getStrYarnSymbol());
                    
                    objConfiguration.getYarnPalette()[i+26].setStrYarnType(type);
                    objConfiguration.getYarnPalette()[i+26].setStrYarnSymbol(symbol);
                    
                    temp = objConfiguration.getYarnPalette()[i];
                    objConfiguration.getYarnPalette()[i]=objConfiguration.getYarnPalette()[i+26];
                    objConfiguration.getYarnPalette()[i+26]=temp;
                }
                loadYarnPalette();
            } catch (Exception ex) {
                new Logging("SEVERE",YarnPaletteView.class.getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        }
        if (actionName.equalsIgnoreCase("WarpWeft")) {
            try {
                String type, symbol;
                for(int i =0; i<26; i++){
                    Yarn objYarn = objConfiguration.getYarnPalette()[i+26];

                    objYarn.setStrYarnType(objConfiguration.getYarnPalette()[i].getStrYarnType());
                    objYarn.setStrYarnName(objConfiguration.getYarnPalette()[i+26].getStrYarnName());
                    objYarn.setStrYarnColor(objConfiguration.getYarnPalette()[i].getStrYarnColor());
                    objYarn.setIntYarnRepeat(objConfiguration.getYarnPalette()[i].getIntYarnRepeat());
                    objYarn.setStrYarnSymbol(objConfiguration.getYarnPalette()[i+26].getStrYarnSymbol());
                    objYarn.setIntYarnCount(objConfiguration.getYarnPalette()[i].getIntYarnCount());
                    objYarn.setStrYarnCountUnit(objConfiguration.getYarnPalette()[i].getStrYarnCountUnit());
                    objYarn.setIntYarnPly(objConfiguration.getYarnPalette()[i].getIntYarnPly());
                    objYarn.setIntYarnDFactor(objConfiguration.getYarnPalette()[i].getIntYarnDFactor());
                    objYarn.setDblYarnDiameter(objConfiguration.getYarnPalette()[i].getDblYarnDiameter());
                    objYarn.setIntYarnTwist(objConfiguration.getYarnPalette()[i].getIntYarnTwist());
                    objYarn.setStrYarnTModel(objConfiguration.getYarnPalette()[i].getStrYarnTModel());
                    objYarn.setIntYarnHairness(objConfiguration.getYarnPalette()[i].getIntYarnHairness());
                    objYarn.setIntYarnHProbability(objConfiguration.getYarnPalette()[i].getIntYarnHProbability());
                    objYarn.setDblYarnPrice(objConfiguration.getYarnPalette()[i].getDblYarnPrice());
                    objYarn.setStrYarnAccess(objConfiguration.getYarnPalette()[i].getStrYarnAccess());
                    objYarn.setStrYarnUser(objConfiguration.getYarnPalette()[i].getStrYarnUser());
                    objYarn.setStrYarnDate(objConfiguration.getYarnPalette()[i].getStrYarnDate());
                    
                    objConfiguration.getYarnPalette()[i+26]=objYarn;
                }
                loadYarnPalette();
            } catch (Exception ex) {
                new Logging("SEVERE",YarnPaletteView.class.getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        }
        if (actionName.equalsIgnoreCase("WeftWarp")) {
            try {
                String type, symbol;
                for(int i =0; i<26; i++){
                    Yarn objYarn = objConfiguration.getYarnPalette()[i];

                    objYarn.setStrYarnType(objConfiguration.getYarnPalette()[i+26].getStrYarnType());
                    objYarn.setStrYarnName(objConfiguration.getYarnPalette()[i].getStrYarnName());
                    objYarn.setStrYarnColor(objConfiguration.getYarnPalette()[i+26].getStrYarnColor());
                    objYarn.setIntYarnRepeat(objConfiguration.getYarnPalette()[i+26].getIntYarnRepeat());
                    objYarn.setStrYarnSymbol(objConfiguration.getYarnPalette()[i].getStrYarnSymbol());
                    objYarn.setIntYarnCount(objConfiguration.getYarnPalette()[i+26].getIntYarnCount());
                    objYarn.setStrYarnCountUnit(objConfiguration.getYarnPalette()[i+26].getStrYarnCountUnit());
                    objYarn.setIntYarnPly(objConfiguration.getYarnPalette()[i+26].getIntYarnPly());
                    objYarn.setIntYarnDFactor(objConfiguration.getYarnPalette()[i+26].getIntYarnDFactor());
                    objYarn.setDblYarnDiameter(objConfiguration.getYarnPalette()[i+26].getDblYarnDiameter());
                    objYarn.setIntYarnTwist(objConfiguration.getYarnPalette()[i+26].getIntYarnTwist());
                    objYarn.setStrYarnTModel(objConfiguration.getYarnPalette()[i+26].getStrYarnTModel());
                    objYarn.setIntYarnHairness(objConfiguration.getYarnPalette()[i+26].getIntYarnHairness());
                    objYarn.setIntYarnHProbability(objConfiguration.getYarnPalette()[i+26].getIntYarnHProbability());
                    objYarn.setDblYarnPrice(objConfiguration.getYarnPalette()[i+26].getDblYarnPrice());
                    objYarn.setStrYarnAccess(objConfiguration.getYarnPalette()[i+26].getStrYarnAccess());
                    objYarn.setStrYarnUser(objConfiguration.getYarnPalette()[i+26].getStrYarnUser());
                    objYarn.setStrYarnDate(objConfiguration.getYarnPalette()[i+26].getStrYarnDate());
                    
                    objConfiguration.getYarnPalette()[i]=objYarn;
                }
                loadYarnPalette();
            } catch (Exception ex) {
                new Logging("SEVERE",YarnPaletteView.class.getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        }
        if (actionName.equalsIgnoreCase("Clear")) {
            try {
                objConfiguration.initYarnPalette();
                loadYarnPalette();
            } catch (Exception ex) {
                new Logging("SEVERE",YarnPaletteView.class.getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        }
        if (actionName.equalsIgnoreCase("Preview")) {
            try {
                //Yarn objYarn = new Yarn(null, type.getText(), name.getValue().toString(), toRGBCode((Color)color.getValue()), Integer.parseInt(repeat.getText()), symbol.getText(), Integer.parseInt(count.getText()), countUnit.getValue().toString(), Integer.parseInt(ply.getText()), Integer.parseInt(factor.getText()), Double.parseDouble(diameter.getText()), Integer.parseInt(txtTwistCount.getText()), twistScene.getSelectedToggle().getUserData().toString(), Integer.parseInt(txtHairLength.getText()), Integer.parseInt(txtHairPercentage.getText()), Double.parseDouble(price.getText()), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);
                Yarn objYarn = new Yarn(null, type.getText(), name.getValue().toString(), colorCB.getStyle().substring(colorCB.getStyle().lastIndexOf("#"), colorCB.getStyle().lastIndexOf("#")+7), Integer.parseInt(repeat.getText()), symbol.getText(), Integer.parseInt(count.getText()), countUnit.getValue().toString(), Integer.parseInt(ply.getText()), Integer.parseInt(factor.getText()), Double.parseDouble(diameter.getText()), Integer.parseInt(txtTwistCount.getText()), twistScene.getSelectedToggle().getUserData().toString(), Integer.parseInt(txtHairLength.getText()), Integer.parseInt(txtHairPercentage.getText()), Double.parseDouble(price.getText()), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);
                objYarn.setObjConfiguration(objConfiguration);
                YarnAction objYarnAction = new YarnAction();
                yarnImage.setImage(SwingFXUtils.toFXImage(objYarnAction.getYarnImage(objYarn), null));                    
            } catch (SQLException ex) {
                new Logging("SEVERE",YarnPaletteView.class.getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            } catch (IOException ex) {
                new Logging("SEVERE",YarnPaletteView.class.getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        }
        if (actionName.equalsIgnoreCase("Update")) {
            try {
                if(yarnIndex>=0){
                    Yarn objYarn = objConfiguration.getYarnPalette()[yarnIndex];

                    objYarn.setStrYarnType(type.getText());
                    objYarn.setStrYarnName(name.getValue().toString());
                    //objYarn.setStrYarnColor(toRGBCode((Color)color.getValue()));
                    objYarn.setStrYarnColor(colorCB.getStyle().substring(colorCB.getStyle().lastIndexOf("#"), colorCB.getStyle().lastIndexOf("#")+7));
                    objYarn.setIntYarnRepeat(Integer.parseInt(repeat.getText()));
                    objYarn.setStrYarnSymbol(symbol.getText());
                    objYarn.setIntYarnCount(Integer.parseInt(count.getText()));
                    objYarn.setStrYarnCountUnit(countUnit.getValue().toString());
                    objYarn.setIntYarnPly(Integer.parseInt(ply.getText()));
                    objYarn.setIntYarnDFactor(Integer.parseInt(factor.getText()));
                    objYarn.setDblYarnDiameter(Double.parseDouble(diameter.getText()));
                    objYarn.setIntYarnTwist(Integer.parseInt(txtTwistCount.getText()));
                    objYarn.setStrYarnTModel(twistScene.getSelectedToggle().getUserData().toString());
                    objYarn.setIntYarnHairness(Integer.parseInt(txtHairLength.getText()));
                    objYarn.setIntYarnHProbability(Integer.parseInt(txtHairPercentage.getText()));
                    objYarn.setDblYarnPrice(Double.parseDouble(price.getText()));
                    //objYarn.setStrYarnAccess(objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"));
                    //objYarn.setStrYarnUser(objConfiguration.getObjUser().getStrUserID());
                    //objYarn.setStrYarnDate(null);
                    //objYarn.setObjConfiguration(objConfiguration);

                    objConfiguration.getYarnPalette()[yarnIndex] = objYarn;
                    loadYarnPalette();
                }
            } catch (Exception ex) {
                new Logging("SEVERE",YarnPaletteView.class.getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        }
        if (actionName.equalsIgnoreCase("Cancel")) {
            System.gc();
            yarnStage.close();
        }
    }
    
    private void loadYarnPalette(){
        for(byte i=0; i<(byte)objConfiguration.getYarnPalette().length; i++){
            System.out.println(i+":"+objConfiguration.getYarnPalette()[i].getStrYarnSymbol());
            Label lblC= new Label(objConfiguration.getYarnPalette()[i].getStrYarnSymbol());
            lblC.setUserData(i);
            setLabelPropertyEvent(lblC, objConfiguration.getYarnPalette()[i]);
            yarnHeader.add(lblC, (i%26)+1, (i/26));
        }
    }
    
    public void setLabelPropertyEvent(final Label lblC, final Yarn objYarn){
        lblC.setText(objYarn.getStrYarnSymbol());
        lblC.setStyle("-fx-background-color: "+objYarn.getStrYarnColor()+"; -fx-font-size: 16; -fx-width:25px; -fx-height:30px; -fx-border-width: 1 1 1 1; -fx-border-color: #000000; ");
        lblC.setPrefSize(30, 30);
        
        lblC.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                colorCB.setStyle("-fx-background-color:"+objYarn.getStrYarnColor()+";");
                //color.setValue(Color.web(objYarn.getStrYarnColor()));
                twistScene.equals(objYarn.getStrYarnTModel());
                countUnit.setValue(objYarn.getStrYarnCountUnit());
                type.setText(objYarn.getStrYarnType());
                name.setValue(objYarn.getStrYarnName());
                repeat.setText(Integer.toString(objYarn.getIntYarnRepeat()));
                symbol.setText(objYarn.getStrYarnSymbol());
                diameter.setText(Double.toString(objYarn.getDblYarnDiameter()));
                count.setText(Integer.toString(objYarn.getIntYarnCount()));
                ply.setText(Integer.toString(objYarn.getIntYarnPly()));
                factor.setText(Integer.toString(objYarn.getIntYarnDFactor()));
                txtTwistCount.setText(Integer.toString(objYarn.getIntYarnTwist()));
                txtHairLength.setText(Integer.toString(objYarn.getIntYarnHairness()));
                txtHairPercentage.setText(Integer.toString(objYarn.getIntYarnHProbability()));
                price.setText(Double.toString(objYarn.getDblYarnPrice()));
                yarnIndex = Integer.parseInt(lblC.getUserData().toString());
                System.err.println(symbol.getText()+"*"+objYarn.getStrYarnSymbol());
    
                objYarn.setObjConfiguration(objConfiguration);
                
                btnPreview.setDisable(false);
                btnUpdate.setDisable(false);
                
                YarnAction objYarnAction;
                try {
                    objYarnAction = new YarnAction();
                    yarnImage.setImage(SwingFXUtils.toFXImage(objYarnAction.getYarnImage(objYarn), null)); 
                } catch (SQLException ex) {
                    Logger.getLogger(YarnPaletteView.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(YarnPaletteView.class.getName()).log(Level.SEVERE, null, ex);
                }
           }
        });
        lblC.setOnDragDetected(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                /* drag was detected, start a drag-and-drop gesture*/
                /* allow any transfer mode */
                Dragboard db = lblC.startDragAndDrop(TransferMode.ANY);

                /* Put a string on a dragboard */
                ClipboardContent content = new ClipboardContent();
                content.putString(lblC.getStyle().substring(lblC.getStyle().lastIndexOf("-fx-background-color:")+21,lblC.getStyle().indexOf(";")).trim());
                db.setContent(content);
                xindex = (byte)Integer.parseInt(lblC.getUserData().toString());
                event.consume();
            }
        });
        lblC.setOnDragOver(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                /* data is dragged over the target */
                /* accept it only if it is not dragged from the same node 
                 * and if it has a string data */
                if (event.getGestureSource() != lblC && event.getDragboard().hasString()) {
                    /* allow for both copying and moving, whatever user chooses */
                    event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                }
                event.consume();
            }
        });
        lblC.setOnDragEntered(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
            /* the drag-and-drop gesture entered the target */
            /* show to the user that it is an actual gesture target */
                 if (event.getGestureSource() != lblC && event.getDragboard().hasString()) {
                     lblC.setTextFill(Color.GREEN);
                 }
                 event.consume();
            }
        });
        lblC.setOnDragExited(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                /* mouse moved away, remove the graphical cues */
                lblC.setTextFill(Color.BLACK);
                event.consume();
            }
        });
        lblC.setOnDragDropped(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                /* data dropped */
                /* if there is a string data on dragboard, read it and use it */
                Dragboard db = event.getDragboard();
                boolean success = false;
                if (db.hasString()) {                    
                   ClipboardContent content = new ClipboardContent();
                   content.putString(lblC.getStyle().substring(lblC.getStyle().lastIndexOf("-fx-background-color:")+21,lblC.getStyle().indexOf(";")).trim());                
                   lblC.setStyle("-fx-background-color: "+db.getString()+"; -fx-font-size: 10; -fx-width:25px; -fx-height:25px;");
                   yindex = (byte)Integer.parseInt(lblC.getUserData().toString());
                   
                   db.setContent(content);                   
                   success = true;                   
                }
                /* let the source know whether the string was successfully 
                 * transferred and used */
                event.setDropCompleted(success);
                event.consume();
             }
        });
        lblC.setOnDragDone(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                /* the drag and drop gesture ended */
                /* if the data was successfully moved, clear it */
                if (event.getTransferMode() == TransferMode.MOVE) {
                    Dragboard db = event.getDragboard();
                    // swapThreadPatterns(lblC.getStyle().substring(lblC.getStyle().lastIndexOf("-fx-background-color:")+21,lblC.getStyle().indexOf(";")).trim(),db.getString());              
                    lblC.setStyle("-fx-background-color: "+db.getString()+"; -fx-font-size: 10; -fx-width:25px; -fx-height:25px;");
                    swapThreadPalette();
                }
                event.consume();
            }
        });
    }
    
    private void swapThreadPalette(){
        Yarn temp;String type, symbol; 
        System.out.println(xindex+"="+yindex);
        if(xindex>-1 && yindex>-1){
                type = objConfiguration.getYarnPalette()[xindex].getStrYarnType();
                symbol = objConfiguration.getYarnPalette()[xindex].getStrYarnSymbol();

                objConfiguration.getYarnPalette()[xindex].setStrYarnType(objConfiguration.getYarnPalette()[yindex].getStrYarnType());
                objConfiguration.getYarnPalette()[xindex].setStrYarnSymbol(objConfiguration.getYarnPalette()[yindex].getStrYarnSymbol());

                objConfiguration.getYarnPalette()[yindex].setStrYarnType(type);
                objConfiguration.getYarnPalette()[yindex].setStrYarnSymbol(symbol);

                temp = objConfiguration.getYarnPalette()[xindex];
                objConfiguration.getYarnPalette()[xindex]=objConfiguration.getYarnPalette()[yindex];
                objConfiguration.getYarnPalette()[yindex]=temp;
        }
        loadYarnPalette();
    }

    public String toRGBCode( Color color ){
        return String.format( "#%02X%02X%02X",
            (int)( color.getRed() * 255 ),
            (int)( color.getGreen() * 255 ),
            (int)( color.getBlue() * 255 ) );
    }
    
    public void start(Stage stage) throws Exception {
        stage.initOwner(WindowView.windowStage);
        new YarnPaletteView(stage);        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

