/*
 * Copyright (C) 2017 HP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.mla.yarn;

import com.mla.main.DbConnect;
import com.mla.main.IDGenerator;
import com.mla.main.Logging;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author HP
 */
public class YarnAction {
        
    Connection connection = null; //DbConnect.getConnection();
    
    public YarnAction() throws SQLException{
        connection = DbConnect.getConnection();
    }
    
    public YarnAction(boolean isDB) throws SQLException{
        if(isDB)
            connection = DbConnect.getConnection();        
    }
    
    public YarnAction(Yarn objYarnCall, boolean isDB) throws SQLException{
        if(isDB)
            connection = DbConnect.getConnection();
        //objYarn = objYarnCall;
    }

    /**
     * countYarnAccess
     * <p>
     * This method is used for counting access in Yarn.
     *
     * @author      Amit Kumar Singh
     * @version     %I%, %G%
     * @since       1.0
     * @date        07/01/2016
     * @Designing   method is used for counting access in Yarn.
     * @see         java.sql.*;
     * @link        com.mla.main.DbConnect
     * @link        com.mla.main.Logging
     * @exception   Exception
     * @return      countAccess [MAP<STRING,Integer>] return total number of items in each access group 
     *              <code>null</code> otherwise.
     */
    public Map countYarnAccess(){
        Statement oStatement =null;
        ResultSet oResultSet= null;
        String strQuery=null;
        Map countAccess = null;
        new Logging("INFO",this.getClass().getName(),"<<<<<<<<<<< countYarnAccess() >>>>>>>>>>>",null);
        try {           
            strQuery = "SELECT ACCESS, COUNT(*) AS COUNTING from mla_yarn_library GROUP BY ACCESS;";
            oStatement = connection.createStatement();
            oResultSet = oStatement.executeQuery(strQuery);
            countAccess = new TreeMap();
            while(oResultSet.next()) {
                countAccess.put(new IDGenerator().getUserAcessValueData("YARN_LIBRARY",oResultSet.getString("ACCESS")), oResultSet.getInt("COUNTING"));
            }            
        } catch (Exception ex) {
            new Logging("SEVERE",this.getClass().getName(),"countYarnAccess() : "+strQuery,ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oStatement!=null) {
                    oStatement.close();
                    oStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",this.getClass().getName(),"countYarnAccess() : Error while closing connection"+e,ex);
                }
            }
        }
        new Logging("INFO",this.getClass().getName(),">>>>>>>>>>> countYarnAccess() <<<<<<<<<<<"+countAccess,null);
        return countAccess;
    }
    public int countYarnUsage(String strYarnID){
        Statement oStatement =null;
        ResultSet oResultSet= null;
        String strQuery=null;
        int count = 0;
        try {           
            //System.out.println("<<<<<< countYarnUsage >>>>>>");
            strQuery = "SELECT COUNT(*) from mla_fabric_base_library WHERE `YARNID` = '"+strYarnID+"' ORDER BY `YARNID` DESC;";
            oStatement = connection.createStatement();
            oResultSet = oStatement.executeQuery(strQuery);  
            while(oResultSet.next()) {
                count += oResultSet.getInt(1);
            }
            strQuery = "SELECT COUNT(*) from mla_fabric_thread WHERE `yarn_id` = '"+strYarnID+"' ORDER BY `yarn_id` DESC;";
            oStatement = connection.createStatement();
            oResultSet = oStatement.executeQuery(strQuery);  
            while(oResultSet.next()) {
                count += oResultSet.getInt(1);
            }
            strQuery = "SELECT COUNT(*) from tmp_fabric_thread WHERE `yarn_id` = '"+strYarnID+"' ORDER BY `yarn_id` DESC;";
            oStatement = connection.createStatement();
            oResultSet = oStatement.executeQuery(strQuery);  
            while(oResultSet.next()) {
                count += oResultSet.getInt(1);
            }
        } catch (Exception ex) {
            new Logging("SEVERE",YarnAction.class.getName(),"countYarnUsage() : "+strQuery,ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oStatement!=null) {
                    oStatement.close();
                    oStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",YarnAction.class.getName(),"countYarnUsage() : Error while closing connection"+e,ex);
                }
            }
        }
        //System.out.println("<<<<<< countYarnUsage >>>>>>"+count);
        return count;
    }

    public Yarn[] getFabricYarn(String strFKID,String type, String strTableType) {
        Statement oStatement =null;
        ResultSet oResultSet= null;
        String strQuery=null;
        Yarn[] yarns=null;
        try {           
            String strTable = "mla_fabric_thread";
            if(strTableType.equalsIgnoreCase("Tmp"))
                strTable = "tmp_fabric_thread";
            
            System.out.println("<<<<<< getFabricYarn >>>>>>");
            strQuery = "SELECT * FROM `mla_yarn_library` LEFT JOIN `"+strTable+"` as fabric_thread ON fabric_thread.`yarn_id` = `mla_yarn_library`.`id` WHERE fabric_id='"+strFKID+"' AND type='"+type+"' ORDER BY `serial` ASC;";
            //SELECT t.yarn_id, t.repeat, yarn_library.* FROM fabric_thread t JOIN generator_256 i ON i.n between 1 and t.repeat JOIN yarn_library on yarn_library.id = t.yarn_id order by t.yarn_id, i.n;
            oStatement = connection.createStatement();
            oResultSet = oStatement.executeQuery(strQuery);
            oResultSet.last();
            //System.out.println("Row= "+oResultSet.getRow());
            yarns = new Yarn[oResultSet.getRow()];
            int i = 0;
            oResultSet.beforeFirst();
            while(oResultSet.next()) {
                yarns[i] = new Yarn(
                        oResultSet.getString("ID").toString(),
                        type,
                        oResultSet.getString("NAME").toString(),
                        oResultSet.getString("COLOR").toString(),
                        oResultSet.getInt("REPEAT"),
                        oResultSet.getString("SYMBOL").toString(),
                        oResultSet.getInt("COUNT"),
                        oResultSet.getString("UNIT").toString(),
                        oResultSet.getInt("PLY"),
                        oResultSet.getInt("DFACTOR"),
                        Double.parseDouble(oResultSet.getString("DIAMETER").toString()),
                        oResultSet.getInt("TWIST"),
                        oResultSet.getString("TMODEL").toString(),
                        oResultSet.getInt("HAIRNESS"),
                        oResultSet.getInt("HPROBABILITY"),
                        Double.parseDouble(oResultSet.getString("PRICE").toString()),
                        oResultSet.getString("ACCESS"),
                        oResultSet.getString("USERID"),
                        oResultSet.getTimestamp("UDATE").toString()
                );
                i++;
            }
        } catch (Exception ex) {
            new Logging("SEVERE",YarnAction.class.getName(),"getFabricYarn : "+strQuery,ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oStatement!=null) {
                    oStatement.close();
                    oStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",YarnAction.class.getName(),"getFabricYarn() : Error while closing connection"+e,ex);
                }
            }
        }
        System.out.println("<<<<<< getFabricYarn >>>>>>");
        return yarns;
    }
    
    public byte setFabricYarn(String strFKID, Yarn objYarn,int i, String strTableType) {
        PreparedStatement oPreparedStatement =null;
        ResultSet oResultSet= null;
        byte oResult= 0;
        String strQuery=null;
        String yarnId = null;
        try {           
            String strTable = "mla_fabric_thread";
            if(strTableType.equalsIgnoreCase("Tmp"))
                strTable = "tmp_fabric_thread";
            
            System.out.println("<<<<<< setFabricYarn >>>>>>");
            strQuery = "INSERT INTO `"+strTable+"` (`fabric_id`, `yarn_id`, `symbol`, `repeat`,`serial`) VALUES (?,?,?,?,?);";
            oPreparedStatement = connection.prepareStatement(strQuery);
            oPreparedStatement.setString(1, strFKID);
            oPreparedStatement.setString(2, objYarn.getStrYarnId());
            oPreparedStatement.setString(3, objYarn.getStrYarnSymbol());
            oPreparedStatement.setInt(4, objYarn.getIntYarnRepeat());
            oPreparedStatement.setInt(5, i);
            oResult = (byte)oPreparedStatement.executeUpdate();
        } catch (Exception ex) {
            new Logging("SEVERE",YarnAction.class.getName(),"addFabricThread : "+strQuery,ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oPreparedStatement!=null) {
                    oPreparedStatement.close();
                    oPreparedStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",YarnAction.class.getName(),"addFabricThread() : Error while closing connection"+e,ex);
                }
            }
        }
        System.out.println(">>>>>> setFabricYarn <<<<<<");
        return oResult;
    }
    
    public boolean clearFabricYarn(String strFKID, String strTableType) {
        PreparedStatement oPreparedStatement =null;
        ResultSet oResultSet= null;
        boolean oResult= false;
        String strQuery=null;
        String yarnId = null;
        try {           
            String strTable = "mla_fabric_thread";
            if(strTableType.equalsIgnoreCase("Tmp"))
                strTable = "tmp_fabric_thread";
            
            System.out.println("<<<<<< clearFabricYarn >>>>>>");
            strQuery = "DELETE FROM `"+strTable+"` WHERE `fabric_id` = ?;";
            oPreparedStatement = connection.prepareStatement(strQuery);
            oPreparedStatement.setString(1, strFKID);
            oResult = oPreparedStatement.execute();
        } catch (Exception ex) {
            new Logging("SEVERE",YarnAction.class.getName(),"clearFabricYarn : "+strQuery,ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oPreparedStatement!=null) {
                    oPreparedStatement.close();
                    oPreparedStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",YarnAction.class.getName(),"clearFabricYarn() : Error while closing connection"+e,ex);
                }
            }
        }
        System.out.println("<<<<<< clearFabricYarn >>>>>>");
        return oResult;
    }

    /**************************** Yarn ******************************************/
    public List lstImportYarn(Yarn objYarn) {
        Statement oStatement =null;
        ResultSet oResultSet= null;
        String strQuery=null;
        List lstYarnDeatails=null, lstYarn;
        new Logging("INFO",YarnAction.class.getName(),"<<<<<<<<<<< lstImportYarn() >>>>>>>>>>>",null);            
        try {           
            String cond = "1";
            String orderBy ="NAME ";
            if(!objYarn.getObjConfiguration().getObjUser().getStrUserID().equals("ADMIN")){                    
               cond = "(USERID = '"+objYarn.getObjConfiguration().getObjUser().getStrUserID()+"' OR `ACCESS`='"+new IDGenerator().getUserAcess("YARN_LIBRARY")+"') ";
            }
            if(!(objYarn.getStrCondition().trim().equals(""))) {
                cond += " AND `NAME` LIKE '"+objYarn.getStrCondition().trim()+"%'";
            }
            if(objYarn.getStrSearchBy().equals("All")) {
                cond+=" ";
            } else{
                cond += " AND `TYPE` LIKE '%"+objYarn.getStrSearchBy().trim()+"%'";
            } 
            if(objYarn.getStrOrderBy().equals("Name")) {
                orderBy = "`NAME`";
            } else if(objYarn.getStrOrderBy().equals("Type")) {
                orderBy = "`TYPE`";
            } else if(objYarn.getStrOrderBy().equals("Ply")) {
                orderBy = "`PLY`";
            } else if(objYarn.getStrOrderBy().equals("Diameter")) {
                orderBy = "`DIAMETER`";
            } else if(objYarn.getStrOrderBy().equals("Price")) {
                orderBy = "`PRICE`";
            }
            if(objYarn.getStrDirection().equals("Ascending")) {
                orderBy += " ASC";
            } else if(objYarn.getStrDirection().equals("Descending")) {
                orderBy += " DESC";
            }
            
            strQuery = "SELECT * FROM `mla_yarn_library` WHERE "+cond+" ORDER BY "+orderBy;
            if(objYarn.getStrLimit()!=null)
                strQuery+=" LIMIT "+objYarn.getStrLimit();
            oStatement = connection.createStatement();
            oResultSet = oStatement.executeQuery(strQuery);
            lstYarnDeatails = new ArrayList();            
            while(oResultSet.next()) {
                lstYarn = new ArrayList();            
                lstYarn.add(oResultSet.getString("ID").toString());
                lstYarn.add(oResultSet.getString("NAME"));
                lstYarn.add(oResultSet.getString("TYPE"));
                lstYarn.add(oResultSet.getString("COLOR"));
                lstYarn.add(oResultSet.getInt("COUNT"));
                lstYarn.add(oResultSet.getString("UNIT"));
                lstYarn.add(oResultSet.getInt("PLY"));
                lstYarn.add(oResultSet.getInt("DFACTOR"));
                lstYarn.add(Double.parseDouble(oResultSet.getString("DIAMETER")));
                lstYarn.add(oResultSet.getInt("TWIST"));
                lstYarn.add(oResultSet.getString("TMODEL"));
                lstYarn.add(oResultSet.getInt("HAIRNESS"));
                lstYarn.add(oResultSet.getInt("HPROBABILITY"));
                lstYarn.add(Double.parseDouble(oResultSet.getString("PRICE")));
                lstYarn.add(oResultSet.getBytes("ICON"));
                lstYarn.add(oResultSet.getString("ACCESS"));
                lstYarn.add(oResultSet.getString("USERID"));
                lstYarn.add(oResultSet.getString("UDATE"));
                lstYarnDeatails.add(lstYarn);              
            }
        } catch (Exception ex) {
            new Logging("SEVERE",YarnAction.class.getName(),"lstImportYarn : "+strQuery,ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oStatement!=null) {
                    oStatement.close();
                    oStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",YarnAction.class.getName(),"lstImportYarn() : Error while closing connection"+e,ex);
                }
            }
        }
        new Logging("INFO",YarnAction.class.getName(),">>>>>>>>>>> lstImportYarn() <<<<<<<<<<<"+strQuery,null);
        return lstYarnDeatails;
    }
    
    public ArrayList<Yarn> getAllYarn(Yarn objYarn) {
        Statement oStatement =null;
        ResultSet oResultSet= null;
        String strQuery=null;
        ArrayList<Yarn> lstYarn = null;  
        try {
            System.out.println("<<<<<< getAllYarn >>>>>>");
            String cond = "(USERID = '"+objYarn.getObjConfiguration().getObjUser().getStrUserID()+"' OR `ACCESS`='"+new IDGenerator().getUserAcess("YARN_LIBRARY")+"') ";
            String orderBy ="NAME ";
            if(!(objYarn.getStrCondition().trim().equals(""))) {
                cond += " AND `NAME` LIKE '"+objYarn.getStrCondition().trim()+"%'";
            }
            if(!(objYarn.getStrSearchBy().trim().equals(""))) {
                cond += " AND `TYPE` LIKE '%"+objYarn.getStrSearchBy().trim()+"%'";
            }
            if(objYarn.getStrOrderBy().equals("Name")) {
                orderBy = "`NAME`;";
            } else if(objYarn.getStrOrderBy().equals("Type")) {
                orderBy = "`TYPE`";
            }
            strQuery = "SELECT * FROM `mla_yarn_library` WHERE "+cond+" ORDER BY "+orderBy;
            oStatement = connection.createStatement();
            oResultSet = oStatement.executeQuery(strQuery);
            lstYarn = new ArrayList<Yarn>();       
            while(oResultSet.next()) {
                lstYarn.add(new Yarn(
                        oResultSet.getString("ID").toString(),
                        oResultSet.getString("NAME"),
                        oResultSet.getString("TYPE"),
                        oResultSet.getString("COLOR"),
                        1,
                        "#",
                        oResultSet.getInt("COUNT"),
                        oResultSet.getString("UNIT"),
                        oResultSet.getInt("PLY"),
                        oResultSet.getInt("DFACTOR"),
                        Double.parseDouble(oResultSet.getString("DIAMETER")),
                        oResultSet.getInt("TWIST"),
                        oResultSet.getString("TMODEL"),
                        oResultSet.getInt("HAIRNESS"),
                        oResultSet.getInt("HPROBABILITY"),
                        Double.parseDouble(oResultSet.getString("PRICE")),
                        oResultSet.getString("ACCESS"),
                        oResultSet.getString("USERID"),
                        oResultSet.getString("UDATE")
                ));
            }
        } catch (Exception ex) {
            new Logging("SEVERE",YarnAction.class.getName(),"getAllYarn : "+strQuery,ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oStatement!=null) {
                    oStatement.close();
                    oStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",YarnAction.class.getName(),"getAllYarn() : Error while closing connection"+e,ex);
                }
            }
        }
        System.out.println("<<<<<< getAllYarn >>>>>>");
        return lstYarn;
    }

    public void getYarn(Yarn objYarn) {
        Statement oStatement =null;
        ResultSet oResultSet= null;
        String strQuery=null;
        new Logging("INFO",YarnAction.class.getName(),"<<<<<<<<<<< getYarn() >>>>>>>>>>>",null);            
        try {
            strQuery = "SELECT * FROM `mla_yarn_library` WHERE id='"+objYarn.getStrYarnId()+"';";
            oStatement = connection.createStatement();
            oResultSet = oStatement.executeQuery(strQuery);
             if(oResultSet.next()){
                objYarn.setStrYarnId(oResultSet.getString("ID").toString());
                objYarn.setStrYarnName(oResultSet.getString("NAME"));
                objYarn.setStrYarnType(oResultSet.getString("TYPE"));
                objYarn.setStrYarnColor(oResultSet.getString("COLOR"));
                objYarn.setIntYarnCount(oResultSet.getInt("COUNT"));
                objYarn.setStrYarnCountUnit(oResultSet.getString("UNIT"));
                objYarn.setIntYarnPly(oResultSet.getInt("PLY"));
                objYarn.setIntYarnDFactor(oResultSet.getInt("DFACTOR"));
                objYarn.setDblYarnDiameter(Double.parseDouble(oResultSet.getString("DIAMETER")));
                objYarn.setIntYarnTwist(oResultSet.getInt("TWIST"));
                objYarn.setStrYarnTModel(oResultSet.getString("TMODEL"));
                objYarn.setIntYarnHairness(oResultSet.getInt("HAIRNESS"));
                objYarn.setIntYarnHProbability(oResultSet.getInt("HPROBABILITY"));
                objYarn.setDblYarnPrice(Double.parseDouble(oResultSet.getString("PRICE")));
                objYarn.setBytYarnThumbnil(oResultSet.getBytes("ICON"));
                objYarn.setStrYarnAccess(oResultSet.getString("ACCESS"));
                objYarn.setStrYarnUser(oResultSet.getString("USERID"));
                objYarn.setStrYarnDate(oResultSet.getString("UDATE"));
            }
        } catch (Exception ex) {
            new Logging("SEVERE",YarnAction.class.getName(),"getYarn : "+strQuery,ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oStatement!=null) {
                    oStatement.close();
                    oStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",YarnAction.class.getName(),"getYarn() : Error while closing connection"+e,ex);
                }
            }
        }
        new Logging("INFO",YarnAction.class.getName(),">>>>>>>>>>> getYarn() <<<<<<<<<<<"+strQuery,null);
        return ;
    }

    public byte setYarn(Yarn objYarn) {
        PreparedStatement oPreparedStatement =null;
        ResultSet oResultSet= null;
        byte oResult= 0;
        String strQuery=null;
        new Logging("INFO",YarnAction.class.getName(),"<<<<<<<<<<< setYarn() >>>>>>>>>>>",null);
        try {           
            strQuery = "INSERT INTO `mla_yarn_library` (`ID`, `TYPE`, `NAME`, `COLOR`, `COUNT`, `UNIT`, `PLY`, `DFACTOR`, `DIAMETER`, `TWIST`, `TMODEL`, `HAIRNESS`, `HPROBABILITY`, `PRICE`, `ICON`, `ACCESS`, `USERID`) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
            getYarnIcon(objYarn);
            oPreparedStatement = connection.prepareStatement(strQuery);
            oPreparedStatement.setString(1, objYarn.getStrYarnId());
            oPreparedStatement.setString(2, objYarn.getStrYarnType());
            oPreparedStatement.setString(3, objYarn.getStrYarnName());
            oPreparedStatement.setString(4, objYarn.getStrYarnColor());
            oPreparedStatement.setInt(5, objYarn.getIntYarnCount());
            oPreparedStatement.setString(6, objYarn.getStrYarnCountUnit());
            oPreparedStatement.setInt(7, objYarn.getIntYarnPly());
            oPreparedStatement.setInt(8, objYarn.getIntYarnDFactor());
            oPreparedStatement.setDouble(9, Double.parseDouble(String.format("%.3f", objYarn.getDblYarnDiameter())));
            oPreparedStatement.setInt(10, objYarn.getIntYarnTwist());
            oPreparedStatement.setString(11, objYarn.getStrYarnTModel());
            oPreparedStatement.setInt(12, objYarn.getIntYarnHairness());
            oPreparedStatement.setInt(13, objYarn.getIntYarnHProbability());
            oPreparedStatement.setDouble(14, objYarn.getDblYarnPrice());
            oPreparedStatement.setBinaryStream(15,new ByteArrayInputStream(objYarn.getBytYarnThumbnil()),objYarn.getBytYarnThumbnil().length);
            oPreparedStatement.setString(16, new IDGenerator().setUserAcessKey("YARN_LIBRARY",objYarn.getObjConfiguration().getObjUser()));
            oPreparedStatement.setString(17, objYarn.getObjConfiguration().getObjUser().getStrUserID());
            oResult = (byte)oPreparedStatement.executeUpdate();
        } catch (Exception ex) {
            new Logging("SEVERE",YarnAction.class.getName(),"setYarn : "+strQuery,ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oPreparedStatement!=null) {
                    oPreparedStatement.close();
                    oPreparedStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",YarnAction.class.getName(),"setYarn() : Error while closing connection"+e,ex);
                }
            }
        }
        new Logging("INFO",YarnAction.class.getName(),">>>>>>>>>>> setYarn() <<<<<<<<<<<"+strQuery,null);
        return oResult;
    }
    
    public String getYarnAvibilityID(Yarn objYarn) {
        Statement oStatement =null;
        ResultSet oResultSet= null;
        String strQuery=null;
        String id = null;
        try {
            strQuery = "SELECT ID FROM mla_yarn_library WHERE "
                    + "`TYPE` = '"+objYarn.getStrYarnType()+"' AND "
                    + "`NAME` = '"+objYarn.getStrYarnName()+"' AND "
                    + "`COLOR` = '"+objYarn.getStrYarnColor()+"' AND "
                    + "`COUNT` = '"+objYarn.getIntYarnCount()+"' AND "
                    + "`UNIT` = '"+objYarn.getStrYarnCountUnit()+"' AND "
                    + "`PLY` = '"+objYarn.getIntYarnPly()+"' AND "
                    + "`DFACTOR` = '"+objYarn.getIntYarnDFactor()+"' AND "
                    + "`DIAMETER` = '"+objYarn.getDblYarnDiameter()+"' AND "
                    + "`TWIST` = '"+objYarn.getIntYarnTwist()+"' AND "
                    + "`TMODEL` = '"+objYarn.getStrYarnTModel()+"' AND "
                    + "`HAIRNESS` = '"+objYarn.getIntYarnHairness()+"' AND "
                    + "`HPROBABILITY` = '"+objYarn.getIntYarnHProbability()+"' AND "
                    + "`PRICE` = '"+objYarn.getDblYarnPrice()+"' AND "
                    + "(USERID = '"+objYarn.getObjConfiguration().getObjUser().getStrUserID()+"' OR "
                    + "`ACCESS`='"+new IDGenerator().getUserAcess("YARN_LIBRARY")+"') "
                    + "LIMIT 0,1;";
            oStatement = connection.createStatement();
            oResultSet = oStatement.executeQuery(strQuery);  
            oResultSet.next();
            id = oResultSet.getString("ID");
        } catch (Exception ex) {
            new Logging("SEVERE",YarnAction.class.getName(),"getYarnID : "+strQuery,ex);
        } finally {
            try {
                if(oResultSet!=null) {
                    oResultSet.close();
                    oResultSet=null;
                }
                if(oStatement!=null) {
                    oStatement.close();
                    oStatement=null;
                }
                if(connection!=null) {
                    connection.close();
                    connection=null;
                }
            } catch (Exception ex) {
                try {
                    DbConnect.close(connection);                
                } catch (Exception e) {
                    new Logging("SEVERE",YarnAction.class.getName(),"getYarnID() : Error while closing connection"+e,ex);
                }
            }
        }
        System.out.println("<<<<<< getYarnID >>>>>>"+id);
        return id;
    }
       
    public void getYarnIcon(Yarn objYarn){
        try {
            int npixel = new java.awt.Color((float)javafx.scene.paint.Color.web(objYarn.getStrYarnColor()).getRed(),(float)javafx.scene.paint.Color.web(objYarn.getStrYarnColor()).getGreen(),(float)javafx.scene.paint.Color.web(objYarn.getStrYarnColor()).getBlue()).getRGB();
            int nred   = (npixel & 0x00ff0000) >> 16;
            int ngreen = (npixel & 0x0000ff00) >> 8;
            int nblue  =  npixel & 0x000000ff;
            //System.out.println("New Red:"+nred+"\tGreen:"+ngreen+"\tBlue:"+nblue);           
            int hair = objYarn.getIntYarnHairness();
            int percentage = objYarn.getIntYarnHProbability();
            int twist = objYarn.getIntYarnTwist();
            char model = objYarn.getStrYarnTModel().charAt(0);
            BufferedImage srcImage = ImageIO.read(YarnAction.class.getResource("/media/yarn.jpg")); 
            
            int intLength = 111;//yarnImage.getHeight();;
            int intHeight = 111;//yarnImage.getWidth();
            BufferedImage myImage = new BufferedImage((int)(intLength), (int)(intHeight),BufferedImage.TYPE_INT_RGB);
            Graphics2D g = myImage.createGraphics();
            g.drawImage(srcImage, 0, 0, (int)(intLength), (int)(intHeight), null);
            g.dispose();
            srcImage = null;
            
            BufferedImage yarnImage = new BufferedImage(intLength, intHeight,BufferedImage.TYPE_INT_RGB);
            int rgb = 0;
            for(int x = 0; x < intHeight; x++) {
                for(int y = 0; y < intLength; y++) {
                    int pixel = myImage.getRGB(y, x);
                    int alpha = (pixel >> 24) & 0xff;
                    int red   = (pixel >>16) & 0xff;
                    int green = (pixel >> 8) & 0xff;
                    int blue  =  pixel & 0xff;
                    //if(red!=255 || green!=255 || blue!=255){
                    // Convert RGB to HSB
                    float[] hsb = java.awt.Color.RGBtoHSB(red, green, blue, null);
                    float hue = hsb[0];
                    float saturation = hsb[1];
                    float brightness = hsb[2];
                    hsb = null;
                    hsb = java.awt.Color.RGBtoHSB(nred, ngreen, nblue, null);
                    hue = hsb[0];
                    saturation = hsb[1];
                    // Convert HSB to RGB value
                    rgb = java.awt.Color.HSBtoRGB(hue, saturation, brightness);
                    //}else{
                    //rgb = pixel;
                    //}
                    yarnImage.setRGB(y, x, rgb);
                }
            }
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(yarnImage, "png", baos);
            objYarn.setBytYarnThumbnil(baos.toByteArray());
        } catch (IOException ex) {
             Logger.getLogger(YarnAction.class.getName()).log(Level.SEVERE, null, ex);           
            new Logging("SEVERE",YarnAction.class.getName(),"getYarnIcon() : Error while creating yarn image"+ex,ex);
        }
    }
    
    public BufferedImage getYarnImage(Yarn objYarn) throws IOException{
        int pixel = new java.awt.Color((float)javafx.scene.paint.Color.web(objYarn.getStrYarnColor()).getRed(),(float)javafx.scene.paint.Color.web(objYarn.getStrYarnColor()).getGreen(),(float)javafx.scene.paint.Color.web(objYarn.getStrYarnColor()).getBlue()).getRGB();
        int red   = (pixel & 0x00ff0000) >> 16;
        int green = (pixel & 0x0000ff00) >> 8;
        int blue  =  pixel & 0x000000ff;     
        
        int hair = objYarn.getIntYarnHairness();
        int percentage = objYarn.getIntYarnHProbability();
        int twist = objYarn.getIntYarnTwist();
        char model = objYarn.getStrYarnTModel().charAt(0);
        
        int height = 333;//yarnImage.getHeight();;
        int width = 111;//yarnImage.getWidth();
        // int threadNo =(int)width/objFabric.getObjConfiguration().getIntDPI();
        
        //System.out.println("New Red:"+red+"\tGreen:"+green+"\tBlue:"+blue);
        
        BufferedImage yarnImage = new BufferedImage(width+hair+hair, height,BufferedImage.TYPE_INT_ARGB); 
        int percentFactor = (height*width)/100;
        float brightness = 1f/width;
        float hsbVals[] = Color.RGBtoHSB(red, green, blue, null);
        Graphics g = yarnImage.getGraphics();
        Random objRandom = new Random();
        if(model=='Z' && twist>0){
            for(int y = 0; y < height; y++) {
                int p = y%width;            
                for(int x = hair; x < width+hair-p-5; x++) {
                    Color rgb = Color.getHSBColor(hsbVals[0], hsbVals[1], brightness*(width+hair-x)*hsbVals[2]);
                    g.setColor(rgb);  
                    g.drawRect(x, y, 1, 1);
                }
                for(int x = width+hair-p-6; x > width+hair-p; x--) {
                    Color rgb = Color.getHSBColor(hsbVals[0], hsbVals[1], brightness*(width+hair-x)*hsbVals[2]);
                    g.setColor(rgb);  
                    g.drawRect(x, y, 1, 1);
                }
                for(int x = width+hair-1; x >= width+hair-p; x--) {
                    Color rgb = Color.getHSBColor(hsbVals[0], hsbVals[1], brightness*(width+hair-x)*hsbVals[2]);
                    g.setColor(rgb);  
                    g.drawRect(x, y, 1, 1);
                }
            }        
        }else if(model=='S' && twist>0){
            
        }else{
            for(int y = 0; y < height; y++) {
                for(int x = hair; x < width+hair; x++) {
                    Color rgb = Color.getHSBColor(hsbVals[0], hsbVals[1], brightness*(width+hair-x)*hsbVals[2]);
                    g.setColor(rgb);  
                    g.drawRect(x, y, 1, 1);
                }
            }    
        }
        if(hair>0 && percentage>0){
            for(int i = percentFactor*percentage; i > 0; i--) {
                int x = objRandom.nextInt(width+hair);
                Color rgb = Color.getHSBColor(hsbVals[0], hsbVals[1], brightness*(width+hair-x)*hsbVals[2]);
                g.setColor(rgb);  
                int y = objRandom.nextInt(height);
                g.drawArc(x, y, hair, hair, objRandom.nextInt(350), objRandom.nextInt(250));
                yarnImage.setRGB(x, y, Color.DARK_GRAY.getRGB());
            }
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(yarnImage, "png", baos);
        objYarn.setBytYarnThumbnil(baos.toByteArray());
        return yarnImage;
    }    
}
