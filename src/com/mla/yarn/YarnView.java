/*
 * Copyright (C) Digital India Corporation (Media Lab Asia)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mla.yarn;

import com.mla.colour.ColourSelector;
import com.mla.dictionary.DictionaryAction;
import com.mla.fabric.FabricAction;
import com.mla.main.Configuration;
import com.mla.main.Logging;
import com.mla.main.WindowView;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
/**
 *
 * @Designing GUI window for weave editor
 * @author Amit Kumar Singh
 * 
 */
public class YarnView {
 
    private static YarnView objYarnView;
    private Yarn objYarn;
    private FabricAction objFabricAction;
    private Configuration objConfiguration;
    DictionaryAction objDictionaryAction;
    
    private Stage yarnStage;
    private BorderPane root;    
    private Scene scene;
    private ImageView yarnImage;
    
    ComboBox colorCB;
    //ColorPicker color;
    ToggleGroup twistScene;
    ComboBox countUnit;
    Button btnUpdate;
    Button btnPreview;
    Button btnCancel;
    Label lblStatus;
    Label type;
    Label name;
    Label repeat;
    Label symbol;
    Label diameter;
    TextField count;
    TextField ply;
    TextField factor;    
    TextField txtTwistCount;
    TextField txtHairLength;
    TextField txtHairPercentage;
    TextField price;
    int index = -1;
               
    private BufferedImage bufferedImage;
       
    private TableView<Yarn> yarnTable = new TableView<Yarn>();
    private ObservableList<Yarn> yarnData = null;
    /* private final ObservableList<Yarn> yarnData =
        FXCollections.observableArrayList(
            new Yarn("Warp", "#000000", "1","Tex", "1", "1","1", "0", "O", "0", "0"),
            new Yarn("Weft", "#FFFFFF", "1","Tex", "1", "1","1", "0", "O", "0", "0")
        );*/
        
    private YarnView(Stage stage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
     public YarnView(Configuration objConfigurationCall) {   
        this.objConfiguration = objConfigurationCall;
        objDictionaryAction = new DictionaryAction(objConfiguration);
        
        yarnStage = new Stage();
        yarnStage.initModality(Modality.APPLICATION_MODAL);//WINDOW_MODAL
        yarnStage.initStyle(StageStyle.UTILITY);
        root = new BorderPane();
        root.setId("popup");
        scene = new Scene(root, objConfiguration.WIDTH/1.5, objConfiguration.HEIGHT/1.5, Color.WHITE);
        scene.getStylesheets().add(YarnView.class.getResource(objConfiguration.getStrTemplate()+"/style.css").toExternalForm());
            
        yarnData = FXCollections.observableArrayList();

        ArrayList<String> lstEntry = new ArrayList();
        for(int i=0; i<objConfiguration.getWarpYarn().length; i++){
            if(lstEntry.size()==0){
                lstEntry.add(objConfiguration.getWarpYarn()[i].getStrYarnColor());
                yarnData.add(objConfiguration.getWarpYarn()[i]);
                //97 - 113 This gives the character 'a'-'z'
            }
            //check for redudancy
            else {                
                if(!lstEntry.contains(objConfiguration.getWarpYarn()[i].getStrYarnColor())){
                    lstEntry.add(objConfiguration.getWarpYarn()[i].getStrYarnColor());
                    yarnData.add(objConfiguration.getWarpYarn()[i]);
                }
            }
        }
        
        lstEntry = new ArrayList();
        for(int i=0; i<objConfiguration.getWeftYarn().length; i++){
            if(lstEntry.size()==0){
                lstEntry.add(objConfiguration.getWeftYarn()[i].getStrYarnColor());
                yarnData.add(objConfiguration.getWeftYarn()[i]);
                //97 - 113 This gives the character 'a'-'z'
            }
            //check for redudancy
            else {                
                if(!lstEntry.contains(objConfiguration.getWeftYarn()[i].getStrYarnColor())){
                    lstEntry.add(objConfiguration.getWeftYarn()[i].getStrYarnColor());
                    yarnData.add(objConfiguration.getWeftYarn()[i]);
                }
            }
        }
        
        if(objConfiguration.getWarpExtraYarn()!=null){
            for(int i=0; i<objConfiguration.getIntExtraWarp(); i++){
                yarnData.add(objConfiguration.getWarpExtraYarn()[i]);
            }
        }
        if(objConfiguration.getWeftExtraYarn()!=null){
            for(int i=0; i<objConfiguration.getIntExtraWeft(); i++){
                yarnData.add(objConfiguration.getWeftExtraYarn()[i]);
            }
        }
        TableColumn selectCol = new TableColumn("Thread#");
        TableColumn typeCol = new TableColumn("Type");
        TableColumn nameCol = new TableColumn("Name");
        TableColumn colorCol = new TableColumn("Color");
        TableColumn repeatCol = new TableColumn("Repeat");
        TableColumn symbolCol = new TableColumn("Symbol");
        TableColumn countCol = new TableColumn("Count");
        TableColumn unitCol = new TableColumn("Count Unit");
        TableColumn plyCol = new TableColumn("Ply");
        TableColumn factorCol = new TableColumn("Dimensional Factor");
        TableColumn diameterCol = new TableColumn("Diameter");
        TableColumn twistCol = new TableColumn("Twist Count");
        TableColumn modelCol = new TableColumn("Twist Model");
        TableColumn hairnessCol = new TableColumn("Hairness");
        TableColumn probabilityCol = new TableColumn("Hair Percentage");
        TableColumn priceCol = new TableColumn("Price");
        selectCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("strYarnId"));
        typeCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("strYarnType"));
        nameCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("strYarnName"));
        colorCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("strYarnColor"));
        repeatCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("intYarnRepeat"));
        symbolCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("strYarnSymbol"));
        countCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("intYarnCount"));
        unitCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("strYarnCountUnit"));
        plyCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("intYarnPly"));
        factorCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("intYarnDFactor"));
        diameterCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("dblYarnDiameter"));        
        twistCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("intYarnTwist"));
        modelCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("strYarnTModel"));
        hairnessCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("intYarnHairness"));
        probabilityCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("intYarnHProbability"));
        priceCol.setCellValueFactory(new PropertyValueFactory<Yarn,String>("dblYarnPrice"));
        yarnTable.setItems(yarnData);
        yarnTable.setEditable(true);
        yarnTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        yarnTable.getColumns().addAll(selectCol, typeCol, nameCol, colorCol, repeatCol, symbolCol, countCol, unitCol, plyCol, factorCol, diameterCol, twistCol, modelCol, hairnessCol, probabilityCol, priceCol);
        yarnTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Yarn>() {
            @Override
            public void changed(ObservableValue<? extends Yarn> ov, Yarn t, Yarn t1) {
                try {
                    index = yarnData.indexOf(t1);
                    colorCB.setStyle("-fx-background-color:"+t1.getStrYarnColor()+";");
                    //color.setValue(Color.web(t1.getStrYarnColor()));
                    twistScene.equals(t1.getStrYarnTModel());
                    countUnit.setValue(t1.getStrYarnCountUnit());
                    type.setText(t1.getStrYarnType());
                    name.setText(t1.getStrYarnName());
                    repeat.setText(Integer.toString(t1.getIntYarnRepeat()));
                    symbol.setText(t1.getStrYarnSymbol());
                    diameter.setText(Double.toString(t1.getDblYarnDiameter()));
                    count.setText(Integer.toString(t1.getIntYarnCount()));
                    ply.setText(Integer.toString(t1.getIntYarnPly()));
                    factor.setText(Integer.toString(t1.getIntYarnDFactor()));
                    txtTwistCount.setText(Integer.toString(t1.getIntYarnTwist()));
                    txtHairLength.setText(Integer.toString(t1.getIntYarnHairness()));
                    txtHairPercentage.setText(Integer.toString(t1.getIntYarnHProbability()));
                    price.setText(Double.toString(t1.getDblYarnPrice()));
                    //objYarn = new Yarn(null, type.getText(), name.getText(), toRGBCode((Color)color.getValue()), Integer.parseInt(repeat.getText()), symbol.getText(), Integer.parseInt(count.getText()), countUnit.getValue().toString(), Integer.parseInt(ply.getText()), Integer.parseInt(factor.getText()), Double.parseDouble(diameter.getText()), Integer.parseInt(txtTwistCount.getText()), twistScene.getSelectedToggle().getUserData().toString(), Integer.parseInt(txtHairLength.getText()), Integer.parseInt(txtHairPercentage.getText()), Double.parseDouble(price.getText()), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);
                    objYarn = new Yarn(null, type.getText(), name.getText(), colorCB.getStyle().substring(colorCB.getStyle().lastIndexOf("#"), colorCB.getStyle().lastIndexOf("#")+7), Integer.parseInt(repeat.getText()), symbol.getText(), Integer.parseInt(count.getText()), countUnit.getValue().toString(), Integer.parseInt(ply.getText()), Integer.parseInt(factor.getText()), Double.parseDouble(diameter.getText()), Integer.parseInt(txtTwistCount.getText()), twistScene.getSelectedToggle().getUserData().toString(), Integer.parseInt(txtHairLength.getText()), Integer.parseInt(txtHairPercentage.getText()), Double.parseDouble(price.getText()), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);
                    objYarn.setObjConfiguration(objConfiguration);
                    YarnAction objYarnAction = new YarnAction();
                    yarnImage.setImage(SwingFXUtils.toFXImage(objYarnAction.getYarnImage(objYarn), null));
                    btnPreview.setDisable(false);
                    btnUpdate.setDisable(false);
                } catch (IOException ex) {
                    new Logging("SEVERE",YarnView.class.getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                    Logger.getLogger(YarnView.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    new Logging("SEVERE",YarnView.class.getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                    Logger.getLogger(YarnView.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
        
        lblStatus = new Label(objDictionaryAction.getWord("SELECTYARNUPDATE"));
        lblStatus.setStyle("-fx-text-fill: #FF0000;");
        
        btnPreview = new Button(objDictionaryAction.getWord("PREVIEW"));
        btnUpdate = new Button(objDictionaryAction.getWord("UPDATE"));
        btnCancel = new Button(objDictionaryAction.getWord("CANCEL"));

        //btnPreview.setMaxWidth(Double.MAX_VALUE);
        //btnUpdate.setMaxWidth(Double.MAX_VALUE);
        //btnClose.setMaxWidth(Double.MAX_VALUE);        

        btnPreview.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/preview.png"));
        btnUpdate.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/update.png"));
        btnCancel.setGraphic(new ImageView(objConfiguration.getStrColour()+"/"+objConfiguration.strIconResolution+"/close.png"));
        
        btnPreview.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPPREVIEW")));
        btnUpdate.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPUPDATE")));
        btnCancel.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPCANCEL")));
        
        btnCancel.setDefaultButton(true);
        btnUpdate.setDisable(true);
        btnPreview.setDisable(true);
        
        btnPreview.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {                
                try {                
                    yarnAction("Preview");
                } catch (SQLException ex) {
                    new Logging("SEVERE",YarnView.class.getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        btnUpdate.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {                
                try {                
                    yarnAction("Update");
                } catch (SQLException ex) {
                    new Logging("SEVERE",YarnView.class.getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                    Logger.getLogger(YarnView.class.getName()).log(Level.SEVERE, null, ex);
                } catch(Exception ex) {
                    Logger.getLogger(YarnView.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        btnCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {                
                try {                
                    yarnAction("Close");
                } catch (SQLException ex) {
                    new Logging("SEVERE",YarnView.class.getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }               
            }
        });
        
        GridPane yarnForm = new GridPane();
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNTYPE")), 0, 0);
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNNAME")), 0, 1);
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNCOUNT")), 0, 2);
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNUNIT")), 0, 3);
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNPLY")), 0, 4);
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNFACTOR")), 0, 5);
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNDIAMETER")), 0, 6); 
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNPRICE")), 0, 7);
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNCOLOR")), 2, 0);
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNREPEAT")), 2, 1);
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNSYMBOL")), 2, 2);
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNSENCE")), 2, 3);          
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNTWIST")), 2, 4);
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNHAIRNESS")), 2, 5);
        yarnForm.add(new Label(objDictionaryAction.getWord("YARNDISTRIBUTION")), 2, 6);
        
        type = new Label();
        yarnForm.add(type, 1, 0);
        name = new Label();
        yarnForm.add(name, 1, 1);
        count = new TextField("1"){
            @Override public void replaceText(int start, int end, String text) {
              if (text.matches("[0-9]*")) {
                super.replaceText(start, end, text);
              }
            }
            @Override public void replaceSelection(String text) {
              if (text.matches("[0-9]*")) {
                super.replaceSelection(text);
              }
            }
          };
        yarnForm.add(count, 1, 2);
        countUnit = new ComboBox();
        countUnit.getItems().addAll("Tex","dTex","K tex","Denier (Td)","New Metric (Nm)","Grains/Yard","Woollen (Aberdeen) (Ta)","Woollen (US Grain)","Asbestos (American) (NaA)","Asbestos (English) (NeA)","Cotton bump Yarn (NB)","Glass (UK & USA)","Linen (Set or Dry Spun) (NeL)","Spun Silk (Ns)","Woollen (American Cut) (Nac)","Woollen (American run) (Nar)","Woollen (Yarkshire) (Ny)","Woollen (Worsted) (New)","Linen, Hemp, Jute (Tj)","Micronaire (Mic)","Yards Per Pound (YPP)","English Worsted Count (NeK)","English Cotton (NeC)","New English (Ne)","Numero en puntos (Np)");  
        countUnit.setValue("Tex");
        yarnForm.add(countUnit, 1, 3);
        ply = new TextField("1"){
            @Override public void replaceText(int start, int end, String text) {
              if (text.matches("[0-9]*")) {
                super.replaceText(start, end, text);
              }
            }
            @Override public void replaceSelection(String text) {
              if (text.matches("[0-9]*")) {
                super.replaceSelection(text);
              }
            }
          };
        yarnForm.add(ply, 1, 4);
        factor = new TextField("1"){
            @Override public void replaceText(int start, int end, String text) {
              if (text.matches("[0-9]*")) {
                super.replaceText(start, end, text);
              }
            }
            @Override public void replaceSelection(String text) {
              if (text.matches("[0-9]*")) {
                super.replaceSelection(text);
              }
            }
          };    
        yarnForm.add(factor, 1, 5);        
        diameter= new Label(); 
        yarnForm.add(diameter, 1, 6);
        price = new TextField("0");
        yarnForm.add(price, 1, 7);
        //color = new ColorPicker();
        //color.setValue(Color.web("#012345"));
        //color.setEditable(true);
        //yarnForm.add(color, 3, 0);
        colorCB=new ComboBox();
        colorCB.setStyle("-fx-background-color:#012345;");
        yarnForm.add(colorCB, 3, 0);
        repeat = new Label("1");
        yarnForm.add(repeat, 3, 1);
        symbol = new Label("");
        yarnForm.add(symbol, 3, 2);
        HBox tsHB = new HBox();
        twistScene = new ToggleGroup();
        RadioButton otsRB = new RadioButton("O");        
        otsRB.setToggleGroup(twistScene);
        otsRB.setUserData("O");
        otsRB.setSelected(true);
        RadioButton stsRB = new RadioButton("S");
        stsRB.setToggleGroup(twistScene);
        stsRB.setUserData("S");
        RadioButton ztsRB = new RadioButton("Z");
        ztsRB.setToggleGroup(twistScene);
        ztsRB.setUserData("Z");
        tsHB.getChildren().addAll(otsRB,stsRB,ztsRB);
        yarnForm.add(tsHB, 3, 3);
        txtTwistCount = new TextField("0"){
            @Override public void replaceText(int start, int end, String text) {
              if (text.matches("[0-9]*")) {
                super.replaceText(start, end, text);
              }
            }
            @Override public void replaceSelection(String text) {
              if (text.matches("[0-9]*")) {
                super.replaceSelection(text);
              }
            }
          };
        txtTwistCount.setEditable(false);
        yarnForm.add(txtTwistCount, 3, 4);
        txtHairLength = new TextField("0"){
            @Override public void replaceText(int start, int end, String text) {
              if (text.matches("[0-9]*")) {
                super.replaceText(start, end, text);
              }
            }
            @Override public void replaceSelection(String text) {
              if (text.matches("[0-9]*")) {
                super.replaceSelection(text);
              }
            }
          };
        yarnForm.add(txtHairLength, 3, 5);
        txtHairPercentage = new TextField("0"){
            @Override public void replaceText(int start, int end, String text) {
              if (text.matches("[0-9]*")) {
                super.replaceText(start, end, text);
              }
            }
            @Override public void replaceSelection(String text) {
              if (text.matches("[0-9]*")) {
                super.replaceSelection(text);
              }
            }
          };
        yarnForm.add(txtHairPercentage, 3, 6);
        
        /*color.setOnAction(new EventHandler() {
            @Override
            public void handle(Event t) {
                Color c = color.getValue();
            }
        });*/
        colorCB.setOnShown(new EventHandler() {
            @Override
            public void handle(Event t) {
                try {
                    ColourSelector objColourSelector=new ColourSelector(objConfiguration);
                    if(objColourSelector.colorCode!=null&&objColourSelector.colorCode.length()>0){
                        colorCB.setStyle("-fx-background-color:#"+objColourSelector.colorCode+";");
                    }
                    colorCB.hide();
                    t.consume();
                } catch (Exception ex) {
                    Logger.getLogger(YarnView.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        colorCB.setTooltip(new Tooltip(objDictionaryAction.getWord("TOOLTIPYARNCOLOR")));
        countUnit.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                try {
                    FabricAction objFabricAction = new FabricAction();
                    double newCount = objFabricAction.convertUnit(t, t1, Double.parseDouble(count.getText()));
                    count.setText(Double.toString(newCount));
                    newCount = objFabricAction.convertUnit(t1, "Tex", newCount);
                    double newDiameter = objFabricAction.calculateDiameter(newCount,Double.parseDouble(ply.getText()),Double.parseDouble(factor.getText()));
                    diameter.setText(Double.toString(newDiameter));
                } catch (SQLException ex) {
                    new Logging("SEVERE",YarnView.class.getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                } catch (UnsupportedOperationException ex) {
                    new Logging("SEVERE",YarnView.class.getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        count.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                try {
                    FabricAction objFabricAction = new FabricAction();
                    double newCount = objFabricAction.convertUnit(countUnit.getValue().toString(), "Tex", Double.parseDouble(count.getText()));
                    double newDiameter = objFabricAction.calculateDiameter(newCount,Double.parseDouble(ply.getText()),Double.parseDouble(factor.getText()));
                    diameter.setText(Double.toString(newDiameter));                   
                } catch (SQLException ex) {
                    new Logging("SEVERE",YarnView.class.getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                } catch (UnsupportedOperationException ex) {
                    new Logging("SEVERE",YarnView.class.getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        ply.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                try {
                    FabricAction objFabricAction = new FabricAction();
                    double newCount = objFabricAction.convertUnit(countUnit.getValue().toString(), "Tex", Double.parseDouble(count.getText()));
                    double newDiameter = objFabricAction.calculateDiameter(newCount,Double.parseDouble(ply.getText()),Double.parseDouble(factor.getText()));
                    diameter.setText(Double.toString(newDiameter));
                } catch (SQLException ex) {
                    new Logging("SEVERE",YarnView.class.getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                } catch (UnsupportedOperationException ex) {
                    new Logging("SEVERE",YarnView.class.getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        factor.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                try {
                    FabricAction objFabricAction = new FabricAction();
                    double newCount = objFabricAction.convertUnit(countUnit.getValue().toString(), "Tex", Double.parseDouble(count.getText()));
                    double newDiameter = objFabricAction.calculateDiameter(newCount,Double.parseDouble(ply.getText()),Double.parseDouble(factor.getText()));
                    diameter.setText(Double.toString(newDiameter));
                } catch (SQLException ex) {
                    new Logging("SEVERE",YarnView.class.getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                } catch (UnsupportedOperationException ex) {
                    new Logging("SEVERE",YarnView.class.getName(),ex.toString(),ex);
                    lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                }
            }
        });
        twistScene.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            public void changed(ObservableValue<? extends Toggle> ov,Toggle old_toggle, Toggle new_toggle) {
              if (twistScene.getSelectedToggle() != null) {
                  char chrFabricTwistScene = twistScene.getSelectedToggle().getUserData().toString().trim().charAt(0);
                  if(chrFabricTwistScene=='S' || chrFabricTwistScene=='Z'){
                      txtTwistCount.setEditable(true);                  
                  }else{
                      txtTwistCount.setText("0");
                      txtTwistCount.setEditable(false);    
                  }
              }
            }
          });
        
        yarnForm.add(btnPreview, 1, 8);
        yarnForm.add(btnUpdate, 2, 8);
        yarnForm.add(btnCancel, 3, 8);
        //yarnForm.add(lblStatus, 0, 9, 4, 4);
        
        yarnImage = new ImageView();
        yarnImage.setFitWidth(123);
        yarnImage.setFitHeight(objConfiguration.HEIGHT/1.5);     
        
        ScrollPane yarnList = new ScrollPane();
        yarnList.setContent(yarnTable);
        
        ScrollPane yarnData = new ScrollPane();
        yarnData.setContent(yarnForm);
        
        yarnTable.setPrefSize(objConfiguration.WIDTH/1.5,objConfiguration.HEIGHT/3);        
        yarnForm.setPrefSize(objConfiguration.WIDTH/1.5, objConfiguration.HEIGHT/3);
        yarnData.setPrefSize(objConfiguration.WIDTH/1.5, objConfiguration.HEIGHT/3);
        yarnList.setPrefSize(objConfiguration.WIDTH/1.5, objConfiguration.HEIGHT/3);
                
        VBox bodyContainer = new VBox();
        bodyContainer.getChildren().addAll(yarnList,yarnData);
        root.setCenter(bodyContainer);
        root.setBottom(lblStatus);
        root.setRight(yarnImage);
        //root.setCenter(yarnList);
        //root.setBottom(yarnData);
        yarnStage.setScene(scene);
        yarnStage.getIcons().add(new Image("/media/icon.png"));
        yarnStage.setTitle(objDictionaryAction.getWord("PROJECT")+" : "+objDictionaryAction.getWord("WINDOWYARNEDITOR")+" \u00A9 "+objDictionaryAction.getWord("TITLE"));
        yarnStage.setIconified(false);
        yarnStage.setResizable(false);        
        yarnStage.showAndWait();
    }    
     
    public void yarnAction(String actionName) throws SQLException {        
        if (actionName.equalsIgnoreCase("Preview")) {
            try {
                //objYarn = new Yarn(null, type.getText(), name.getText(), toRGBCode((Color)color.getValue()), Integer.parseInt(repeat.getText()), symbol.getText(), Integer.parseInt(count.getText()), countUnit.getValue().toString(), Integer.parseInt(ply.getText()), Integer.parseInt(factor.getText()), Double.parseDouble(diameter.getText()), Integer.parseInt(txtTwistCount.getText()), twistScene.getSelectedToggle().getUserData().toString(), Integer.parseInt(txtHairLength.getText()), Integer.parseInt(txtHairPercentage.getText()), Double.parseDouble(price.getText()), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);
                objYarn = new Yarn(null, type.getText(), name.getText(), colorCB.getStyle().substring(colorCB.getStyle().lastIndexOf("#"), colorCB.getStyle().lastIndexOf("#")+7), Integer.parseInt(repeat.getText()), symbol.getText(), Integer.parseInt(count.getText()), countUnit.getValue().toString(), Integer.parseInt(ply.getText()), Integer.parseInt(factor.getText()), Double.parseDouble(diameter.getText()), Integer.parseInt(txtTwistCount.getText()), twistScene.getSelectedToggle().getUserData().toString(), Integer.parseInt(txtHairLength.getText()), Integer.parseInt(txtHairPercentage.getText()), Double.parseDouble(price.getText()), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);
                objYarn.setObjConfiguration(objConfiguration);
                YarnAction objYarnAction = new YarnAction();
                yarnImage.setImage(SwingFXUtils.toFXImage(objYarnAction.getYarnImage(objYarn), null));                    
            } catch (SQLException ex) {
                new Logging("SEVERE",YarnView.class.getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            } catch (IOException ex) {
                new Logging("SEVERE",YarnView.class.getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
            }
        }
        if (actionName.equalsIgnoreCase("Update")) {
            try {
                char character = symbol.getText().trim().charAt(0); 
                int ascii = (int) character;
                if(65<=ascii && ascii<=96) //65 - 96 This gives the character 'A'-'Z'
                    //objConfiguration.getColourPalette()[ascii-65]=toRGBCode((Color)color.getValue()).substring(1);
                    objConfiguration.getColourPalette()[ascii-65]=colorCB.getStyle().substring(colorCB.getStyle().lastIndexOf("#")+1, colorCB.getStyle().lastIndexOf("#")+7);
                else if(97<=ascii && ascii<=113) //97 - 113 This gives the character 'a'-'z'
                    //objConfiguration.getColourPalette()[ascii-71]=toRGBCode((Color)color.getValue()).substring(1);
                    objConfiguration.getColourPalette()[ascii-71]=colorCB.getStyle().substring(colorCB.getStyle().lastIndexOf("#")+1, colorCB.getStyle().lastIndexOf("#")+7);
                    //objConfiguration.strThreadPalettes[26+index-objConfiguration.getWarpYarn().length]=objYarn.getStrYarnColor().substring(1);
                
                FabricAction objFabricAction = new FabricAction();
                //objYarn = new Yarn(null, type.getText(), name.getText(), toRGBCode((Color)color.getValue()), Integer.parseInt(repeat.getText()), symbol.getText(), Integer.parseInt(count.getText()), countUnit.getValue().toString(), Integer.parseInt(ply.getText()), Integer.parseInt(factor.getText()), Double.parseDouble(diameter.getText()), Integer.parseInt(txtTwistCount.getText()), twistScene.getSelectedToggle().getUserData().toString(), Integer.parseInt(txtHairLength.getText()), Integer.parseInt(txtHairPercentage.getText()), Double.parseDouble(price.getText()), objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"),objConfiguration.getObjUser().getStrUserID(),null);
                if(!objYarn.getStrYarnType().equals("Extra Weft") && !objYarn.getStrYarnType().equals("Extra Warp")){
                    for(index = 0; index<yarnData.size();index++){
                        objYarn = yarnData.get(index);
                        if(objYarn.getStrYarnSymbol().equals(symbol.getText()) && objYarn.getStrYarnType().equals(type.getText()) && !objYarn.getStrYarnType().equals("Extra Weft") && !objYarn.getStrYarnType().equals("Extra Warp")){
                            objYarn.setStrYarnType(type.getText());
                            objYarn.setStrYarnName(name.getText());
                            //objYarn.setStrYarnColor(toRGBCode((Color)color.getValue()));
                            objYarn.setStrYarnColor(colorCB.getStyle().substring(colorCB.getStyle().lastIndexOf("#"), colorCB.getStyle().lastIndexOf("#")+7));
                            objYarn.setIntYarnRepeat(Integer.parseInt(repeat.getText()));
                            objYarn.setStrYarnSymbol(symbol.getText());
                            objYarn.setIntYarnCount(Integer.parseInt(count.getText()));
                            objYarn.setStrYarnCountUnit(countUnit.getValue().toString());
                            objYarn.setIntYarnPly(Integer.parseInt(ply.getText()));
                            objYarn.setIntYarnDFactor(Integer.parseInt(factor.getText()));
                            objYarn.setDblYarnDiameter(Double.parseDouble(diameter.getText()));
                            objYarn.setIntYarnTwist(Integer.parseInt(txtTwistCount.getText()));
                            objYarn.setStrYarnTModel(twistScene.getSelectedToggle().getUserData().toString());
                            objYarn.setIntYarnHairness(Integer.parseInt(txtHairLength.getText()));
                            objYarn.setIntYarnHProbability(Integer.parseInt(txtHairPercentage.getText()));
                            objYarn.setDblYarnPrice(Double.parseDouble(price.getText()));
                            objYarn.setStrYarnAccess(objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"));
                            objYarn.setStrYarnUser(objConfiguration.getObjUser().getStrUserID());
                            //objYarn.setStrYarnDate(null);
                            objYarn.setObjConfiguration(objConfiguration);
                            yarnData.set(index, objYarn);
							if(objYarn.getStrYarnType().equalsIgnoreCase("Warp")){
                                for(int a=0; a<objConfiguration.getWarpYarn().length; a++){
                                    if(objConfiguration.getWarpYarn()[a].getStrYarnSymbol().equals(objYarn.getStrYarnSymbol()))
                                        objConfiguration.getWarpYarn()[a]=objYarn;
                                }
                            }
                            else if(objYarn.getStrYarnType().equalsIgnoreCase("Weft")){
                                for(int a=0; a<objConfiguration.getWeftYarn().length; a++){
                                    if(objConfiguration.getWeftYarn()[a].getStrYarnSymbol().equals(objYarn.getStrYarnSymbol()))
                                        objConfiguration.getWeftYarn()[a]=objYarn;
                                }
                            }
                            else if(objConfiguration.getWarpExtraYarn()!=null && objYarn.getStrYarnType().equalsIgnoreCase("Extra Warp")){
                                for(int a=0; a<objConfiguration.getWarpExtraYarn().length; a++){
                                    if(objConfiguration.getWarpExtraYarn()[a].getStrYarnSymbol().equals(objYarn.getStrYarnSymbol()))
                                        objConfiguration.getWarpExtraYarn()[a]=objYarn;
                                }
                            }
                            else if(objConfiguration.getWeftExtraYarn()!=null && objYarn.getStrYarnType().equalsIgnoreCase("Extra Weft")){
                                for(int a=0; a<objConfiguration.getWeftExtraYarn().length; a++){
                                    if(objConfiguration.getWeftExtraYarn()[a].getStrYarnSymbol().equals(objYarn.getStrYarnSymbol()))
                                        objConfiguration.getWeftExtraYarn()[a]=objYarn;
                                }
                            }
                            /*System.err.println("Index:"+index+"Warp"+objConfiguration.getWarpYarn().length+"Weft:"+objConfiguration.getWeftYarn().length);
                            if(objConfiguration.getWarpYarn().length>index){
                                objConfiguration.getWarpYarn()[index]=objYarn;
                            }else if(objConfiguration.getWeftYarn().length>(index-objConfiguration.getWarpYarn().length)){
                                objConfiguration.getWeftYarn()[index-objConfiguration.getWarpYarn().length]=objYarn;
                            }else if(objConfiguration.getWarpExtraYarn()!=null && objConfiguration.getWarpExtraYarn().length>(index-(objConfiguration.getWarpYarn().length+objConfiguration.getWeftYarn().length))){
                                objConfiguration.getWarpExtraYarn()[index-(objConfiguration.getWarpYarn().length+objConfiguration.getWeftYarn().length)]=objYarn;
                            }else if(objConfiguration.getWeftExtraYarn()!=null){
                                if(objConfiguration.getWarpExtraYarn()!=null && objConfiguration.getWeftExtraYarn().length>(index-(objConfiguration.getWarpYarn().length+objConfiguration.getWeftYarn().length+objConfiguration.getWarpExtraYarn().length)))
                                    objConfiguration.getWeftExtraYarn()[index-(objConfiguration.getWarpYarn().length+objConfiguration.getWeftYarn().length+objConfiguration.getWarpExtraYarn().length)]=objYarn;
                                else
                                    objConfiguration.getWeftExtraYarn()[index-(objConfiguration.getWarpYarn().length+objConfiguration.getWeftYarn().length)]=objYarn;
                            }*/
							
                            //yarnTable.getColumns().get(amit).setVisible(false);
                            //yarnTable.getColumns().get(amit).setVisible(true);
                        }
                    }
                } else{
                    objYarn = yarnData.get(index);

                    objYarn.setStrYarnType(type.getText());
                    objYarn.setStrYarnName(name.getText());
                    //objYarn.setStrYarnColor(toRGBCode((Color)color.getValue()));
                    objYarn.setStrYarnColor(colorCB.getStyle().substring(colorCB.getStyle().lastIndexOf("#"), colorCB.getStyle().lastIndexOf("#")+7));
                    objYarn.setIntYarnRepeat(Integer.parseInt(repeat.getText()));
                    objYarn.setStrYarnSymbol(symbol.getText());
                    objYarn.setIntYarnCount(Integer.parseInt(count.getText()));
                    objYarn.setStrYarnCountUnit(countUnit.getValue().toString());
                    objYarn.setIntYarnPly(Integer.parseInt(ply.getText()));
                    objYarn.setIntYarnDFactor(Integer.parseInt(factor.getText()));
                    objYarn.setDblYarnDiameter(Double.parseDouble(diameter.getText()));
                    objYarn.setIntYarnTwist(Integer.parseInt(txtTwistCount.getText()));
                    objYarn.setStrYarnTModel(twistScene.getSelectedToggle().getUserData().toString());
                    objYarn.setIntYarnHairness(Integer.parseInt(txtHairLength.getText()));
                    objYarn.setIntYarnHProbability(Integer.parseInt(txtHairPercentage.getText()));
                    objYarn.setDblYarnPrice(Double.parseDouble(price.getText()));
                    objYarn.setStrYarnAccess(objConfiguration.getObjUser().getUserAccess("YARN_LIBRARY"));
                    objYarn.setStrYarnUser(objConfiguration.getObjUser().getStrUserID());
                    //objYarn.setStrYarnDate(null);
                    objYarn.setObjConfiguration(objConfiguration);
                    yarnData.set(index, objYarn);
                    if(objConfiguration.getWarpYarn().length>index){
                        objConfiguration.getWarpYarn()[index]=objYarn;
                    }else if(objConfiguration.getWeftYarn().length>(index-objConfiguration.getWarpYarn().length)){
                        objConfiguration.getWeftYarn()[index-objConfiguration.getWarpYarn().length]=objYarn;
                    }else if(objConfiguration.getWarpExtraYarn()!=null && objConfiguration.getWarpExtraYarn().length>(index-(objConfiguration.getWarpYarn().length+objConfiguration.getWeftYarn().length))){
                        objConfiguration.getWarpExtraYarn()[index-(objConfiguration.getWarpYarn().length+objConfiguration.getWeftYarn().length)]=objYarn;
                    }else if(objConfiguration.getWeftExtraYarn()!=null){
                        if(objConfiguration.getWarpExtraYarn()!=null && objConfiguration.getWeftExtraYarn().length>(index-(objConfiguration.getWarpYarn().length+objConfiguration.getWeftYarn().length+objConfiguration.getWarpExtraYarn().length)))
                            objConfiguration.getWeftExtraYarn()[index-(objConfiguration.getWarpYarn().length+objConfiguration.getWeftYarn().length+objConfiguration.getWarpExtraYarn().length)]=objYarn;
                        else
                            objConfiguration.getWeftExtraYarn()[index-(objConfiguration.getWarpYarn().length+objConfiguration.getWeftYarn().length)]=objYarn;
                    }
                    //yarnTable.getColumns().get(amit).setVisible(false);
                    //yarnTable.getColumns().get(amit).setVisible(true);
                }
                refreshTable();                
            } catch (SQLException ex) {
                new Logging("SEVERE",YarnView.class.getName(),ex.toString(),ex);
                lblStatus.setText(objDictionaryAction.getWord("ERROR"));
                Logger.getLogger(YarnView.class.getName()).log(Level.SEVERE, null, ex);
            } catch(Exception ex) {
                Logger.getLogger(YarnView.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (actionName.equalsIgnoreCase("Close")) {
            System.gc();
            yarnStage.close();
        }
    }

    public void refreshTable() {
        final List<Yarn> items = yarnTable.getItems();
        if( items == null || items.size() == 0) return;

        final Yarn item = yarnTable.getItems().get(0);
        items.remove(0);
        Platform.runLater(new Runnable(){
            @Override
            public void run() {
                items.add(0, item);
            }
        });
     }
    
    public String toRGBCode( Color color ){
        return String.format( "#%02X%02X%02X",
            (int)( color.getRed() * 255 ),
            (int)( color.getGreen() * 255 ),
            (int)( color.getBlue() * 255 ) );
    }
    
    public void start(Stage stage) throws Exception {
        stage.initOwner(WindowView.windowStage);
        new YarnView(stage);
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
