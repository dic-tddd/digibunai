/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mla.yarn;

import com.mla.fabric.FabricAction;
import com.mla.main.Logging;
import java.awt.image.BufferedImage;
import java.io.File;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.imageio.ImageIO;

/**
 * Select a Yarn image from file system for simulation
 * @author Aatif Ahmad Khan
 */
public class YarnSelector {
    String yarnFileName = "Warp_Cotton.png"; // warp file name
    String filePrefix = "Warp_";
    int yarnColorRGB = -1; // default SOLID_WHITE
    Stage selectorStage;
    ScrollPane container;
    GridPane containerGP;
    public YarnSelector(final String yarnType, final int colorRGB){
        selectorStage = new Stage();
        selectorStage.initModality(Modality.APPLICATION_MODAL);//WINDOW_MODAL
        selectorStage.initStyle(StageStyle.UTILITY);
        
        containerGP = new GridPane();
        containerGP.setAlignment(Pos.CENTER);
        containerGP.setHgap(5);
        containerGP.setVgap(5);
        containerGP.setPadding(new Insets(10, 10, 10, 10));
        container = new ScrollPane();
        container.setContent(containerGP);
        
        filePrefix = yarnType+"_";
        yarnColorRGB = colorRGB;
        
        BorderPane parent = new BorderPane();
        Label lblYarnType = new Label("Yarn Type: ");
        ComboBox yarnTypeCB = new ComboBox();
        yarnTypeCB.getItems().addAll("All", "Cotton", "Silk", "Wool");
        yarnTypeCB.setEditable(false); 
        yarnTypeCB.setValue("All");
        yarnTypeCB.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String oldValue, String newValue) {
                if(!newValue.equalsIgnoreCase("All"))
                    filePrefix = yarnType+"_"+newValue;
                else
                    filePrefix = yarnType+"_";
                populateYarnSelectorPane();
            }    
        });
        GridPane topContainerGP = new GridPane();
        topContainerGP.setAlignment(Pos.CENTER);
        topContainerGP.setHgap(5);
        topContainerGP.setVgap(5);
        topContainerGP.setPadding(new Insets(10, 10, 10, 10));
        topContainerGP.add(lblYarnType, 0, 0);
        topContainerGP.add(yarnTypeCB, 1, 0);
        
        parent.setTop(topContainerGP);
        parent.setCenter(container);
        
        populateYarnSelectorPane();
        
        Scene scene = new Scene(parent, 280, 320, Color.WHITE);
        selectorStage.setScene(scene);
        selectorStage.getIcons().add(new Image("/media/icon.png"));
        selectorStage.setTitle("Yarn Selector");
        selectorStage.setIconified(false);
        selectorStage.setResizable(false);        
        selectorStage.showAndWait();
    }
    
    private void populateYarnSelectorPane(){
        containerGP.getChildren().clear();
        try {
            File[] files = new File(System.getProperty("user.dir")+"/mla/yarn").listFiles();
            if(files.length==0){
                containerGP.getChildren().add(new Label("No Yarn Images found."));
            }else{
                for (int i=0, j=0; i<files.length; i++){
                    File file = files[i];
                    if(!file.getName().startsWith(filePrefix))
                        continue;
                    BufferedImage srcImage = ImageIO.read(file);
                    //FabricAction objFabricAction = new FabricAction(false);
                    //objFabricAction.getColoredImage(srcImage, yarnColorRGB);
                    Image image=SwingFXUtils.toFXImage(srcImage, null);
                    final ImageView imageView = new ImageView(image);
                    imageView.setOpacity(0.5);
                    imageView.setFitHeight(80);
                    imageView.setFitWidth(80);
                    imageView.setId(file.getName());
                    // two more imageviews for showing repeated image (continuous yarn)
                    final ImageView imageView1 = new ImageView(image);
                    imageView1.setOpacity(0.5);
                    imageView1.setFitHeight(80);
                    imageView1.setFitWidth(80);
                    imageView1.setId(file.getName());
                    final ImageView imageView2 = new ImageView(image);
                    imageView2.setOpacity(0.5);
                    imageView2.setFitHeight(80);
                    imageView2.setFitWidth(80);
                    imageView2.setId(file.getName());
                    String strTooltip = file.getName();
                    Tooltip toolTip = new Tooltip(strTooltip);
                    Tooltip.install(imageView, toolTip);
                    Tooltip.install(imageView1, toolTip);
                    Tooltip.install(imageView2, toolTip);
                    Pane pane; //hbox or vbox depending on Weft (horizontal yarn) or Warp (vertical yarn)
                    if(filePrefix.startsWith("Weft"))
                        pane = new HBox();
                    else
                        pane = new VBox();
                        
                    pane.setStyle("-fx-border-width: 1;  -fx-border-color: black;");
                    pane.getChildren().addAll(imageView, imageView1, imageView2);
                    
                    imageView.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            yarnFileName = imageView.getId();
                            selectorStage.close();
                        }
                    });
                    imageView1.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            yarnFileName = imageView1.getId();
                            selectorStage.close();
                        }
                    });
                    imageView2.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            yarnFileName = imageView2.getId();
                            selectorStage.close();
                        }
                    });
                    if(filePrefix.startsWith("Weft"))
                        containerGP.add(pane, 0, j); // add hboxes vertically
                    else
                        containerGP.add(pane, j, 0); // add vboxes horizontally
                    j++; // increment number of file names starting with warp
                }
            }
        }catch (Exception ex){
            new Logging("SEVERE",YarnSelector.class.getName(),"populateYarnSelectorPane()",ex);
        }
    }
    
}
