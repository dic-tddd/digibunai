/*
 * Copyright (C) 2017 Media Lab Asia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mla.yarn;

import com.mla.main.Configuration;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.DoubleProperty;
/**
 *
 * @Designing accessor and mutator function for yarn
 * @author Amit Kumar Singh
 * 
 */
public class Yarn {

    private final StringProperty strYarnId;
    private final StringProperty strYarnType;
    private final StringProperty strYarnName;
    private final StringProperty strYarnColor;
    private final IntegerProperty intYarnRepeat;    
    private final StringProperty strYarnSymbol;
    private final IntegerProperty intYarnCount;
    private final StringProperty strYarnCountUnit;
    private final IntegerProperty intYarnPly;
    private final IntegerProperty intYarnDFactor;
    private final DoubleProperty dblYarnDiameter;
    private final IntegerProperty intYarnTwist;
    private final StringProperty strYarnTModel;
    private final IntegerProperty intYarnHairness;
    private final IntegerProperty intYarnHProbability;
    private final DoubleProperty dblYarnPrice;
    private final StringProperty strYarnAccess;
    private final StringProperty strYarnUser;
    private final StringProperty strYarnDate;
   
    private byte[] bytYarnThumbnil;
    
    private Configuration objConfiguration;
    
    private String strSearchBy;
    private String strCondition;
    private String strOrderBy;
    private String strDirection;
    private String strLimit;

    public Yarn(String ytId, String ytType, String ytName, String ytColor, int ytRepeat, String ytSymbol, int ytCount, String ytcUnit, int ytPly, int ytdFactor, double ytdDiameter, int ytTwist, String ytTModel, int ytHairness, int ytHProbability, double ytPrice, String ytAccess, String ytUser, String ytDate) {
        this.strYarnId = new SimpleStringProperty(ytId);
        this.strYarnType = new SimpleStringProperty(ytType);
        this.strYarnName = new SimpleStringProperty(ytName);
        this.strYarnColor = new SimpleStringProperty(ytColor);
        this.intYarnRepeat = new SimpleIntegerProperty(ytRepeat);
        this.strYarnSymbol = new SimpleStringProperty(ytSymbol);
        this.intYarnCount = new SimpleIntegerProperty(ytCount);
        this.strYarnCountUnit = new SimpleStringProperty(ytcUnit);
        this.intYarnPly = new SimpleIntegerProperty(ytPly);
        this.intYarnDFactor = new SimpleIntegerProperty(ytdFactor);
        this.dblYarnDiameter = new SimpleDoubleProperty(ytdDiameter);
        this.intYarnTwist = new SimpleIntegerProperty(ytTwist);
        this.strYarnTModel = new SimpleStringProperty(ytTModel);
        this.intYarnHairness = new SimpleIntegerProperty(ytHairness);
        this.intYarnHProbability = new SimpleIntegerProperty(ytHProbability);
        this.dblYarnPrice = new SimpleDoubleProperty(ytPrice);
        this.strYarnAccess = new SimpleStringProperty(ytAccess);
        this.strYarnUser = new SimpleStringProperty(ytUser);
        this.strYarnDate = new SimpleStringProperty(ytDate);
    }

    public StringProperty strYarnId() {
        return strYarnId;
    }
    public String getStrYarnId() {
        return strYarnId.get();
    }
    public void setStrYarnId(String ytId) {
        strYarnId.set(ytId);
    }
    
    public StringProperty strYarnType() {
        return strYarnType;
    }
    public String getStrYarnType() {
        return strYarnType.get();
    }
    public void setStrYarnType(String ytType) {
        strYarnType.set(ytType);
    }
    
    public StringProperty strYarnName() {
        return strYarnName;
    }
    public String getStrYarnName() {
        return strYarnName.get();
    }
    public void setStrYarnName(String ytName) {
        strYarnName.set(ytName);
    }
    
    public StringProperty strYarnColor() {
        return strYarnColor;
    }
    public String getStrYarnColor() {
        return strYarnColor.get();
    }
    public void setStrYarnColor(String ytColor) {
        strYarnColor.set(ytColor);
    }
    
    public IntegerProperty intYarnRepeat() {
        return intYarnRepeat;
    }
    public int getIntYarnRepeat() {
        return intYarnRepeat.get();
    }
    public void setIntYarnRepeat(int ytRepeat) {
        intYarnRepeat.set(ytRepeat);
    }
    
    public StringProperty strYarnSymbol() {
        return strYarnSymbol;
    }
    public String getStrYarnSymbol() {
        return strYarnSymbol.get();
    }
    public void setStrYarnSymbol(String ytSymbol) {
        strYarnSymbol.set(ytSymbol);
    }
    
    public IntegerProperty intYarnCount() {
        return intYarnCount;
    }
    public int getIntYarnCount() {
        return intYarnCount.get();
    }
    public void setIntYarnCount(int ytCount) {
        intYarnCount.set(ytCount);
    }
    
    public StringProperty strYarnCountUnit() {
        return strYarnCountUnit;
    }
    public String getStrYarnCountUnit() {
        return strYarnCountUnit.get();
    }
    public void setStrYarnCountUnit(String ytcUnit) {
        strYarnCountUnit.set(ytcUnit);
    }
    
    public IntegerProperty intYarnPly() {
        return intYarnPly;
    }
    public int getIntYarnPly() {
        return intYarnPly.get();
    }
    public void setIntYarnPly(int ytPly) {
        intYarnPly.set(ytPly);
    }
    
    public IntegerProperty intYarnDFactor() {
        return intYarnDFactor;
    }
    public int getIntYarnDFactor() {
        return intYarnDFactor.get();
    }
    public void setIntYarnDFactor(int ytdFactor) {
        intYarnDFactor.set(ytdFactor);
    }
    
    public DoubleProperty dblYarnDiameter() {
        return dblYarnDiameter;
    }
    public double getDblYarnDiameter() {
        return dblYarnDiameter.get();
    }
    public void setDblYarnDiameter(double ytdDiameter) {
        dblYarnDiameter.set(ytdDiameter);
    }
    
    public IntegerProperty intYarnTwist() {
        return intYarnTwist;
    }
    public int getIntYarnTwist() {
        return intYarnTwist.get();
    }
    public void setIntYarnTwist(int ytTwist) {
        intYarnTwist.set(ytTwist);
    }
    
    public StringProperty strYarnTModel() {
        return strYarnTModel;
    }
    public String getStrYarnTModel() {
        return strYarnTModel.get();
    }
    public void setStrYarnTModel(String ytTModel) {
        strYarnTModel.set(ytTModel);
    }
    
    public IntegerProperty intYarnHairness() {
        return intYarnHairness;
    }
    public int getIntYarnHairness() {
        return intYarnHairness.get();
    }
    public void setIntYarnHairness(int ytHairness) {
        intYarnHairness.set(ytHairness);
    }
    
    public IntegerProperty intYarnHProbability() {
        return intYarnHProbability;
    }
    public int getIntYarnHProbability() {
        return intYarnHProbability.get();
    }
    public void setIntYarnHProbability(int ytHProbability) {
        intYarnHProbability.set(ytHProbability);
    }
    
    public DoubleProperty dblYarnPrice() {
        return dblYarnPrice;
    }
    public double getDblYarnPrice() {
        return dblYarnPrice.get();
    }
    public void setDblYarnPrice(double ytPrice) {
        dblYarnPrice.set(ytPrice);
    }
    
    public StringProperty strYarnAccess() {
        return strYarnAccess;
    }
    public String getStrYarnAccess() {
        return strYarnAccess.get();        
    }
    public void setStrYarnAccess(String ytAccess) {
        strYarnAccess.set(ytAccess);
    }
    
    public StringProperty strYarnUser() {
        return strYarnUser;
    }
    public String getStrYarnUser() {
        return strYarnUser.get();        
    }
    public void setStrYarnUser(String ytUser) {
        strYarnUser.set(ytUser);
    }
    
    public StringProperty strYarnDate() {
        return strYarnDate;
    }
    public String getStrYarnDate() {
        return strYarnDate.get();        
    }
    public void setStrYarnDate(String ytDate) {
        strYarnDate.set(ytDate);
    }
    
    public byte[] getBytYarnThumbnil() {
        return bytYarnThumbnil;
    }
    public void setBytYarnThumbnil(byte[] bytYarnThumbnil) {
        this.bytYarnThumbnil=bytYarnThumbnil;		
    }
    
    public void setObjConfiguration(Configuration objConfiguration) {
        this.objConfiguration = objConfiguration;
    }
    public Configuration getObjConfiguration() {
        return objConfiguration;
    }
    
    public String getStrCondition() {
        return strCondition;
    }
    public void setStrCondition(String strCondition) {
        this.strCondition=strCondition;		
    }
    
    public String getStrOrderBy() {
        return strOrderBy;
    }
    public void setStrOrderBy(String strOrderBy) {
        this.strOrderBy=strOrderBy;		
    }
    
    public String getStrSearchBy() {
        return strSearchBy;
    }
    public void setStrSearchBy(String strSearchBy) {
        this.strSearchBy=strSearchBy;		
    }
    
    public String getStrDirection() {
        return strDirection;
    }
    public void setStrDirection(String strDirection) {
        this.strDirection=strDirection;		
    }
    
    public void setStrLimit(String strLimit){
        this.strLimit=strLimit;
    }
    public String getStrLimit(){
        return strLimit;
    }
}
