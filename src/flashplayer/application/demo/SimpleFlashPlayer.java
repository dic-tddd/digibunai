/*
 * Christopher Deckers (chrriis@nextencia.net)
 * http://www.nextencia.net
 *
 * See the file "readme.txt" for information on usage and redistribution of
 * this file, and for a DISCLAIMER OF ALL WARRANTIES.
 */
package flashplayer.application.demo;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import flashplayer.application.components.JFlashPlayer;

public class SimpleFlashPlayer extends Composite {

	
  public SimpleFlashPlayer(Composite parent,String imagePath) {
    super(parent, SWT.NONE);
    setLayout(new FillLayout());
    JFlashPlayer flashPlayer = new JFlashPlayer(this);
    
    //Location of the .swf file 
    //For relative path use flashPlayer.load(getClass(),imagePath)
    //For absolute path use flashPlayer.load(imagePath)
    flashPlayer.load(imagePath);
  
  }
  public static void newFunction(String imagePath){
	  Display display = new Display();
	    Shell shell = new Shell(display);
	    shell.setLayout(new FillLayout());
	    new SimpleFlashPlayer(shell,imagePath);
	    shell.setSize(800, 600);
	    shell.open();
	    while(!shell.isDisposed()) {
	      if(!display.readAndDispatch()) {
	        display.sleep();
	      }
	    }
	    display.dispose();
	  
	  
  }

  /* Standard main method to try that test as a standalone application. */
  public static void main(String[] args) {
  newFunction("resource/EnvelopeDistordTest.swf");
  }

}
