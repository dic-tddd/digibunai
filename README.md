# Introduction

**DigiBunai™** is an open source Computer Aided Textile Designing (CATD) application for weaving. It is a Java-based desktop application. Its front end uses JavaFX based user interface. All the business logic is implemented in Java. For backend and data storage MySQL Server is used. For accessing back end, JDBC APIs are used.<br>
[Homepage](https://digibunai.dic.gov.in/)

# Getting Source Code

DigiBunai™ source code may be cloned from its [Gitlab Repository](https://gitlab.com/dic-tddd/digibunai.git).<br>
It is available under GNU General Public License v3 (GPL3).

# Building

Java Development Kit (JDK 8) is required for compiling and building the application. JDK 8 update 202 is the recommended version to use.<br><br>
For Netbeans IDE users, copy all the contents of cloned repository into Netbeans Projects directory. Then, from File menu of the IDE, select Open Project, then select DigiBunai. Then select Clean & Build option for compiling and building the application.<br><br>
Other users need to Compile source code files under “./src” directory of cloned repository and then generate the JAR.

# Reporting Issue/Bug

For Gitlab users bugs/issues can be reported at [Issue Tracker](https://gitlab.com/dic-tddd/digibunai/-/issues).<br><br>
For users not having a Gitlab account, please contact DigiBunai™ team for reporting bug(s) or other issue(s) faced with the application.<br><br>
While reporting bug/issue, please mention following details:<br>
•	Your Name<br>
•	Your Email Address<br>
•	Version of DigiBunai™ application (e.g. 0.9.6 Beta, 1.0 Beta etc.)<br>
•	Module impacted (e.g. Artwork Designer, Dobby Weave etc.)<br>
•	Description of Bug/Issue<br>
•	Expected Output<br>
•	Steps to reproduce the bug/issue<br>
